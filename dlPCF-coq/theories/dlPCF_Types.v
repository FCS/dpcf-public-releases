Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax.
Require PCF_Types.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.
Open Scope vector_scope.

Import SigmaTypeNotations.

From Coq Require Import Vector Fin.
From Coq Require Import Lia.

Open Scope PCF.


(** ** dlPCF Types *)

(** Notes:
- This is the call-by-value version of dlPCF.
- We do not formalise (universal) equational programs. Instead, we use Coq's logic.
  That is, we use Coq's type [nat] to model index terms, and all propositions that
  are true for Coq's [nat] are true for our model of index terms. More precisely, we
  use syntax inspired by PHOAS (polymorphic higher-order syntax) to represent types.
  In the leave nodes of the types (i.e. in [Nat]), a function that takes [ϕ : nat]
  parameters specifies the numerical value of the type.
- We use only strict intervals ([Nat I]), where [I : idx ϕ].
- Operations on contexts are defined in the module [dlPCF_Contexts]
- The typing rules are defined in the module [dlPCF_Typing]
*)




(** *** Model of index types and constraints *)


Implicit Type ϕ : nat.

(** An constraint is imply a predicate over [ϕ]. *)

Definition constr ϕ : Type := Vector.t nat ϕ -> Prop.

(** The constraint that is always true *)
Definition cEmpt {ϕ} : constr ϕ := fun _ => True.
Notation "'c∅'" := (cEmpt).

Hint Unfold cEmpt : core.


(** For example, the index set ϕ=a,b over natural numbers is represented as [ϕ := nat*nat]. *)


(** The type for indexes *)
Definition idx ϕ : Set := Vector.t nat ϕ -> nat.

Definition iConst {ϕ} (n : nat) : idx ϕ := fun _ => n.





(** Semantical entailment *)
Definition entails {ϕ} (Φ : constr ϕ) (Ψ : Vector.t nat ϕ -> Prop) :=
  forall xs : Vector.t nat ϕ, Φ xs -> Ψ xs.

Notation "sem! Φ ⊨ Ψ" := (entails Φ Ψ) (at level 80, format "sem!  Φ  ⊨ '/'  Ψ").


Lemma cEmpt_True {ϕ} (Φ : constr ϕ) :
  sem! Φ ⊨ c∅.
Proof. hnf. firstorder. Qed.

Lemma entails_empty_left_iff {ϕ} (Φ : constr ϕ) :
  (sem! c∅ ⊨ Φ) <-> (forall xs, Φ xs).
Proof. firstorder. Qed.

Lemma entails_refl {ϕ} : reflexive _ (@entails ϕ).
Proof. hnf. firstorder. Qed.

Lemma entails_trans {ϕ} : transitive _ (@entails ϕ).
Proof. hnf. firstorder. Qed.

Instance entails_Pre {ϕ} : PreOrder (@entails ϕ).
Proof.
  constructor.
  - hnf. apply entails_refl.
  - hnf. apply entails_trans.
Qed.


Lemma entails_eq_refl' {ϕ} (Φ : constr ϕ) (i1 i2 : idx ϕ) :
  (forall xs, Φ xs -> i1 xs = i2 xs) ->
  sem! Φ ⊨ (fun xs => i1 xs = i2 xs).
Proof. unfold entails. intuition. Qed.

Lemma entails_eq_refl {ϕ} (Φ : constr ϕ) (i : idx ϕ) :
  sem! Φ ⊨ (fun xs => i xs = i xs).
Proof. apply entails_eq_refl'. reflexivity. Qed.


Definition biEntails {ϕ} (Φ : constr ϕ) (Ψ : Vector.t nat ϕ -> Prop) :=
  forall xs : Vector.t nat ϕ, Φ xs <-> Ψ xs.

Notation "sem! Φ ⟛ Ψ" := (biEntails Φ Ψ) (at level 80, format "sem!  Φ  ⟛ '/'  Ψ").

Lemma biEntails_refl {ϕ} : reflexive _ (@biEntails ϕ).
Proof. hnf. firstorder. Qed.
Lemma biEntails_sym {ϕ} : symmetric _ (@biEntails ϕ).
Proof. hnf. firstorder. Qed.
Lemma biEntails_trans {ϕ} : transitive _ (@biEntails ϕ).
Proof. hnf. firstorder. Qed.

Instance biEntails_Equiv {ϕ} : Equivalence (@biEntails ϕ).
Proof.
  constructor.
  - hnf. apply biEntails_refl.
  - hnf. apply biEntails_sym.
  - hnf. apply biEntails_trans.
Qed.

Instance entails_Proper {ϕ} : Proper (@biEntails ϕ ==> @biEntails ϕ ==> Basics.flip Basics.impl) (@entails ϕ).
Proof. firstorder. Qed.




(** Linear and modal types *)
Inductive lty (ϕ : nat) : Type :=
| Arr (τ1 : mty ϕ) (τ2 : mty ϕ) : lty ϕ
with mty (ϕ : nat) : Type :=
| Nat (i : idx ϕ) : mty ϕ
| Quant (i : idx ϕ) (A : lty (S ϕ)) : mty ϕ
.

Infix "⊸" := Arr (at level 60, right associativity).

Notation "'[' '<' I ']' '⋅' A" := (Quant I A) (at level 59, A at level 65, right associativity) : PCF.

(** Default/error types. Note that these are defined with [Qed]. *)
Definition def_nat : nat. Proof. exact 42. Qed.
Definition def_idx {ϕ} : idx ϕ. Proof. exact (fun _ => def_nat). Qed.
Definition def_mty {ϕ} : mty ϕ. Proof. exact (Nat def_idx). Qed.
Definition def_lty {ϕ} : lty ϕ. Proof. exact (Arr def_mty def_mty). Qed.



(*
Check [ < iConst 3] ⋅ (Nat (fun xs => xs[@Fin0]) ⊸ Nat (fun xs => S xs[@Fin0])).
Check [< iConst 3] ⋅ (Nat (fun xs => xs[@Fin0]) ⊸ [< fun xs => S xs[@Fin0]] ⋅ Nat (fun xs => xs[@Fin1]) ⊸ Nat (fun xs => xs[@Fin0] + xs[@Fin1])).
*)


Fixpoint size_lty {ϕ} (A : lty ϕ) : nat :=
  match A with
  | σ ⊸ τ => S (size_mty σ + size_mty τ)
  end
with size_mty {ϕ} (τ : mty ϕ) : nat :=
  match τ with
  | Nat K => 1
  | Quant i A => S (size_lty A)
  end.





(** *** Safe casting for types (for ϕ) *)

Section TypeCast.

  Definition idx_cast {ϕ1 ϕ2} (i : idx ϕ1) (Heq: ϕ2=ϕ1) : idx ϕ2.
  Proof. hnf in i|-*. intros. apply i. eapply Vector.cast; eassumption. Defined.

  Lemma idx_cast_twice {ϕ1 ϕ2 ϕ3} (i : idx ϕ1) (Heq1: ϕ2 = ϕ1) (Heq2: ϕ3 = ϕ2) (Heq3: ϕ3 = ϕ1) :
    idx_cast (idx_cast i Heq1) Heq2 = idx_cast i Heq3.
  Proof. fext; intros xs. unfold idx_cast. subst. rewrite !vect_cast_id. reflexivity. Qed.

  Lemma idx_cast_inv {ϕ1 ϕ2} (i : idx ϕ1) (Heq1: ϕ1 = ϕ2) (Heq2: ϕ2 = ϕ1) :
    idx_cast (idx_cast i Heq2) Heq1 = i.
  Proof. unfold idx_cast. fext; intros xs. f_equal. now rewrite vect_cast_inv. Qed.

  Fixpoint lty_cast {ϕ1 ϕ2} (A : lty ϕ1) (Heq: ϕ2=ϕ1) : lty ϕ2
  with mty_cast {ϕ1 ϕ2} (τ : mty ϕ1) (Heq: ϕ2=ϕ1) : mty ϕ2.
  Proof.
    {
      destruct A.
      refine (_ ⊸ _); eapply mty_cast.
      - apply τ1.
      - apply Heq.
      - apply τ2.
      - apply Heq.
    }
    {
      destruct τ.
      - refine (Nat _). eapply idx_cast.
        + apply i.
        + apply Heq.
      - refine (Quant _ _).
        + eapply idx_cast.
          * apply i.
          * apply Heq.
        + eapply lty_cast.
          * eassumption.
          * abstract (apply f_equal; eassumption).
    }
  Defined.

  (* Eval cbv -[hd tl Vector.cast idx_cast lty_cast f_equal] in *)
  (*     lty_cast ((Nat hd ⊸ Nat (hd >> S))) (_ : 7 = 3+4). *)

  (* Eval cbv -[hd tl Vector.cast idx_cast lty_cast f_equal] in *)
  (*     mty_cast ([< iConst 3] ⋅ (Nat hd ⊸ Nat (hd >> S))) (_ : 7 = 3+4). *)

  Lemma idx_cast_id {ϕ} (H : ϕ=ϕ) (i : idx ϕ) :
    idx_cast i H = i.
  Proof.
    fext. intros xs. unfold idx_cast.
    f_equal.
    clear i. induction xs in H|-*; cbn; f_equal; auto.
  Qed.

  Fixpoint lty_cast_id {ϕ} (H : ϕ=ϕ) (A : lty ϕ) :
    lty_cast A H = A
  with mty_cast_id {ϕ} (H : ϕ=ϕ) (τ : mty ϕ) :
    mty_cast τ H = τ.
  Proof.
    - destruct A; cbn. f_equal; apply mty_cast_id.
    - destruct τ; cbn; f_equal.
      + apply idx_cast_id.
      + apply idx_cast_id.
      + apply lty_cast_id.
  Qed.


  Definition constr_cast {ϕ1 ϕ2} (Φ : constr ϕ1) (H : ϕ2 = ϕ1) : constr ϕ2 :=
    fun xs => Φ (vect_cast xs H).

  Lemma constr_cast_id {ϕ} (Φ : constr ϕ) (H : ϕ = ϕ) :
    constr_cast Φ H = Φ.
  Proof. unfold constr_cast; fext; cbn. intros. now rewrite vect_cast_id. Qed.

End TypeCast.



(** *** Subtyping rules *)

Definition Arr_proj1 {m} (A : lty m) : mty m :=
  match A with
  | σ1 ⊸ σ2 => σ1
  end.

Definition Arr_proj2 {m} (A : lty m) : mty m :=
  match A with
  | σ1 ⊸ σ2 => σ2
  end.

Lemma Arr_eta {ϕ} (A : lty ϕ) :
  Arr_proj1 A ⊸ Arr_proj2 A = A.
Proof. now destruct A. Qed.



Reserved Notation "'mty!' Φ '⊢' σ ⊑ τ" (at level 80, format "mty!  Φ  ⊢ '/' '['  σ  ⊑ '/'  τ ']'", no associativity).
Reserved Notation "'lty!' Φ '⊢' σ ⊑ τ" (at level 80, format "lty!  Φ  ⊢ '/' '['  σ  ⊑ '/'  τ ']'", no associativity).
Reserved Notation "'mty!' Φ '⊢' σ ≡ τ" (at level 80, format "mty!  Φ  ⊢ '/' '['  σ  ≡ '/'  τ ']'", no associativity).
Reserved Notation "'lty!' Φ '⊢' σ ≡ τ" (at level 80, format "lty!  Φ  ⊢ '/' '['  σ  ≡ '/'  τ ']'", no associativity).

(* Notations that use [c∅] *)
Reserved Notation "σ ⊑l τ" (at level 80, no associativity).
Reserved Notation "σ ⊑m τ" (at level 80, no associativity).
Reserved Notation "σ ≡l τ" (at level 80, no associativity).
Reserved Notation "σ ≡m τ" (at level 80, no associativity).

(* Note that [σ ≡m τ] is equivalent to [σ = τ], up to functional extensionality *)


Inductive sublty {ϕ : nat} (Φ : constr ϕ) : lty ϕ -> lty ϕ -> Prop :=
| sublty_arr τ1 τ2 σ1 σ2 :
    mty! Φ ⊢ σ2 ⊑ σ1 ->
    mty! Φ ⊢ τ1 ⊑ τ2 ->
    lty! Φ ⊢ (σ1 ⊸ τ1) ⊑ (σ2 ⊸ τ2)
where "lty! Φ ⊢ A ⊑ B" := (sublty Φ A B) : PCF
with submty (ϕ : nat) (Φ : constr ϕ) : mty ϕ -> mty ϕ -> Prop :=
| submty_Nat (k1 k2 : idx ϕ) :
    (sem! Φ ⊨ (fun xs => k1 xs = k2 xs)) ->
    mty! Φ ⊢ Nat k1 ⊑ Nat k2
| submty_Quant (i j : idx ϕ) (A B : lty (S ϕ)) :
    (@sublty (S ϕ) (fun xs => hd xs < j (tl xs) /\ Φ (Vector.tl xs)) A B) ->
    (sem! Φ ⊨ fun xs => j xs <= i xs) ->
    mty! Φ ⊢ Quant i A ⊑ Quant j B
(* | submty_Trans (σ1 σ2 σ3 : mty ϕ) : (* Might be admissible *) *)
(*     mty! Φ ⊢ σ1 ⊑ σ2 -> *)
(*     mty! Φ ⊢ σ2 ⊑ σ3 -> *)
(*     mty! Φ ⊢ σ1 ⊑ σ3 *)
where "mty! Φ ⊢ A ⊑ B" := (submty Φ A B) : PCF.

Notation "A ⊑l B" := (sublty c∅ A B).
Notation "σ ⊑m τ" := (submty c∅ σ τ).


(** Inversion lemmas *)


Lemma sublty_arr_inv' {ϕ} {Φ : constr ϕ} {σ1 σ2 : mty ϕ} {B : lty ϕ} :
  lty! Φ ⊢ σ1 ⊸ σ2 ⊑ B ->
  exists τ1 τ2, B = τ1 ⊸ τ2 /\
           mty! Φ ⊢ σ2 ⊑ τ2 /\
           mty! Φ ⊢ τ1 ⊑ σ1.
Proof. inversion 1; eauto. Qed.

Lemma sublty_arr_inv {ϕ} {Φ : constr ϕ} {σ1 σ2 τ1 τ2 : mty ϕ} :
  lty! Φ ⊢ σ1 ⊸ σ2 ⊑ τ1 ⊸ τ2 ->
  mty! Φ ⊢ σ2 ⊑ τ2 /\
  mty! Φ ⊢ τ1 ⊑ σ1.
Proof. now inversion 1. Qed.


Lemma submty_Nat_inv'  {ϕ} {Φ : constr ϕ} {k : idx ϕ} (σ : mty ϕ) :
  mty! Φ ⊢ Nat k ⊑ σ ->
  exists k', σ = Nat k' /\
        sem! Φ ⊨ fun xs => k' xs = k xs.
Proof.
  intros H. remember (Nat k) as τ. induction H; try congruence; subst.
  repeat eexists. firstorder congruence.
Qed.

Lemma submty_Nat_inv  {ϕ} {Φ : constr ϕ} {k1 k2 : idx ϕ} :
  mty! Φ ⊢ Nat k1 ⊑ Nat k2 ->
  sem! Φ ⊨ fun xs => k1 xs = k2 xs.
Proof. intros (k'&[=->]&H1) % submty_Nat_inv'. firstorder. Qed.

Lemma submty_Nat_inv'_sig  {ϕ} {Φ : constr ϕ} {k : idx ϕ} (σ : mty ϕ) :
  mty! Φ ⊢ Nat k ⊑ σ ->
  existsS k', σ = Nat k' /\
        sem! Φ ⊨ fun xs => k' xs = k xs.
Proof.
  intros H. destruct σ.
  - exists i. split; auto. apply submty_Nat_inv in H. firstorder.
  - exfalso. inv H.
Qed.


Lemma submty_Quant_inv' {ϕ} {Φ : constr ϕ} {i : idx ϕ} {A : lty (S ϕ)} {τ2 : mty ϕ} :
  mty! Φ ⊢ [< i]⋅ A ⊑ τ2 ->
  exists j B,
    (@sublty (S ϕ) (fun xs => hd xs < j (tl xs) /\ Φ (Vector.tl xs)) A B) /\
    (sem! Φ ⊨ fun xs => j xs <= i xs) /\
    τ2 = [< j] ⋅ B.
Proof. inversion 1; eauto. Qed.

Lemma submty_Quant_inv {ϕ} {Φ : constr ϕ} {i : idx ϕ} {A : lty (S ϕ)} {j : idx ϕ} {B : lty (S ϕ)} :
  mty! Φ ⊢ [< i]⋅ A ⊑ [< j] ⋅ B ->
  (@sublty (S ϕ) (fun xs => hd xs < j (tl xs) /\ Φ (Vector.tl xs)) A B) /\
  (sem! Φ ⊨ fun xs => j xs <= i xs).
Proof. intros (j'&B'&H1&H2& [= -> <-]) % submty_Quant_inv'. firstorder. Qed.

Lemma submty_Quant_inv'_sig {ϕ} {Φ : constr ϕ} {i : idx ϕ} {A : lty (S ϕ)} {τ2 : mty ϕ} :
  mty! Φ ⊢ [< i]⋅ A ⊑ τ2 ->
  existsS j B,
    (@sublty (S ϕ) (fun xs => hd xs < j (tl xs) /\ Φ (Vector.tl xs)) A B) /\
    (sem! Φ ⊨ fun xs => j xs <= i xs) /\
    τ2 = [< j] ⋅ B.
Proof.
  intros H. destruct τ2.
  - exfalso. inv H.
  - apply submty_Quant_inv in H as (H1&H2). eexists _,_; split; eauto.
Qed.


(** Reflexivity *)

Fixpoint sublty_refl {ϕ} (Φ : constr ϕ) (A : lty ϕ) :
  lty! Φ ⊢ A ⊑ A
with submty_refl {ϕ} (Φ : constr ϕ) τ :
  mty! Φ ⊢ τ ⊑ τ.
Proof.
  - destruct A as [σ τ]. constructor; apply submty_refl.
  - destruct τ as [k | i Ai].
    + apply submty_Nat. apply entails_eq_refl.
    + apply submty_Quant.
      * intros. apply sublty_refl.
      * hnf; reflexivity.
Qed.



(** We can replace the constraint with a stronger one. *)

Fixpoint sublty_mono_Φ {ϕ} {Φ : constr ϕ} (A B : lty ϕ) (H1 : lty! Φ ⊢ A ⊑ B) {struct H1 } :
  forall (Ψ : constr ϕ),
    sem! Ψ ⊨ Φ ->
    lty! Ψ ⊢ A ⊑ B
with submty_mono_Φ {ϕ} {Φ : constr ϕ} (σ τ : mty ϕ) (H1 : mty! Φ ⊢ σ ⊑ τ) { struct H1 } :
  forall (Ψ : constr ϕ),
    sem! Ψ ⊨ Φ ->
    mty! Ψ ⊢ σ ⊑ τ.
Proof.
  {
    destruct H1 as [ σ1 τ1 σ2 τ2 H1 H2 ]. intros Ψ H3.
    pose proof submty_mono_Φ _ _ _ _ H1 _ H3 as IH1.
    pose proof submty_mono_Φ _ _ _ _ H2 _ H3 as IH2.
    constructor; assumption.
  }
  {
    destruct H1.
    - intros Ψ H2. constructor. etransitivity; eauto.
    - intros Ψ H2. constructor.
      + eapply sublty_mono_Φ; eauto. hnf; intuition.
      + hnf; intuition.
  }
Qed.





(** Transitivity *)


(* In the following lemma, we would need a mutual double-induction over two types. It was easier to prove the lemma
using size induction. *)

Lemma sublty_submty_trans {ϕ} (Φ : constr ϕ) (s : nat) :
  (forall A B C,
      size_lty A + size_lty B + size_lty C <= s ->
      lty! Φ ⊢ A ⊑ B ->
      lty! Φ ⊢ B ⊑ C ->
      lty! Φ ⊢ A ⊑ C) /\
  (forall σ1 σ2 σ3,
      size_mty σ1 + size_mty σ2 + size_mty σ3 <= s ->
      mty! Φ ⊢ σ1 ⊑ σ2 ->
      mty! Φ ⊢ σ2 ⊑ σ3 ->
      mty! Φ ⊢ σ1 ⊑ σ3).
Proof.
  revert dependent ϕ. induction s as [s IH] using lt_wf_ind; intros; split.
  - intros A B C Hs H1 H2. destruct H1. eapply sublty_arr_inv' in H2 as (ρ1 & ρ2 & -> & H2 & H3); cbn in Hs.
    econstructor; eapply IH; eauto; lia.
  - intros σ1 σ2 σ3 Hs H1 H2. destruct H1.
    + apply submty_Nat_inv' in H2 as (k2'&->&H2).
      apply submty_Nat. hnf in H,H2|-*. firstorder congruence.
    + apply submty_Quant_inv' in H2 as (j'&B'&H2&H3&->); cbn in Hs.
      econstructor.
      * eapply IH; eauto. lia.
        eapply sublty_mono_Φ. apply H.
        hnf; intros xs (Hxs1&Hxs2); split; eauto.
        hnf in H0,H3|-*. specialize (H0 _ Hxs2); specialize (H3 _ Hxs2). lia.
      * hnf in H0,H3|-*. firstorder omega.
Qed.

Lemma sublty_trans {ϕ} (Φ : constr ϕ) : forall A B C, lty! Φ ⊢ A ⊑ B -> lty! Φ ⊢ B ⊑ C -> lty! Φ ⊢ A ⊑ C.
Proof. intros. eapply sublty_submty_trans; eauto. Qed.

Lemma submty_trans {ϕ} (Φ : constr ϕ) : forall σ1 σ2 σ3, mty! Φ ⊢ σ1 ⊑ σ2 -> mty! Φ ⊢ σ2 ⊑ σ3 -> mty! Φ ⊢ σ1 ⊑ σ3.
Proof. intros. eapply sublty_submty_trans; eauto. Qed.


Instance sublty_PreOrder {ϕ} {Φ : constr ϕ} : PreOrder (@sublty ϕ Φ).
Proof. constructor; hnf. apply sublty_refl. apply sublty_trans. Qed.

Instance submty_PreOrder {ϕ} {Φ : constr ϕ} : PreOrder (@submty ϕ Φ).
Proof. constructor; hnf. apply submty_refl. apply submty_trans. Qed.


(** *** Type equivalence *)


Definition eqlty {ϕ} (Φ : constr ϕ) (A B : lty ϕ) : Prop := lty! Φ ⊢ A ⊑ B /\ lty! Φ ⊢ B ⊑ A.
Notation "'lty!' Φ '⊢' A ≡ B" := (eqlty Φ A B).
Notation "A ≡l B" := (eqlty c∅ A B).

Hint Unfold eqlty : core.

Lemma eqlty_refl {ϕ} (Φ : constr ϕ) : reflexive _ (eqlty Φ).
Proof. hnf. split; eauto using sublty_refl. Qed.

Lemma eqlty_sym {ϕ} (Φ : constr ϕ) : symmetric _ (eqlty Φ).
Proof. hnf. split; firstorder. Qed.

Lemma eqlty_trans {ϕ} (Φ : constr ϕ) : transitive _ (eqlty Φ).
Proof. unfold transitive, eqlty. intuition (etransitivity; eauto). Qed.

Instance eqlty_Equiv {ϕ} {Φ : constr ϕ} : Equivalence (eqlty Φ).
Proof.
  unfold eqlty. constructor.
  - hnf. apply eqlty_refl.
  - hnf. apply eqlty_sym.
  - hnf. apply eqlty_trans.
Qed.

Lemma eqlty_eq {ϕ} (Φ : constr ϕ) A1 A2 :
  A1 = A2 ->
  lty! Φ ⊢ A1 ≡ A2.
Proof. intros ->. reflexivity. Qed.

Lemma sublty_eqmty {ϕ} {Φ : constr ϕ} A1 A2 A1' A2' :
  lty! Φ ⊢ A1  ⊑ A2 ->
  lty! Φ ⊢ A1  ≡ A1' ->
  lty! Φ ⊢ A2  ≡ A2' ->
  lty! Φ ⊢ A1' ⊑ A2'.
Proof.
  intros H1 [H2 H3] [H4 H5]. etransitivity.
  - apply H3.
  - etransitivity.
    + apply H1.
    + apply H4.
Qed.

(* The setoid rewriting version of the above lemma *)
Instance sublty_Proper {ϕ} {Φ : constr ϕ} : Proper (@eqlty ϕ Φ ==> @eqlty ϕ Φ ==> Basics.flip Basics.impl) (@sublty ϕ Φ).
Proof.
  hnf. intros. hnf. intros. hnf. intros. eapply sublty_eqmty; eauto.
  - now symmetry.
  - now symmetry.
Qed.


Definition eqmty {ϕ} (Φ : constr ϕ) (σ τ : mty ϕ) : Prop :=
  mty! Φ ⊢ σ ⊑ τ /\ mty! Φ ⊢ τ ⊑ σ.
Notation "'mty!' Φ '⊢' σ ≡ τ" := (eqmty Φ σ τ).
Notation "σ ≡m τ" := (eqmty c∅ σ τ).

Hint Unfold eqmty : core.

Lemma eqmty_refl {ϕ} (Φ : constr ϕ) : reflexive _ (eqmty Φ).
Proof. hnf. split; eauto using submty_refl. Qed.

Lemma eqmty_trans {ϕ} (Φ : constr ϕ) : transitive _ (eqmty Φ).
Proof. hnf. split; firstorder etransitivity; eauto. Qed.

Lemma eqmty_sym {ϕ} (Φ : constr ϕ) : symmetric _ (eqmty Φ).
Proof. hnf. split; firstorder. Qed.

Instance eqmty_Equiv {ϕ} (Φ : constr ϕ) : Equivalence (eqmty Φ).
Proof.
  unfold eqmty. constructor.
  - hnf. apply eqmty_refl.
  - hnf. apply eqmty_sym.
  - hnf. apply eqmty_trans.
Qed.

Lemma eqmty_eq {ϕ} (Φ : constr ϕ) τ1 τ2 :
  τ1 = τ2 ->
  mty! Φ ⊢ τ1 ≡ τ2.
Proof. intros ->. reflexivity. Qed.


Lemma submty_eqmty {ϕ} (Φ : constr ϕ) τ1 τ2 τ1' τ2' :
  mty! Φ ⊢ τ1  ⊑ τ2 ->
  mty! Φ ⊢ τ1  ≡ τ1' ->
  mty! Φ ⊢ τ2  ≡ τ2' ->
  mty! Φ ⊢ τ1' ⊑ τ2'.
Proof.
  intros H1 [H2 H3] [H4 H5]. etransitivity.
  - apply H3.
  - etransitivity.
    + apply H1.
    + apply H4.
Qed.

(* The setoid rewriting version of the above lemma *)
Instance submty_Proper {ϕ} {Φ : constr ϕ} : Proper (eqmty Φ ==> eqmty Φ ==> Basics.flip Basics.impl) (submty Φ).
Proof.
  hnf. intros. hnf. intros. hnf. intros. eapply submty_eqmty; eauto.
  - now symmetry.
  - now symmetry.
Qed.


Lemma eqlty_mono_Φ {ϕ} {Φ : constr ϕ} (A B : lty ϕ) :
  lty! Φ ⊢ A ≡ B ->
  forall (Ψ : constr ϕ),
    sem! Ψ ⊨ Φ ->
    lty! Ψ ⊢ A ≡ B.
Proof.
  intros [H1 H2] Ψ H3. split.
  - eapply sublty_mono_Φ; eauto.
  - eapply sublty_mono_Φ; eauto.
Qed.

Lemma eqmty_mono_Φ {ϕ} {Φ : constr ϕ} (σ τ : mty ϕ) :
  mty! Φ ⊢ σ ≡ τ ->
  forall (Ψ : constr ϕ),
    sem! Ψ ⊨ Φ ->
    mty! Ψ ⊢ σ ≡ τ.
Proof.
  intros [H1 H2] Ψ H3. split.
  - eapply submty_mono_Φ; eauto.
  - eapply submty_mono_Φ; eauto.
Qed.

Lemma eqlty_Arr {ϕ} (Φ : constr ϕ) σ1 σ2 τ1 τ2 :
  mty! Φ ⊢ σ1 ≡ τ1 -> mty! Φ ⊢ σ2 ≡ τ2 ->
  lty! Φ ⊢ σ1 ⊸ σ2 ≡ τ1 ⊸ τ2.
Proof. intros [H1 H2] [H3 H4]. split; apply sublty_arr; eauto. Qed.

Lemma eqlty_Arr_inv' {ϕ} (Φ : constr ϕ) σ1 σ2 B :
  (lty! Φ ⊢ σ1 ⊸ σ2 ≡ B) ->
  (exists τ1 τ2, B = τ1 ⊸ τ2 /\ mty! Φ ⊢ σ1 ≡ τ1 /\ mty! Φ ⊢ σ2 ≡ τ2).
Proof.
  intros [(τ1&τ2&->&H1&H2) % sublty_arr_inv' H3].
  apply sublty_arr_inv in H3 as [H3 H4].
  eexists _,_. split; [ | split]. reflexivity.
  all: repeat split; eauto using submty_trans.
Qed.

Lemma eqlty_Arr_inv {ϕ} (Φ : constr ϕ) σ1 σ2 τ1 τ2 :
  (lty! Φ ⊢ σ1 ⊸ σ2 ≡ τ1 ⊸ τ2) ->
  (mty! Φ ⊢ σ1 ≡ τ1 /\ mty! Φ ⊢ σ2 ≡ τ2).
Proof.
  intros [[H1 H2] % sublty_arr_inv [H3 H4] % sublty_arr_inv].
  repeat split; eauto using submty_trans.
Qed.

Lemma eqlty_Arr_inv'_sig {ϕ} (Φ : constr ϕ) σ1 σ2 B :
  (lty! Φ ⊢ σ1 ⊸ σ2 ≡ B) ->
  (existsS τ1 τ2, B = τ1 ⊸ τ2 /\ mty! Φ ⊢ σ1 ≡ τ1 /\ mty! Φ ⊢ σ2 ≡ τ2).
Proof.
  intros H. destruct B.
  eexists _, _. split. reflexivity.
  now apply eqlty_Arr_inv in H.
Qed.


Lemma eqmty_Nat {ϕ} (Φ : constr ϕ) k1 k2 :
  (sem! Φ ⊨ fun xs => k1 xs = k2 xs) ->
  mty! Φ ⊢ Nat k1 ≡ Nat k2.
Proof. split; eapply submty_Nat; eauto; firstorder. Qed.

Lemma eqmty_Nat_inv' {ϕ} (Φ : constr ϕ) k τ :
  (mty! Φ ⊢ Nat k ≡ τ) ->
  (exists k', τ = Nat k' /\ sem! Φ ⊨ fun xs => k' xs = k xs).
Proof. intros [(k'&->&H) % submty_Nat_inv' _ ]. eauto. Qed.

Lemma eqmty_Nat_inv {ϕ} (Φ : constr ϕ) k1 k2 :
  (mty! Φ ⊢ Nat k1 ≡ Nat k2) -> (sem! Φ ⊨ fun xs => k1 xs = k2 xs).
Proof. intros [(k'&[= ->]&H) % submty_Nat_inv' _ ]. firstorder. Qed.

Lemma eqmty_Nat_inv'_sig {ϕ} (Φ : constr ϕ) k τ :
  (mty! Φ ⊢ Nat k ≡ τ) ->
  (existsS k', τ = Nat k' /\ sem! Φ ⊨ fun xs => k' xs = k xs).
Proof.
  intros H.
  destruct τ.
  - apply eqmty_Nat_inv in H. eexists; split. reflexivity. firstorder.
  - exfalso. apply eqmty_Nat_inv' in H as (k'&[=]&_).
Qed.

Lemma eqmty_Nat_empty {ϕ} k k' :
  k = k' ->
  (mty! c∅ ⊢ @Nat ϕ k ≡ Nat k').
Proof. intros ->. reflexivity. Qed.

Lemma eqmty_Nat_empty_inv {ϕ} k (τ : mty ϕ) :
  (mty! c∅ ⊢ Nat k ≡ τ) -> (τ = Nat k).
Proof.
  intros (j&->&H) % eqmty_Nat_inv'.
  f_equal; fext; firstorder.
Qed.



Lemma eqmty_Quant {ϕ} (Φ : constr ϕ) i A j B :
  (@eqlty (S ϕ) (fun xs => hd xs < j (tl xs) /\ Φ (Vector.tl xs)) A B) ->
  (sem! Φ ⊨ fun xs => i xs = j xs) ->
  mty! Φ ⊢ [< i] ⋅ A ≡ [< j] ⋅ B.
Proof.
  intros [H1 H2] H3. split; constructor.
  - eapply sublty_mono_Φ; eauto. firstorder.
  - hnf in *; firstorder.
  - eapply sublty_mono_Φ; eauto.
    hnf. intros xs (Hxs1&Hxs2). split. 2: tauto.
    enough (i (tl xs) = j (tl xs)) by omega.
    firstorder.
  - hnf in *; firstorder.
Qed.

Lemma eqmty_Quant_inv {ϕ} (Φ : constr ϕ) i A j B :
  (mty! Φ ⊢ [< i] ⋅ A ≡ [< j] ⋅ B) ->
  (@eqlty (S ϕ) (fun xs => hd xs < j (tl xs) /\ Φ (Vector.tl xs)) A B /\
    sem! Φ ⊨ fun xs => i xs = j xs).
Proof.
  intros [ [H1 H2] % submty_Quant_inv [H3 H4] % submty_Quant_inv ].
  repeat split.
  - firstorder.
  - eapply sublty_mono_Φ; eauto.
    hnf. intros xs (Hxs1&Hxs2). split. 2: tauto.
    enough (j (tl xs) <= i (tl xs)) by omega.
    firstorder.
  - firstorder.
Qed.

Lemma eqmty_Quant_inv' {ϕ} (Φ : constr ϕ) i A τ :
  (mty! Φ ⊢ [< i] ⋅ A ≡ τ) ->
  (exists j B, τ = [< j] ⋅ B /\
          @eqlty (S ϕ) (fun xs => hd xs < (j (tl xs)) /\ Φ (Vector.tl xs)) A B /\
          (sem! Φ ⊨ fun xs => i xs = j xs)).
Proof.
  intros [ (j&B&H1&H2&->) % submty_Quant_inv' [H3 H4] % submty_Quant_inv ].
  repeat eexists.
  - eapply sublty_mono_Φ; eauto. firstorder.
  - eapply sublty_mono_Φ; eauto.
    hnf. intros xs (Hxs1&Hxs2). split. 2: tauto.
    enough (j (tl xs) <= i (tl xs)) by omega.
    firstorder.
  - firstorder.
Qed.

Lemma eqmty_Quant_inv'_sig {ϕ} (Φ : constr ϕ) i A τ :
  (mty! Φ ⊢ [< i] ⋅ A ≡ τ) ->
  (existsS j B, τ = [< j] ⋅ B /\
          @eqlty (S ϕ) (fun xs => hd xs < (j (tl xs)) /\ Φ (Vector.tl xs)) A B /\
          (sem! Φ ⊨ fun xs => i xs = j xs)).
Proof.
  intros H. destruct τ.
  - exfalso. symmetry in H. apply eqmty_Nat_inv' in H as (k&[=]&_).
  - apply eqmty_Quant_inv in H as (H1&H2). eexists _,_; split; eauto.
Qed.

Lemma eqmty_Quant_empty_inv {ϕ} (i : idx ϕ) A j B :
  (mty! c∅ ⊢ [< i] ⋅ A ≡ [< j] ⋅ B) ->
  (lty! (fun xs => hd xs < i (tl xs)) ⊢ A ≡ B /\ i = j).
Proof.
  intros (H1 & H2) % eqmty_Quant_inv.
  split.
  - eapply eqlty_mono_Φ; eauto.
    hnf in H2. hnf; intros xs Hxs. split; auto. now rewrite <- H2.
  - fext; firstorder.
Qed.

Lemma eqmty_Quant_empty {ϕ} (i : idx ϕ) A j B :
  (lty! (fun xs => hd xs < i (tl xs)) ⊢ A ≡ B) ->
  i = j ->
  mty! c∅ ⊢ [< i] ⋅ A ≡ [< j] ⋅ B.
Proof.
  intros H ->. eapply eqmty_Quant.
  - eapply eqlty_mono_Φ; eauto. firstorder.
  - hnf; reflexivity.
Qed.



(** *** Substitution for types *)

(** **** General parallel substitution *)

(** Apply a function [f] on all indexes in the type *)

Fixpoint subst_lty {m n : nat} (A : lty m) (f : idx m -> idx n) { struct A } : lty n :=
  match A with
  | Arr τ1 τ2 => Arr (subst_mty τ1 f) (subst_mty τ2 f)
  end
with subst_mty {m n :  nat} (τ : mty m) (f : idx m -> idx n) { struct τ } : mty n :=
  match τ with
  | Nat i => Nat (f i)
  | Quant i A =>
    Quant
      (f i)
      (subst_lty
         A
         (* Here We build a new funtion [f' : idx (S m) -> idx (S n)] using [f : idx m -> idx n]. *)
         (* Note that we don't use the [i] from [Quant i A]. *)
         (fun (i' : idx (S m)) =>
            fun (xs : Vector.t nat (S n)) =>
              let j : idx n := f (fun ys : Vector.t nat m => i' (hd xs ::: ys)) in
              j (tl xs)))
  end.


(** Apply substitution twice *)
Fixpoint subst_lty_twice (m n p : nat) (A : lty m) (f1 : idx m -> idx n) (f2 : idx n -> idx p) { struct A } :
  subst_lty (subst_lty A f1) f2 = subst_lty A (f1 >> f2)
with subst_mty_twice (m n p : nat) (τ : mty m) (f1 : idx m -> idx n) (f2 : idx n -> idx p) { struct τ } :
  subst_mty (subst_mty τ f1) f2 = subst_mty τ (f1 >> f2).
Proof.
  - destruct A; cbn; f_equal; apply subst_mty_twice.
  - destruct τ; cbn; f_equal; apply subst_lty_twice.
Qed.

(** Identity substitution *)
Fixpoint subst_lty_id (m : nat) (A : lty m) { struct A } :
  subst_lty A id = A
with subst_mty_id (m : nat) (τ : mty m) { struct τ } :
  subst_mty τ id = τ.
Proof.
  - destruct A; cbn; f_equal; apply subst_mty_id.
  - destruct τ; cbn.
    + reflexivity.
    + f_equal. rewrite <- subst_lty_id.
      f_equal; fext; intros i' xs. unfold id. f_equal. symmetry; apply Vector.eta.
Qed.

Lemma subst_lty_id' {ϕ} (A : lty ϕ) (f : idx ϕ -> idx ϕ) :
  (forall (g : idx ϕ) (xs : Vector.t nat ϕ), f g xs = g xs) ->
  subst_lty A f = A.
Proof. intros H. rewrite <- subst_lty_id. f_equal; fext; auto. Qed.
Lemma subst_mty_id' {ϕ} (τ : mty ϕ) (f : idx ϕ -> idx ϕ) :
  (forall (g : idx ϕ) (xs : Vector.t nat ϕ), f g xs = g xs) ->
  subst_mty τ f = τ.
Proof. intros H. rewrite <- subst_mty_id. f_equal; fext; auto. Qed.


(** We can use equivalent indexes *)
Fixpoint sublty_subst_idx_sem (m n : nat) (Φ : constr n) (f1 f2 : idx m -> idx n) (A : lty m) :
  (sem! Φ ⊨ fun xs => forall (j : idx m), f1 j xs = f2 j xs) ->
  (lty! Φ ⊢ subst_lty A f1 ⊑ subst_lty A f2)
with submty_subst_idx_sem (m n : nat) (Φ : constr n) (f1 f2 : idx m -> idx n) (τ : mty m) :
  (sem! Φ ⊨ fun xs => forall (j : idx m), f1 j xs = f2 j xs) ->
  (mty! Φ ⊢ subst_mty τ f1 ⊑ subst_mty τ f2).
Proof.
  - intros Hsem. destruct A. cbn. constructor; apply submty_subst_idx_sem; firstorder.
  - intros Hsem. destruct τ; cbn.
    + constructor. hnf in Hsem|-*. intros xs Hxs; specialize (Hsem xs Hxs). rewrite Hsem. reflexivity.
    + constructor.
      * apply sublty_subst_idx_sem; eauto. firstorder.
      * hnf in Hsem|-*. intros xs Hxs; specialize (Hsem xs Hxs). rewrite Hsem. reflexivity.
Qed.

Lemma eqlty_subst_idx_sem (m n : nat) (Φ : constr n) (f1 f2 : idx m -> idx n) (A : lty m) :
  (sem! Φ ⊨ fun xs => forall (j : idx m), f1 j xs = f2 j xs) ->
  (lty! Φ ⊢ subst_lty A f1 ≡ subst_lty A f2).
Proof. intros H. split; apply sublty_subst_idx_sem; eauto. firstorder. Qed.

Lemma eqmty_subst_idx_sem (m n : nat) (Φ : constr n) (f1 f2 : idx m -> idx n) (τ : mty m) :
  (sem! Φ ⊨ fun xs => forall (j : idx m), f1 j xs = f2 j xs) ->
  (mty! Φ ⊢ subst_mty τ f1 ≡ subst_mty τ f2).
Proof. intros H. split; apply submty_subst_idx_sem; eauto. firstorder. Qed.


(** Casting lemmas *)

Lemma subst_lty_cast m m' n (A : lty m') (f : idx m -> idx n) (H : m = m') :
  subst_lty (lty_cast A H) f = subst_lty A (fun j : idx m' => f (idx_cast j H))
with subst_mty_cast m m' n (τ : mty m') (f : idx m -> idx n) (H : m = m') :
  subst_mty (mty_cast τ H) f = subst_mty τ (fun j : idx m' => f (idx_cast j H)).
Proof.
  - subst. rewrite lty_cast_id. f_equal. fext; intros. now rewrite idx_cast_id.
  - subst. rewrite mty_cast_id. f_equal. fext; intros. now rewrite idx_cast_id.
Qed.

Lemma subst_lty_cast' m n n' (A : lty m) (f : idx m -> idx n) (H : n' = n) :
  lty_cast (subst_lty A f) H = subst_lty A (fun j : idx m => idx_cast (f j) H)
with subst_mty_cast' m n n' (τ : mty m) (f : idx m -> idx n) (H : n' = n) :
  mty_cast (subst_mty τ f) H = subst_mty τ (fun j : idx m => idx_cast (f j) H).
Proof.
  - subst. rewrite lty_cast_id. f_equal. fext; intros. now rewrite idx_cast_id.
  - subst. rewrite mty_cast_id. f_equal. fext; intros. now rewrite idx_cast_id.
Qed.


(** General beta substitution is defined in the module [dlPCF_iSubst]. This module proves a general subtyping
substitution lemma for all kinds of beta substitution. *)


(** *** Stripping and Ex-falso subtyping *)



(** Compute the PCF type by removing all annotations *)
Fixpoint lty_strip {ϕ} (A : lty ϕ) : PCF_Types.ty :=
  match A with
  | Arr τ1 τ2 => PCF_Types.Arr (mty_strip τ1) (mty_strip τ2)
  end
with mty_strip {ϕ} (τ : mty ϕ) : PCF_Types.ty :=
  match τ with
  | Nat _ => PCF_Types.Nat
  | Quant _ A => lty_strip A
  end.


Fixpoint subst_lty_strip {m n} (A : lty m) (f : idx m -> idx n) { struct A } :
  lty_strip (subst_lty A f) = lty_strip A
with subst_mty_strip {m n} (τ : mty m) (f : idx m -> idx n) { struct τ } :
  mty_strip (subst_mty τ f) = mty_strip τ.
Proof.
  - destruct A; cbn; f_equal; apply subst_mty_strip.
  - destruct τ; cbn.
    + reflexivity.
    + apply subst_lty_strip.
Qed.


Fixpoint sublty_strip {ϕ} (Φ : constr ϕ) (A B : lty ϕ)
  (H: lty! Φ ⊢ A ⊑ B) { struct H } :
  lty_strip A = lty_strip B
with submty_strip {ϕ} (Φ : constr ϕ) (σ τ : mty ϕ)
  (H: mty! Φ ⊢ σ ⊑ τ) { struct H } :
  mty_strip σ = mty_strip τ.
Proof.
  - destruct H. cbn. f_equal.
    + symmetry. eapply submty_strip; eauto.
    + eapply submty_strip; eauto.
  - destruct H; cbn.
    + reflexivity.
    + eapply sublty_strip; eauto.
Qed.


Lemma eqlty_strip {ϕ} (Φ : constr ϕ) (A B : lty ϕ) :
  lty! Φ ⊢ A ≡ B ->
  lty_strip A = lty_strip B.
Proof. intros [H _]. eauto using sublty_strip. Qed.

Lemma eqmty_strip {ϕ} (Φ : constr ϕ) (σ τ : mty ϕ) :
  mty! Φ ⊢ σ ≡ τ ->
  mty_strip σ = mty_strip τ.
Proof. intros [H _]. eauto using submty_strip. Qed.

Lemma eqlty_strip_commutes {ϕ} (Φ : constr ϕ) (A1 A2 B1 B2 : lty ϕ) :
  lty_strip A1 = lty_strip B1 ->
  (lty! Φ ⊢ A1 ≡ A2) ->
  (lty! Φ ⊢ B1 ≡ B2) ->
  lty_strip A2 = lty_strip B2.
Proof.
  intros H1 H2 H3.
  erewrite <- eqlty_strip by eauto.
  rewrite H1. now erewrite eqlty_strip by eauto.
Qed.
Lemma eqlty_strip_commutes' {ϕ} (Φ1 Φ2 : constr ϕ) (A1 A2 B1 B2 : lty ϕ) :
  (lty! Φ1 ⊢ A1 ≡ A2) ->
  (lty! Φ2 ⊢ B1 ≡ B2) ->
  lty_strip A1 = lty_strip B1 ->
  lty_strip A2 = lty_strip B2.
Proof.
  intros H1 H2 H3.
  erewrite <- eqlty_strip by eauto.
  rewrite H3. now erewrite eqlty_strip by eauto.
Qed.



Lemma sublty_submty_exfalso' :
  forall (i : nat) ϕ (Φ : constr ϕ),
    (forall (A1 A2 : lty ϕ),
        size_lty A1 + size_lty A2 <= i ->
        (sem! Φ ⊨ fun _ => False) -> lty_strip A1 = lty_strip A2 -> lty! Φ ⊢ A1 ⊑ A2) /\
    (forall (τ1 τ2 : mty ϕ),
        size_mty τ1 + size_mty τ2 <= i ->
        (sem! Φ ⊨ fun _ => False) -> mty_strip τ1 = mty_strip τ2 -> mty! Φ ⊢ τ1 ⊑ τ2).
Proof.
  induction i as [i IHi] using lt_wf_ind; intros. split.
  - intros A1 A2 Hi Hsem Hstrip.
    destruct A1, A2; cbn in *; injection Hstrip as Hstrip1 Hstrip2.
    constructor.
    + eapply IHi; eauto. lia.
    + eapply IHi; eauto. lia.
  - intros τ1 τ2 Hi Hsem Hstrip. destruct τ1 as [h|h A], τ2 as [j|j B]; cbn in *.
    + hnf in Hsem. constructor. hnf. firstorder.
    + destruct B; cbn; intros; exfalso; cbn in *; congruence.
    + destruct A; cbn; intros; exfalso; cbn in *; congruence.
    + eapply submty_Quant.
      * eapply IHi; eauto. cbn. lia. cbn. firstorder.
      * firstorder.
Qed.

Lemma sublty_exfalso {ϕ} (Φ : constr ϕ) A1 A2 :
  (sem! Φ ⊨ fun _ => False) ->
  lty_strip A1 = lty_strip A2 ->
  lty! Φ ⊢ A1 ⊑ A2.
Proof. eapply sublty_submty_exfalso'; eauto. Qed.

Lemma submty_exfalso {ϕ} (Φ : constr ϕ) τ1 τ2 :
  (sem! Φ ⊨ fun _ => False) ->
  mty_strip τ1 = mty_strip τ2 ->
  mty! Φ ⊢ τ1 ⊑ τ2.
Proof. eapply sublty_submty_exfalso'; eauto. Qed.


Lemma eqlty_exfalso {ϕ} (Φ : constr ϕ) A1 A2 :
  (sem! Φ ⊨ fun _ => False) ->
  lty_strip A1 = lty_strip A2 ->
  lty! Φ ⊢ A1 ≡ A2.
Proof. intros. split; eapply sublty_exfalso; eauto. Qed.

Lemma eqmty_exfalso {ϕ} (Φ : constr ϕ) τ1 τ2 :
  (sem! Φ ⊨ fun _ => False) ->
  mty_strip τ1 = mty_strip τ2 ->
  mty! Φ ⊢ τ1 ≡ τ2.
Proof. intros. split; eapply submty_exfalso; eauto. Qed.
