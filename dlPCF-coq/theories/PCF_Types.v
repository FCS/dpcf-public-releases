Require Import Common.
Require Import PCF_Syntax PCF_Semantics.


Open Scope PCF.


(** ** PCF Type System *)


(** *** PCF Types *)

(** There are only two constructors *)

Inductive ty : Set :=
| Arr (τ1 τ2 : ty) : ty
| Nat : ty.


(** *** UIP for types *)

From Coq Require Import Eqdep_dec.

Lemma ty_eq_dec : forall x y : ty, {x = y} + {x <> y}.
Proof. decide equality. Defined.

Lemma ty_UIP (ρ1 ρ2 : ty) (H1 H2: ρ1 = ρ2) : H1 = H2.
Proof. apply UIP_dec, ty_eq_dec. Qed.

Lemma ty_UIP_refl (ρ : ty) (H : ρ = ρ) : H = eq_refl.
Proof. apply (ty_UIP H eq_refl). Qed.

Lemma ty_eq_rec_eq : forall (τ : ty) (Q : ty -> Set) (x : Q τ) (h : τ = τ), x = eq_rec τ Q x τ h.
Proof. intros. pose proof ty_UIP_refl h as ->. reflexivity. Qed.

Lemma ty_eq_rect_eq : forall (τ : ty) (Q : ty -> Type) (x : Q τ) (h : τ = τ), x = eq_rect τ Q x τ h.
Proof. intros. pose proof ty_UIP_refl h as ->. reflexivity. Qed.



(** *** PCF Typing Rules *)


(** There is no subtyping *)


Notation ctx := (@ctx ty) % PCF.


Implicit Types Γ Δ : ctx.

Inductive hasty (Γ : ctx) : tm -> ty -> Prop :=
| ty_Var x τ :
    Γ x = τ ->
    hasty Γ (Var x) τ
| ty_Lam t τ1 τ2 :
    hasty (τ1 .: Γ) t τ2 ->
    hasty Γ (Lam t) (Arr τ1 τ2)
| ty_Fix t τ :
    hasty (τ .: Γ) (Lam t) τ ->
    hasty Γ (Fix t) τ
| ty_App t1 t2 τ1 τ2 :
    hasty Γ t1 (Arr τ1 τ2) ->
    hasty Γ t2 τ1 ->
    hasty Γ (App t1 t2) τ2
| ty_Ifz t1 t2 t3 τ :
    hasty Γ t1 Nat ->
    hasty Γ t2 τ ->
    hasty Γ t3 τ ->
    hasty Γ (Ifz t1 t2 t3) τ
| ty_Const k :
    hasty Γ (Const k) Nat
| ty_S t :
    hasty Γ t Nat ->
    hasty Γ (Succ t) Nat
| ty_P t :
    hasty Γ t Nat ->
    hasty Γ (Pred t) Nat
.


(** *** Context retyping *)

Lemma retype_free Γ Δ t τ :
  (forall i, free i t -> Γ i = Δ i) ->
  hasty Γ t τ ->
  hasty Δ t τ.
Proof.
  intros Hcomp Hty. induction Hty in Δ,Hcomp|-*; cbn in *.
  - eapply ty_Var. subst. now rewrite Hcomp.
  - eapply ty_Lam; eauto. eapply IHHty; eauto. intros [ | ]; cbn; auto.
  - eapply ty_Fix; eauto. eapply IHHty; eauto. intros [ | ]; cbn; auto.
  - eapply ty_App; eauto.
  - eapply ty_Ifz; eauto.
  - eapply ty_Const; eauto.
  - eapply ty_S; eauto.
  - eapply ty_P; eauto.
Qed.

Lemma retype_bound Γ Δ t m τ :
  bound m t ->
  (forall i, i < m -> Γ i = Δ i) ->
  hasty Γ t τ ->
  hasty Δ t τ.
Proof.
  intros Hbound Hcomp Hty. induction Hty in Δ,Hcomp,Hbound,m|-*; bound_inv.
  - eapply ty_Var. now specialize Hcomp with (1 := Hbound) as <-.
  - eapply ty_Lam; eauto. eapply IHHty; eauto. intros [ | ]; cbn; auto. intros. now rewrite Hcomp by omega.
  - eapply ty_Fix; eauto. eapply IHHty; eauto. intros [ | ]; cbn; auto. intros. now rewrite Hcomp by omega.
  - eapply ty_App; eauto.
  - eapply ty_Ifz; eauto.
  - eapply ty_Const; eauto.
  - eapply ty_S; eauto.
  - eapply ty_P; eauto.
Qed.

Lemma retype_closed Γ Δ t τ :
  closed t ->
  hasty Γ t τ ->
  hasty Δ t τ.
Proof. intros H1 H2. eapply retype_bound. 1,3: eassumption. intros; omega. Qed.




(** *** Substitution Lemma *)

(* [Γ] is like [Γ'] (for all [m] free variables except [x]), but it has [x : σ] *)
Definition ctxExtends (Γ : ctx) (m : nat) (x : nat) (σ : ty) (Γ' : ctx) : Prop :=
  (forall y, y < m -> x <> y -> Γ' y = Γ y) /\ Γ x = σ.

Lemma typepres_nsubst (m x : nat) (Γ Γ' Δ : ctx) t v σ τ :
  hasty Γ t τ ->
  bound m t ->
  closed v ->
  hasty Δ v σ ->
  ctxExtends Γ m x σ Γ' ->
  hasty Γ' (nsubst t x v) τ.
Proof.
  intros Hty1 Hbound Hclos Hty2 Hext.
  induction Hty1 in x,m,Γ',Δ,σ,v,Hbound,Hclos,Hty2,Hext|-*; bound_inv; cbn in *.
  - (* Var *) destruct Nat.eq_dec as [->|d].
    + subst. rewrite (proj2 Hext). eapply retype_closed; eauto.
    + eapply ty_Var. rewrite <- H. apply (proj1 Hext); auto.
  - (* Lam *) eapply ty_Lam. eapply IHHty1; eauto.
    split; cbn; eauto.
    + intros [ | y]; cbn; auto.
      intros. apply (proj1 Hext); auto with arith.
    + firstorder.
  - (* Fix *) eapply ty_Fix. eapply IHHty1; eauto.
    split; cbn; eauto.
    + intros [ | y]; cbn; auto.
      intros. apply (proj1 Hext); auto with arith.
    + firstorder.
  - cbn. eapply ty_App; eauto.
  - cbn. eapply ty_Ifz; eauto.
  - cbn. eapply ty_Const; eauto.
  - cbn. eapply ty_S; eauto.
  - cbn. eapply ty_P; eauto.
Qed.

Lemma typepres_nbeta1 (Γ : ctx) (τ1 τ2 : ty) (t : tm) (v : tm) :
  hasty (τ2 .: Γ) t τ1 ->
  hasty Γ v τ2 ->
  bound 1 t -> closed v ->
  hasty Γ (nbeta1 t v) τ1.
Proof. intros. eapply typepres_nsubst; eauto. firstorder lia. Qed.
  
Lemma typepres_nbeta2 (Γ : ctx) (τ1 τ2 τ3 : ty) (t : tm) (v1 v2 : tm) :
  hasty (τ3 .: (τ2 .: Γ)) t τ1 ->
  hasty Γ v1 τ2 ->
  hasty Γ v2 τ3 ->
  bound 2 t -> closed v1 -> closed v2 ->
  hasty Γ (nbeta2 t v2 v1) τ1.
Proof.
  intros.
  eapply typepres_nbeta1; eauto.
  - eapply typepres_nsubst; eauto.
    hnf. cbn. split; auto. intros [ | y ]; intros; cbn; auto. lia.
  - apply nsubst_bound'; eauto.
Qed.


(** *** Subject Reduction *)

Theorem preservation (Γ : ctx) (t t' : tm) τ :
  hasty Γ t τ ->
  t ≻ t' ->
  closed t ->
  hasty Γ t' τ.
Proof.
  intros Hty (κ&Hstep) Hclos. induction Hty in κ,t',Hstep,Hclos|-*; closed_inv.
  all: try solve [ now stuck
                 | step_inv; eauto using hasty ].
  { (* App case *)
    step_inv.
    - eapply ty_App; eauto.
    - eapply ty_App; eauto.
    - inv Hty1. eapply typepres_nbeta1; eauto.
    - inv Hty1. inv H0. eapply typepres_nbeta2; eauto. eapply ty_Fix, ty_Lam; eauto.
  }
Qed.


(** *** Progress *)

Lemma val_arr_form(Γ : ctx) t τ1 τ2 :
  val t ->
  hasty Γ t (Arr τ1 τ2) ->
  (exists t', t = Lam t') \/ (exists t', t = Fix t').
Proof. intros Hval % valb_iff Htyp. inv Htyp; cbn in *; eauto; congruence. Qed.

Lemma val_nat_form(Γ : ctx) t :
  val t ->
  hasty Γ t Nat ->
  exists k, t = Const k.
Proof.
  intros Hval % valb_iff Htyp. inv Htyp; cbn in *; eauto; try congruence.
  (* Case Lam *) now inv H.
Qed.


Local Ltac solve_progress :=
  repeat
    match goal with
    | [ |- progressive ?t ] => now (right; isVal)
    | [ |- val ?t         ] => isVal
    | [ H : closed _ -> progressive _ |- _ ] => destruct H; auto
    | [ H : progressive ?t |- _ ] => destruct H
    | [ H1 : (val ?t), H2 : (hasty ?Γ ?t (Arr ?τ1 ?τ2)) |- _ ] =>
      pose proof val_arr_form H1 H2 as [ (?&->) | (?&->) ]; clear H2
    | [ H1 : val ?t, H2 : hasty ?Γ ?t Nat |- _ ] =>
      pose proof val_nat_form H1 H2 as (?&->); cbn in *; clear H2
    | [ |- progressive ?l ] => now (eleft; econstructor; eauto; solve_progress)
    end.

Local Arguments bound : simpl never.

Theorem progress (Γ : ctx) t τ :
  closed t ->
  hasty Γ t τ ->
  progressive t.
Proof.
  intros Hclos Htyp. induction Htyp; subst; cbn in *; closed_inv.
  all: solve_progress.
  all: solve [destruct x; cbn;
              [now eleft; econstructor; eauto
              |eleft; eapply step_ifz3; omega]].
Qed.

Theorem safety (Γ : ctx) t t' τ :
  closed t ->
  hasty Γ t τ ->
  t ≻* t' ->
  progressive t'.
Proof.
  intros Hground Htyp Hsteps. induction Hsteps in Hground,Htyp.
  - eapply progress; eauto.
  - destruct H as [κ H]. apply IHHsteps; eauto.
    + eapply preservation_closed; hnf; eauto.
    + eapply preservation; hnf; eauto.
Qed.
