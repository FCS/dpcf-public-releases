Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax PCF_Semantics.
Require Import PCF_TypesT.
Require Import dlPCF_iSubst.
Require Import ForestCard dlPCF_Types dlPCF_Sum dlPCF_Contexts dlPCF_Typing.
Require Import dlPCF_TypingT.
Require Import dlPCF_MakeSum.
Require Import dlPCF_Joining dlPCF_pJoining.
Require PCF_Types.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.
Import SigmaTypeNotations.
Open Scope vector_scope.

From Coq Require Import Vector Fin.

Open Scope PCF.

Implicit Types (ϕ : nat).

Require Import dlPCF_Safety.

Import Skel.
Import EqNotations.




Definition PCF_ctxExtend (Γ : PCF.ctx) (x : nat) (σ : PCF.ty) : PCF.ctx :=
  fun y => if x =? y then σ else Γ y.

Lemma PCF_ctxExtend_scons (Γ : PCF.ctx) (x : nat) (σ : PCF.ty) (τ : PCF.ty) :
  (PCF_ctxExtend (τ .: Γ) (S x) σ) = (τ .: (PCF_ctxExtend Γ x σ)).
Proof. fext; intros [ | ?]; reflexivity. Qed.


Definition ctxExtend {ϕ} (Γ : ctx ϕ) (x : nat) (σ : mty ϕ) : ctx ϕ :=
  fun y => if x =? y then σ else Γ y.

Lemma ctxExtend_scons {ϕ} (Γ : ctx ϕ) (x : nat) (σ : mty ϕ) (τ : mty ϕ) :
  (ctxExtend (τ .: Γ) (S x) σ) = (τ .: (ctxExtend Γ x σ)).
Proof. fext; intros [ | ?]; reflexivity. Qed.

Lemma ctxExtend_correct {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (x : nat) (σ : mty ϕ) m :
  ctxExtends Φ (ctxExtend Γ x σ) m x σ Γ.
Proof.
  hnf. split.
  - intros. unfold ctxExtend. rewrite (proj2 (Nat.eqb_neq _ _)) by assumption. reflexivity.
  - unfold ctxExtend. rewrite (proj2 (Nat.eqb_eq _ _)) by reflexivity. reflexivity.
Qed.



Lemma nsubst_eq_Var_inv (x y : nat) (t v : tm) :
  Var y = nsubst t x v ->
  (t = Var x /\ v = Var y) \/
  (t = Var y /\ x <> y).
Proof.
  destruct t; cbn; try discriminate.
  destruct (Nat.eq_dec x n).
  - intros. subst. intros; subst. eauto.
  - intros [= ->]. right. eauto.
Qed.

Lemma nsubst_eq_Lam_inv (x : nat) (t t' v : tm) :
  Lam t = nsubst t' x v ->
  (t' = Var x /\ v = Lam t) \/ (* The boring case *)
  (exists t'', t' = Lam t'' /\ t = nsubst t'' (S x) v).
Proof.
  destruct t' eqn:E; cbn; try discriminate.
  destruct (Nat.eq_dec x n).
  - intros. subst. intros; subst. eauto.
  - intros [= ->].
  - intros [= ->]. right. eauto.
Qed.



Lemma nsubst_free (t v : tm) (x y : nat) :
  free y t ->
  x <> y ->
  free y (nsubst t x v).
Proof.
  intros Hfree Hxy. induction t in x,y,Hfree,Hxy|-*; cbn in *.
  - subst. destruct Nat.eq_dec as [->|Hd]; try tauto. reflexivity.
  - apply IHt; eauto.
  - apply IHt; eauto.
  - destruct Hfree.
    + left. apply IHt1; eauto.
    + right. apply IHt2; eauto.
  - destruct Hfree as [Hfree | [Hfree | Hfree]].
    + left. apply IHt1; eauto.
    + right. left. apply IHt2; eauto.
    + right. right. apply IHt3; eauto.
  - assumption.
  - apply IHt; eauto.
  - apply IHt; eauto.
Qed.


Lemma converse_subst {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (K : idx ϕ) (t : tm) (x : nat) (v : tm)
      (ρ : mty ϕ) (σ_pcf τ_pcf : PCF.ty)
      (pcfTy1 : PCF.hastyT (PCF_ctxExtend (stripCtx Γ) x σ_pcf) t τ_pcf)
      (pcfTy2 : PCF.hastyT (stripCtx Γ) v σ_pcf) :
  let s1 := PCF.strip pcfTy1 in
  let s2 := PCF.strip pcfTy2 in
  τ_pcf = mty_strip ρ ->
  (* (forall xs, { Φ xs } + { ~ Φ xs }) -> *)
  (Ty! Φ; Γ ⊢(K) nsubst t x v : ρ @ (PCF.skel_subst x t s1 s2)) ->
  val v -> closed v ->
  existsS (M N : idx ϕ) σ,
    (Ty! Φ; ctxExtend Γ x σ ⊢(M) t : ρ @ s1) **
    (Ty! Φ; Γ ⊢(N) v : σ @ s2) **
    (sem! Φ ⊨ fun xs => M xs + N xs = K xs) **
    mty_strip σ = σ_pcf.
Proof.
  intros s1 s2 (* dec *) -> Hty Hval Hclos.
  revert Hty. revert s1 s2. revert σ_pcf pcfTy1 pcfTy2.
  revert x ϕ Φ Γ K v ρ Hval Hclos. (* revert s1 s2. *)
  induction t as [t IHt] using (@induction_ltof1_Type _ (@size)); unfold ltof in *; intros.
  destruct t; cbn [nsubst size] in *.
  - (* Var *) clear IHt.
    pose proof PCF.tyT_Var_inv' pcfTy1 as (H_strip & ->). cbn in *.
    unfold ctxExtend; cbn.
    destruct (Nat.eq_dec x n) as [->|Hdec].
    { (* [x = n] *)
      eexists _, _, _; repeat_split.
      * eapply tyT_Var. rewrite Nat.eqb_refl.
        -- reflexivity.
        -- hnf; reflexivity.
      * apply Hty.
      * cbn. hnf; reflexivity.
      * unfold PCF_ctxExtend, stripCtx in H_strip. rewrite Nat.eqb_refl in H_strip. auto.
    }
    { (* [x <> n]: [x] is not in the term, so we make a bogus typing for [v]. *)
      pose proof tyT_triv_sig Φ pcfTy2 Hval Hclos as (σ & Hv & <-).
      eexists _, _, _; repeat_split.
      * eapply tyT_weaken; eauto.
        hnf; intros var Hvar. cbn in Hvar. subst. rewrite (proj2 (Nat.eqb_neq _ _)) by auto. reflexivity.
      * eauto.
      * hnf; auto.
      * reflexivity.
    }

  - (* Lam *)
    pose proof PCF.tyT_Lam_inv' pcfTy1 as (τ1_pcf & τ2_pcf & ty'_pcf & Hstrip & ->).
    destruct ρ as [ | J B]; cbn in Hstrip; try discriminate.
    destruct B as [ρ1 ρ2]; cbn in Hstrip. injection Hstrip as -> ->. pose proof PCF.ty_UIP_refl Hstrip as ->.

    specialize (IHt t); spec_assert IHt by (cbn; lia).
    specialize IHt with (1 := Hval) (2 := Hclos) (x := S x) (ϕ := S ϕ).
    inv Hty.

    pose (Γ'' := σ .: Δ).
    specialize IHt with (Φ := fun xs : Vector.t nat (S ϕ) => hd xs < I (tl xs) /\ Φ (tl xs)) (Γ := Γ'') (K := K0).
    specialize IHt with (σ_pcf := σ_pcf) (ρ := ρ2).

    assert (mty_strip ρ1 = mty_strip σ /\ mty_strip ρ2 = mty_strip τ) as (Hσ_split & Hτ_split).
    {
      apply eqmty_Quant_inv in Hρ as [Hρ Hρ']. apply eqlty_Arr_inv in Hρ as [Hρ1 Hρ2].
      apply eqmty_strip in Hρ1; apply eqmty_strip in Hρ2. split; congruence.
    }

    epose proof eq_refl ?[ty'_pcf'] as H_refl.
    unshelve specialize IHt with (pcfTy1 := ?ty'_pcf').
    {
      eapply PCF.retypeT_free.
      - apply ty'_pcf.
      - unfold Γ''. intros y Hy. rewrite stripCtx_scons, PCF_ctxExtend_scons.
        destruct y as [ | y]; cbn.
        + assumption.
        + unfold PCF_ctxExtend. destruct Nat.eqb eqn:E.
          * reflexivity.
          * apply Nat.eqb_neq in E. unfold stripCtx.
            hnf in Hbsum; specialize (Hbsum y); spec_assert Hbsum.
            { cbn. eapply nsubst_free; eauto. }
            hnf in HΓ; specialize (HΓ y); spec_assert HΓ.
            { cbn. eapply nsubst_free; eauto. }
            apply bsum_strip in Hbsum. apply eqmty_strip in HΓ. congruence.
    }
    clear H_refl. rewrite PCF.retypeT_free_skel in IHt.

    epose proof eq_refl ?[pcfTy2] as H_refl.
    unshelve specialize IHt with (pcfTy2 := ?pcfTy2).
    { eapply PCF.retypeT_closed. assumption. apply pcfTy2. }
    clear H_refl. rewrite PCF.retypeT_closed_skel in IHt.

    spec_assert IHt.
    {
      eapply tyT_sub'.
      - apply Hty0.
      - apply eqmty_Quant_inv in Hρ as [Hρ Hρ']. apply eqlty_Arr_inv in Hρ as [Hρ1 Hρ2].
        eapply eqmty_mono_Φ; eauto.
        hnf; intros xs (Hxs1&Hxs2). split; auto.
        eapply Nat.lt_le_trans; eauto. now rewrite Hρ'.
      - hnf; reflexivity.
    }
    destruct IHt as (M_IH & N_IH & σ_IH & IH1 & IH2 & IH3 & IH4).

    (* Parametric joining *)
    pose proof pjoining _ _ IH2 Hval Hclos as (σ_join & σc' & Hjoin1 & Hjoin2 & Hjoin3).

    eexists _, _, _; repeat_split.
    2:{ eapply retypeT_closed; eauto. }
    3:{  rewrite <- IH4.
         apply bsum_strip in Hjoin2. rewrite <- Hjoin2.
         eapply eqmty_strip_commutes. reflexivity. all: symmetry; eauto. reflexivity. }
    1:{
      cbn.
      eapply tyT_Lam with
          (σ0 := σ)
          (Δ0 := ctxExtend Δ x σc')
          (Γ'0 := ctxExtend Γ' x σ_join).
      - eapply tyT_sub.
        + apply IH1.
        + unfold Γ''. rewrite ctxExtend_scons. apply eqCtx_scons.
          * reflexivity.
          * intros y Hy. unfold ctxExtend. destruct Nat.eqb.
            -- eauto.
            -- reflexivity.
        + apply eqmty_Quant_inv in Hρ as [Hρ Hρ']. apply eqlty_Arr_inv in Hρ as [Hρ1 Hρ2].
          symmetry. eapply eqmty_mono_Φ. apply Hρ2.
          hnf; intros xs (Hxs1&Hxs2); split; auto. eapply Nat.lt_le_trans; eauto. now rewrite <- Hρ'.
        + hnf; reflexivity.

      - hnf; intros y Hy. unfold ctxExtend.
        destruct Nat.eqb eqn:E.
        + apply Hjoin2.
        + apply Nat.eqb_neq in E. cbn. hnf in Hbsum. apply Hbsum. apply nsubst_free; auto.

      - hnf; intros y Hy. unfold ctxExtend. destruct Nat.eqb eqn:E.
        + reflexivity.
        + apply Nat.eqb_neq in E. hnf in HΓ. apply HΓ. apply nsubst_free; auto.

      - hnf; reflexivity.
      - eauto.
    }
    1:{ (* Lastly... the cost (easy) *)
      cbn; hnf; intros xs Hxs.
      rewrite <- HM by auto.
      setoid_rewrite Sum_ext at 3.
      2:{ intros a Ha. rewrite <- IH3 by auto. reflexivity. }
      cbn. rewrite Sum_plus. symmetry; apply Nat.add_assoc.
    }

  - (* Fix *)
    pose proof PCF.tyT_Fix_inv' pcfTy1 as (pcfTy1' & ->).
    specialize (IHt (Lam t)); spec_assert IHt by (cbn; lia).
    specialize IHt with (1 := Hval) (2 := Hclos) (x := S x) (ϕ := S ϕ).

    inv Hty; cbn zeta in *.
    pose (Γ'' := Quant Ib A .: Δ).

    specialize IHt with (Φ := fun xs : Vector.t nat (S ϕ) => hd xs < cardH (tl xs) /\ Φ (tl xs)) (Γ := Γ'') (K := Jb).
    specialize IHt with (σ_pcf := σ_pcf) (ρ := Quant (iConst 1) B).

    assert (mty_strip ρ = lty_strip B /\ lty_strip B = lty_strip A) as (HB_strip & HA_strip).
    { split.
      - apply eqmty_strip in Hρ. cbn in Hρ. setoid_rewrite subst_lty_strip in Hρ. auto.
      - apply eqlty_strip in Hsub. setoid_rewrite subst_lty_strip in Hsub. auto. }

    epose proof eq_refl ?[ty'_pcf'] as H_refl.
    unshelve specialize IHt with (pcfTy1 := ?ty'_pcf').
    {
      eapply PCF.retypeT_free.
      - cbn. eapply PCF_tyT_congr; eauto.
      - unfold Γ''. intros y Hy. rewrite stripCtx_scons, PCF_ctxExtend_scons.
        destruct y as [ | y]; cbn.
        + congruence.
        + unfold PCF_ctxExtend. destruct Nat.eqb eqn:E.
          * reflexivity.
          * apply Nat.eqb_neq in E. unfold stripCtx.
            hnf in Hbsum; specialize (Hbsum y); spec_assert Hbsum.
            { cbn. eapply nsubst_free; eauto. }
            hnf in HΓ; specialize (HΓ y); spec_assert HΓ.
            { cbn. eapply nsubst_free; eauto. }
            apply bsum_strip in Hbsum. apply eqmty_strip in HΓ. congruence.
    }
    clear H_refl. rewrite PCF.retypeT_free_skel, PCF_tyT_congr_skel in IHt.

    epose proof eq_refl ?[pcfTy2] as H_refl.
    unshelve specialize IHt with (pcfTy2 := ?pcfTy2).
    1:{ eapply PCF.retypeT_closed. assumption. apply pcfTy2. }
    clear H_refl. rewrite PCF.retypeT_closed_skel in IHt.

    specialize IHt with (1 := Hty0) as (M_IH & N_IH & σ_IH & IH1 & IH2 & IH3 & IH4).


    (* Parametric joining *)
    pose proof pjoining _ _ IH2 Hval Hclos as (σ_join & σc' & Hjoin1 & Hjoin2 & Hjoin3).

    eexists _, _, _; repeat_split.
    2:{ eapply retypeT_closed; eauto. }
    3:{ rewrite <- IH4.
        apply bsum_strip in Hjoin2. rewrite <- Hjoin2.
        eapply eqmty_strip_commutes. reflexivity. all: symmetry; eauto. reflexivity. }
    1:{
      cbn.
      eapply tyT_Fix with
          (* The same parameters *)
          (A0 := A) (B0 := B) (cardH0 := cardH) (card3 := card1) (card4 := card2) (Ib0 := Ib) (K1 := K0)
          (Δ0 := ctxExtend Δ x σc')
          (Γ'0 := ctxExtend Γ' x σ_join); cbn zeta.
      - cbn in Hty0. eapply tyT_weaken. apply IH1.
        unfold Γ''. rewrite ctxExtend_scons. apply eqCtx_scons.
        + reflexivity.
        + intros y Hy. unfold ctxExtend. destruct Nat.eqb eqn:E.
          * auto.
          * reflexivity.
      - eauto.
      - eauto.
      - eauto.
      - eauto.
      - hnf; intros y Hy. unfold ctxExtend. destruct Nat.eqb eqn:E.
        + eauto.
        + apply Nat.eqb_neq in E. apply Hbsum. eapply nsubst_free; eauto.
      - hnf; intros y Hy. unfold ctxExtend. destruct Nat.eqb eqn:E.
        + reflexivity.
        + apply Nat.eqb_neq in E. apply HΓ. eapply nsubst_free; eauto.
      - hnf; reflexivity.
      - eauto.
    }
    { (* The cost... *)
      cbn; hnf; intros xs Hxs.
      rewrite <- HM by auto.
      setoid_rewrite Sum_ext at 3.
      2:{ intros a Ha. rewrite <- IH3 by auto. reflexivity. }
      cbn. rewrite Sum_plus. lia.
    }

  - (* App *)
    pose proof PCF.tyT_App_inv' pcfTy1 as (σ_pcf' & pcfTy11 & pcfTy12 & ->).
    inv Hty.
    specialize (IHt t1) with (2 := Hval) (3 := Hclos) as IHt1; spec_assert IHt1 by (cbn; lia).
    specialize (IHt t2) with (2 := Hval) (3 := Hclos) as IHt2; spec_assert IHt2 by (cbn; lia).
    clear IHt.
    specialize IHt1 with (x := x) (Φ := Φ) (Γ := Δ1) (ρ := Quant (iConst 1) (σ ⊸ τ)).

    epose proof eq_refl ?[pcfTy11] as H_refl.
    unshelve specialize IHt1 with (pcfTy1 := ?pcfTy11).
    2:{
      cbn. replace (mty_strip τ) with (mty_strip ρ).
      2:{ apply eqmty_strip in Hρ. setoid_rewrite subst_mty_strip in Hρ. auto. }
      eapply PCF.retypeT_free.
      - apply pcfTy11.
      - intros y Hy. unfold PCF_ctxExtend. destruct Nat.eqb eqn:E.
        + reflexivity.
        + apply Nat.eqb_neq in E. unfold stripCtx.
          hnf in Hmsum. specialize (Hmsum y); spec_assert Hmsum.
          { eapply nsubst_free with (t := App t1 t2); cbn; eauto. }
          apply msum_strip in Hmsum as [Hmsum1 Hmsum2].
          hnf in HΓ. specialize (HΓ y); spec_assert HΓ.
          { eapply nsubst_free with (t := App t1 t2); cbn; eauto. }
          apply eqmty_strip in HΓ. congruence.
    }
    clear H_refl; cbn in IHt1; destruct (subrelation_proper); cbn in *; rewrite PCF.retypeT_free_skel in *.

    specialize IHt1 with (K := K1).

    epose proof eq_refl ?[pcfTy2] as H_refl.
    unshelve specialize IHt1 with (pcfTy2 := ?pcfTy2).
    { eapply PCF.retypeT_closed. assumption. apply pcfTy2. }
    clear H_refl. rewrite PCF.retypeT_closed_skel in IHt1.
    specialize IHt1 with (1 := Hty1) as (M_IH1 & N_IH1 & σ_IH1 & IH11 & IH12 & IH13 & IH14).

    (* The same for [IHt2]... *)

    specialize IHt2 with (x := x) (Φ := Φ) (Γ := Δ2) (ρ := subst_mty_beta_ground σ (iConst 0)).
    epose proof eq_refl ?[pcfTy11] as H_refl.
    unshelve specialize IHt2 with (pcfTy1 := ?pcfTy11).
    2:{
      unfold subst_mty_beta_ground; rewrite subst_mty_strip.
      eapply PCF.retypeT_free.
      - apply pcfTy12.
      - intros y Hy. unfold PCF_ctxExtend. destruct Nat.eqb eqn:E.
        + reflexivity.
        + apply Nat.eqb_neq in E. unfold stripCtx.
          hnf in Hmsum. specialize (Hmsum y); spec_assert Hmsum.
          { eapply nsubst_free with (t := App t1 t2); cbn; eauto. }
          apply msum_strip in Hmsum as [Hmsum1 Hmsum2].
          hnf in HΓ. specialize (HΓ y); spec_assert HΓ.
          { eapply nsubst_free with (t := App t1 t2); cbn; eauto. }
          apply eqmty_strip in HΓ. congruence.
    }
    clear H_refl.
    cbn in IHt2; destruct (subst_mty_strip σ (subst_beta_ground_fun (iConst 0))); cbn in *; rewrite PCF.retypeT_free_skel in IHt2.

    specialize IHt2 with (K := K2).
    epose proof eq_refl ?[pcfTy2] as H_refl.
    unshelve specialize IHt2 with (pcfTy2 := ?pcfTy2).
    { eapply PCF.retypeT_closed. assumption. apply pcfTy2. }

    clear H_refl. rewrite PCF.retypeT_closed_skel in IHt2. cbn in IHt2.
    unshelve specialize IHt2 with (1 := Hty2) as (M_IH2 & N_IH2 & σ_IH2 & IH21 & IH22 & IH23 & IH24).


    (* Now we join the two typings *)
    pose proof joining IH12 IH22 Hval as Hjoin.
    spec_assert Hjoin by congruence.
    spec_assert Hjoin.
    { hnf; intros y Hy. exfalso. eapply (proj1 (free_closed_iff _) _); eauto. }
    destruct Hjoin as (σ_join & σ1'_join & σ2'_join & Γ1'_join & Γ2'_join & Γ12_join & Hjoin1 & Hjoin2 & Hjoin3 & Hjoin4 & Hjoin5 & Hjoin6 & Hjoin7).

    eexists _, _, σ_join; repeat_split.
    2:{ eapply retypeT_closed; eauto. }
    3:{ apply msum_strip in Hjoin3 as [Hjoin3 Hjoin3']. apply eqmty_strip in Hjoin1. congruence. }
    1:{
      eapply tyT_skel_congr.
      2:{
        replace (skel_App (mty_strip (subst_mty σ (subst_beta_ground_fun (iConst 0)))) (PCF.strip pcfTy11) (PCF.strip pcfTy12))
          with (skel_App (mty_strip σ) (PCF.strip pcfTy11) (PCF.strip pcfTy12)).
        2:{ f_equal. now rewrite subst_mty_strip. }
        reflexivity.
      }
      eapply tyT_App with
          (Δ3 := ctxExtend Δ1 x σ1'_join) (Δ4 := ctxExtend Δ2 x σ2'_join)
          (Γ'0 := ctxExtend Γ' x σ_join).
      - eapply tyT_weaken.
        + apply IH11.
        + hnf; intros y Hy. unfold ctxExtend. destruct Nat.eqb eqn:E.
          * assumption.
          * reflexivity.
      - eapply tyT_weaken.
        + apply IH21.
        + hnf; intros y Hy. unfold ctxExtend. destruct Nat.eqb eqn:E.
          * assumption.
          * reflexivity.
      - hnf in Hmsum|-*. intros y Hy.
        unfold ctxExtend. destruct Nat.eqb eqn:E.
        + eauto.
        + apply Nat.eqb_neq in E. apply Hmsum. eapply nsubst_free with (t := App t1 t2); cbn; eauto.
      - hnf; intros y Hy. unfold ctxExtend. destruct Nat.eqb eqn:E.
        + reflexivity.
        + apply Nat.eqb_neq in E. apply HΓ. eapply nsubst_free with (t := App t1 t2); cbn; eauto.
      - hnf; reflexivity.
      - eauto.
    }
    { (* Finally, the cost... *)
      cbn; hnf; intros xs Hxs. rewrite <- HM by auto. rewrite <- IH13, <- IH23 by auto. lia.
    }

  - (* Ifz *)
    pose proof PCF.tyT_Ifz_inv' pcfTy1 as (pcfTy11 & pcfTy12 & pcfTy13 & ->).
    inv Hty.

    specialize (IHt t1) with (2 := Hval) (3 := Hclos) as IHt1; spec_assert IHt1 by (cbn; lia).
    specialize (IHt t2) with (2 := Hval) (3 := Hclos) as IHt2; spec_assert IHt2 by (cbn; lia).
    specialize (IHt t3) with (2 := Hval) (3 := Hclos) as IHt3; spec_assert IHt3 by (cbn; lia).
    clear IHt.

    specialize IHt1 with (x := x) (Φ := Φ) (Γ := Δ1) (ρ := Nat J).
    epose proof eq_refl ?[pcfTy11] as H_refl.
    unshelve specialize IHt1 with (pcfTy1 := ?pcfTy11).
    2:{
      cbn. eapply PCF.retypeT_free.
      - apply pcfTy11.
      - intros y Hy. unfold PCF_ctxExtend. destruct Nat.eqb eqn:E.
        + reflexivity.
        + apply Nat.eqb_neq in E. unfold stripCtx.
          hnf in Hmsum. specialize (Hmsum y); spec_assert Hmsum.
          { eapply nsubst_free with (t := Ifz t1 t2 t3); cbn; eauto. }
          apply msum_strip in Hmsum as [Hmsum1 Hmsum2].
          hnf in HΓ. specialize (HΓ y); spec_assert HΓ.
          { eapply nsubst_free with (t := Ifz t1 t2 t3); cbn; eauto. }
          apply eqmty_strip in HΓ. congruence.
    }
    clear H_refl; cbn in IHt1. rewrite PCF.retypeT_free_skel in *.

    specialize IHt1 with (K := M1).

    epose proof eq_refl ?[pcfTy2] as H_refl.
    unshelve specialize IHt1 with (pcfTy2 := ?pcfTy2).
    { eapply PCF.retypeT_closed. assumption. apply pcfTy2. }
    clear H_refl. rewrite PCF.retypeT_closed_skel in IHt1.
    specialize IHt1 with (1 := Hty1) as (M_IH1 & N_IH1 & σ_IH1 & IH11 & IH12 & IH13 & IH14).

    (* The same for [IHt2]... *)

    specialize IHt2 with (x := x) (Φ := fun xs => J xs = 0 /\ Φ xs) (Γ := Δ2) (ρ := ρ).
    epose proof eq_refl ?[pcfTy11] as H_refl.
    unshelve specialize IHt2 with (pcfTy1 := ?pcfTy11).
    2:{
      cbn. eapply PCF.retypeT_free.
      - apply pcfTy12.
      - intros y Hy. unfold PCF_ctxExtend. destruct Nat.eqb eqn:E.
        + reflexivity.
        + apply Nat.eqb_neq in E. unfold stripCtx.
          hnf in Hmsum. specialize (Hmsum y); spec_assert Hmsum.
          { eapply nsubst_free with (t := Ifz t1 t2 t3); cbn; eauto. }
          apply msum_strip in Hmsum as [Hmsum1 Hmsum2].
          hnf in HΓ. specialize (HΓ y); spec_assert HΓ.
          { eapply nsubst_free with (t := Ifz t1 t2 t3); cbn; eauto. }
          apply eqmty_strip in HΓ. congruence.
    }
    clear H_refl; cbn in IHt2. rewrite PCF.retypeT_free_skel in *.
    specialize IHt2 with (K := M2).
    epose proof eq_refl ?[pcfTy2] as H_refl.
    unshelve specialize IHt2 with (pcfTy2 := ?pcfTy2).
    { eapply PCF.retypeT_closed. assumption. apply pcfTy2. }
    clear H_refl. rewrite PCF.retypeT_closed_skel in IHt2.
    specialize IHt2 with (1 := Hty2) as (M_IH2 & N_IH2 & σ_IH2 & IH21 & IH22 & IH23 & IH24).

    (* The same for [IHt3]... *)

    specialize IHt3 with (x := x) (Φ := fun xs => 1 <= J xs /\ Φ xs) (Γ := Δ2) (ρ := ρ).
    epose proof eq_refl ?[pcfTy11] as H_refl.
    unshelve specialize IHt3 with (pcfTy1 := ?pcfTy11).
    2:{
      cbn. eapply PCF.retypeT_free.
      - apply pcfTy13.
      - intros y Hy. unfold PCF_ctxExtend. destruct Nat.eqb eqn:E.
        + reflexivity.
        + apply Nat.eqb_neq in E. unfold stripCtx.
          hnf in Hmsum. specialize (Hmsum y); spec_assert Hmsum.
          { eapply nsubst_free with (t := Ifz t1 t2 t3); cbn; eauto. }
          apply msum_strip in Hmsum as [Hmsum1 Hmsum2].
          hnf in HΓ. specialize (HΓ y); spec_assert HΓ.
          { eapply nsubst_free with (t := Ifz t1 t2 t3); cbn; eauto. }
          apply eqmty_strip in HΓ. congruence.
    }
    clear H_refl; cbn in IHt3. rewrite PCF.retypeT_free_skel in *.
    specialize IHt3 with (K := M2).
    epose proof eq_refl ?[pcfTy2] as H_refl.
    unshelve specialize IHt3 with (pcfTy2 := ?pcfTy2).
    { eapply PCF.retypeT_closed. assumption. apply pcfTy2. }
    clear H_refl. rewrite PCF.retypeT_closed_skel in IHt3.
    specialize IHt3 with (1 := Hty3) as (M_IH3 & N_IH3 & σ_IH3 & IH31 & IH32 & IH33 & IH34).


    (* We simply 'join' the two typings with [σ_IH2] and [σ_IH3], using a simple case distinction on [J = 0]. *)
    pose (σ_23 := mty_if (fun xs => J xs =? 0) σ_IH2 σ_IH3).
    assert (Ty! Φ; Δ2 ⊢(fun xs => if J xs =? 0 then N_IH2 xs else N_IH3 xs)
                      v : σ_23 @ PCF.strip pcfTy2) as IHv23.
    {
      eapply tyT_weaken. eapply tyT_weaken_Φ. eapply tyT_cases with (f := fun xs => J xs =? 0).
      - apply IH22.
      - apply IH32.
      - reflexivity.
      - congruence.
      - cbn. hnf; intros xs Hxs. destruct Nat.eqb eqn:E; split; auto.
        + now apply Nat.eqb_eq in E.
        + apply Nat.eqb_neq in E. lia.
      - hnf; intros y Hy. unfold ctx_if. symmetry. eapply eqmty_mono_Φ. eapply eqmty_if_no_choice. firstorder.
    }

    (* Now we must join the [v] typing [σ_IH1] with [σ_23] *)
    pose proof joining IH12 IHv23 Hval as Hjoin.
    spec_assert Hjoin.
    { unfold σ_23. rewrite mty_if_strip; congruence. }
    spec_assert Hjoin.
    { hnf; intros y Hy. exfalso. eapply (proj1 (free_closed_iff _) _); eauto. }
    destruct Hjoin as (σ_join & σ1'_join & σ2'_join & Γ1'_join & Γ2'_join & Γ12_join & Hjoin1 & Hjoin2 & Hjoin3 & Hjoin4 & Hjoin5 & Hjoin6 & Hjoin7).

    eexists _, _, σ_join; repeat_split.
    2:{ eapply retypeT_closed; eauto. }
    3:{ apply msum_strip in Hjoin3 as [Hjoin3 Hjoin3']. apply eqmty_strip in Hjoin1. congruence. }
    1:{
      cbn.
      eapply tyT_Ifz with
          (Δ3 := ctxExtend Δ1 x σ1'_join) (Δ4 := ctxExtend Δ2 x σ2'_join)
          (J0 := J)
          (M3 := M_IH1) (M4 := fun xs => if J xs =? 0 then M_IH2 xs else M_IH3 xs)
          (Γ'0 := ctxExtend Γ' x σ_join).
      - eapply tyT_weaken.
        + apply IH11.
        + hnf; intros y Hy. unfold ctxExtend. destruct Nat.eqb eqn:E.
          * assumption.
          * reflexivity.
      - eapply tyT_sub.
        + apply IH21.
        + hnf; intros y Hy. unfold ctxExtend. destruct Nat.eqb eqn:E.
          * transitivity σ_23.
            { eapply eqmty_mono_Φ; eauto. firstorder. }
            { unfold σ_23. eapply eqmty_if_left.
              - congruence.
              - hnf; intros xs (Hxs1&Hxs2). now apply Nat.eqb_eq.
            }
          * reflexivity.
        + reflexivity.
        + hnf; intros xs (Hxs1&Hxs2). rewrite (proj2 (Nat.eqb_eq _ _)) by auto. reflexivity.
      - eapply tyT_sub.
        + apply IH31.
        + hnf; intros y Hy. unfold ctxExtend. destruct Nat.eqb eqn:E.
          * transitivity σ_23.
            { eapply eqmty_mono_Φ; eauto. firstorder. }
            { unfold σ_23. eapply eqmty_if_right.
              - congruence.
              - hnf; intros xs (Hxs1&Hxs2). apply Nat.eqb_neq. lia.
            }
          * reflexivity.
        + reflexivity.
        + hnf; intros xs (Hxs1&Hxs2). rewrite (proj2 (Nat.eqb_neq _ _)) by lia. reflexivity.
      - hnf in Hmsum|-*. intros y Hy.
        unfold ctxExtend. destruct Nat.eqb eqn:E.
        + eauto.
        + apply Nat.eqb_neq in E. apply Hmsum. eapply nsubst_free with (t := Ifz t1 t2 t3); cbn; eauto.
      - hnf; intros y Hy. unfold ctxExtend. destruct Nat.eqb eqn:E.
        + reflexivity.
        + apply Nat.eqb_neq in E. apply HΓ. eapply nsubst_free with (t := Ifz t1 t2 t3); cbn; eauto.
      - hnf; reflexivity.
    }
    { (* Finally, the cost... *)
      cbn; hnf; intros xs Hxs. rewrite <- HM by auto.
      destruct Nat.eqb eqn:E.
      - apply Nat.eqb_eq in E. rewrite <- IH13, <- IH23 by auto. lia.
      - apply Nat.eqb_neq in E. rewrite <- IH13 by auto. rewrite <- IH33 by (split; auto; lia). lia.
    }

  - (* Const *) clear IHt.

    inv Hty.
    pose proof PCF_tyT_Const_inv' pcfTy1 as (H_strip & ->). destruct H_strip; cbn in *. clear H.

    (* Again we need a bogus typing for [v] *)
    pose proof tyT_triv_sig Φ pcfTy2 Hval Hclos as (σ & Hv & <-).

    eexists _, _,_; repeat_split.
    + eapply tyT_Const. assumption. hnf; reflexivity.
    + eauto.
    + hnf; auto.
    + reflexivity.

  - (* P *)
    inv Hty.
    pose proof PCF_tyT_P_inv' pcfTy1 as (H_strip & pcfTy1' & ->). destruct H_strip; cbn in *. injection H as ->.
    specialize (IHt t) with (2 := Hval) (3 := Hclos); spec_assert IHt by (cbn; lia).
    specialize IHt with (x := x) (Φ := Φ) (Γ := Γ) (ρ := Nat K0).
    specialize IHt with (pcfTy1 := pcfTy1').
    specialize IHt with (1 := Hty0).
    destruct IHt as (M_IH & N_IH & σ_IH & IH1 & IH2 & IH3 & IH4).
    eexists _, _, _; repeat_split.
    + eapply tyT_Pred; eauto.
    + eauto.
    + eauto.
    + eauto.
  - (* P *)
    inv Hty.
    pose proof PCF_tyT_S_inv' pcfTy1 as (H_strip & pcfTy1' & ->). destruct H_strip; cbn in *. injection H as ->.
    specialize (IHt t) with (2 := Hval) (3 := Hclos); spec_assert IHt by (cbn; lia).
    specialize IHt with (x := x) (Φ := Φ) (Γ := Γ) (ρ := Nat K0).
    specialize IHt with (pcfTy1 := pcfTy1').
    specialize IHt with (1 := Hty0).
    destruct IHt as (M_IH & N_IH & σ_IH & IH1 & IH2 & IH3 & IH4).
    eexists _, _, _; repeat_split.
    + eapply tyT_Succ; eauto.
    + eauto.
    + eauto.
    + eauto.
  Unshelve. all: auto.
Qed.
