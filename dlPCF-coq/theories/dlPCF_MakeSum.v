Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax.
Require Import dlPCF_iSubst.
Require Import dlPCF_Types dlPCF_Sum dlPCF_Contexts.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.
Import SigmaTypeNotations.
Open Scope vector_scope.

From Coq Require Import Vector Fin.

Open Scope PCF.

Implicit Types (ϕ : nat).


(* TODO: Move somewhere else *)

(** Precise subtyping to inprecise subtyping *)

Lemma eqmty_submty {ϕ} (Φ : constr ϕ) (τ1 τ2 : mty ϕ) :
  (mty! Φ ⊢ τ1 ≡ τ2) -> (mty! Φ ⊢ τ1 ⊑ τ2).
Proof. firstorder. Qed.

Lemma eqlty_sublty {ϕ} (Φ : constr ϕ) (τ1 τ2 : lty ϕ) :
  (lty! Φ ⊢ τ1 ≡ τ2) -> (lty! Φ ⊢ τ1 ⊑ τ2).
Proof. firstorder. Qed.

Lemma eqCtx_subCtx {ϕ} (Φ : constr ϕ) (t : tm) (τ1 τ2 : ctx ϕ) :
  (ctx! t; Φ ⊢ τ1 ≡ τ2) -> (ctx! t; Φ ⊢ τ1 ⊑ τ2).
Proof. firstorder. Qed.

Lemma entails_eq_le {ϕ} (Φ : constr ϕ) (I1 I2 : idx ϕ) :
  (sem! Φ ⊨ fun xs => I1 xs = I2 xs) ->
  (sem! Φ ⊨ fun xs => I1 xs <= I2 xs).
Proof. intros H; hnf in H|-*. firstorder. Qed.

Hint Resolve eqlty_sublty eqmty_submty eqCtx_subCtx entails_eq_le : eqty_subty.







(** ** Create (bounded) Sums  *)


(** *** Syntactic case distinction for types *)

(** Combine two types with the same shape. In all index terms of the new type, it will do a case-distinction
whether to use the index from the one or the other type. *)

Definition lty_err {ϕ} : lty ϕ. Proof. now repeat constructor. Qed.
Definition mty_err {ϕ} : mty ϕ. Proof. now repeat constructor. Qed.


Fixpoint lty_if {ϕ} (f : Vector.t nat ϕ -> bool) (A B : lty ϕ) : lty ϕ :=
  match A, B with
  | Arr τ1 σ1, Arr τ2 σ2 =>
    let C := mty_if f τ1 τ2 in
    let D := mty_if f σ1 σ2 in
    Arr C D
  end
with mty_if {ϕ} (f : Vector.t nat ϕ -> bool) (τ1 τ2 : mty ϕ) : mty ϕ :=
  match τ1, τ2 with
  | Nat I1, Nat I2 =>
    Nat (fun xs => if f xs then I1 xs else I2 xs)
  | Quant I1 A, Quant I2 B =>
    let B := lty_if (fun xs => f (tl xs)) A B in
      Quant (fun xs => if f xs then I1 xs else I2 xs) B
  | _, _ => mty_err
  end.


(** Examples *)
Goal forall ϕ,
    let A := ([< iConst 1] ⋅ Arr (Nat hd) (Nat hd)) in
    let B := ([< iConst 2] ⋅ Arr (Nat (hd >> S)) (Nat (hd >> S >> S))) in
    let f := (fun xs => hd xs <? 2) in
    let C := ([< fun xs => if hd xs <? 2 then 1 else 2] ⋅
               Nat (fun xs => if hd (tl xs) <? 2 then hd xs else S (hd xs)) ⊸
               Nat (fun xs => if hd (tl xs) <? 2 then hd xs else S (S (hd xs)))) in
    @mty_if (S ϕ) f A B = C.
Proof. reflexivity. Qed.


Fixpoint eqlty_if_left {ϕ} (Φ : constr ϕ) (f : Vector.t nat ϕ -> bool) (A B : lty ϕ) { struct A } :
  lty_strip A = lty_strip B ->
  (sem! Φ ⊨ (fun xs => f xs = true)) ->
  lty! Φ ⊢ lty_if f A B ≡ A
with eqmty_if_left {ϕ} (Φ : constr ϕ) (f : Vector.t nat ϕ -> bool) (τ1 τ2 : mty ϕ) { struct τ1 } :
  mty_strip τ1 = mty_strip τ2 ->
  (sem! Φ ⊨ (fun xs => f xs = true)) ->
  mty! Φ ⊢ mty_if f τ1 τ2 ≡ τ1.
Proof.
  {
    destruct A, B; cbn.
    intros [= Hs1 Hs2] Hsem.
    eapply eqlty_Arr; eapply eqmty_if_left; eauto.
  }
  {
    destruct τ1, τ2; cbn; try discriminate.
    - intros _ Hsem.
      eapply eqmty_Nat. hnf in Hsem|-*. intros xs Hxs. now rewrite Hsem.
    - destruct A; cbn; discriminate.
    - destruct A; cbn; discriminate.
    - destruct A, A0; cbn; intros [= Hs1 Hs2] Hsem.
      eapply eqmty_Quant.
      + apply eqlty_Arr; apply eqmty_if_left; auto; firstorder.
      + hnf in Hsem|-*. intros xs Hxs. now rewrite Hsem.
  }
Qed.

(* Analogously *)
Fixpoint eqlty_if_right {ϕ} (Φ : constr ϕ) (f : Vector.t nat ϕ -> bool) (A B : lty ϕ) { struct A } :
  lty_strip A = lty_strip B ->
  (sem! Φ ⊨ (fun xs => f xs = false)) ->
  lty! Φ ⊢ lty_if f A B ≡ B
with eqmty_if_right {ϕ} (Φ : constr ϕ) (f : Vector.t nat ϕ -> bool) (τ1 τ2 : mty ϕ) { struct τ1 } :
  mty_strip τ1 = mty_strip τ2 ->
  (sem! Φ ⊨ (fun xs => f xs = false)) ->
  mty! Φ ⊢ mty_if f τ1 τ2 ≡ τ2.
Proof.
  {
    destruct A, B; cbn.
    intros [= Hs1 Hs2] Hsem.
    eapply eqlty_Arr; eapply eqmty_if_right; eauto.
  }
  {
    destruct τ1, τ2; cbn; try discriminate.
    - intros _ Hsem.
      eapply eqmty_Nat. hnf in Hsem|-*. intros xs Hxs. now rewrite Hsem.
    - destruct A; cbn; discriminate.
    - destruct A; cbn; discriminate.
    - destruct A, A0; cbn; intros [= Hs1 Hs2] Hsem.
      eapply eqmty_Quant.
      + apply eqlty_Arr; apply eqmty_if_right; auto; firstorder.
      + hnf in Hsem|-*. intros xs Hxs. now rewrite Hsem.
  }
Qed.


Fixpoint lty_if_strip {ϕ} (f : Vector.t nat ϕ -> bool) (A B : lty ϕ) { struct A } :
  lty_strip A = lty_strip B ->
  lty_strip (lty_if f A B) = lty_strip A
with mty_if_strip {ϕ} (f : Vector.t nat ϕ -> bool) (τ1 τ2 : mty ϕ) { struct τ1 } :
  mty_strip τ1 = mty_strip τ2 ->
  mty_strip (mty_if f τ1 τ2) = mty_strip τ1.
Proof.
  - destruct A, B; cbn. intros [= H1 H2]. f_equal; eapply mty_if_strip; eauto.
  - destruct τ1, τ2; cbn.
    + reflexivity.
    + destruct A; discriminate.
    + destruct A; discriminate.
    + intros H. now rewrite lty_if_strip.
Qed.


(** [mty_if] and substitution *)

Lemma lty_if_cast {ϕ1 ϕ2} (f : Vector.t nat ϕ1 -> bool) (A1 A2 : lty ϕ2) (Heq1 : ϕ1 = ϕ2) (Heq2 : ϕ2 = ϕ1) :
  lty_if f (lty_cast A1 Heq1) (lty_cast A2 Heq1) =
  lty_cast (lty_if (fun xs => f (vect_cast xs Heq2)) A1 A2) Heq1.
Proof.
  subst. rewrite !lty_cast_id. f_equal.
  fext; intros xs. f_equal. now rewrite vect_cast_id.
Qed.



Lemma lty_if_subst_var_beta {ϕ} (x : t (S ϕ)) (y : nat) (i : idx (y + (ϕ -' fin_to_nat x))) (A B : lty (S ϕ))
      (f : Vector.t nat (S ϕ) -> bool) :
  lty_strip A = lty_strip B ->
  subst_lty_var_beta x y i (lty_if f A B) =
  lty_if (subst_var_beta_fun x y i f) (subst_lty_var_beta x y i A) (subst_lty_var_beta x y i B)
with mty_if_subst_var_beta {ϕ} (x : t (S ϕ)) (y : nat) (i : idx (y + (ϕ -' fin_to_nat x))) (A B : mty (S ϕ))
      (f : Vector.t nat (S ϕ) -> bool) :
  mty_strip A = mty_strip B ->
  subst_mty_var_beta x y i (mty_if f A B) =
  mty_if (subst_var_beta_fun x y i f) (subst_mty_var_beta x y i A) (subst_mty_var_beta x y i B).
Proof.
  - clear lty_if_subst_var_beta. destruct A, B; cbn; intros [= H1 H2]. now f_equal; eapply mty_if_subst_var_beta.
  - clear mty_if_subst_var_beta. destruct A, B.
    + cbn. intros _. reflexivity.
    + destruct A; cbn; discriminate.
    + destruct A; cbn; discriminate.
    + assert (S (y + ϕ) = y + S ϕ) as aux by lia.
      assert (y + S ϕ = S (y + ϕ)) as aux' by lia.
      cbn [mty_strip]. intros H. erewrite !subst_mty_var_beta_eq_Quant. cbn [mty_if]. erewrite !subst_mty_var_beta_eq_Quant.
      f_equal.
      do 3 instantiate (1 := aux).
      rewrite lty_if_cast with (Heq2 := aux'). f_equal.
      specialize (lty_if_subst_var_beta (S ϕ) (FS x) y) as IH; cbn in IH; clear lty_if_subst_var_beta.
      specialize (IH i A A0 (fun xs : Vector.t nat (S (S ϕ)) => f (tl xs)) H).
      rewrite IH. f_equal.
      now rewrite subst_var_beta_fun_FS with (Heq2 := aux').
Qed.


(** We can prove a more general lemma then [mty_if_shift_add]. *)
(** Note that in the said lemma, [f] is not allowed to change the variable. *)

Definition add_at {ϕ} (x : Fin.t (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) (xs : Vector.t nat (S ϕ)) : Vector.t nat (S ϕ).
Proof.
  apply vect_cast with (n := fin_to_nat x + (S (ϕ -' fin_to_nat x))) in xs.
  2: abstract (rewrite sub'_correct; pose proof fin_to_nat_lt x; lia).
  pose (xs1 := vect_take _ _ xs).
  pose (xs2 := vect_skip _ _ xs).
  refine (vect_cast (vect_app xs1 (i (tl xs2) + hd xs2 ::: tl xs2)) _).
  abstract (rewrite sub'_correct; pose proof fin_to_nat_lt x; lia).
Defined.


Lemma add_at_shift_var_add {ϕ} (x : t (S ϕ)) (f : Vector.t nat (S ϕ) -> bool) (i : idx (ϕ -' fin_to_nat x)) (i0 i1 : idx (S ϕ)) :
  shift_var_add_fun x i (fun xs : Vector.t nat (S ϕ) => if f xs then i0 xs else i1 xs) =
  (fun xs : Vector.t nat (S ϕ) => if f (add_at x i xs) then shift_var_add_fun x i i0 xs else shift_var_add_fun x i i1 xs).
Proof.
  unfold shift_var_add_fun, subst_var_beta_fun; fext; intros xs.
  assert
    ((vect_cast
         (vect_app (vect_take (fin_to_nat x) (1 + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x 1)))
            (i (tl (vect_skip (fin_to_nat x) (1 + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x 1)))) +
             hd (vect_skip (fin_to_nat x) (1 + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x 1)))
             ::: vect_skip 1 (ϕ -' fin_to_nat x)
                   (vect_skip (fin_to_nat x) (1 + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x 1)))))
         (subst_var_beta_fun_subproof0 x)) =
     add_at x i xs) as Hf.
  { f_equal. unfold add_at. simp_vect_to_list. f_equal. f_equal. f_equal.
    - f_equal. now simp_vect_to_list.
    - f_equal. now simp_vect_to_list. }
  now repeat setoid_rewrite Hf.
Qed.


Lemma lty_if_shift_var_add {ϕ} (x : Fin.t (S ϕ)) (f : Vector.t nat (S ϕ) -> bool) (i : idx (ϕ -' fin_to_nat x)) (A1 A2 : lty (S ϕ)) :
  lty_strip A1 = lty_strip A2 ->
  lty_shift_var_add x (lty_if f A1 A2) i =
  lty_if (fun xs : Vector.t nat (S ϕ) => f (add_at x i xs)) (lty_shift_var_add x A1 i) (lty_shift_var_add x A2 i)
with mty_if_shift_var_add {ϕ} (x : Fin.t (S ϕ)) (f : Vector.t nat (S ϕ) -> bool) (i : idx (ϕ -' fin_to_nat x)) (τ1 τ2 : mty (S ϕ)) :
  mty_strip τ1 = mty_strip τ2 ->
  mty_shift_var_add x (mty_if f τ1 τ2) i =
  mty_if (fun xs : Vector.t nat (S ϕ) => f (add_at x i xs)) (mty_shift_var_add x τ1 i) (mty_shift_var_add x τ2 i).
Proof.
  - intros H. epose proof lty_if_subst_var_beta x 1 ?[x] A1 A2 f H as X.
    unfold lty_shift_var_add, subst_var_beta_fun.
    setoid_rewrite X. f_equal.
    unfold subst_var_beta_fun, add_at; fext; intros xs; f_equal.
    simp_vect_to_list. (do 3 f_equal).
    + f_equal. f_equal. now simp_vect_to_list.
    + f_equal. f_equal. now simp_vect_to_list.
  - intros H. epose proof mty_if_subst_var_beta x 1 ?[x] τ1 τ2 f H as X.
    unfold mty_shift_var_add, subst_var_beta_fun.
    setoid_rewrite X. f_equal.
    unfold subst_var_beta_fun, add_at; fext; intros xs; f_equal.
    simp_vect_to_list. (do 3 f_equal).
    + f_equal. f_equal. now simp_vect_to_list.
    + f_equal. f_equal. now simp_vect_to_list.
Qed.

Lemma lty_if_shift_add {ϕ} (f : Vector.t nat (S ϕ) -> bool) (A1 A2 : lty (S ϕ)) (i : idx ϕ) :
  lty_strip A1 = lty_strip A2 ->
  lty_shift_add (lty_if f A1 A2) i =
  lty_if (fun xs => f (i (tl xs) + hd xs ::: tl xs)) (lty_shift_add A1 i) (lty_shift_add A2 i).
Proof.
  intros H.
  rewrite !lty_shift_add_correct.
  setoid_rewrite lty_if_shift_var_add; eauto.
  f_equal. fext; intros xs; f_equal. unfold add_at; cbn. f_equal.
  - f_equal.
    + f_equal. now simp_vect_to_list.
    + f_equal. now simp_vect_to_list.
  - now simp_vect_to_list.
Qed.
Lemma mty_if_shift_add {ϕ} (f : Vector.t nat (S ϕ) -> bool) (τ1 τ2 : mty (S ϕ)) (i : idx ϕ) :
  mty_strip τ1 = mty_strip τ2 ->
  mty_shift_add (mty_if f τ1 τ2) i =
  mty_if (fun xs => f (i (tl xs) + hd xs ::: tl xs)) (mty_shift_add τ1 i) (mty_shift_add τ2 i).
Proof.
  intros H.
  rewrite !mty_shift_add_correct.
  setoid_rewrite mty_if_shift_var_add; eauto.
  f_equal. fext; intros xs; f_equal. unfold add_at; cbn. f_equal.
  - f_equal.
    + f_equal. now simp_vect_to_list.
    + f_equal. now simp_vect_to_list.
  - now simp_vect_to_list.
Qed.


Lemma lty_if_subst_beta_two {ϕ} (f : Vector.t nat (S ϕ) -> bool) (A1 A2 : lty (S ϕ)) (i : idx (S (S ϕ))) :
  lty_strip A1 = lty_strip A2 ->
  subst_lty_beta_two (lty_if f A1 A2) i =
  lty_if (fun xs : Vector.t nat (S (S ϕ)) => f (i xs ::: tl (tl xs))) (subst_lty_beta_two A1 i) (subst_lty_beta_two A2 i).
Proof.
  intros H.
  rewrite !subst_lty_beta_two_correct'.
  rewrite lty_if_subst_var_beta by auto.
  f_equal. rewrite subst_var_beta_fun_Fin0. reflexivity.
Qed.




(** More complicated, less general lemma, where the substitution itself depends on [i] *)

(* TODO: Maybe rename a bit *)
Definition tam {ϕ} (x : Fin.t (S ϕ)) : Vector.t nat (S ϕ) -> Vector.t nat (ϕ -' fin_to_nat x).
Proof.
  intros xs.
  apply vect_cast with (n := fin_to_nat x + S (ϕ -' fin_to_nat x)) in xs.
  2: abstract (rewrite sub'_correct; pose proof fin_to_nat_lt x; lia).
  exact (tl (vect_skip _ _ xs)).
Defined.

Definition tamtam {ϕ} (x : Fin.t (S ϕ)) (y : nat) : Vector.t nat (y + ϕ) -> Vector.t nat (ϕ -' fin_to_nat x).
Proof.
  intros xs.
  pose proof vect_skip _ _ xs as xs'.
  apply vect_cast with (n := fin_to_nat x + (ϕ -' fin_to_nat x)) in xs'.
  2: abstract (rewrite sub'_correct; pose proof fin_to_nat_lt x; lia).
  exact (vect_skip _ _ xs').
Defined.

Lemma tam_FS {ϕ} (x : Fin.t (S ϕ)) (xs : Vector.t nat (S (S ϕ))) :
  tam (FS x) xs = tam x (tl xs).
Proof.
  unfold tam. simp_vect_to_list. cbn [fin_to_nat].
  now rewrite !skipn_tl, skipn_tl'.
Qed.

Goal forall ϕ (xs : Vector.t nat _),
    @tam ϕ Fin0 xs = tl xs.
Proof. intros. unfold tam. cbn. now simp_vect_to_list. Qed.


Lemma skipn_skipn' (X : Type) (xs : list X) (m n : nat) :
  skipn m (skipn n xs) = skipn n (skipn m xs).
Proof. rewrite !skipn_skipn. f_equal. lia. Qed.


Lemma tamtamtam {X:Type} {ϕ} (x : Fin.t (S ϕ)) (y : nat) (f : Vector.t nat (ϕ -' fin_to_nat x) -> bool) i i0 i1 i2 :
  subst_var_beta_fun (X := X) x y
    (fun xs : Vector.t nat (y + (ϕ -' fin_to_nat x)) => if f (vect_skip y (ϕ -' fin_to_nat x) xs) then i1 xs else i2 xs)
    (fun xs : Vector.t nat (S ϕ) => if f (tam x xs) then i xs else i0 xs) =
  (fun xs : Vector.t nat (y + ϕ) => if f (tamtam x y xs) then subst_var_beta_fun x y i1 i xs else subst_var_beta_fun x y i2 i0 xs).
Proof.
  fext; intros xs.
  destruct (f (tamtam x y xs)) eqn:E.
  * unfold subst_var_beta_fun, tam, tamtam in *; cbn in *.
    replace (tl
     (vect_skip (fin_to_nat x) (S (ϕ -' fin_to_nat x))
        (vect_cast
           (vect_cast
              (vect_app (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
                 ((if
                    f
                      (vect_skip y (ϕ -' fin_to_nat x)
                         (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y))))
                   then i1 (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
                   else i2 (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y))))
                  ::: vect_skip y (ϕ -' fin_to_nat x)
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))))
              (subst_var_beta_fun_subproof0 x)) (tam_subproof x))))
      with (vect_skip (fin_to_nat x) (ϕ -' fin_to_nat x) (vect_cast (vect_skip y ϕ xs) (tamtam_subproof x))).
    2:{
      replace (vect_skip y (ϕ -' fin_to_nat x)
                         (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y))))
        with (vect_skip (fin_to_nat x) (ϕ -' fin_to_nat x) (vect_cast (vect_skip y ϕ xs) (tamtam_subproof x))).
      2:{ simp_vect_to_list. apply skipn_skipn'. }
      rewrite E.
      simp_vect_to_list.
      rewrite !skipn_skipn.
      rewrite skipn_app'.
      2:{ rewrite firstn_length, vect_to_list_length. pose proof fin_to_nat_lt x. lia. }
      cbn. reflexivity.
    }
    rewrite E.
    f_equal. simp_vect_to_list.
    replace (vect_skip y (ϕ -' fin_to_nat x)
                       (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y))))
      with (vect_skip (fin_to_nat x) (ϕ -' fin_to_nat x) (vect_cast (vect_skip y ϕ xs) (tamtam_subproof x))).
    2:{ simp_vect_to_list. apply skipn_skipn'. }
    rewrite E. auto.
  * unfold subst_var_beta_fun, tam, tamtam in *; cbn in *.
    replace (tl
     (vect_skip (fin_to_nat x) (S (ϕ -' fin_to_nat x))
        (vect_cast
           (vect_cast
              (vect_app (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
                 ((if
                    f
                      (vect_skip y (ϕ -' fin_to_nat x)
                         (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y))))
                   then i1 (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
                   else i2 (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y))))
                  ::: vect_skip y (ϕ -' fin_to_nat x)
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))))
              (subst_var_beta_fun_subproof0 x)) (tam_subproof x))))
      with (vect_skip (fin_to_nat x) (ϕ -' fin_to_nat x) (vect_cast (vect_skip y ϕ xs) (tamtam_subproof x))).
    2:{
      replace (vect_skip y (ϕ -' fin_to_nat x)
                         (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y))))
        with (vect_skip (fin_to_nat x) (ϕ -' fin_to_nat x) (vect_cast (vect_skip y ϕ xs) (tamtam_subproof x))).
      2:{ simp_vect_to_list. apply skipn_skipn'. }
      rewrite E.
      simp_vect_to_list.
      rewrite !skipn_skipn.
      rewrite skipn_app'.
      2:{ rewrite firstn_length, vect_to_list_length. pose proof fin_to_nat_lt x. lia. }
      cbn. reflexivity.
    }
    rewrite E.
    f_equal. simp_vect_to_list.
    replace (vect_skip y (ϕ -' fin_to_nat x)
                       (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y))))
      with (vect_skip (fin_to_nat x) (ϕ -' fin_to_nat x) (vect_cast (vect_skip y ϕ xs) (tamtam_subproof x))).
    2:{ simp_vect_to_list. apply skipn_skipn'. }
    rewrite E. auto.
Qed.



Lemma lty_if_subst_var_beta2 {ϕ} (x : t (S ϕ)) (y : nat) (i1 i2 : idx (y + (ϕ -' fin_to_nat x))) (A B : lty (S ϕ))
      (f : Vector.t nat (ϕ -' fin_to_nat x) -> bool) :
  lty_strip A = lty_strip B ->
  subst_lty_var_beta x y (fun xs => if f (vect_skip _ _ xs) then i1 xs else i2 xs) (lty_if (fun xs => f (tam x xs)) A B) =
  lty_if (fun xs => f (tamtam x y xs)) (subst_lty_var_beta x y i1 A) (subst_lty_var_beta x y i2 B)
with mty_if_subst_var_beta2 {ϕ} (x : t (S ϕ)) (y : nat) (i1 i2 : idx (y + (ϕ -' fin_to_nat x))) (A B : mty (S ϕ))
      (f : Vector.t nat (ϕ -' fin_to_nat x) -> bool) :
  mty_strip A = mty_strip B ->
  subst_mty_var_beta x y (fun xs => if f (vect_skip _ _ xs) then i1 xs else i2 xs) (mty_if (fun xs => f (tam x xs)) A B) =
  mty_if (fun xs => f (tamtam x y xs)) (subst_mty_var_beta x y i1 A) (subst_mty_var_beta x y i2 B).
Proof.
  - clear lty_if_subst_var_beta2. destruct A, B; cbn; intros [= H1 H2]. now f_equal; eapply mty_if_subst_var_beta2.
  - clear mty_if_subst_var_beta2. destruct A, B.
    + cbn. intros _. f_equal; apply tamtamtam.
    + destruct A; cbn; discriminate.
    + destruct A; cbn; discriminate.
    + assert (S (y + ϕ) = y + S ϕ) as aux by lia.
      assert (y + S ϕ = S (y + ϕ)) as aux' by lia.
      cbn [mty_strip]. intros H. erewrite !subst_mty_var_beta_eq_Quant. cbn [mty_if]. erewrite !subst_mty_var_beta_eq_Quant.
      f_equal.
      {
        apply tamtamtam.
      }
      {
        do 3 instantiate (1 := aux).
        rewrite lty_if_cast with (Heq2 := aux'). f_equal.
        specialize (lty_if_subst_var_beta2 (S ϕ) (FS x) y) as IH; cbn in IH; clear lty_if_subst_var_beta2.
        specialize (IH i1 i2 A A0 f H).
        replace (lty_if (fun xs : Vector.t nat (S (S ϕ)) => f (tam x (tl xs))) A A0)
          with (lty_if (fun xs : Vector.t nat (S (S ϕ)) => f (tam (FS x) xs)) A A0).
        2:{f_equal; fext; intros xs. f_equal. apply tam_FS. }
        rewrite IH. f_equal. fext; intros xs. f_equal.
        unfold tamtam. simp_vect_to_list. now rewrite skipn_tl', !skipn_skipn, skipn_tl'.
      }
Qed.


(* x = 0, y = 0 *)
Lemma lty_if_subst_beta_ground2 {ϕ} (f : Vector.t nat ϕ -> bool) (A1 A2 : lty (S ϕ)) (i1 i2 : idx ϕ) :
  lty_strip A1 = lty_strip A2 ->
  subst_lty_beta_ground (lty_if (fun xs => f (tl xs)) A1 A2) (fun xs => if f xs then i1 xs else i2 xs) =
  lty_if f (subst_lty_beta_ground A1 i1) (subst_lty_beta_ground A2 i2).
Proof.
  intros H.
  rewrite !subst_lty_beta_ground_correct.
  replace (fun xs : Vector.t nat (S ϕ) => f (tl xs)) with (fun xs => f (tam Fin0 xs)).
  2:{ fext; intros; f_equal. now unfold tam; simp_vect_to_list. }
  rewrite lty_if_subst_var_beta2 with (x := Fin0) (y := 0) (f0 := f) by auto.
  cbn. f_equal.
  fext; intros xs. now rewrite vect_cast_id.
Qed.


(* x = 0, y = 2 *)
Lemma lty_if_subst_beta_two2 {ϕ} (f : Vector.t nat ϕ -> bool) (A1 A2 : lty (S ϕ)) (i1 i2 : idx (S (S ϕ))) :
  lty_strip A1 = lty_strip A2 ->
  subst_lty_beta_two (lty_if (fun xs => f (tl xs)) A1 A2) (fun xs => if f (tl (tl xs)) then i1 xs else i2 xs) =
                                                                (* ^ Ignore the new free vars *)
  lty_if (fun xs => f (tl (tl xs))) (subst_lty_beta_two A1 i1) (subst_lty_beta_two A2 i2).
         (* ^ Ignore the new free vars *)
Proof.
  intros H.
  setoid_rewrite subst_lty_beta_two_correct'.
  replace (fun xs : Vector.t nat (S (S ϕ)) => f (tl (tl xs))) with (fun xs => f (tamtam Fin0 2 xs)).
  2:{ fext; intros xs. f_equal. unfold tamtam; cbn. now simp_vect_to_list. }
  erewrite <- lty_if_subst_var_beta2 with (x := Fin0) (y := 2) (f0 := f) by auto.
  f_equal. f_equal. fext; intros xs. f_equal. unfold tam; cbn. now simp_vect_to_list.
Qed.

Lemma lty_if_shift_add2 {ϕ} (f : Vector.t nat ϕ -> bool) (A1 A2 : lty (S ϕ)) (i1 i2 : idx ϕ) :
  lty_strip A1 = lty_strip A2 ->
  lty_shift_add (lty_if (fun xs => f (tl xs)) A1 A2) (fun xs => if f xs then i1 xs else i2 xs) =
  lty_if (fun xs => f (tl xs)) (lty_shift_add A1 i1) (lty_shift_add A2 i2).
Proof.
  intros H.
  setoid_rewrite lty_shift_add_correct'. cbn.
  replace (fun xs : Vector.t nat (S ϕ) => f (tl xs)) with (fun xs => f (tam Fin0 xs)).
  2:{ fext; intros. f_equal. unfold tam; cbn. now simp_vect_to_list. }
  replace (fun xs : Vector.t nat (S ϕ) => (if f (tl xs) then i1 (tl xs) else i2 (tl xs)) + hd xs)
    with (fun xs : Vector.t nat (S ϕ) => if f (tl xs) then i1 (tl xs) + hd xs else i2 (tl xs) + hd xs).
  2:{ fext; intros xs. now destruct f. }
  erewrite lty_if_subst_var_beta2 with (x := Fin0) (y := 1) (i3 := fun xs => i1 (tl xs) + hd xs) (i4 := fun xs => i2 (tl xs) + hd xs); auto.
  f_equal. fext; intros xs. f_equal. now unfold tam, tamtam; simp_vect_to_list.
Qed.
Lemma mty_if_shift_add2 {ϕ} (f : Vector.t nat ϕ -> bool) (A1 A2 : mty (S ϕ)) (i1 i2 : idx ϕ) :
  mty_strip A1 = mty_strip A2 ->
  mty_shift_add (mty_if (fun xs => f (tl xs)) A1 A2) (fun xs => if f xs then i1 xs else i2 xs) =
  mty_if (fun xs => f (tl xs)) (mty_shift_add A1 i1) (mty_shift_add A2 i2).
Proof.
  intros H.
  setoid_rewrite mty_shift_add_correct'. cbn.
  replace (fun xs : Vector.t nat (S ϕ) => f (tl xs)) with (fun xs => f (tam Fin0 xs)).
  2:{ fext; intros. f_equal. unfold tam; cbn. now simp_vect_to_list. }
  replace (fun xs : Vector.t nat (S ϕ) => (if f (tl xs) then i1 (tl xs) else i2 (tl xs)) + hd xs)
    with (fun xs : Vector.t nat (S ϕ) => if f (tl xs) then i1 (tl xs) + hd xs else i2 (tl xs) + hd xs).
  2:{ fext; intros xs. now destruct f. }
  erewrite mty_if_subst_var_beta2 with (x := Fin0) (y := 1) (i3 := fun xs => i1 (tl xs) + hd xs) (i4 := fun xs => i2 (tl xs) + hd xs); auto.
  f_equal. fext; intros xs. f_equal. now unfold tam, tamtam; simp_vect_to_list.
Qed.


Lemma mty_if_beta_ground2 {ϕ} (f : Vector.t nat ϕ -> bool) (σ1 σ2 : mty (S ϕ)) (i1 i2 : idx ϕ) :
  mty_strip σ1 = mty_strip σ2 ->
  subst_mty_beta_ground (mty_if (fun xs => f (tl xs)) σ1 σ2) (fun xs => if f xs then i1 xs else i2 xs) =
  mty_if f (subst_mty_beta_ground σ1 i1) (subst_mty_beta_ground σ2 i2).
Proof.
  intros H.
  setoid_rewrite subst_mty_beta_ground_correct.
  replace (fun xs : Vector.t nat (S ϕ) => f (tl xs)) with (fun xs => f (tam Fin0 xs)).
  2:{ fext; intros. f_equal. unfold tam; cbn. now simp_vect_to_list. }
  rewrite mty_if_subst_var_beta2 with (x := Fin0) (y := 0) (f0 := f); auto.
  f_equal. fext; intros xs; f_equal. unfold tamtam; cbn. now simp_vect_to_list.
Qed.



(** *** Generating the modal sum *)

(** On paper, generating modal sums is only possible if the 'equational program' has the 'universality property',
i.e. all kinds of computations are allowed in the index terms. *)


(** Can [τ1] and [τ2] be combined? This implies [lty_same_shape τ1 τ2]. *)
Definition mty_comp {ϕ} (Φ : constr ϕ) (τ1 τ2 : mty ϕ) : Prop :=
  match τ1, τ2 with
  | Nat i1, Nat i2 => (sem! Φ ⊨ fun xs => i1 xs = i2 xs)
  | Quant I1 A, Quant I2 B => lty_strip A = lty_strip B
  | _, _ => False
  end.

Definition lty_comp {ϕ} (Φ : constr ϕ) (A B : lty ϕ) : Prop :=
  match A, B with
  | Arr τ1 σ1, Arr τ2 σ2 =>
    mty_comp Φ τ1 τ2 /\ mty_comp Φ σ1 σ2
  end.


Lemma lty_comp_same_shape {ϕ} (Φ : constr ϕ) (A B : lty ϕ) :
  lty_comp Φ A B -> lty_strip A = lty_strip B
with mty_comp_same_shape {ϕ} (Φ : constr ϕ) (τ1 τ2 : mty ϕ) :
  mty_comp Φ τ1 τ2 -> mty_strip τ1 = mty_strip τ2.
Proof.
  - destruct A, B; cbn. intros (H1&H2); hnf; cbn; f_equal; now eapply mty_comp_same_shape; eauto.
  - destruct τ1, τ2; cbn; tauto.
Qed.


Lemma lty_comp_refl {ϕ} (Φ : constr ϕ) (A : lty ϕ) :
  lty_comp Φ A A
with mty_comp_refl {ϕ} (Φ : constr ϕ) (τ : mty ϕ) :
  mty_comp Φ τ τ.
Proof.
  - destruct A. cbn. split; apply mty_comp_refl.
  - destruct τ; cbn.
    + hnf; reflexivity.
    + hnf; reflexivity.
Qed.

Lemma lty_comp_sym {ϕ} (Φ : constr ϕ) (A B : lty ϕ) :
  lty_comp Φ A B ->
  lty_comp Φ B A
with mty_comp_sym {ϕ} (Φ : constr ϕ) (τ1 τ2 : mty ϕ) :
  mty_comp Φ τ1 τ2 ->
  mty_comp Φ τ2 τ1.
Proof.
  - destruct A, B. cbn. intros (H1&H2). split; apply mty_comp_sym; eassumption.
  - destruct τ1, τ2; cbn; try tauto.
    + now intros ->.
    + now hnf; symmetry.
Qed.

Lemma lty_comp_trans {ϕ} (Φ : constr ϕ) (A B C : lty ϕ) :
  lty_comp Φ A B -> lty_comp Φ B C -> lty_comp Φ A C
with mty_comp_trans {ϕ} (Φ : constr ϕ) (τ1 τ2 τ3 : mty ϕ) :
  mty_comp Φ τ1 τ2 -> mty_comp Φ τ2 τ3 -> mty_comp Φ τ1 τ3.
Proof.
  - destruct A, B, C; cbn. intros (H1&H2) (H3&H4).
    split.
    + eapply mty_comp_trans; eassumption.
    + eapply mty_comp_trans; eassumption.
  - destruct τ1, τ2, τ3; cbn; try tauto.
    intros H1 H2.
    + firstorder congruence.
    + hnf; now etransitivity; eauto.
Qed.

Instance lty_comp_Equiv {ϕ} {Φ : constr ϕ} : Equivalence (@lty_comp ϕ Φ).
Proof.
  constructor.
  - hnf. apply lty_comp_refl.
  - hnf. apply lty_comp_sym.
  - hnf. apply lty_comp_trans.
Qed.

Instance mty_comp_Equiv {ϕ} {Φ : constr ϕ} : Equivalence (@mty_comp ϕ Φ).
Proof.
  constructor.
  - hnf. apply mty_comp_refl.
  - hnf. apply mty_comp_sym.
  - hnf. apply mty_comp_trans.
Qed.


Lemma msum_mty_com {ϕ} (Φ : constr ϕ) (τ1 τ2 σ : mty ϕ) :
  msum τ1 τ2 σ ->
  mty_comp Φ τ1 τ2.
Proof.
  destruct τ1 as [i|i A], τ2 as [j|j B], σ as [k|k C]; cbn in *; try tauto.
  - intros (->&->). hnf; reflexivity.
  - intros (->&->&H). now setoid_rewrite subst_lty_strip.
Qed.



Definition make_msum {ϕ} (τ1 τ2 : mty ϕ) : mty ϕ * (mty ϕ * mty ϕ) :=
  match τ1, τ2 with
  | Nat i, Nat j => (Nat i, (Nat i, Nat i)) (* By assumption, i=j *)
  | Quant i A, Quant j B =>
    (* [B'] is like [B], but it the indexes in the [Nat] leaves are shifted by [i] *)
    let B' := lty_shift_sub B i in
    let C := lty_if (fun xs => hd xs <? i (tl xs)) A B' in
    (Quant i C,
     (Quant j (lty_shift_add C i),
      Quant (fun xs => i xs + j xs) C))
  | _, _ => (mty_err, (mty_err, mty_err))
  end.

(* The syntactic part *)
Lemma make_msum_correct_msum {ϕ} (Φ : constr ϕ) (τ1 τ2 : mty ϕ) :
  mty_comp Φ τ1 τ2 ->
  let ρ1 := fst (make_msum τ1 τ2) in
  let ρ2 := fst (snd (make_msum τ1 τ2)) in
  let ρ3 := snd (snd (make_msum τ1 τ2)) in
  msum ρ1 ρ2 ρ3.
Proof. intros; subst. destruct τ1, τ2; cbn in *; tauto. Qed.


(* The semantic part *)
Lemma make_msum_correct_eqmty {ϕ} (Φ : constr ϕ) (τ1 τ2 : mty ϕ) :
  mty_comp Φ τ1 τ2 ->
  let ρ1 := fst (make_msum τ1 τ2) in
  let ρ2 := fst (snd (make_msum τ1 τ2)) in
  let ρ3 := snd (snd (make_msum τ1 τ2)) in
  (mty! Φ ⊢ ρ1 ≡ τ1) /\
  (mty! Φ ⊢ ρ2 ≡ τ2).
Proof.
  intros H **; subst.
  destruct τ1, τ2; cbn -[Nat.ltb] in *; subst; try tauto.
  - split; apply eqmty_Nat; hnf in H|-*; firstorder.
  - destruct A, A0; cbn in H; injection H as H1 H2.
    split.
    + apply eqmty_Quant. 2: hnf; reflexivity.
      eapply eqlty_if_left.
      * setoid_rewrite subst_lty_strip. cbn. congruence.
      * hnf; intros. now apply Nat.ltb_lt.
    + apply eqmty_Quant. 2: hnf; reflexivity.
      etransitivity.
      * eapply eqlty_mono_Φ. eapply eqlty_shift_add.
        -- eapply eqlty_if_right.
           ++ setoid_rewrite subst_lty_strip. cbn. congruence.
           ++ hnf; intros. apply Nat.ltb_ge. apply H.
        -- hnf; intros. cbn. lia.
      * now rewrite lty_shift_add_sub.
Qed.

Lemma make_msum_correct {ϕ} (Φ : constr ϕ) (τ1 τ2 : mty ϕ) :
  mty_comp Φ τ1 τ2 ->
  let ρ1 := fst (make_msum τ1 τ2) in
  let ρ2 := fst (snd (make_msum τ1 τ2)) in
  let ρ3 := snd (snd (make_msum τ1 τ2)) in
  msum ρ1 ρ2 ρ3 /\ (mty! Φ ⊢ ρ1 ≡ τ1) /\ (mty! Φ ⊢ ρ2 ≡ τ2).
Proof.
  intros H. split.
  - now eapply make_msum_correct_msum; eauto.
  - now eapply make_msum_correct_eqmty; eauto.
Qed.


Definition make_ctxMSum {ϕ} (Γ1 Γ2 : ctx ϕ) : ctx ϕ * (ctx ϕ * ctx ϕ) :=
  ((fun x => fst (make_msum (Γ1 x) (Γ2 x))),
   ((fun x => fst (snd (make_msum (Γ1 x) (Γ2 x)))),
    (fun x => snd (snd (make_msum (Γ1 x) (Γ2 x)))))).

Lemma make_ctxMSum_correct {ϕ} (Φ : constr ϕ) (t : tm) (Γ1 Γ2 : ctx ϕ) :
  (forall x, free x t -> mty_comp Φ (Γ1 x) (Γ2 x)) ->
  let Δ1 := fst (make_ctxMSum Γ1 Γ2) in
  let Δ2 := fst (snd (make_ctxMSum Γ1 Γ2)) in
  let Δ3 := snd (snd (make_ctxMSum Γ1 Γ2)) in
  ctxMSum t Δ1 Δ2 Δ3 /\ (ctx! t; Φ ⊢ Δ1 ≡ Γ1) /\ (ctx! t; Φ ⊢ Δ2 ≡ Γ2).
Proof.
  intros H. cbn zeta. unfold ctxMSum, make_ctxMSum; cbn. repeat_split.
  - intros x Hx. eapply make_msum_correct; eauto.
  - intros x Hx. eapply make_msum_correct; eauto.
  - intros x Hx. eapply make_msum_correct; eauto.
Qed.



(** *** Generating the bounded modal sum *)

(* Maybe start a new file? *)

(* TODO: Move *)
Lemma eta2 : forall (A : Type) (n : nat) (v : VectorDef.t A (S (S n))), v = hd v ::: hd (tl v) ::: tl (tl v).
Proof. intros. now rewrite <- !eta. Qed.

Definition bsum_ok {ϕ} (σ : mty (S ϕ)) : Type :=
  match σ with
  | Nat (_ as I) =>
    (* In case σ is a [Nat I], we just have to assume the same as in the definition *)
    { J : idx ϕ & σ = subst_mty (Nat J) (fun f xs => f (tl xs)) }
  | Quant (_ as I) A => True
  end.


Fixpoint find_Sum_slot (i : nat) (f : nat -> nat) (x : nat) { struct i } : nat * nat :=
  match i with
  | 0 => (def_nat, def_nat)
  | S i =>
    if lt_dec x (f 0) then (0, x)
    else let '(a, b) := find_Sum_slot i (fun n => f (S n)) (x - f 0) in
         (S a, b)
  end.

(*
Eval cbv in
    let f := fun n => match n with
                   | 0 => 3
                   | 1 => 2
                   | _ => 3
                   end in
    let i := 3 in
    List.map (find_Sum_slot i f) (0 :: 1 :: 2 :: 3 :: 4 :: 5 :: 6 :: 7 :: List.nil).
(* = (0, 0) :: (0, 1) :: (0, 2) :: (1, 0) :: (1, 1) :: (2, 0) :: (2, 1) :: (2, 2) :: nil *)
*)


Lemma find_Sum_slot_correct (i : nat) (f : nat -> nat) (a b a' b' : nat) :
  a < i ->
  b < f a ->
  find_Sum_slot i f (b + Σ_{d < a} f d) = (a', b') ->
  a = a' /\ b = b'.
Proof.
  induction i as [ | i IH] in f,a,b,a',b'|-*; intros Hi Hb Hfind; cbn in *.
  - exfalso. lia.
  - destruct lt_dec as [d|d].
    + injection Hfind as <- <-.
      destruct a.
      * cbn in d|-*. rewrite Nat.add_0_r in d|-*. split; reflexivity.
      * exfalso. rewrite Sum_eq_S' in d. lia.
    + destruct (find_Sum_slot i (fun n : nat => f (S n)) (b + (Σ_{d < a} f d) - f 0)) as (m&n) eqn:E.
      injection Hfind as <- <-.
      destruct a.
      * exfalso. cbn in *. lia.
      * rewrite Sum_eq_S' in E.
        replace (b + (f 0 + (Σ_{x < a} f (S x))) - f 0) with (b + (Σ_{x < a} f (S x))) in E by lia.
        eapply IH in E; eauto; lia.
Qed.



Definition idx_find_slot {ϕ} (I : idx ϕ) (J : idx (S ϕ)) : Vector.t nat (S ϕ) -> nat * nat :=
  fun xs : Vector.t nat (S ϕ) =>
    let c := hd xs in
    let xs' := tl xs in
    find_Sum_slot (I xs') (fun x => J (x ::: xs')) c.


Lemma idx_find_slot_correct {ϕ} (I : idx ϕ) (J : idx (S ϕ)) :
  forall (a b a' b' : nat) (xs : Vector.t nat ϕ),
    a < I xs ->
    b < J (a ::: xs) ->
    idx_find_slot I J (b + (Σ_{d < a} J (d ::: xs)) ::: xs) = (a', b') ->
    a = a' /\ b = b'.
Proof.
  cbn; intros * Ha Hb Hf.
  exact (find_Sum_slot_correct (fun x => J (x ::: xs)) Ha Hb Hf).
Qed.



Definition make_bsum_Quant {ϕ} (I : idx ϕ) (J : idx (S ϕ)) (A : lty (S (S ϕ))) : lty (S (S ϕ)) * (mty ϕ) :=
 let g := idx_find_slot I J in
 let A' := subst_lty A (fun (j : idx (S (S ϕ))) (xs : Vector.t nat (S ϕ)) => let '(a, b) := g xs in j (b ::: a ::: tl xs)) in
 (subst_lty_beta_two
    A' (fun xs : Vector.t nat (S (S ϕ)) =>
          let b := hd xs in let a := hd (tl xs) in let xs' := tl (tl xs) in b + (Σ_{d < a} J (d ::: xs'))),
  [ < fun xs : Vector.t nat ϕ => Σ_{a < I xs} J (a ::: xs)] ⋅ A').


Lemma make_bsum_Quant_correct {ϕ} (I : idx ϕ) (J : idx (S ϕ)) (A : lty (S (S ϕ))) :
  let B := fst (make_bsum_Quant I J A) in
  let ρ := snd (make_bsum_Quant I J A) in
  bsum_Quant I (Quant J B) ρ **
  (mty! fun xs => hd xs < I (tl xs) ⊢ (Quant J A) ≡ (Quant J B)).
Proof.
  intros B ρ. unfold make_bsum_Quant in *; cbn -[Nat.ltb] in *.
  split.
  - hnf. eexists _, _. split; reflexivity.
  - eapply eqmty_Quant. 2: hnf; reflexivity.
    setoid_rewrite subst_lty_twice; unfold funcomp.
    rewrite <- subst_lty_id at 1.
    eapply eqlty_subst_idx_sem.
    hnf; intros xs (Hxs1&Hxs2). intros j. unfold id.
    destruct (idx_find_slot I J (hd xs + (Σ_{d < hd (tl xs)} J (d ::: tl (tl xs))) ::: tl (tl xs))) as (a,b) eqn:E.
    pose proof (idx_find_slot_correct I J) as Hg.
    apply Hg in E as (Hf1&Hf2); cbn; eauto.
    2:{ now rewrite <- eta. }
    f_equal. rewrite eta2 at 1. congruence.
Qed.


Theorem make_bsum_sig {ϕ} (I : idx ϕ) {σ : mty (S ϕ)} :
  bsum_ok σ ->
  existsS (σ' : mty (S ϕ)) (ρ : mty ϕ),
    (mty! fun xs => hd xs < I (tl xs) ⊢ σ ≡ σ') **
    bsum I σ' ρ.
Proof.
  intros H. destruct σ as [k | J A]; cbn in *.
  - destruct H as (J&[= ->]).
    eexists _,_. split.
    + reflexivity.
    + hnf. left. hnf. cbn. eauto.
  - pose proof make_bsum_Quant_correct I J A as (L1 & L2).
    eexists _, _; split; hnf; eauto.
Qed.



(** *** Case distinction lemma for subtyping *)

Lemma sublty_submty_cases {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (s : nat) :
  (forall A B, size_lty A + size_lty B <= s ->
          (lty! Φ1 ⊢ A ⊑ B) ->
          (lty! Φ2 ⊢ A ⊑ B) ->
          (lty! (fun xs => if f xs then Φ1 xs else Φ2 xs) ⊢ A ⊑ B)) /\
  (forall σ τ, size_mty σ + size_mty τ <= s ->
          (mty! Φ1 ⊢ σ ⊑ τ) ->
          (mty! Φ2 ⊢ σ ⊑ τ) ->
          (mty! (fun xs => if f xs then Φ1 xs else Φ2 xs) ⊢ σ ⊑ τ)).
Proof.
  revert dependent ϕ. induction s as [s IH] using lt_wf_ind; intros. split.
  - intros A B Hs H1 H2. destruct H1. apply sublty_arr_inv in H2 as (H2&H3); cbn in Hs.
    constructor; eapply IH; eauto; lia.
  - intros σ τ Hs H1 H2. destruct H1.
    + apply submty_Nat_inv in H2 as H2. constructor.
      hnf in H,H2|-*; intros xs Hxs. destruct (f xs); auto.
    + apply submty_Quant_inv in H2 as (H2&H3); cbn in Hs. econstructor.
      * apply @sublty_mono_Φ with
            (Φ := fun xs : Vector.t nat (S ϕ) => if (fun xs => f (tl xs)) xs then hd xs < j (tl xs) /\ Φ1 (tl xs) else hd xs < j (tl xs) /\ Φ2 (tl xs)).
        2:{ hnf; intros xs Hxs. destruct (f (tl xs)); firstorder. }
        eapply IH; eauto.
        lia.
      * hnf in H,H2|-*; intros xs Hxs. destruct (f xs); firstorder.
Qed.

Lemma sublty_cases {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) :
  forall A B, (lty! Φ1 ⊢ A ⊑ B) ->
          (lty! Φ2 ⊢ A ⊑ B) ->
          (lty! (fun xs => if f xs then Φ1 xs else Φ2 xs) ⊢ A ⊑ B)
with submty_cases {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) :
  forall σ τ, (mty! Φ1 ⊢ σ ⊑ τ) ->
          (mty! Φ2 ⊢ σ ⊑ τ) ->
          (mty! (fun xs => if f xs then Φ1 xs else Φ2 xs) ⊢ σ ⊑ τ).
Proof. all: (intros; eapply sublty_submty_cases; eauto). Qed.

Lemma eqlty_cases {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) :
  forall A B, (lty! Φ1 ⊢ A ≡ B) ->
          (lty! Φ2 ⊢ A ≡ B) ->
          (lty! (fun xs => if f xs then Φ1 xs else Φ2 xs) ⊢ A ≡ B)
with eqmty_cases {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) :
  forall σ τ, (mty! Φ1 ⊢ σ ≡ τ) ->
          (mty! Φ2 ⊢ σ ≡ τ) ->
          (mty! (fun xs => if f xs then Φ1 xs else Φ2 xs) ⊢ σ ≡ τ).
Proof.
  - intros ? ? [H1 H2] [H3 H4]; split; eapply sublty_cases; eauto.
  - intros ? ? [H1 H2] [H3 H4]; split; eapply submty_cases; eauto.
Qed.


Lemma sublty_cases' {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (A B : lty ϕ) :
  (lty! fun xs => f xs = true  /\ Φ1 xs ⊢ A ⊑ B) ->
  (lty! fun xs => f xs = false /\ Φ2 xs ⊢ A ⊑ B) ->
  (lty! (fun xs => if f xs then Φ1 xs else Φ2 xs) ⊢ A ⊑ B).
Proof.
  intros ty1 ty2.
  eapply @sublty_mono_Φ with (Φ := fun xs => if f xs then f xs = true /\ Φ1 xs else f xs = false /\ Φ2 xs).
  2:{ hnf; intros xs Hxs; destruct (f xs); tauto. }
  now apply sublty_cases.
Qed.
Lemma submty_cases' {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ τ : mty ϕ) :
  (mty! fun xs => f xs = true  /\ Φ1 xs ⊢ σ ⊑ τ) ->
  (mty! fun xs => f xs = false /\ Φ2 xs ⊢ σ ⊑ τ) ->
  (mty! (fun xs => if f xs then Φ1 xs else Φ2 xs) ⊢ σ ⊑ τ).
Proof.
  intros ty1 ty2.
  eapply @submty_mono_Φ with (Φ := fun xs => if f xs then f xs = true /\ Φ1 xs else f xs = false /\ Φ2 xs).
  2:{ hnf; intros xs Hxs; destruct (f xs); tauto. }
  now apply submty_cases.
Qed.


Lemma sublty_cases_lt {ϕ} (Φ : constr ϕ) (X Y : idx ϕ) :
  forall A B, (lty! fun xs => X xs < Y xs /\ Φ xs ⊢ A ⊑ B) ->
         (lty! fun xs => X xs >= Y xs /\ Φ xs ⊢ A ⊑ B) ->
         (lty! Φ ⊢ A ⊑ B).
Proof.
  intros.
  apply @sublty_mono_Φ with (Φ := fun xs => if Nat.ltb (X xs) (Y xs) then X xs < Y xs /\ Φ xs else X xs >= Y xs /\ Φ xs).
  2:{ hnf; intros xs Hxs. destruct Nat.ltb eqn:E; split; eauto.
      - now apply Nat.ltb_lt in E.
      - now apply Nat.ltb_ge in E. }
  eapply sublty_cases; eauto.
Qed.
Lemma submty_cases_lt {ϕ} (Φ : constr ϕ) (X Y : idx ϕ) :
  forall σ τ, (mty! fun xs => X xs < Y xs /\ Φ xs ⊢ σ ⊑ τ) ->
         (mty! fun xs => X xs >= Y xs /\ Φ xs ⊢ σ ⊑ τ) ->
         (mty! Φ ⊢ σ ⊑ τ).
Proof.
  intros.
  apply @submty_mono_Φ with (Φ := fun xs => if Nat.ltb (X xs) (Y xs) then X xs < Y xs /\ Φ xs else X xs >= Y xs /\ Φ xs).
  2:{ hnf; intros xs Hxs. destruct Nat.ltb eqn:E; split; eauto.
      - now apply Nat.ltb_lt in E.
      - now apply Nat.ltb_ge in E. }
  eapply submty_cases; eauto.
Qed.


Lemma eqlty_if_no_choice {ϕ} (f : Vector.t nat ϕ -> bool) (A : lty ϕ) :
  lty_if f A A ≡l A.
Proof.
  eapply eqlty_mono_Φ.
  eapply eqlty_cases with (f0 := f) (Φ1 := fun xs => f xs = true) (Φ2 := fun xs => f xs = false).
  - etransitivity. eapply eqlty_if_left. 1,3:reflexivity. firstorder.
  - etransitivity. eapply eqlty_if_right. 1,3:reflexivity. firstorder.
  - hnf; intros xs _. now destruct f.
Qed.
Lemma eqmty_if_no_choice {ϕ} (f : Vector.t nat ϕ -> bool) (A : mty ϕ) :
  mty_if f A A ≡m A.
Proof.
  eapply eqmty_mono_Φ.
  eapply eqmty_cases with (f0 := f) (Φ1 := fun xs => f xs = true) (Φ2 := fun xs => f xs = false).
  - etransitivity. eapply eqmty_if_left. 1,3:reflexivity. firstorder.
  - etransitivity. eapply eqmty_if_right. 1,3:reflexivity. firstorder.
  - hnf; intros xs _. now destruct f.
Qed.

Lemma submty_if_cases {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ1 σ2 ρ1 ρ2 : mty ϕ) :
  mty! Φ1 ⊢ σ1 ⊑ ρ1 ->
  mty! Φ2 ⊢ σ2 ⊑ ρ2 ->
  mty_strip σ1 = mty_strip σ2 ->
  mty_strip ρ1 = mty_strip ρ2 ->
  mty! fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢
       mty_if f σ1 σ2 ⊑ mty_if f ρ1 ρ2.
Proof.
  intros Hsub1 Hsub2 Hstrip1 Hstrip2.
  eapply submty_cases'.
  - etransitivity.
    { eapply eqmty_submty. eapply eqmty_if_left.
      2:{ intros xs (Hxs1&Hxs2); exact Hxs1. }
      now rewrite Hstrip1.
    }
    transitivity ρ1.
    + eapply submty_mono_Φ; eauto. firstorder.
    + eapply eqmty_submty. symmetry. eapply eqmty_if_left; auto.
      intros xs (Hxs1&Hxs2); exact Hxs1.
  - etransitivity.
    { eapply eqmty_submty. eapply eqmty_if_right.
      2:{ intros xs (Hxs1&Hxs2); exact Hxs1. }
      now rewrite Hstrip1.
    }
    transitivity ρ2.
    + eapply submty_mono_Φ; eauto. firstorder.
    + eapply eqmty_submty. symmetry. eapply eqmty_if_right; auto.
      intros xs (Hxs1&Hxs2); exact Hxs1.
Qed.

Lemma sublty_if_cases {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ1 σ2 ρ1 ρ2 : lty ϕ) :
  lty! Φ1 ⊢ σ1 ⊑ ρ1 ->
  lty! Φ2 ⊢ σ2 ⊑ ρ2 ->
  lty_strip σ1 = lty_strip σ2 ->
  lty_strip ρ1 = lty_strip ρ2 ->
  lty! fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢
       lty_if f σ1 σ2 ⊑ lty_if f ρ1 ρ2.
Proof.
  intros Hsub1 Hsub2 Hstrip1 Hstrip2.
  eapply sublty_cases'.
  - etransitivity.
    { eapply eqlty_sublty. eapply eqlty_if_left.
      2:{ intros xs (Hxs1&Hxs2); exact Hxs1. }
      now rewrite Hstrip1.
    }
    transitivity ρ1.
    + eapply sublty_mono_Φ; eauto. firstorder.
    + eapply eqlty_sublty. symmetry. eapply eqlty_if_left; auto.
      intros xs (Hxs1&Hxs2); exact Hxs1.
  - etransitivity.
    { eapply eqlty_sublty. eapply eqlty_if_right.
      2:{ intros xs (Hxs1&Hxs2); exact Hxs1. }
      now rewrite Hstrip1.
    }
    transitivity ρ2.
    + eapply sublty_mono_Φ; eauto. firstorder.
    + eapply eqlty_sublty. symmetry. eapply eqlty_if_right; auto.
      intros xs (Hxs1&Hxs2); exact Hxs1.
Qed.

Lemma eqmty_if_cases {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ1 σ2 ρ1 ρ2 : mty ϕ) :
  mty! Φ1 ⊢ σ1 ≡ ρ1 ->
  mty! Φ2 ⊢ σ2 ≡ ρ2 ->
  mty_strip σ1 = mty_strip σ2 ->
  mty_strip ρ1 = mty_strip ρ2 ->
  mty! fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢
       mty_if f σ1 σ2 ≡ mty_if f ρ1 ρ2.
Proof. intros [? ?] [? ?] ? ?. split; eapply submty_if_cases; eauto. Qed.
Lemma eqlty_if_cases {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ1 σ2 ρ1 ρ2 : lty ϕ) :
  lty! Φ1 ⊢ σ1 ≡ ρ1 ->
  lty! Φ2 ⊢ σ2 ≡ ρ2 ->
  lty_strip σ1 = lty_strip σ2 ->
  lty_strip ρ1 = lty_strip ρ2 ->
  lty! fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢
       lty_if f σ1 σ2 ≡ lty_if f ρ1 ρ2.
Proof. intros [? ?] [? ?] ? ?. split; eapply sublty_if_cases; eauto. Qed.



(* Simpler version *)
Lemma submty_if_cases' {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ ρ1 ρ2 : mty ϕ) :
  mty! Φ1 ⊢ σ ⊑ ρ1 -> mty! Φ2 ⊢ σ ⊑ ρ2 ->
  mty_strip ρ1 = mty_strip ρ2 ->
  mty! fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢ σ ⊑ mty_if f ρ1 ρ2.
Proof.
  intros Hsub1 Hsub2 Hstrip.
  transitivity (mty_if f σ σ).
  { eapply submty_mono_Φ. eapply eqmty_if_no_choice. firstorder. }
  eapply submty_if_cases; eauto.
Qed.

(* In the other direction *)
Lemma submty_if_cases'' {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ ρ1 ρ2 : mty ϕ) :
  mty! Φ1 ⊢ ρ1 ⊑ σ -> mty! Φ2 ⊢ ρ2 ⊑ σ ->
  mty_strip ρ1 = mty_strip ρ2 ->
  mty! fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢ mty_if f ρ1 ρ2 ⊑ σ.
Proof.
  intros Hsub1 Hsub2 Hstrip.
  transitivity (mty_if f σ σ).
  2:{ eapply submty_mono_Φ. eapply eqmty_if_no_choice. firstorder. }
  eapply submty_if_cases; eauto.
Qed.

Lemma eqmty_if_cases' {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ ρ1 ρ2 : mty ϕ) :
  mty! Φ1 ⊢ σ ≡ ρ1 -> mty! Φ2 ⊢ σ ≡ ρ2 ->
  mty_strip ρ1 = mty_strip ρ2 ->
  mty! fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢ σ ≡ mty_if f ρ1 ρ2.
Proof.
  intros [? ?] [? ?] ?. split.
  - eapply submty_if_cases'; eauto.
  - eapply submty_if_cases''; eauto.
Qed.

Lemma eqmty_if_cases'' {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ ρ1 ρ2 : mty ϕ) :
  mty! Φ1 ⊢ σ ≡ ρ1 -> mty! Φ2 ⊢ σ ≡ ρ2 ->
  mty_strip ρ1 = mty_strip ρ2 ->
  mty! fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢ mty_if f ρ1 ρ2 ≡ σ.
Proof. intros. symmetry. eapply eqmty_if_cases'; eauto. Qed.


Lemma sublty_if_cases' {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ ρ1 ρ2 : lty ϕ) :
  lty! Φ1 ⊢ σ ⊑ ρ1 -> lty! Φ2 ⊢ σ ⊑ ρ2 ->
  lty_strip ρ1 = lty_strip ρ2 ->
  lty! fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢ σ ⊑ lty_if f ρ1 ρ2.
Proof.
  intros Hsub1 Hsub2 Hstrip.
  transitivity (lty_if f σ σ).
  { eapply sublty_mono_Φ. eapply eqlty_if_no_choice. firstorder. }
  eapply sublty_if_cases; eauto.
Qed.

(* In the other direction *)
Lemma sublty_if_cases'' {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ ρ1 ρ2 : lty ϕ) :
  lty! Φ1 ⊢ρ1 ⊑ σ -> lty! Φ2 ⊢ ρ2 ⊑ σ ->
  lty_strip ρ1 = lty_strip ρ2 -> lty! fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢
       lty_if f ρ1 ρ2 ⊑ σ.
Proof.
  intros Hsub1 Hsub2 Hstrip.
  transitivity (lty_if f σ σ).
  2:{ eapply sublty_mono_Φ. eapply eqlty_if_no_choice. firstorder. }
  eapply sublty_if_cases; eauto.
Qed.

Lemma eqlty_if_cases' {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ ρ1 ρ2 : lty ϕ) :
  lty! Φ1 ⊢ σ ≡ ρ1 -> lty! Φ2 ⊢ σ ≡ ρ2 ->
  lty_strip ρ1 = lty_strip ρ2 ->
  lty! fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢ σ ≡ lty_if f ρ1 ρ2.
Proof.
  intros [? ?] [? ?] ?. split.
  - eapply sublty_if_cases'; eauto.
  - eapply sublty_if_cases''; eauto.
Qed.

Lemma eqlty_if_cases'' {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ ρ1 ρ2 : lty ϕ) :
  lty! Φ1 ⊢ σ ≡ ρ1 -> lty! Φ2 ⊢ σ ≡ ρ2 ->
  lty_strip ρ1 = lty_strip ρ2 ->
  lty! fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢ lty_if f ρ1 ρ2 ≡ σ.
Proof. intros. symmetry. eapply eqlty_if_cases'; eauto. Qed.







(** *** Joining two bounded sums *)

(* TODO: Move *)
Lemma fext (X Y : Type) (f1 f2 : X -> Y) :
  (forall x, f1 x = f2 x) -> f1 = f2.
Proof. intros. now fext. Qed.
Lemma fext_inv (X Y : Type) (f1 f2 : X -> Y) :
  f1 = f2 -> (forall x, f1 x = f2 x).
Proof. intros ->. reflexivity. Qed.
Lemma fext_iff (X Y : Type) (f1 f2 : X -> Y) :
  f1 = f2 <-> (forall x, f1 x = f2 x).
Proof. split; eauto using fext, fext_inv. Qed.


(** As a corollary of [make_bsum_sig], we show that we can 'join' two bounded sums. *)
(** This can be seen as the converse of [bsum_split]. *)

Lemma bsum_join_Nat {ϕ} (I1 I2 : idx ϕ) (σ1 σ2 : mty (S ϕ)) (τ1 τ2 : mty ϕ) :
  bsum_Nat I1 σ1 τ1 ->
  bsum_Nat I2 σ2 τ2 ->
  mty_comp (fun xs => hd xs < I1 (tl xs) + I2 (tl xs)) σ1 σ2 ->
  existsS (σ : mty (S ϕ)) (τ : mty ϕ),
    (bsum_Nat (fun xs => I1 xs + I2 xs) σ τ) **
    (mty! (fun xs => hd xs < I1 (tl xs) + I2 (tl xs)) ⊢ mty_if (fun xs => hd xs <? I1 (tl xs)) σ1 (mty_shift_sub σ2 I1) ≡ σ).
Proof.
  intros H1 H2 Hcomp. hnf in H1,H2. destruct H1 as (J1&->&->); destruct H2 as (J2&->&->).
  cbn in Hcomp.
  eexists _, _. hnf.
  split.
  - eexists (fun xs => J1 xs).
    split. 2: reflexivity.
    reflexivity.
  - setoid_rewrite subst_mty_twice; unfold funcomp; cbn -[Nat.ltb].
    unfold shift_sub_fun.
    eapply eqmty_Nat.
    hnf in Hcomp|-*; intros xs Hxs.
    destruct (hd xs <? I1 (tl xs)).
    + reflexivity.
    + cbn. now rewrite Hcomp.
Qed.


Lemma mty_if_eq_Quant {ϕ} f (I1 I2 : idx ϕ) (A1 A2 : lty (S ϕ)) :
  mty_if f (Quant I1 A1) (Quant I2 A2) =
  Quant (fun xs => if f xs then I1 xs else I2 xs) (lty_if (fun xs => f (tl xs)) A1 A2).
Proof. reflexivity. Qed.


Lemma lty_shift_one_sub_0 {ϕ} (A : lty (S (S ϕ))) :
  lty_shift_one_sub A (iConst 0) = A.
Proof.
  setoid_rewrite <- subst_lty_id at 3. unfold lty_shift_one_sub, shift_one_sub_fun, iConst, id. f_equal.
  fext; intros f xs; f_equal.  rewrite eta. f_equal. rewrite eta. f_equal. lia.
Qed.



(** Replace the predicate with an equivialent predicate. *)
Lemma eqlty_if_f {ϕ} (Φ : constr ϕ) (f1 f2 : Vector.t nat ϕ -> bool) (A1 A2 : lty ϕ) :
  lty_strip A1 = lty_strip A2 ->
  (sem! Φ ⊨ fun xs => f1 xs = f2 xs) ->
  (lty! Φ ⊢ lty_if f1 A1 A2 ≡ lty_if f2 A1 A2).
Proof.
  intros Hstrip H.
  eapply eqlty_mono_Φ. apply eqlty_cases with (f := f1) (Φ1 := fun xs => f1 xs = true /\ Φ xs) (Φ2 := fun xs => f1 xs = false /\ Φ xs).
  - rewrite !eqlty_if_left; eauto.
    + reflexivity.
    + hnf in H|-*. intros xs (Hxs1&Hxs2). now rewrite <- H.
    + firstorder.
  - rewrite !eqlty_if_right; eauto.
    + reflexivity.
    + hnf in H|-*. intros xs (Hxs1&Hxs2). now rewrite <- H.
    + firstorder.
  - hnf in H|-*; intros xs Hxs. destruct f1; auto.
Qed.


Lemma eqlty_if_cases_f {ϕ} (f1 f2 : Vector.t nat ϕ -> bool) (Φ : constr ϕ) (σ1 σ2 ρ1 ρ2 : lty ϕ) :
  (sem! Φ ⊨ fun xs => f1 xs = f2 xs) ->
  lty! fun xs => f1 xs = true  /\ Φ xs ⊢ σ1 ≡ ρ1 ->
  lty! fun xs => f1 xs = false /\ Φ xs ⊢ σ2 ≡ ρ2 ->
  lty_strip σ1 = lty_strip σ2 ->
  lty_strip ρ1 = lty_strip ρ2 ->
  lty! Φ ⊢ lty_if f1 σ1 σ2 ≡ lty_if f2 ρ1 ρ2.
Proof.
  intros H1 H2 H3 H4 H5.
  rewrite eqlty_if_f; eauto.
  eapply eqlty_mono_Φ. eapply eqlty_if_cases; eauto.
  cbn. hnf; intros. rewrite <- H1 by assumption. now destruct f1.
Qed.


Lemma eqmty_if_f {ϕ} (Φ : constr ϕ) (f1 f2 : Vector.t nat ϕ -> bool) (A1 A2 : mty ϕ) :
  mty_strip A1 = mty_strip A2 ->
  (sem! Φ ⊨ fun xs => f1 xs = f2 xs) ->
  (mty! Φ ⊢ mty_if f1 A1 A2 ≡ mty_if f2 A1 A2).
Proof.
  intros Hstrip H.
  eapply eqmty_mono_Φ. apply eqmty_cases with (f := f1) (Φ1 := fun xs => f1 xs = true /\ Φ xs) (Φ2 := fun xs => f1 xs = false /\ Φ xs).
  - rewrite !eqmty_if_left; eauto.
    + reflexivity.
    + hnf in H|-*. intros xs (Hxs1&Hxs2). now rewrite <- H.
    + firstorder.
  - rewrite !eqmty_if_right; eauto.
    + reflexivity.
    + hnf in H|-*. intros xs (Hxs1&Hxs2). now rewrite <- H.
    + firstorder.
  - hnf in H|-*; intros xs Hxs. destruct f1; auto.
Qed.

Lemma eqmty_if_cases_f {ϕ} (f1 f2 : Vector.t nat ϕ -> bool) (Φ : constr ϕ) (σ1 σ2 ρ1 ρ2 : mty ϕ) :
  (sem! Φ ⊨ fun xs => f1 xs = f2 xs) ->
  mty! fun xs => f1 xs = true  /\ Φ xs ⊢ σ1 ≡ ρ1 ->
  mty! fun xs => f1 xs = false /\ Φ xs ⊢ σ2 ≡ ρ2 ->
  mty_strip σ1 = mty_strip σ2 ->
  mty_strip ρ1 = mty_strip ρ2 ->
  mty! Φ ⊢ mty_if f1 σ1 σ2 ≡ mty_if f2 ρ1 ρ2.
Proof.
  intros H1 H2 H3 H4 H5.
  rewrite eqmty_if_f; eauto.
  eapply eqmty_mono_Φ. eapply eqmty_if_cases; eauto.
  cbn. hnf; intros. rewrite <- H1 by assumption. now destruct f1.
Qed.



Lemma le_lt_plus (a b c d : nat) :
  a <= c ->
  b < d ->
  a + b < c + d.
Proof. intros. lia. Qed.


(* TODO: Move *)
Lemma Sum_monotone' (I : nat) (f1 f2 : nat -> nat) :
  (forall i, i < I -> f1 i <= f2 i) ->
  (Σ_{a < I} f1 a) <= Σ_{a < I} f2 a.
Proof.
  induction I as [ | I IH] in f1,f2|-* ; intros.
  - reflexivity.
  - rewrite !Sum_eq_S'.
    specialize (H 0 ltac:(lia)) as H'.
    enough ((Σ_{x < I} f1 (S x)) <= (Σ_{x < I} f2 (S x))) by lia.
    apply IH. intros i Hi. apply H. lia.
Qed.


Definition bsum_join_Quant {ϕ} (I1 I2 : idx ϕ) (σ1 σ2 : mty (S ϕ)) (τ1 τ2 : mty ϕ) : mty (S ϕ) * mty ϕ :=
  match σ1, τ1, σ2, τ2 with
  | Quant J1 _, Quant _ A1, Quant J2 _, Quant _ A2 =>
    (
      [ < fun xs : Vector.t nat (S ϕ) => if hd xs <? I1 (tl xs) then J1 xs else shift_sub_fun I1 J2 xs]
        ⋅ subst_lty_beta_two
        (lty_if
           (fun xs : Vector.t nat (S ϕ) => let c := hd xs in let xs' := tl xs in c <? Σ_{d < I1 xs'} J1 (d ::: xs'))
           A1
           (lty_shift_sub A2 (fun xs : Vector.t nat ϕ => Σ_{d < I1 xs} J1 (d ::: xs))))
        (fun xs : Vector.t nat (S (S ϕ)) =>
           let b := hd xs in
           let a := hd (tl xs) in
           let xs' := tl (tl xs) in
           b +
           (Σ_{d < a} if d <? I1 xs' then J1 (d ::: xs')
                      else shift_sub_fun I1 J2 (d ::: xs'))),
      [ < fun xs : Vector.t nat ϕ =>
            Σ_{a < I1 xs + I2 xs}
              (* A bit of abstraction here to help the proof below *)
              (fun xs0 : Vector.t nat (S ϕ) =>
                 if hd xs0 (* = a *) <? I1 (tl xs0) (* = xs *) then J1 xs0 else shift_sub_fun I1 J2 xs0)
              (a ::: xs)]
        ⋅ lty_if
        (fun xs : Vector.t nat (S ϕ) => let c := hd xs in let xs' := tl xs in c <? Σ_{d < I1 xs'} J1 (d ::: xs')) A1
        (lty_shift_sub A2 (fun xs : Vector.t nat ϕ => Σ_{d < I1 xs} J1 (d ::: xs)))
    )
  | _, _, _, _ => (def_mty, def_mty)
  end.

Lemma bsum_join_Quant_correct {ϕ} (I1 I2 : idx ϕ) (σ1 σ2 : mty (S ϕ)) (τ1 τ2 : mty ϕ)  :
  mty_strip σ1 = mty_strip σ2 ->
  bsum_Quant I1 σ1 τ1 ->
  bsum_Quant I2 σ2 τ2 ->
  let σ : mty (S ϕ) := fst (bsum_join_Quant I1 I2 σ1 σ2 τ1 τ2) in
  let τ : mty ϕ     := snd (bsum_join_Quant I1 I2 σ1 σ2 τ1 τ2) in
  bsum_Quant (fun xs => I1 xs + I2 xs) σ τ **
  (mty! fun xs => hd xs < I1 (tl xs) + I2 (tl xs) ⊢ (mty_if (fun xs => hd xs <? I1 (tl xs)) σ1 (mty_shift_sub σ2 I1)) ≡ σ).
Proof.
  intros Hstrip H1 H2; hnf in H1,H2.
  destruct H1 as (J1 & A1 & -> & ->); destruct H2 as (J2 & A2 & -> & ->).
  change (mty_shift_sub (Quant ?I ?A) ?i) with (Quant (shift_sub_fun i I) (lty_shift_one_sub A i)).
  rewrite mty_if_eq_Quant.
  replace
    (lty_shift_one_sub
       (subst_lty_beta_two
          A2
          (fun xs : Vector.t nat (S (S ϕ)) =>
             let b := hd xs in let a := hd (tl xs) in let xs' := tl (tl xs) in b + (Σ_{d < a} J2 (d ::: xs')))) I1)
    with (subst_lty_beta_two A2 (fun xs : Vector.t nat (S (S ϕ)) => hd xs + (Σ_{d < hd (tl xs) - I1 (tl (tl xs))} J2 (d ::: tl (tl xs)))))
    by now setoid_rewrite subst_lty_twice.
  cbn zeta. cbn in Hstrip. setoid_rewrite subst_lty_strip in Hstrip.

  split.
  - hnf. eexists _, _. split. 2: reflexivity. reflexivity.
  - eapply eqmty_Quant.
    2:{ hnf; reflexivity. }
    cbn beta zeta. cbn -[Nat.ltb].
    rewrite lty_if_subst_beta_two. cbn zeta; cbn -[Nat.ltb].
    2:{ try setoid_rewrite subst_lty_strip; auto. }

    eapply eqlty_if_cases_f.
    { (* The predicate *)
      unfold shift_sub_fun; cbn -[Nat.ltb]. hnf; intros xs (Hxs1&Hx2).
      set (b := hd xs) in *; set (a := hd (tl xs)) in *; set (xs' := tl (tl xs)) in *; cbn in a, b, xs'.
      destruct (a <? I1 xs') eqn:E.
      - apply Nat.ltb_lt in E. symmetry; apply Nat.ltb_lt.
        eapply Nat.lt_le_trans.
        2:{ rewrite <- Sum_monotone. 2: apply E. reflexivity. }
        rewrite Sum_eq_S.
        replace (a ::: xs') with (tl xs).
        2:{ subst xs' a. now rewrite <- eta. }
        rewrite Nat.add_comm.
        apply le_lt_plus; auto.
        apply Nat.eq_le_incl.
        apply Sum_ext. intros c Hc. rewrite (proj2 (Nat.ltb_lt _ _)) by lia. reflexivity.
      - apply Nat.ltb_ge in E. symmetry; apply Nat.ltb_ge.
        etransitivity.
        2:{
          apply plus_le_compat_l.
          rewrite <- Sum_monotone. 2: apply E. reflexivity. }
        rewrite Nat.add_comm. apply le_plus_trans.
        apply Sum_monotone'. intros c Hc. now rewrite (proj2 (Nat.ltb_lt _ _)) by lia.
    }
    { (* The first type *)
      unfold shift_sub_fun; cbn -[Nat.ltb].
      eapply eqlty_subst_idx_sem. hnf; intros xs (Hxs1&Hxs2&Hxs3).
      set (b := hd xs) in *; set (a := hd (tl xs)) in *; set (xs' := tl (tl xs)) in *; cbn in a, b, xs'.
      intros j; f_equal; clear j. f_equal. f_equal.
      rewrite Hxs1 in *. apply Nat.ltb_lt in Hxs1.
      apply Sum_ext. intros c Hc. rewrite (proj2 (Nat.ltb_lt _ _)) by lia. reflexivity.
    }
    { (* The second type *)
      setoid_rewrite subst_lty_twice; unfold funcomp; cbn -[Nat.ltb].
      unfold shift_add_fun, shift_sub_fun; cbn -[Nat.ltb].
      eapply eqlty_subst_idx_sem. hnf; intros xs (Hxs1&Hxs2&Hxs3).
      set (b := hd xs) in *; set (a := hd (tl xs)) in *; set (xs' := tl (tl xs)) in *; cbn in a, b, xs'.
      intros j; f_equal; clear j. f_equal.
      rewrite Hxs1 in *. apply Nat.ltb_ge in Hxs1.

      setoid_rewrite <- Sum_split at 2. 2: apply Hxs1.
      setoid_rewrite Sum_ext at 2.
      2:{ intros c Hc. rewrite (proj2 (Nat.ltb_lt _ _)) by assumption. reflexivity. }
      setoid_rewrite Sum_ext at 3.
      2:{ intros c Hc. rewrite (proj2 (Nat.ltb_ge _ _)) by lia. replace (I1 xs' + c - I1 xs') with c by lia. reflexivity. }
      lia.
    }
    { now setoid_rewrite subst_lty_strip. }
    { now repeat setoid_rewrite subst_lty_strip. }
Qed.


Lemma bsum_join {ϕ} (I1 I2 : idx ϕ) (σ1 σ2 : mty (S ϕ)) (τ1 τ2 : mty ϕ) :
  bsum I1 σ1 τ1 ->
  bsum I2 σ2 τ2 ->
  mty_comp (fun xs => hd xs < I1 (tl xs) + I2 (tl xs)) σ1 σ2 ->
  existsS (σ : mty (S ϕ)) (τ : mty ϕ),
    (mty! fun xs => hd xs < I1 (tl xs) + I2 (tl xs) ⊢ (mty_if (fun xs => hd xs <? I1 (tl xs)) σ1 (mty_shift_sub σ2 I1)) ≡ σ) **
    bsum (fun xs => I1 xs + I2 xs) σ τ.
Proof.
  intros [H1|H1] [H2|H2] H3.
  -
    hnf in H1,H2.
    pose proof (bsum_join_Nat H1 H2 H3) as (σ & τ & L1 & L2).
    eexists _, _. split; swap 1 2.
    + left; eauto.
    + eauto.
  - exfalso. hnf in H1,H2. destruct H1 as (?&->&->). destruct H2 as (?&?&->&->). contradiction H3.
  - exfalso. hnf in H1,H2. destruct H2 as (?&->&->). destruct H1 as (?&?&->&->). contradiction H3.
  - apply mty_comp_same_shape in H3.
    pose proof bsum_join_Quant_correct H3 H1 H2 as (X1 & X2).
    eexists _, _; split; hnf; eauto.
Qed.


Lemma eqmty_strip_commutes {ϕ} (Φ : constr ϕ) (σ1 σ2 τ1 τ2 : mty ϕ) :
  mty_strip σ1 = mty_strip τ1 ->
  (mty! Φ ⊢ σ1 ≡ σ2) ->
  (mty! Φ ⊢ τ1 ≡ τ2) ->
  mty_strip σ2 = mty_strip τ2.
Proof.
  intros H1 H2 H3.
  erewrite <- eqmty_strip by eauto.
  rewrite H1. now erewrite eqmty_strip by eauto.
Qed.


(** In this lemma, we first join two bounded sums. We have assumed that there are types that are equivalent to
the bounded sums. We build a binary sum over these types. Then we show that the binary sum is equivalent to the
joined bounded sum. *)

Lemma bsum_join_msum_Quant {ϕ} (Φ : constr ϕ) (I1 I2 : idx ϕ) (σ1 σ2 : mty (S ϕ)) (τ1 τ2 τ1' τ2' : mty ϕ) :
  mty_strip σ1 = mty_strip σ2 ->
  bsum_Quant I1 σ1 τ1 ->
  bsum_Quant I2 σ2 τ2 ->
  (mty! Φ ⊢ τ1 ≡ τ1') ->
  (mty! Φ ⊢ τ2 ≡ τ2') ->
  existsS (σ_bsum : mty (S ϕ)) (τ_bsum : mty ϕ) (τ1'_msum τ2'_msum τ_msum : mty ϕ),
    (bsum_Quant (fun xs => I1 xs + I2 xs) σ_bsum τ_bsum) **
    (mty! fun xs => hd xs < I1 (tl xs) + I2 (tl xs) ⊢ (mty_if (fun xs => hd xs <? I1 (tl xs)) σ1 (mty_shift_sub σ2 I1)) ≡ σ_bsum) **
    (msum τ1'_msum τ2'_msum τ_msum) **
    (τ1'_msum ≡m τ1') **
    (τ2'_msum ≡m τ2') **
    (mty! Φ ⊢ τ_bsum ≡ τ_msum).
Proof.
  intros H0 H1 H2 H3 H4.
  destruct σ1 as [? | J1 A1], σ2 as [? | J2 A2].
  1-3: exfalso; firstorder congruence.

  pose (J12 := fun xs : Vector.t nat (S ϕ) => if hd xs <? I1 (tl xs) then J1 xs else shift_sub_fun I1 J2 xs).
  pose (A12 := lty_if (fun xs : Vector.t nat (S (S ϕ)) => hd (tl xs) <? I1 (tl (tl xs))) A1 (lty_shift_one_sub A2 I1)).


  unshelve epose proof bsum_join_Quant_correct H0 H1 H2 as (X1&X2).
  set (σ_bsum := fst (bsum_join_Quant I1 I2 ([ < J1]⋅ A1) ([ < J2]⋅ A2) τ1 τ2)) in X1,X2.
  set (τ_bsum := snd (bsum_join_Quant I1 I2 ([ < J1]⋅ A1) ([ < J2]⋅ A2) τ1 τ2)) in X1,X2.


  unshelve epose proof make_msum_correct (fun xs => True) τ1' τ2' _ as Y.
  {
    destruct τ1, τ2; cbn in H1; try discriminate.
    1-3: exfalso; eapply bsum_Quant_not_Nat; eauto.
    hnf in H1, H2. destruct H1 as (? & D1 & [= <- ->] & [= -> <-]); destruct H2 as (? & D2 & [= <- ->] & [= -> <-]).
    apply eqmty_Quant_inv'_sig in H3 as (? & ? & -> & H3 & H3'). apply eqmty_Quant_inv'_sig in H4 as (? & ? & -> & H4 & H4').
    cbn in H0|-*. setoid_rewrite subst_lty_strip in H0.
    erewrite <- !eqlty_strip by eauto. symmetry. erewrite <- eqlty_strip by eauto. auto.
  }

  eexists _, τ_bsum.
  exists (fst (make_msum τ1' τ2')), (fst (snd (make_msum τ1' τ2'))), (snd (snd (make_msum τ1' τ2'))).
  repeat_split.
  - apply X1.
  - apply X2.
  - apply Y.
  - apply Y.
  - apply Y.
  - destruct τ1, τ2; cbn in H1,H2.
    1-3: now exfalso; eapply bsum_Quant_not_Nat; eauto.

    apply eqmty_Quant_inv'_sig in H3 as (j1 & B1 & -> & H3 & H3'); apply eqmty_Quant_inv'_sig in H4 as (j2 & B2 & -> & H4 & H4').
    hnf in H1,H2. destruct H1 as (? & ? & [= -> ->] & [= -> ->]). destruct H2 as (? & ? & [= -> ->] & [= -> ->]).
    cbn in H0. setoid_rewrite subst_lty_strip in H0.

    cbn -[Nat.ltb].
    apply eqmty_Quant.
    {
      apply eqlty_if_cases_f.
      { hnf; intros xs (Hxs1&Hxs2). now rewrite H3'. }
      { eapply eqlty_mono_Φ; eauto.
        hnf; intros xs (Hxs1&Hxs2&Hxs3); split; auto.
        apply Nat.ltb_lt in Hxs1. now rewrite <- H3' by auto. }
      {
        etransitivity.
        { eapply eqlty_mono_Φ. eapply eqlty_shift_sub; eauto.
          cbn -[Nat.ltb]. hnf; intros xs (Hxs1&Hxs2&Hxs3); split; auto.
          apply Nat.ltb_ge in Hxs1.
          rewrite <- !H3' in Hxs2 by auto; rewrite <- H4' in Hxs2|-* by auto. lia. }

        apply eqlty_subst_idx_sem.
        hnf; intros xs (Hxs1&Hxs2&Hxs3).
        unfold shift_sub_fun; cbn. intros j; f_equal; clear j.
        f_equal. f_equal. now rewrite <- H3'.
      }
      { now setoid_rewrite subst_lty_strip. }
      { setoid_rewrite subst_lty_strip.
        erewrite <- eqlty_strip by eauto. symmetry. erewrite <- eqlty_strip by eauto. symmetry. assumption. }
    }
    { hnf; intros xs Hxs.
      rewrite Sum_idx_plus.
      setoid_rewrite Sum_ext at 1.
      2:{ intros a Ha. rewrite (proj2 (Nat.ltb_lt _ _)) by assumption. reflexivity. }
      setoid_rewrite Sum_ext at 2.
      2:{ intros a Ha. rewrite (proj2 (Nat.ltb_ge _ _)) by lia.
          unfold shift_sub_fun; cbn. rewrite minus_plus. reflexivity. }
      now rewrite H3', H4' by assumption.
    }
Qed.


Lemma bsum_join_msum_Nat {ϕ} (Φ : constr ϕ) (I1 I2 : idx ϕ) (σ1 σ2 : mty (S ϕ)) (τ1 τ2 τ1' τ2' : mty ϕ) :
  mty_comp (fun xs => Φ (tl xs)) σ1 σ2 ->
  bsum_Nat I1 σ1 τ1 ->
  bsum_Nat I2 σ2 τ2 ->
  (mty! Φ ⊢ τ1 ≡ τ1') ->
  (mty! Φ ⊢ τ2 ≡ τ2') ->
  existsS (σ_bsum : mty (S ϕ)) (τ_bsum : mty ϕ) (τ1'_msum τ2'_msum τ_msum : mty ϕ),
    (bsum_Nat (fun xs => I1 xs + I2 xs) σ_bsum τ_bsum) **
    (mty! fun xs => hd xs < I1 (tl xs) + I2 (tl xs) /\ Φ (tl xs) ⊢ (mty_if (fun xs => hd xs <? I1 (tl xs)) σ1 (mty_shift_sub σ2 I1)) ≡ σ_bsum) **
    (msum τ1'_msum τ2'_msum τ_msum) **
    (mty! Φ ⊢ τ1'_msum ≡ τ1') **
    (mty! Φ ⊢ τ2'_msum ≡ τ2') **
    (mty! Φ ⊢ τ_bsum ≡ τ_msum).
Proof.
  intros H1 H2 H3 H4 H5.
  hnf in H2, H3. destruct H2 as (J1 & -> & ->); destruct H3 as (J2 & -> & ->).
  cbn in H1.
  eapply eqmty_Nat_inv'_sig in H4 as (J1' & -> & H4'); eapply eqmty_Nat_inv'_sig in H5 as (J2' & -> & H5').
  eexists (Nat (fun xs => J1 (tl xs))), (Nat J1), (Nat J1'), (Nat J1'), _.
  repeat_split.
  - hnf. eexists; split. 2: reflexivity. cbn. reflexivity.
  - cbn -[Nat.ltb]. eapply eqmty_Nat.
    hnf; intros xs (Hxs1&Hxs2). unfold shift_sub_fun. cbn -[Nat.ltb]. rewrite H1 by assumption. now destruct Nat.ltb.
  - apply msum_Nat'.
  - reflexivity.
  - apply eqmty_Nat. hnf in H4',H5'|-*. intros xs Hxs. rewrite H4', H5' by auto.
    setoid_rewrite <- (H1 (0 ::: _)); auto.
  - eapply eqmty_Nat. firstorder.
Qed.



Lemma bsum_join_msum' {ϕ} (Φ : constr ϕ) (I1 I2 : idx ϕ) (σ1 σ2 : mty (S ϕ)) (τ1 τ2 τ1' τ2' : mty ϕ) :
  mty_comp (fun xs => Φ (tl xs)) σ1 σ2 ->
  bsum I1 σ1 τ1 ->
  bsum I2 σ2 τ2 ->
  (mty! Φ ⊢ τ1 ≡ τ1') ->
  (mty! Φ ⊢ τ2 ≡ τ2') ->
  existsS (σ_bsum : mty (S ϕ)) (τ_bsum : mty ϕ) (τ1'_msum τ2'_msum τ_msum : mty ϕ),
    (bsum (fun xs => I1 xs + I2 xs) σ_bsum τ_bsum) **
    (mty! fun xs => hd xs < I1 (tl xs) + I2 (tl xs) /\ Φ (tl xs) ⊢ (mty_if (fun xs => hd xs <? I1 (tl xs)) σ1 (mty_shift_sub σ2 I1)) ≡ σ_bsum) **
    (msum τ1'_msum τ2'_msum τ_msum) **
    (mty! Φ ⊢ τ1'_msum ≡ τ1') **
    (mty! Φ ⊢ τ2'_msum ≡ τ2') **
    (mty! Φ ⊢ τ_bsum ≡ τ_msum).
Proof.
  intros H0 H1 H2 H3 H4.
  destruct H1 as [H1|H1], H2 as [H2|H2].
  - pose proof bsum_join_msum_Nat H0 H1 H2 H3 H4 as (σ_bsum & τ_bsum & τ1'_msum & τ2'_msum & τ_msum & X1 & X2 & X3 & X4 & X5 & X6).
    repeat_eexists; repeat_split; hnf; eauto.
  - exfalso. hnf in H1, H2. destruct H1 as (? & -> & ->). destruct H2 as (? & ? & -> & ->). contradiction.
  - exfalso. hnf in H1, H2. destruct H2 as (? & -> & ->). destruct H1 as (? & ? & -> & ->). contradiction.
  - apply mty_comp_same_shape in H0.
    pose proof bsum_join_msum_Quant H0 H1 H2 H3 H4 as (σ_bsum & τ_bsum & τ1'_msum & τ2'_msum & τ_msum & X1 & X2 & X3 & X4 & X5 & X6).
    repeat_eexists; repeat_split; hnf; eauto.
    + eapply eqmty_mono_Φ; eauto. firstorder.
    + eapply eqmty_mono_Φ; eauto. firstorder.
    + eapply eqmty_mono_Φ; eauto. firstorder.
Qed.


Lemma bsum_Nat_eqmty_comp {ϕ} (Φ : constr ϕ) (I1 I2 : idx ϕ) (σ1 σ2 : mty (S ϕ)) (τ1 τ2 : mty ϕ) :
  bsum_Nat I1 σ1 τ1 ->
  bsum_Nat I2 σ2 τ2 ->
  mty_comp Φ τ1 τ2 ->
  mty_comp (fun xs : Vector.t nat (S ϕ) => Φ (tl xs)) σ1 σ2.
Proof.
  intros H1 H2 H3; hnf in H1, H2.
  destruct H1 as (J1 & -> & ->); destruct H2 as (J2 & -> & ->).
  hnf in *. intros xs Hxs. specialize (H3 _ Hxs). congruence.
Qed.

Lemma bsum_Quant_eqmty_comp {ϕ} (Φ : constr ϕ) (I1 I2 : idx ϕ) (σ1 σ2 : mty (S ϕ)) (τ1 τ2 : mty ϕ) :
  bsum_Quant I1 σ1 τ1 ->
  bsum_Quant I2 σ2 τ2 ->
  mty_comp Φ τ1 τ2 ->
  mty_comp (fun xs : Vector.t nat (S ϕ) => Φ (tl xs)) σ1 σ2.
Proof.
  intros H1 H2 H3; hnf in H1, H2.
  destruct H1 as (J1 & A1 & -> & ->); destruct H2 as (J2 & A2 & -> & ->).
  hnf in *. cbn. now setoid_rewrite subst_lty_strip.
Qed.


Lemma bsum_eqmty_comp {ϕ} (Φ : constr ϕ) (I1 I2 : idx ϕ) (σ1 σ2 : mty (S ϕ)) (τ1 τ2 : mty ϕ) :
  bsum I1 σ1 τ1 ->
  bsum I2 σ2 τ2 ->
  mty_comp Φ τ1 τ2 ->
  mty_comp (fun xs : Vector.t nat (S ϕ) => Φ (tl xs)) σ1 σ2.
Proof.
  intros [H1|H1] [H2|H2] H3.
  - eapply bsum_Nat_eqmty_comp; eauto.
  - exfalso. hnf in H1, H2. destruct H1 as (J1 & -> & ->). destruct H2 as (? & ? & -> & ->). contradiction.
  - exfalso. hnf in H1, H2. destruct H2 as (J1 & -> & ->). destruct H1 as (? & ? & -> & ->). contradiction.
  - eapply bsum_Quant_eqmty_comp; eauto.
Qed.

Lemma eqmty_comp_commutes {ϕ} (Φ : constr ϕ) (σ1 σ2 τ1 τ2 : mty ϕ) :
  mty_comp Φ σ1 τ1 ->
  (mty! Φ ⊢ σ1 ≡ σ2) ->
  (mty! Φ ⊢ τ1 ≡ τ2) ->
  mty_comp Φ σ2 τ2.
Proof.
  destruct σ1, τ1; cbn; try tauto.
  - intros H1 H2 H3. apply eqmty_Nat_inv'_sig in H2 as (k1 & -> & H2); apply eqmty_Nat_inv'_sig in H3 as (k2 & -> & H3).
    cbn. hnf in H1, H2, H3|-*. firstorder.
  - intros H1 H2 H3. apply eqmty_Quant_inv'_sig in H2 as (?&?&->&?&?). apply eqmty_Quant_inv'_sig in H3 as (?&?&->&?&?).
    cbn. erewrite <- eqlty_strip by eauto. rewrite H1. now erewrite eqlty_strip by eauto.
Qed.

Lemma bsum_join_msum {ϕ} (Φ : constr ϕ) (I1 I2 : idx ϕ) (σ1 σ2 : mty (S ϕ)) (τ1 τ2 τ1' τ2' : mty ϕ) :
  bsum I1 σ1 τ1 ->
  bsum I2 σ2 τ2 ->
  (mty! Φ ⊢ τ1 ≡ τ1') ->
  (mty! Φ ⊢ τ2 ≡ τ2') ->
  mty_comp Φ τ1' τ2' ->
  existsS (σ_bsum : mty (S ϕ)) (τ_bsum : mty ϕ) (τ1'_msum τ2'_msum τ_msum : mty ϕ),
    (bsum (fun xs => I1 xs + I2 xs) σ_bsum τ_bsum) **
    (mty! fun xs => hd xs < I1 (tl xs) + I2 (tl xs) /\ Φ (tl xs) ⊢ (mty_if (fun xs => hd xs <? I1 (tl xs)) σ1 (mty_shift_sub σ2 I1)) ≡ σ_bsum) **
    (msum τ1'_msum τ2'_msum τ_msum) **
    (mty! Φ ⊢ τ1'_msum ≡ τ1') **
    (mty! Φ ⊢ τ2'_msum ≡ τ2') **
    (mty! Φ ⊢ τ_bsum ≡ τ_msum).
Proof.
  intros H1 H2 H3 H4 H5. eapply bsum_join_msum'; eauto.
  eapply bsum_eqmty_comp; eauto.
  eapply eqmty_comp_commutes; eauto. all: now symmetry.
Qed.
