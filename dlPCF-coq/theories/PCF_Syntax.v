(** ** PCI Syntax *)

Require Export Common.
Require Export fext.


(** Terms *)
Inductive tm  : Type :=
| Var : nat -> tm 
| Lam : tm -> tm 
| Fix : tm -> tm 
| App : tm -> tm -> tm 
| Ifz : tm -> tm -> tm -> tm 
| Const : nat -> tm 
| Pred : tm -> tm 
| Succ : tm -> tm.


(** Equality decider *)
Lemma tm_eq_dec (t1 t2 : tm) :
  {t1 = t2} + {t1 <> t2}.
Proof. decide equality; apply Nat.eq_dec. Defined.


(** Size of a term *)
Fixpoint size (t : tm) : nat :=
  match t with
  | Var x => 1
  | Lam t => S (size t)
  | Fix t => S (S (size t))
  | App t1 t2 => S (size t1 + size t2)
  | Ifz t1 t2 t3 => S (size t1 + size t2 + size t3)
  | Const k => 1
  | Pred t => S (size t)
  | Succ t => S (size t)
  end.

Lemma size_ge1 t :
  1 <= size t.
Proof. induction t; cbn in *; omega. Qed.


(** *** Notations *)


Declare Scope PCF.
Delimit Scope PCF with PCF.
Global Open Scope PCF.


Coercion App : tm >-> Funclass.

Definition vzero  : tm := Eval cbv in Var 0.
Definition vone   : tm := Eval cbv in Var 1.
Definition vtwo   : tm := Eval cbv in Var 2.
Definition vthree : tm := Eval cbv in Var 3.


(** *** Values and numbers *)

Inductive val : tm -> Prop :=
| val_lam t : val (Lam t)
| val_fix t : val (Fix t)
| val_const k : val (Const k)
.

Ltac isVal :=
  lazymatch goal with
  | [ H: val ?x |- val ?x ] => assumption
  | [ |- val (Lam ?t) ] => constructor
  | [ |- val (Fix ?t) ] => constructor
  | [ |- val (Const ?k) ] => constructor
  end.


Notation Zero  := (Const 0).
Notation One   := (Const 1).
Notation Two   := (Const 2).
Notation Three := (Const 3).


(** Reflection for values *)

Fixpoint valb (t : tm) : bool :=
  match t with
  | Lam t => true
  | Fix t => true
  | Const k => true
  | _ => false
  end.

Lemma valb_iff t :
  val t <-> valb t = true.
Proof.
  split.
  - induction t; intros; cbn in *; try now (inv H; auto; inv H0).
  - induction t; intros; cbn in *; inv H; repeat constructor; auto.
Qed.

Lemma valb_true t :
  val t -> valb t = true.
Proof. now intros H % valb_iff. Qed.

Lemma valb_false t :
  ~ val t -> valb t = false.
Proof.
  intros. destruct (valb t) eqn:E; auto.
  apply valb_iff in E. tauto.
Qed.


Lemma val_inv (t : tm) :
  val t -> (exists t', t = Lam t') \/ (exists t', t = Fix t') \/ (exists k, t = Const k).
Proof.
  intros H. induction H as [ | | ]; eauto.
Qed.

Lemma val_not_app (t1 t2 : tm) :
  ~ val (t1 t2).
Proof. inversion 1; subst. Qed.



(** *** Closedness *)


Fixpoint bound (m : nat) (t : tm) : Prop :=
  match t with
  | Var x => x < m
  | Lam t => bound (S m) t
  | Fix t => bound (S (S m)) t
  | App t1 t2 => bound m t1 /\ bound m t2
  | Ifz t1 t2 t3 => bound m t1 /\ bound m t2 /\ bound m t3
  | Const k => True
  | Pred t => bound m t
  | Succ t => bound m t
  end.

Definition closed (t : tm) : Prop := bound 0 t.

Lemma bound_var_inv (x : nat) m :
  bound m (Var x) -> x < m.
Proof. intros H. now cbn in H. Qed.

Lemma closed_var_inv (x : nat) :
  ~ closed (Var x).
Proof. intros H. hnf in H. omega. Qed.

Lemma bound_app_inv (t1 t2 : tm) m :
  bound m (t1 t2) -> bound m t1 /\ bound m t2.
Proof. now cbn. Qed.

Lemma closed_app_inv (t1 t2 : tm) :
  closed (t1 t2) -> closed t1 /\ closed t2.
Proof. now cbn. Qed.

Lemma bound_ifz_inv (t1 t2 t3 : tm) m :
  bound m (Ifz t1 t2 t3) -> bound m t1 /\ bound m t2 /\ bound m t3.
Proof. now cbn. Qed.

Lemma closed_ifz_inv (t1 t2 t3 : tm) :
  closed (Ifz t1 t2 t3) -> closed t1 /\ closed t2 /\ closed t3.
Proof. unfold closed. now cbn. Qed.

Lemma bound_p_inv (t : tm) m :
  bound m (Pred t) -> bound m t.
Proof. now cbn. Qed.

Lemma closed_p_inv (t : tm) :
  closed (Pred t) -> closed t.
Proof. now cbn. Qed.

Lemma bound_s_inv (t : tm) m :
  bound m (Succ t) -> bound m t.
Proof. now cbn. Qed.

Lemma closed_s_inv (t : tm) :
  closed (Succ t) -> closed t.
Proof. now cbn. Qed.

Lemma bound_Const k m :
  bound m (Const k).
Proof. now cbn. Qed.

Lemma closed_Const k :
  closed (Const k).
Proof. now cbn. Qed.

Lemma bound_s (t : tm) m :
  bound m t -> bound m (Succ t).
Proof. now cbn. Qed.

Lemma closed_s (t : tm) :
  closed t -> closed (Succ t).
Proof. now cbn. Qed.

Lemma bound_p (t : tm) m :
  bound m t -> bound m (Pred t).
Proof. now cbn. Qed.

Lemma closed_p (t : tm) :
  closed t -> closed (Pred t).
Proof. now cbn. Qed.

Lemma bound_ifz (t1 t2 t3 : tm) m :
  bound m t1 -> bound m t2 -> bound m t3 -> bound m (Ifz t1 t2 t3).
Proof. now cbn. Qed.

Lemma closed_ifz (t1 t2 t3 : tm) :
  closed t1 -> closed t2 -> closed t3 -> closed (Ifz t1 t2 t3).
Proof. now cbn. Qed.

Lemma bound_app (t1 t2 : tm) m :
  bound m t1 -> bound m t2 -> bound m (t1 t2).
Proof. now cbn. Qed.

Lemma closed_app (t1 t2 : tm) :
  closed t1 -> closed t2 -> closed (t1 t2).
Proof. now cbn. Qed.

Ltac bound_inv :=
  repeat lazymatch goal with
         | [ H : bound ?m (Var   _) |- _ ] => apply bound_var_inv in H
         | [ H : bound ?m (App   _ _ ) |- _ ] => let H2 := fresh H in apply bound_app_inv in H as (H&H2)
         | [ H : bound ?m (Ifz _ _ _ ) |- _ ] => let H2 := fresh H in let H3 := fresh H in apply bound_ifz_inv in H as (H&H2&H3)
         | [ H : bound ?m (Succ    _ ) |- _ ] => apply bound_s_inv in H
         | [ H : bound ?m (Pred    _ ) |- _ ] => apply bound_p_inv in H
         end.

Ltac isBound :=
  repeat lazymatch goal with
         | [ |- bound ?m (App   _ _ ) ] => apply bound_app
         | [ |- bound ?m (Ifz _ _ _ ) ] => apply bound_ifz
         | [ |- bound ?m (Succ    _ ) ] => apply bound_s
         | [ |- bound ?m (Pred    _ ) ] => apply bound_p
         | [ |- bound ?m (Const ?k  ) ] => apply bound_Const
         end.

Ltac closed_inv :=
  repeat lazymatch goal with
         | [ H : closed (Var   _) |- _ ] => contradict (@closed_var_inv _ H)
         | [ H : closed (App   _ _ ) |- _ ] => let H2 := fresh H in apply closed_app_inv in H as (H&H2)
         | [ H : closed (Ifz _ _ _ ) |- _ ] => let H2 := fresh H in let H3 := fresh H in apply closed_ifz_inv in H as (H&H2&H3)
         | [ H : closed (Succ    _ ) |- _ ] => apply closed_s_inv in H
         | [ H : closed (Pred    _ ) |- _ ] => apply closed_p_inv in H
         end.

Ltac isClosed :=
  repeat lazymatch goal with
         | [ |- closed (App   _ _ ) ] => apply closed_app
         | [ |- closed (Ifz _ _ _ ) ] => apply closed_ifz
         | [ |- closed (Succ    _ ) ] => apply closed_s
         | [ |- closed (Pred    _ ) ] => apply closed_p
         | [ |- closed (Const ?k  ) ] => apply closed_Const
         end.


Lemma bound_monotone m1 m2 t :
  bound m1 t ->
  m1 <= m2 ->
  bound m2 t.
Proof. induction t in m1,m2|-*; intros; cbn in *; firstorder. Qed.


(** *** Very similar notion: The number of free variables *)

(** [bound m t] says that [m] is an upper bound for the number of free variables. [maxVar t] computes the
greatest free variable (plus one). With other words, [maxVar t] is equal to the greatest [m] such that
[bound m t]. *)

Fixpoint maxVar (t : tm) : nat :=
  match t with
  | Var x => S x
  | Lam t => pred (maxVar t)
  | Fix t => pred (pred (maxVar t))
  | App t1 t2 => max (maxVar t1) (maxVar t2)
  | Ifz t1 t2 t3 => max (max (maxVar t1) (maxVar t2)) (maxVar t3)
  | Const k => 0
  | Pred t => maxVar t
  | Succ t => maxVar t
  end.

Lemma maxVar_bound (t : tm) :
  bound (maxVar t) t.
Proof.
  induction t; cbn.
  all: solve [lia
             |tauto
             |repeat_split; eapply bound_monotone; eauto; lia
             ].
Qed.

Lemma maxVar_bound_le (t : tm) (m : nat) :
  maxVar t <= m ->
  bound m t.
Proof. eauto using bound_monotone, maxVar_bound. Qed.

Lemma maxVar_bound_glb (t : tm) (m : nat) :
  bound m t ->
  maxVar t <= m.
Proof.
  induction t in m|-*; intros; cbn in *; bound_inv.
  all: try lia.
  - specialize IHt with (1 := H). lia.
  - specialize IHt with (1 := H). lia.
  - specialize IHt1 with (1 := (proj1 H)). specialize IHt2 with (1 := (proj2 H)). lia.
  - specialize IHt1 with (1 := (proj1 H)). specialize IHt2 with (1 := proj1 (proj2 H)). specialize IHt3 with (1 := proj2 (proj2 H)). lia.
  - specialize IHt with (1 := H). lia.
  - specialize IHt with (1 := H). lia.
Qed.

Lemma maxVar_closed (t : tm) :
  maxVar t = 0 ->
  closed t.
Proof. intros H. hnf. rewrite <- H. apply maxVar_bound. Qed.

Lemma maxVar_closed': forall t : tm, closed t -> maxVar t = 0.
Proof. intros. enough (maxVar t <= 0) by lia. now apply maxVar_bound_glb. Qed.

Lemma maxVar_closed_iff: forall t : tm, closed t <-> maxVar t = 0.
Proof. split. apply maxVar_closed'. apply maxVar_closed. Qed.


(** Yet another notion... *)

Fixpoint free (n : nat) (t : tm) : Prop :=
  match t with
  | Var x => n = x
  | Lam t => free (S n) t
  | Fix t => free (S (S n)) t
  | App t1 t2 => free n t1 \/ free n t2
  | Ifz t1 t2 t3 => free n t1 \/ free n t2 \/ free n t3
  | Const k => False
  | Succ t => free n t
  | Pred t => free n t
  end.

Goal free 0 (Lam vone). reflexivity. Qed.

Lemma free_bound_iff (t : tm) (m : nat) :
  bound m t <-> (forall x, free x t -> x < m).
Proof.
  split.
  - induction t in m|-*; intros; cbn in *; firstorder.
  - induction t in m|-*; intros; cbn in *.
    all: try now intuition.
    + apply IHt. intros [ | x] Hx. lia. apply lt_n_S. auto.
    + apply IHt. intros [ | [ | x ]] Hx. lia. lia. apply lt_n_S, lt_n_S. auto.
Qed.

Lemma free_closed_iff (t : tm) :
  closed t <-> (forall x, ~ free x t).
Proof. unfold closed. rewrite free_bound_iff. firstorder. Qed.

(* Reflection *)

Fixpoint freeb (n : nat) (t : tm) : bool :=
  match t with
  | Var x => n =? x
  | Lam t => freeb (S n) t
  | Fix t => freeb (S (S n)) t
  | App t1 t2 => freeb n t1 || freeb n t2
  | Ifz t1 t2 t3 => freeb n t1 || freeb n t2 || freeb n t3
  | Const k => false
  | Succ t => freeb n t
  | Pred t => freeb n t
  end.

Lemma free_iff n t :
  free n t <-> freeb n t = true.
Proof.
  induction t in n|-*; cbn in *.
  1: now rewrite Nat.eqb_eq.
  1-2,5-7: now firstorder.
  1-2: rewrite !Bool.orb_true_iff.
  - firstorder.
  - specialize (IHt1 n); specialize (IHt2 n); specialize (IHt3 n). firstorder.
Qed.

Lemma free_dec n t :
  { free n t } + { ~ free n t }.
Proof.
  destruct (freeb n t) eqn:E.
  - left. now apply free_iff.
  - right. intros H % free_iff. congruence.
Qed.



(** *** Contexts *)

(** We model the context as a function and assume extensionality. This is the easiest way to handle contexts and is supported by autosubst2. *)

Definition ctx {ty : Type} := (nat -> ty).

Definition scons {ty : Type} (τ : ty) (Γ : @ctx ty) : @ctx ty :=
  fun x => match x with
        | 0 => τ
        | S x' => Γ x'
        end.
Notation "s .: sigma" := (scons s sigma) (at level 70).

(** Function composition *)
Definition funcomp {X Y Z} (g : Y -> Z) (f : X -> Y) := fun x => g (f x).

Notation "f >> g" := (funcomp g f) (*fun x => g (f x)*) (at level 50).

Lemma scons_eta {ty : Type} (Γ : @ctx ty) :
  Γ = scons (Γ 0) (S >> Γ).
Proof. fext. intros [ | x]; reflexivity. Qed.



(** *** Example terms *)


(** Simple identity function [λx. x] *)
Example example_identity : tm := Lam vzero.
Example closed_example_identity : closed example_identity.
Proof. hnf. reflexivity. Qed.

(** Simple diverging function [fix f x. f x] *)
Example example_divfun : tm := Fix (App vone vzero).
Example example_div : tm := example_divfun Zero.

Example closed_example_divfun : closed example_divfun.
Proof. hnf. cbn. omega. Qed.
Example closed_example_div: closed example_div.
Proof. apply closed_app. apply closed_example_divfun. constructor. Qed.


(** Type a simple recursive function (always returning 0) *)
Example example_term  : tm := Fix (Ifz vzero (Zero) ((vone) (Pred vzero))).
Example example_term' : tm := Eval cbn in example_term (Const 1).

Example closed_example : closed example_term.
Proof.
  (* [isClosed] doesn't work here because of the fix *)
  unfold example_term. hnf. cbn. repeat constructor.
Qed.

Example closed_example' : closed example_term'.
Proof. unfold example_term'. repeat isClosed. apply closed_example. Qed.

(* My favourite function... *)
(** fix f g. λ x. Ifz x then g 0 else f (λy. S (g y)) (p x) *)
Example example_closure_building_identity : tm :=
  Fix (Lam (Ifz vzero (vone Zero) (vtwo (Lam (Succ (vtwo vzero))) (Pred vzero)))).

Example example_closure_building_identity' : tm :=
  example_closure_building_identity example_identity One.

Example closed_example_closure_building_identity :
  closed example_closure_building_identity.
Proof. unfold example_closure_building_identity. cbn. cbn. repeat split; omega. Qed.

Example closed_example_closure_building_identity' :
  closed example_closure_building_identity'.
Proof. repeat apply closed_app. apply closed_example_closure_building_identity. apply closed_example_identity. constructor. Qed.
