Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax.
Require Import dlPCF_iSubst.
Require Import ForestCard dlPCF_Types dlPCF_Sum dlPCF_Contexts dlPCF_Typing.
Require Import dlPCF_TypingT.
Require Import dlPCF_MakeSum.
Require PCF_Types.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.
Import SigmaTypeNotations.
Open Scope vector_scope.

From Coq Require Import Vector Fin.

Open Scope PCF.

Implicit Types (ϕ : nat).

Require Import dlPCF_Safety.

Import Skel.


(** ** Parametric splitting *)


(** *** Key typing lemma *)


(** The key lemma for parametric joining, [tyT_sum] says the following:

Let Γ, M, ρ have [c] as a variable.
Let [θ := {b + Σ_{d < a} J{d/a} / c}] be a substitution that creates the variables [a] and [b].
Assume the typing [a < I(b) /\ b < J /\ Φ θ; Γ θ ⊢(M θ) t : ρ : θ].
Then we can derive [c < Σ_{d < I} J(d) /\ Φ; Γ ⊢(M) t : ρ].
*)


(** Note that this function is used in the definition of [bsum_Quant]. *)

Definition subst_bsum_fun {ϕ} (I : idx ϕ) (J : idx (S ϕ)) : idx (S (S ϕ)) :=
  fun xs : Vector.t nat (S (S ϕ)) =>
    let b := hd xs in
    let a := hd (tl xs) in
    let xs' := tl (tl xs) in
    b + (Σ_{d < a} J (d ::: xs')).


(** **** Key subtyping lemma *)

(** We first show the case for subtyping *)



(** A different use for [find_Sum_slot] *)
Lemma find_Sum_slot_correct2 (i : nat) (f : nat -> nat) (a b c : nat) :
  c < (Σ_{d < i} f d) ->
  find_Sum_slot i f c = (a, b) ->
  c = b + (Σ_{d < a} f d) /\
  a < i /\ b < f a.
Proof.
  induction i as [ | i IH] in f,a,b,c|-*.
  - cbn. lia.
  - rewrite Sum_eq_S'. cbn [find_Sum_slot].
    destruct lt_dec as [Hlt|Hge].
    + intros Hc [= <- ->]. cbn. repeat_split; auto. lia.
    + intros Hc. destruct (find_Sum_slot i (fun n : nat => f (S n)) (c - f 0)) as [a' b'] eqn:E.
      intros [= <- ->].
      specialize IH with (2 := E); spec_assert IH as (IH1 & IH2 & IH3) by lia.
      repeat_split.
      * rewrite Sum_eq_S'. lia.
      * lia.
      * lia.
Qed.

Definition idx_find_slot_correct2 {ϕ} (I : idx ϕ) (J : idx (S ϕ)) :
  forall c (xs : Vector.t nat ϕ),
    c < (Σ_{d < I xs} J (d ::: xs)) ->
    let '(a, b) := idx_find_slot I J (c ::: xs) in
    c = b + (Σ_{d < a} J (d ::: xs)) /\
    a < I xs /\ b < J (a ::: xs).
Proof.
  cbn; intros * Hc. unfold idx_find_slot.
  destruct find_Sum_slot eqn:E; now apply find_Sum_slot_correct2.
Qed.




(** Inverse substitution for the substitution in the key typing lemma *)

Definition subst_bsum_inv_fun {X : Type} {ϕ} (I : idx ϕ) (J : idx (S ϕ)) : (Vector.t nat (S (S ϕ)) -> X) -> (Vector.t nat (S ϕ) -> X) :=
  fun (g : Vector.t nat (S (S ϕ)) -> X) (xs : Vector.t nat (S ϕ)) =>
    let f := idx_find_slot I J in
    let c := hd xs in
    let xs' := tl xs in
    g (snd (f (c ::: xs')) ::: (fst (f (c ::: xs'))) ::: xs').


(** We can express this function using combinators. Note that we need [subst_clone_fun] because we use the fresh variable [c] twice. *)

Require Import dlPCF_CloneVarT.

Definition subst_bsum_inv_fun' {X : Type} {ϕ} (I : idx ϕ) (J : idx (S ϕ)) : (Vector.t nat (S (S ϕ)) -> X) -> (Vector.t nat (S ϕ) -> X) :=
  (* First replace [a] and introduce [c] *)
  let f := idx_find_slot I J in
  subst_beta_one_fun
    1
    (fun xs : Vector.t nat (S ϕ) =>
       let c' := hd xs in
       let xs' := tl xs in
       fst (f (c' ::: xs')))
    >>
  (* Now replace [a] and introduce [c'] *)
  subst_beta_fun
    1
    (fun xs : Vector.t nat (S (S ϕ)) =>
       let c := hd xs in
       let a := hd (tl xs) in
       let xs' := tl (tl xs) in
       snd (f (c ::: xs')))
    >>
  (* Finally, replace [c'] with [c] *)
  subst_clone_fun.

Lemma subst_bsum_inv_fun_correct {X : Type} {ϕ} (I : idx ϕ) (J : idx (S ϕ)) :
  subst_bsum_inv_fun (X := X) I J =
  subst_bsum_inv_fun' I J.
Proof. reflexivity. Qed.


Lemma tyT_subst_beta {ϕ} (y : nat) (i : idx (y + ϕ)) (Φ : constr (S ϕ)) (Γ : ctx (S ϕ)) (M : idx (S ϕ)) (t : tm) (τ : mty (S ϕ)) (s : skel) :
  (Ty! Φ; Γ ⊢(M) t : τ @ s) ->
  (Ty! subst_beta_fun y i Φ; subst_ctx Γ (subst_beta_fun y i) ⊢(subst_beta_fun y i M) t : subst_mty_beta y i τ @ s).
Proof. intros H. unfold subst_mty_beta. rewrite !subst_beta_fun_correct. now apply tyT_subst_var_beta. Qed.

Lemma tyT_subst_beta_one {ϕ} (y : nat) (i : idx (y + ϕ)) (Φ : constr (S (S ϕ))) (Γ : ctx (S (S ϕ))) (M : idx (S (S ϕ))) (t : tm) (τ : mty (S (S ϕ))) (s : skel) :
  (Ty! Φ; Γ ⊢(M) t : τ @ s) ->
  (Ty! subst_beta_one_fun y i Φ; subst_ctx Γ (subst_beta_one_fun y i) ⊢(subst_beta_one_fun y i M) t : subst_mty τ (subst_beta_one_fun y i)  @ s).
Proof.
  intros H.
  assert (y + S ϕ = S (y + ϕ)) as aux by lia.
  eapply tyT_sub. eapply @tyT_weaken_Φ.
  - eapply tyT_subst_var_beta with (x := Fin1) (y0 := y) (I := i) in H.
    unshelve eapply @tyT_cast with (ϕ2 := S y + ϕ) in H; auto.
    apply H.
  - erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0; cbn.
    hnf; intros xs Hxs.
    unfold constr_cast, subst_beta_one_fun, subst_beta_fun in *; cbn in *.
    eapply p_equal; eauto.
    simp_vect_to_list. f_equal.
    + f_equal. now simp_vect_to_list.
    + f_equal. f_equal. now simp_vect_to_list.
  - hnf; intros var Hvar. apply eqmty_eq. unfold subst_ctx, ctx_cast, subst_ctx_var_beta; cbn.
    setoid_rewrite subst_mty_cast'.
    unfold subst_beta_one_fun, idx_cast, subst_beta_fun; cbn.
    erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0; cbn.
    unfold subst_beta_one_fun, idx_cast, subst_beta_fun; cbn.
    f_equal; fext; intros f xs. f_equal. simp_vect_to_list. f_equal.
    + f_equal. now simp_vect_to_list.
    + f_equal. f_equal. now simp_vect_to_list.
  - apply eqmty_eq.
    setoid_rewrite subst_mty_cast'.
    unfold subst_beta_one_fun, idx_cast, subst_beta_fun; cbn.
    erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0; cbn.
    unfold subst_beta_one_fun, idx_cast, subst_beta_fun; cbn.
    f_equal; fext; intros f xs. f_equal. simp_vect_to_list. f_equal.
    + f_equal. now simp_vect_to_list.
    + f_equal. f_equal. now simp_vect_to_list.
  - unfold idx_cast.
    hnf; intros xs Hxs. unfold subst_beta_one_fun in Hxs.
    erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0; unfold subst_beta_fun, subst_beta_one_fun; cbn.
    f_equal. simp_vect_to_list. f_equal.
    + f_equal. now simp_vect_to_list.
    + f_equal. f_equal. now simp_vect_to_list.
  Unshelve. all: auto.
Qed.

(* TODO: Move... *)
Lemma eqlty_subst_beta_one {ϕ} (y : nat) (Φ : constr (S (S ϕ))) (A B : lty (S (S ϕ))) (i : idx (y + ϕ)) :
  (lty! Φ ⊢ A ≡ B) ->
  (lty! subst_beta_one_fun y i Φ ⊢ dlPCF_Splitting.subst_lty_beta_one y i A ≡ dlPCF_Splitting.subst_lty_beta_one y i B).
Proof. now intros [H1 H2]; split; apply dlPCF_Splitting.sublty_subst_beta_one. Qed.

Lemma subst_bsum_inv_correct {X : Type} {ϕ} (Φ : constr (S ϕ)) (I : idx ϕ) (J : idx (S ϕ)) :
  sem! fun xs : Vector.t nat (S ϕ) => hd xs < (Σ_{d < I (tl xs)} J (d ::: tl xs)) /\ Φ xs ⊨
       fun (xs : Vector.t nat (S ϕ)) =>
         forall (M : Vector.t nat (S ϕ) -> X),
           subst_bsum_inv_fun (X := X) I J (fun ys : Vector.t nat (S (S ϕ)) => M (subst_bsum_fun I J ys ::: tl (tl ys))) xs = M xs.
Proof.
  intros xs (Hxs1 & Hxs2) M.
  unfold subst_bsum_inv_fun; cbn.
  f_equal.
  pose proof (idx_find_slot_correct2 I J) as Hf.
  rewrite <- eta. specialize Hf with (1 := Hxs1). rewrite <- eta in Hf.
  destruct (idx_find_slot I J xs) as [a' b'] eqn:Eab'; cbn.
  destruct Hf as (Hf1 & Hf2 & Hf3).
  now rewrite <- Hf1, <- eta.
Qed.

Lemma subst_bsum_inv_correct' {X : Type} {ϕ} (Φ : constr (S ϕ)) (I : idx ϕ) (J : idx (S ϕ)) :
  forall (M : Vector.t nat (S ϕ) -> X),
  sem! fun xs : Vector.t nat (S ϕ) => hd xs < (Σ_{d < I (tl xs)} J (d ::: tl xs)) /\ Φ xs ⊨
       fun (xs : Vector.t nat (S ϕ)) =>
         subst_bsum_inv_fun (X := X) I J (fun ys : Vector.t nat (S (S ϕ)) => M (subst_bsum_fun I J ys ::: tl (tl ys))) xs = M xs.
Proof. intros M xs (Hxs1 & Hxs2). now apply subst_bsum_inv_correct with (Φ0 := Φ). Qed.

Lemma subst_mty_bsum_inv_correct {ϕ} (Φ : constr (S ϕ)) (I : idx ϕ) (J : idx (S ϕ)) :
  forall (ρ : mty (S ϕ)),
  mty! fun xs : Vector.t nat (S ϕ) => hd xs < (Σ_{d < I (tl xs)} J (d ::: tl xs)) /\ Φ xs ⊢
   subst_mty (subst_mty_beta_two ρ (subst_bsum_fun I J)) (subst_bsum_inv_fun I J) ≡ ρ.
Proof.
  intros ρ.
  setoid_rewrite subst_mty_twice. rewrite <- subst_mty_id. apply eqmty_subst_idx_sem.
  unfold funcomp, id. apply subst_bsum_inv_correct.
Qed.

(* TODO: Move *)
Lemma eqlty_eqlty {ϕ} (Φ : constr ϕ) (A1 A2 B1 B2 : lty ϕ) :
  (lty! Φ ⊢ A1 ≡ B1) -> (lty! Φ ⊢ A1 ≡ A2) -> (lty! Φ ⊢ B1 ≡ B2) -> (lty! Φ ⊢ A2 ≡ B2).
Proof. intros H1 H2 H3. now rewrite <- H3, <- H2. Qed.


Lemma eqlty_subst_bsum_inv {ϕ} (Φ : constr _) (I : idx ϕ) (J : idx (S ϕ)) (A B : lty _) :
  (lty! Φ ⊢ A ≡ B) ->
  (lty! subst_bsum_inv_fun I J Φ ⊢ subst_lty A (subst_bsum_inv_fun I J) ≡ subst_lty B (subst_bsum_inv_fun I J)).
Proof.
  intros Hty.
  repeat change (subst_bsum_inv_fun (X := ?X) I J) with (subst_bsum_inv_fun' (X := X) I J).
  unfold subst_bsum_inv_fun', funcomp.
  pose proof (idx_find_slot_correct I J) as Hf; cbn.
  eapply eqlty_mono_Φ.
  eapply eqlty_eqlty.
  - eapply eqlty_subst_clone.
    eapply eqlty_subst_beta with
        (I0 :=
           (fun xs : Vector.t nat (S (S ϕ)) =>
              let c := hd xs in
              let a := hd (tl xs) in
              let xs' := tl (tl xs) in
              snd (idx_find_slot I J (c ::: xs')))
        ) (y := 1).
    eapply eqlty_subst_beta_one with
        (i :=
           (fun xs : Vector.t nat (S ϕ) =>
              let c' := hd xs in
              let xs' := tl xs in
              fst (idx_find_slot I J (c' ::: xs')))
        ) (y := 1).
    apply Hty.
  - cbn. repeat setoid_rewrite subst_lty_twice; unfold funcomp; cbn. reflexivity.
  - cbn. repeat setoid_rewrite subst_lty_twice; unfold funcomp; cbn. reflexivity.
  - firstorder.
Qed.


Lemma tyT_bsum {ϕ} (Φ : constr (S ϕ)) (Γ : ctx (S ϕ))
      (I : idx ϕ) (J : idx (S ϕ))
      (M : idx (S ϕ)) (t : tm) (ρ : mty (S ϕ)) (s : skel) :
  (Ty! fun xs : Vector.t nat (S (S ϕ)) =>
         let b := hd xs in
         let a := hd (tl xs) in
         let xs' := tl (tl xs) in
         b < J (a ::: xs') /\ a < I xs' /\
         Φ (b + (Σ_{d < a} J (d ::: xs')) ::: xs');
  (subst_ctx Γ (fun (f : idx (S ϕ)) (xs : Vector.t nat (S (S ϕ))) => f (subst_bsum_fun I J xs ::: tl (tl xs))))
    ⊢(fun xs => M (subst_bsum_fun I J xs ::: tl (tl xs)))
                  t : subst_mty_beta_two ρ (subst_bsum_fun I J) @ s) ->
  (Ty! fun xs => let c := hd xs in
              let xs' := tl xs in
              c < (Σ_{d < I xs'} J (d ::: xs')) /\ Φ xs;
       Γ ⊢(M) t : ρ @ s).
Proof.
  intros ty.
  (* Apply the inverse substitution to the given typing *)
  assert (Ty! subst_bsum_inv_fun I J
            (fun xs : Vector.t nat (S (S ϕ)) =>
               let b := hd xs in
               let a := hd (tl xs) in
               let xs' := tl (tl xs) in b < J (a ::: xs') /\ a < I xs' /\ Φ (b + (Σ_{d < a} J (d ::: xs')) ::: xs'));
          subst_ctx (subst_ctx Γ (fun (f : idx (S ϕ)) (xs : Vector.t nat (S (S ϕ))) => f (subst_bsum_fun I J xs ::: tl (tl xs))))
                    (subst_bsum_inv_fun I J)
         ⊢(subst_bsum_inv_fun I J (fun xs : Vector.t nat (S (S ϕ)) => M (subst_bsum_fun I J xs ::: tl (tl xs))))
           t : subst_mty (subst_mty_beta_two ρ (subst_bsum_fun I J)) (subst_bsum_inv_fun I J) @ s) as Hty'.
  {
    repeat change (subst_bsum_inv_fun (X := ?X) I J) with (subst_bsum_inv_fun' (X := X) I J).
    unfold subst_bsum_inv_fun', funcomp.
    pose proof (idx_find_slot I J) as Hf; cbn.
    eapply tyT_sub. eapply tyT_weaken_Φ.
    eapply tyT_subst_clone.
    eapply tyT_subst_beta with
        (i :=
           (fun xs : Vector.t nat (S (S ϕ)) =>
              let c := hd xs in
              let a := hd (tl xs) in
              let xs' := tl (tl xs) in
              snd (idx_find_slot I J (c ::: xs')))
        ) (y := 1).
    eapply tyT_subst_beta_one with
        (i :=
           (fun xs : Vector.t nat (S ϕ) =>
              let c' := hd xs in
              let xs' := tl xs in
              fst (idx_find_slot I J (c' ::: xs')))
        ) (y := 1).
    apply ty.
    - cbn. firstorder.
    - cbn. hnf; intros y Hy. repeat setoid_rewrite subst_mty_twice; unfold funcomp; cbn. reflexivity.
    - cbn. repeat setoid_rewrite subst_mty_twice; unfold funcomp; cbn. reflexivity.
    - firstorder.
  }
  cbn in Hty'. eapply tyT_sub. eapply tyT_weaken_Φ.
  - apply Hty'.
  - cbn.
    hnf; intros xs (Hxs1 & Hxs2).
    unfold subst_bsum_inv_fun; cbn.
    rewrite <- !eta.
    pose proof (idx_find_slot_correct2 I J) as Hf; cbn.
    specialize Hf with (1 := Hxs1). rewrite <- eta in Hf.
    destruct (idx_find_slot I J xs) as [a' b'] eqn:Eab'; cbn.
    destruct Hf as (Hf1 & Hf2 & Hf3).
    now rewrite <- Hf1, <- eta.
  - cbn. hnf; intros y Hy. symmetry. apply subst_mty_bsum_inv_correct.
  - cbn. apply subst_mty_bsum_inv_correct.
  - cbn. apply subst_bsum_inv_correct'.
Qed.




(** *** Case [Const k] *)


Lemma pjoining_Const {ϕ} (L : idx ϕ) (Φ : constr ϕ) (Γc : ctx (S ϕ)) (Nc : idx (S ϕ)) (k : nat) (ρc : mty (S ϕ)) (s : skel) :
  (Ty! fun xs => hd xs < L (tl xs) /\ Φ (tl xs); Γc ⊢(Nc) Const k : ρc @ s) ->
  existsS (ρ : mty ϕ) (ρc' : mty (S ϕ)),
    (mty! fun xs => hd xs < L (tl xs) /\ Φ (tl xs) ⊢ ρc' ≡ ρc) **
    (bsum L ρc' ρ) **
    (Ty! Φ; def_ctx ⊢(fun xs => Σ_{c < L xs} Nc (c ::: xs)) Const k : ρ @ s).
Proof.
  intros ty. inv ty. apply eqmty_Nat_inv'_sig in Hty as (k' & -> & Hk).
  exists (Nat (iConst k)), (Nat (iConst k)); repeat_split.
  - apply eqmty_Nat. firstorder.
  - hnf; left; hnf. exists (iConst k); unfold iConst; cbn; auto.
  - apply tyT_Const.
    + reflexivity.
    + hnf; intros xs Hxs. erewrite Sum_ext.
      2:{ intros. rewrite <- HM by auto. reflexivity. }
      now rewrite Sum_constO.
Qed.


Require Import dlPCF_Joining. (* For the inversion lemmas; TODO: Move them... *)



Theorem pjoining_Lam {ϕ} (L : idx ϕ) (Φ : constr ϕ) (Γc : ctx (S ϕ)) (Nc : idx (S ϕ)) (t : tm) (ρc : mty (S ϕ)) (s : skel) :
  (Ty! fun xs => hd xs (* c *) < L (tl xs) /\ Φ (tl xs); Γc ⊢(Nc) Lam t : ρc @ s) ->
  closed (Lam t) ->
  existsS (ρ : mty ϕ) (ρc' : mty (S ϕ)),
    (mty! fun xs => hd xs < L (tl xs) /\ Φ (tl xs) ⊢ ρc' ≡ ρc) **
    (bsum L ρc' ρ) **
    (Ty! Φ; def_ctx ⊢(fun xs => Σ_{c < L xs} Nc (c ::: xs)) Lam t : ρ @ s).
Proof.
  (* Note: [L] is [I] in the application of [tyT_bsum] and [I] is [J]. *)

  intros Hty Hclos.
  eapply tyT_Lam_inv in Hty as (I & Γ' & Δ & K & σ & τ & s' & -> & HΓ & Hbsum & Hty & HM & ->).

  (* Contexts are futile, so let's remove them *)
  clear HΓ Hbsum Γ'.
  apply retypeT_free with (Σ := σ .: def_ctx) in Hty; auto.
  2:{ intros [ | y] Hy; cbn; auto. exfalso. eapply free_closed_iff in Hclos; eauto. }
  clear Δ.

  (* Build the bounded sum *)
  pose proof make_bsum_Quant_correct L I (σ ⊸ τ) as Hnewsum.
  set (X := make_bsum_Quant L I (σ ⊸ τ)) in Hnewsum.
  set (B := fst X) in Hnewsum; set (ρ' := snd X) in Hnewsum; cbn zeta in Hnewsum.
  cbn [X make_bsum_Quant fst snd] in B, ρ'.
  set (C := (subst_lty (σ ⊸ τ)
            (fun (j : idx (S (S ϕ))) (xs : Vector.t nat (S ϕ)) =>
             let '(a, b) := idx_find_slot L I xs in j (b ::: a ::: tl xs)))) in B,ρ'.

  assert (lty! fun xs : Vector.t nat (S (S ϕ)) => hd xs < I (tl xs) /\ hd (tl xs) < L (tl (tl xs)) /\ Φ (tl (tl xs)) ⊢
               B ≡ σ ⊸ τ) as HB.
  {
    revert Hnewsum; clear_all; intros.
    unfold B, C in *.
    destruct Hnewsum as (Hnewsum1 & Hnewsum2).
    apply eqmty_Quant_inv in Hnewsum2 as (Hnewsum2 & _).
    cbn in Hnewsum2.
    apply eqlty_Arr_inv in Hnewsum2 as (Hnewsum2 & Hnewsum2'); cbn in *.
    apply eqlty_Arr.
    - symmetry. eapply eqmty_mono_Φ; eauto. firstorder.
    - symmetry. eapply eqmty_mono_Φ; eauto. firstorder.
  }

  eexists ρ', (Quant I B); repeat_split.
  {
    apply eqmty_Quant.
    - apply HB.
    - hnf; reflexivity.
  }
  { right. apply Hnewsum. }
  {
    (* apply tyT_bsum in Hty. *)
    eapply tyT_Lam with
        (I0 := fun xs => Σ_{a < L xs} I (a ::: xs))
        (K0 := fun xs : Vector.t nat (S ϕ) => let '(a, b) := idx_find_slot L I xs in K (b ::: a ::: tl xs))
        (σ0 := Arr_proj1 C) (τ0 := Arr_proj2 C)
        (Γ' := def_ctx) (Δ := def_ctx).
    (* Context bogus stuff *)
    2:{ hnf; intros y Hy. exfalso. contradict Hy. eapply free_closed_iff; eauto. }
    2:{ hnf; intros y Hy. exfalso. contradict Hy. eapply free_closed_iff; eauto. }
    3:{ reflexivity. (* The final subtyping *) }
    1:{ (* The typing of [t] *)
      eapply tyT_bsum; cbn zeta.
      eapply tyT_sub. eapply tyT_weaken_Φ.
      - apply Hty.
      - cbn. hnf; intros xs (Hxs1&Hxs2&Hxs3); cbn in Hxs1,Hxs2,Hxs3|-*; repeat_split; auto.
        now rewrite <- eta in Hxs1.
      - hnf; intros [ | y] Hy; cbn -[C].
        + clear Hnewsum.
          unfold B, C in HB; cbn in HB. cbn.
          apply eqlty_Arr_inv in HB as (HB1 & HB2).
          eapply eqmty_mono_Φ; eauto.
          intros xs (Hxs1&Hxs2&Hxs3); cbn in Hxs1,Hxs2,Hxs3|-*; repeat_split; auto.
          now rewrite <- eta in Hxs1.
        + exfalso. contradict Hy. eapply free_closed_iff in Hclos; eauto.
      - clear Hnewsum.
        unfold B, C in HB; cbn in HB. cbn.
        apply eqlty_Arr_inv in HB as (HB1 & HB2).
        symmetry. eapply eqmty_mono_Φ; eauto.
        intros xs (Hxs1&Hxs2&Hxs3); cbn in Hxs1,Hxs2,Hxs3|-*; repeat_split; auto.
        now rewrite <- eta in Hxs1.
      - cbn. hnf; intros xs (Hxs1 & Hxs2 & Hxs3).
        pose proof (idx_find_slot_correct L I) as Hf.
        destruct (idx_find_slot L I (subst_bsum_fun L I xs ::: tl (tl xs))) as [a b] eqn:Eab.
        specialize Hf with (1 := Hxs2) (2 := Hxs1) (3 := Eab) as [<- <-].
        now rewrite <- !eta.
    }
    1:{ (* The final cost *)
      hnf; intros xs Hxs. cbn.
      setoid_rewrite Sum_ext at 4. (* Unfold [Nc] *)
      2:{ intros a Ha. rewrite <- HM by auto. reflexivity. }
      setoid_rewrite dlPCF_Splitting.Sum_idx_Sum.
      rewrite Sum_plus. f_equal. (* The first sums ([Σ_{a < L xs} I (a ::: xs)]) are the same *)
      apply Sum_ext. intros a Ha. apply Sum_ext. intros b Hb.
      pose proof (idx_find_slot_correct L I) as Hf.
      rewrite Nat.add_comm with (m := b).
      destruct (idx_find_slot L I (b + (Σ_{d < a} I (d ::: xs)) ::: xs)) as [a' b'] eqn:Eab'.
      specialize Hf with (1 := Ha) (2 := Hb) (3 := Eab') as [<- <-].
      reflexivity.
    }
  }
Qed.



(** Lifting of [join_forests] to an index. *)

Definition idx_join_forests {ϕ} (L : idx ϕ) (K : idx (S ϕ)) (I : idx (S (S ϕ))) (cardH : idx (S ϕ)) : idx (S ϕ) :=
  fun xs : Vector.t nat (S ϕ) =>
    let b := hd xs in
    let xs' := tl xs in
    join_forests (L xs') (fun c => K (c ::: xs')) (fun c b => I (b ::: c ::: xs')) (fun c => cardH (c ::: xs')) b.


(* TODO: Move *)
Lemma eqlty_subst_beta_one_two {ϕ} (Φ : constr (S (S ϕ))) (A B : lty (S (S ϕ))) (i : idx (S (S ϕ))) :
  (lty! Φ ⊢ A ≡ B) ->
  (lty! dlPCF_Splitting.subst_beta_one_two_fun i Φ ⊢ dlPCF_Splitting.subst_lty_beta_one_two i A ≡ dlPCF_Splitting.subst_lty_beta_one_two i B).
Proof. now intros [H1 H2]; split; apply dlPCF_Splitting.sublty_subst_beta_one_two. Qed.

Lemma eqlty_subst_beta_Fix1 {ϕ} (Φ : constr (S (S ϕ))) (A B : lty (S (S ϕ))) (card1 : idx (S (S ϕ))) :
  (lty! Φ ⊢ A ≡ B) ->
  (lty! fun xs =>
          let a := hd xs in
          let b := hd (tl xs) in
          let xs' := tl (tl xs) in
          Φ (0 ::: S (card1 xs + b) ::: xs') ⊢ subst_lty A (subst_beta_Fix1_fun card1) ≡ subst_lty B (subst_beta_Fix1_fun card1)).
Proof.
  intros H.
  unfold subst_beta_Fix1_fun.
  repeat setoid_rewrite <- subst_lty_twice.
  eapply eqlty_mono_Φ. apply eqlty_subst_beta_ground. apply eqlty_subst_beta_one_two. apply H.
  exact (fun xs Hxs => Hxs).
Qed.



(** Variant of [subst_bsum_inv_fun] that skips one variable *)

Definition subst_bsum_inv_one_fun {X : Type} {ϕ} (I : idx ϕ) (J : idx (S ϕ)) : (Vector.t nat (S (S (S ϕ))) -> X) -> (Vector.t nat (S (S ϕ)) -> X) :=
  fun (g : Vector.t nat (S (S (S ϕ))) -> X) (xs : Vector.t nat (S (S ϕ))) =>
    let f := idx_find_slot I J in
    g (hd xs ::: snd (f (tl xs)) ::: fst (f (tl xs)) ::: tl (tl xs)).

Definition subst_bsum_inv_one_fun' {X : Type} {ϕ} (I : idx ϕ) (J : idx (S ϕ)) :
  (Vector.t nat (S (S (S ϕ))) -> X) -> (Vector.t nat (S (S ϕ)) -> X) :=
  (* First replace [a] and introduce [c] *)
  let f := idx_find_slot I J in
  subst_var_beta_fun
    Fin2 1
    (fun xs : Vector.t nat (S ϕ) =>
       let c' := hd xs in
       let xs' := tl xs in
       fst (f (c' ::: xs')))
    >>
  (* Now replace [a] and introduce [c'] *)
  subst_var_beta_fun
    Fin1 1
    (fun xs : Vector.t nat (S (S ϕ)) =>
       let c := hd (xs) in
       let a := hd (tl xs) in
       let xs' := tl (tl xs) in
       snd (f (c ::: xs')))
    >>
  (* Finally, replace [c'] with [c] *)
  subst_var_clone_fun Fin1.

Lemma subst_bsum_inv_one_fun_correct {X : Type} {ϕ} (I : idx ϕ) (J : idx (S ϕ)) :
  subst_bsum_inv_one_fun (X := X) I J =
  subst_bsum_inv_one_fun' I J.
Proof.
  fext; intros xs Hxs.
  unfold subst_bsum_inv_one_fun, subst_bsum_inv_one_fun'; cbn.
  unshelve erewrite !subst_var_beta_fun_FS, !subst_var_beta_fun_Fin0; cbn. 1-3: reflexivity.
  rewrite !subst_var_clone_fun_FS. rewrite <- subst_clone_fun_correct.
  unfold funcomp, subst_beta_fun, subst_beta_one_fun, subst_clone_fun; cbn.
  now rewrite !vect_cast_id.
Qed.

Lemma eqlty_subst_bsum_one_inv {ϕ} (Φ : constr _) (I : idx ϕ) (J : idx (S ϕ)) (A B : lty _) :
  (lty! Φ ⊢ A ≡ B) ->
  (lty! subst_bsum_inv_one_fun I J Φ ⊢ subst_lty A (subst_bsum_inv_one_fun I J) ≡ subst_lty B (subst_bsum_inv_one_fun I J)).
Proof.
  intros Hty.
  repeat rewrite subst_bsum_inv_one_fun_correct.
  unfold subst_bsum_inv_one_fun'.
  repeat setoid_rewrite <- subst_lty_twice.
  eapply eqlty_mono_Φ.
  - apply eqlty_subst_var_clone.
    apply eqlty_subst_var_beta with (x := Fin1) (y := 1).
    apply eqlty_subst_var_beta with (x := Fin2) (y := 1).
    apply Hty.
  - exact (fun xs Hxs => Hxs).
Qed.


Theorem pjoining_Fix {ϕ} (L : idx ϕ) (Φ : constr ϕ) (Γc : ctx (S ϕ)) (Nc : idx (S ϕ)) (t : tm) (ρc : mty (S ϕ)) (s : skel) :
  (Ty! fun xs => hd xs (* c *) < L (tl xs) /\ Φ (tl xs); Γc ⊢(Nc) Fix t : ρc @ s) ->
  closed (Fix t) ->
  existsS (ρ : mty ϕ) (ρc' : mty (S ϕ)),
    (mty! fun xs => hd xs < L (tl xs) /\ Φ (tl xs) ⊢ ρc' ≡ ρc) **
    (bsum L ρc' ρ) **
    (Ty! Φ; def_ctx ⊢(fun xs => Σ_{c < L xs} Nc (c ::: xs)) Fix t : ρ @ s).
Proof.
  (* Note: [L] is [I] in the application of [tyT_bsum], and [I] is [K]. *)
  intros Hty Hclos.
  apply tyT_Fix_inv in Hty
    as (Ib & Δ & Jb & A & B & B' & K & card1 & card2 & cardH & Γ' & s' & Hty & Hsub & HcardH & Hcard1 & Hcard2
        & Hbsum & HΓ & HM & HB & -> & ->).

  (* Contexts are futile, so let's remove them *)
  clear HΓ Hbsum Γ'.
  apply retypeT_free with (Σ := [ < Ib]⋅ A .: def_ctx) in Hty; auto.
  2:{ intros [ | y] Hy; cbn; auto. exfalso. eapply free_closed_iff in Hclos; eauto. }
  clear Δ.

  (* Build the bounded sum *)
  pose proof make_bsum_Quant_correct L K B' as Hnewsum.
  set (X := make_bsum_Quant L K B') in Hnewsum.
  set (B'' := fst X) in Hnewsum; set (ρ' := snd X) in Hnewsum; cbn zeta in Hnewsum.
  cbn [X make_bsum_Quant fst snd] in B'', ρ'.
  set (C := (subst_lty B'
            (fun (j : idx (S (S ϕ))) (xs : Vector.t nat (S ϕ)) =>
               let '(a, b) := idx_find_slot L K xs in j (b ::: a ::: tl xs)))) in B'',ρ'.

  assert (lty! fun xs : Vector.t nat (S (S ϕ)) => hd xs < K (tl xs) /\ hd (tl xs) < L (tl (tl xs)) /\ Φ (tl (tl xs)) ⊢
               B'≡ B'') as HB'.
  {
    revert Hnewsum; clear_all; intros.
    unfold B'', C in *.
    destruct Hnewsum as (Hnewsum1 & Hnewsum2).
    apply eqmty_Quant_inv in Hnewsum2 as (Hnewsum2 & _).
    cbn in Hnewsum2. eapply eqlty_mono_Φ; eauto. firstorder.
  }

  (* Note that [I] has [c] as #0 and [b] as #1. *)
  (* [I c] describes the [c]th forests, which consists of [K c] trees. *)
  (* First, we join the [L] forests... *)
  pose (I_joined := idx_join_forests L K Ib cardH).

  (* The cardinality of the joined forest is simply the sum of the cardinality of the forests *)
  pose (cardH_join := fun xs : Vector.t nat ϕ => Σ_{c < L xs} cardH (c ::: xs)).


  (* The first aux card *)
  pose (card1' :=
          fun xs : Vector.t nat (S (S ϕ)) =>
            let a := hd xs in let b := hd (tl xs) in let xs' := tl (tl xs) in
            let '(a', b') := idx_find_slot L cardH (b ::: xs') in
            card1 (a ::: b' ::: a' ::: xs')).
  assert
    (sem! fun xs : Vector.t nat (S (S ϕ)) =>
            let a := hd xs in let b := hd (tl xs) in let xs' := tl (tl xs) in
            a < I_joined (b ::: xs') /\ b < cardH_join xs' /\ Φ xs' ⊨
          fun xs : Vector.t nat (S (S ϕ)) =>
            let a := hd xs in let b := hd (tl xs) in let xs' := tl (tl xs) in
            isForestCard (fun c : nat => I_joined (S (b + c) ::: xs')) a (card1' xs)) as Hcard1'.
  {
    subst card1'; cbn; hnf; intros xs (Hxs1 & Hxs2 & Hxs3).
    pose proof idx_find_slot_correct2 L cardH as Hf. unfold cardH_join in Hxs2.
    specialize Hf with (1 := Hxs2).
    destruct (idx_find_slot L cardH (hd (tl xs) ::: tl (tl xs))) as [a' b']; cbn.
    destruct Hf as (Hf1 & Hf2 & Hf3).

    assert (hd xs < Ib (b' ::: a' ::: tl (tl xs))) as L1.
    {
      eapply Nat.lt_le_trans; eauto.
      rewrite Hf1.
      unfold I_joined, idx_join_forests; cbn; eta. rewrite join_forests_Sum; eauto.
    }

    eapply isForestCard_ext_strong.
    - hnf in Hcard1; cbn in Hcard1. apply (Hcard1 (hd xs ::: b' ::: a' ::: tl (tl xs))); cbn. repeat_split; auto.
    - cbn. intros x Hx.
      rewrite Hf1.
      replace (S (b' + (Σ_{d < a'} cardH (d ::: tl (tl xs))) + x)) with ((S b' + x) + (Σ_{d < a'} cardH (d ::: tl (tl xs)))) by lia.
      unfold I_joined, idx_join_forests; cbn -["+"]; eta. rewrite join_forests_Sum; eauto.
      enough (S b' + card1 (hd xs ::: b' ::: a' ::: tl (tl xs)) < cardH (a' ::: tl (tl xs))) by lia.
      eapply isForestCard_child_bounded'.
      3:{ apply HcardH; auto. }
      2:{ apply Hf3. }
      2:{ cbn. hnf in Hcard1; cbn in Hcard1. eapply (Hcard1 (_ ::: b' ::: a' ::: tl (tl xs))); cbn. repeat_split; auto. }
      1:{ cbn. auto. }
  }


  (* The second aux card *)
  pose
    (card2' :=
       fun xs : Vector.t nat (S ϕ) =>
           let a := hd xs in (* a < Σ_{c < L xs'} K (c ::: xs') *)
           let xs' := tl xs in
           let f := idx_find_slot L K in
           let (b', a') := f xs in
           (Σ_{c < b'} cardH (c ::: xs')) + card2 (a' ::: b' ::: xs')).
  assert
    (sem! fun xs : Vector.t nat (S ϕ) => let a := hd xs in let xs' := tl xs in a < (Σ_{c < L xs'} K (c ::: xs')) /\ Φ xs' ⊨
          fun xs : Vector.t nat (S ϕ) =>
            let a := hd xs in (* a < Σ_{c < L xs'} K (c ::: xs') *)
            let xs' := tl xs in
            isForestCard (fun b => I_joined (b ::: xs')) a (card2' xs)) as Hcard2'.
  {
    subst card2'; cbn; hnf; intros xs (Hxs1 & Hxs2).
    pose proof (idx_find_slot_correct2 L K) as Hf.
    specialize Hf with (1 := Hxs1). rewrite <- eta in Hf.
    destruct (idx_find_slot L K xs) as [a' b']. destruct Hf as (Hf1 & Hf2 & Hf3).
    rewrite Hf1.
    unfold I_joined, idx_join_forests; cbn; eta.
    apply isForestCard_join_correct1.
    - apply Hf2.
    - apply Hf3.
    - intros i Ha. apply HcardH; cbn; split; auto. lia.
    - hnf in Hcard2. now apply (Hcard2 (b' ::: a' ::: tl xs)).
  }


  exists ρ', (Quant K B''); repeat_split.
  {
    apply eqmty_Quant.
    - symmetry. apply HB'.
    - hnf; reflexivity.
  }
  { right. apply Hnewsum. }
  {
    eapply tyT_Fix with
        (Ib0 := I_joined) (cardH0 := cardH_join) (card4 := card2')
        (K0 := fun xs => Σ_{c < L xs} K (c ::: xs))
        (Jb0 := subst_bsum_inv_fun L cardH Jb)
        (* fun xs : Vector.t nat (S ϕ) => let '(a, b) := proj1_sig (idx_find_slot L cardH) xs in Jb (b ::: a ::: tl xs) *)
        (Δ := def_ctx) (Γ' := def_ctx)
        (A0 := subst_lty (* Inverse [bsum] substitution with one var skipped *)
                 A
                 (fun (j : idx (S (S (S ϕ)))) (xs : Vector.t nat (S (S ϕ))) =>
                    let '(a, b) := idx_find_slot L cardH (tl xs) in
                    j (hd xs ::: b ::: a ::: tl (tl xs))))
        (B0 := subst_lty
                 B
                 (fun (j : idx (S (S (S ϕ)))) (xs : Vector.t nat (S (S ϕ))) =>
                    let '(a, b) := idx_find_slot L cardH (tl xs) in
                    j (hd xs ::: b ::: a ::: tl (tl xs)))).
    (* Bogus contexts first *)
    6:{ hnf; intros y Hy. exfalso. contradict Hy. now eapply free_closed_iff. }
    6:{ reflexivity. }
    1:{ (* The typing of [Lam t] *)
      eapply tyT_bsum.
      eapply tyT_sub. eapply tyT_weaken_Φ.
      - apply Hty.
      - cbn. intros xs (Hxs1 & Hxs2 & Hxs3); repeat_split; auto. now rewrite <- eta in Hxs1.
      - (* Context *) hnf; intros [ | y] Hy; cbn.
        + apply eqmty_Quant. (* (only the first variable is not free) *)
          * setoid_rewrite subst_lty_twice; unfold funcomp. rewrite <- subst_lty_id. apply eqlty_subst_idx_sem.
            hnf; intros xs (Hxs1 & Hxs2 & Hxs3 & Hxs4); unfold id; cbn.
            pose proof (idx_find_slot_correct L cardH) as Hf.
            destruct (idx_find_slot L cardH (subst_bsum_fun L cardH (tl xs) ::: tl (tl (tl xs)))) as [a' b'] eqn:Eab'; cbn.
            intros j; f_equal; clear j.
            specialize Hf with (1 := Hxs3); cbn in Hf. rewrite <- eta in Hf,Hxs2. specialize Hf with (1 := Hxs2).
            specialize Hf with (1 := Eab') as (<- & <-). now rewrite <- !eta.
          * hnf; intros xs (Hxs1 & Hxs2 & Hxs3).
            unfold I_joined, subst_bsum_fun, idx_join_forests; cbn. rewrite <- eta in Hxs1.
            rewrite join_forests_Sum; try rewrite <- !eta; auto.
        + exfalso. eapply free_closed_iff with (x := y) in Hclos. cbn in Hy,Hclos. contradiction.
      - (* B *)
        rewrite dlPCF_Splitting.subst_mty_beta_two_eq_Quant. setoid_rewrite subst_lty_twice; unfold funcomp; cbn.
        apply eqmty_Quant. 2: hnf; reflexivity.
        symmetry. rewrite <- subst_lty_id. apply eqlty_subst_idx_sem.
        hnf; intros xs (Hxs1 & Hxs2 & Hxs3 & Hxs4).
        unfold dlPCF_Splitting.subst_beta_one_two_fun, id; cbn.
        pose proof (idx_find_slot_correct L cardH) as Hf.
        destruct (idx_find_slot L cardH (subst_bsum_fun L cardH (tl xs) ::: tl (tl (tl xs)))) as [a' b'] eqn:Eab'; cbn.
        intros j; f_equal; clear j.
        specialize Hf with (1 := Hxs3); cbn in Hf. rewrite <- eta in Hf,Hxs2. specialize Hf with (1 := Hxs2).
        specialize Hf with (1 := Eab') as (<- & <-). now rewrite <- !eta.
      - (* J *) cbn. hnf; intros xs (Hxs1 & Hxs2 & Hxs3). unfold subst_bsum_inv_fun.
        pose proof (idx_find_slot_correct L cardH) as Hf.
        destruct (idx_find_slot L cardH (subst_bsum_fun L cardH xs ::: tl (tl xs))) as [a' b'] eqn:Eab'; cbn.
        specialize Hf with (1 := Hxs2); cbn in Hf. rewrite <- eta in Hf, Hxs1. specialize Hf with (1 := Hxs1).
        specialize Hf with (1 := Eab') as (<- & <-).
        rewrite Eab'; cbn. now rewrite <- !eta.
    }
    5:{ (* The cost *)
      hnf; intros xs Hxs. unfold cardH_join.
      unfold subst_bsum_inv_fun; cbn.
      setoid_rewrite Sum_ext at 3. (* Unfold [Nc] *)
      2:{ intros a Ha. rewrite <- HM by auto. reflexivity. }
      cbn. setoid_rewrite dlPCF_Splitting.Sum_idx_Sum.
      apply Sum_ext. intros a Ha. apply Sum_ext. intros b Hb.
      pose proof (idx_find_slot_correct L cardH) as Hf.
      rewrite Nat.add_comm with (m := b).
      destruct (idx_find_slot L cardH (b + (Σ_{d < a} cardH (d ::: xs)) ::: xs)) as [a' b'] eqn:Eab'; cbn.
      now specialize Hf with (1 := Ha) (2 := Hb) (3 := Eab') as (<- & <-).
    }
    2:{ (* The total card *)
      unfold I_joined, cardH_join. hnf; intros xs Hsx.
      unfold idx_join_forests; cbn; eta. eapply isForestCard_join_correct0; auto.
    }
    2:{ (* The first aux card *) exact Hcard1'. }
    2:{ (* The second aux card *) exact Hcard2'. }
    1:{ (* The first subtyping *)
      cbn. etransitivity.
      2:{
        (* TODO: These functions should have the same form everywhere... *)
        replace
          (subst_lty A
                     (fun (j : idx (S (S (S ϕ)))) (xs : Vector.t nat (S (S ϕ))) =>
                        let '(a, b) := idx_find_slot L cardH (tl xs) in j (hd xs ::: b ::: a ::: tl (tl xs))))
          with (subst_lty A (subst_bsum_inv_one_fun L cardH)).
        2:{ f_equal. fext; intros j xs. unfold subst_bsum_inv_one_fun. now destruct idx_find_slot; cbn. }
        eapply eqlty_mono_Φ. eapply eqlty_subst_bsum_one_inv.
        - apply Hsub.
        - cbn; hnf; intros xs (Hxs1 & Hxs2 & Hxs3). unfold subst_bsum_inv_one_fun; cbn.
          pose proof idx_find_slot_correct2 L cardH as Hf. specialize Hf with (1 := Hxs2). rewrite <- eta in Hf.
          destruct (idx_find_slot L cardH (tl xs)) as [a b]; cbn.
          destruct Hf as (Hf1 & Hf2 & Hf3).
          repeat_split; auto.
          eapply Nat.lt_le_trans; eauto.
          rewrite Hf1.
          unfold I_joined, idx_join_forests; cbn; eta.
          rewrite join_forests_Sum; auto.
      }
      {
        setoid_rewrite subst_lty_twice; unfold funcomp; cbn. apply eqlty_subst_idx_sem.
        hnf; intros xs (Hxs1 & Hxs2 & Hxs3).
        unfold subst_bsum_inv_one_fun; cbn.
        unfold card1'; cbn. rewrite <- !eta.
        pose proof idx_find_slot_correct2 L cardH as Hf1. specialize Hf1 with (1 := Hxs2). rewrite <- eta in Hf1.
        destruct (idx_find_slot L cardH (tl xs)) as [a' b']. destruct Hf1 as (Hf1 & Hf1' & Hf1'').

        rewrite Hf1; cbn.
        replace (S (card1 (hd xs ::: b' ::: a' ::: tl (tl xs)) + (b' + (Σ_{d < a'} cardH (d ::: tl (tl xs)))))) with
            ((S (card1 (hd xs ::: b' ::: a' ::: tl (tl xs)) + b')) + (Σ_{d < a'} cardH (d ::: tl (tl xs)))) by lia.

        pose proof idx_find_slot_correct L cardH as Hf2.
        specialize (Hf2 a' (S (card1 (hd xs ::: b' ::: a' ::: tl (tl xs)) + b'))) with (1 := Hf1').
        destruct (idx_find_slot L cardH (_ + (Σ_{d < a'} cardH (d ::: tl (tl xs))) ::: tl (tl xs))) as [a'' b''].
        specialize Hf2 with (2 := eq_refl). spec_assert Hf2.
        {
          rewrite Nat.add_comm with (m := b').

          assert (hd xs < Ib (b' ::: a' ::: tl (tl xs))) as L1.
          {
            eapply Nat.lt_le_trans; eauto.
            rewrite Hf1.
            unfold I_joined, idx_join_forests; cbn; eta. rewrite join_forests_Sum; eauto.
          }

          eapply isForestCard_child_bounded'.
          3:{ apply HcardH; auto. }
          2:{ apply Hf1''. }
          2:{ cbn. hnf in Hcard1; cbn in Hcard1. apply (Hcard1 (hd xs ::: b' ::: a' ::: tl (tl xs))); cbn. repeat_split; auto. }
          1:{ cbn. auto. }
        }
        destruct Hf2 as (<- & <-). reflexivity.
      }
    }
    1:{ (* The second subtyping *)
      unfold ρ'. apply eqmty_Quant. 2: hnf; reflexivity.
      unfold C. etransitivity.
      2:{
        (* TODO: These functions should have the same form everywhere... *)
        replace
          (subst_lty B'
                     (fun (j : idx (S (S ϕ))) (xs : Vector.t nat (S ϕ)) =>
                        let '(a, b) := idx_find_slot L K xs in j (b ::: a ::: tl xs)))
          with (subst_lty B' (subst_bsum_inv_fun L K)).
        2:{ f_equal. fext; intros j xs. unfold subst_bsum_inv_fun. now destruct idx_find_slot. }

        eapply eqlty_mono_Φ. eapply eqlty_subst_bsum_inv.
        - apply HB.
        - hnf; intros xs (Hxs1 & Hxs2). unfold subst_bsum_inv_fun; cbn.
          pose proof (idx_find_slot_correct2 L K) as Hf.
          specialize Hf with (1 := Hxs1).
          destruct (idx_find_slot L K (hd xs ::: tl xs)) as [a b]; cbn.
          now destruct Hf as (Hf1 & Hf2 & Hf3).
      }
      {
        setoid_rewrite subst_lty_twice; unfold funcomp; cbn. apply eqlty_subst_idx_sem.
        hnf; intros xs (Hxs1 & Hxs2).

        pose proof idx_find_slot_correct2 L K as Hf1.
        specialize Hf1 with (1 := Hxs1). rewrite <- eta in Hf1.
        unfold subst_bsum_inv_fun; cbn. unfold card2'; cbn. rewrite <- !eta.
        destruct (idx_find_slot L K xs) as [a b] eqn:E1; cbn.
        destruct Hf1 as (Hf1 & Hf1' & Hf1'').

        (* Note that we have different parameters here! *)
        pose proof idx_find_slot_correct L cardH as Hf2.
        rewrite Nat.add_comm.
        specialize (Hf2 a (card2 (b ::: a ::: tl xs))) with (1 := Hf1').
        destruct (idx_find_slot L cardH (card2 (b ::: a ::: tl xs) + (Σ_{c < a} cardH (c ::: tl xs)) ::: tl xs)) as [a' b'] eqn:E2.
        specialize Hf2 with (2 := eq_refl).
        spec_assert Hf2.
        {
          eapply isForestCard_smono.
          - apply Hcard2; cbn; auto.
          - cbn. apply HcardH; cbn; auto.
          - cbn. auto.
        }
        destruct Hf2 as (<- & <-). reflexivity.
      }
    }
  }
Qed.


Theorem pjoining {ϕ} (L : idx ϕ) (Φ : constr ϕ) (Γc : ctx (S ϕ)) (Nc : idx (S ϕ)) (v : tm) (ρc : mty (S ϕ)) (s : skel) :
  (Ty! fun xs => hd xs (* c *) < L (tl xs) /\ Φ (tl xs); Γc ⊢(Nc) v : ρc @ s) ->
  val v -> closed v ->
  existsS (ρ : mty ϕ) (ρc' : mty (S ϕ)),
    (mty! fun xs => hd xs < L (tl xs) /\ Φ (tl xs) ⊢ ρc' ≡ ρc) **
    (bsum L ρc' ρ) **
    (Ty! Φ; def_ctx ⊢(fun xs => Σ_{c < L xs} Nc (c ::: xs)) v : ρ @ s).
Proof.
  intros ty Hval Hclos.
  apply valb_iff in Hval; destruct v; cbn in Hval; try discriminate; clear Hval.
  - eapply pjoining_Lam; eauto.
  - eapply pjoining_Fix; eauto.
  - eapply pjoining_Const; eauto.
Qed.
