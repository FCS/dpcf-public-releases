Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax.
Require Import dlPCF_Types.
Require Import dlPCF_iSubst.
Require PCF_Types.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.

Open Scope vector_scope.

Import SigmaTypeNotations.

From Coq Require Import Vector Fin.
From Coq Require Import Lia.

Delimit Scope PCF with PCF.
Open Scope PCF.


(** ** Modal Sum Operators *)


(** *** Modal sum *)

(** Modal sum (⊎) is a partial binary operation on modal types *)

Definition msum {ϕ} (τ1 τ2 : mty ϕ) (τ : mty ϕ) : Prop :=
  match τ1, τ2, τ with
  | Nat i, Nat j, Nat k => i = j /\ j = k
  | Quant i A, Quant j B, Quant k C =>
    A = C /\ (* A and C are equal (up to alpha equivalence *)
    B = lty_shift_add C i /\ (* B simply is A shifted by i *)
    (forall xs, k xs = i xs + j xs)
  | _, _, _ => False
  end.


(** Examples *)

Goal forall ϕ,
  @msum ϕ
     ([< iConst 3] ⋅ Nat (hd) ⊸ Nat (hd >> S))
     ([< iConst 2] ⋅ Nat (hd >> (plus 3)) ⊸ Nat (hd >> (fun b => S b + 3)))
     ([< iConst 5] ⋅ Nat (hd) ⊸ Nat (hd >> S)).
Proof.
  intros. cbn. repeat_split.
  - reflexivity.
  - repeat f_equal.
    fext; intros xs; unfold funcomp, shift_add_fun; cbn. omega.
  - reflexivity.
Qed.

(* More complex computation *)
Goal forall ϕ,
  @msum ϕ
     ([< iConst 3] ⋅ Nat (hd >> (minus 5)) ⊸ Nat (hd))
     ([< iConst 2] ⋅ Nat (hd >> (minus 2)) ⊸ Nat (hd >> (plus 3)))
     ([< iConst 5] ⋅ Nat (hd >> (minus 5)) ⊸ Nat (hd)).
Proof. repeat constructor. Qed.


(** [msum] is functional *)

Lemma msum_functional {ϕ} (τ1 τ2 : mty ϕ) (ρ1 ρ2 : mty ϕ) :
  msum τ1 τ2 ρ1 ->
  msum τ1 τ2 ρ2 ->
  ρ1 = ρ2.
Proof.
  intros H1 H2.
  destruct τ1,τ2,ρ1,ρ2; cbn in *; try tauto.
  - now destruct H1, H2; subst.
  - destruct H1 as (->&->&H1). destruct H2 as (->&_&H2).
    f_equal. fext; intros xs. now rewrite H1,H2.
Qed.


(* [msum] preserves shapes *)



(** **** Inversion and constructors *)


Lemma msum_Nat {ϕ} (i j k : idx ϕ) :
  i = j -> j = k ->
  msum (Nat i) (Nat j) (Nat k).
Proof. intros -> ->. cbn. split; reflexivity. Qed.

Lemma msum_Nat' {ϕ} (k : idx ϕ) :
  msum (Nat k) (Nat k) (Nat k).
Proof. now apply msum_Nat. Qed.

Lemma msum_Nat_inv {ϕ} (τ1 τ2 : mty ϕ) k :
  msum τ1 τ2 (Nat k) ->
  τ1 = Nat k /\
  τ2 = Nat k.
Proof.
  destruct τ1,τ2; cbn in *; try tauto.
  intros [-> ->]. tauto.
Qed.

Lemma msum_Quant {ϕ} (i j k : idx ϕ) (A B C : lty (S ϕ)) :
  A = C ->
  B = lty_shift_add C i ->
  (forall xs, k xs = i xs + j xs) ->
  msum (Quant i A) (Quant j B) (Quant k C).
Proof. cbn. tauto. Qed.

Lemma msum_Quant' {ϕ} (I1 I2 : idx ϕ) (A : lty (S ϕ)) :
  msum (Quant I1 A) (Quant I2 (lty_shift_add A I1)) (Quant (fun xs => I1 xs + I2 xs) A).
Proof. cbn. tauto. Qed.

Lemma msum_Quant_inv {ϕ} (τ1 τ2 : mty ϕ) k (C : lty (S ϕ)) :
  msum τ1 τ2 (Quant k C) ->
  exists (i : idx ϕ) (j : idx ϕ),
    τ1 = Quant i C /\ τ2 = Quant j (lty_shift_add C i) /\
    (forall xs, k xs = i xs + j xs).
Proof.
  intros H.
  destruct τ1,τ2; cbn in *; try tauto.
  destruct H as (->&->&H). eauto.
Qed.


(** *** Substitution / Shifting lemma *)


Lemma p_equal (X : Type) (f : X -> Prop) (x y : X) : f x -> x = y -> f y.
Proof. now intros H ->. Qed.

Lemma constr_shift_var_add_0 {ϕ} (Φ : constr (S ϕ)) (I : idx ϕ) (Heq: ϕ = ϕ-0) :
  sem! fun xs : Vector.t nat (S ϕ) => Φ (I (tl xs) + hd xs ::: tl xs) ⊨
   shift_var_add_fun Fin0 I Φ.
Proof.
  hnf; intros xs Hxs.
  unfold shift_var_add_fun.
  refine (p_equal _ Hxs _).
  unfold idx_cast; cbn.
  now erewrite !vect_cast_id.
Qed.

Lemma lty_shift_add_strip {ϕ} (A : lty (S ϕ)) i :
  lty_strip (lty_shift_add A i) = lty_strip A.
Proof. apply subst_lty_strip. Qed.
Lemma mty_shift_add_strip {ϕ} (τ : mty (S ϕ)) i :
  mty_strip (mty_shift_add τ i) = mty_strip τ.
Proof. apply subst_mty_strip. Qed.
Lemma lty_shift_sub_strip {ϕ} (A : lty (S ϕ)) i :
  lty_strip (lty_shift_sub A i) = lty_strip A.
Proof. apply subst_lty_strip. Qed.
Lemma mty_shift_sub_strip {ϕ} (τ : mty (S ϕ)) i :
  mty_strip (mty_shift_sub τ i) = mty_strip τ.
Proof. apply subst_mty_strip. Qed.



(** **** Associativity of (binary) modal sums *)

Lemma msum_assoc {ϕ} { τ1 τ2 τ3 σ ρ : mty ϕ } :
  msum τ1 τ2 σ ->
  msum σ τ3 ρ ->
  { σ' | msum τ2 τ3 σ' /\ msum τ1 σ' ρ }.
Proof.
  destruct τ1,τ2,σ,τ3,ρ; cbn; try tauto.
  - intros (<-&<-) (<-&<-).
    eexists (Nat _); eauto.
  - intros (<-&->&H1). intros (<-&->&H2).
    eexists (Quant (fun xs => _) _); cbn; repeat_split; eauto.
    + rewrite lty_shift_add_twice. f_equal. fext; intros xs; auto.
    + cbn. intro xs. specialize (H1 xs); specialize (H2 xs). nia.
Qed.

(** Associativity in the other direction *)
Lemma msum_assoc' {ϕ} { τ1 τ2 τ3 σ ρ : mty ϕ } :
  msum τ1 σ ρ ->
  msum τ2 τ3 σ ->
  { σ' : mty ϕ | msum τ1 τ2 σ' /\ msum σ' τ3 ρ }.
Proof.
  destruct τ1,τ2,σ,τ3,ρ; cbn; try tauto.
  - intros (<-&<-) (<-&<-).
    eexists (Nat _); repeat_split; eauto.
    cbn; auto.
  - intros (<-&->&H1). intros (->&->&H2).
    eexists (Quant (fun xs => _) _); cbn; repeat_split; eauto.
    + now rewrite lty_shift_add_twice.
    + cbn. intro xs. specialize (H1 xs); specialize (H2 xs). nia.
Qed.


(** **** More lemmas about modal sums *)

Lemma submty_msum_left {ϕ} (τ1 τ2 σ : mty ϕ) :
  msum τ1 τ2 σ ->
  σ ⊑m τ1.
Proof.
  intros H.
  destruct τ1, τ2, σ; cbn in *;
    repeat (match goal with
            | [H : ?H1 /\ ?H2 |- _ ] => destruct H
            | [H : False |- _ ] => contradiction H
            | [H : exists i, ?P |- _ ] => destruct H
            end; subst).
  - now constructor.
  - constructor.
    + eapply sublty_eqmty; eauto using sublty_refl.
    + hnf; intros xs Hxs. rewrite H0. omega.
Qed.



(** *** Bounded sums *)


(** Definition of bounded sums for [Nat]:
If σ = Nat J, then Σ_{a<I} σ = Nat J, given that "a is not free in σ". *)

(** Note that this definition has sort [Type].
This is needed, because we need to lift the definitions and (sigma) theorems to contexts. *)

Definition bsum_Nat {ϕ} (I : idx ϕ) (σ : mty (S ϕ)) (τ : mty ϕ) : Type :=
  { J : idx ϕ | σ = subst_mty (Nat J) (fun f xs => f (tl xs)) /\ (τ = Nat J) } % type.


(** Variant of beta substitution, where we introduce two new variables *)

Definition subst_lty_beta_two {m:nat} (A : lty (S m)) (i : idx (S (S m))) : lty (S (S m)) :=
  subst_lty A (fun (f : idx (S m)) (xs : Vector.t nat (S (S m))) => f (i xs ::: tl (tl xs))).

Lemma subst_lty_beta_two_correct {m:nat} (A : lty (S m)) (i : idx (S (S m))) :
  subst_lty_beta_two A i = subst_lty_beta 2 i A.
Proof. reflexivity. Qed.

Lemma subst_lty_beta_two_correct' {m:nat} (A : lty (S m)) (i : idx (S (S m))) :
  subst_lty_beta_two A i = subst_lty_var_beta Fin0 2 i A.
Proof.
  rewrite subst_lty_beta_two_correct.
  unfold subst_lty_beta, subst_lty_var_beta; f_equal. now rewrite subst_var_beta_fun_Fin0.
Qed.

Definition subst_mty_beta_two {m:nat} (A : mty (S m)) (i : idx (S (S m))) : mty (S (S m)) :=
  subst_mty A (fun (f : idx (S m)) (xs : Vector.t nat (S (S m))) => f (i xs ::: tl (tl xs))).

Lemma subst_mty_beta_two_correct {m:nat} (A : mty (S m)) (i : idx (S (S m))) :
  subst_mty_beta_two A i = subst_mty_beta 2 i A.
Proof. reflexivity. Qed.

Lemma subst_mty_beta_two_correct' {m:nat} (A : mty (S m)) (i : idx (S (S m))) :
  subst_mty_beta_two A i = subst_mty_var_beta Fin0 2 i A.
Proof.
  rewrite subst_mty_beta_two_correct.
  unfold subst_mty_beta, subst_mty_var_beta; f_equal. now rewrite subst_var_beta_fun_Fin0.
Qed.


(** For sums of quantifications, [Σ_{a<I} σ a] with [σ a = [b < J a] ⋅ A (b + Σ_{d<a} J d)],
the bounded sum is defined as [[c < Σ_{a<I} J a] ⋅ A c]. *)

Definition bsum_Quant {ϕ} (I : idx ϕ) (σ : mty (S ϕ)) (τ : mty ϕ) : Type :=
  existsST J & A,
    (* 0 = a in J *)
    (* 0 = c in A *)
    σ = [<J] ⋅ (subst_lty_beta_two
                  A
                  (fun xs =>
                     let b := hd xs in
                     let a := hd (tl xs) in
                     let xs' := tl (tl xs) in
                     b + Σ_{d<a} (J (d ::: xs')))) /\
    (τ = [< fun xs => Σ_{a < I xs} J (a ::: xs)] ⋅ A).


Definition bsum {ϕ} (I : idx ϕ) (σ : mty (S ϕ)) (τ : mty ϕ) : Type :=
  bsum_Nat I σ τ + bsum_Quant I σ τ.


(** Examples *)

(* Σ_{a<I} 42 = 42 *)
Goal forall ϕ (I : idx ϕ), bsum I (Nat (iConst 42)) (Nat (iConst 42)).
Proof. intros. left. hnf. cbn. eexists. split. 2: reflexivity. reflexivity. Qed.


(**
This example is an instance of the 'universality lemma' for bounded sums.

The equation [Σ_{a<I} [b<J] ⋅ Nat a ⊸ Nat a = [c < J] ⋅ Nat c ⊸ Nat c] with [I = 1] doesn't hold,
because (bounded) sums are syntactically properties. We first have to change the type in a way that
is still semantically equivalent to the original term, under the provision [a < I].
*)
Goal
  forall ϕ (J : idx ϕ),
    let I  : idx ϕ := iConst 1 in
    let σ  : mty (S ϕ) := [<fun xs => J (tl xs)] ⋅ Nat hd ⊸ Nat hd in
    let σ' : mty (S ϕ) := [ < fun xs : Vector.t nat (S ϕ) => J (tl xs)] ⋅
                            Nat (fun xs : Vector.t nat (S (S ϕ)) => hd xs + (Σ_{_ < hd (tl xs)} J (tl (tl xs)))) ⊸
                            Nat (fun xs : Vector.t nat (S (S ϕ)) => hd xs + (Σ_{_ < hd (tl xs)} J (tl (tl xs)))) in
    let τ  : mty ϕ := [< fun xs => Σ_{a<I xs} J xs] ⋅ Nat hd ⊸ Nat hd in
    let τ' : mty ϕ := ([<J] ⋅ Nat hd ⊸ Nat hd) in
    prod (mty! fun xs : Vector.t nat (S ϕ) => hd xs < I (tl xs) ⊢ σ ≡ σ')
    (prod (bsum I σ' τ)
    (τ = τ')). (* equational simplification *)
Proof.
  intros. split. 2: split.
  2:{
    cbn. hnf. right. hnf.
    eexists (fun xs => J (tl xs)), (Nat hd ⊸ Nat hd). split.
    2:{ reflexivity. }
    cbn. unfold σ'. reflexivity.
  }
  - unfold I, iConst in *; cbn. apply eqmty_Quant.
    + apply eqlty_Arr.
      * apply eqmty_Nat. hnf; intros xs (Hxs1&Hxs2). replace (hd (tl xs)) with 0 by nia. cbn. nia.
      * apply eqmty_Nat. hnf; intros xs (Hxs1&Hxs2). replace (hd (tl xs)) with 0 by nia. cbn. nia.
    + intros xs Hxs. reflexivity.
  - reflexivity.
Qed.



(** Five silly lemmas *)

Lemma bsum_Nat_not_Quant {ϕ} (I : idx ϕ) (σ : mty (S ϕ)) J Ai :
  bsum_Nat I σ (Quant J Ai) -> False.
Proof. hnf. intros (a&H1&H2). discriminate. Qed.

Lemma bsum_Nat_not_Quant' {ϕ} (I : idx ϕ) A (ρ : mty ϕ) J :
  bsum_Nat I (Quant J A) ρ -> False.
Proof. hnf. intros (a&H1&->). cbn in H1. discriminate. Qed.

Lemma bsum_Quant_not_Nat {ϕ} (I : idx ϕ) (σi : mty (S ϕ)) k :
  bsum_Quant I σi (Nat k) -> False.
Proof. hnf. intros (J&A&H1&H2). discriminate. Qed.

Lemma bsum_Quant_not_Nat' {ϕ} (I : idx ϕ) A (ρ : mty ϕ) :
  bsum_Quant I (Nat A) ρ -> False.
Proof. hnf. intros (a&H1&H2&H3). congruence. Qed.

Lemma bsum_not_Nat_Quant {ϕ} (I1 I2 : idx ϕ) (σ1 σ2 : mty (S ϕ)) (ρ : mty ϕ) :
  bsum_Nat I1 σ1 ρ -> bsum_Quant I2 σ2 ρ -> False.
Proof. hnf. now intros (a&->&->) H % bsum_Quant_not_Nat. Qed.


(** I don't know whether this holds for Quantifiers *)

Lemma bsum_Nat_functional {ϕ} (I : idx ϕ) (τ : mty (S ϕ)) (ρ1 ρ2 : mty ϕ) :
  bsum_Nat I τ ρ1 ->
  bsum_Nat I τ ρ2 ->
  ρ1 = ρ2.
Proof.
  intros (J&H1&->) (K&H2&->).
  f_equal. fext; intros xs.
  rewrite H1 in H2. cbn in H2. injection H2 as H2.
  change (J xs) with ((fun xs => J (tl xs)) (def_nat ::: xs)). change (K xs) with ((fun xs => K (tl xs)) (def_nat ::: xs)).
  rewrite H2. reflexivity.
Qed.



(** *** Substitution and (bounded) sums *)

Lemma msum_subst_var_beta {ϕ} (x : Fin.t (S ϕ)) (y : nat) (i : idx (y + (ϕ -' fin_to_nat x))) (τ1 τ2 ρ : mty (S ϕ)) :
  msum τ1 τ2 ρ ->
  msum (subst_mty_var_beta x y i τ1) (subst_mty_var_beta x y i τ2) (subst_mty_var_beta x y i ρ).
Proof.
  destruct τ1, τ2, ρ; cbn; try tauto.
  - now intros [-> ->].
  - intros (->&->&H).
    repeat_split.
    + reflexivity.
    + setoid_rewrite subst_lty_twice. reflexivity.
    + assert (i2 = fun xs => i0 xs + i1 xs) as -> by (fext; auto). reflexivity.
Qed.


Lemma bsum_subst_beta_ground {ϕ} (i : idx ϕ) (I : idx (S ϕ)) (τ : mty (S (S ϕ))) ρ :
  bsum I τ ρ ->
  bsum (subst_beta_ground_fun i I) (subst_mty_beta_one_ground τ i) (subst_mty_beta_ground ρ i).
Proof.
  intros [H|H]; [left|right]; hnf in H|-*.
  - destruct H as (J&->&->). eexists. split. 2: reflexivity. cbn. reflexivity.
  - destruct H as (J&A&->&->). eexists _,_; repeat_split.
    1: cbn; f_equal.
    2: cbn; reflexivity.
    now setoid_rewrite subst_lty_twice.
Qed.

Lemma bsum_shift {ϕ} (x : Fin.t (S ϕ)) (y : nat) (i : idx ϕ) (I : idx (S ϕ))
      (τ : mty (S (S ϕ))) ρ :
  bsum I τ ρ ->
  bsum (shift_add_fun i I) (mty_shift_one_add τ i) (mty_shift_add ρ i).
Proof.
  intros [H|H]; [left|right]; hnf in H|-*.
  - destruct H as (J&->&->). eexists. split; cbn. 2: reflexivity. reflexivity.
  - destruct H as (J&A&->&->). eexists _,_; repeat_split.
    1: cbn; f_equal.
    2: cbn; reflexivity.
    now setoid_rewrite subst_lty_twice.
Qed.


Lemma bsum_Nat_subst_var_beta {ϕ} (x : Fin.t (S ϕ)) (y : nat) (i : idx (y + (ϕ -' fin_to_nat x))) (I : idx (S ϕ))
      (τ : mty (S (S ϕ))) ρ
      (Heq: S (y + ϕ) = y + S ϕ) :
  bsum_Nat I τ ρ ->
  bsum_Nat (subst_var_beta_fun x y i I) (mty_cast (subst_mty_var_beta (FS x) y i τ) Heq) (subst_mty_var_beta x y i ρ).
Proof.
  intros (J&->&->). eexists. split. 2: reflexivity.
  cbn. unfold idx_cast. f_equal. fext; intros xs.
  erewrite subst_var_beta_fun_FS; cbn. f_equal.
  now rewrite vect_cast_inv.
  Unshelve. abstract lia.
Qed.

Lemma bsum_Quant_subst_var_beta {ϕ} (x : Fin.t (S ϕ)) (y : nat) (i : idx (y + (ϕ -' fin_to_nat x))) (I : idx (S ϕ))
      (τ : mty (S (S ϕ))) ρ
      (Heq: S (y + ϕ) = y + S ϕ) :
  bsum_Quant I τ ρ ->
  bsum_Quant (subst_var_beta_fun x y i I) (mty_cast (subst_mty_var_beta (FS x) y i τ) Heq) (subst_mty_var_beta x y i ρ).
Proof.
  intros (J&A&->&->). eexists _,_; split.
  2:{ cbn. f_equal. fext. intros ys.
      unfold subst_var_beta_fun. f_equal.
      fext; intros a.
      instantiate (* This is essentially a copy of [subst_var_beta_fun] *)
        (1 := fun xs =>
                J (hd xs ::: vect_cast
                      (vect_app (vect_take _ _ (vect_cast (tl xs) _))
                                (i (vect_skip _ _ (vect_cast (tl xs) _)) :::
                                   vect_skip _ _ (vect_skip _ _ (vect_cast (tl xs) _)))) _)).
      cbn. reflexivity.
  }
  cbn. f_equal.
  - unfold idx_cast. fext; intros xs. erewrite subst_var_beta_fun_FS. cbn. unfold subst_var_beta_fun. repeat f_equal.
    all: now erewrite vect_cast_inv.
  - rewrite subst_lty_cast'.
    setoid_rewrite subst_lty_twice; unfold funcomp; cbn. f_equal. fext; intros f xs; cbn.
    unfold idx_cast, subst_var_beta_fun; cbn.
    f_equal. f_equal.
    all: try simp_vect_to_list; auto.
    {
      f_equal. 2: f_equal.
      - now rewrite vect_cast_hd.
      - erewrite !vect_to_list_hd with (def := def_nat). f_equal. vect_to_list. reflexivity.
      - fext; intros z. f_equal. f_equal. simp_vect_to_list. f_equal. f_equal. f_equal. now simp_vect_to_list.
    }
    do 4 f_equal. now simp_vect_to_list.
  Unshelve. abstract lia.
Qed.

Lemma bsum_subst_var_beta {ϕ} (x : Fin.t (S ϕ)) (y : nat) (i : idx (y + (ϕ -' fin_to_nat x))) (I : idx (S ϕ))
      (τ : mty (S (S ϕ))) ρ
      (Heq: S (y + ϕ) = y + S ϕ) :
  bsum I τ ρ ->
  bsum (subst_var_beta_fun x y i I) (mty_cast (subst_mty_var_beta (FS x) y i τ) Heq) (subst_mty_var_beta x y i ρ).
Proof.
  intros [H|H]; [left|right].
  - now eapply bsum_Nat_subst_var_beta.
  - now eapply bsum_Quant_subst_var_beta.
Qed.



(** A few instances *)

Lemma msum_subst_beta_ground {ϕ} (τ1 τ2 ρ : mty (S ϕ)) (i : idx ϕ) :
  msum τ1 τ2 ρ ->
  msum (subst_mty_beta_ground τ1 i) (subst_mty_beta_ground τ2 i) (subst_mty_beta_ground ρ i).
Proof. intros. erewrite !subst_mty_beta_ground_correct. now apply msum_subst_var_beta with (y := 0). Qed.

Lemma msum_shift {ϕ} (τ1 τ2 ρ : mty (S ϕ)) (i : idx ϕ) :
  msum τ1 τ2 ρ ->
  msum (mty_shift_add τ1 i) (mty_shift_add τ2 i) (mty_shift_add ρ i).
Proof. intros. erewrite !mty_shift_add_correct'. now apply msum_subst_var_beta with (y := 1). Qed.


(** *** Merging and splitting *)

(** The indexes are irrelevant in [Σ_{a<I} Nat σ] *)
Lemma bsum_Nat_monotone {ϕ} {I1 I2 : idx ϕ} (σ : mty (S ϕ)) {τ : mty ϕ} :
  bsum_Nat I1 σ τ ->
  bsum_Nat I2 σ τ.
Proof. intros (k&H1&H1'). hnf. eauto. Qed.

Lemma bsum_Quant_monotone {ϕ} {I1 I2 : idx ϕ} {σ : mty (S ϕ)} {τ : mty ϕ} :
  bsum_Quant I1 σ τ ->
  { ρ : mty ϕ & bsum_Quant I2 σ ρ }.
Proof.
  unfold bsum_Quant.
  intros (J&A&->&->). eexists ([ < fun xs : Vector.t nat ϕ => Σ_{a < I2 xs} J (a ::: xs)]⋅ A), J, A. split; reflexivity.
Qed.

Lemma bsum_Quant_monotone' {ϕ} {I1 I2 : idx ϕ} {σ : mty (S ϕ)} {τ : mty ϕ} :
  bsum_Quant I1 σ τ ->
  { ρ : mty ϕ &
    { _ : bsum_Quant I2 σ ρ |
    (mty! fun xs => I1 xs <= I2 xs ⊢ ρ ⊑ τ) /\
    mty! fun xs => I2 xs <= I1 xs ⊢ τ ⊑ ρ } }.
Proof.
  unfold bsum_Quant.
  intros (J&A&->&->). eexists ([ < fun xs : Vector.t nat ϕ => Σ_{a < I2 xs} J (a ::: xs)]⋅ A). eexists. 2: split.
  - exists J,A. split; reflexivity.
  - apply submty_Quant.
    + reflexivity.
    + hnf; intros xs Hxs. now apply Sum_monotone.
  - apply submty_Quant.
    + reflexivity.
    + hnf; intros xs Hxs. now apply Sum_monotone.
Qed.


Lemma bsum_monotone {ϕ} {I1 I2 : idx ϕ} {σ : mty (S ϕ)} {τ : mty ϕ} :
  bsum I2 σ τ ->
  { ρ & bsum I1 σ ρ }.
Proof.
  intros [H | H].
  - epose proof bsum_Nat_monotone H as L.
    exists τ. left. eassumption.
  - epose proof bsum_Quant_monotone H as (ρ&L).
    exists ρ. right. eassumption.
Qed.

Lemma bsum_monotone' {ϕ} {I1 I2 : idx ϕ} {σ : mty (S ϕ)} {τ : mty ϕ} :
  bsum I1 σ τ ->
  { ρ & { _ : (bsum I2 σ ρ) |
    (mty! fun xs => I1 xs <= I2 xs ⊢ ρ ⊑ τ) /\
    mty! fun xs => I2 xs <= I1 xs ⊢ τ ⊑ ρ } }.
Proof.
  intros [H | H].
  - epose proof bsum_Nat_monotone H as L.
    exists τ. eexists. 2: split.
    + left. eassumption.
    + reflexivity.
    + reflexivity.
  - epose proof bsum_Quant_monotone' H as (ρ&(L1&L2)).
    exists ρ. split.
    + right. eassumption.
    + assumption.
Qed.


(* TODO: move *)
Lemma Sum_split (I1 I2 : nat) (f : nat -> nat) :
  I1 <= I2 ->
  (Σ_{a<I1} f a) + (Σ_{a<I2-I1} f (I1 + a)) = (Σ_{a<I2} f a).
Proof.
  intros H.
  pose proof Nat.le_exists_sub _ _ H as (p&->&_).
  replace (p + I1 - I1) with p by nia. replace (p + I1) with (I1 + p) by nia.
  rewrite !Sum_idx_plus. nia.
Qed.


Lemma bsum_Quant_shift {ϕ} {I1 I2 : idx ϕ} {σ : mty (S ϕ)} {τ : mty ϕ} :
  bsum_Quant I1 σ τ ->
  { ρ & bsum_Quant I2 (mty_shift_add σ I1) ρ }.
Proof.
  unfold bsum_Quant. intros (J&A&->&->).
  eexists.
  pose (J' := fun xs : Vector.t nat (S ϕ) => J (I1 (tl xs) + hd xs ::: tl xs)). eexists J'.
  eexists (lty_shift_add A (fun xs => Σ_{d<I1 xs} J (d ::: xs))).
  cbn. split. 2: reflexivity.
  cbn. unfold subst_lty_beta, lty_shift_add, subst_lty_beta_two, shift_add_fun. unfold J'. rewrite !subst_lty_twice; unfold funcomp; cbn.
  f_equal. f_equal. fext; intros j xs. f_equal. f_equal.
  (* rewrite Sum_idx_plus''. *) rewrite Sum_idx_plus. nia.
Qed.

Lemma bsum_Quant_split' {ϕ} {I1 I2 : idx ϕ} {σ : mty (S ϕ)} {τ : mty ϕ} :
  bsum_Quant I2 σ τ ->
  { ρ1 & { ρ2 & bsum_Quant I1 σ ρ1 * bsum_Quant I2 (mty_shift_add σ I1) ρ2 } } % type.
Proof.
  intros H.
  pose proof bsum_Quant_monotone (I2 := I1) H as (ρ1&L1).
  pose proof bsum_Quant_shift (I2 := I2) L1 as (ρ2&L2).
  eauto.
Qed.


(* This proof is essentiall a copy of [bsum_Quant_monotone] + [bsum_Quant_shift] *)
Lemma bsum_Quant_split {ϕ} {I1 I2 : idx ϕ} {σ : mty (S ϕ)} {τ : mty ϕ} :
  bsum_Quant (fun xs => I1 xs + I2 xs) σ τ ->
  { ρ1 & { ρ2 & bsum_Quant I1 σ ρ1 * bsum_Quant I2 (mty_shift_add σ I1) ρ2 * msum ρ1 ρ2 τ } } % type.
Proof.
  unfold bsum_Quant. intros (J&A&->&->).
  eexists ([ < fun xs : Vector.t nat ϕ => Σ_{a < I1 xs} J (a ::: xs)]⋅ A), _; repeat_split.
  - eexists J, A. split. reflexivity. reflexivity.
  - pose (J' := fun xs : Vector.t nat (S ϕ) => J (I1 (tl xs) + hd xs ::: tl xs)). eexists J'.
    exists (lty_shift_add A (fun xs => Σ_{d<I1 xs} J (d ::: xs))).
    cbn. split. 2: reflexivity.
    cbn. unfold subst_lty_beta, lty_shift_add, subst_lty_beta_two, shift_add_fun. unfold J'. rewrite !subst_lty_twice; unfold funcomp; cbn.
    f_equal. f_equal. fext; intros j xs. f_equal. f_equal.
    (* rewrite Sum_idx_plus''. *) rewrite Sum_idx_plus. nia.
  - cbn. repeat_split.
    + reflexivity.
    + reflexivity.
    + intros xs. rewrite Sum_idx_plus. reflexivity.
Qed.


Theorem bsum_split {ϕ} {I1 I2 : idx ϕ} {σ : mty (S ϕ)} {τ : mty ϕ} :
  bsum (fun xs => I1 xs + I2 xs) σ τ ->
  { ρ1 & { ρ2 & bsum I1 σ ρ1 * bsum I2 (mty_shift_add σ I1) ρ2 * msum ρ1 ρ2 τ } } % type.
Proof.
  intros [H1 | H1].
  - hnf in H1. destruct H1 as (J&->&->).
    eexists _,_. repeat_split.
    + hnf. left. hnf. eauto.
    + hnf. left. hnf.
      eexists; split; eauto.
      cbn. reflexivity.
    + apply msum_Nat'.
  - pose proof bsum_Quant_split H1 as (ρ1&ρ2&((L1&L2)&L3)).
    exists ρ1,ρ2. repeat_split; hnf; eauto.
Qed.


