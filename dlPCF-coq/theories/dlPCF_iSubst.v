Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax.
Require Import dlPCF_Types.
Require PCF_Types.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.

Open Scope vector_scope.

Import SigmaTypeNotations.

From Coq Require Import Vector Fin.
From Coq Require Import Lia.

Delimit Scope PCF with PCF.
Open Scope PCF.


Definition idx'_cast {X: Type} {ϕ1 ϕ2} (i : Vector.t nat ϕ1 -> X) (Heq: ϕ2=ϕ1) : Vector.t nat ϕ2 -> X.
Proof. hnf in i|-*. intros. apply i. eapply Vector.cast; eauto. Defined.


Lemma vect_cast_ext' (X : Type) (m n : nat) (xs ys : Vector.t X m) (H1 H2 : m = n) :
  xs = ys ->
  vect_cast xs H1 = vect_cast ys H2.
Proof. intros ->. apply vect_cast_ext. Qed.


Lemma vect_to_list_of_list (X : Type) (xs : list X) :
  vect_to_list (of_list xs) = xs.
Proof. apply to_list_of_list_opp. Qed.

Hint Rewrite vect_to_list_of_list : vect_to_list.


(** Version of the minus function where [x - 0 = x] holds by conversion *)
Fixpoint sub' (m n : nat) :=
  match n with
  | 0 => m
  | S n' => sub' (pred m) n'
  end.

Notation "x -' y" := (sub' x y) (at level 50, left associativity).
(* Check eq_refl : (?[x] -' 0 = ?x). *)

Lemma sub'_correct (m n : nat) : m -' n = m - n.
Proof.
  induction n in m|-*; cbn.
  - lia.
  - rewrite IHn. lia.
Qed.




(** *** Special case: Substitution of the first free variable *)

(** The special case where the variable in 0. [y] free variables are introduced. *)
(** This polymorphic function can be used for indexes ([X := nat]) or for constraints ([X := Prop]). *)
Definition subst_beta_fun {X: Type} {ϕ} (y:nat) (i : idx (y+ϕ)) : (Vector.t nat (S ϕ) -> X) -> (Vector.t nat (y + ϕ) -> X) :=
  fun (f : Vector.t nat (S ϕ) -> X) (xs : Vector.t nat (y+ϕ)) =>
    f (i xs ::: vect_skip _ _ xs).


(** Special case of the above with [y=0], i.e. no new variables *)
Definition subst_beta_ground_fun {X: Type} {ϕ} (i : idx ϕ) : (Vector.t nat (S ϕ) -> X) -> (Vector.t nat ϕ -> X) :=
  fun (f : Vector.t nat (S ϕ) -> X) (xs : Vector.t nat ϕ) => f (i xs ::: xs).

Definition subst_lty_beta_ground {ϕ:nat} (A : lty (S ϕ)) (i : idx ϕ) : lty ϕ :=
  subst_lty A (subst_beta_ground_fun i).
Definition subst_mty_beta_ground {ϕ:nat} (A : mty (S ϕ)) (i : idx ϕ) : mty ϕ :=
  subst_mty A (subst_beta_ground_fun i).

Lemma subst_beta_ground_fun_correct {X : Type} {ϕ} (i : idx ϕ) :
  @subst_beta_ground_fun X ϕ i = @subst_beta_fun X ϕ 0 i.
Proof. reflexivity. Qed.


(** Similar special cases, but for the next variable *)

Definition subst_beta_one_fun {X: Type} {ϕ} (y:nat) (i : idx (y+ϕ)) : (Vector.t nat (S (S ϕ)) -> X) -> (Vector.t nat (S (y + ϕ)) -> X) :=
  fun (f : Vector.t nat (S (S ϕ)) -> X) (xs : Vector.t nat (S (y+ϕ))) =>
    f (hd xs ::: i (tl xs) ::: vect_skip _ _ (tl xs)).

Definition subst_beta_one_ground_fun {X : Type} {ϕ} (I : idx ϕ) : (Vector.t nat (S (S ϕ)) -> X) -> (Vector.t nat (S ϕ) -> X) :=
  (fun f xs => f (hd xs ::: I (tl xs) ::: tl xs)).

Definition subst_lty_beta_one_ground {ϕ} (A : lty (S (S ϕ))) (I : idx ϕ) : lty (S ϕ).
Proof. eapply (subst_lty A). exact (@subst_beta_one_ground_fun nat ϕ I). Defined.
Definition subst_mty_beta_one_ground {ϕ} (τ : mty (S (S ϕ))) (I : idx ϕ) : mty (S ϕ).
Proof. eapply (subst_mty τ). exact (@subst_beta_one_ground_fun nat ϕ I). Defined.

Lemma subst_beta_one_ground_fun_correct {X : Type} {ϕ} (i : idx ϕ) :
  @subst_beta_one_ground_fun X ϕ i = @subst_beta_one_fun X ϕ 0 i.
Proof. reflexivity. Qed.



(** *** General case (substitution of the [x]th variable, introducing [y] fresh variables) *)



(** This polymorphic function can be used for indexes ([X := nat]) or for constraints ([X := Prop]). *)

Definition subst_var_beta_fun {X: Type} {ϕ} (x : Fin.t (S ϕ)) (y : nat)
           (i : idx (y + (ϕ -' fin_to_nat x))) : (Vector.t nat (S ϕ) -> X) -> (Vector.t nat (y + ϕ) -> X).
Proof.
  refine (fun f xs => f _); cbn in *.
  apply vect_cast with (n := (fin_to_nat x + (y + (ϕ -' (fin_to_nat x))))) in xs.
  2:{ abstract (rewrite !sub'_correct; pose proof fin_to_nat_lt x; lia). }
  pose proof (vect_take _ _ xs)  as ys1. (* [x] *)
  pose proof (vect_skip _ _ xs)  as ys2. (* [y + (ϕ - x)] *)
  pose proof (vect_skip _ _ ys2) as ys3. (* [(ϕ - x)] *)
  refine (vect_cast (Vector.append ys1 (i ys2 ::: ys3)) _).
  abstract (rewrite !sub'_correct; pose proof fin_to_nat_lt x; lia).
Defined.


Definition subst_lty_var_beta {ϕ:nat} (x : Fin.t (S ϕ)) (y : nat) (i : idx (y + (ϕ -' fin_to_nat x)))
           (A : lty (S ϕ)) : lty (y + ϕ) := subst_lty A (subst_var_beta_fun (X := nat) x y i).
Definition subst_mty_var_beta {ϕ:nat} (x : Fin.t (S ϕ)) (y : nat) (i : idx (y + (ϕ -' fin_to_nat x)))
           (τ : mty (S ϕ)) : mty (y + ϕ) := subst_mty τ (subst_var_beta_fun (X := nat) x y i).

(** Special case [x=Fin0] *)
Definition subst_lty_beta {ϕ:nat} (y : nat) (i : idx (y + ϕ))
           (A : lty (S ϕ)) : lty (y + ϕ) := subst_lty A (subst_beta_fun (X := nat) y i).
Definition subst_mty_beta {ϕ:nat} (y : nat) (i : idx (y + ϕ))
           (τ : mty (S ϕ)) : mty (y + ϕ) := subst_mty τ (subst_beta_fun (X := nat) y i).


(** We can easily recover [subst_beta_fun], by instantiating [x := Fin0] *)
Lemma subst_beta_fun_correct :
  forall {X:Type} {ϕ} (y : nat) (i : idx (y + ϕ)),
    subst_beta_fun (X := X) y i = subst_var_beta_fun Fin0 y i.
Proof.
  unfold subst_beta_fun, subst_var_beta_fun, idx_cast; intros; fext; intros f xs; cbn.
  f_equal. f_equal.
  - f_equal. now rewrite vect_cast_id.
  - now simp_vect_to_list.
Qed.

Lemma subst_lty_beta_ground_correct {ϕ} (A : lty (S ϕ)) (i : idx ϕ) :
  subst_lty_beta_ground A i = subst_lty_var_beta Fin0 0 i A.
Proof. unfold subst_lty_beta_ground, subst_lty_var_beta. now erewrite !subst_beta_ground_fun_correct, !subst_beta_fun_correct. Qed.
Lemma subst_mty_beta_ground_correct {ϕ} (τ : mty (S ϕ)) (i : idx ϕ) :
  subst_mty_beta_ground τ i = subst_mty_var_beta Fin0 0 i τ.
Proof. unfold subst_mty_beta_ground, subst_mty_var_beta. now erewrite !subst_beta_ground_fun_correct, !subst_beta_fun_correct. Qed.


(** **** Simplification lemmas for specific [x] *)

Lemma subst_beta_fun_Fin0 {X: Type} {ϕ} (y: nat) (i : idx (y+ϕ)) :
  @subst_var_beta_fun X ϕ Fin0 y i = @subst_beta_fun X ϕ y i.
Proof.
  fext; intros f xs. unfold subst_var_beta_fun, subst_beta_fun, idx_cast; cbn.
  f_equal. f_equal.
  - f_equal. apply vect_cast_id.
  - now simp_vect_to_list.
Qed.


Lemma subst_var_beta_fun_Fin0 {X: Type} {ϕ} (y: nat) (i : idx (y + ϕ)) :
  @subst_var_beta_fun X ϕ Fin0 y i = @subst_beta_fun X ϕ y i.
Proof.
  fext; intros f xs. unfold subst_var_beta_fun, subst_beta_fun, idx_cast; cbn.
  f_equal. f_equal.
  - f_equal. apply vect_cast_id.
  - now simp_vect_to_list.
Qed.

Lemma subst_var_beta_fun_FS {X: Type} {ϕ} (y: nat) (x : Fin.t (S ϕ)) (i : idx (y + (ϕ -' (fin_to_nat x))))
      (Heq2: y + S ϕ = S (y + ϕ)) :
  @subst_var_beta_fun X (S ϕ) (FS x) y i =
  fun (f : Vector.t nat (S (S ϕ)) -> X)
    (xs : Vector.t nat (y + (S ϕ))) =>
    let xs' : Vector.t nat (S (y + ϕ)) := vect_cast xs Heq2 in
    @subst_var_beta_fun X ϕ x y
                        i
                        (fun ys : Vector.t nat (S ϕ) => f (hd xs' ::: ys))
                        (tl xs').
Proof.
  fext; intros i' xs.
  unfold subst_var_beta_fun, idx_cast.
  f_equal. simp_vect_to_list.
  unshelve erewrite <- vect_to_list_cast. 2: apply Heq2. set (xs' := vect_cast xs Heq2) in *.

  (* Some book keeping for [xs : Vector.t nat (y + S ϕ)] *)
  pose proof vector_inv_S xs' as (z&xs''&Hxs1).
  assert (vect_to_list xs'' = List.tl (vect_to_list xs')) as Hxs2.
  { subst xs'. rewrite Hxs1. reflexivity. }
  assert (vect_to_list xs = vect_to_list xs') as Hxs3.
  { subst xs'. now rewrite vect_to_list_cast. }

  rewrite !Hxs1, !vect_to_list_eq_cons.
  cbn [fin_to_nat].
  cbn [firstn app]. rewrite !vect_hd_eq, !vect_tl_eq.
  repeat f_equal. simp_vect_to_list. rewrite Hxs2. rewrite skipn_tl, skipn_tl'. congruence.
Qed.

Lemma subst_var_beta_fun_FS'' {X: Type} {ϕ} (y: nat) (x : Fin.t (S ϕ)) (i : idx (y + (ϕ -' fin_to_nat x)))
      (Heq: S (y + ϕ) = y + S ϕ) :
  (fun (i' : Vector.t nat (S (S ϕ)) -> X) (xs : Vector.t nat (S (y + ϕ))) =>
     subst_var_beta_fun x y i (fun ys : Vector.t nat (S ϕ) => i' (hd xs ::: ys)) (tl xs)) =
  (fun (i' : (Vector.t nat (S (S ϕ)) -> X)) (xs: Vector.t nat (S (y + ϕ))) =>
      @subst_var_beta_fun X (S ϕ) (FS x) y i i' (vect_cast xs Heq)).
Proof.
  fext; intros i' xs.
  unfold subst_var_beta_fun, idx'_cast, idx_cast.
  f_equal. simp_vect_to_list.
  pose proof vector_inv_S xs as (z&xs'&->).
  rewrite !vect_to_list_eq_cons.
  cbn [fin_to_nat]. cbn [firstn app]. rewrite !vect_hd_eq, !vect_tl_eq.
  repeat f_equal. simp_vect_to_list. reflexivity.
Qed.




(** From now on, don't simplify [subst_var_beta_fun] using [cbn] *)
Arguments subst_var_beta_fun : simpl never.


Lemma subst_mty_var_beta_eq_Quant {ϕ} (x : Fin.t (S ϕ)) (y : nat) (I : idx ((S ϕ))) (A : lty ((S (S ϕ))))
      (i : idx (y + (ϕ -' fin_to_nat x)))
      (Heq2: S (y + ϕ) = y + S ϕ) :
  subst_mty_var_beta x y i (Quant I A) =
  Quant (subst_var_beta_fun x y i I) (lty_cast (subst_lty_var_beta (Fin.FS x) y i A) Heq2).
Proof.
  cbn. f_equal.
  setoid_rewrite subst_var_beta_fun_FS''.
  now setoid_rewrite subst_lty_cast'.
Qed.


(** Case for [x = Fin1] *)

Lemma subst_lty_beta_one_ground_correct {ϕ} (A : lty (S (S ϕ))) (i : idx ϕ) :
  subst_lty_beta_one_ground A i = subst_lty_var_beta Fin1 0 i A.
Proof.
  unfold subst_lty_beta_one_ground, subst_lty_var_beta. f_equal.
  erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0. cbn.
  rewrite <- subst_beta_ground_fun_correct.
  unfold subst_beta_one_ground_fun, subst_beta_ground_fun.
  fext; intros f xs. f_equal. f_equal.
  - now rewrite vect_cast_hd.
  - f_equal. f_equal. all: now rewrite vect_cast_id.
    Unshelve. reflexivity.
Qed.

Lemma subst_mty_beta_one_ground_correct {ϕ} (τ : mty (S (S ϕ))) (i : idx ϕ) :
  subst_mty_beta_one_ground τ i = subst_mty_var_beta Fin1 0 i τ.
Proof.
  unfold subst_mty_beta_one_ground, subst_mty_var_beta. f_equal.
  erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0. cbn.
  rewrite <- subst_beta_ground_fun_correct.
  unfold subst_beta_one_ground_fun, subst_beta_ground_fun.
  fext; intros f xs. f_equal. f_equal.
  - now rewrite vect_cast_hd.
  - f_equal. f_equal. all: now rewrite vect_cast_id.
    Unshelve. reflexivity.
Qed.



(** *** Positive Shifting *)

(** Positive shifting means replacing a variable [a] by [i + a], where [i] is an index term. *)
(** It can be derived from [subst_var_beta_fun] with [y = 1]. *)


Definition shift_var_add_fun {X : Type} {ϕ} (x : Fin.t (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) :
  (Vector.t nat (S ϕ) -> X) -> Vector.t nat (S ϕ) -> X :=
  subst_var_beta_fun x 1 (fun xs => i (tl xs) + hd xs).

Definition lty_shift_var_add {ϕ} (x : Fin.t (S ϕ)) (A : lty (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) : lty (S ϕ) :=
  subst_lty A (shift_var_add_fun x i).
Definition mty_shift_var_add {ϕ} (x : Fin.t (S ϕ)) (τ : mty (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) : mty (S ϕ) :=
  subst_mty τ (shift_var_add_fun x i).


(** Special case where [x = Fin0] *)
Definition shift_add_fun {X : Type} {ϕ} (i : idx ϕ) : (Vector.t nat (S ϕ) -> X) -> (Vector.t nat (S ϕ) -> X) :=
  fun j xs => j (i (tl xs) + hd xs ::: tl xs).

Definition lty_shift_add {ϕ} (A : lty (S ϕ)) (i : idx ϕ) : lty (S ϕ) :=
  subst_lty A (shift_add_fun i).
Definition mty_shift_add {ϕ} (τ : mty (S ϕ)) (i : idx ϕ) : mty (S ϕ) :=
  subst_mty τ (shift_add_fun i).

Lemma shift_add_fun_correct' {X : Type} {ϕ} (i : idx ϕ) :
  @shift_add_fun X ϕ i = shift_var_add_fun Fin0 i.
Proof.
  unfold shift_add_fun, shift_var_add_fun, subst_var_beta_fun, idx_cast.
  fext; intros j xs. f_equal. simp_vect_to_list. cbn. f_equal. f_equal.
  - f_equal. now simp_vect_to_list.
  - now rewrite vect_cast_hd.
Qed.

Lemma shift_add_fun_correct {X : Type} {ϕ} (i : idx ϕ) :
  @shift_add_fun X ϕ i = subst_var_beta_fun Fin0 1 (fun xs => i (tl xs) + hd xs).
Proof. apply shift_add_fun_correct'. Qed.

Lemma lty_shift_add_correct' {ϕ} (A : lty (S ϕ)) (I : idx ϕ) :
  lty_shift_add A I = subst_lty_var_beta Fin0 1 (fun xs => I (tl xs) + hd xs) A.
Proof. unfold lty_shift_add, subst_lty_var_beta. f_equal. erewrite shift_add_fun_correct; eauto. Qed.
Lemma mty_shift_add_correct' {ϕ} (τ : mty (S ϕ)) (I : idx ϕ) :
  mty_shift_add τ I = subst_mty_var_beta Fin0 1 (fun xs => I (tl xs) + hd xs) τ.
Proof. unfold mty_shift_add, subst_mty_var_beta. f_equal. erewrite shift_add_fun_correct; eauto. Qed.

Lemma lty_shift_add_twice {ϕ} (A : lty (S ϕ)) (i1 i2 : idx ϕ) :
  lty_shift_add (lty_shift_add A i1) i2 = lty_shift_add A (fun xs => i1 xs + i2 xs)
with mty_shift_add_twice {ϕ} (τ : mty (S ϕ)) (i1 i2 : idx ϕ) :
  mty_shift_add (mty_shift_add τ i1) i2 = mty_shift_add τ (fun xs => i1 xs + i2 xs).
Proof.
  - unfold lty_shift_add, shift_add_fun. rewrite subst_lty_twice. f_equal. fext; intros j xs; unfold funcomp; cbn. now rewrite Nat.add_assoc.
  - unfold mty_shift_add, shift_add_fun. rewrite subst_mty_twice. f_equal. fext; intros j xs; unfold funcomp; cbn. now rewrite Nat.add_assoc.
Qed.

Lemma shift_var_add_fun_FS {X: Type} {ϕ} (x : Fin.t (S ϕ)) i :
  shift_var_add_fun (X := X) (FS x) i =
  (fun (i' : Vector.t nat (S (S ϕ)) -> X) (xs : Vector.t nat (S (S ϕ))) =>
     shift_var_add_fun x i (fun ys : Vector.t nat (S ϕ) => i' (hd xs ::: ys)) (tl xs)).
Proof.
  unfold shift_var_add_fun.
  setoid_rewrite subst_var_beta_fun_FS.
  unfold subst_beta_fun, idx_cast. fext; intros f xs.
  f_equal.
  - fext; intros ys. f_equal. now erewrite vect_cast_hd.
  - now rewrite vect_cast_id.
    Unshelve. all: abstract nia.
Qed.


(** Note: You may want to use [mty_shift_add_eq_Quant'] instead *)
Lemma mty_shift_add_eq_Quant {ϕ} (I : idx (S ϕ)) (A : lty (S (S ϕ))) (i : idx ϕ) :
  mty_shift_add (Quant I A) i = Quant (shift_add_fun i I) (lty_shift_var_add Fin1 A i).
Proof.
  cbn. f_equal.
  unfold lty_shift_var_add; f_equal; fext; intros i' xs.
  now rewrite shift_var_add_fun_FS, shift_add_fun_correct.
Qed.

(** Special case where [x = Fin1] *)

Definition shift_one_add_fun {X : Type} {ϕ} (i : idx ϕ) : (Vector.t nat (S (S ϕ)) -> X) -> (Vector.t nat (S (S ϕ)) -> X) :=
  fun j xs => j (hd xs ::: i (tl (tl xs)) + hd (tl xs) ::: tl (tl xs)).

Definition lty_shift_one_add {ϕ} (A : lty (S (S ϕ))) (i : idx ϕ) : lty (S (S ϕ)) :=
  subst_lty A (shift_one_add_fun i).
Definition mty_shift_one_add {ϕ} (τ : mty (S (S ϕ))) (i : idx ϕ) : mty (S (S ϕ)) :=
  subst_mty τ (shift_one_add_fun i).

Lemma shift_one_add_fun_correct' {X : Type} {ϕ} (i : idx ϕ) :
  @shift_one_add_fun X ϕ i = shift_var_add_fun Fin1 i.
Proof.
  unfold shift_one_add_fun, shift_var_add_fun, subst_var_beta_fun, idx_cast.
  fext; intros j xs. f_equal. simp_vect_to_list.
  cbn [fin_to_nat].
  pose proof vector_inv_S xs as (x&xs'&->).
  rewrite vect_to_list_eq_cons. cbn. f_equal. f_equal. f_equal.
  - f_equal. now simp_vect_to_list.
  - now rewrite vect_cast_hd.
Qed.

Lemma shift_one_add_fun_correct {X : Type} {ϕ} (i : idx ϕ) :
  @shift_one_add_fun X ϕ i = subst_var_beta_fun Fin1 1 (fun xs => i (tl xs) + hd xs).
Proof.
  unfold shift_one_add_fun, idx_cast.
  erewrite subst_var_beta_fun_FS. cbn. erewrite subst_var_beta_fun_Fin0.
  unfold subst_beta_fun, idx_cast. cbn.
  fext; intros j xs. f_equal. f_equal.
  - now rewrite vect_cast_hd.
  - f_equal.
    + f_equal.
      * f_equal. now simp_vect_to_list.
      * f_equal. now rewrite vect_cast_id.
    + now rewrite vect_cast_id.
  Unshelve. all: abstract (cbn; lia).
Qed.

Lemma lty_shift_one_add_correct' {ϕ} (A : lty (S (S ϕ))) (I : idx ϕ) :
  lty_shift_one_add A I = subst_lty_var_beta Fin1 1 (fun xs => I (tl xs) + hd xs) A.
Proof. unfold lty_shift_one_add, subst_lty_var_beta. f_equal. erewrite shift_one_add_fun_correct; eauto. Qed.
Lemma mty_shift_one_add_correct' {ϕ} (τ : mty (S (S ϕ))) (I : idx ϕ) :
  mty_shift_one_add τ I = subst_mty_var_beta Fin1 1 (fun xs => I (tl xs) + hd xs) τ.
Proof. unfold mty_shift_one_add, subst_mty_var_beta. f_equal. erewrite shift_one_add_fun_correct; eauto. Qed.

Lemma mty_shift_add_eq_Quant' {ϕ} (I : idx (S ϕ)) (A : lty (S (S ϕ))) (i : idx ϕ) :
  mty_shift_add (Quant I A) i = Quant (shift_add_fun i I) (lty_shift_one_add A i).
Proof. reflexivity. Qed.



(** *** Negative Shifting *)

(** This is analogous to positive shifting; the variable [a] is decremented by [i]. *)

Definition shift_var_sub_fun {X : Type} {ϕ} (x : Fin.t (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) :
  (Vector.t nat (S ϕ) -> X) -> Vector.t nat (S ϕ) -> X :=
  subst_var_beta_fun x 1 (fun xs => hd xs - i (tl xs)).

Definition lty_shift_var_sub {ϕ} (x : Fin.t (S ϕ)) (A : lty (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) : lty (S ϕ) :=
  subst_lty A (shift_var_sub_fun x i).
Definition mty_shift_var_sub {ϕ} (x : Fin.t (S ϕ)) (τ : mty (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) : mty (S ϕ) :=
  subst_mty τ (shift_var_sub_fun x i).


(** Special case where [x = Fin0] *)
Definition shift_sub_fun {X : Type} {ϕ} (i : idx ϕ) : (Vector.t nat (S ϕ) -> X) -> (Vector.t nat (S ϕ) -> X) :=
  fun j xs => j (hd xs - i (tl xs) ::: tl xs).

Definition lty_shift_sub {ϕ} (A : lty (S ϕ)) (i : idx ϕ) : lty (S ϕ) :=
  subst_lty A (shift_sub_fun i).
Definition mty_shift_sub {ϕ} (τ : mty (S ϕ)) (i : idx ϕ) : mty (S ϕ) :=
  subst_mty τ (shift_sub_fun i).

Lemma shift_sub_fun_correct' {X : Type} {ϕ} (i : idx ϕ) :
  @shift_sub_fun X ϕ i = shift_var_sub_fun Fin0 i.
Proof.
  unfold shift_sub_fun, shift_var_sub_fun, subst_var_beta_fun, idx_cast.
  fext; intros j xs. f_equal. simp_vect_to_list. cbn. f_equal. f_equal.
  - f_equal. now simp_vect_to_list.
  - f_equal. now simp_vect_to_list.
Qed.

Lemma shift_sub_fun_correct {X : Type} {ϕ} (i : idx ϕ) :
  @shift_sub_fun X ϕ i = subst_var_beta_fun Fin0 1 (fun xs => hd xs - i (tl xs)).
Proof. apply shift_sub_fun_correct'. Qed.

Lemma lty_shift_sub_correct' {ϕ} (A : lty (S ϕ)) (I : idx ϕ) :
  lty_shift_sub A I = subst_lty_var_beta Fin0 1 (fun xs => hd xs - I (tl xs)) A.
Proof. unfold lty_shift_sub, subst_lty_var_beta. f_equal. erewrite shift_sub_fun_correct; eauto. Qed.
Lemma mty_shift_sub_correct' {ϕ} (τ : mty (S ϕ)) (I : idx ϕ) :
  mty_shift_sub τ I = subst_mty_var_beta Fin0 1 (fun xs => hd xs - I (tl xs)) τ.
Proof. unfold mty_shift_sub, subst_mty_var_beta. f_equal. erewrite shift_sub_fun_correct; eauto. Qed.

Lemma lty_shift_sub_twice {ϕ} (A : lty (S ϕ)) (i1 i2 : idx ϕ) :
  lty_shift_sub (lty_shift_sub A i1) i2 = lty_shift_sub A (fun xs => i1 xs + i2 xs)
with mty_shift_sub_twice {ϕ} (τ : mty (S ϕ)) (i1 i2 : idx ϕ) :
  mty_shift_sub (mty_shift_sub τ i1) i2 = mty_shift_sub τ (fun xs => i1 xs + i2 xs).
Proof.
  - unfold lty_shift_sub, shift_sub_fun. rewrite subst_lty_twice. f_equal. fext; intros j xs; unfold funcomp; cbn.
    f_equal. f_equal. lia.
  - unfold mty_shift_sub, shift_sub_fun. rewrite subst_mty_twice. f_equal. fext; intros j xs; unfold funcomp; cbn.
    f_equal. f_equal. lia.
Qed.

Lemma shift_var_sub_fun_FS {X: Type} {ϕ} (x : Fin.t (S ϕ)) i :
  shift_var_sub_fun (X := X) (FS x) i =
  (fun (i' : Vector.t nat (S (S ϕ)) -> X) (xs : Vector.t nat (S (S ϕ))) =>
     shift_var_sub_fun x i (fun ys : Vector.t nat (S ϕ) => i' (hd xs ::: ys)) (tl xs)).
Proof.
  unfold shift_var_sub_fun.
  setoid_rewrite subst_var_beta_fun_FS.
  unfold subst_beta_fun, idx_cast. fext; intros f xs.
  f_equal.
  - fext; intros ys. f_equal. now erewrite vect_cast_hd.
  - now rewrite vect_cast_id.
    Unshelve. all: abstract nia.
Qed.


(** Note: You may want to use [mty_shift_sub_eq_Quant'] instead *)
Lemma mty_shift_sub_eq_Quant {ϕ} (I : idx (S ϕ)) (A : lty (S (S ϕ))) (i : idx ϕ) :
  mty_shift_sub (Quant I A) i = Quant (shift_sub_fun i I) (lty_shift_var_sub Fin1 A i).
Proof.
  cbn. f_equal.
  unfold lty_shift_var_sub; f_equal; fext; intros i' xs.
  now rewrite shift_var_sub_fun_FS, shift_sub_fun_correct.
Qed.

(** Special case where [x = Fin1] *)

Definition shift_one_sub_fun {X : Type} {ϕ} (i : idx ϕ) : (Vector.t nat (S (S ϕ)) -> X) -> (Vector.t nat (S (S ϕ)) -> X) :=
  fun j xs => j (hd xs :::  hd (tl xs) - i (tl (tl xs)) ::: tl (tl xs)).

Definition lty_shift_one_sub {ϕ} (A : lty (S (S ϕ))) (i : idx ϕ) : lty (S (S ϕ)) :=
  subst_lty A (shift_one_sub_fun i).
Definition mty_shift_one_sub {ϕ} (τ : mty (S (S ϕ))) (i : idx ϕ) : mty (S (S ϕ)) :=
  subst_mty τ (shift_one_sub_fun i).

Lemma shift_one_sub_fun_correct' {X : Type} {ϕ} (i : idx ϕ) :
  @shift_one_sub_fun X ϕ i = shift_var_sub_fun Fin1 i.
Proof.
  unfold shift_one_sub_fun, shift_var_sub_fun, subst_var_beta_fun, idx_cast.
  fext; intros j xs. f_equal. simp_vect_to_list.
  cbn [fin_to_nat].
  pose proof vector_inv_S xs as (x&xs'&->).
  rewrite vect_to_list_eq_cons. cbn. f_equal. f_equal. f_equal.
  - f_equal. now simp_vect_to_list.
  - f_equal. now simp_vect_to_list.
Qed.

Lemma shift_one_sub_fun_correct {X : Type} {ϕ} (i : idx ϕ) :
  @shift_one_sub_fun X ϕ i = subst_var_beta_fun Fin1 1 (fun xs => hd xs - i (tl xs)).
Proof.
  unfold shift_one_sub_fun, idx_cast.
  erewrite subst_var_beta_fun_FS. cbn. erewrite subst_var_beta_fun_Fin0.
  unfold subst_beta_fun, idx_cast. cbn.
  fext; intros j xs. f_equal. f_equal.
  - now rewrite vect_cast_hd.
  - f_equal.
    + f_equal.
      * f_equal. now simp_vect_to_list.
      * f_equal. now rewrite vect_cast_id.
    + now rewrite vect_cast_id.
  Unshelve. all: abstract (cbn; lia).
Qed.

Lemma lty_shift_one_sub_correct' {ϕ} (A : lty (S (S ϕ))) (I : idx ϕ) :
  lty_shift_one_sub A I = subst_lty_var_beta Fin1 1 (fun xs => hd xs - I (tl xs)) A.
Proof. unfold lty_shift_one_sub, subst_lty_var_beta. f_equal. erewrite shift_one_sub_fun_correct; eauto. Qed.
Lemma mty_shift_one_sub_correct' {ϕ} (τ : mty (S (S ϕ))) (I : idx ϕ) :
  mty_shift_one_sub τ I = subst_mty_var_beta Fin1 1 (fun xs => hd xs - I (tl xs)) τ.
Proof. unfold mty_shift_one_sub, subst_mty_var_beta. f_equal. erewrite shift_one_sub_fun_correct; eauto. Qed.

Lemma mty_shift_sub_eq_Quant' {ϕ} (I : idx (S ϕ)) (A : lty (S (S ϕ))) (i : idx ϕ) :
  mty_shift_sub (Quant I A) i = Quant (shift_sub_fun i I) (lty_shift_one_sub A i).
Proof. reflexivity. Qed.



(** If we first shift negative, then positive, we replace [a] by [(i + a) - i], which is extensionally equal to [a] *)

Lemma lty_shift_add_sub {ϕ} (A : lty (S ϕ)) (i : idx ϕ) :
  lty_shift_add (lty_shift_sub A i) i = A.
Proof.
  setoid_rewrite subst_lty_twice. unfold shift_sub_fun, shift_add_fun, funcomp.
  rewrite <- subst_lty_id. f_equal; fext; intros j' xs. unfold id; cbn.
  f_equal. rewrite eta. f_equal. lia.
Qed.



(** *** General closed beta *)


(** Beta subst function for arbitrary variables *)
Definition subst_var_beta_ground_fun {X:Type} {ϕ} (x : Fin.t (S ϕ)) (i : idx (ϕ -' (fin_to_nat x)))
           (f : Vector.t nat (S ϕ) -> X) : Vector.t nat ϕ -> X.
Proof.
  refine (fun xs => f _); cbn in *.
  apply vect_cast with (n := (fin_to_nat x) + ((ϕ -' (fin_to_nat x)))) in xs.
  2:{ abstract (rewrite sub'_correct; pose proof fin_to_nat_lt x; lia). }
  pose proof (vect_take _ _ xs) as ys1. (* [x] *)
  pose proof (vect_skip _ _ xs) as ys2. (* [(S (ϕ - S (fin_to_nat x)))] *)
  refine (vect_cast (Vector.append ys1 (i ys2 ::: ys2)) _).
  abstract (rewrite sub'_correct; pose proof fin_to_nat_lt x; lia).
Defined.

Definition subst_lty_var_beta_ground {ϕ} (x : Fin.t (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) (A : lty (S ϕ)) : lty ϕ :=
  subst_lty A (subst_var_beta_ground_fun x i).
Definition subst_mty_var_beta_ground {ϕ} (x : Fin.t (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) (A : mty (S ϕ)) : mty ϕ :=
  subst_mty A (subst_var_beta_ground_fun x i).

Lemma subst_var_beta_ground_fun_correct {X:Type} {ϕ} (x : Fin.t (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) :
  subst_var_beta_ground_fun (X := X) x i = subst_var_beta_fun x 0 i.
Proof.
  unfold subst_var_beta_ground_fun, subst_var_beta_fun; fext; intros f xs.
  f_equal. simp_vect_to_list. cbn. (do 4 f_equal). now apply vect_cast_ext.
Qed.

Lemma subst_var_beta_ground_fun_Fin0 {X:Type} {ϕ} (i : idx ϕ) :
  subst_var_beta_ground_fun (X := X) Fin0 i = subst_beta_ground_fun i.
Proof.
  unfold subst_var_beta_ground_fun, subst_beta_ground_fun; fext; intros f xs.
  f_equal. simp_vect_to_list. cbn. f_equal. f_equal. now simp_vect_to_list.
Qed.

Lemma firstn_S (X : Type) (xs : list X) (n : nat) (def : X) :
  n < length xs ->
  firstn (S n) xs = List.hd def xs :: firstn n (List.tl xs).
Proof.
  intros Hn. destruct xs; cbn in *.
  - exfalso; lia.
  - reflexivity.
Qed.

Lemma subst_var_beta_ground_fun_FS {X:Type} {ϕ} (x : Fin.t (S ϕ)) (i : idx (ϕ -' (fin_to_nat x))) :
  subst_var_beta_ground_fun (X := X) (FS x) i =
  fun (f : Vector.t nat (S (S ϕ)) -> X)
    (xs : Vector.t nat (S ϕ)) =>
    @subst_var_beta_ground_fun X ϕ x i (fun ys : Vector.t nat (S ϕ) => f (hd xs ::: ys)) (tl xs).
Proof.
  unfold subst_var_beta_ground_fun; fext; intros f xs.
  f_equal. simp_vect_to_list.
  cbn [fin_to_nat sub' pred].
  rewrite skipn_tl, !skipn_tl'.
  erewrite firstn_S.
  2:{ rewrite vect_to_list_length. pose proof fin_to_nat_lt x. lia. }
  rewrite <- vect_to_list_hd. cbn. repeat f_equal. now simp_vect_to_list.
  Unshelve. exact 0.
Qed.

Lemma subst_lty_var_beta_ground_correct {ϕ} (x : Fin.t (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) (A : lty (S ϕ)) :
  subst_lty_var_beta_ground x i A = subst_lty_var_beta x 0 i A.
Proof. unfold subst_lty_var_beta_ground. now rewrite subst_var_beta_ground_fun_correct. Qed.
Lemma subst_mty_var_beta_ground_correct {ϕ} (x : Fin.t (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) (τ : mty (S ϕ)) :
  subst_mty_var_beta_ground x i τ = subst_mty_var_beta x 0 i τ.
Proof. unfold subst_mty_var_beta_ground. now rewrite subst_var_beta_ground_fun_correct. Qed.

Lemma subst_mty_var_beta_ground_eq_Quant {ϕ} (x : Fin.t (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) (I : idx (S ϕ)) (A : lty (S (S ϕ))) :
  subst_mty_var_beta_ground x i (Quant I A) =
  Quant (subst_var_beta_ground_fun x i I)
        (subst_lty_var_beta_ground (Fin.FS x) i A).
Proof. cbn. f_equal. unfold subst_lty_var_beta_ground. now rewrite subst_var_beta_ground_fun_FS. Qed.



(** *** Substitution Lemma for subtyping *)


Definition sem_cast {ϕ1 ϕ2} (Φ Ψ : constr ϕ1) (H : ϕ2 = ϕ1) :
  (sem! Φ ⊨ Ψ) ->
  sem! constr_cast Φ H ⊨ constr_cast Ψ H.
Proof.
  intros H1. unfold constr_cast in *.
  hnf; intros xs Hxs. hnf in H1. specialize H1 with (1 := Hxs).
  unfold idx_cast in *. subst. rewrite vect_cast_id in *. auto.
Qed.


Lemma sublty_cast {ϕ1 ϕ2} (Φ : constr ϕ1) (A B : lty ϕ1) (H0 H1 H2 : ϕ2 = ϕ1) :
  (lty! Φ ⊢ A ⊑ B) ->
  (lty! constr_cast Φ H0 ⊢ lty_cast A H1 ⊑ lty_cast B H2).
Proof. intros. subst. now rewrite constr_cast_id, !lty_cast_id. Qed.

Lemma submty_cast {ϕ1 ϕ2} (Φ : constr ϕ1) (A B : mty ϕ1) (H0 H1 H2 : ϕ2 = ϕ1) :
  (mty! Φ ⊢ A ⊑ B) ->
  (mty! constr_cast Φ H0 ⊢ mty_cast A H1 ⊑ mty_cast B H2).
Proof. intros. subst. now rewrite constr_cast_id, !mty_cast_id. Qed.

Lemma eqlty_cast {ϕ1 ϕ2} (Φ : constr ϕ1) (A B : lty ϕ1) (H0 H1 H2 : ϕ2 = ϕ1) :
  (lty! Φ ⊢ A ≡ B) ->
  (lty! constr_cast Φ H0 ⊢ lty_cast A H1 ≡ lty_cast B H2).
Proof. intros. subst. now rewrite constr_cast_id, !lty_cast_id. Qed.

Lemma eqmty_cast {ϕ1 ϕ2} (Φ : constr ϕ1) (A B : mty ϕ1) (H0 H1 H2 : ϕ2 = ϕ1) :
  (mty! Φ ⊢ A ≡ B) ->
  (mty! constr_cast Φ H0 ⊢ mty_cast A H1 ≡ mty_cast B H2).
Proof. intros. subst. now rewrite constr_cast_id, !mty_cast_id. Qed.


Lemma p_equal (X : Type) (f : X -> Prop) (x y : X) : f x -> x = y -> f y.
Proof. now intros H ->. Qed.


Fixpoint sublty_subst_var_beta {ϕ} (x : Fin.t (S ϕ)) (y : nat) (Φ : constr (S ϕ)) (A B : lty (S ϕ))
         (I : idx (y + (ϕ -' fin_to_nat x)))
         (H : lty! Φ ⊢ A ⊑ B) { struct H } :
  (lty! subst_var_beta_fun x y I Φ ⊢
      subst_lty_var_beta x y I A ⊑ subst_lty_var_beta x y I B)
with submty_subst_var_beta {ϕ} (x : Fin.t (S ϕ)) (y : nat) (Φ : constr (S ϕ)) (σ τ : mty (S ϕ))
         (I : idx (y + (ϕ -' fin_to_nat x)))
         (H : mty! Φ ⊢ σ ⊑ τ) { struct H } :
  (mty! subst_var_beta_fun x y I Φ ⊢
      subst_mty_var_beta x y I σ ⊑ subst_mty_var_beta x y I τ).
Proof.
  { destruct H. cbn. constructor.
    - now apply submty_subst_var_beta.
    - now apply submty_subst_var_beta. }
  { destruct H.
    - cbn. apply submty_Nat.
      { hnf. intros xs Hxs. now apply H. }
    - erewrite !subst_mty_var_beta_eq_Quant.
      apply submty_Quant.
      + eapply sublty_mono_Φ.
        * eapply sublty_cast.
          apply sublty_subst_var_beta with (x := Fin.FS x). exact H.
        * intros xs (Hxs1&Hxs2). unfold constr_cast.
          erewrite subst_var_beta_fun_FS. cbn.
          hnf; split.
          { cbn. rewrite !vect_cast_inv.
            eapply Nat.lt_le_trans; eauto.
          }
          { cbn. rewrite !vect_cast_inv.
            hnf in Hxs2. cbn in Hxs2. apply (p_equal _ Hxs2); clear Hxs2.
            unfold idx_cast. simp_vect_to_list. reflexivity.
          }
      + firstorder.
  }
  Unshelve. all: reflexivity + (abstract lia).
Qed.

Lemma eqlty_subst_var_beta {ϕ} (x : Fin.t (S ϕ)) (y : nat) (Φ : constr (S ϕ)) (A B : lty (S ϕ))
      (I : idx (y + (ϕ -' fin_to_nat x))) :
         (lty! Φ ⊢ A ≡ B) ->
  (lty! subst_var_beta_fun x y I Φ ⊢
      subst_lty_var_beta x y I A ≡ subst_lty_var_beta x y I B).
Proof. now intros [H1 H2]; split; apply sublty_subst_var_beta. Qed.
Lemma eqmty_subst_var_beta {ϕ} (x : Fin.t (S ϕ)) (y : nat) (Φ : constr (S ϕ)) (σ τ : mty (S ϕ))
      (I : idx (y + (ϕ -' fin_to_nat x))) :
         (mty! Φ ⊢ σ ≡ τ) ->
  (mty! subst_var_beta_fun x y I Φ ⊢
        subst_mty_var_beta x y I σ ≡ subst_mty_var_beta x y I τ).
Proof. now intros [H1 H2]; split; apply submty_subst_var_beta. Qed.


(** Special case [x=Fin0] *)
Lemma sublty_subst_beta {ϕ} (y : nat) (Φ : constr (S ϕ)) (A B : lty (S ϕ)) (I : idx (y + ϕ)) :
  (lty! Φ ⊢ A ⊑ B) ->
  (lty! subst_beta_fun y I Φ ⊢
      subst_lty_beta y I A ⊑ subst_lty_beta y I B).
Proof. unfold subst_lty_beta. erewrite !subst_beta_fun_correct. now apply sublty_subst_var_beta. Qed.
Lemma submty_subst_beta {ϕ} (y : nat) (Φ : constr (S ϕ)) (σ τ : mty (S ϕ)) (I : idx (y + ϕ)) :
  (mty! Φ ⊢ σ ⊑ τ) ->
  (mty! subst_beta_fun y I Φ ⊢
      subst_mty_beta y I σ ⊑ subst_mty_beta y I τ).
Proof. unfold subst_mty_beta. erewrite !subst_beta_fun_correct. now apply submty_subst_var_beta. Qed.
Lemma eqlty_subst_beta {ϕ} (y : nat) (Φ : constr (S ϕ)) (A B : lty (S ϕ)) (I : idx (y + ϕ)) :
  (lty! Φ ⊢ A ≡ B) ->
  (lty! subst_beta_fun y I Φ ⊢
      subst_lty_beta y I A ≡ subst_lty_beta y I B).
Proof. now intros [H1 H2]; split; apply sublty_subst_beta. Qed.
Lemma eqmty_subst_beta {ϕ} (y : nat) (Φ : constr (S ϕ)) (σ τ : mty (S ϕ)) (I : idx (y + ϕ)) :
       (mty! Φ ⊢ σ ≡ τ) ->
  (mty! subst_beta_fun y I Φ ⊢
        subst_mty_beta y I σ ≡ subst_mty_beta y I τ).
Proof. now intros [H1 H2]; split; apply submty_subst_beta. Qed.



(** **** Instances *)


(** Instance for shifting (y=1) *)

Lemma sublty_shift_var_add {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (A B : lty (S ϕ)) (I : idx (ϕ -' fin_to_nat x)) :
  (lty! Φ ⊢ A ⊑ B) ->
  (lty! shift_var_add_fun x I Φ ⊢
      lty_shift_var_add x A I ⊑ lty_shift_var_add x B I).
Proof. apply sublty_subst_var_beta with (y := 1). Qed.
Lemma submty_shift_var_add {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (τ1 τ2 : mty (S ϕ)) (I : idx (ϕ -' fin_to_nat x)) :
  (mty! Φ ⊢ τ1 ⊑ τ2) ->
  (mty! shift_var_add_fun x I Φ ⊢
        mty_shift_var_add x τ1 I ⊑ mty_shift_var_add x τ2 I).
Proof. apply submty_subst_var_beta with (y := 1). Qed.
Lemma eqlty_shift_var_add {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (A B : lty (S ϕ)) (I : idx (ϕ -' (fin_to_nat x))) :
  (lty! Φ ⊢ A ≡ B) ->
  (lty! shift_var_add_fun x I Φ ⊢
      lty_shift_var_add x A I ≡ lty_shift_var_add x B I).
Proof. apply eqlty_subst_var_beta with (y := 1). Qed.
Lemma eqmty_shift_var_add {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (τ1 τ2 : mty (S ϕ)) (I : idx (ϕ -' (fin_to_nat x))) :
  (mty! Φ ⊢ τ1 ≡ τ2) ->
  (mty! shift_var_add_fun x I Φ ⊢
        mty_shift_var_add x τ1 I ≡ mty_shift_var_add x τ2 I).
Proof. apply eqmty_subst_var_beta with (y := 1). Qed.

Lemma sublty_shift_var_sub {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (A B : lty (S ϕ)) (I : idx (ϕ -' fin_to_nat x)) :
  (lty! Φ ⊢ A ⊑ B) ->
  (lty! shift_var_sub_fun x I Φ ⊢
      lty_shift_var_sub x A I ⊑ lty_shift_var_sub x B I).
Proof. apply sublty_subst_var_beta with (y := 1). Qed.
Lemma submty_shift_var_sub {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (τ1 τ2 : mty (S ϕ)) (I : idx (ϕ -' fin_to_nat x)) :
  (mty! Φ ⊢ τ1 ⊑ τ2) ->
  (mty! shift_var_sub_fun x I Φ ⊢
        mty_shift_var_sub x τ1 I ⊑ mty_shift_var_sub x τ2 I).
Proof. apply submty_subst_var_beta with (y := 1). Qed.
Lemma eqlty_shift_var_sub {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (A B : lty (S ϕ)) (I : idx (ϕ -' (fin_to_nat x))) :
  (lty! Φ ⊢ A ≡ B) ->
  (lty! shift_var_sub_fun x I Φ ⊢
      lty_shift_var_sub x A I ≡ lty_shift_var_sub x B I).
Proof. apply eqlty_subst_var_beta with (y := 1). Qed.
Lemma eqmty_shift_var_sub {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (τ1 τ2 : mty (S ϕ)) (I : idx (ϕ -' (fin_to_nat x))) :
  (mty! Φ ⊢ τ1 ≡ τ2) ->
  (mty! shift_var_sub_fun x I Φ ⊢
        mty_shift_var_sub x τ1 I ≡ mty_shift_var_sub x τ2 I).
Proof. apply eqmty_subst_var_beta with (y := 1). Qed.


(** Ground case *)

Lemma lty_shift_add_correct {ϕ} (τ : lty (S ϕ)) (I : idx ϕ) :
  lty_shift_add τ I = @lty_shift_var_add ϕ Fin0 τ I
with mty_shift_add_correct {ϕ} (τ : mty (S ϕ)) (I : idx ϕ) :
  mty_shift_add τ I = @mty_shift_var_add ϕ Fin0 τ I.
Proof.
  - intros. unfold lty_shift_var_add, lty_shift_add. f_equal. now rewrite shift_add_fun_correct.
  - intros. unfold mty_shift_var_add, mty_shift_add. f_equal. now rewrite shift_add_fun_correct.
Qed.
Lemma lty_shift_sub_correct {ϕ} (τ : lty (S ϕ)) (I : idx ϕ) :
  lty_shift_sub τ I = @lty_shift_var_sub ϕ Fin0 τ I
with mty_shift_sub_correct {ϕ} (τ : mty (S ϕ)) (I : idx ϕ) :
  mty_shift_sub τ I = @mty_shift_var_sub ϕ Fin0 τ I.
Proof.
  - intros. unfold lty_shift_var_sub, lty_shift_sub. f_equal. now rewrite shift_sub_fun_correct.
  - intros. unfold mty_shift_var_sub, mty_shift_sub. f_equal. now rewrite shift_sub_fun_correct.
Qed.

Lemma sublty_shift_add {ϕ} (A B : lty (S ϕ)) (Φ : constr (S ϕ)) (I : idx ϕ) :
  lty! Φ ⊢ A ⊑ B ->
  (lty! fun xs : Vector.t nat (S ϕ) => Φ (I (tl xs) + hd xs ::: tl xs) ⊢ lty_shift_add A I ⊑ lty_shift_add B I)
with submty_shift_add {ϕ} (τ1 τ2 : mty (S ϕ)) (Φ : constr (S ϕ)) (I : idx ϕ) :
  mty! Φ ⊢ τ1 ⊑ τ2 ->
  (mty! fun xs : Vector.t nat (S ϕ) => Φ (I (tl xs) + hd xs ::: tl xs) ⊢ mty_shift_add τ1 I ⊑ mty_shift_add τ2 I).
Proof.
  - intros. erewrite !lty_shift_add_correct.
    eapply sublty_mono_Φ.
    eapply sublty_shift_var_add; eauto.
    hnf; intros xs Hxs. eauto.
    now rewrite <- shift_add_fun_correct'.
  - intros. erewrite !mty_shift_add_correct.
    eapply submty_mono_Φ.
    eapply submty_shift_var_add; eauto.
    hnf; intros xs Hxs. eauto.
    now rewrite <- shift_add_fun_correct'.
Qed.
Lemma sublty_shift_sub {ϕ} (A B : lty (S ϕ)) (Φ : constr (S ϕ)) (I : idx ϕ) :
  lty! Φ ⊢ A ⊑ B ->
  (lty! fun xs : Vector.t nat (S ϕ) => Φ (hd xs - I (tl xs)::: tl xs) ⊢ lty_shift_sub A I ⊑ lty_shift_sub B I)
with submty_shift_sub {ϕ} (τ1 τ2 : mty (S ϕ)) (Φ : constr (S ϕ)) (I : idx ϕ) :
  mty! Φ ⊢ τ1 ⊑ τ2 ->
  (mty! fun xs : Vector.t nat (S ϕ) => Φ (hd xs - I (tl xs) ::: tl xs) ⊢ mty_shift_sub τ1 I ⊑ mty_shift_sub τ2 I).
Proof.
  - intros. erewrite !lty_shift_sub_correct.
    eapply sublty_mono_Φ.
    eapply sublty_shift_var_sub; eauto.
    hnf; intros xs Hxs. eauto.
    now rewrite <- shift_sub_fun_correct'.
  - intros. erewrite !mty_shift_sub_correct.
    eapply submty_mono_Φ.
    eapply submty_shift_var_sub; eauto.
    hnf; intros xs Hxs. eauto.
    now rewrite <- shift_sub_fun_correct'.
Qed.

Lemma eqlty_shift_add {ϕ} (A B : lty (S ϕ)) (Φ : constr (S ϕ)) (I : idx ϕ)
         (H : lty! Φ ⊢ A ≡ B) :
  (lty! fun xs : Vector.t nat (S ϕ) => Φ (I (tl xs) + hd xs ::: tl xs) ⊢ lty_shift_add A I ≡ lty_shift_add B I)
with eqmty_shift_add {ϕ} (τ1 τ2 : mty (S ϕ)) (Φ : constr (S ϕ)) (I : idx ϕ)
         (H : mty! Φ ⊢ τ1 ≡ τ2) :
  (mty! fun xs : Vector.t nat (S ϕ) => Φ (I (tl xs) + hd xs ::: tl xs) ⊢ mty_shift_add τ1 I ≡ mty_shift_add τ2 I).
Proof.
  - destruct H. now split; apply sublty_shift_add.
  - destruct H. now split; apply submty_shift_add.
Qed.
Lemma eqlty_shift_sub {ϕ} (A B : lty (S ϕ)) (Φ : constr (S ϕ)) (I : idx ϕ)
         (H : lty! Φ ⊢ A ≡ B) :
  (lty! fun xs : Vector.t nat (S ϕ) => Φ (hd xs - I (tl xs) ::: tl xs) ⊢ lty_shift_sub A I ≡ lty_shift_sub B I)
with eqmty_shift_sub {ϕ} (τ1 τ2 : mty (S ϕ)) (Φ : constr (S ϕ)) (I : idx ϕ)
         (H : mty! Φ ⊢ τ1 ≡ τ2) :
  (mty! fun xs : Vector.t nat (S ϕ) => Φ (hd xs - I (tl xs) ::: tl xs) ⊢ mty_shift_sub τ1 I ≡ mty_shift_sub τ2 I).
Proof.
  - destruct H. now split; apply sublty_shift_sub.
  - destruct H. now split; apply submty_shift_sub.
Qed.


(** Shifting of the variable [Fin1] instead of [Fin0] *)
Lemma lty_shift_one_add_correct {ϕ} (τ : lty (S (S ϕ))) (I : idx ϕ) :
  lty_shift_one_add τ I = @lty_shift_var_add (S ϕ) Fin1 τ I
with mty_shift_one_add_correct {ϕ} (τ : mty (S (S ϕ))) (I : idx ϕ) :
  mty_shift_one_add τ I = @mty_shift_var_add (S ϕ) Fin1 τ I.
Proof.
  - intros. unfold lty_shift_var_add, lty_shift_one_add. f_equal. now erewrite shift_one_add_fun_correct.
  - intros. unfold mty_shift_var_add, mty_shift_one_add. f_equal. now erewrite shift_one_add_fun_correct.
Qed.
Lemma lty_shift_one_sub_correct {ϕ} (τ : lty (S (S ϕ))) (I : idx ϕ) :
  lty_shift_one_sub τ I = @lty_shift_var_sub (S ϕ) Fin1 τ I
with mty_shift_one_sub_correct {ϕ} (τ : mty (S (S ϕ))) (I : idx ϕ) :
  mty_shift_one_sub τ I = @mty_shift_var_sub (S ϕ) Fin1 τ I.
Proof.
  - intros. unfold lty_shift_var_sub, lty_shift_one_sub. f_equal. now erewrite shift_one_sub_fun_correct.
  - intros. unfold mty_shift_var_sub, mty_shift_one_sub. f_equal. now erewrite shift_one_sub_fun_correct.
Qed.

Lemma sublty_shift_one_add {ϕ} (A B : lty (S (S ϕ))) (Φ : constr (S (S ϕ))) (I : idx ϕ) :
  lty! Φ ⊢ A ⊑ B ->
  (lty! fun xs => Φ (hd xs ::: I (tl (tl xs)) + hd (tl xs) ::: tl (tl xs)) ⊢ lty_shift_one_add A I ⊑ lty_shift_one_add B I)
with submty_shift_one_add {ϕ} (τ1 τ2 : mty (S (S ϕ))) (Φ : constr (S (S ϕ))) (I : idx ϕ) :
  mty! Φ ⊢ τ1 ⊑ τ2 ->
  (mty! fun xs => Φ (hd xs ::: I (tl (tl xs)) + hd (tl xs) ::: tl (tl xs)) ⊢ mty_shift_one_add τ1 I ⊑ mty_shift_one_add τ2 I).
Proof.
  - intros. erewrite !lty_shift_one_add_correct.
    eapply sublty_mono_Φ.
    eapply sublty_shift_var_add; eauto.
    now rewrite shift_var_add_fun_FS, <- shift_add_fun_correct'.
  - intros. erewrite !mty_shift_one_add_correct.
    eapply submty_mono_Φ.
    eapply submty_shift_var_add; eauto.
    now rewrite shift_var_add_fun_FS, <- shift_add_fun_correct'.
Qed.
Lemma sublty_shift_one_sub {ϕ} (A B : lty (S (S ϕ))) (Φ : constr (S (S ϕ))) (I : idx ϕ) :
  lty! Φ ⊢ A ⊑ B ->
  (lty! fun xs => Φ (hd xs ::: hd (tl xs) - I (tl (tl xs)) ::: tl (tl xs)) ⊢ lty_shift_one_sub A I ⊑ lty_shift_one_sub B I)
with submty_shift_one_sub {ϕ} (τ1 τ2 : mty (S (S ϕ))) (Φ : constr (S (S ϕ))) (I : idx ϕ) :
  mty! Φ ⊢ τ1 ⊑ τ2 ->
  (mty! fun xs => Φ (hd xs ::: hd (tl xs) - I (tl (tl xs)) ::: tl (tl xs)) ⊢ mty_shift_one_sub τ1 I ⊑ mty_shift_one_sub τ2 I).
Proof.
  - intros. erewrite !lty_shift_one_sub_correct.
    eapply sublty_mono_Φ.
    eapply sublty_shift_var_sub; eauto.
    now rewrite shift_var_sub_fun_FS, <- shift_sub_fun_correct'.
  - intros. erewrite !mty_shift_one_sub_correct.
    eapply submty_mono_Φ.
    eapply submty_shift_var_sub; eauto.
    now rewrite shift_var_sub_fun_FS, <- shift_sub_fun_correct'.
Qed.

Lemma eqlty_shift_one_add {ϕ} (A B : lty (S (S ϕ))) (Φ : constr (S (S ϕ))) (I : idx ϕ)
         (H : lty! Φ ⊢ A ≡ B) :
  (lty! fun xs => Φ (hd xs ::: I (tl (tl xs)) + hd (tl xs) ::: tl (tl xs)) ⊢ lty_shift_one_add A I ≡ lty_shift_one_add B I)
with eqmty_shift_one_add {ϕ} (τ1 τ2 : mty (S (S ϕ))) (Φ : constr (S (S ϕ))) (I : idx ϕ)
         (H : mty! Φ ⊢ τ1 ≡ τ2) :
  (mty! fun xs => Φ (hd xs ::: I (tl (tl xs)) + hd (tl xs) ::: tl (tl xs)) ⊢ mty_shift_one_add τ1 I ≡ mty_shift_one_add τ2 I).
Proof.
  - destruct H. now split; apply sublty_shift_one_add.
  - destruct H. now split; apply submty_shift_one_add.
Qed.
Lemma eqlty_shift_one_sub {ϕ} (A B : lty (S (S ϕ))) (Φ : constr (S (S ϕ))) (I : idx ϕ)
         (H : lty! Φ ⊢ A ≡ B) :
  (lty! fun xs => Φ (hd xs ::: hd (tl xs) - I (tl (tl xs)) ::: tl (tl xs)) ⊢ lty_shift_one_sub A I ≡ lty_shift_one_sub B I)
with eqmty_shift_one_sub {ϕ} (τ1 τ2 : mty (S (S ϕ))) (Φ : constr (S (S ϕ))) (I : idx ϕ)
         (H : mty! Φ ⊢ τ1 ≡ τ2) :
  (mty! fun xs => Φ (hd xs ::: hd (tl xs) - I (tl (tl xs)) ::: tl (tl xs)) ⊢ mty_shift_one_sub τ1 I ≡ mty_shift_one_sub τ2 I).
Proof.
  - destruct H. now split; apply sublty_shift_one_sub.
  - destruct H. now split; apply submty_shift_one_sub.
Qed.




(** Ground beta *)

Lemma sublty_subst_var_beta_ground {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (A B : lty (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) :
  lty! Φ ⊢ A ⊑ B ->
  (lty! subst_var_beta_ground_fun x i Φ ⊢ subst_lty_var_beta_ground x i A ⊑ subst_lty_var_beta_ground x i B).
Proof.
  intros. rewrite subst_var_beta_ground_fun_correct, !subst_lty_var_beta_ground_correct.
  now apply sublty_subst_var_beta with (x0 := x) (y := 0).
Qed.
Lemma submty_subst_var_beta_ground {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (σ τ : mty (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) :
  mty! Φ ⊢ σ ⊑ τ ->
  (mty! subst_var_beta_ground_fun x i Φ ⊢ subst_mty_var_beta_ground x i σ ⊑ subst_mty_var_beta_ground x i τ).
Proof.
  intros. rewrite subst_var_beta_ground_fun_correct, !subst_mty_var_beta_ground_correct.
  now apply submty_subst_var_beta with (x0 := x) (y := 0).
Qed.


(** First variable *)

Lemma sublty_subst_beta_ground {ϕ} (A B : lty (S ϕ)) (Φ : constr (S ϕ)) (I : idx ϕ) :
  lty! Φ ⊢ A ⊑ B ->
  (lty! subst_beta_ground_fun I Φ ⊢ subst_lty_beta_ground A I ⊑ subst_lty_beta_ground B I)
with submty_subst_beta_ground {ϕ} (σ τ : mty (S ϕ)) (Φ : constr (S ϕ)) (I : idx ϕ) :
  mty! Φ ⊢ σ ⊑ τ ->
  (mty! subst_beta_ground_fun I Φ ⊢ subst_mty_beta_ground σ I ⊑ subst_mty_beta_ground τ I).
Proof.
  - intros. unfold subst_lty_beta_ground. rewrite !subst_beta_ground_fun_correct.
    change (subst_lty ?X (subst_beta_fun 0 I)) with (subst_lty_beta 0 I X).
    now apply sublty_subst_beta with (y := 0).
  - intros. unfold subst_mty_beta_ground. rewrite !subst_beta_ground_fun_correct.
    change (subst_mty ?X (subst_beta_fun 0 I)) with (subst_mty_beta 0 I X).
    now apply submty_subst_beta with (y := 0).
Qed.

Lemma eqlty_subst_beta_ground {ϕ} (A B : lty (S ϕ)) (Φ : constr (S ϕ)) (I : idx ϕ) :
  lty! Φ ⊢ A ≡ B ->
  (lty! subst_beta_ground_fun I Φ ⊢ subst_lty_beta_ground A I ≡ subst_lty_beta_ground B I)
with eqmty_subst_beta_ground {ϕ} (σ τ : mty (S ϕ)) (Φ : constr (S ϕ)) (I : idx ϕ) :
  mty! Φ ⊢ σ ≡ τ ->
  (mty! subst_beta_ground_fun I Φ ⊢ subst_mty_beta_ground σ I ≡ subst_mty_beta_ground τ I).
Proof.
  - intros. unfold subst_lty_beta_ground. rewrite !subst_beta_ground_fun_correct.
    change (subst_lty ?X (subst_beta_fun 0 I)) with (subst_lty_beta 0 I X).
    now apply eqlty_subst_beta with (y := 0).
  - intros. unfold subst_mty_beta_ground. rewrite !subst_beta_ground_fun_correct.
    change (subst_mty ?X (subst_beta_fun 0 I)) with (subst_mty_beta 0 I X).
    now apply eqmty_subst_beta with (y := 0).
Qed.
