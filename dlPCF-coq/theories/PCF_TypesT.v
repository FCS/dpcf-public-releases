Require Import Common.
Require Import PCF_Syntax PCF_Semantics PCF_StepT PCF_Types.

Open Scope PCF.

Import SigmaTypeNotations.
Import EqNotations.


(** ** Version of [PCF_Types] in [Type] *)



Inductive hastyT (Γ : ctx) : tm -> ty -> Type :=
| tyT_Var x τ :
    Γ x = τ ->
    hastyT Γ (Var x) τ
| tyT_Lam t τ1 τ2 :
    hastyT (τ1 .: Γ) t τ2 ->
    hastyT Γ (Lam t) (Arr τ1 τ2)
| tyT_Fix t τ :
    hastyT (τ .: Γ) (Lam t) τ ->
    hastyT Γ (Fix t) τ
| tyT_App t1 t2 τ1 τ2 :
    hastyT Γ t1 (Arr τ1 τ2) ->
    hastyT Γ t2 τ1 ->
    hastyT Γ (App t1 t2) τ2
| tyT_Ifz t1 t2 t3 τ :
    hastyT Γ t1 Nat ->
    hastyT Γ t2 τ ->
    hastyT Γ t3 τ ->
    hastyT Γ (Ifz t1 t2 t3) τ
| tyT_Const k :
    hastyT Γ (Const k) Nat
| tyT_S t :
    hastyT Γ t Nat ->
    hastyT Γ (Succ t) Nat
| tyT_P t :
    hastyT Γ t Nat ->
    hastyT Γ (Pred t) Nat
.


Lemma hasty_to_hastyT (Γ : ctx) (t : tm) (τ : ty) :
  hasty Γ t τ -> exists (_ : hastyT Γ t τ), True.
Proof. induction 1; firstorder; eauto using hastyT. Qed.

Lemma hastyT_to_hasty (Γ : ctx) (t : tm) (τ : ty) :
  hastyT Γ t τ -> hasty Γ t τ.
Proof. induction 1; firstorder; eauto using hasty. Qed.



(** [PCF.hastyT] is not proof irrelevant; there might be different typings.
Note that this is NOT proofable in the [Prop] version ([PCF.hasty] *)

Goal exists (Γ : ctx) (t : tm) (τ : ty),
    exists (s1 s2 : hastyT Γ t τ), s1 <> s2.
Proof.
  exists (fun _ => Nat). (* Any context *)
  (* [(λx. 0) (λx. x)] admits different typings for the identity. The type is [Nat]. *)
  exists (App (Lam (Const 0)) (Lam vzero)).
  exists Nat.
  unshelve eexists.
  { apply tyT_App with (τ1 := Arr Nat Nat).
    - apply tyT_Lam. apply tyT_Const.
    - apply tyT_Lam. apply tyT_Var. reflexivity. }
  unshelve eexists.
  { apply tyT_App with (τ1 := Arr (Arr Nat Nat) (Arr Nat Nat)).
    - apply tyT_Lam. apply tyT_Const.
    - apply tyT_Lam. apply tyT_Var. reflexivity. }
  congruence.
Qed.


Fixpoint retypeT_free Γ Δ t τ
  (ty: hastyT Γ t τ) { struct ty } :
  (forall i, free i t -> Γ i = Δ i) ->
  hastyT Δ t τ.
Proof.
  intros Hcomp. destruct ty; cbn in *.
  - eapply tyT_Var. abstract (subst; now rewrite Hcomp).
  - eapply tyT_Lam; eauto. eapply retypeT_free; eauto. abstract (eauto; intros [ | ]; cbn; auto).
  - eapply tyT_Fix; eauto. eapply retypeT_free; eauto. abstract (eauto; intros [ | ]; cbn; auto).
  - eapply tyT_App; eauto.
  - eapply tyT_Ifz; eauto.
  - eapply tyT_Const; eauto.
  - eapply tyT_S; eauto.
  - eapply tyT_P; eauto.
Defined.

Lemma retypeT_bound Γ Δ t m τ :
  bound m t ->
  (forall i, i < m -> Γ i = Δ i) ->
  hastyT Γ t τ ->
  hastyT Δ t τ.
Proof.
  intros H1 H2 H3.
  eapply retypeT_free; eauto.
  abstract (intros i Hi; eapply (proj1 (free_bound_iff _ _)) in H1; eauto).
Defined.

Lemma retypeT_closed Γ Δ t τ :
  closed t ->
  hastyT Γ t τ ->
  hastyT Δ t τ.
Proof. intros H1 H2. eapply retypeT_bound. 1,3: eassumption. intros; omega. Defined.


(** *** Inversion lemmas *)

Section Inversion.

  Variable (Γ : ctx) (τ : ty).

  Lemma tyT_Var_inv x :
    hastyT Γ (Var x) τ ->
    Γ x = τ.
  Proof. inversion 1; eauto. Qed.

  Lemma tyT_Var_inv' x :
    forall (s : hastyT Γ (Var x) τ),
      { H : Γ x = τ & s = tyT_Var _ _ H }.
  Proof.
    intros s.
    refine (match s with
            | tyT_Var _ _ _ => _
            end);
      eauto.
  Qed.

  Lemma tyT_Lam_inv t :
    hastyT Γ (Lam t) τ ->
    existsS τ1 τ2 (ty' : hastyT (τ1 .: Γ) t τ2),
      τ = Arr τ1 τ2.
  Proof. inversion 1; eauto. Qed.

  Lemma tyT_Lam_inv' t :
    forall ty : hastyT Γ (Lam t) τ,
    existsS τ1 τ2 (ty' : hastyT (τ1 .: Γ) t τ2) (Heq : Arr τ1 τ2 = τ),
      ty = rew Heq in tyT_Lam ty'.
  Proof.
    intros s.
    refine (match s with
            | @tyT_Lam _ _ τ1 τ2 ty' => _
            end);
      cbn.
    exists τ1, τ2, ty', eq_refl. reflexivity.
  Qed.

  Lemma tyT_Fix_inv t :
    hastyT Γ (Fix t) τ ->
    hastyT (τ .: Γ) (Lam t) τ.
  Proof. inversion 1; eauto. Qed.

  Lemma tyT_Fix_inv' t :
    forall ty : hastyT Γ (Fix t) τ,
    existsS (ty' : hastyT (τ .: Γ) (Lam t) τ),
      ty = tyT_Fix ty'.
  Proof.
    intros s.
    refine (match s with
            | @tyT_Fix _ _ _ _ => _
            end);
      cbn;
      eauto.
  Qed.

  Lemma tyT_App_inv t1 t2 :
    hastyT Γ (App t1 t2) τ ->
    existsS σ,
      (hastyT Γ t1 (Arr σ τ) *
       hastyT Γ t2 σ) % type.
  Proof. inversion 1; cbn; eauto. Qed.

  Lemma tyT_App_inv' t1 t2 :
    forall ty : hastyT Γ (App t1 t2) τ,
    existsS σ (ty1 : hastyT Γ t1 (Arr σ τ)) (ty2 : hastyT Γ t2 σ),
      ty = tyT_App ty1 ty2.
  Proof.
    intros s.
    refine (match s with
            | @tyT_App _ _ _ _ _ _ _ => _
            end);
      cbn;
      eauto.
  Qed.

  Lemma tyT_Ifz_inv t1 t2 t3 :
    hastyT Γ (Ifz t1 t2 t3) τ ->
    (hastyT Γ t1 Nat *
     hastyT Γ t2 τ *
     hastyT Γ t3 τ) % type.
  Proof. inversion 1; cbn; eauto. Qed.

  Lemma tyT_Ifz_inv' t1 t2 t3 :
    forall ty : hastyT Γ (Ifz t1 t2 t3) τ,
    existsS (ty1 : hastyT Γ t1 Nat) (ty2 : hastyT Γ t2 τ) (ty3 : hastyT Γ t3 τ),
      ty = tyT_Ifz ty1 ty2 ty3.
  Proof.
    intros s.
    refine (match s with
            | @tyT_Ifz _ _ _ _ _ _ _ _ => _
            end);
      cbn;
      eauto.
  Qed.

  Lemma tyT_Const_inv' x :
    forall ty : hastyT Γ (Const x) Nat,
      ty = tyT_Const _ _.
  Proof.
    intros s.
    refine (match s with
            | @tyT_Const _ _ => _
            end);
      cbn;
      eauto.
  Qed.

  Lemma tyT_S_inv t :
    hastyT Γ (Succ t) τ ->
    (hastyT Γ t Nat *
     (τ = Nat)) % type.
  Proof. inversion 1; auto. Qed.

  Lemma tyT_S_inv' t :
    forall ty : hastyT Γ (Succ t) Nat,
      existsS (s : hastyT Γ t Nat) ,
      ty = tyT_S s.
  Proof.
    intros s.
    refine (match s with
            | @tyT_S _ _ _ => _
            end);
      cbn;
      eauto.
  Qed.

  Lemma tyT_P_inv t :
    hastyT Γ (Pred t) τ ->
    (hastyT Γ t Nat *
     (τ = Nat)) % type.
  Proof. inversion 1; auto. Qed.

  Lemma tyT_P_inv' t :
    forall ty : hastyT Γ (Pred t) Nat,
      existsS (s : hastyT Γ t Nat) ,
      ty = tyT_P s.
  Proof.
    intros s.
    refine (match s with
            | @tyT_P _ _ _ => _
            end);
      cbn;
      eauto.
  Qed.

End Inversion.



(** *** Skeletons *)

(** The 'skeleton' of a typing precisely describes all possible choices that can be done in a typing.
The only choice is in the application rule: The type [τ1] can be chosen freely.
We will prove that if two typings have the same skeleton, they are equal. *)

Module Skel.

Inductive skel : Type :=
| skel_Var : skel
| skel_Lam : skel -> skel
| skel_Fix : skel -> skel
| skel_App (τ1 : ty) : skel -> skel -> skel
| skel_Ifz : skel -> skel -> skel -> skel
| skel_Const : skel
| skel_S : skel -> skel
| skel_P : skel -> skel
.

End Skel.

Import Skel.



Fixpoint strip (Γ : ctx) (t : tm) (τ : ty) (ty : hastyT Γ t τ) { struct ty } : skel.
Proof.
  destruct ty.
  - apply skel_Var.
  - apply skel_Lam. apply (strip _ _ _ ty).
  - apply skel_Fix. apply (strip _ _ _ ty).
  - apply skel_App.
    + apply τ1.
    + apply (strip _ _ _ ty1).
    + apply (strip _ _ _ ty2).
  - apply skel_Ifz.
    + apply (strip _ _ _ ty1).
    + apply (strip _ _ _ ty2).
    + apply (strip _ _ _ ty3).
  - apply skel_Const.
  - apply skel_S. apply (strip _ _ _ ty).
  - apply skel_P. apply (strip _ _ _ ty).
Defined.


Lemma tyT_skel_unique (Γ : ctx) (t : tm) (τ : ty) (ty1 ty2 : hastyT Γ t τ) :
  strip ty1 = strip ty2 ->
  ty1 = ty2.
Proof.
  induction ty1 in ty2|-*.
  - pose proof tyT_Var_inv' ty2 as (H&->).
    intros _. f_equal. apply ty_UIP.
  - pose proof tyT_Lam_inv' ty2 as (τ1' & τ2' & ty' & Heq & ->); cbn.
    injection Heq as -> ->. rewrite <- ty_eq_rect_eq; cbn.
    intros [= Hstrip]. f_equal. eauto.
  - pose proof tyT_Fix_inv' ty2 as (ty'&->); cbn.
    intros [= Hstrip]. f_equal. auto.
  - pose proof tyT_App_inv' ty2 as (σ&ty'&ty''&->); cbn.
    intros [= -> Hstrip1 Hstrip2]. f_equal; auto.
  - pose proof tyT_Ifz_inv' ty2 as (ty'&ty''&ty'''&->); cbn.
    intros [= Hstrip1 Hstrip2 Hstrip3]. f_equal; auto.
  - pose proof tyT_Const_inv' ty2 as ->. cbn. reflexivity.
  - pose proof tyT_S_inv' ty2 as (ty' & ->). cbn. intros [= Hstrip]. f_equal. auto.
  - pose proof tyT_P_inv' ty2 as (ty' & ->). cbn. intros [= Hstrip]. f_equal. auto.
Qed.



(** Retyping doesn't change the skeleton *)

Lemma retypeT_free_skel Γ Δ t τ
      (ty : hastyT Γ t τ)
      (H : forall i, free i t -> Γ i = Δ i) :
  strip (retypeT_free _ ty H) = strip ty.
Proof. induction ty in Δ,H|-*; cbn; f_equal; auto. Qed.

Opaque retypeT_free.

Opaque retypeT_free.

Lemma retypeT_bound_skel Γ Δ t τ m
      (ty : hastyT Γ t τ)
      (H1 : bound m t)
      (H2 : forall i : nat, i < m -> Γ i = Δ i) :
  strip (retypeT_bound _ H1 H2 ty) = strip ty.
Proof. now setoid_rewrite retypeT_free_skel. Qed.

Opaque retypeT_bound.

Lemma retypeT_closed_skel Γ Δ t τ
      (ty : hastyT Γ t τ)
      (H1 : closed t) :
  strip (retypeT_closed Δ H1 ty) = strip ty.
Proof. now setoid_rewrite retypeT_bound_skel. Qed.

Opaque retypeT_closed.


(** *** Substitution *)


Fixpoint typepresT_nsubst (m x : nat) (Γ Γ' Δ : ctx) (σ : ty) t τ v
         (ty: hastyT Γ t τ) { struct ty } :
  bound m t ->
  closed v ->
  hastyT Δ v σ ->
  ctxExtends Γ m x σ Γ' ->
  hastyT Γ' (nsubst t x v) τ.
Proof.
  intros Hbound Hclos Hty2 Hext.
  destruct ty in x,m,Γ',Δ,σ,v,Hbound,Hclos,Hty2,Hext|-*; cbn in *.
  - (* Var *)
    destruct (Nat.eq_dec x x0) as [->|d].
    + subst. rewrite (proj2 Hext).
      eapply retypeT_closed; eauto.
    + eapply tyT_Var. subst.
      apply (proj1 Hext); auto.

  - (* Lam *)
    specialize (typepresT_nsubst) with (1 := ty) as IHty.
    specialize IHty with (1 := Hbound).
    econstructor.
    { eapply IHty; eauto.
      abstract (split;
                [ intros [ | y] Hy1 Hy2; cbn in *; [reflexivity|eapply (proj1 Hext); lia]
                | cbn; apply (proj2 Hext)
                ]).
    }
  - (* Fix *)
    specialize (typepresT_nsubst) with (1 := ty) as IHty.
    specialize IHty with (1 := Hbound).
    econstructor.
    { eapply IHty; eauto.
      abstract (split;
                [ intros [ | y] Hy1 Hy2; cbn in *; [reflexivity|eapply (proj1 Hext); lia]
                | cbn; apply (proj2 Hext)
                ]).
    }

  (* The rest is trivial *)
  - (* App *) destruct Hbound as (Hbound1&Hbound2). econstructor; eauto.
  - (* Ifz *) destruct Hbound as (Hbound1&Hbound2&Hbound3). now econstructor; eauto.
  - (* Const *) now constructor.
  - (* Succ *) apply tyT_S; eauto.
  - (* Pred *) apply tyT_P; eauto.
Defined.


Lemma ctxExtends_beta τ2 Γ :
  ctxExtends (τ2 .: Γ) 1 0 τ2 Γ.
Proof.
  hnf. split.
  - abstract (intros; exfalso; omega).
  - reflexivity.
Qed.

Lemma typepresT_beta (Γ : ctx) (τ1 τ2 : ty) (t : tm) (v : tm) :
  hastyT (τ2 .: Γ) t τ1 ->
  bound 1 t ->
  val v -> closed v ->
  hastyT Γ v τ2 ->
  hastyT Γ (nbeta1 t v) τ1.
Proof.
  intros.
  eapply typepresT_nsubst; eauto.
  - apply ctxExtends_beta.
Defined.


Lemma typepresT_beta2 (Γ : ctx) (τ1 τ2 τ3 : ty) (t : tm) (v1 v2 : tm) :
  hastyT (τ3 .: (τ2 .: Γ)) t τ1 ->
  hastyT Γ v1 τ3 ->
  hastyT Γ v2 τ2 ->
  bound 2 t ->
  val v1 -> val v2 -> closed v1 -> closed v2 ->
  hastyT Γ (nbeta2 t v1 v2) τ1.
Proof.
  (* Forwards-style proof *)
  intros Hty1 Hty2 Hty3 Hbound Hv1 Hv2 Hc1 Hc2.
  epose proof typepresT_nsubst (x := 1) Hty1 Hbound Hc2 Hty3 as L1. spec_assert L1.
  { split.
    - intros. assert (y = 0) as -> by (revert H H0; clear_all; abstract lia). cbn. shelve.
    - cbn. reflexivity. }
  assert (bound 2 (nsubst t 1 v2)) as L2.
  { eapply nsubst_bound; eauto. }
  epose proof typepresT_nsubst (x := 0) L1 L2 Hc1 Hty2 as L3. spec_assert L3.
  { split.
    - intros. assert (y = 1) as -> by (revert H H0; clear_all; abstract lia). shelve.
    - shelve. }
  apply L3.
  Unshelve. instantiate (1 := τ3 .: S >> Γ). all: cbn; reflexivity.
Defined.



Definition skel_err : skel. now constructor. Qed.

Fixpoint skel_subst (x : nat) (t : tm) (s1 : skel) (s2 : skel) { struct s1 } : skel :=
  match s1, t with
  | skel_Var, Var y =>
    if Nat.eq_dec x y then s2 else skel_Var
  | skel_Lam s1', Lam t => skel_Lam (skel_subst (S x) t s1' s2)
  | skel_Fix s1', Fix t => skel_Fix (skel_subst (S x) (Lam t) s1' s2)
  | skel_App τ1 s1' s1'', App t1 t2 =>
    skel_App τ1 (skel_subst x t1 s1' s2) (skel_subst x t2 s1'' s2)
  | skel_Ifz s1' s1'' s1''', Ifz t1 t2 t3 =>
    skel_Ifz (skel_subst x t1 s1' s2) (skel_subst x t2 s1'' s2) (skel_subst x t3 s1''' s2)
  | skel_Const, _ => skel_Const
  | skel_S s1', Succ t => skel_S (skel_subst x t s1' s2)
  | skel_P s1', Pred t => skel_P (skel_subst x t s1' s2)
  | _, _ => skel_err
  end.


Lemma typepresT_subst_skel (Γ Δ Γ' : ctx) (x m : nat) (τ σ : ty) (t : tm) (v : tm)
      (ty1: hastyT Γ t τ)
      (Hbound: bound m t)
      (Hclos: closed v)
      (ty2: hastyT Δ v σ)
      (Hext: ctxExtends Γ m x σ Γ') :
  strip (typepresT_nsubst ty1 Hbound Hclos ty2 Hext) =
  skel_subst x t (strip ty1) (strip ty2).
Proof.
  induction ty1 in x,m,Γ',Δ,σ,v,Hbound,Hclos,ty2,Hext|-*; cbn.
  - destruct (Nat.eq_dec x x0) as [->|d]; subst; cbn.
    + destruct (proj2 Hext); cbn. now rewrite retypeT_closed_skel.
    + reflexivity.
  - f_equal. now rewrite IHty1.
  - f_equal. erewrite <- IHty1; auto.
  - destruct Hbound; cbn. f_equal; auto.
  - destruct Hbound; cbn. destruct a; cbn. f_equal; auto.
  - reflexivity.
  - f_equal. auto.
  - f_equal. auto.
Qed.


(** *** Subject reduction *)

(** Note that we use the informative semantics [stepT]. *)


Lemma stepT_app_inv (t1 t2 : tm) κ (t' : tm) :
  stepT (App t1 t2) κ t' ->
  { t1' & stepT t1 κ t1' ** t' = App t1' t2 } +
  (val t1 ** { t2' & stepT t2 κ t2' ** t' = App t1 t2' }) +
  (val t2 ** { t1' | t1 = Lam t1' /\ t' = nbeta1 t1' t2 /\ κ = β }) +
  (val t2 ** { t1' | t1 = Fix t1' /\ t' = nbeta2 t1' t2 (Fix t1') /\ κ = β }).
Proof. inversion 1; subst; eauto 7. Qed.

Lemma stepT_ifz_inv t1 t2 t3 t κ :
  stepT (Ifz t1 t2 t3) κ t ->
  { t1' & stepT t1 κ t1' ** t = Ifz t1' t2 t3 } +
  (t1 = Zero /\ κ = ϵ /\ t = t2) +
  { k | t1 = Const (S k) /\ κ = ϵ /\ t = t3 }.
Proof. intros H. inv H; eauto 8. Qed.

Lemma stepT_s_inv t1 t κ :
  stepT (Succ t1) κ t ->
  { t1' & stepT t1 κ t1' ** t = Succ t1' } +
  { k | t1 = Const k /\ κ = ϵ /\ t = Const (S k) }.
Proof. intros H. inv H; eauto 5. Qed.

Lemma stepT_p_inv t1 t κ :
  stepT (Pred t1) κ t ->
  { t1' & stepT t1 κ t1' ** t = Pred t1' } +
  { k | t1 = Const k /\ κ = ϵ /\ t = Const (pred k) }.
Proof. intros H. inv H; eauto 5. Qed.



Lemma tyT_congr_tm (Γ : ctx) (t t' : tm) (τ : ty) :
  hastyT Γ t τ -> t = t' -> hastyT Γ t' τ.
Proof. congruence. Defined.

Lemma tyT_congr_tm_skel (Γ : ctx) (t t' : tm) (τ : ty) (Hty: hastyT Γ t τ) (Heq: t = t') :
  strip (tyT_congr_tm Hty Heq) = strip Hty.
Proof. subst. reflexivity. Qed.



Fixpoint preservationT (Γ : ctx) κ (t t' : tm) τ
         (Hty : hastyT Γ t τ) { struct Hty } :
  stepT t κ t' ->
  closed t ->
  hastyT Γ t' τ.
Proof.
  intros Hstep Hclos. destruct Hty in κ,t',Hstep,Hclos|-*; closed_inv.
  all: try now stuck'.
  - (* App *)
    pose proof stepT_app_inv Hstep as
        [[[(t1' & Hstep' & ->) |  (Hval1 & t2' & Hstep' & ->)] | (Hval2 & t1' & -> & -> & ->)] | (Hval2 & t1' & -> & -> & ->) ].
    + (* left *) econstructor; eauto.
    + (* right *) econstructor; eauto.
    + (* lambda beta *)
      eapply typepresT_beta; eauto.
      { now pose proof tyT_Lam_inv' Hty1 as (? & ? & Hty1' & [= <- <-] & ->). }
    + (* fix beta *)
      pose proof Hty1 as Hty1_backup.
      pose proof tyT_Fix_inv' Hty1 as (Hty1' & ->).
      eapply typepresT_beta2; eauto. 2: isVal.
      now pose proof tyT_Lam_inv' Hty1' as (? & ? & Hty1'' & [= -> ->] & ->).
  - (* Ifz *)
    apply stepT_ifz_inv in Hstep as
        [ [ (t1'&H&->) | (->&->&->) ] | (k&->&->&->) ].
    + econstructor; eauto.
    + eauto.
    + eauto.
  - (* Succ *)
    apply stepT_s_inv in Hstep as
        [ (t1'&Hstep&->) | (k&->&->&->) ].
    + apply tyT_S; eauto.
    + apply tyT_Const.
  - (* Pred *)
    apply stepT_p_inv in Hstep as
        [ (t1'&Hstep&->) | (k&->&->&->) ].
    + apply tyT_P; eauto.
    + apply tyT_Const.
Defined.


Definition def_skel : skel. Proof. exact skel_Var. Qed.

Fixpoint skel_red (t : tm) (s : skel) : skel :=
  match s with
  | skel_Var | skel_Lam _ | skel_Fix _ | skel_Const => def_skel (* stuck *)
  | skel_S s =>
    match t with
    | Succ (Const _) => skel_Const
    | Succ t => skel_S (skel_red t s)
    | _ => def_skel
    end
  | skel_P s =>
    match t with
    | Pred (Const _) => skel_Const
    | Pred t => skel_P (skel_red t s)
    | _ => def_skel
    end
  | skel_Ifz s1 s2 s3 =>
    match t with
    | Ifz t1 t2 t3 =>
      match t1 with
      | Const 0 => s2
      | Const (S _) => s3
      | _ => skel_Ifz (skel_red t1 s1) s2 s3
      end
    | _ => def_skel
    end
  | skel_App τ s1 s2 =>
    match t with
    | App t1 t2 =>
      if valb t1 then
        if valb t2 then
          match s1, t1 with
          | skel_Lam s1', Lam t' => skel_subst 0 t' s1' s2
          | skel_Fix (skel_Lam s1'), Fix t' =>
            skel_subst 0 (nsubst t' 1 (Fix t')) (skel_subst 1 t' s1' s1) s2
          | _, _ => def_skel
          end
        else skel_App τ s1 (skel_red t2 s2)
      else skel_App τ (skel_red t1 s1) s2
    | _ => def_skel
    end
  end.


Lemma preservationT_skel (Γ : ctx) (κ : stepKind) (t t' : tm) (τ : ty) (Hty: hastyT Γ t τ) (Hstep : stepT t κ t') (Hclos : closed t) :
  strip (preservationT Hty Hstep Hclos) = skel_red t (strip Hty).
Proof.
  induction Hty in κ,t',Hstep,Hclos|-*.
  all: try now (exfalso; stuck').
  - (* App *) cbn.
    destruct (closed_app_inv Hclos).
    destruct (stepT_app_inv Hstep) as
        [[[(t1' & Hstep' & ->) |  (Hval1 & t2' & Hstep' & ->)] | (Hval2 & t1' & -> & -> & ->)] | (Hval2 & t1' & -> & -> & ->) ];
      cbn -[skel_subst].
    + rewrite valb_false by (intros Hval; stuck'). now erewrite IHHty1.
    + rewrite valb_true by assumption. rewrite valb_false by (intros Hval; stuck'). now rewrite IHHty2.
    + setoid_rewrite typepresT_subst_skel.
      rewrite valb_true by assumption.
      destruct (tyT_Lam_inv' Hty1) as (? & ? & Hty1' & Heq & ->).
      injection Heq as -> ->; pose proof ty_UIP_refl Heq as ->; cbn -[skel_subst]. reflexivity.
    + rewrite valb_true by assumption.
      destruct (tyT_Fix_inv' Hty1) as (Hty1' & ->); cbn.
      repeat setoid_rewrite typepresT_subst_skel; cbn.
      destruct (tyT_Lam_inv' Hty1') as (? & ? & Hty1'' & Heq & ->).
      injection Heq as -> ->; pose proof ty_UIP_refl Heq as ->; cbn -[skel_subst].
      reflexivity.
  - (* Ifz *) cbn.
    destruct (closed_ifz_inv Hclos) as (Hclos1 & Hclos2 & Hclos3).
    destruct (stepT_ifz_inv Hstep) as [ [ (t1'&H&->) | (->&->&->) ] | (k&->&->&->) ]; cbn.
    + assert (forall k, t1 <> Const k).
      { intros k ->. stuck'. }
      rewrite IHHty1. destruct t1; congruence.
    + reflexivity.
    + reflexivity.
  - (* S *) cbn.
    destruct (stepT_s_inv Hstep) as [ (t1'&Hstep'&->) | (k&->&->&->) ]; cbn.
    + assert (forall k, t <> Const k).
      { intros k ->. inv Hstep. stuck'. }
      destruct t; congruence.
    + reflexivity.
  - (* P *) cbn.
    destruct (stepT_p_inv Hstep) as [ (t1'&Hstep'&->) | (k&->&->&->) ]; cbn.
    + assert (forall k, t <> Const k).
      { intros k ->. inv Hstep. stuck'. }
      destruct t; congruence.
    + reflexivity.
Qed.

Opaque preservationT.
