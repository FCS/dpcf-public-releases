Require Export unscoped.
Require Export header_extensible.

Section tm.
Inductive tm  : Type :=
  | var_tm : ( nat ) -> tm 
  | Lam : ( tm   ) -> tm 
  | Fix : ( tm   ) -> tm 
  | App : ( tm   ) -> ( tm   ) -> tm 
  | Ifz : ( tm   ) -> ( tm   ) -> ( tm   ) -> tm 
  | Const : ( nat   ) -> tm 
  | Pred : ( tm   ) -> tm 
  | Succ : ( tm   ) -> tm .

Lemma congr_Lam  { s0 : tm   } { t0 : tm   } (H1 : s0 = t0) : Lam  s0 = Lam  t0 .
Proof. congruence. Qed.

Lemma congr_Fix  { s0 : tm   } { t0 : tm   } (H1 : s0 = t0) : Fix  s0 = Fix  t0 .
Proof. congruence. Qed.

Lemma congr_App  { s0 : tm   } { s1 : tm   } { t0 : tm   } { t1 : tm   } (H1 : s0 = t0) (H2 : s1 = t1) : App  s0 s1 = App  t0 t1 .
Proof. congruence. Qed.

Lemma congr_Ifz  { s0 : tm   } { s1 : tm   } { s2 : tm   } { t0 : tm   } { t1 : tm   } { t2 : tm   } (H1 : s0 = t0) (H2 : s1 = t1) (H3 : s2 = t2) : Ifz  s0 s1 s2 = Ifz  t0 t1 t2 .
Proof. congruence. Qed.

Lemma congr_Const  { s0 : nat   } { t0 : nat   } (H1 : s0 = t0) : Const  s0 = Const  t0 .
Proof. congruence. Qed.

Lemma congr_Pred  { s0 : tm   } { t0 : tm   } (H1 : s0 = t0) : Pred  s0 = Pred  t0 .
Proof. congruence. Qed.

Lemma congr_Succ  { s0 : tm   } { t0 : tm   } (H1 : s0 = t0) : Succ  s0 = Succ  t0 .
Proof. congruence. Qed.

Definition upRen_tm_tm   (xi : ( nat ) -> nat) : ( nat ) -> nat :=
  (up_ren) xi.

Fixpoint ren_tm   (xitm : ( nat ) -> nat) (s : tm ) : tm  :=
    match s return tm  with
    | var_tm  s => (var_tm ) (xitm s)
    | Lam  s0 => Lam  ((ren_tm (upRen_tm_tm xitm)) s0)
    | Fix  s0 => Fix  ((ren_tm (upRen_tm_tm (upRen_tm_tm xitm))) s0)
    | App  s0 s1 => App  ((ren_tm xitm) s0) ((ren_tm xitm) s1)
    | Ifz  s0 s1 s2 => Ifz  ((ren_tm xitm) s0) ((ren_tm xitm) s1) ((ren_tm xitm) s2)
    | Const  s0 => Const  ((fun x => x) s0)
    | Pred  s0 => Pred  ((ren_tm xitm) s0)
    | Succ  s0 => Succ  ((ren_tm xitm) s0)
    end.

Definition up_tm_tm   (sigma : ( nat ) -> tm ) : ( nat ) -> tm  :=
  (scons) ((var_tm ) (var_zero)) ((funcomp) (ren_tm (shift)) sigma).

Fixpoint subst_tm   (sigmatm : ( nat ) -> tm ) (s : tm ) : tm  :=
    match s return tm  with
    | var_tm  s => sigmatm s
    | Lam  s0 => Lam  ((subst_tm (up_tm_tm sigmatm)) s0)
    | Fix  s0 => Fix  ((subst_tm (up_tm_tm (up_tm_tm sigmatm))) s0)
    | App  s0 s1 => App  ((subst_tm sigmatm) s0) ((subst_tm sigmatm) s1)
    | Ifz  s0 s1 s2 => Ifz  ((subst_tm sigmatm) s0) ((subst_tm sigmatm) s1) ((subst_tm sigmatm) s2)
    | Const  s0 => Const  ((fun x => x) s0)
    | Pred  s0 => Pred  ((subst_tm sigmatm) s0)
    | Succ  s0 => Succ  ((subst_tm sigmatm) s0)
    end.

Definition upId_tm_tm  (sigma : ( nat ) -> tm ) (Eq : forall x, sigma x = (var_tm ) x) : forall x, (up_tm_tm sigma) x = (var_tm ) x :=
  fun n => match n with
  | S nat_n => (ap) (ren_tm (shift)) (Eq nat_n)
  | 0 => eq_refl
  end.

Fixpoint idSubst_tm  (sigmatm : ( nat ) -> tm ) (Eqtm : forall x, sigmatm x = (var_tm ) x) (s : tm ) : subst_tm sigmatm s = s :=
    match s return subst_tm sigmatm s = s with
    | var_tm  s => Eqtm s
    | Lam  s0 => congr_Lam ((idSubst_tm (up_tm_tm sigmatm) (upId_tm_tm (_) Eqtm)) s0)
    | Fix  s0 => congr_Fix ((idSubst_tm (up_tm_tm (up_tm_tm sigmatm)) (upId_tm_tm (_) (upId_tm_tm (_) Eqtm))) s0)
    | App  s0 s1 => congr_App ((idSubst_tm sigmatm Eqtm) s0) ((idSubst_tm sigmatm Eqtm) s1)
    | Ifz  s0 s1 s2 => congr_Ifz ((idSubst_tm sigmatm Eqtm) s0) ((idSubst_tm sigmatm Eqtm) s1) ((idSubst_tm sigmatm Eqtm) s2)
    | Const  s0 => congr_Const ((fun x => (eq_refl) x) s0)
    | Pred  s0 => congr_Pred ((idSubst_tm sigmatm Eqtm) s0)
    | Succ  s0 => congr_Succ ((idSubst_tm sigmatm Eqtm) s0)
    end.

Definition upExtRen_tm_tm   (xi : ( nat ) -> nat) (zeta : ( nat ) -> nat) (Eq : forall x, xi x = zeta x) : forall x, (upRen_tm_tm xi) x = (upRen_tm_tm zeta) x :=
  fun n => match n with
  | S nat_n => (ap) (shift) (Eq nat_n)
  | 0 => eq_refl
  end.

Fixpoint extRen_tm   (xitm : ( nat ) -> nat) (zetatm : ( nat ) -> nat) (Eqtm : forall x, xitm x = zetatm x) (s : tm ) : ren_tm xitm s = ren_tm zetatm s :=
    match s return ren_tm xitm s = ren_tm zetatm s with
    | var_tm  s => (ap) (var_tm ) (Eqtm s)
    | Lam  s0 => congr_Lam ((extRen_tm (upRen_tm_tm xitm) (upRen_tm_tm zetatm) (upExtRen_tm_tm (_) (_) Eqtm)) s0)
    | Fix  s0 => congr_Fix ((extRen_tm (upRen_tm_tm (upRen_tm_tm xitm)) (upRen_tm_tm (upRen_tm_tm zetatm)) (upExtRen_tm_tm (_) (_) (upExtRen_tm_tm (_) (_) Eqtm))) s0)
    | App  s0 s1 => congr_App ((extRen_tm xitm zetatm Eqtm) s0) ((extRen_tm xitm zetatm Eqtm) s1)
    | Ifz  s0 s1 s2 => congr_Ifz ((extRen_tm xitm zetatm Eqtm) s0) ((extRen_tm xitm zetatm Eqtm) s1) ((extRen_tm xitm zetatm Eqtm) s2)
    | Const  s0 => congr_Const ((fun x => (eq_refl) x) s0)
    | Pred  s0 => congr_Pred ((extRen_tm xitm zetatm Eqtm) s0)
    | Succ  s0 => congr_Succ ((extRen_tm xitm zetatm Eqtm) s0)
    end.

Definition upExt_tm_tm   (sigma : ( nat ) -> tm ) (tau : ( nat ) -> tm ) (Eq : forall x, sigma x = tau x) : forall x, (up_tm_tm sigma) x = (up_tm_tm tau) x :=
  fun n => match n with
  | S nat_n => (ap) (ren_tm (shift)) (Eq nat_n)
  | 0 => eq_refl
  end.

Fixpoint ext_tm   (sigmatm : ( nat ) -> tm ) (tautm : ( nat ) -> tm ) (Eqtm : forall x, sigmatm x = tautm x) (s : tm ) : subst_tm sigmatm s = subst_tm tautm s :=
    match s return subst_tm sigmatm s = subst_tm tautm s with
    | var_tm  s => Eqtm s
    | Lam  s0 => congr_Lam ((ext_tm (up_tm_tm sigmatm) (up_tm_tm tautm) (upExt_tm_tm (_) (_) Eqtm)) s0)
    | Fix  s0 => congr_Fix ((ext_tm (up_tm_tm (up_tm_tm sigmatm)) (up_tm_tm (up_tm_tm tautm)) (upExt_tm_tm (_) (_) (upExt_tm_tm (_) (_) Eqtm))) s0)
    | App  s0 s1 => congr_App ((ext_tm sigmatm tautm Eqtm) s0) ((ext_tm sigmatm tautm Eqtm) s1)
    | Ifz  s0 s1 s2 => congr_Ifz ((ext_tm sigmatm tautm Eqtm) s0) ((ext_tm sigmatm tautm Eqtm) s1) ((ext_tm sigmatm tautm Eqtm) s2)
    | Const  s0 => congr_Const ((fun x => (eq_refl) x) s0)
    | Pred  s0 => congr_Pred ((ext_tm sigmatm tautm Eqtm) s0)
    | Succ  s0 => congr_Succ ((ext_tm sigmatm tautm Eqtm) s0)
    end.

Definition up_ren_ren_tm_tm    (xi : ( nat ) -> nat) (tau : ( nat ) -> nat) (theta : ( nat ) -> nat) (Eq : forall x, ((funcomp) tau xi) x = theta x) : forall x, ((funcomp) (upRen_tm_tm tau) (upRen_tm_tm xi)) x = (upRen_tm_tm theta) x :=
  up_ren_ren xi tau theta Eq.

Fixpoint compRenRen_tm    (xitm : ( nat ) -> nat) (zetatm : ( nat ) -> nat) (rhotm : ( nat ) -> nat) (Eqtm : forall x, ((funcomp) zetatm xitm) x = rhotm x) (s : tm ) : ren_tm zetatm (ren_tm xitm s) = ren_tm rhotm s :=
    match s return ren_tm zetatm (ren_tm xitm s) = ren_tm rhotm s with
    | var_tm  s => (ap) (var_tm ) (Eqtm s)
    | Lam  s0 => congr_Lam ((compRenRen_tm (upRen_tm_tm xitm) (upRen_tm_tm zetatm) (upRen_tm_tm rhotm) (up_ren_ren (_) (_) (_) Eqtm)) s0)
    | Fix  s0 => congr_Fix ((compRenRen_tm (upRen_tm_tm (upRen_tm_tm xitm)) (upRen_tm_tm (upRen_tm_tm zetatm)) (upRen_tm_tm (upRen_tm_tm rhotm)) (up_ren_ren (_) (_) (_) (up_ren_ren (_) (_) (_) Eqtm))) s0)
    | App  s0 s1 => congr_App ((compRenRen_tm xitm zetatm rhotm Eqtm) s0) ((compRenRen_tm xitm zetatm rhotm Eqtm) s1)
    | Ifz  s0 s1 s2 => congr_Ifz ((compRenRen_tm xitm zetatm rhotm Eqtm) s0) ((compRenRen_tm xitm zetatm rhotm Eqtm) s1) ((compRenRen_tm xitm zetatm rhotm Eqtm) s2)
    | Const  s0 => congr_Const ((fun x => (eq_refl) x) s0)
    | Pred  s0 => congr_Pred ((compRenRen_tm xitm zetatm rhotm Eqtm) s0)
    | Succ  s0 => congr_Succ ((compRenRen_tm xitm zetatm rhotm Eqtm) s0)
    end.

Definition up_ren_subst_tm_tm    (xi : ( nat ) -> nat) (tau : ( nat ) -> tm ) (theta : ( nat ) -> tm ) (Eq : forall x, ((funcomp) tau xi) x = theta x) : forall x, ((funcomp) (up_tm_tm tau) (upRen_tm_tm xi)) x = (up_tm_tm theta) x :=
  fun n => match n with
  | S nat_n => (ap) (ren_tm (shift)) (Eq nat_n)
  | 0 => eq_refl
  end.

Fixpoint compRenSubst_tm    (xitm : ( nat ) -> nat) (tautm : ( nat ) -> tm ) (thetatm : ( nat ) -> tm ) (Eqtm : forall x, ((funcomp) tautm xitm) x = thetatm x) (s : tm ) : subst_tm tautm (ren_tm xitm s) = subst_tm thetatm s :=
    match s return subst_tm tautm (ren_tm xitm s) = subst_tm thetatm s with
    | var_tm  s => Eqtm s
    | Lam  s0 => congr_Lam ((compRenSubst_tm (upRen_tm_tm xitm) (up_tm_tm tautm) (up_tm_tm thetatm) (up_ren_subst_tm_tm (_) (_) (_) Eqtm)) s0)
    | Fix  s0 => congr_Fix ((compRenSubst_tm (upRen_tm_tm (upRen_tm_tm xitm)) (up_tm_tm (up_tm_tm tautm)) (up_tm_tm (up_tm_tm thetatm)) (up_ren_subst_tm_tm (_) (_) (_) (up_ren_subst_tm_tm (_) (_) (_) Eqtm))) s0)
    | App  s0 s1 => congr_App ((compRenSubst_tm xitm tautm thetatm Eqtm) s0) ((compRenSubst_tm xitm tautm thetatm Eqtm) s1)
    | Ifz  s0 s1 s2 => congr_Ifz ((compRenSubst_tm xitm tautm thetatm Eqtm) s0) ((compRenSubst_tm xitm tautm thetatm Eqtm) s1) ((compRenSubst_tm xitm tautm thetatm Eqtm) s2)
    | Const  s0 => congr_Const ((fun x => (eq_refl) x) s0)
    | Pred  s0 => congr_Pred ((compRenSubst_tm xitm tautm thetatm Eqtm) s0)
    | Succ  s0 => congr_Succ ((compRenSubst_tm xitm tautm thetatm Eqtm) s0)
    end.

Definition up_subst_ren_tm_tm    (sigma : ( nat ) -> tm ) (zetatm : ( nat ) -> nat) (theta : ( nat ) -> tm ) (Eq : forall x, ((funcomp) (ren_tm zetatm) sigma) x = theta x) : forall x, ((funcomp) (ren_tm (upRen_tm_tm zetatm)) (up_tm_tm sigma)) x = (up_tm_tm theta) x :=
  fun n => match n with
  | S nat_n => (eq_trans) (compRenRen_tm (shift) (upRen_tm_tm zetatm) ((funcomp) (shift) zetatm) (fun x => eq_refl) (sigma nat_n)) ((eq_trans) ((eq_sym) (compRenRen_tm zetatm (shift) ((funcomp) (shift) zetatm) (fun x => eq_refl) (sigma nat_n))) ((ap) (ren_tm (shift)) (Eq nat_n)))
  | 0 => eq_refl
  end.

Fixpoint compSubstRen_tm    (sigmatm : ( nat ) -> tm ) (zetatm : ( nat ) -> nat) (thetatm : ( nat ) -> tm ) (Eqtm : forall x, ((funcomp) (ren_tm zetatm) sigmatm) x = thetatm x) (s : tm ) : ren_tm zetatm (subst_tm sigmatm s) = subst_tm thetatm s :=
    match s return ren_tm zetatm (subst_tm sigmatm s) = subst_tm thetatm s with
    | var_tm  s => Eqtm s
    | Lam  s0 => congr_Lam ((compSubstRen_tm (up_tm_tm sigmatm) (upRen_tm_tm zetatm) (up_tm_tm thetatm) (up_subst_ren_tm_tm (_) (_) (_) Eqtm)) s0)
    | Fix  s0 => congr_Fix ((compSubstRen_tm (up_tm_tm (up_tm_tm sigmatm)) (upRen_tm_tm (upRen_tm_tm zetatm)) (up_tm_tm (up_tm_tm thetatm)) (up_subst_ren_tm_tm (_) (_) (_) (up_subst_ren_tm_tm (_) (_) (_) Eqtm))) s0)
    | App  s0 s1 => congr_App ((compSubstRen_tm sigmatm zetatm thetatm Eqtm) s0) ((compSubstRen_tm sigmatm zetatm thetatm Eqtm) s1)
    | Ifz  s0 s1 s2 => congr_Ifz ((compSubstRen_tm sigmatm zetatm thetatm Eqtm) s0) ((compSubstRen_tm sigmatm zetatm thetatm Eqtm) s1) ((compSubstRen_tm sigmatm zetatm thetatm Eqtm) s2)
    | Const  s0 => congr_Const ((fun x => (eq_refl) x) s0)
    | Pred  s0 => congr_Pred ((compSubstRen_tm sigmatm zetatm thetatm Eqtm) s0)
    | Succ  s0 => congr_Succ ((compSubstRen_tm sigmatm zetatm thetatm Eqtm) s0)
    end.

Definition up_subst_subst_tm_tm    (sigma : ( nat ) -> tm ) (tautm : ( nat ) -> tm ) (theta : ( nat ) -> tm ) (Eq : forall x, ((funcomp) (subst_tm tautm) sigma) x = theta x) : forall x, ((funcomp) (subst_tm (up_tm_tm tautm)) (up_tm_tm sigma)) x = (up_tm_tm theta) x :=
  fun n => match n with
  | S nat_n => (eq_trans) (compRenSubst_tm (shift) (up_tm_tm tautm) ((funcomp) (up_tm_tm tautm) (shift)) (fun x => eq_refl) (sigma nat_n)) ((eq_trans) ((eq_sym) (compSubstRen_tm tautm (shift) ((funcomp) (ren_tm (shift)) tautm) (fun x => eq_refl) (sigma nat_n))) ((ap) (ren_tm (shift)) (Eq nat_n)))
  | 0 => eq_refl
  end.

Fixpoint compSubstSubst_tm    (sigmatm : ( nat ) -> tm ) (tautm : ( nat ) -> tm ) (thetatm : ( nat ) -> tm ) (Eqtm : forall x, ((funcomp) (subst_tm tautm) sigmatm) x = thetatm x) (s : tm ) : subst_tm tautm (subst_tm sigmatm s) = subst_tm thetatm s :=
    match s return subst_tm tautm (subst_tm sigmatm s) = subst_tm thetatm s with
    | var_tm  s => Eqtm s
    | Lam  s0 => congr_Lam ((compSubstSubst_tm (up_tm_tm sigmatm) (up_tm_tm tautm) (up_tm_tm thetatm) (up_subst_subst_tm_tm (_) (_) (_) Eqtm)) s0)
    | Fix  s0 => congr_Fix ((compSubstSubst_tm (up_tm_tm (up_tm_tm sigmatm)) (up_tm_tm (up_tm_tm tautm)) (up_tm_tm (up_tm_tm thetatm)) (up_subst_subst_tm_tm (_) (_) (_) (up_subst_subst_tm_tm (_) (_) (_) Eqtm))) s0)
    | App  s0 s1 => congr_App ((compSubstSubst_tm sigmatm tautm thetatm Eqtm) s0) ((compSubstSubst_tm sigmatm tautm thetatm Eqtm) s1)
    | Ifz  s0 s1 s2 => congr_Ifz ((compSubstSubst_tm sigmatm tautm thetatm Eqtm) s0) ((compSubstSubst_tm sigmatm tautm thetatm Eqtm) s1) ((compSubstSubst_tm sigmatm tautm thetatm Eqtm) s2)
    | Const  s0 => congr_Const ((fun x => (eq_refl) x) s0)
    | Pred  s0 => congr_Pred ((compSubstSubst_tm sigmatm tautm thetatm Eqtm) s0)
    | Succ  s0 => congr_Succ ((compSubstSubst_tm sigmatm tautm thetatm Eqtm) s0)
    end.

Definition rinstInst_up_tm_tm   (xi : ( nat ) -> nat) (sigma : ( nat ) -> tm ) (Eq : forall x, ((funcomp) (var_tm ) xi) x = sigma x) : forall x, ((funcomp) (var_tm ) (upRen_tm_tm xi)) x = (up_tm_tm sigma) x :=
  fun n => match n with
  | S nat_n => (ap) (ren_tm (shift)) (Eq nat_n)
  | 0 => eq_refl
  end.

Fixpoint rinst_inst_tm   (xitm : ( nat ) -> nat) (sigmatm : ( nat ) -> tm ) (Eqtm : forall x, ((funcomp) (var_tm ) xitm) x = sigmatm x) (s : tm ) : ren_tm xitm s = subst_tm sigmatm s :=
    match s return ren_tm xitm s = subst_tm sigmatm s with
    | var_tm  s => Eqtm s
    | Lam  s0 => congr_Lam ((rinst_inst_tm (upRen_tm_tm xitm) (up_tm_tm sigmatm) (rinstInst_up_tm_tm (_) (_) Eqtm)) s0)
    | Fix  s0 => congr_Fix ((rinst_inst_tm (upRen_tm_tm (upRen_tm_tm xitm)) (up_tm_tm (up_tm_tm sigmatm)) (rinstInst_up_tm_tm (_) (_) (rinstInst_up_tm_tm (_) (_) Eqtm))) s0)
    | App  s0 s1 => congr_App ((rinst_inst_tm xitm sigmatm Eqtm) s0) ((rinst_inst_tm xitm sigmatm Eqtm) s1)
    | Ifz  s0 s1 s2 => congr_Ifz ((rinst_inst_tm xitm sigmatm Eqtm) s0) ((rinst_inst_tm xitm sigmatm Eqtm) s1) ((rinst_inst_tm xitm sigmatm Eqtm) s2)
    | Const  s0 => congr_Const ((fun x => (eq_refl) x) s0)
    | Pred  s0 => congr_Pred ((rinst_inst_tm xitm sigmatm Eqtm) s0)
    | Succ  s0 => congr_Succ ((rinst_inst_tm xitm sigmatm Eqtm) s0)
    end.

Lemma rinstInst_tm   (xitm : ( nat ) -> nat) : ren_tm xitm = subst_tm ((funcomp) (var_tm ) xitm) .
Proof. exact ((FunctionalExtensionality.functional_extensionality _ _ ) (fun x => rinst_inst_tm xitm (_) (fun n => eq_refl) x)). Qed.

Lemma instId_tm  : subst_tm (var_tm ) = id .
Proof. exact ((FunctionalExtensionality.functional_extensionality _ _ ) (fun x => idSubst_tm (var_tm ) (fun n => eq_refl) ((id) x))). Qed.

Lemma rinstId_tm  : @ren_tm   (id) = id .
Proof. exact ((eq_trans) (rinstInst_tm ((id) (_))) instId_tm). Qed.

Lemma varL_tm   (sigmatm : ( nat ) -> tm ) : (funcomp) (subst_tm sigmatm) (var_tm ) = sigmatm .
Proof. exact ((FunctionalExtensionality.functional_extensionality _ _ ) (fun x => eq_refl)). Qed.

Lemma varLRen_tm   (xitm : ( nat ) -> nat) : (funcomp) (ren_tm xitm) (var_tm ) = (funcomp) (var_tm ) xitm .
Proof. exact ((FunctionalExtensionality.functional_extensionality _ _ ) (fun x => eq_refl)). Qed.

Lemma compComp_tm    (sigmatm : ( nat ) -> tm ) (tautm : ( nat ) -> tm ) (s : tm ) : subst_tm tautm (subst_tm sigmatm s) = subst_tm ((funcomp) (subst_tm tautm) sigmatm) s .
Proof. exact (compSubstSubst_tm sigmatm tautm (_) (fun n => eq_refl) s). Qed.

Lemma compComp'_tm    (sigmatm : ( nat ) -> tm ) (tautm : ( nat ) -> tm ) : (funcomp) (subst_tm tautm) (subst_tm sigmatm) = subst_tm ((funcomp) (subst_tm tautm) sigmatm) .
Proof. exact ((FunctionalExtensionality.functional_extensionality _ _ ) (fun n => compComp_tm sigmatm tautm n)). Qed.

Lemma compRen_tm    (sigmatm : ( nat ) -> tm ) (zetatm : ( nat ) -> nat) (s : tm ) : ren_tm zetatm (subst_tm sigmatm s) = subst_tm ((funcomp) (ren_tm zetatm) sigmatm) s .
Proof. exact (compSubstRen_tm sigmatm zetatm (_) (fun n => eq_refl) s). Qed.

Lemma compRen'_tm    (sigmatm : ( nat ) -> tm ) (zetatm : ( nat ) -> nat) : (funcomp) (ren_tm zetatm) (subst_tm sigmatm) = subst_tm ((funcomp) (ren_tm zetatm) sigmatm) .
Proof. exact ((FunctionalExtensionality.functional_extensionality _ _ ) (fun n => compRen_tm sigmatm zetatm n)). Qed.

Lemma renComp_tm    (xitm : ( nat ) -> nat) (tautm : ( nat ) -> tm ) (s : tm ) : subst_tm tautm (ren_tm xitm s) = subst_tm ((funcomp) tautm xitm) s .
Proof. exact (compRenSubst_tm xitm tautm (_) (fun n => eq_refl) s). Qed.

Lemma renComp'_tm    (xitm : ( nat ) -> nat) (tautm : ( nat ) -> tm ) : (funcomp) (subst_tm tautm) (ren_tm xitm) = subst_tm ((funcomp) tautm xitm) .
Proof. exact ((FunctionalExtensionality.functional_extensionality _ _ ) (fun n => renComp_tm xitm tautm n)). Qed.

Lemma renRen_tm    (xitm : ( nat ) -> nat) (zetatm : ( nat ) -> nat) (s : tm ) : ren_tm zetatm (ren_tm xitm s) = ren_tm ((funcomp) zetatm xitm) s .
Proof. exact (compRenRen_tm xitm zetatm (_) (fun n => eq_refl) s). Qed.

Lemma renRen'_tm    (xitm : ( nat ) -> nat) (zetatm : ( nat ) -> nat) : (funcomp) (ren_tm zetatm) (ren_tm xitm) = ren_tm ((funcomp) zetatm xitm) .
Proof. exact ((FunctionalExtensionality.functional_extensionality _ _ ) (fun n => renRen_tm xitm zetatm n)). Qed.

End tm.

















Global Instance Subst_tm   : Subst1 (( nat ) -> tm ) (tm ) (tm ) := @subst_tm   .

Global Instance Ren_tm   : Ren1 (( nat ) -> nat) (tm ) (tm ) := @ren_tm   .

Global Instance VarInstance_tm  : Var (nat) (tm ) := @var_tm  .

Notation "x '__tm'" := (var_tm x) (at level 5, format "x __tm") : subst_scope.

Notation "x '__tm'" := (@ids (_) (_) VarInstance_tm x) (at level 5, only printing, format "x __tm") : subst_scope.

Notation "'var'" := (var_tm) (only printing, at level 1) : subst_scope.

Class Up_tm X Y := up_tm : ( X ) -> Y.

Notation "↑__tm" := (up_tm) (only printing) : subst_scope.

Notation "↑__tm" := (up_tm_tm) (only printing) : subst_scope.

Global Instance Up_tm_tm   : Up_tm (_) (_) := @up_tm_tm   .

Notation "s [ sigmatm ]" := (subst_tm sigmatm s) (at level 7, left associativity, only printing) : subst_scope.

Notation "[ sigmatm ]" := (subst_tm sigmatm) (at level 1, left associativity, only printing) : fscope.

Notation "s ⟨ xitm ⟩" := (ren_tm xitm s) (at level 7, left associativity, only printing) : subst_scope.

Notation "⟨ xitm ⟩" := (ren_tm xitm) (at level 1, left associativity, only printing) : fscope.

Ltac auto_unfold := repeat unfold subst1,  subst2,  Subst1,  Subst2,  ids,  ren1,  ren2,  Ren1,  Ren2,  Subst_tm,  Ren_tm,  VarInstance_tm.

Tactic Notation "auto_unfold" "in" "*" := repeat unfold subst1,  subst2,  Subst1,  Subst2,  ids,  ren1,  ren2,  Ren1,  Ren2,  Subst_tm,  Ren_tm,  VarInstance_tm in *.

Ltac asimpl' := repeat first [progress rewrite ?instId_tm| progress rewrite ?compComp_tm| progress rewrite ?compComp'_tm| progress rewrite ?rinstId_tm| progress rewrite ?compRen_tm| progress rewrite ?compRen'_tm| progress rewrite ?renComp_tm| progress rewrite ?renComp'_tm| progress rewrite ?renRen_tm| progress rewrite ?renRen'_tm| progress rewrite ?varL_tm| progress rewrite ?varLRen_tm| progress (unfold up_ren, upRen_tm_tm, up_tm_tm)| progress (cbn [subst_tm ren_tm])| fsimpl].

Ltac asimpl := repeat try unfold_funcomp; auto_unfold in *; asimpl'; repeat try unfold_funcomp.

Tactic Notation "asimpl" "in" hyp(J) := revert J; asimpl; intros J.

Tactic Notation "auto_case" := auto_case (asimpl; cbn; eauto).

Tactic Notation "asimpl" "in" "*" := auto_unfold in *; repeat first [progress rewrite ?instId_tm in *| progress rewrite ?compComp_tm in *| progress rewrite ?compComp'_tm in *| progress rewrite ?rinstId_tm in *| progress rewrite ?compRen_tm in *| progress rewrite ?compRen'_tm in *| progress rewrite ?renComp_tm in *| progress rewrite ?renComp'_tm in *| progress rewrite ?renRen_tm in *| progress rewrite ?renRen'_tm in *| progress rewrite ?varL_tm in *| progress rewrite ?varLRen_tm in *| progress (unfold up_ren, upRen_tm_tm, up_tm_tm in *)| progress (cbn [subst_tm ren_tm] in *)| fsimpl in *].

Ltac substify := auto_unfold; try repeat (erewrite rinstInst_tm).

Ltac renamify := auto_unfold; try repeat (erewrite <- rinstInst_tm).
