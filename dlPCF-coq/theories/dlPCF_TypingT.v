Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax.
Require Import dlPCF_iSubst.
Require Import ForestCard dlPCF_Types dlPCF_Sum dlPCF_Contexts.
Require Import dlPCF_Typing.

Require PCF_Types PCF_TypesT.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.
Import SigmaTypeNotations.
Open Scope vector_scope.

From Coq Require Import Vector Fin.

Open Scope PCF.


Implicit Types (ϕ : nat).


(** ** dlPCF Type system in [Type] *)

(** This file declares a variant of [hasty] in [Type] instead of [Prop].
We only use this version for the completeness proof, because we need to eliminate the typing
into [Type], e.g. in the grounding theorem (in the file [dlPCF_Uniformisation]).

Also we parametrise typings by a PCF typing proof ([PCF_TypesT.hastyT], also in [Type]).

We will never need a beta substitution lemma. Note that for substitution we would have to generate
a new PCF typing shape.

Only bidirectional subtyping (≡) is allowed. This restriction is needed in the joining lemmas.
*)


Require Import Eqdep_dec.


(** Abbreviation module *)
Module PCF.
  Include PCF_Types.
  Include PCF_TypesT.

  (* TODO: Move *)
  Fixpoint ty_size (τ : ty) : nat :=
    match τ with
    | Nat => 1
    | Arr τ1 τ2 => S (ty_size τ1 + ty_size τ2)
    end.

  Lemma ty_size_ge1 (τ : ty) :
    1 <= ty_size τ.
  Proof. induction τ; cbn; lia. Qed.

End PCF.

Module Skel := PCF.Skel.

Import Skel.


Implicit Type s : skel.




Reserved Notation "Ty! Φ ; Γ '⊢(' i ')' t : τ @ s"
         (at level 80, t at level 99, format "Ty!  Φ ;  Γ  '⊢(' i ')' '/' '['  t  ':'  τ ']'  @  s").

(**
This is an extension of [hasty].
- Defined in [Type] instead of [Prop]
- Skeleton parameter
- [≡] instead of [⊑]
*)

Inductive hastyT {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) :
  forall (t : tm) (ρ : mty ϕ), skel -> Type :=
| tyT_Var x ρ :
    (* Subtyping *)
    forall (Hsub: mty! Φ ⊢ Γ x ≡ ρ)
      (HM: sem! Φ ⊨ fun xs => 0 = M xs),
      (Ty! Φ; Γ ⊢(M) (Var x) : ρ @ skel_Var)
| tyT_Lam (t : tm) (I : idx ϕ) (Δ : ctx (S ϕ)) (σ τ : mty (S ϕ)) (K : idx (S ϕ))
         (ρ : mty ϕ) (* The type after subtyping *)
         (Γ' : ctx ϕ) (* The context sum of [Γb], before subtyping *)
         (s : skel) :
    forall (Hty: @hastyT (S ϕ) (fun xs => hd xs < I (tl xs) /\ Φ (tl xs)) (σ .: Δ) K t τ s)
      (Hbsum: ctxBSum (Lam t) I Δ Γ')
      (* Subtyping *)
      (HΓ: ctx! (Lam t); Φ ⊢ Γ ≡ Γ') (* The variable "x" is excluded from the subtyping *)
      (HM: sem! Φ ⊨ fun xs => (I xs + Σ_{a<I xs} K (a ::: xs)) = M xs)
      (Hρ: mty! Φ ⊢ [<I] ⋅ σ ⊸ τ ≡ ρ),
      (Ty! Φ; Γ ⊢(M) Lam t : ρ @ skel_Lam s)
| tyT_Fix (t : tm) (* The term! *)
         (Ib : idx (S ϕ)) (* depends on [b<H] *)
         (Δ : ctx (S ϕ)) (* depends on [b<H] *)
         (Jb : idx (S ϕ)) (* depends on [b<H] *)
         (A : lty (S (S ϕ))) (* depends on [a<I] and [b<H] *)
         (B : lty (S (S ϕ))) (* depends on [a<1] and [b<H] *)
         (K : idx ϕ) (* Fixed number of times the fixpoint will be used *)
         (card1 : idx (S (S ϕ))) (* Value of the cardinal in the second line. Depends on [a] and [b]. *)
         (card2 : idx (S ϕ)) (* Value of the cardinal that is substituted for [b] in the last line. Depends on [a]. *)
         (cardH : idx ϕ) (* Cardinal of the entire tree *)
         (Γ' : ctx ϕ) (* The context sum of [Γb], before subtyping *)
         (ρ : mty ϕ) (* The type after subtyping *)
         (s : skel) :
    forall (Hty: (* Typing obligation *)
         @hastyT (S ϕ)
                (fun xs =>
                   let b := hd xs in
                   let xs' := tl xs in
                   b < cardH (xs') /\ Φ xs') ([< Ib] ⋅ A .: Δ)
                (Jb) (Lam t) ([<iConst 1] ⋅ B) s)
      (* Subtyping obligation *)
      (Hsub:
         lty! fun xs => let a := hd xs in
                     let b := hd (tl xs) in
                     let xs' : Vector.t nat ϕ := tl (tl xs) in
                     a < Ib (b ::: xs') /\ b < cardH xs' /\ Φ xs' ⊢
             subst_lty B
             (fun f xs =>
                let a := hd xs in
                let b := hd (tl xs) in
                let xs' := tl (tl xs) in
                f (0 ::: S (card1 xs + b) ::: xs'))
             ≡ A)
      (* Three forest cardinalities *)
      (HcardH: sem! Φ ⊨ fun xs => isForestCard (fun b => Ib (b ::: xs)) (K xs) (cardH xs))
      (Hcard1:
         sem! fun xs => let a := hd xs in
                     let b := hd (tl xs) in
                     let xs' : Vector.t nat ϕ := tl (tl xs) in
                     a < Ib (b ::: xs') /\ b < cardH xs' /\ Φ xs'
            ⊨ fun xs => let a := hd xs in
                     let b := hd (tl xs) in
                     let xs' := tl (tl xs) in
                     isForestCard (fun c => Ib (S (b+c) ::: xs')) a (card1 xs))
      (Hcard2:
         sem! fun xs => let a := hd xs in
                     let xs' := tl xs in
                     a < K xs' /\ Φ xs'
            ⊨ fun xs => let a := hd xs in
                     let xs' := tl xs in
                     isForestCard (fun b => Ib (b ::: xs')) a (card2 xs))
      (* The bounded sum *)
      (Hbsum: ctxBSum (Fix t) cardH Δ Γ')
      (* Subtyping *)
      (HΓ: ctx! (Fix t); Φ ⊢ Γ ≡ Γ')
      (HM: sem! Φ ⊨ fun xs => (Σ_{b < cardH xs} Jb (b ::: xs)) = M xs)
      (Hρ:
         mty! Φ ⊢ ([<K] ⋅ (subst_lty
                             B
                             (fun f xs =>
                                let a := hd xs in
                                let xs' := tl xs in
                                f (0 ::: (card2 xs) ::: xs'))))
            ≡ ρ),
      (Ty! Φ; Γ ⊢(M) Fix t : ρ @ skel_Fix s)
| tyT_App (t1 t2 : tm) (Δ1 Δ2 : ctx ϕ) (σ τ : mty (S ϕ)) (K1 K2 : idx ϕ)
         (Γ' : ctx ϕ)  (* The context sum before subtyping *)
         (ρ : mty ϕ) (* The type after subtyping *)
         s1 s2 :
    forall (Hty1: Ty! Φ; Δ1 ⊢(K1) t1 : [<iConst 1] ⋅ (σ ⊸ τ) @ s1)
      (Hty2: Ty! Φ; Δ2 ⊢(K2) t2 : (subst_mty_beta_ground σ (iConst 0)) @ s2)
      (Hmsum: ctxMSum (App t1 t2) Δ1 Δ2 Γ')
      (* Subtyping *)
      (HΓ: ctx! (App t1 t2); Φ ⊢ Γ ≡ Γ')
      (HM: sem! Φ ⊨ fun xs => (K1 xs + K2 xs) = M xs)
      (Hρ: mty! Φ ⊢ subst_mty_beta_ground τ (iConst 0) ≡ ρ),
      (Ty! Φ; Γ ⊢(M) t1 t2 : ρ @ skel_App (mty_strip σ) s1 s2)
| tyT_Ifz (Δ1 Δ2 : ctx ϕ) (t1 t2 t3 : tm) (M1 M2 : idx ϕ) (J : idx ϕ)
         (Γ' : ctx ϕ)  (* The context sum before subtyping *)
         (ρ : mty ϕ) (* The type. (Subtyping in the other typings) *)
         s1 s2 s3 :
    forall (Hty1: Ty! Φ; Δ1 ⊢(M1) t1 : Nat J @ s1)
      (Hty2: Ty! (fun xs => J xs = 0 /\ Φ xs); Δ2 ⊢(M2) t2 : ρ @ s2)
      (Hty3: Ty! (fun xs => 1 <= J xs /\ Φ xs); Δ2 ⊢(M2) t3 : ρ @ s3)
      (Hmsum: ctxMSum (Ifz t1 t2 t3) Δ1 Δ2 Γ')
      (* Subtyping *)
      (HΓ: ctx! (Ifz t1 t2 t3); Φ ⊢ Γ ≡ Γ')
      (HM: sem! Φ ⊨ fun xs => M1 xs + M2 xs = M xs),
      (Ty! Φ; Γ ⊢(M) (Ifz t1 t2 t3) : ρ @ skel_Ifz s1 s2 s3)
| tyT_Const n ρ :
    (* Subtyping *)
    forall (Hty: mty! Φ ⊢ Nat (iConst n) ≡ ρ)
      (HM: sem! Φ ⊨ fun xs => 0 = M xs),
      (Ty! Φ; Γ ⊢(M) Const n : ρ @ skel_Const)
| tyT_Succ t K ρ s :
    forall (Hty: Ty! Φ; Γ ⊢(M) t : Nat K @ s)
      (* Subtyping *)
      (Hρ: mty! Φ ⊢ Nat (K >> S) ≡ ρ),
      (Ty! Φ; Γ ⊢(M) Succ t : ρ @ skel_S s)
| tyT_Pred t K ρ s :
    forall (Hty: Ty! Φ; Γ ⊢(M) t : Nat K @ s)
      (* Subtyping *)
      (Hρ: mty! Φ ⊢ Nat (K >> pred) ≡ ρ),
      (Ty! Φ; Γ ⊢(M) Pred t : ρ @ skel_P s)
where "Ty! Φ ; Γ '⊢(' i ')' t : τ @ s" := (@hastyT _ Φ Γ i t τ s).

Lemma tyT_skel_congr {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (K : idx ϕ) (t : tm) (ρ : mty ϕ) (s1 s2 : skel) :
  (Ty! Φ; Γ ⊢(K) t : ρ @ s1) ->
  s1 = s2 ->
  (Ty! Φ; Γ ⊢(K) t : ρ @ s2).
Proof. congruence. Qed.


(** Conversion from a dlPCF typing to a PCF typing with the same skeleton. *)
Lemma hastyT_PCF {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (t : tm) (ρ : mty ϕ) (s : skel) :
  (Ty! Φ; Γ ⊢(M) t : ρ @ s) ->
  { ty : PCF.hastyT (stripCtx Γ) t (mty_strip ρ) | PCF.strip ty = s }.
Proof.
  intros ty. induction ty.
  - (* Var *)
    unshelve eexists.
    + eapply PCF.tyT_Var. unfold stripCtx. now erewrite eqmty_strip by eauto.
    + cbn. reflexivity.
  - (* Lam *)
    apply eqmty_strip in Hρ. destruct ρ; cbn in *; try discriminate. destruct A; cbn in *; injection Hρ as Hρ Hρ'.
    pose proof eqCtx_strip HΓ as L1. pose proof ctxBSum_strip Hbsum as L2.
    destruct IHty as (pctTy & <-).
    unshelve eexists.
    + eapply PCF.retypeT_free.
      * rewrite <- Hρ, <- Hρ'.
        apply PCF.tyT_Lam; eauto.
        clear ty. revert pctTy; clear_all; intros.
        rewrite stripCtx_scons in pctTy. eauto.
      * intros. rewrite L1; auto.
    + cbn. rewrite PCF.retypeT_free_skel. destruct Hρ; cbn. destruct Hρ'; cbn. destruct (stripCtx_scons σ Δ); cbn. reflexivity.

  - (* Fix *) cbn zeta in *. clear HcardH Hcard1 Hcard2 HM. (* all semantical judgements *)
    rewrite stripCtx_scons in IHty.
    apply eqlty_strip in Hsub. apply eqmty_strip in Hρ.
    destruct A, B; cbn in *. injection Hsub as Hsub Hsub'.
    rewrite subst_mty_strip in Hsub, Hsub'.
    destruct ρ; cbn in *; try discriminate. destruct A; cbn in *. injection Hρ as Hρ Hρ'.
    rewrite subst_mty_strip in Hρ, Hρ'.
    pose proof eqCtx_strip HΓ as L1. pose proof ctxBSum_strip Hbsum as L2.

    destruct IHty as (pctTy & <-).
    unshelve eexists.
    + eapply PCF.retypeT_free.
      * apply PCF.tyT_Fix; eauto.
        clear ty. revert Hρ Hρ' Hsub Hsub' pctTy; clear_all; intros.
        rewrite <- Hρ, <- Hρ' in *. rewrite Hsub, Hsub' in *; eauto.
      * intros. rewrite L1; auto.
    + rewrite PCF.retypeT_free_skel. cbn. destruct Hρ, Hρ', Hsub, Hsub'; cbn. reflexivity.

  - (* App *) cbn in *.
    destruct IHty1 as (pcfTy1 & <-); destruct IHty2 as (pcfTy2 & <-).
    apply eqmty_strip in Hρ. setoid_rewrite subst_mty_strip in Hρ.
    pose proof eqCtx_strip HΓ as L1. pose proof ctxMSum_strip Hmsum as L2.

    unshelve eexists.
    + eapply PCF.tyT_App with (τ1 := mty_strip σ).
      * eapply PCF.retypeT_free.
        -- rewrite <- Hρ. eauto.
        -- intros x Hx. rewrite L1; cbn; eauto. now rewrite (proj1 (L2 x ltac:(cbn;eauto))), (proj2 (L2 x ltac:(cbn;eauto))).
      * eapply PCF.retypeT_free.
        -- erewrite <- subst_mty_strip; eauto.
        -- intros x Hx. rewrite L1; cbn; eauto. now rewrite (proj2 (L2 x ltac:(cbn;eauto))).
    + cbn. rewrite !PCF.retypeT_free_skel. f_equal.
      * now destruct Hρ; cbn.
      * destruct Hρ; cbn. destruct (subst_mty_strip σ (subst_beta_ground_fun (iConst 0))); cbn. reflexivity.

  - (* Ifz *) cbn in *.
    pose proof ctxMSum_strip Hmsum as L1. pose proof eqCtx_strip HΓ as L2.
    destruct IHty1 as (pcfTy1 & <-); destruct IHty2 as (pcfTy2 & <-); destruct IHty3 as (pcfTy3 & <-).

    unshelve eexists.
    + eapply PCF.tyT_Ifz.
      * eapply PCF.retypeT_free; eauto.
        intros x Hx. rewrite (proj1 (L1 x ltac:(cbn;eauto))), (proj2 (L1 x ltac:(cbn;eauto))), L2; cbn; auto.
      * eapply PCF.retypeT_free; eauto.
        intros x Hx. rewrite (proj2 (L1 x ltac:(cbn;eauto))), L2; cbn; auto.
      * eapply PCF.retypeT_free; eauto.
        intros x Hx. rewrite (proj2 (L1 x ltac:(cbn;eauto))), L2; cbn; auto.
    + cbn. rewrite !PCF.retypeT_free_skel. reflexivity.

  - (* Const *) apply eqmty_strip in Hty. cbn in Hty. rewrite <- Hty.
    unshelve eexists.
    + apply PCF.tyT_Const.
    + reflexivity.

  - (* Succ *) apply eqmty_strip in Hρ. cbn in Hρ. destruct ρ; cbn in *; try discriminate. 2: destruct A; discriminate.
    destruct IHty as (pcfTy & <-).
    unshelve eexists.
    + apply PCF.tyT_S; eauto.
    + reflexivity.
  - (* Pred *) apply eqmty_strip in Hρ. cbn in Hρ. destruct ρ; cbn in *; try discriminate. 2: destruct A; discriminate.
    destruct IHty as (pcfTy & <-).
    unshelve eexists.
    + apply PCF.tyT_P; eauto.
    + reflexivity.
Qed.


(** *** Translation from the [Prop] version to [Type] and vice versa *)


Lemma eqmty_submty {ϕ} (Φ : constr ϕ) (τ1 τ2 : mty ϕ) :
  (mty! Φ ⊢ τ1 ≡ τ2) -> (mty! Φ ⊢ τ1 ⊑ τ2).
Proof. firstorder. Qed.

Lemma eqlty_sublty {ϕ} (Φ : constr ϕ) (τ1 τ2 : lty ϕ) :
  (lty! Φ ⊢ τ1 ≡ τ2) -> (lty! Φ ⊢ τ1 ⊑ τ2).
Proof. firstorder. Qed.

Lemma eqCtx_subCtx {ϕ} (Φ : constr ϕ) (t : tm) (τ1 τ2 : ctx ϕ) :
  (ctx! t; Φ ⊢ τ1 ≡ τ2) -> (ctx! t; Φ ⊢ τ1 ⊑ τ2).
Proof. firstorder. Qed.

Lemma entails_eq_le {ϕ} (Φ : constr ϕ) (I1 I2 : idx ϕ) :
  (sem! Φ ⊨ fun xs => I1 xs = I2 xs) ->
  (sem! Φ ⊨ fun xs => I1 xs <= I2 xs).
Proof. intros H; hnf in H|-*. firstorder. Qed.

Hint Resolve eqlty_sublty eqmty_submty eqCtx_subCtx entails_eq_le : mty_subty.


Lemma hastyT_hasty {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (t : tm) (ρ : mty ϕ) (s : skel) :
  @hastyT ϕ Φ Γ M t ρ s -> hasty Φ Γ M t ρ.
Proof. induction 1; econstructor; eauto with mty_subty. Qed.


(* (* Doesn't hold any more with precise subtyping *)
Lemma hasty_hastyT {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (t : tm) (ρ : mty ϕ) :
  hasty Φ Γ M t ρ -> exists (s : skel) (_ : @hastyT ϕ Φ Γ M t ρ s), True.
Proof.
  induction 1;
    repeat lazymatch goal with
           | [ H: exists (s : skel), _ |- _] => destruct H as (?s & H)
           | [ H: exists (ty : @hastyT _ _ _ _ _ _ _), _ |- _] => destruct H as (?IH & H)
           end;
    repeat match goal with
             [ H : (Ty! _;_ ⊢(_) _ : _ @ ?s) |- _] =>
             is_var x; pose proof hastyT_skelleton_correct1 H as <-; cbn in *
           end;
    solve [unshelve eexists; swap 1 2; [constructor; auto; econstructor; eauto | ]].
Qed.
*)



(** *** Subsumption *)

(** Weakening of [Φ] *)
Lemma tyT_weaken_Φ {ϕ} (Φ Ψ : constr ϕ) Γ I t τ (s : skel) :
  (Ty! Φ; Γ ⊢(I) t : τ @ s) ->
  (sem! Ψ ⊨ Φ) ->
  (Ty! Ψ; Γ ⊢(I) t : τ @ s).
Proof.
  intros H1 H2. induction H1 in Ψ,H2|-*.
  - (* Var *) eapply tyT_Var; eauto using eqmty_mono_Φ. firstorder.
  - (* Lam *) eapply tyT_Lam; eauto.
    + eapply IHhastyT. firstorder.
    + eapply eqCtx_mono_Φ; eauto.
    + firstorder.
    + eapply eqmty_mono_Φ; eauto.
  - (* Fix *) eapply tyT_Fix; eauto.
    + eapply IHhastyT; eauto. firstorder.
    + eapply eqlty_mono_Φ; eauto. firstorder.
    + firstorder.
    + firstorder.
    + firstorder.
    + eapply eqCtx_mono_Φ; eauto.
    + firstorder.
    + eapply eqmty_mono_Φ; eauto.
  - (* App *) eapply tyT_App; eauto.
    + eapply eqCtx_mono_Φ; eauto.
    + firstorder.
    + eapply eqmty_mono_Φ; eauto.
  - (* If *) eapply tyT_Ifz; eauto.
    + apply IHhastyT2. firstorder.
    + apply IHhastyT3. firstorder.
    + eapply eqCtx_mono_Φ; eauto.
    + firstorder.
  - (* Const *) eapply tyT_Const; eauto using eqmty_mono_Φ. firstorder.
  - (* Succ *) eapply tyT_Succ; eauto using eqmty_mono_Φ.
  - (* Pred *) eapply tyT_Pred; eauto using eqmty_mono_Φ.
Qed.


Lemma tyT_sub {ϕ} (Φ : constr ϕ) Γ1 Γ2 t τ1 τ2 I1 I2 (s : skel) :
  (Ty! Φ; Γ1 ⊢(I1) t : τ1 @ s) ->
  (ctx! t; Φ ⊢ Γ2 ≡ Γ1) ->
  (mty! Φ ⊢ τ1 ≡ τ2) ->
  (sem! Φ ⊨ fun xs => I1 xs = I2 xs) ->
  Ty! Φ; Γ2 ⊢(I2) t : τ2 @ s.
Proof.
  intros Hty Hctx Hmty Hsem.
  revert dependent Γ2. revert dependent I2. revert dependent τ2.
  induction Hty; intros.
  - (* Var *)
    replace (mty_strip ρ) with (mty_strip τ2) by (now erewrite <- eqmty_strip by eauto).
    eapply tyT_Var; eauto.
    transitivity (Γ x). firstorder.
    transitivity ρ; assumption.
    { hnf; intros xs Hxs. now rewrite <- Hsem, <- HM. }
  - (* Lam *) eapply tyT_Lam; eauto.
    + etransitivity; eauto.
    + hnf in Hsem,HM|-*; intros xs Hxs; specialize Hsem with (1 := Hxs); specialize HM with (1 := Hxs). etransitivity; eassumption.
    + etransitivity; eauto.
  - (* Fix *) cbn zeta in *. eapply tyT_Fix; eauto.
    + etransitivity; eauto.
    + hnf in Hsem,HM|-*; intros xs Hxs; specialize Hsem with (1 := Hxs); specialize HM with (1 := Hxs). etransitivity; eassumption.
    + etransitivity; eauto.
  - (* App *) eapply tyT_App; eauto.
    + etransitivity; eauto.
    + hnf in Hsem,HM|-*; intros xs Hxs; specialize Hsem with (1 := Hxs); specialize HM with (1 := Hxs). etransitivity; eassumption.
    + etransitivity; eauto.
  - (* Ifz *) eapply tyT_Ifz; eauto.
    + eapply IHHty2; eauto.
      * eapply eqmty_mono_Φ; eauto. firstorder.
      * hnf; reflexivity.
      * reflexivity.
    + eapply IHHty3; eauto.
      * eapply eqmty_mono_Φ; eauto. firstorder.
      * hnf; reflexivity.
      * reflexivity.
    + etransitivity; eauto.
    + hnf in Hsem,HM|-*; intros xs Hxs; specialize Hsem with (1 := Hxs); specialize HM with (1 := Hxs). etransitivity; eassumption.
  - (* Const *) eapply tyT_Const. etransitivity; eauto.
    { hnf; intros xs Hxs. now rewrite <- Hsem, <- HM. }
  - (* Succ *) eapply tyT_Succ.
    + eapply IHHty; eauto. reflexivity.
    + etransitivity; eauto.
  - (* Pred *) eapply tyT_Pred.
    + eapply IHHty; eauto. reflexivity.
    + etransitivity; eauto.
Qed.


Lemma tyT_sub' {ϕ} (Φ : constr ϕ) Γ t τ1 τ2 I1 I2 (s : skel) :
  (Ty! Φ; Γ ⊢(I1) t : τ1 @ s) ->
  (mty! Φ ⊢ τ1 ≡ τ2) ->
  (sem! Φ ⊨ fun xs => I1 xs = I2 xs) ->
  Ty! Φ; Γ ⊢(I2) t : τ2 @ s.
Proof. intros. eapply tyT_sub; eauto. reflexivity. Qed.

(* Weakening/strengthening in/of the context *)
Lemma tyT_weaken {ϕ} (Φ : constr ϕ) Γ1 Γ2 i t τ (s : skel) :
  (Ty! Φ; Γ1 ⊢(i) t : τ @ s) ->
  (ctx! t; Φ ⊢ Γ2 ≡ Γ1) ->
  Ty! Φ; Γ2 ⊢(i) t : τ @ s.
Proof. intros. eapply tyT_sub; eauto. reflexivity. hnf; reflexivity. Qed.


(** *** Typing inversion *)


(** The typing inversion lemmas eliminate to sigma types *)


(* From Coq Require Import Program.Equality. *)


Section TyInv.

  Context {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (ρ : mty ϕ).

  Lemma tyT_Const_inv (k : nat) (s : skel) :
    @hastyT ϕ Φ Γ M (Const k) ρ s ->
    mty! Φ ⊢ Nat (iConst k) ≡ ρ /\ (sem! Φ ⊨ fun xs => 0 = M xs) /\ s = skel_Const.
  Proof. now inversion 1. Qed.

  Lemma tyT_Lam_inv t (s : skel) :
    @hastyT ϕ Φ Γ M (Lam t) ρ s ->
    existsS (I : idx ϕ) (Δa : ctx (S ϕ)) (Δ : ctx ϕ) (Ka : idx (S ϕ)) σa τa
      (Hbsum : ctxBSum (Lam t) I Δa Δ) (s' : skel),
      (mty! Φ ⊢ ([< I] ⋅ σa ⊸ τa) ≡ ρ) **
      (Ty! (fun xs => hd xs < I (tl xs) /\ Φ (tl xs)); (σa .: Δa) ⊢(Ka) t : τa @ s') **
      (ctx! (Lam t); Φ ⊢ Γ ≡ Δ) **
      (sem! Φ ⊨ fun xs => (I xs + Σ_{a < I xs} Ka (a ::: xs)) = M xs) **
      (s = skel_Lam s').
  Proof. inversion 1; subst; repeat_eexists; repeat_split; eauto. Qed.

  (* TODO: More inversions? *)

End TyInv.


(** *** Index substitution lemma *)

Lemma tyT_cast {ϕ1 ϕ2} {Φ : constr ϕ1} (Γ : ctx ϕ1) (I : idx ϕ1) (t : tm) (τ : mty ϕ1) (s : skel) (Heq : ϕ2 = ϕ1) :
  hastyT Φ Γ I t τ s ->
  hastyT (constr_cast Φ Heq) (ctx_cast Γ Heq) (idx_cast I Heq) t (mty_cast τ Heq) s.
Proof. subst. now rewrite constr_cast_id, ctx_cast_id, idx_cast_id, !mty_cast_id. Qed.


(* TODO: Move *)
Lemma lty_strip_cast {ϕ1 ϕ2} (A : lty ϕ1) (Heq : ϕ2 = ϕ1) :
  lty_strip (lty_cast A Heq) = lty_strip A.
Proof. subst. rewrite lty_cast_id. reflexivity. Qed.
Lemma mty_strip_cast {ϕ1 ϕ2} (ρ : mty ϕ1) (Heq : ϕ2 = ϕ1) :
  mty_strip (mty_cast ρ Heq) = mty_strip ρ.
Proof. subst. rewrite mty_cast_id. reflexivity. Qed.


From Coq Require Import Program.Equality.


Lemma subst_var_beta_fun_eq {ϕ} (x : t (S ϕ)) (y : nat) (I : idx (y + (ϕ -' fin_to_nat x))) (K1 K2 : idx (S ϕ)) xs :
  subst_var_beta_fun x y I (fun xs => K1 xs = K2 xs) xs <->
  subst_var_beta_fun x y I K1 xs = subst_var_beta_fun x y I K2 xs.
Proof. firstorder. Qed.

(* This proof is just a copy. *)

(* TODO: Port the ⊑ to ≡ change *)

Lemma tyT_subst_var_beta {ϕ} (x : Fin.t (S ϕ)) (y : nat) (Φ : constr (S ϕ)) (Γ : ctx (S ϕ)) (K : idx (S ϕ)) (t : tm)
      (τ : mty (S ϕ)) (I : idx (y + (ϕ -' fin_to_nat x))) (s : skel) :
  (Ty! Φ; Γ ⊢(K) t : τ @ s) ->
  (Ty! subst_var_beta_fun x y I Φ; subst_ctx_var_beta x y I Γ ⊢(subst_var_beta_fun x y I K) t : subst_mty_var_beta x y I τ @ s).
Proof.
  intros Hty. dependent induction Hty; intros; bound_inv.
  - (* Var *) apply tyT_Var. split; eapply submty_subst_var_beta; eauto; firstorder.
    { hnf; intros xs Hxs. now erewrite HM by eauto. }
  - (* Lam *) eapply tyT_Lam; eauto.
    + eapply tyT_sub. eapply tyT_weaken_Φ.
      * specialize IHHty with (1 := eq_refl).
        do 4 specialize IHHty with (1 := JMeq_refl).
        specialize (IHHty (FS x)). cbn in IHHty. specialize (IHHty I).
        eapply tyT_cast. now apply IHHty.
      * clear_all. hnf; intros xs (Hxs1&Hxs2). unfold constr_cast.
        erewrite subst_var_beta_fun_FS. cbn. erewrite vect_cast_inv.
        split; eauto.
        eapply Nat.lt_le_trans; eauto. apply Nat.eq_le_incl.
        instantiate (1 := subst_var_beta_fun _  _ _ _). reflexivity.
      * rewrite subst_ctx_var_beta_scons. rewrite ctx_cast_scons. eapply eqCtx_scons; reflexivity.
      * reflexivity.
      * hnf; reflexivity.
    + cbn zeta. eapply ctxBSum_subst_var_beta. eassumption.
    + eapply eqCtx_subst_var_beta; firstorder.
    + etransitivity. eapply sem_subst_var_beta; eassumption. clear.
      unfold idx_cast.
      intros xs Hxs.
      apply subst_var_beta_fun_eq in Hxs.
      rewrite <- Hxs. clear Hxs. rewrite subst_var_beta_fun_plus.
      f_equal.
      erewrite subst_var_beta_fun_sum. f_equal. fext; intros a. reflexivity.
    + cbn. eapply eqmty_mono_Φ.
      * etransitivity.
        2: now (split; eapply submty_subst_var_beta; eauto; firstorder).
        change (mty_cast (subst_mty_var_beta (FS x) y I σ) ?Heq ⊸ mty_cast (subst_mty_var_beta (FS x) y I τ0) ?Heq) with
               (lty_cast (subst_lty_var_beta (FS x) y I (σ ⊸ τ0)) Heq).
        rewrite <- subst_mty_var_beta_eq_Quant.
        eapply eqmty_subst_var_beta. eassumption.
      * refine (fun x Hxs => Hxs).
  - (* Fix *)
    assert (y + ϕ = fin_to_nat x + (y + (ϕ -' fin_to_nat x))) as aux.
    { rewrite sub'_correct. pose proof fin_to_nat_lt x. lia. }

    eapply tyT_Fix; eauto.
    + eapply tyT_sub. eapply tyT_weaken_Φ.
      specialize IHHty with (1 := eq_refl).
      do 4 specialize IHHty with (1 := JMeq_refl).
      specialize (IHHty (FS x)). cbn in IHHty. specialize (IHHty I).
      eapply tyT_cast; eauto.
      * clear_all. hnf; intros xs (Hxs1&Hxs2). unfold constr_cast.
        unshelve erewrite subst_var_beta_fun_FS. apply aux1. cbn zeta. rewrite vect_cast_inv.
        split; eauto.
        eapply Nat.lt_le_trans; eauto. apply Nat.eq_le_incl.
        instantiate (1 := subst_var_beta_fun _  _ _ _). reflexivity.
      * rewrite subst_ctx_var_beta_scons. rewrite ctx_cast_scons. cbn zeta.
        eapply eqCtx_scons. 2: reflexivity.
        unshelve erewrite subst_mty_var_beta_eq_Quant. abstract lia.
        cbn [mty_cast]. eapply eqmty_Quant. 2: hnf; reflexivity.
        instantiate (1 := aux2 _ _). unshelve erewrite lty_cast_twice. abstract lia. reflexivity.
      * cbn [mty_cast]. eapply eqmty_Quant. reflexivity. firstorder.
      * hnf; reflexivity.
    + cbn zeta.
      (* ?B: lty (S (S (y + ϕ))) *)
      etransitivity.
      2:{ eapply eqlty_mono_Φ.
          - eapply eqlty_cast. eapply eqlty_subst_var_beta. eassumption.
          - clear_all. hnf; intros xs (Hxs1&Hxs2&Hxs3). unfold constr_cast.
            erewrite !subst_var_beta_fun_FS; cbn. erewrite vect_cast_twice, vect_cast_hd, vect_cast_tl.
            unfold subst_var_beta_fun. repeat_split; eauto.
            + eapply Nat.lt_le_trans, Nat.eq_le_incl; eauto. cbn.
              unfold idx_cast. erewrite subst_var_beta_fun_FS. cbn -[vect_cast]. erewrite vect_cast_twice, vect_cast_hd, vect_cast_tl.
              cbn. unfold subst_var_beta_fun. f_equal. f_equal.
              all: try now simp_vect_to_list.
              * erewrite !vect_to_list_hd. now vect_to_list.
              * simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
            + eapply Nat.lt_le_trans. erewrite !vect_cast_twice, vect_cast_hd. eassumption. apply Nat.eq_le_incl.
              unfold subst_var_beta_fun. f_equal. simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
            + unfold subst_var_beta_fun in Hxs3. eapply p_equal; eauto. clear_all.
              simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
      }
      {
        clear_all.
        setoid_rewrite subst_lty_cast; setoid_rewrite subst_lty_cast'.
        eapply eqlty_congr.
        setoid_rewrite subst_lty_twice; unfold funcomp; cbn. unfold idx_cast.
        f_equal. fext; intros f xs.
        erewrite !subst_var_beta_fun_FS; cbn -[vect_cast]. erewrite vect_cast_twice, vect_cast_hd, vect_cast_tl.
        unfold subst_var_beta_fun. f_equal. f_equal. erewrite vect_cast_twice, vect_cast_hd, vect_cast_tl. cbn. f_equal.
        - f_equal. f_equal.
          + reflexivity.
          + now erewrite vect_cast_tl, vect_cast_twice, vect_cast_hd.
        - simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
      }
    + revert HcardH. clear_all. intros H. hnf in H|-*. intros xs Hxs.
      unfold subst_var_beta_fun in *; cbn in *. unfold idx_cast. cbn -[vect_cast].
      specialize (H ((vect_cast
          (vect_app (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
             (I (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
              ::: vect_skip y (ϕ -' fin_to_nat x)
                    (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))))
          (subst_var_beta_fun_subproof0 x)))).
      spec_assert H by firstorder. clear Hxs.
      eapply p_equal; eauto.
      replace (fun b : nat =>
         Ib
           (b
            ::: vect_cast
                  (vect_app (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
                     (I (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
                      ::: vect_skip y (ϕ -' fin_to_nat x)
                            (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))))
                  (subst_var_beta_fun_subproof0 x)))
        with (fun b : nat =>
     Ib
       (hd (vect_cast (vect_cast (b ::: xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))
        ::: vect_cast
              (vect_app
                 (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                    (tl (vect_cast (vect_cast (b ::: xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))
                 (I
                    (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                       (tl (vect_cast (vect_cast (b ::: xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))
                  ::: vect_skip y (ϕ -' fin_to_nat x)
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                           (tl (vect_cast (vect_cast (b ::: xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))))
              (f_equal Init.Nat.pred (subst_var_beta_fun_subproof0 (FS x))))) in H; eauto.
      { instantiate (1 := fun xs => (K0
           (vect_cast
              (vect_app (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
                 (I (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
                  ::: vect_skip y (ϕ -' fin_to_nat x)
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))))
              (subst_var_beta_fun_subproof0 x)))); eauto. }
      { clear_all. fext; intros b. f_equal. erewrite vect_cast_twice, vect_cast_hd. cbn. f_equal.
        simp_vect_to_list. do 3 f_equal. now simp_vect_to_list. }

    + cbn zeta. revert Hcard1. clear_all. intros H. hnf in H|-*. intros xs (Hxs1&Hxs2&Hxs3).
      instantiate (1 := aux1 _ _). unshelve instantiate (1 := _). abstract lia.
      unfold subst_var_beta_fun in *; cbn in *. unfold idx_cast. cbn -[vect_cast].
      erewrite !vect_cast_hd, !vect_cast_tl. erewrite !vect_cast_twice, vect_cast_hd.
      unshelve instantiate (1 := _). abstract lia.
      specialize (H (hd xs
        ::: hd (tl xs)
            ::: vect_cast
                  (vect_app
                     (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                        (vect_cast (tl (vect_cast (tl xs) (tyT_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                           (subst_var_beta_fun_subproof x y)))
                     (I
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                           (vect_cast (tl (vect_cast (tl xs) (tyT_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                              (subst_var_beta_fun_subproof x y)))
                      ::: vect_skip y (ϕ -' fin_to_nat x)
                            (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                               (vect_cast (tl (vect_cast (tl xs) (tyT_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                          (subst_var_beta_fun_subproof x y))))) (subst_var_beta_fun_subproof0 x))).
      spec_assert H.
      { clear H. cbn. repeat_split; eauto.
        - eapply Nat.lt_le_trans; eauto. clear Hxs1 Hxs2 Hxs3. eapply Nat.eq_le_incl. unfold idx_cast. f_equal. f_equal.
          + now erewrite vect_cast_twice, vect_cast_hd.
          + simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
        - eapply Nat.lt_le_trans. eauto. clear Hxs1 Hxs2 Hxs3. eapply Nat.eq_le_incl. unfold idx_cast. f_equal.
          simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
        - clear Hxs1 Hxs2. eapply p_equal; eauto. clear. simp_vect_to_list. do 3 f_equal. now simp_vect_to_list. }
      clear Hxs1 Hxs2 Hxs3.
      replace (fun c : nat =>
         Ib
           (S
              (hd
                 (tl
                    (hd xs
                     ::: hd (tl xs)
                         ::: vect_cast
                               (vect_app
                                  (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                                     (vect_cast (tl (vect_cast (tl xs) (tyT_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                        (subst_var_beta_fun_subproof x y)))
                                  (I
                                     (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                                        (vect_cast (tl (vect_cast (tl xs) (tyT_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                           (subst_var_beta_fun_subproof x y)))
                                   ::: vect_skip y (ϕ -' fin_to_nat x)
                                         (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                                            (vect_cast (tl (vect_cast (tl xs) (tyT_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                               (subst_var_beta_fun_subproof x y))))) (subst_var_beta_fun_subproof0 x))) + c)
            ::: tl
                  (tl
                     (hd xs
                      ::: hd (tl xs)
                          ::: vect_cast
                                (vect_app
                                   (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                                      (vect_cast (tl (vect_cast (tl xs) (tyT_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                         (subst_var_beta_fun_subproof x y)))
                                   (I
                                      (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                                         (vect_cast (tl (vect_cast (tl xs) (tyT_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                            (subst_var_beta_fun_subproof x y)))
                                    ::: vect_skip y (ϕ -' fin_to_nat x)
                                          (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                                             (vect_cast (tl (vect_cast (tl xs) (tyT_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                                        (subst_var_beta_fun_subproof x y))))) (subst_var_beta_fun_subproof0 x)))))
        with ((fun c : nat =>
     Ib
       (hd (vect_cast (vect_cast (S (hd (tl xs) + c) ::: tl (tl xs)) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))
        ::: vect_cast
              (vect_app
                 (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                    (tl (vect_cast (vect_cast (S (hd (tl xs) + c) ::: tl (tl xs)) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))
                 (I
                    (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                       (tl (vect_cast (vect_cast (S (hd (tl xs) + c) ::: tl (tl xs)) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))
                  ::: vect_skip y (ϕ -' fin_to_nat x)
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                           (tl
                              (vect_cast (vect_cast (S (hd (tl xs) + c) ::: tl (tl xs)) (aux2 ϕ y))
                                 (subst_var_beta_fun_subproof (FS x) y))))))
              (f_equal Init.Nat.pred (subst_var_beta_fun_subproof0 (FS x)))))) in H; eauto.
      { clear H. fext; intros z. f_equal. f_equal.
        - now erewrite vect_cast_twice, vect_cast_hd.
        - simp_vect_to_list. cbn -[vect_cast]. do 3 f_equal. now simp_vect_to_list. }
    + cbn zeta. revert Hcard2 aux. clear_all. intros H aux. hnf in H|-*. intros xs (Hxs1&Hxs2).
      unfold subst_var_beta_fun in *; cbn in *. unfold idx_cast.
      (* assert (aux: y + ϕ = y + S ϕ) by lia. *)
      specialize (H (hd xs ::: vect_cast
              (vect_app
                 (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                    ((vect_cast (tl xs) aux)))
                 (I
                    (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                       (vect_cast (tl xs) aux))
                  ::: vect_skip y (ϕ -' fin_to_nat x)
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                           (vect_cast (tl xs) aux))))
              (f_equal Init.Nat.pred (subst_var_beta_fun_subproof0 (FS x))))).
      spec_assert H.
      { clear H. cbn. split.
        - eapply Nat.lt_le_trans; eauto. eapply Nat.eq_le_incl. f_equal. simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
        - eapply p_equal; eauto. simp_vect_to_list. do 3 f_equal. now simp_vect_to_list. }
      clear Hxs1 Hxs2. cbn -[vect_to_list vect_cast] in *.
      assert ((fun b : nat =>
     Ib
       (hd (vect_cast (vect_cast (b ::: tl xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))
        ::: vect_cast
              (vect_app
                 (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                    (tl (vect_cast (vect_cast (b ::: tl xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))
                 (I
                    (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                       (tl (vect_cast (vect_cast (b ::: tl xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))
                  ::: vect_skip y (ϕ -' fin_to_nat x)
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                           (tl (vect_cast (vect_cast (b ::: tl xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))))
              (f_equal Init.Nat.pred (subst_var_beta_fun_subproof0 (FS x)))))
        = (fun b : nat =>
    Ib
      (b
       ::: vect_cast
             (vect_app (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast (tl xs) aux))
                (I (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast (tl xs) aux))
                 ::: vect_skip y (ϕ -' fin_to_nat x) (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast (tl xs) aux))))
             (f_equal Init.Nat.pred (subst_var_beta_fun_subproof0 (FS x)))))).
      { clear H. fext; intros b. f_equal. simp_vect_to_list. f_equal.
        - now erewrite vect_cast_twice, vect_cast_hd.
        - do 3 f_equal. now simp_vect_to_list. }
      { setoid_rewrite H0. clear H0.
          now instantiate (1 := fun xs => (card2 (hd xs
            ::: vect_cast
                  (vect_app (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast (tl xs) aux))
                     (I (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast (tl xs) aux))
                      ::: vect_skip y (ϕ -' fin_to_nat x) (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast (tl xs) aux))))
                  (f_equal Init.Nat.pred (subst_var_beta_fun_subproof0 (FS x)))))). }
    + eapply ctxBSum_subst_var_beta. eassumption.
    + now apply eqCtx_subst_var_beta.
    + revert HM. clear. etransitivity. eapply sem_subst_var_beta; eassumption. clear.
      unfold idx_cast.
      intros xs Hxs.
      apply subst_var_beta_fun_eq in Hxs.
      rewrite <- Hxs. clear Hxs.
      erewrite subst_var_beta_fun_sum. f_equal. fext; intros a. reflexivity.
    + revert Hρ. clear. intros H.
      cbn. eapply eqmty_mono_Φ.
      * etransitivity.
        2: eapply eqmty_subst_var_beta; eassumption.
        clear H.
        unshelve erewrite subst_mty_var_beta_eq_Quant. apply aux2.
        eapply eqmty_Quant.
        -- eapply eqlty_congr. setoid_rewrite subst_lty_cast'.
           setoid_rewrite subst_lty_twice; unfold funcomp, idx_cast; cbn -[vect_cast]. f_equal. fext; intros j xs; cbn -[vect_cast].
           erewrite subst_var_beta_fun_FS; cbn -[vect_cast]. unfold subst_var_beta_fun; cbn -[vect_cast].
           f_equal. f_equal.
           erewrite !vect_cast_tl. erewrite vect_cast_twice, vect_cast_hd. cbn.
           f_equal.
           ++ f_equal. f_equal.
              ** now erewrite vect_cast_twice, vect_cast_hd.
              ** simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
           ++ simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
        -- hnf; reflexivity.
      * refine (fun xs Hxs => Hxs).

  - (* App *)
    eapply tyT_skel_congr. eapply tyT_App.
    + eapply tyT_sub. eapply tyT_weaken_Φ.
      * eapply IHHty1; eauto.
      * refine (fun xs Hxs => Hxs).
      * reflexivity.
      * unshelve erewrite subst_mty_var_beta_eq_Quant. clear_all. abstract lia. cbn. reflexivity.
      * hnf; reflexivity.
    + eapply tyT_sub. eapply tyT_weaken_Φ.
      * eapply IHHty2; eauto.
      * refine (fun xs Hxs => Hxs).
      * reflexivity.
      * change (subst_mty σ (subst_var_beta_fun (FS x) y I)) with (subst_mty_var_beta (FS x) y I σ).
        replace (subst_mty_beta_ground (mty_cast (subst_mty_var_beta (FS x) y I σ) (tyT_subst_var_beta_subproof3 y ϕ)) (iConst 0))
          with (subst_mty_var_beta x y I (subst_mty_beta_ground σ (iConst 0))).
        2:{ clear_all. setoid_rewrite subst_mty_cast'. setoid_rewrite subst_mty_twice. f_equal.
            unfold funcomp, subst_beta_ground_fun, idx_cast. cbn -[vect_cast].
            fext; intros j xs. erewrite subst_var_beta_fun_FS; cbn zeta. rewrite vect_cast_inv. cbn. reflexivity. }
        eapply eqmty_subst_var_beta. reflexivity.
      * hnf; reflexivity.
    + eapply ctxMSum_subst_var_beta. eassumption.
    + eapply eqCtx_subst_var_beta. assumption.
    + firstorder.
    + change (subst_mty τ0 (subst_var_beta_fun (FS x) y I)) with (subst_mty_var_beta (FS x) y I τ0).
      replace (subst_mty_beta_ground (mty_cast (subst_mty_var_beta (FS x) y I τ0) (tyT_subst_var_beta_subproof3 y ϕ)) (iConst 0))
        with (subst_mty_var_beta x y I (subst_mty_beta_ground τ0 (iConst 0))).
      2:{ clear_all. setoid_rewrite subst_mty_cast'. setoid_rewrite subst_mty_twice. f_equal.
          unfold funcomp, subst_beta_ground_fun, idx_cast. cbn -[vect_cast].
          fext; intros j xs. erewrite subst_var_beta_fun_FS; cbn zeta. rewrite vect_cast_inv. cbn. reflexivity. }
      now eapply eqmty_subst_var_beta.
    + now rewrite mty_strip_cast, subst_mty_strip.
  - (* Ifz *) eapply tyT_Ifz; eauto.
    + eapply tyT_sub'.
      * eapply IHHty1; eauto.
      * cbn. reflexivity.
      * hnf; reflexivity.
    + eapply tyT_sub'. eapply tyT_weaken_Φ.
      * eapply IHHty2; eauto. reflexivity.
      * instantiate (1 := I). firstorder.
      * reflexivity.
      * hnf; reflexivity.
    + eapply tyT_sub'. eapply tyT_weaken_Φ.
      * eapply IHHty3; eauto. reflexivity.
      * firstorder.
      * reflexivity.
      * hnf; reflexivity.
    + apply ctxMSum_subst_var_beta; eauto.
    + apply eqCtx_subst_var_beta; auto.
    + firstorder.
  - (* Const *) eapply tyT_Const.
    change (Nat (iConst n)) with (subst_mty_var_beta x y I (Nat (iConst n))). now apply eqmty_subst_var_beta.
    { hnf; intros xs Hxs. now erewrite HM by eauto. }
  - (* Succ *) eapply tyT_Succ.
    + instantiate (1 := (subst_var_beta_fun x y I K0)).
      change (Nat ?K1) with (subst_mty_var_beta x y I (Nat K0)).
      eapply IHHty; eauto.
    + change (Nat _) with (subst_mty_var_beta x y I (Nat (K0>>S))).
      now apply eqmty_subst_var_beta.
  - (* Pred *) eapply tyT_Pred.
    + instantiate (1 := (subst_var_beta_fun x y I K0)).
      change (Nat ?K1) with (subst_mty_var_beta x y I (Nat K0)).
      eapply IHHty; eauto.
    + change (Nat _) with (subst_mty_var_beta x y I (Nat (K0>>pred))).
      now apply eqmty_subst_var_beta.
      Unshelve. all: try now auto.
      Unshelve. all: cbn; try abstract lia.
      all: abstract (rewrite !sub'_correct; pose proof fin_to_nat_lt x; lia).
Qed.


Lemma tyT_shift_var_add {ϕ} (Φ : constr (S ϕ)) (x : Fin.t (S ϕ)) (Γ : ctx (S ϕ)) (K : idx (S ϕ)) (t : tm) (τ : mty (S ϕ))
      (I : idx (ϕ -' fin_to_nat x)) (s : skel) :
  (Ty! Φ; Γ ⊢(K) t : τ @ s) ->
  (Ty! shift_var_add_fun x I Φ; ctx_shift_var_add x Γ I
          ⊢(shift_var_add_fun x I K) t : mty_shift_var_add x τ I @ s).
Proof.
  unfold mty_shift_var_add, shift_var_add_fun.
  intros H.
  eapply tyT_sub; [eapply tyT_weaken_Φ|..].
  1: apply tyT_subst_var_beta with (y := 1) (x0 := x); eassumption.
  all: try now (reflexivity + hnf;reflexivity).
Qed.

Lemma tyT_shift_add {ϕ} (Φ : constr (S ϕ)) (Γ : ctx (S ϕ)) (K : idx (S ϕ)) (t : tm) (τ : mty (S ϕ)) (s : skel) (I : idx ϕ) :
  (Ty! Φ; Γ ⊢(K) t : τ @ s) ->
  (Ty! shift_add_fun I Φ; ctx_shift_add Γ I
          ⊢(shift_add_fun I K) t : mty_shift_add τ I @ s).
Proof.
  unfold mty_shift_add. intros H.
  rewrite !shift_add_fun_correct.
  replace (ctx_shift_add Γ I) with (ctx_shift_var_add Fin0 Γ I).
  2:{ unfold ctx_shift_var_add, ctx_shift_add. fext; intros var. now erewrite mty_shift_add_correct. }
  replace (ctx_shift_var_add Fin0 Γ I) with (subst_ctx_var_beta Fin0 1 (fun xs : Vector.t nat (S ϕ) => I (tl xs) + hd xs) Γ).
  2:{ unfold ctx_shift_var_add, subst_ctx_var_beta, subst_mty_var_beta; cbn. reflexivity. }
  now apply tyT_subst_var_beta with (x := Fin0) (y := 1).
Qed.
Lemma tyT_shift_sub {ϕ} (Φ : constr (S ϕ)) (Γ : ctx (S ϕ)) (K : idx (S ϕ)) (t : tm) (τ : mty (S ϕ)) (s : skel) (I : idx ϕ) :
  (Ty! Φ; Γ ⊢(K) t : τ @ s) ->
  (Ty! shift_sub_fun I Φ; ctx_shift_sub Γ I
          ⊢(shift_sub_fun I K) t : mty_shift_sub τ I @ s).
Proof.
  unfold mty_shift_sub. intros H.
  rewrite !shift_sub_fun_correct.
  replace (ctx_shift_sub Γ I) with (ctx_shift_var_sub Fin0 Γ I).
  2:{ unfold ctx_shift_var_sub, ctx_shift_sub. fext; intros var. now erewrite mty_shift_sub_correct. }
  replace (ctx_shift_var_sub Fin0 Γ I) with (subst_ctx_var_beta Fin0 1 (fun xs : Vector.t nat (S ϕ) => hd xs - I (tl xs)) Γ).
  2:{ unfold ctx_shift_var_sub, subst_ctx_var_beta, subst_mty_var_beta; cbn. reflexivity. }
  now apply tyT_subst_var_beta with (x := Fin0) (y := 1).
Qed.

Lemma tyT_subst_beta_ground {ϕ} (Φ : constr (S ϕ)) (Γ : ctx (S ϕ)) (K : idx (S ϕ)) (t : tm) (τ : mty (S ϕ)) (s : skel) (I : idx ϕ) :
  (Ty! Φ; Γ ⊢(K) t : τ @ s) ->
  (Ty! subst_beta_ground_fun I Φ; subst_ctx_beta_ground I Γ
       ⊢(subst_beta_ground_fun I K) t : subst_mty_beta_ground τ I @ s).
Proof.
  intros H.
  rewrite !subst_beta_ground_fun_correct.
  rewrite subst_mty_beta_ground_correct, !subst_beta_fun_correct.
  replace (subst_ctx_beta_ground I Γ) with (subst_ctx_var_beta Fin0 0 I Γ).
  2:{ unfold subst_ctx_var_beta, subst_ctx_beta_ground. fext. intros. now rewrite subst_mty_beta_ground_correct. }
  now apply tyT_subst_var_beta with (x := Fin0) (y := 0).
Qed.


(** *** Retyping closed terms for arbitrary contexts *)

Lemma retypeT_free {ϕ} (Φ : constr ϕ) Γ Σ M t τ (s : skel) :
  (Ty! Φ; Γ ⊢(M) t : τ @ s) ->
  (forall var, free var t -> Σ var = Γ var) ->
  (Ty! Φ; Σ ⊢(M) t : τ @ s).
Proof.
  intros Hty Hext. induction Hty in Σ,Hext|-*; bound_inv.
  - (* Var *) eapply tyT_Var. rewrite Hext; eauto. cbn; reflexivity.
    { hnf; intros xs Hxs. now erewrite HM by eauto. }
  - (* Lam *)
    eapply tyT_Lam.
    + eapply tyT_weaken. eapply IHHty; eauto.
      hnf; intros [ | var].
      { cbn. reflexivity. }
      cbn [scons]. instantiate (1 := fun var => _). cbn beta zeta. reflexivity.
    + instantiate (1 := fun var => Γ' var). hnf. intros var Hvar. now apply Hbsum.
    + intros i Hi. rewrite Hext; eauto.
    + assumption.
    + assumption.
  - (* Fix *)
    eapply tyT_Fix.
    + eapply tyT_weaken.
      -- eapply IHHty. eauto.
      -- cbn. apply eqCtx_scons; reflexivity.
    + eauto.
    + eauto.
    + eauto.
    + eauto.
    + instantiate (1 := fun var => Γ' var). hnf. intros var Hvar. now apply Hbsum.
    + hnf. intros var **. rewrite Hext; auto.
    + firstorder.
    + firstorder.
  - (* App *) eapply tyT_App.
    + now eapply IHHty1; eauto.
    + now eapply IHHty2; eauto.
    + instantiate (1 := fun var => Γ' var). hnf. intros var Hvar. eauto.
    + hnf. intros var **. rewrite Hext; auto.
    + assumption.
    + assumption.
  - (* Ifz *) eapply tyT_Ifz.
    + now eapply IHHty1; eauto.
    + now eapply IHHty2; eauto.
    + now eapply IHHty3; eauto.
    + instantiate (1 := fun var => Γ' var). hnf. intros var Hvar. eauto.
    + hnf. intros var **. rewrite Hext; auto.
    + assumption.
  - (* Const *) now apply tyT_Const.
  - (* Succ *) eapply tyT_Succ; eauto.
  - (* Pred *) eapply tyT_Pred; eauto.
Qed.


(** The context is irrelevant for closed terms *)
Lemma retypeT_closed {ϕ} (Φ : constr ϕ) Γ Σ M t τ (s : skel) :
  (Ty! Φ; Γ ⊢(M) t : τ @ s) ->
  closed t ->
  (Ty! Φ; Σ ⊢(M) t : τ @ s).
Proof.
  intros. eapply retypeT_free; eauto.
  { intros var Hvar. exfalso. eapply free_closed_iff; eauto. }
Qed.



(** *** Ex-falso *)

Lemma tyT_exfalso {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (t : tm) (τ : mty ϕ) :
  (sem! Φ ⊨ fun _ => False) ->
  forall (pcfTy : PCF.hastyT (stripCtx Γ) t (mty_strip τ)),
  hastyT Φ Γ (iConst 0) t τ (PCF.strip pcfTy).
Proof.
  intros Hsem Hsty.
  remember (stripCtx Γ) as Σ. remember (mty_strip τ) as A. induction Hsty in ϕ,Φ,Hsem,Γ,HeqΣ,τ,HeqA|-*.
  - (* Var *) eapply tyT_Var. apply eqmty_exfalso; eauto. subst. unfold stripCtx in e. congruence.
    { hnf; reflexivity. }
  - (* Lam *) rename τ1 into A, τ2 into B.
    destruct τ as [ ? | k c]; cbn in *; try discriminate. destruct c as [τ1 τ2]. cbn in HeqA. injection HeqA as -> ->.
    eapply tyT_Lam with (I := iConst 0).
    + eapply IHHsty; eauto.
      * firstorder.
      * rewrite stripCtx_scons; f_equal. symmetry. apply decorateCtx_strip.
    + apply ctxBSum_decorate.
    + apply eqCtx_exfalso. auto. subst. now rewrite decorateCtx_strip.
    + hnf; intros xs Hxs; exfalso; firstorder.
    + apply eqmty_exfalso; eauto.

  - (* Fix *) subst. destruct τ as [ ? | k c]; cbn in *; try discriminate.
    1: now inv Hsty. (* The Fix can't have type [Nat] *)
    (* The forest cardinalites are all bogus because the obligations for them are semantical. *)
    eapply tyT_Fix with (cardH := iConst 0) (card1 := iConst 0) (card2 := iConst 0).
    + eapply IHHsty; eauto.
      * firstorder.
      * rewrite stripCtx_scons; cbn. f_equal.
        2: symmetry; apply decorateCtx_strip.
        (* Bogus substitution *)
        instantiate (1 := subst_lty c (fun j xs => j (tl xs))). now rewrite !subst_lty_strip.
      * cbn. instantiate (1 := subst_lty c (fun j xs => j (tl xs))). now rewrite !subst_lty_strip.
    + cbn. eapply eqlty_exfalso; eauto.
      * hnf. unfold iConst. intros. lia.
      * now rewrite !subst_lty_strip.
    + hnf in Hsem|-*. intros. exfalso. eapply Hsem; eauto.
    + cbn. hnf in Hsem|-*. intros xs (Hxs1&Hxs2&Hxs3). unfold iConst in *. exfalso. lia.
    + cbn. hnf in Hsem|-*. intros xs (Hxs1&Hxs2). exfalso. eapply Hsem; eauto.
    + apply ctxBSum_decorate.
    + eapply eqCtx_exfalso; eauto. now rewrite decorateCtx_strip.
    + cbn. firstorder.
    + eapply eqmty_exfalso; eauto. cbn. rewrite subst_lty_strip. now rewrite !subst_lty_strip.

  - (* App *) subst. cbn.
    eapply tyT_skel_congr. eapply tyT_App.
    + apply IHHsty1.
      * firstorder.
      * symmetry. apply decorateCtx_strip.
      * cbn. f_equal; symmetry; apply decorate_strip.
    + apply IHHsty2.
      * firstorder.
      * symmetry. apply decorateCtx_strip.
      * cbn. setoid_rewrite subst_mty_strip. now rewrite decorate_strip.
    + apply ctxMSum_decorate.
    + eapply eqCtx_exfalso; eauto. now rewrite decorateCtx_strip.
    + cbn; hnf; reflexivity.
    + eapply eqmty_exfalso; eauto. setoid_rewrite subst_mty_strip. now rewrite decorate_strip.
    + f_equal. now rewrite decorate_strip.

  - (* Ifz *) subst. eapply tyT_Ifz.
    + apply IHHsty1.
      * firstorder.
      * symmetry. apply decorateCtx_strip.
      * cbn. reflexivity.
    + apply IHHsty2.
      * instantiate (1 := iConst 42). firstorder.
      * symmetry. apply decorateCtx_strip.
      * reflexivity.
    + apply IHHsty3.
      * firstorder.
      * symmetry. apply decorateCtx_strip.
      * reflexivity.
    + apply ctxMSum_decorate.
    + eapply eqCtx_exfalso; eauto. now rewrite decorateCtx_strip.
    + hnf; reflexivity.

  - (* Const *) apply tyT_Const. now apply eqmty_exfalso.
    { hnf; reflexivity. }
  - (* Succ *) eapply tyT_Succ; eauto. now apply eqmty_exfalso.
  - (* Pred *) eapply tyT_Pred; eauto. now apply eqmty_exfalso.

    (* A lot of bogus indexes are left *)
    Unshelve. all: exact (iConst 0).
Qed.



(** *** Trivial type *)

(** There is a more/less trivial type for every value *)


(* TODO: Move to [PCF] *)
Lemma PCF_tyT_congr (Γ : PCF.ctx) (t : tm) (τ1 τ2 : PCF.ty) :
  PCF.hastyT Γ t τ1 ->
  τ1 = τ2 ->
  PCF.hastyT Γ t τ2.
Proof. now intros H []. Defined.
Lemma PCF_tyT_congr_skel (Γ : PCF.ctx) (t : tm) (τ1 τ2 : PCF.ty)
      (ty: PCF.hastyT Γ t τ1)
      (Heq: τ1 = τ2) :
  PCF.strip (PCF_tyT_congr ty Heq) = PCF.strip ty.
Proof. subst. reflexivity. Qed.



Lemma tyT_triv_Lam {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (t : tm) (σ τ : mty (S ϕ)) :
  forall (pcfTy: PCF.hastyT (stripCtx Γ) (Lam t) (lty_strip (σ ⊸ τ))),
    closed (Lam t) ->
    Ty! Φ; Γ ⊢(iConst 0) Lam t : [< iConst 0] ⋅ σ ⊸ τ @ (PCF.strip pcfTy).
Proof.
  intros Hty Hclos; cbn in *.
  pose proof PCF.tyT_Lam_inv' Hty as (? & ? & pcfTy' & Heq & Heq2).
  injection Heq as -> ->. pose proof PCF.ty_UIP_refl Heq as ->.
  cbn in *. subst Hty.
  eapply tyT_skel_congr. eapply tyT_Lam.
  3,5: reflexivity.
  - eapply tyT_exfalso; eauto.
    + unfold iConst. firstorder.
  - hnf; intros var Hvar. exfalso. contradict Hclos. cbn in *. rewrite free_bound_iff. firstorder.
  - unfold iConst; cbn. hnf; lia.
  - unshelve instantiate (1 := _).
    { eapply PCF.retypeT_free; eauto.
      intros [ | x] Hx; cbn.
      - reflexivity.
      - unfold stripCtx.
        instantiate (1 := fun x => subst_mty (Γ x) (fun f xs => f (tl xs))).
        cbn. rewrite subst_mty_strip. reflexivity.
    }
    cbn. f_equal. now rewrite PCF.retypeT_free_skel.
Qed.

Lemma tyT_triv_Lam' {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (t : tm) (A : lty (S ϕ)) :
  forall (pcfTy: PCF.hastyT (stripCtx Γ) (Lam t) (lty_strip A)),
    closed (Lam t) ->
    Ty! Φ; Γ ⊢(iConst 0) Lam t : [< iConst 0] ⋅ A @ (PCF.strip pcfTy).
Proof. intros. destruct A as [σ τ]. eapply tyT_triv_Lam; eauto. Qed.


Lemma tyT_triv_Fix {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (t : tm) (σ τ : mty (S ϕ)) :
  forall (pcfTy: PCF.hastyT (stripCtx Γ) (Fix t) (lty_strip (σ ⊸ τ))),
    closed (Fix t) ->
    Ty! Φ; Γ ⊢(iConst 0) Fix t : [< iConst 0] ⋅ σ ⊸ τ @ (PCF.strip pcfTy).
Proof.
  intros pcfTy Hclos; cbn in *.
  pose proof PCF.tyT_Fix_inv' pcfTy as (pcfTy' & Heq).
  eapply tyT_skel_congr. eapply tyT_Fix with (cardH := iConst 0) (K := iConst 0); cbn zeta.
  - eapply tyT_exfalso; eauto.
    + unfold iConst. firstorder omega.
  - eapply eqlty_exfalso; eauto.
    + unfold iConst. firstorder omega.
  - unfold iConst. hnf; intros. hnf. exists 0. cbn. reflexivity.
  - unfold iConst. firstorder omega.
  - unfold iConst. firstorder omega.
  - hnf; intros var Hvar. exfalso. contradict Hclos. cbn in *. rewrite free_bound_iff. firstorder.
  - reflexivity.
  - cbn. hnf; reflexivity.
  - cbn. eapply eqmty_Quant.
    + instantiate (2 := subst_mty σ (fun f xs => f (tl xs)) ⊸ subst_mty τ (fun f xs => f (tl xs))).
      cbn. setoid_rewrite subst_mty_twice; unfold funcomp; cbn.
      instantiate (1 := hd).
      replace (fun (f : idx (S ϕ)) (xs : Vector.t nat (S ϕ)) => f (hd xs ::: tl xs)) with (fun (f : idx (S ϕ)) xs => f xs).
      2:{ fext; intros. f_equal. now rewrite <- eta. }
      rewrite !subst_mty_id. reflexivity.
    + hnf; reflexivity.
  - cbn.
    unshelve instantiate (1 := _).
    { eapply PCF.retypeT_free.
      - cbn. eapply PCF_tyT_congr; eauto.
        { rewrite !subst_mty_strip; eauto. }
      - intros [ | x] Hx; cbn.
        + rewrite !subst_mty_strip. eauto.
        + unfold stripCtx.
          instantiate (1 := fun x => subst_mty (Γ x) (fun f xs => f (tl xs))).
          cbn. rewrite subst_mty_strip. reflexivity.
    }
    cbn. f_equal. rewrite PCF.retypeT_free_skel, PCF_tyT_congr_skel. now subst pcfTy.
  Unshelve. all: exact (iConst 0).
Qed.

Lemma tyT_triv_Fix' {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (t : tm) (A : lty (S ϕ)) :
  forall (pcfTy: PCF.hastyT (stripCtx Γ) (Fix t) (lty_strip A)),
    closed (Fix t) ->
    Ty! Φ; Γ ⊢(iConst 0) Fix t : [< iConst 0] ⋅ A @ (PCF.strip pcfTy).
Proof. intros. destruct A as [σ τ]. eapply tyT_triv_Fix; eauto. Qed.


(* Only needed for [ty_triv_sig] *)
Definition ty_triv {ϕ} (v : tm) (A : PCF_Types.ty) : mty ϕ :=
  match v, A with
  | Lam t, PCF_Types.Arr A B => [ < iConst 0] ⋅ decorate A _ ⊸ decorate B _
  | Fix t, PCF_Types.Arr A B => [ < iConst 0] ⋅ decorate A _ ⊸ decorate B _
  | Const k, PCF_Types.Nat => Nat (iConst k)
  | _, _ => Nat (iConst 0) (* invalid *)
  end.


(* TODO: Move *)
Import EqNotations.
Lemma PCF_tyT_Const_inv' Γ x τ :
  forall ty : PCF.hastyT Γ (Const x) τ,
    existsS (H : PCF.Nat = τ),
      ty = rew H in PCF.tyT_Const _ _.
Proof.
  intros s.
  refine (match s with
          | @PCF.tyT_Const _ _ => _
          end);
    cbn.
  eexists eq_refl. reflexivity.
Qed.

Lemma PCF_tyT_S_inv' Γ (t : tm) τ :
  forall ty : PCF.hastyT Γ (Succ t) τ,
    existsS (H : PCF.Nat = τ) (ty' : PCF.hastyT Γ t PCF.Nat),
      ty = rew H in PCF.tyT_S ty'.
Proof.
  intros s.
  refine (match s with
          | @PCF.tyT_S _ _ _ => _
          end);
    cbn.
  eexists eq_refl, _. reflexivity.
Qed.

Lemma PCF_tyT_P_inv' Γ (t : tm) τ :
  forall ty : PCF.hastyT Γ (Pred t) τ,
    existsS (H : PCF.Nat = τ) (ty' : PCF.hastyT Γ t PCF.Nat),
      ty = rew H in PCF.tyT_P ty'.
Proof.
  intros s.
  refine (match s with
          | @PCF.tyT_P _ _ _ => _
          end);
    cbn.
  eexists eq_refl, _. reflexivity.
Qed.


Lemma PCF_tyT_Const_inv_is_Nat Γ (k : nat) A :
  PCF.hastyT Γ (Const k) A ->
  A = PCF.Nat.
Proof.
  intros H.
  pose proof PCF_tyT_Const_inv' H.
  firstorder.
Qed.

Lemma PCF_tyT_Lam_inv_is_Arr Γ t A :
  PCF.hastyT Γ (Lam t) A ->
  existsS A1 A2, A = PCF.Arr A1 A2.
Proof.
  intros H.
  pose proof PCF.tyT_Lam_inv H.
  firstorder.
Qed.

Lemma PCF_tyT_Fix_inv_is_Arr Γ t A :
  PCF.hastyT Γ (Fix t) A ->
  existsS A1 A2, A = PCF.Arr A1 A2.
Proof.
  intros H.
  pose proof PCF.tyT_Fix_inv H.
  eapply PCF_tyT_Lam_inv_is_Arr; eauto.
Qed.


Lemma tyT_triv_sig {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (v : tm) (A : PCF_Types.ty) :
  forall (pcfTy: PCF.hastyT (stripCtx Γ) v A),
    val v -> closed v ->
  { ρ & Ty! Φ; Γ ⊢(iConst 0) v : ρ @ (PCF.strip pcfTy) ** mty_strip ρ = A }.
Proof.
  intros pcfTy Hval Hclos.
  exists (ty_triv v A).
  apply valb_iff in Hval. destruct v; cbn in Hval; try congruence.
  - (* Lam *)
    pose proof PCF_tyT_Lam_inv_is_Arr pcfTy as (A1 & A2 & ->).
    unshelve epose proof tyT_triv_Lam' Φ (decorate A1 _ ⊸ decorate A2 _) _ Hclos.
    2:{ eapply PCF_tyT_congr; eauto. cbn. now rewrite !decorate_strip. }
    rewrite PCF_tyT_congr_skel in X.
    split.
    + apply X.
    + cbn. now rewrite !decorate_strip.
  - (* Fix *)
    pose proof PCF_tyT_Fix_inv_is_Arr pcfTy as (A1 & A2 & ->).
    unshelve epose proof tyT_triv_Fix' Φ (decorate A1 _ ⊸ decorate A2 _) _ Hclos.
    2:{ eapply PCF_tyT_congr; eauto. cbn. now rewrite !decorate_strip. }
    rewrite PCF_tyT_congr_skel in X.
    split.
    + apply X.
    + cbn. now rewrite !decorate_strip.
  - (* Const *)
    pose proof PCF_tyT_Const_inv' pcfTy as (H & ->). subst A; cbn in *.
    split.
    + constructor. reflexivity. hnf; reflexivity.
    + reflexivity.
Qed.


(* TODO: Clean up here! *)
