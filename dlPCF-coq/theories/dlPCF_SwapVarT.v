Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax.
Require Import dlPCF_iSubst.
Require Import ForestCard dlPCF_Types dlPCF_Sum dlPCF_Contexts dlPCF_Typing.
Require Import dlPCF_TypingT.
Require Import dlPCF_MakeSum.
Require PCF_Types.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.
Import SigmaTypeNotations.
Open Scope vector_scope.

From Coq Require Import Vector Fin.

Open Scope PCF.

Implicit Types (ϕ : nat).

Require Import dlPCF_Safety.

Import Skel.


(** ** Swapping two variables *)

(** In this file, we implement the parallel substitution [{x := y, y := x}], where [y] is the variable that comes after [x]. *)

(** Note: This is (almost) a literal copy of [dlPCF_CloneVar.v], modified with [s/clone/swap/g]. *)


Definition subst_swap_fun {X : Type} {ϕ} : (Vector.t nat (S (S ϕ)) -> X) -> (Vector.t nat (S (S ϕ)) -> X) :=
  fun f xs => f (hd (tl xs) ::: hd xs ::: tl (tl xs)).

Definition subst_var_swap_fun {X : Type} {ϕ} (x : Fin.t (S ϕ)) : (Vector.t nat (S (S ϕ)) -> X) -> (Vector.t nat (S (S ϕ)) -> X).
Proof.
  refine (fun f xs => f _).
  apply vect_cast with (n := fin_to_nat x + (S (S (ϕ -' fin_to_nat x)))) in xs.
  2:{ abstract (rewrite !sub'_correct; pose proof fin_to_nat_lt x; lia). }
  pose (ys1 := vect_take _ _ xs).
  pose (ys2 := vect_skip _ _ xs).
  pose (a := hd ys2). pose (b := hd (tl ys2)).
  refine (vect_cast (vect_app ys1 (b ::: a ::: tl (tl ys2))) _).
  1:{ abstract (rewrite !sub'_correct; pose proof fin_to_nat_lt x; lia). }
Defined.

Lemma subst_swap_fun_correct {X : Type} {ϕ} :
  subst_swap_fun (X := X) (ϕ := ϕ) = subst_var_swap_fun Fin0.
Proof.
  fext; intros f xs. unfold subst_swap_fun, subst_var_swap_fun. f_equal.
  simp_vect_to_list. cbn. f_equal.
  - f_equal. now simp_vect_to_list.
  - f_equal. f_equal. now simp_vect_to_list.
Qed.


Definition subst_lty_var_swap {ϕ} (x : Fin.t (S ϕ)) (A : lty (S (S ϕ))) : lty (S (S ϕ)) := subst_lty A (subst_var_swap_fun x).
Definition subst_mty_var_swap {ϕ} (x : Fin.t (S ϕ)) (τ : mty (S (S ϕ))) : mty (S (S ϕ)) := subst_mty τ (subst_var_swap_fun x).
Definition subst_lty_swap {ϕ} (A : lty (S (S ϕ))) : lty (S (S ϕ)) := subst_lty A (subst_swap_fun).
Definition subst_mty_swap {ϕ} (τ : mty (S (S ϕ))) : mty (S (S ϕ)) := subst_mty τ (subst_swap_fun).


Lemma firstn_hd (X : Type) (xs : list X) (a : nat) (def : X) :
  xs <> List.nil ->
  firstn (S a) xs = List.hd def xs :: firstn a (List.tl xs).
Proof. destruct xs; cbn; congruence. Qed.



Lemma subst_var_swap_fun_FS {X : Type} {ϕ} (x : Fin.t (S ϕ)) :
  subst_var_swap_fun (X := X) (FS x) =
  (fun (f : Vector.t nat (S (S (S ϕ))) -> X) (xs : Vector.t nat (S (S (S ϕ)))) =>
     subst_var_swap_fun x (fun ys : Vector.t nat (S (S ϕ)) => f (hd xs ::: ys)) (tl xs)).
Proof.
  fext; intros f xs. unfold subst_var_swap_fun. f_equal.
  simp_vect_to_list.
  cbn [fin_to_nat]. rewrite !firstn_hd with (def := def_nat).
  2:{ intros H % length_zero_iff_nil. rewrite vect_to_list_length in H. lia. }
  rewrite <- vect_to_list_hd. cbn -[skipn]. f_equal. f_equal. f_equal.
  - f_equal. now simp_vect_to_list.
  - f_equal.
    + f_equal. now simp_vect_to_list.
    + f_equal. now rewrite skipn_tl, skipn_tl'.
Qed.


Lemma subst_mty_var_swap_eq_Quant {ϕ} (x : Fin.t (S ϕ)) (i : idx (S (S ϕ))) (A : lty (S (S (S ϕ)))) :
  subst_mty_var_swap x ([ < i]⋅ A) =
  [ < subst_var_swap_fun x i] ⋅ subst_lty_var_swap (Fin.FS x) A.
Proof. cbn. f_equal. symmetry. unfold subst_lty_var_swap. f_equal. apply subst_var_swap_fun_FS. Qed.


Lemma subst_var_swap_fun_sem_Rel {ϕ} (x : Fin.t (S ϕ)) (Φ : constr _) (k1 k2 : idx _) (R : nat -> nat -> Prop) :
  (sem! Φ ⊨ (fun xs : Vector.t nat (S (S ϕ)) => R (k1 xs) (k2 xs))) ->
  sem! subst_var_swap_fun x Φ ⊨
   (fun xs : Vector.t nat (S (S ϕ)) => R (subst_var_swap_fun x k1 xs) (subst_var_swap_fun x k2 xs)).
Proof.
  intros H; hnf in H|-*; intros xs Hxs. unfold subst_var_swap_fun in Hxs|-*.
  apply (H _ Hxs).
Qed.
Lemma subst_var_swap_fun_sem_eq {ϕ} (x : Fin.t (S ϕ)) (Φ : constr _) (k1 k2 : idx _) :
  (sem! Φ ⊨ (fun xs : Vector.t nat (S (S ϕ)) => k1 xs = k2 xs)) ->
  sem! subst_var_swap_fun x Φ ⊨
   (fun xs : Vector.t nat (S (S ϕ)) => subst_var_swap_fun x k1 xs = subst_var_swap_fun x k2 xs).
Proof. apply subst_var_swap_fun_sem_Rel. Qed.
Lemma subst_var_swap_fun_sem_le {ϕ} (x : Fin.t (S ϕ)) (Φ : constr _) (k1 k2 : idx _) :
  (sem! Φ ⊨ (fun xs : Vector.t nat (S (S ϕ)) => k1 xs <= k2 xs)) ->
  sem! subst_var_swap_fun x Φ ⊨
   (fun xs : Vector.t nat (S (S ϕ)) => subst_var_swap_fun x k1 xs <= subst_var_swap_fun x k2 xs).
Proof. apply subst_var_swap_fun_sem_Rel. Qed.
Lemma subst_var_swap_fun_sem_lt {ϕ} (x : Fin.t (S ϕ)) (Φ : constr _) (k1 k2 : idx _) :
  (sem! Φ ⊨ (fun xs : Vector.t nat (S (S ϕ)) => k1 xs < k2 xs)) ->
  sem! subst_var_swap_fun x Φ ⊨
   (fun xs : Vector.t nat (S (S ϕ)) => subst_var_swap_fun x k1 xs < subst_var_swap_fun x k2 xs).
Proof. apply subst_var_swap_fun_sem_Rel. Qed.


Fixpoint sublty_subst_var_swap {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S (S ϕ))) (A B : lty (S (S ϕ)))
         (H: lty! Φ ⊢ A ⊑ B) { struct H } :
  (lty! subst_var_swap_fun x Φ ⊢ subst_lty_var_swap x A ⊑ subst_lty_var_swap x B)
with submty_subst_var_swap {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S (S ϕ))) (σ τ : mty (S (S ϕ)))
         (H: mty! Φ ⊢ σ ⊑ τ) { struct H } :
  (mty! subst_var_swap_fun x Φ ⊢ subst_mty_var_swap x σ ⊑ subst_mty_var_swap x τ).
Proof.
  - destruct H as [H1 H2]. cbn. apply sublty_arr; eapply submty_subst_var_swap; eauto.
  - destruct H.
    + cbn. apply submty_Nat. unfold subst_var_swap_fun. now apply subst_var_swap_fun_sem_eq.
    + rewrite !subst_mty_var_swap_eq_Quant. apply submty_Quant.
      * eapply sublty_mono_Φ; eauto. hnf; intros xs (Hxs1&Hxs2). rewrite subst_var_swap_fun_FS; cbn. firstorder.
      * now apply subst_var_swap_fun_sem_le.
Qed.

Lemma eqlty_subst_var_swap {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S (S ϕ))) (A B : lty (S (S ϕ))) :
  lty! Φ ⊢ A ≡ B ->
  lty! subst_var_swap_fun x Φ ⊢ subst_lty_var_swap x A ≡ subst_lty_var_swap x B.
Proof. now intros [H1 H2]; split; eapply sublty_subst_var_swap. Qed.
Lemma eqmty_subst_var_swap {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S (S ϕ))) (σ τ : mty (S (S ϕ))) :
  mty! Φ ⊢ σ ≡ τ ->
  mty! subst_var_swap_fun x Φ ⊢ subst_mty_var_swap x σ ≡ subst_mty_var_swap x τ.
Proof. now intros [H1 H2]; split; eapply submty_subst_var_swap. Qed.


Lemma sublty_subst_swap {ϕ} (Φ : constr (S (S ϕ))) (A B : lty (S (S ϕ))) :
  lty! Φ ⊢ A ⊑ B ->
  lty! subst_swap_fun Φ ⊢ subst_lty_swap A ⊑ subst_lty_swap B.
Proof. intros H. unfold subst_lty_swap. rewrite !subst_swap_fun_correct. now eapply sublty_subst_var_swap. Qed.
Lemma submty_subst_swap {ϕ} (Φ : constr (S (S ϕ))) (σ τ : mty (S (S ϕ))) :
  mty! Φ ⊢ σ ⊑ τ ->
  mty! subst_swap_fun Φ ⊢ subst_mty_swap σ ⊑ subst_mty_swap τ.
Proof. intros H. unfold subst_mty_swap. rewrite !subst_swap_fun_correct. now eapply submty_subst_var_swap. Qed.

Lemma eqlty_subst_swap {ϕ} (Φ : constr (S (S ϕ))) (A B : lty (S (S ϕ))) :
  lty! Φ ⊢ A ≡ B ->
  lty! subst_swap_fun Φ ⊢ subst_lty_swap A ≡ subst_lty_swap B.
Proof. now intros [H1 H2]; split; eapply sublty_subst_swap. Qed.
Lemma eqmty_subst_swap {ϕ} (Φ : constr (S (S ϕ))) (σ τ : mty (S (S ϕ))) :
  mty! Φ ⊢ σ ≡ τ ->
  mty! subst_swap_fun Φ ⊢ subst_mty_swap σ ≡ subst_mty_swap τ.
Proof. now intros [H1 H2]; split; eapply submty_subst_swap. Qed.


(** This was easy... Now let's proof the typing lemma... *)


Require Import Program.Equality.

(* TODO: Move *)
Lemma subst_ctx_scons {ϕ1 ϕ2} (τ : mty ϕ1) (Γ : ctx ϕ1) (f : idx ϕ1 -> idx ϕ2) :
  (subst_ctx (τ .: Γ) f) = (subst_mty τ f .: (subst_ctx Γ f)).
Proof. fext; intros [ | y]; reflexivity. Qed.

(* TODO: Move *)
Lemma lt_eq_congr   (a a' b b' : nat) : a < b -> a = a' -> b = b' -> a' < b'. Proof. lia. Qed.
Lemma lt_eq_congr_l (a b b' : nat) : a < b -> b = b' -> a < b'. Proof. lia. Qed.
Lemma lt_eq_congr_r (a a' b : nat) : a < b -> a = a' -> a' < b. Proof. lia. Qed.


(* TODO: Move *)
Lemma Sum_idx_congr I1 I2 K :
  I1 = I2 -> (Σ_{a < I1} K a) = (Σ_{a < I2} K a).
Proof. congruence. Qed.
Lemma Sum_idx_congr_ext I1 I2 K1 K2 :
  I1 = I2 ->
  (forall a, a < I1 -> K1 a = K2 a) ->
  (Σ_{a < I1} K1 a) = (Σ_{a < I2} K2 a).
Proof. intros ->. apply Sum_ext. Qed.



(** Substitution in (bounded) sums *)

Lemma msum_subst_var_swap {ϕ} (x : Fin.t (S ϕ)) (τ1 τ2 ρ : mty (S (S ϕ))) :
  msum τ1 τ2 ρ ->
  msum (subst_mty_var_swap x τ1) (subst_mty_var_swap x τ2) (subst_mty_var_swap x ρ).
Proof.
  destruct τ1, τ2, ρ; cbn; try tauto.
  - now intros [-> ->].
  - intros (->&->&H).
    repeat_split.
    + reflexivity.
    + setoid_rewrite subst_lty_twice. reflexivity.
    + firstorder.
Qed.

Lemma bsum_Nat_subst_var_swap {ϕ} (x : Fin.t (S ϕ)) (I : idx (S (S ϕ))) (τ : mty (S (S (S ϕ)))) ρ :
  bsum_Nat I τ ρ ->
  bsum_Nat (subst_var_swap_fun x I) (subst_mty_var_swap (FS x) τ) (subst_mty_var_swap x ρ).
Proof. intros (J&->&->). eexists. split. 2: reflexivity. cbn. now rewrite subst_var_swap_fun_FS. Qed.

Lemma bsum_Quant_subst_var_swap {ϕ} (x : Fin.t (S ϕ)) (I : idx (S (S ϕ)))
      (τ : mty (S (S (S ϕ)))) ρ :
  bsum_Quant I τ ρ ->
  bsum_Quant (subst_var_swap_fun x I) (subst_mty_var_swap (FS x) τ) (subst_mty_var_swap x ρ).
Proof.
  intros (J&A&->&->). eexists _,_; split.
  2:{ cbn. f_equal. fext; intros ys.
      unfold subst_var_swap_fun; cbn; apply Sum_ext; intros a Ha.
      instantiate (1 := fun ys => J (hd ys ::: (fun ys' => _) (tl ys))); cbn; reflexivity.
  }
  cbn. f_equal.
  - unfold idx_cast. fext; intros xs. rewrite subst_var_swap_fun_FS. reflexivity.
  - repeat setoid_rewrite subst_lty_twice; unfold funcomp; cbn. now rewrite subst_var_swap_fun_FS.
Qed.

Lemma bsum_subst_var_swap {ϕ} (x : Fin.t (S ϕ)) (I : idx (S (S ϕ))) (τ : mty (S (S (S ϕ)))) ρ :
  bsum I τ ρ ->
  bsum (subst_var_swap_fun x I) (subst_mty_var_swap (FS x) τ) (subst_mty_var_swap x ρ).
Proof.
  intros [H|H]; [left|right].
  - now eapply bsum_Nat_subst_var_swap.
  - now eapply bsum_Quant_subst_var_swap.
Qed.


Lemma tyT_subst_var_swap {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S (S ϕ))) (Γ : ctx (S (S ϕ))) (M : idx (S (S ϕ)))
      (t : tm) (ρ : mty (S (S ϕ))) (s : skel) :
  (Ty! Φ; Γ ⊢(M) t : ρ @ s) ->
  (Ty! subst_var_swap_fun x Φ; subst_ctx Γ (subst_var_swap_fun x) ⊢(subst_var_swap_fun x M) t : subst_mty_var_swap x ρ @ s).
Proof.
  intros ty. dependent induction ty.
  - (* Var *) apply tyT_Var. now apply eqmty_subst_var_swap.
    { hnf; intros xs Hxs. now erewrite HM by eauto. }

  - (* Lam *)
    specialize IHty with (1 := eq_refl) (2 := JMeq_refl) (3 := JMeq_refl) (4 := JMeq_refl) (5 := JMeq_refl).
    specialize IHty with (x := Fin.FS x).
    eapply tyT_Lam with (I0 := subst_var_swap_fun x I).
    + eapply tyT_sub. eapply tyT_weaken_Φ.
      * eapply IHty.
      * rewrite subst_var_swap_fun_FS. cbn. hnf; intros xs (Hxs1&Hxs2); split.
        -- eapply lt_eq_congr_l; eauto.
        -- apply Hxs2.
      * rewrite subst_ctx_scons. reflexivity.
      * reflexivity.
      * hnf; reflexivity.
    + hnf; intros y Hy. apply bsum_subst_var_swap; eauto.
    + hnf; intros y Hy; cbn. apply eqmty_subst_var_swap; auto.
    + hnf; intros xs Hxs. rewrite subst_var_swap_fun_FS; cbn. unfold subst_var_swap_fun; now rewrite <- HM by firstorder.
    + repeat change (subst_mty ?σ (subst_var_swap_fun ?x)) with (subst_mty_var_swap x σ).
      etransitivity.
      2:{ eapply eqmty_subst_var_swap. apply Hρ. }
      rewrite subst_mty_var_swap_eq_Quant.
      cbn. apply eqmty_Quant.
      * reflexivity.
      * hnf; reflexivity.
  - (* Fix *)
    specialize IHty with (1 := eq_refl) (2 := JMeq_refl) (3 := JMeq_refl) (4 := JMeq_refl) (5 := JMeq_refl).
    specialize IHty with (x := Fin.FS x).
    eapply tyT_Fix with
        (A0 := subst_lty_var_swap (Fin.FS (Fin.FS x)) A) (* Skip [a] and [b] *)
        (K0 := subst_var_swap_fun x K)
        (cardH0 := subst_var_swap_fun x cardH)
        (Ib0 := subst_var_swap_fun (Fin.FS x) Ib) (* Skip [a] *)
        (card3 := subst_var_swap_fun (X := nat) (Fin.FS (Fin.FS x)) card1)
        (card4 := subst_var_swap_fun (X := nat) (Fin.FS x) card2).
    + eapply tyT_sub. eapply tyT_weaken_Φ.
      * apply IHty.
      * rewrite subst_var_swap_fun_FS. cbn. hnf; intros xs (Hxs1&Hxs2); split.
        -- eapply lt_eq_congr_l; eauto.
        -- apply Hxs2.
      * cbn. rewrite subst_ctx_scons. setoid_rewrite subst_mty_var_swap_eq_Quant. reflexivity.
      * setoid_rewrite subst_mty_var_swap_eq_Quant. cbn. reflexivity.
      * cbn. hnf; reflexivity.
    + cbn.
      etransitivity.
      2:{
        eapply eqlty_mono_Φ.
        - eapply eqlty_subst_var_swap with (x0 := Fin.FS (Fin.FS x)). apply Hsub.
        - hnf; intros xs (Hxs1 & Hxs2 & Hxs3).
          rewrite !subst_var_swap_fun_FS in Hxs1; cbn in Hxs1. rewrite !subst_var_swap_fun_FS; cbn. firstorder.
      }
      setoid_rewrite subst_lty_twice; unfold funcomp; cbn. eapply eqlty_subst_idx_sem.
      hnf; intros xs (Hxs1 & Hxs2 & Hxs3). intros j. rewrite !subst_var_swap_fun_FS; cbn. reflexivity.
    + hnf; intros xs Hxs. rewrite subst_var_swap_fun_FS. firstorder.
    + hnf; intros xs (Hxs1 & Hxs2 & Hxs3).
      rewrite subst_var_swap_fun_FS in Hxs1; cbn in Hxs1.
      rewrite !subst_var_swap_fun_FS; cbn.
      apply (Hcard1 (hd xs ::: hd (tl xs) ::: _)) with (1 := conj Hxs1 (conj Hxs2 Hxs3)); cbn in Hcard2.
    + cbn. hnf in Hcard2|-*; intros xs (Hxs1 & Hxs2).
      rewrite subst_var_swap_fun_FS; cbn.
      apply (Hcard2 (hd xs ::: _)) with (1 := conj Hxs1 Hxs2); cbn in Hcard2.
    + hnf; intros y Hy. apply bsum_subst_var_swap; eauto.
    + hnf; intros y Hy; cbn. apply eqmty_subst_var_swap; auto.
    + hnf; intros xs Hxs. rewrite subst_var_swap_fun_FS; cbn. unfold subst_var_swap_fun; now rewrite <- HM by firstorder.
    + cbn.
      etransitivity.
      2:{ eapply eqmty_subst_var_swap with (x0 := x). apply Hρ. }
      rewrite subst_mty_var_swap_eq_Quant.
      setoid_rewrite subst_lty_twice; unfold funcomp; cbn.
      apply eqmty_Quant. 2: hnf; reflexivity.
      eapply eqlty_subst_idx_sem. hnf; intros xs (Hxs1 & Hxs2). intros j. rewrite !subst_var_swap_fun_FS; cbn. reflexivity.

  - (* App *)
     eapply tyT_skel_congr. eapply tyT_App.
    + eapply tyT_sub. eapply tyT_weaken_Φ.
      * eapply IHty1; eauto.
      * refine (fun xs Hxs => Hxs).
      * reflexivity.
      * erewrite subst_mty_var_swap_eq_Quant. cbn. reflexivity.
      * hnf; reflexivity.
    + eapply tyT_sub. eapply tyT_weaken_Φ.
      * eapply IHty2; eauto.
      * refine (fun xs Hxs => Hxs).
      * reflexivity.
      * change (subst_mty σ (subst_var_swap_fun (FS x))) with (subst_mty_var_swap (FS x) σ).
        setoid_rewrite subst_mty_twice. rewrite subst_var_swap_fun_FS. reflexivity.
      * hnf; reflexivity.
    + hnf; intros y Hy. apply msum_subst_var_swap; auto.
    + hnf; intros y Hy. apply eqmty_subst_var_swap; auto.
    + firstorder.
    + change (subst_mty τ (subst_var_swap_fun (FS x))) with (subst_mty_var_swap (FS x) τ).
      etransitivity.
      2:{ eapply eqmty_subst_var_swap. apply Hρ. }
      setoid_rewrite subst_mty_twice. rewrite subst_var_swap_fun_FS. reflexivity.
    + now rewrite subst_mty_strip.

  - (* Ifz *) eapply tyT_Ifz.
    + eapply tyT_sub. eapply tyT_weaken_Φ.
      * eapply IHty1; eauto.
      * cbn. reflexivity.
      * reflexivity.
      * cbn. reflexivity.
      * hnf; reflexivity.
    + eapply tyT_sub'. eapply tyT_weaken_Φ.
      * eapply IHty2 with (x := x); eauto. reflexivity.
      * firstorder.
      * reflexivity.
      * hnf; reflexivity.
    + eapply tyT_sub'. eapply tyT_weaken_Φ.
      * eapply IHty3; eauto. reflexivity.
      * firstorder.
      * reflexivity.
      * hnf; reflexivity.
    + hnf; intros y Hy. apply msum_subst_var_swap; auto.
    + hnf; intros y Hy. apply eqmty_subst_var_swap; auto.
    + firstorder.

  - (* Const *) eapply tyT_Const.
    change (Nat (iConst n)) with (subst_mty_var_swap x (Nat (iConst n))). now apply eqmty_subst_var_swap.
    { hnf; intros xs Hxs. now erewrite HM by eauto. }
  - (* Succ *) eapply tyT_Succ with (K0 := subst_var_swap_fun x K).
    + change (Nat ?K1) with (subst_mty_var_swap x (Nat K)). eapply IHty; eauto.
    + change (Nat _) with (subst_mty_var_swap x (Nat (K>>S))). now apply eqmty_subst_var_swap.
  - (* Pred *) eapply tyT_Pred with (K0 := subst_var_swap_fun x K).
    + change (Nat ?K1) with (subst_mty_var_swap x (Nat K)). eapply IHty; eauto.
    + change (Nat _) with (subst_mty_var_swap x (Nat (K>>pred))). now apply eqmty_subst_var_swap.
Qed.

Lemma tyT_subst_swap {ϕ} (Φ : constr (S (S ϕ))) (Γ : ctx (S (S ϕ))) (M : idx (S (S ϕ)))
      (t : tm) (ρ : mty (S (S ϕ))) (s : skel) :
  (Ty! Φ; Γ ⊢(M) t : ρ @ s) ->
  (Ty! subst_swap_fun Φ; subst_ctx Γ (subst_swap_fun) ⊢(subst_swap_fun M) t : subst_mty_swap ρ @ s).
Proof.
  intros H. eapply tyT_subst_var_swap with (x := Fin0) in H.
  eapply tyT_sub. eapply tyT_weaken_Φ.
  - apply H.
  - now rewrite subst_swap_fun_correct.
  - rewrite !subst_swap_fun_correct. reflexivity.
  - eapply eqmty_subst_idx_sem. hnf; intros xs Hxs j. now rewrite subst_swap_fun_correct.
  - hnf; intros xs Hxs. now rewrite subst_swap_fun_correct.
Qed.


(* (End of copied lemmas) *)


(** Because [subst_swap_fun] is an invulution, we can also swap in the goal. *)

Lemma subst_swap_fun_twice {X : Type} {ϕ} (xs : Vector.t nat _) (f : Vector.t nat (S (S ϕ)) -> X):
  subst_swap_fun (subst_swap_fun f) xs = f xs.
Proof. unfold funcomp, subst_swap_fun, id; cbn. f_equal. now rewrite <- !eta. Qed.

Lemma subst_swap_fun_twice_fext {X : Type} {ϕ} :
  subst_swap_fun (X := X) (ϕ := ϕ) >> subst_swap_fun = id.
Proof. fext; intros f xs. unfold funcomp, subst_swap_fun, id; cbn. f_equal. now rewrite <- !eta. Qed.


Lemma subst_lty_swap_twice {ϕ} (A : lty (S (S ϕ))) :
  subst_lty_swap (subst_lty_swap A) = A.
Proof.
  setoid_rewrite subst_lty_twice; unfold funcomp, subst_swap_fun; cbn; eta.
  rewrite <- subst_lty_id. f_equal; fext; intros f xs. unfold id.
  f_equal. now rewrite <- !eta.
Qed.

Lemma subst_mty_swap_twice {ϕ} (τ : mty (S (S ϕ))) :
  subst_mty_swap (subst_mty_swap τ) = τ.
Proof.
  setoid_rewrite subst_mty_twice; unfold funcomp, subst_swap_fun; cbn; eta.
  rewrite <- subst_mty_id. f_equal; fext; intros f xs. unfold id.
  f_equal. now rewrite <- !eta.
Qed.


Lemma eqlty_swap {ϕ} (Φ : constr (S (S ϕ))) (A B : lty (S (S ϕ))) :
  lty! subst_swap_fun Φ ⊢ subst_lty_swap A ≡ subst_lty_swap B ->
  lty! Φ ⊢ A ≡ B.
Proof.
  intros H.
  setoid_rewrite <- subst_lty_swap_twice at 1. setoid_rewrite <- subst_lty_swap_twice at 4.
  eapply eqlty_mono_Φ. eapply eqlty_subst_swap; eauto.
  hnf; intros xs Hxs. now rewrite subst_swap_fun_twice.
Qed.

Lemma eqmty_swap {ϕ} (Φ : constr (S (S ϕ))) (σ τ : mty (S (S ϕ))) :
  mty! subst_swap_fun Φ ⊢ subst_mty_swap σ ≡ subst_mty_swap τ ->
  mty! Φ ⊢ σ ≡ τ.
Proof.
  intros H.
  setoid_rewrite <- subst_mty_swap_twice at 1. setoid_rewrite <- subst_mty_swap_twice at 4.
  eapply eqmty_mono_Φ. eapply eqmty_subst_swap; eauto.
  hnf; intros xs Hxs. now rewrite subst_swap_fun_twice.
Qed.

Lemma tyT_swap {ϕ} (Φ : constr (S (S ϕ))) Γ M t τ (s : skel) :
  (Ty! subst_swap_fun Φ; subst_ctx Γ subst_swap_fun ⊢(subst_swap_fun M) t : subst_mty_swap τ @ s) ->
  Ty! Φ; Γ ⊢(M) t : τ @ s.
Proof.
  intros H.
  eapply tyT_sub. eapply tyT_weaken_Φ.
  - apply tyT_subst_swap, H.
  - hnf; intros xs Hxs. now rewrite subst_swap_fun_twice.
  - hnf; intros y Hy. setoid_rewrite subst_mty_swap_twice. reflexivity.
  - setoid_rewrite subst_mty_swap_twice. reflexivity.
  - hnf; intros xs Hxs. now rewrite subst_swap_fun_twice.
Qed.
