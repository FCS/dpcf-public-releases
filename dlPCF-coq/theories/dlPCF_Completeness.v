Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax PCF_Semantics.
Require Import PCF_StepT PCF_TypesT.
Require Import dlPCF_iSubst.
Require Import ForestCard dlPCF_Types dlPCF_Sum dlPCF_Contexts dlPCF_Typing.
Require Import dlPCF_TypingT.
Require Import dlPCF_MakeSum.
Require PCF_Types.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.
Import SigmaTypeNotations.
Open Scope vector_scope.

From Coq Require Import Vector Fin.

Open Scope PCF.

Implicit Types (ϕ : nat).

Require Import dlPCF_Safety.
Require Import dlPCF_ConvSubst.
Require Import dlPCF_SubjExp.

Import Skel.
Import EqNotations.


Theorem subject_expansion {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ)
      (t t' : tm) (κ : stepKind) (s s' : skel)
      (ρ : mty ϕ) (ρ_PCF : PCF.ty)
      (pcfTy : PCF.hastyT (stripCtx Γ) t ρ_PCF) :
  (Ty! Φ; Γ ⊢(M) t' : ρ @ s') ->
  stepT t κ t' ->
  ρ_PCF = mty_strip ρ ->
  s = PCF.strip pcfTy ->
  s' = PCF.skel_red t s ->
  closed t ->
  existsS (N : idx ϕ),
    (Ty! Φ; Γ ⊢(N) t : ρ @ s) **
    (sem! Φ ⊨ fun xs => N xs = costAfter (M xs) κ).
Proof.
  intros Hty Hstep Heqρ Heqs Heqs' Hclos.
  destruct κ; cbn [costAfter].
  - eapply subject_expansion_beta; eauto.
  - pose proof subject_expansion_nat pcfTy Hty Hstep Heqρ Heqs Heqs' Hclos. firstorder.
Qed.


Fixpoint skel_reds (t : tm) (s : skel) (k : nat) : skel :=
  match k with
  | 0 => s
  | S k' =>
    match stepFun t with
    | Some (κ, t') => skel_reds t' (PCF.skel_red t s) k'
    | None => skel_err
    end
  end.


(** Iteratively apply subject expansion *)

Lemma subject_expansion_star {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ)
      (t t' : tm) (k i : nat) (s s' : skel)
      (ρ : mty ϕ) (ρ_PCF : PCF.ty)
      (pcfTy : PCF.hastyT (stripCtx Γ) t ρ_PCF) :
  (Ty! Φ; Γ ⊢(M) t' : ρ @ s') ->
  starBT' k i t t' ->
  ρ_PCF = mty_strip ρ ->
  s = PCF.strip pcfTy ->
  s' = skel_reds t s k ->
  closed t ->
  existsS (N : idx ϕ),
    (Ty! Φ; Γ ⊢(N) t : ρ @ s) **
    (sem! Φ ⊨ fun xs => N xs = i + M xs).
Proof.
  intros Hty Hsteps Heqρ Heqs Heqs' Hclos.
  induction Hsteps as [t | k i t1 t2 t3 Hstep Hstar IH | k i t1 t2 t3 Hstep Hstar IH]
    in s,s',M,pcfTy,Hty,Heqρ,Heqs,Heqs',Hclos|-*; cbn in *; subst.
  - eexists; split; hnf; eauto.
  - rewrite (stepFun_complete (stepT_to_step Hstep)) in *.
    specialize IH with (1 := Hty).
    specialize IH with (pcfTy0 := PCF.preservationT pcfTy Hstep Hclos) (1 := eq_refl) (2 := eq_refl).
    spec_assert IH by now rewrite PCF.preservationT_skel.
    spec_assert IH.
    { eapply preservation_closed; eauto. hnf; eauto using stepT_to_step. }
    destruct IH as (N & IH1 & IH2).
    rewrite PCF.preservationT_skel in IH1.
    pose proof subject_expansion pcfTy IH1 Hstep eq_refl eq_refl eq_refl Hclos as (N' & L1 & L2); cbn in L2.
    eexists; split.
    + apply L1.
    + hnf; intros xs Hxs. now rewrite L2, IH2 by auto.
  - rewrite (stepFun_complete (stepT_to_step Hstep)) in *.
    specialize IH with (1 := Hty).
    specialize IH with (pcfTy0 := PCF.preservationT pcfTy Hstep Hclos) (1 := eq_refl) (2 := eq_refl).
    spec_assert IH by now rewrite PCF.preservationT_skel.
    spec_assert IH.
    { eapply preservation_closed; eauto. hnf; eauto using stepT_to_step. }
    destruct IH as (N & IH1 & IH2).
    rewrite PCF.preservationT_skel in IH1.
    pose proof subject_expansion pcfTy IH1 Hstep eq_refl eq_refl eq_refl Hclos as (N' & L1 & L2); cbn in L2.
    eexists; split.
    + apply L1.
    + hnf; intros xs Hxs. now rewrite L2, IH2 by auto.
Qed.



Lemma PCF_subjRed_stepBT' k i t t' Γ τ
      (ty: PCF.hastyT Γ t τ) :
  starBT' k i t t' ->
  closed t ->
  { ty' : PCF.hastyT Γ t' τ | PCF.strip ty' = skel_reds t (PCF.strip ty) k /\ closed t' }.
Proof.
  induction 1; intros Hclos.
  - cbn. eauto.
  - cbn. rewrite (stepFun_complete (stepT_to_step s)).
    unshelve evar (ty' : PCF.hastyT Γ t2 τ).
    { eapply PCF.preservationT; eauto. }
    specialize IHstarBT' with (ty := ty'); spec_assert IHstarBT'.
    { eapply preservation_closed; hnf; eauto using stepT_to_step. }
    destruct IHstarBT' as (ty'' & IH1 & IH2).
    unfold ty' in IH1; setoid_rewrite PCF.preservationT_skel in IH1.
    eauto.
  - cbn. rewrite (stepFun_complete (stepT_to_step s)).
    unshelve evar (ty' : PCF.hastyT Γ t2 τ).
    { eapply PCF.preservationT; eauto. }
    specialize IHstarBT' with (ty := ty'); spec_assert IHstarBT'.
    { eapply preservation_closed; hnf; eauto using stepT_to_step. }
    destruct IHstarBT' as (ty'' & IH1 & IH2).
    unfold ty' in IH1; setoid_rewrite PCF.preservationT_skel in IH1.
    eauto.
Qed.

Theorem completeness_for_values {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ)
      (t v : tm) (k i : nat) (ρ_PCF : PCF.ty)
      (pcfTy : PCF.hastyT (stripCtx Γ) t ρ_PCF) :
  starBT' k i t v ->
  closed t -> val v ->
  existsS (ρ : mty ϕ),
    (Ty! Φ; Γ ⊢(iConst i) t : ρ @ strip pcfTy) **
    (Ty! Φ; Γ ⊢(iConst 0) v : ρ @ skel_reds t (strip pcfTy) k) **
    mty_strip ρ = ρ_PCF.
Proof.
  intros Hstar Hclos Hval.
  pose proof PCF_subjRed_stepBT' pcfTy Hstar Hclos as (pcfTy' & HpcfTy'_strip & Hclos').
  pose proof tyT_triv_sig Φ pcfTy' Hval Hclos' as (ρ & Hty & Hρ_strip).
  pose proof subject_expansion_star pcfTy Hty Hstar (eq_sym Hρ_strip) eq_refl (HpcfTy'_strip) Hclos as (N & Hty' & HN).
  unfold iConst in HN; rewrite Nat.add_0_r in HN.
  eexists _; repeat_split.
  - eapply tyT_sub'.
    + apply Hty'.
    + reflexivity.
    + apply HN.
  - eapply tyT_skel_congr.
    + apply Hty.
    + assumption.
  - assumption.
Qed.

Corollary completeness_for_programs {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ)
      (t : tm) (n : nat) (k i : nat) (ρ_PCF : PCF.ty)
      (pcfTy : PCF.hastyT (stripCtx Γ) t ρ_PCF) :
  starBT' k i t (Const n) ->
  closed t ->
  Ty! Φ; Γ ⊢(iConst i) t : Nat (iConst n) @ strip pcfTy.
Proof.
  intros Hstar Hclos.
  pose proof completeness_for_values Φ pcfTy Hstar Hclos (val_const _) as (ρ & L1 & L2 & L3).
  assert (mty! Φ ⊢ Nat (iConst n) ≡ ρ) by now inv L2.
  apply eqmty_Nat_inv'_sig in H as (n' & -> & Hn); unfold iConst in *.
  eapply tyT_sub'.
  - apply L1.
  - apply eqmty_Nat. firstorder.
  - hnf; reflexivity.
Qed.


(** Completeness of the [Prop] version [hasty]. *)

Corollary completeness_for_programs' {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (t : tm) (n : nat) (i : nat) (ρ_PCF : PCF.ty) :
  PCF.hastyT (stripCtx Γ) t ρ_PCF ->
  t ≻^(i) (Const n) ->
  closed t ->
  ty! Φ; Γ ⊢(iConst i) t : Nat (iConst n).
Proof.
  intros Hty (k & Hstar) % starB_to_starBT' Hclos.
  eapply hastyT_hasty; eauto.
  unshelve eapply completeness_for_programs; eauto.
Qed.

(* Print Assumptions completeness_for_programs'. *)
(*
Axioms:
- FunctionalExtensionality.functional_extensionality_dep
- JMeq.JMeq_eq
*)
