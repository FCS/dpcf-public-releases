(** ** Well-Founded induction over lexicographical orders *)


Set Implicit Arguments.
From Coq Require Import Relations.
From Coq Require Export Wf_nat.
From Coq Require Import Omega.


Section LexOrderWf.
  Variable A B : Type.
  Variable (R1 : relation A) (R2 : relation B).
  Hypothesis (wf_R1 : well_founded R1) (wf_R2 : well_founded R2).


  Definition lex : relation (A*B) :=
    fun '(a1,b1) => fun '(a2,b2) => R1 a1 a2 \/ (a1 = a2 /\ R2 b1 b2).


  Definition wf_lex : well_founded lex.
  Proof.
    hnf. intros [a b].
    refine ((fix f1 (a : A) (acc_a : Acc R1 a) { struct acc_a } :=
               fix f2 (b : B) (acc_b : Acc R2 b) { struct acc_b } := _)
              a (wf_R1 a) b (wf_R2 b)).
    constructor. intros (a', b') [H | [ -> H]].
    - apply f1. apply acc_a. apply H. apply wf_R2.
    - apply f2. apply acc_b. assumption.
  Defined.

  
End LexOrderWf.


Section Wf_Map.
  Variable X Y : Type.
  Variable f : X -> Y.
  Variable R : relation Y.

  Definition mapRel : relation X :=
    fun a b => R (f a) (f b).

  Lemma wf_mapRel :
    well_founded R ->
    well_founded mapRel.
  Proof.
    unfold well_founded. intros H a. specialize (H (f a)).
    remember (f a) as a'. induction H as [ x _ IH ] in Heqa',a; subst.
    constructor. intros x Hx. unfold mapRel in Hx. 
    now specialize IH with (1 := Hx) (2 := eq_refl).
  Qed.
  
End Wf_Map.


Section LexOrderWf_le.

  Definition lex_lt : relation (nat * nat) :=
    (* fun '(a, c) => fun '(b, d) => a < b \/ (a = b /\ c < d). *)
    @lex nat nat lt lt.

  Variable X : Type.
  
  Definition lexOf (f : X -> nat*nat) : relation X :=
    (* fun a b => lex_lt (f a) (f b). *)
    mapRel f lex_lt.

  Lemma lexof_well_founded f : well_founded (lexOf f).
  Proof. apply wf_mapRel. apply wf_lex; apply lt_wf. Defined.

End LexOrderWf_le.


(** If the first argument did not increase, but the second argument definitely decreases, this is also smaller *)
Lemma lex_le (a1 b1 a2 b2 : nat) :
  a1 <= a2 ->
  b1 < b2 ->
  lex lt lt (a1,b1) (a2, b2).
Proof. intros H1 H2. hnf. omega. Qed.
