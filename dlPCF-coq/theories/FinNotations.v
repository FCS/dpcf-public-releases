(** * Notations for [Fin.t] *)
(* Author: Maxi Wuttke *)

From Coq Require Import Fin Vector.

Delimit Scope vector_scope with vector.

Local Open Scope vector_scope.


Module FinNumNotations.

Notation "'Fin0'"  := (Fin.F1)       : vector_scope.
Notation "'Fin1'"  := (Fin.FS Fin0)  : vector_scope.
Notation "'Fin2'"  := (Fin.FS Fin1)  : vector_scope.

End FinNumNotations.


(** Alternative to [VectorNotations], with different syntax as lists. *)

Module VectorNotations2.

Notation "[| |]" := (@nil _) : vector_scope.
Notation "h ':::' t" := (@cons _ h _ t) (at level 60, right associativity) : vector_scope.

Notation "[ | x | ]" := (x ::: nil) (format "[ | x | ]") : vector_scope.
Notation "[ | x ; y ; .. ; z | ] " := (@cons _ x _ (cons _ y _ .. (cons _ z _ (nil _)) ..))
                                        (format "[ | x ;  y ;  .. ;  z | ]") : vector_scope.
Notation "v [@ p ]" := (@Vector.nth _ _ v p) (at level 1, format "v [@ p ]") : vector_scope.

End VectorNotations2.
