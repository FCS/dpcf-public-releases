Require Import Common.

(** ** Forest cardinality *)

(** *** Forests *)

(** We don't really need the definition of forests here, but it will be
useful to test our definition of forest cardinality *)

Section Forests.

  (** Unlabelled finite trees/forests *)
  Inductive Tree : Type :=
  | Tree_Node : Forest -> Tree
  with Forest : Type :=
  | Forest_nil : Forest
  | Forest_cons : Tree -> Forest -> Forest
  .

  (** Compute the size of a tree/forest *)
  Fixpoint Tree_size (t : Tree) : nat :=
    match t with
    | Tree_Node fs => S (Forest_size fs)
    end
  with Forest_size (fs : Forest) : nat :=
    match fs with
    | Forest_nil => 0
    | Forest_cons t fs => Tree_size t + Forest_size fs
    end.


  (** Get the nth subtree in pre-order *)
  Fixpoint Tree_sub (t : Tree) (i : nat) : option Tree :=
    match i with
    | 0 => Some t (* The current tree itself *)
    | S i =>
      match t with
      | Tree_Node fs =>
        Forest_sub fs i
      end
    end
  with Forest_sub (fs : Forest) (i : nat) : option Tree :=
    match fs with
    | Forest_nil => None
    | Forest_cons t fs' =>
      match Tree_sub t i with
      | Some t => Some t
      | None => Forest_sub fs' (i - Tree_size t)
      end
    end.


  (** Number of subtrees in a tree/forest *)
  Fixpoint Forest_trees (fs : Forest) : nat :=
    match fs with
    | Forest_nil => 0
    | Forest_cons _ fs => S (Forest_trees fs)
    end.
  Definition Tree_children (t : Tree) : nat :=
    match t with
    | Tree_Node fs => Forest_trees fs
    end.


End Forests.


(** *** Definition *)

(**
Note that this definition is different from the original 'paper definition'.
First, we only have two parameters: a function describing the tree, and the
number of trees we should count. The definition using 'starting index' can be
obtained by shifting, see below.

The second difference is the 'direction' in which we count the nodes. We first
compute the size of the first tree and then recursively compute the rest. In
the papers, it is done differently. Our definition makes the proof of the
splitting lemma below much easier. *)


Fixpoint forestCard (K : nat -> nat) (fuel : nat) (j : nat) : option nat :=
  match j with
  | 0 => Some 0
  | S j =>
    match fuel with
    | 0 => None
    | S fuel =>
      bind_option
        (fun x =>
           bind_option
             (fun y => Some (S (x + y)))
             (forestCard (fun a => K (S (x + a))) fuel j))
        (forestCard (fun a => K (S a)) fuel (K 0))
    end
  end.

Lemma forestCard_eq_0 (K : nat -> nat) fuel :
  forestCard K fuel 0 = Some 0.
Proof. destruct fuel; reflexivity. Qed.

Lemma forestCard_eq_S (K : nat -> nat) fuel j :
  forestCard K (S fuel) (S j) =
  bind_option
    (fun x =>
       bind_option
         (fun y => Some (S (x + y)))
         (forestCard (fun a => K (S (x + a))) fuel j))
    (forestCard (fun a => K (S a)) fuel (K 0)).
Proof. reflexivity. Qed.

Lemma forestCard_ext (H1 H2 : nat -> nat) (fuel : nat) (j : nat) :
  (forall x, H1 x = H2 x) ->
  forestCard H1 fuel j = forestCard H2 fuel j.
Proof.
  intros H. induction fuel in j,H,H1,H2|-*.
  - cbn. now destruct j.
  - cbn. destruct j.
    + reflexivity.
    + rewrite H. erewrite IHfuel; eauto.
      2:{ instantiate (1 := fun x => H2 (S x)). cbn. intros x. rewrite H; auto. }
      destruct (forestCard (fun a : nat => H2 (S a)) fuel (H2 0)); cbn; auto.
      erewrite IHfuel; eauto. cbn. intros. now rewrite H.
Qed.

Lemma forestCard_ext' (H1 H2 : nat -> nat) (fuel : nat) (j : nat) (x : option nat) :
  (forall x, H1 x = H2 x) ->
  forestCard H1 fuel j = x ->
  forestCard H2 fuel j = x.
Proof. intros H <-. now apply forestCard_ext. Qed.

Lemma forestCard_fuel_mono (K : nat -> nat) (fuel1 fuel2 : nat) (j : nat) :
  fuel1 <= fuel2 ->
  forall x, forestCard K fuel1 j = Some x ->
       forestCard K fuel2 j = Some x.
Proof.
  induction fuel1 as [ | fuel1 IH] in fuel2,j,K|-*; cbn in *.
  - intros Hle x Heq. destruct j; cbn in *; inv Heq. destruct fuel2; reflexivity.
  - destruct j; cbn in *.
    + destruct fuel2; cbn; congruence.
    + destruct fuel2 as [ | fuel2]; cbn in *; [intros; omega|].
      intros H % le_S_n. intros x Heq.
      destruct (forestCard (fun a : nat => K (S a)) fuel1 (K 0)) as [ y | ] eqn:E1; cbn in *; try discriminate.
      rewrite (IH _ _ _ H _ E1). cbn.
      destruct (forestCard (fun a : nat => K (S (y + a))) fuel1 j) as [ z | ] eqn:E2; cbn in *; try discriminate.
      injection Heq as <-. cbn. rewrite (IH _ _ _ H _ E2). cbn. reflexivity.
Qed.

(* Better for [eauto] *)
Lemma forestCard_fuel_mono' (K : nat -> nat) (fuel1 fuel2 : nat) (j : nat) :
  forall x, forestCard K fuel1 j = Some x ->
       fuel1 <= fuel2 ->
       forestCard K fuel2 j = Some x.
Proof. eauto using forestCard_fuel_mono. Qed.


Ltac eta := repeat change (fun x => ?f x) with f.

Lemma forestCard_split (I1 I2 : nat) (K : nat -> nat) (fuel : nat) (H : nat) :
  forestCard K fuel (I1+I2) = Some H ->
  { H1 & { H2 |
           forestCard K fuel I1 = Some H1 /\
           forestCard (fun a => K (H1 + a)) fuel I2 = Some H2 /\
           H = H1 + H2 } }.
Proof.
  induction fuel in I1,I2,K,H|-*.
  - destruct I1,I2; cbn; try discriminate. intros [= <-]. eauto.
  - destruct I1 as [ | I1'].
    + cbn. destruct I2.
      * intros [= <-]. eauto.
      * destruct (forestCard (fun a : nat => K (S a)) fuel (K 0)) eqn:E1; cbn; try discriminate.
        destruct (forestCard (fun a : nat => K (S (n + a))) fuel I2) eqn:E2; cbn; try discriminate.
        intros [= <-].
        eexists _,_; repeat_split.
        -- reflexivity.
        -- cbn. eta. rewrite E1. cbn. rewrite E2. cbn. reflexivity.
        -- cbn. reflexivity.
    + cbn [plus].
      rewrite forestCard_eq_S.
      destruct (forestCard (fun a : nat => K (S a)) fuel (K 0)) as [H1'| ] eqn:E1; cbn [bind_option]; try discriminate.
      destruct (forestCard (fun a : nat => K (S (H1' + a))) fuel (I1' + I2)) as [H2'| ] eqn:E2; cbn [bind_option]; try discriminate.
      intros [= <-].
      specialize (IHfuel I1' I2) with (1 := E2) as (H1''&H2''&IH1&IH2&->).

      rewrite forestCard_eq_S.
      rewrite E1. cbn [bind_option]. rewrite IH1. cbn [bind_option].
      eexists _,_; repeat_split; eauto.
      * eapply forestCard_fuel_mono.
        2:{ rewrite <- IH2. eapply forestCard_ext; eauto. intros; f_equal; lia. }
        lia.
      * lia.
Qed.

Lemma forestCard_merge (I1 I2 : nat) (K : nat -> nat) (fuel1 fuel2 : nat) (H1 H2 : nat) :
  forestCard K fuel1 I1 = Some H1 ->
  forestCard (fun a => K (H1 + a)) fuel2 I2 = Some H2 ->
  exists fuel', forestCard K fuel' (I1+I2) = Some (H1+H2).
Proof.
  induction fuel1 as [ | fuel1 IH] in I1,I2,K,H1,H2,fuel2|-*.
  - destruct I1,I2; cbn; intros [= <-]; exists fuel2; eauto.
  - destruct I1 as [ | I1'].
    + cbn. destruct I2.
      * intros [= <-]. exists fuel2. eauto.
      * intros [= <-]. cbn. eauto.
    + cbn [plus]. rewrite forestCard_eq_S.
      destruct (forestCard (fun a : nat => K (S a)) fuel1 (K 0)) eqn:E1; cbn [bind_option]; try discriminate.
      destruct (forestCard (fun a : nat => K (S (n + a))) fuel1 I1') eqn:E2; cbn [bind_option]; try discriminate.
      intros [= <-] E3.
      assert (forestCard (fun a : nat => K (S (n + (n0 + a)))) fuel2 I2 = Some H2) as E3'.
      { eapply forestCard_ext'; cbn; eauto. intros x. cbn. f_equal. lia. }
      specialize IH with (1 := E2) (2 := E3') as (fuel'&IH); cbn in IH.
      eexists (S (max fuel1 fuel')). rewrite forestCard_eq_S.
      assert (forestCard (fun a : nat => K (S a)) (max fuel1 fuel') (K 0) = Some n) as E1'.
      { eapply forestCard_fuel_mono. 2: eauto. lia. }
      rewrite E1'. cbn [bind_option].
      assert (forestCard (fun a : nat => K (S (n + a))) (max fuel1 fuel') (I1' + I2) = Some (n0 + H2)) as IH'.
      { eapply forestCard_fuel_mono. 2: eauto. lia. }
      rewrite IH'. cbn. f_equal. lia.
Qed.


Definition forestCard_shift (K : nat -> nat) (fuel : nat) (I J : nat) :=
  forestCard (fun a => K (I + a)) fuel J.

Lemma forestCard_shift0 (K : nat -> nat) (fuel : nat) (J : nat) :
  forestCard_shift K fuel 0 J = forestCard K fuel J.
Proof. unfold forestCard_shift. apply forestCard_ext. intros; f_equal; lia. Qed.


Definition isForestCard (K : nat -> nat) (j : nat) (x : nat) :=
  exists fuel, forestCard K fuel j = Some x.

Definition isForestCard_shift (K : nat -> nat) (i j : nat) (x : nat) :=
  exists fuel, forestCard_shift K fuel i j = Some x.


Definition isForestCard_constr (K : nat -> nat) (j : nat) (x : nat) fuel :
  forestCard K fuel j = Some x ->
  isForestCard K j x.
Proof. unfold isForestCard; eauto. Qed.

Lemma isForestCard_congr (K : nat -> nat) (j : nat) (H1 H2 : nat) :
  isForestCard K j H1 ->
  H1 = H2 ->
  isForestCard K j H2.
Proof. congruence. Qed.


From Coq Require ConstructiveEpsilon.
Lemma isForestCard_sig (K : nat -> nat) (j : nat) (x : nat) :
  isForestCard K j x ->
  { fuel | forestCard K fuel j = Some x }.
Proof.
  refine (ConstructiveEpsilon.constructive_indefinite_ground_description_nat (fun fuel => forestCard K fuel j = Some x) _).
  decide equality. apply Nat.eq_dec.
Qed.
Lemma isForestCard_shift_sig (K : nat -> nat) (i j : nat) (x : nat) :
  isForestCard_shift K i j x ->
  { fuel | forestCard_shift K fuel i j = Some x }.
Proof. apply isForestCard_sig. Qed.

Lemma isForestCard_functional (K : nat -> nat) (j : nat) (x1 x2 : nat) :
  isForestCard K j x1 ->
  isForestCard K j x2 ->
  x1 = x2.
Proof.
  intros (fuel1&H1).
  intros (fuel2&H2).
  assert (fuel1 <= fuel2 \/ fuel2 <= fuel1) as [Hle|Hle] by omega.
  + erewrite forestCard_fuel_mono in H2; eauto. congruence.
  + erewrite forestCard_fuel_mono in H1; eauto. congruence.
Qed.


Lemma forestCard_fuel_Some (K : nat -> nat) (fuel1 fuel2 : nat) (j : nat) (x1 x2 : nat) :
  forestCard K fuel1 j = Some x1 ->
  forestCard K fuel1 j = Some x2 ->
  x1 = x2.
Proof. intros. eapply isForestCard_functional; hnf; eauto. Qed.


Lemma isForestCard_shift_functional (K : nat -> nat) (i j : nat) (x1 x2 : nat) :
  isForestCard_shift K i j x1 ->
  isForestCard_shift K i j x2 ->
  x1 = x2.
Proof. apply isForestCard_functional. Qed.


Lemma isForestCard_split (K : nat -> nat) (I1 I2 : nat) (H : nat) :
  isForestCard K (I1+I2) H ->
  { H1 & { H2 |
           isForestCard K I1 H1 /\
           isForestCard (fun a => K (H1 + a)) I2 H2 /\
           H = H1 + H2 } }.
Proof.
  intros (fuel & (H1&H2&E1&E2&->) % forestCard_split) % isForestCard_sig.
  exists H1, H2. unfold isForestCard. eauto.
Qed.

Lemma isForestCard_merge (I1 I2 : nat) (K : nat -> nat) (H1 H2 : nat) :
  isForestCard K I1 H1 ->
  isForestCard (fun a => K (H1 + a)) I2 H2 ->
  isForestCard K (I1+I2) (H1+H2).
Proof.
  intros (fuel1&E1) % isForestCard_sig.
  intros (fuel2&E2) % isForestCard_sig.
  pose proof forestCard_merge _ _ _ _ _ E1 E2 as (fuel3&E3) % isForestCard_sig.
  unfold isForestCard. eauto.
Qed.



(** Constructors / destructors for [isForestCard] *)

Lemma isForestCard_0 (K : nat -> nat) :
  isForestCard K 0 0.
Proof. exists 0. reflexivity. Qed.

Lemma isForestCard_S (K : nat -> nat) (j : nat) (x y : nat) :
  isForestCard (fun a => K (S a))       (K 0) x ->
  isForestCard (fun a => K (S (x + a))) j     y ->
  isForestCard K (S j) (S (x + y)).
Proof.
  intros (fuel1 & H1) (fuel2 & H2).
  exists (S (max fuel1 fuel2)). cbn.
  rewrite forestCard_fuel_mono with (2 := H1) by lia. cbn [bind_option].
  rewrite forestCard_fuel_mono with (2 := H2) by lia. reflexivity.
Qed.

Lemma isForestCard_0_inv (K : nat -> nat) x :
  isForestCard K 0 x ->
  x = 0.
Proof. intros (fuel & H). rewrite forestCard_eq_0 in H. congruence. Qed.

Lemma isForestCard_S_inv (K : nat -> nat) (j : nat) (z : nat) :
  isForestCard K (S j) z ->
  { x & { y |
    isForestCard (fun a => K (S a))       (K 0) x /\
    isForestCard (fun a => K (S (x + a))) j     y /\
    z = S (x + y) } }.
Proof.
  intros (fuel & H) % isForestCard_sig. destruct fuel; cbn in *; try discriminate.
  destruct (forestCard (fun a : nat => K (S a)) fuel (K 0)) eqn:E1; cbn [bind_option] in *; try discriminate.
  destruct (forestCard (fun a : nat => K (S (n + a))) fuel j) eqn:E2; cbn [bind_option] in *; try discriminate.
  injection H as <-. unfold isForestCard. eexists _, _; repeat_split; eauto.
Qed.



(** The following lemma is a generalisation of [forestCard_split].
If the counter is a sum itself, we can express the cardinality by inductively applying this lemma. *)

Fixpoint forestCard_split_Sum_fun (I : nat -> nat) (L : nat -> nat) (fuel : nat) (d : nat) { struct d } : option nat :=
  match d with
  | 0 => forestCard I fuel (L 0)
  | S d =>
    bind_option
      (fun x =>
         forestCard_split_Sum_fun (fun a => I (x + a)) (fun b => L (S b)) fuel d)
      (forestCard I fuel (L 0))
  end.

Lemma forestCard_split_Sum_fun_fuel_mono (I : nat -> nat) (L : nat -> nat) (fuel1 fuel2 : nat) (d : nat) (x : nat) :
  fuel1 <= fuel2 ->
  forestCard_split_Sum_fun I L fuel1 d = Some x ->
  forestCard_split_Sum_fun I L fuel2 d = Some x.
Proof.
  induction d in x,I,L|-*; intros Hfuel; cbn.
  - eintros H % forestCard_fuel_mono; eauto.
  - destruct (forestCard I fuel1 (L 0)) eqn:E; cbn; try discriminate.
    eapply forestCard_fuel_mono in E; eauto. rewrite E. cbn. auto.
Qed.

Lemma forestCard_split_Sum_fun_fuel_mono' (I : nat -> nat) (L : nat -> nat) (fuel1 fuel2 : nat) (d : nat) (x : nat) :
  forestCard_split_Sum_fun I L fuel1 d = Some x ->
  fuel1 <= fuel2 ->
  forestCard_split_Sum_fun I L fuel2 d = Some x.
Proof. eauto using forestCard_split_Sum_fun_fuel_mono. Qed.

Lemma isForestCard_split_Sum_fun_functional (I : nat -> nat) (L : nat -> nat) (fuel1 fuel2 : nat) (d : nat) (x1 x2 : nat) :
  forestCard_split_Sum_fun I L fuel1 d = Some x1 ->
  forestCard_split_Sum_fun I L fuel2 d = Some x2 ->
  x1 = x2.
Proof.
  intros H1 H2.
  assert (fuel1 <= fuel2 \/ fuel2 <= fuel1) as [Hle|Hle] by omega.
  + erewrite forestCard_split_Sum_fun_fuel_mono in H2; eauto. congruence.
  + erewrite forestCard_split_Sum_fun_fuel_mono in H1; eauto. congruence.
Qed.


Lemma forestCard_split_Sum (L : nat -> nat) (J : nat) (I : nat -> nat) (fuel : nat) x :
  forestCard I fuel (Σ_{a < J} L a) = Some x ->
  { H : nat -> nat |
    (forall c, c < J -> forestCard_split_Sum_fun I L fuel c = Some (H c)) /\
    ((Σ_{c < J} H c) = x) /\
    (forall c, c < J -> exists fuel', forestCard I fuel' (Σ_{d < c} L d) = Some (Σ_{d < c} H d))
  }.
Proof.
  induction J in L,I,x|-*.
  - cbn. rewrite forestCard_eq_0. intros [= <-].
    exists (fun _ => 0). repeat_split; eauto.
    + intros; exfalso; lia.
    + exists 0. intros; exfalso; lia.
  - rewrite !Sum_eq_S'.
    intros (H1&H2&HH1&HH2&->) % forestCard_split.
    specialize IHJ with (1 := HH2) as (H&HH&IH1&IH2).
    exists (fun d => match d with 0 => H1 | S d => H d end).
    repeat_split.
    + intros [ | d].
      * intros _. eauto.
      * intros Hd % lt_S_n. rewrite <- HH by auto. cbn. rewrite HH1. cbn. reflexivity.
    + rewrite Sum_eq_S'; auto.
    + intros [ | c] Hc.
      * exists 0. cbn. reflexivity.
      * apply lt_S_n in Hc. specialize (IH2 c Hc) as (fuel'&IH2).
        rewrite !Sum_eq_S'. epose proof forestCard_merge _ _ _ _ _ HH1 IH2; eauto.
Qed.


Lemma forestCard_merge_Sum (L : nat -> nat) (J : nat) (I : nat -> nat) (fuel : nat) (Hd : nat -> nat) :
  (forall d, d < J -> forestCard_split_Sum_fun I L fuel d = Some (Hd d)) ->
  forall c, c <= J -> exists fuel', forestCard I fuel' (Σ_{d < c} L d) = Some (Σ_{d < c} Hd d).
Proof.
  induction J in L,I,Hd,fuel|-*.
  - cbn. intros _. intros ? -> % Nat.le_0_r.  exists 0. now rewrite forestCard_eq_0.
  - intros H [ | d] HHd'.
    + cbn. exists 0. reflexivity.
    + apply le_S_n in HHd'.
      rewrite !Sum_eq_S'. cbn.
      specialize (H 0 ltac:(lia)) as H'. cbn in H'.
      edestruct IHJ with (I := fun a : nat => I (Hd 0 + a)) (Hd := fun d => (Hd (S d))) (L := fun x : nat => L (S x)) as (fuel'&IH).
      { intros. rewrite <- H by lia. cbn. rewrite H'. cbn. reflexivity. }
      { eauto. }
      eauto using forestCard_merge.
Qed.

Lemma forestCard_merge_Sum' (L : nat -> nat) (J : nat) (I : nat -> nat) (fuel : nat) (Hd : nat -> nat) :
  (forall d, d < J -> forestCard_split_Sum_fun I L fuel d = Some (Hd d)) ->
  exists fuel', forestCard I fuel' (Σ_{c < J} L c) = Some (Σ_{d < J} Hd d).
Proof. eauto using forestCard_merge_Sum. Qed.


Lemma forestCard_split_Sum_fun_correct' (I : nat -> nat) (L : nat -> nat) (fuel : nat) (c : nat) (x y : nat) :
  forestCard_split_Sum_fun I L fuel c = Some x ->
  forestCard I fuel (Σ_{d<c} L d) = Some y ->
  forestCard (fun b => I (y + b)) fuel (L c) = Some x.
Proof.
  induction c as [ | c IH] in I,L,fuel,x,y|-*.
  - cbn. rewrite forestCard_eq_0. intros H [= <-]. cbn. auto.
  - cbn [forestCard_split_Sum_fun].
    rewrite Sum_eq_S'.
    intros HH1 HH2.
    apply forestCard_split in HH2 as (H1&H2&HH2&HH3&->).
    rewrite HH2 in HH1; cbn [bind_option] in HH1.
    specialize IH with (L := fun x => L (S x)) (1 := HH1) (2 := HH3); cbn in IH.
    eapply forestCard_ext'; eauto. cbn. intros; f_equal; lia.
Qed.

Lemma forestCard_split_Sum_fun_correct (I : nat -> nat) (L : nat -> nat) (fuel : nat) (Hd : nat -> nat) (c : nat) :
  (forall d, d <= c -> forestCard_split_Sum_fun I L fuel d = Some (Hd d)) ->
  exists fuel', forestCard (fun b => I ((Σ_{d<c} Hd d) + b)) fuel' (L c) = Some (Hd c).
Proof.
  intros.
  assert (forall d : nat, d < c -> forestCard_split_Sum_fun I L fuel d = Some (Hd d)) as H'.
  { intros. apply H. auto with arith. }
  pose proof forestCard_merge_Sum' _ _ _ _ H' as (fuel'&LL).
  exists (max fuel fuel').
  erewrite <- forestCard_split_Sum_fun_correct' with (fuel := max fuel fuel').
  - reflexivity.
  - specialize (H c (Nat.le_refl c)).
    eapply forestCard_split_Sum_fun_fuel_mono'; eauto. lia.
  - eapply forestCard_fuel_mono'; eauto. lia.
Qed.


Lemma isForestCard_split_Sum (L : nat -> nat) (J : nat) (I : nat -> nat) x :
  isForestCard I (Σ_{a < J} L a) (x) ->
  { fuel : nat &
  { H : nat -> nat |
    (forall c, c < J -> forestCard_split_Sum_fun I L fuel c = Some (H c)) /\
    ((Σ_{c < J} H c) = x) /\
    (forall c, c < J -> isForestCard I (Σ_{d < c} L d) (Σ_{d < c} H d))
  } }.
Proof.
  intros (fuel&HH) % isForestCard_sig.
  exists fuel.
  pose proof forestCard_split_Sum _ _ _ _ HH as (H&HH1&HH2&HH3).
  exists H. repeat_split; eauto.
Qed.

Lemma isForestCard_split_Sum' (L : nat -> nat) (J : nat) (I : nat -> nat) x :
  isForestCard I (Σ_{c < J} L c) (x) ->
  { H : nat -> nat |
    (Σ_{c < J} H c) = x /\
    (forall c, c < J -> isForestCard I (Σ_{d < c} L d) (Σ_{d < c} H d)) /\
    (forall c, c < J -> isForestCard (fun b => I ((Σ_{d<c} H d) + b)) (L c) (H c))
  }.
Proof.
  intros (fuel&H&HH1&HH2&HH3) % isForestCard_split_Sum.
  exists (fun d => fold_opt id 0 (forestCard_split_Sum_fun I L fuel d)).
  repeat_split.
  - erewrite Sum_ext; eauto. intros d Hd. now rewrite HH1.
  - intros c Hc. specialize (HH3 c Hc) as (fuel'&HH3).
    exists fuel'. rewrite HH3. f_equal. apply Sum_ext.
    intros d Hd. rewrite HH1; cbn; auto. lia.
  - intros c Hc.
    pose proof forestCard_split_Sum_fun_correct I L fuel H (c := c) as (fuel'&HH4).
    { intros d Hd. apply HH1. lia. }
    rewrite HH1 by assumption; cbn [fold_opt].
    exists fuel'. eapply forestCard_ext'; cbn; eauto.
    cbn. intros y. f_equal. f_equal.
    apply Sum_ext. intros a Ha. now rewrite HH1 by lia.
Qed.


(** An instance of the sum splitting lemma: The cardinality of the root can be split up into the sum of the cardinalities of the children. *)

Lemma isForestCard_children (I : nat->nat) (x : nat) :
  isForestCard I 1 (S x) <->
  isForestCard (fun a => I (S a)) (I 0) x.
Proof.
  split; intros (fuel&H).
  - destruct fuel; cbn in H; try discriminate.
    destruct (forestCard (fun a : nat => I (S a)) fuel (I 0)) eqn:E1; cbn in *; try discriminate.
    rewrite forestCard_eq_0 in H. cbn in H. injection H as [= <-].
    rewrite Nat.add_0_r. hnf; eauto.
  - exists (S fuel). cbn. rewrite H. cbn. rewrite forestCard_eq_0. cbn. repeat f_equal. lia.
Qed.

Lemma isForestCard_ge1 (I : nat->nat) (K : nat) x :
  isForestCard I K x -> 1 <= K -> 1 <= x.
Proof.
  intros (fuel&H1) H2.
  destruct K as [ | K]; [exfalso;lia|clear H2].
  destruct fuel; cbn in *; try discriminate.
  do 2 (destruct forestCard; cbn [bind_option] in *; try discriminate).
  injection H1 as <-; lia.
Qed.

Lemma isForestCard_children' (I : nat->nat) (x : nat) :
  isForestCard I 1 x ->
  { y | isForestCard (fun a => I (S a)) (I 0) y /\ x = S y }.
Proof.
  intros H. pose proof isForestCard_ge1 H ltac:(reflexivity). destruct x. omega.
  apply isForestCard_children in H. eauto.
Qed.

Lemma isForestCard_split_children (I : nat -> nat) x :
  isForestCard I 1 x ->
  { H : nat -> nat |
    1 + (Σ_{d < I 0} H d) = x /\
    (isForestCard (fun a : nat => I (S a)) (I 0) (Σ_{d < I 0} H d)) /\
    (forall c : nat, c < I 0 -> isForestCard (fun a => I (S a)) c (Σ_{d < c} H d)) /\
    (forall c : nat, c < I 0 -> isForestCard (fun a => I (S ((Σ_{d < c} H d) + a))) 1 (H c))
  }.
Proof.
  intros H.
  pose proof H as H'.
  apply isForestCard_children' in H as (y&H&->).
  replace (I 0) with (Σ_{_ < I 0} 1) in H by now rewrite Sum_const1.
  apply isForestCard_split_Sum' in H as (H&LL1&LL2&LL3).
  exists H. repeat_split.
  - cbn. f_equal. assumption.
  - rewrite LL1. now apply isForestCard_children.
  - now setoid_rewrite Sum_const1 in LL2.
  - assumption.
Qed.



(** Extensionality of [isForestCard] *)

Lemma isForestCard_ext (I1 I2 : nat -> nat) (K : nat) x :
  isForestCard I1 K x ->
  (forall n, I1 n = I2 n) ->
  isForestCard I2 K x.
Proof. intros (fuel&H1) H2; exists fuel; eauto using forestCard_ext'. Qed.

(** Stronger lemma than [isForestCard_ext] *)

Lemma isForestCard_ext_strong_aux (K1 K2 : nat -> nat) (fuel : nat) (j : nat) (H : nat) :
  forestCard K1 fuel j = Some H ->
  (forall x, x < H -> K1 x = K2 x) ->
  forestCard K2 fuel j = Some H.
Proof.
  intros HH1 HH2. induction fuel in j,K1,K2,H,HH1,HH2|-*; cbn in *.
  - destruct j; auto.
  - destruct j; auto.
    destruct (forestCard (fun a : nat => K1 (S a)) fuel (K1 0)) eqn:E1; cbn [bind_option] in *; try discriminate.
    destruct (forestCard (fun a : nat => K1 (S (n + a))) fuel j) eqn:E2; cbn [bind_option] in *; try discriminate.
    injection HH1 as <-.
    rewrite HH2 in * by lia.
    eapply IHfuel in E1. eapply IHfuel in E2.
    erewrite E1. cbn [bind_option].
    2:{ intros x Hx. rewrite HH2 by lia. reflexivity. }
    rewrite E2. cbn [bind_option].
    2:{ cbn. intros x Hx. now rewrite HH2 by lia. }
    reflexivity.
Qed.

Lemma isForestCard_ext_strong (K1 K2 : nat -> nat) (j : nat) (H : nat) :
  isForestCard K1 j H ->
  (forall x, x < H -> K1 x = K2 x) ->
  isForestCard K2 j H.
Proof. intros (fuel & H1) H2. exists fuel. eapply isForestCard_ext_strong_aux; eauto. Qed.


(** Monotonicity of [forestCard]: If we count more trees, the [forestCard] can't decrease *)
Lemma isForestCard_mono_aux (K : nat -> nat) (fuel : nat) (j1 j2 : nat) (H1 H2 : nat) :
  forestCard K fuel j1 = Some H1 ->
  forestCard K fuel j2 = Some H2 ->
  j1 <= j2 -> H1 <= H2.
Proof.
  intros HH1 HH2 Hj. induction fuel in j1,j2,K,H1,H2,HH1,HH2,Hj|-*; cbn in *.
  - destruct j2; cbn in *; try discriminate.
    assert (j1 = 0) as -> by lia. congruence.
  - destruct j2.
    + assert (j1 = 0) as -> by lia. congruence.
    + destruct j1.
      * injection HH1 as <-. lia.
      * destruct (forestCard (fun a : nat => K (S a)) fuel (K 0)) eqn:E; cbn [bind_option] in *; try discriminate.
        destruct (forestCard (fun a : nat => K (S (n + a))) fuel j2) eqn:E2; cbn [bind_option] in *; try discriminate.
        destruct (forestCard (fun a : nat => K (S (n + a))) fuel j1) eqn:E3; cbn [bind_option] in *; try discriminate.
        injection HH1 as <-. injection HH2 as <-.
        apply le_S_n in Hj. enough (n1 <= n0) by lia.
        eapply IHfuel; eauto.
Qed.

Lemma isForestCard_mono (K : nat -> nat) (j1 j2 : nat) (H1 H2 : nat) :
  isForestCard K j1 H1 ->
  isForestCard K j2 H2 ->
  j1 <= j2 -> H1 <= H2.
Proof.
  intros (fuel1 & HH1) (fuel2 & HH2) Hj.
  eapply isForestCard_mono_aux with (fuel := max fuel1 fuel2).
  - eapply forestCard_fuel_mono'; eauto. lia.
  - eapply forestCard_fuel_mono'; eauto. lia.
  - assumption.
Qed.

(** Strict monotonicity *)
Lemma isForestCard_smono_aux (K : nat -> nat) (fuel : nat) (j1 j2 : nat) (H1 H2 : nat) :
  forestCard K fuel j1 = Some H1 ->
  forestCard K fuel j2 = Some H2 ->
  j1 < j2 -> H1 < H2.
Proof.
  intros HH1 HH2 Hj. induction fuel in j1,j2,K,H1,H2,HH1,HH2,Hj|-*; cbn in *.
  - destruct j2; cbn in *; try discriminate.
    assert (j1 = 0) as -> by lia. congruence.
  - destruct j2.
    + assert (j1 = 0) as -> by lia. congruence.
    + destruct j1.
      * injection HH1 as <-.
        destruct (forestCard (fun a : nat => K (S a)) fuel (K 0)) eqn:E; cbn [bind_option] in *; try discriminate.
        destruct (forestCard (fun a : nat => K (S (n + a))) fuel j2) eqn:E2; cbn [bind_option] in *; try discriminate.
        injection HH2 as <-. lia.
      * destruct (forestCard (fun a : nat => K (S a)) fuel (K 0)) eqn:E; cbn [bind_option] in *; try discriminate.
        destruct (forestCard (fun a : nat => K (S (n + a))) fuel j2) eqn:E2; cbn [bind_option] in *; try discriminate.
        destruct (forestCard (fun a : nat => K (S (n + a))) fuel j1) eqn:E3; cbn [bind_option] in *; try discriminate.
        injection HH1 as <-. injection HH2 as <-.
        apply lt_S_n in Hj. enough (n1 < n0) by lia.
        eapply IHfuel; eauto.
Qed.

Lemma isForestCard_smono (K : nat -> nat) (j1 j2 : nat) (H1 H2 : nat) :
  isForestCard K j1 H1 ->
  isForestCard K j2 H2 ->
  j1 < j2 -> H1 < H2.
Proof.
  intros (fuel1 & HH1) (fuel2 & HH2) Hj.
  eapply isForestCard_smono_aux with (fuel := max fuel1 fuel2).
  - eapply forestCard_fuel_mono'; eauto. lia.
  - eapply forestCard_fuel_mono'; eauto. lia.
  - assumption.
Qed.


(** The forest cardinality is always less than the counter *)
Lemma isForestCard_le (K : nat -> nat) (j : nat) (H : nat) :
  isForestCard K j H ->
  j <= H.
Proof.
  intros (fuel & HH). induction fuel as [ | fuel IH] in K,j,H,HH|-*; cbn in *.
  - destruct j; try discriminate. injection HH as <-. reflexivity.
  - destruct j.
    + injection HH as <-. reflexivity.
    + destruct (forestCard (fun a : nat => K (S a)) fuel (K 0)) eqn:E1; cbn [bind_option] in *; try discriminate.
      destruct (forestCard (fun a : nat => K (S (n + a))) fuel j) eqn:E2; cbn [bind_option] in *; try discriminate.
      injection HH as <-.
      apply le_n_S. apply IH in E1. apply IH in E2. lia.
Qed.


(** Somehow the converse of [isForestCard_smono]: If the second forest card is larger, then the counter also must be larger. *)
Lemma isForestCard_mono_converse (K : nat -> nat) (a b : nat) (H1 H2 : nat) :
  isForestCard K a H1 ->
  isForestCard K b H2 ->
  H1 <= H2 -> a <= b.
Proof.
  intros HH1 HH2 HH3.
  enough (~ b < a) by lia. intros HH4.
  enough (H2 < H1) by lia; clear HH3.
  eapply isForestCard_smono; eauto.
Qed.

Lemma isForestCard_smono_converse (K : nat -> nat) (a b : nat) (H1 H2 : nat) :
  isForestCard K a H1 ->
  isForestCard K b H2 ->
  H1 < H2 -> a < b.
Proof.
  intros HH1 HH2 HH3.
  enough (~ b <= a) by lia. intros HH4.
  enough (H2 <= H1) by lia; clear HH3.
  eapply isForestCard_mono; eauto.
Qed.





(** Appending two forests *)
(** [I1] and [I2] are forest descriptions. [H1] is the cardinality of the first forest. *)
(** Return a description for the new forest where [I2] is appended right to [I1]. *)
Definition app_forests (I1 I2 : nat -> nat) (H1 : nat) : nat -> nat :=
  fun a => if a <? H1 then I1 a else I2 (a - H1).

(** Append two (descriptions of) forests. The second forest is right of the first forest. *)
(** [K1] and [K2] are the number of trees in the first/second forest. *)

Section isForestCard_append.

  Variable (I1 I2 : nat -> nat) (K1 K2 : nat).

  Lemma isForestCard_append_correct0 (H1 H2 : nat) :
    isForestCard I1 K1 H1 ->
    isForestCard I2 K2 H2 ->
    isForestCard (app_forests I1 I2 H1) (K1 + K2) (H1 + H2).
  Proof.
    intros HH1 HH2.
    eapply isForestCard_merge.
    - eapply isForestCard_ext_strong with (1 := HH1).
      unfold app_forests. intros x Hx. now rewrite (proj2 (Nat.ltb_lt _ _)) by assumption.
    - eapply isForestCard_ext_strong with (1 := HH2).
      unfold app_forests. intros x Hx. rewrite (proj2 (Nat.ltb_ge _ _)) by lia.
      f_equal. lia.
  Qed.

  Lemma isForestCard_append_correct1 (a : nat) (H1 H2 : nat) :
    a <= K1 ->
    isForestCard I1 K1 H1 ->
    isForestCard I1 a H2 ->
    isForestCard (app_forests I1 I2 H1) a H2.
  Proof.
    intros HH1 HH2 HH3.
    eapply isForestCard_ext_strong.
    - apply HH3.
    - unfold app_forests. intros x Hx. erewrite (proj2 (Nat.ltb_lt _ _)); auto.
      eapply Nat.lt_le_trans; eauto.
      eapply isForestCard_mono; eauto.
  Qed.


  Lemma isForestCard_append_correct2 (a : nat) (H1 H2 : nat) :
    a >= K1 ->
    isForestCard I1 K1 H1 ->
    isForestCard I2 (a - K1) H2 ->
    isForestCard (app_forests I1 I2 H1) a (H1 + H2).
  Proof.
    intros Ha HH1 HH2.
    replace a with (K1 + (a - K1)) at 1 by lia.
    eapply isForestCard_merge.
    - apply isForestCard_append_correct1 with (a := K1); eauto.
    - unfold app_forests. eapply isForestCard_ext_strong.
      + apply HH2.
      + intros x Hx. rewrite (proj2 (Nat.ltb_ge _ _)) by lia. f_equal. lia.
  Qed.


  (** [a] is on the 'right site'. *)
  Lemma isForestCard_append_correct3 (a : nat) (H1 H2 : nat) :
    a < K1 + K2 ->
    isForestCard I1 K1 H1 ->
    isForestCard (app_forests I1 I2 H1) a H2 ->
    H2 < H1 <-> a < K1.
  Proof.
    intros HH1 HH2 HH3; split; intros HH4.
    - unfold app_forests in HH3.
      eapply isForestCard_ext_strong in HH3.
      2:{ intros x Hx. rewrite (proj2 (Nat.ltb_lt _ _)) by lia. reflexivity. }
      eapply isForestCard_smono_converse; eauto.
    - eapply isForestCard_smono; eauto. apply isForestCard_append_correct1; eauto.
  Qed.


End isForestCard_append.



(** This lemma shows that the [a]th child of a node [b] is in the same forest (with [K] trees). *)
Lemma isForestCard_child_bounded (I : nat -> nat) (a b : nat) (H1 H2 : nat) (K : nat) :
  a <= I b ->
  b < H1 ->
  isForestCard I K H1 ->
  isForestCard (fun c => I (S (b + c))) a H2 ->
  S (b + H2) <= H1.
Proof.
  intros HH1 HH2 (fuel&HH3) HH4.
  revert K I b H1 H2 HH1 HH2 HH3 HH4.
  induction fuel as [ fuel IH] using lt_wf_ind; intros.
  destruct K as [ | K].
  - exfalso. rewrite forestCard_eq_0 in HH3. injection HH3 as [= <-]. lia.
  - destruct fuel; cbn in *; try discriminate.
    destruct (forestCard (fun a : nat => I (S a)) fuel (I 0)) as [x1| ] eqn:Hx1; cbn in *; try discriminate.
    destruct (forestCard (fun a : nat => I (S (x1 + a))) fuel K) as [x2| ] eqn:Hx2; cbn in *; try discriminate.
    injection HH3 as <-.
    apply le_n_S. apply le_S_n in HH2.
    assert (b = 0 \/ 0 < b <= x1 \/ 0 < b /\ x1 < b) as [-> | [Hd | Hd]] by lia.
    + cbn in *. clear HH2 IH.
      replace (I 0) with (a + (I 0 - a)) in Hx1 by lia.
      apply isForestCard_constr in Hx1. apply isForestCard_constr in Hx2.
      apply isForestCard_split in Hx1 as (x11 & x12 & Hx11 & Hx12 & ->).
      pose proof isForestCard_functional Hx11 HH4 as <-. lia.
    + destruct b as [ | b']. 1: exfalso; lia. destruct Hd as [_ Hd].
      specialize IH with (I := fun a : nat => I (S a)) (K := I 0); cbn in IH.
      specialize IH with (4 := Hx1) (5 := HH4).
      repeat spec_assert IH by lia. lia.
    + destruct b as [ | b']. 1: exfalso; lia. destruct Hd as [_ Hd].
      (* assert (b - x1 < S x2) as Hb by lia. *)

      specialize IH with (I := fun a => I (S (x1 + a))) (b := b' - x1); cbn in IH.
      specialize IH with (K := K) (4 := Hx2). specialize IH with (H2 := H2).
      spec_assert IH by lia.
      spec_assert IH by (now replace (S (x1 + (b' - x1))) with (S b') by lia).
      spec_assert IH by lia.
      spec_assert IH.
      { eapply isForestCard_ext; eauto. intros n; cbn. f_equal. lia. }
      lia.
Qed.


(** Version with [a < I b] *)
Lemma isForestCard_child_bounded' (I : nat -> nat) (a b : nat) (H1 H2 : nat) (K : nat) :
  a < I b ->
  b < H1 ->
  isForestCard I K H1 ->
  isForestCard (fun c => I (S (b + c))) a H2 ->
  S (b + H2) < H1.
Proof.
  intros HH1 HH2 (fuel&HH3) HH4.
  revert K I b H1 H2 HH1 HH2 HH3 HH4.
  induction fuel as [ fuel IH] using lt_wf_ind; intros.
  destruct K as [ | K].
  - exfalso. rewrite forestCard_eq_0 in HH3. injection HH3 as [= <-]. lia.
  - destruct fuel; cbn in *; try discriminate.
    destruct (forestCard (fun a : nat => I (S a)) fuel (I 0)) as [x1| ] eqn:Hx1; cbn in *; try discriminate.
    destruct (forestCard (fun a : nat => I (S (x1 + a))) fuel K) as [x2| ] eqn:Hx2; cbn in *; try discriminate.
    injection HH3 as <-.
    apply lt_n_S.
    assert (b = 0 \/ 0 < b <= x1 \/ 0 < b /\ x1 < b) as [-> | [Hd | Hd]] by lia.
    + cbn in *. clear HH2 IH.
      replace (I 0) with (a + (I 0 - a)) in Hx1 by lia.
      apply isForestCard_constr in Hx1. apply isForestCard_constr in Hx2.
      apply isForestCard_split in Hx1 as (x11 & x12 & Hx11 & Hx12 & ->).
      pose proof isForestCard_functional Hx11 HH4 as <-.
      pose proof isForestCard_ge1 Hx12 as L1; spec_assert L1 by lia.
      lia.
    + destruct b as [ | b']. 1: exfalso; lia. destruct Hd as [_ Hd].
      specialize IH with (I := fun a : nat => I (S a)) (K := I 0); cbn in IH.
      specialize IH with (4 := Hx1) (5 := HH4).
      repeat spec_assert IH by lia. lia.
    + destruct b as [ | b']. 1: exfalso; lia. destruct Hd as [_ Hd].
      (* assert (b - x1 < S x2) as Hb by lia. *)

      specialize IH with (I := fun a => I (S (x1 + a))) (b := b' - x1); cbn in IH.
      specialize IH with (K := K) (4 := Hx2). specialize IH with (H2 := H2).
      spec_assert IH by lia.
      spec_assert IH by (now replace (S (x1 + (b' - x1))) with (S b') by lia).
      spec_assert IH by lia.
      spec_assert IH.
      { eapply isForestCard_ext; eauto. intros n; cbn. f_equal. lia. }
      lia.
Qed.



(** Join [L] forests. Converse of [isForestCard_split_Sum'] *)

Section join_forests.

  (** [L]: The number of forests to join *)
  (** [K]: The Number of tres in forest [i < L]  *)
  (** [I i]: Description of forest [i < L] *)
  (** [H i]: The cardinality of the [i]th forest *)
  Fixpoint join_forests (L : nat) (K : nat -> nat) (I : nat -> nat -> nat) (H : nat -> nat) { struct L } : nat -> nat :=
    match L with
    | 0 => I 0
    | S L' => app_forests (I 0) (join_forests L' (fun i => K (S i)) (fun i => I (S i)) (fun i => H (S i))) (H 0)
    end.

  (** The cardinality of the joined forest is equal to the sum of the forest cardinalities. *)
  Lemma isForestCard_join_correct0 (L : nat) (K : nat -> nat) (I : nat -> nat -> nat) (H : nat -> nat) :
    (forall i, i < L -> isForestCard (I i) (K i) (H i)) ->
    isForestCard (join_forests L K I H) (Σ_{i < L} K i) (Σ_{i < L} H i).
  Proof.
    induction L in K,I,H|-*; intros HH.
    - cbn. apply isForestCard_0.
    - cbn [join_forests]. rewrite !Sum_eq_S'. eapply isForestCard_append_correct0.
      + apply HH. lia.
      + apply IHL. intros i Hi. apply HH. lia.
  Qed.


  Lemma join_forests_Sum (L : nat) (K : nat -> nat) (I : nat -> nat -> nat) (H : nat -> nat) (a b : nat) :
    (forall i, i < L -> isForestCard (I i) (K i) (H i)) ->
    a < L ->
    b < H a ->
    join_forests L K I H (b + Σ_{d < a} H d) = I a b.
  Proof.
    intros HH Ha Hb. induction L in K,I,H,a,b,HH,Ha,Hb|-*; cbn [join_forests].
    - exfalso. lia.
    - destruct a.
      + cbn. unfold app_forests. rewrite (proj2 (Nat.ltb_lt _ _)) by lia. f_equal. lia.
      + rewrite Sum_eq_S'. unfold app_forests. rewrite (proj2 (Nat.ltb_ge _ _)) by lia.
        replace (b + (H 0 + (Σ_{x < a} H (S x))) - H 0) with (b + (Σ_{x < a} H (S x))) by lia.
        rewrite IHL; auto.
        * intros i Hi. apply HH. lia.
        * lia.
  Qed.


  (** We compute the size of the first [a < L] forests, and add [b < K a] more trees. *)
  Lemma isForestCard_join_correct1 (L : nat) (K : nat -> nat) (I : nat -> nat -> nat) (H : nat -> nat) (a b : nat) :
    a < L ->
    b < K a ->
    (forall i, i <= a -> isForestCard (I i) (K i) (H i)) ->
    forall x, isForestCard (I a) b x ->
         isForestCard (join_forests L K I H) (b + Σ_{d < a} K d) ((Σ_{c < a} H c) + x).
  Proof.
    intros HHa HHb HH x HHx. induction L in K,I,H,a,b,HHa,HHb,HH,x,HHx|-*; cbn [join_forests].
    - exfalso. lia.
    - destruct a.
      + cbn. rewrite Nat.add_0_r.
        eapply isForestCard_append_correct1 with (K1 := K 0) (H2 := x).
        * lia.
        * now apply HH.
        * now apply HHx.
      + rewrite !Sum_eq_S', !Nat.add_assoc.
        apply lt_S_n in HHa.
        specialize IHL with (K := fun i => K (S i)) (I := fun i => I (S i)) (H := fun i => H (S i)); cbn in IHL.
        specialize IHL with (1 := HHa) (2 := HHb) (4 := HHx); spec_assert IHL.
        { intros i Hi. apply HH. lia. }

        rewrite <- Nat.add_assoc with (p := x).
        eapply isForestCard_append_correct2 with (K1 := K 0).
        * lia.
        * apply HH. lia.
        * replace (b + K 0 + (Σ_{x0 < a} K (S x0)) - K 0) with (b + (Σ_{x0 < a} K (S x0))) by lia.
          apply IHL.
  Qed.


End join_forests.




(** *** Examples *)

(** Description of a forest *)
Definition Forest_description (fs : Forest) : nat -> nat :=
  fun a =>
    match Forest_sub fs a with
    | Some t' => Tree_children t'
    | None => 0 (* arbitrarily *)
    end.



(*
Import ListNotations.

Section Ex.

  Infix ":::" := (Forest_cons) (at level 60, right associativity).

  Definition Tree_Leave : Tree := Tree_Node (Forest_nil).
  Definition Tree_Singleton (t : Tree) : Tree := Tree_Node (t ::: Forest_nil).


  (* Example tree from the paper(s) *)
  Example ex_Forest : Forest :=
    Tree_Singleton
      (Tree_Node
         ((Tree_Node (Tree_Leave ::: Tree_Leave ::: Forest_nil))
            ::: Tree_Leave
            ::: (Tree_Singleton Tree_Leave)
            ::: Forest_nil))
      ::: Tree_Node (Tree_Singleton Tree_Leave ::: Tree_Singleton Tree_Leave ::: Forest_nil)
      ::: Forest_nil
  .

  Compute Forest_size ex_Forest. (* 13 *)

  (* Test *)
  Eval cbv in map (fun i => (i, Forest_description ex_Forest i)) [0;1;2;3;4;5;6;7;8;9;10;11;12].
  (* [(0, 1); (1, 3); (2, 2); (3, 0); (4, 0); (5, 0); (6, 1); (7, 0); (8, 2); (9, 1); (10, 0); (11, 1); (12, 0)] *)

  Example ex_Fuel : nat := Eval cbv in Forest_size ex_Forest. (* This should suffice *) (* 13 *)
  Example ex_Fuel' : nat := 999.
  Example ex_card' (fuel : nat) : nat -> nat -> option nat := forestCard_shift (Forest_description ex_Forest) fuel.
  Example ex_card : nat -> nat -> option nat := ex_card' ex_Fuel.
  Example ex_K := Forest_description ex_Forest.

  Compute ex_card _ 0. (* 0 *)
  Compute ex_card 0 2. (* 13 *)
  Compute ex_card 0 1. (* 8 *)
  Compute ex_card 8 1. (* 5 *)
  Compute ex_card 4 9. (* 14 *)

  (* Test for the splitting lemma *)
  Eval cbv in
    let K1 := 0 in
    let K2 := 1 in
    let K3 := 2 in
    let fuel := 9 in
    match ex_card' fuel K1 (K2+K3) with
    | Some H =>
      match ex_card K1 K2 with
      | Some H1 =>
        match ex_card' fuel (K1+H1) K3 with
        | Some H2 =>
          if H =? H1 + H2 then
            (H, H1, H2)
          else ?[FAIL3]
        | _ => ?[FAIL2]
        end
      | _ => ?[FAIL1]
      end
    | _ => ?[NOFAIL] (* Not enough fuel for [H] *)
    end.



  (* Split the subforest starting at index into two "chunks" (of size 1 and 2, respectively). *)
  (* Each chunk consists of three nodes. *)
  Eval cbv in
      let L := fun a => if a =? 0 then 1 else 2 in
      let s := 2 in (* the offset *)
      let J := 2 in
      let I := fun a => ex_K (s + a) in
      let fuel := ex_Fuel in
      let Hd := fun d => fold_opt id 1000 (forestCard_split_Sum_fun I L fuel d) in
      forestCard I fuel (Σ_{c < J} L c) =
      Some (Σ_{d < J} Hd d).


  (* Test of lemma 3.2 *)
  Eval cbv in
      let s := 1 in (* the offset *)
      let J := 3 in
      let I := fun a => ex_K (s + a) in
      let fuel := ex_Fuel in
      let fc1b := fun b => fold_opt id 1000 (forestCard (fun a => I (S a)) fuel b) in (* cardinality of the first [b] children in [I] *)
      if Nat.leb J (I 0) then
        fc1b J = (Σ_{b < J} fold_opt id 1000 (forestCard (fun a => I (1 + a + fc1b b)) fuel 1))
      else ?[NOFAIL].



  (** Test of [description_append] *)
  Eval cbv in
      let I1 := fun a => ex_K (2 + a) in
      let K1 := 2 in
      let H1 := fold_opt id 1000 (forestCard I1 ex_Fuel K1) in (* H1 = 6 *)
      let I2 := fun a => I1 (H1 + a) in
      let K2 := 2 in
      let H2 := fold_opt id 1000 (forestCard I2 ex_Fuel K2) in (* H2 = 5 *)
      (H1, H2,
       forestCard (app_forests I1 I2 H1) ex_Fuel (K1 + K2) (* = H1 + H2 = 11 *)
      ).

End Ex.
*)
