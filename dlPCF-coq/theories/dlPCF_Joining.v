Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax.
Require Import dlPCF_iSubst.
Require Import ForestCard dlPCF_Types dlPCF_Sum dlPCF_Contexts dlPCF_Typing dlPCF_Splitting.
Require Import dlPCF_TypingT.
Require Import dlPCF_MakeSum.
Require PCF_Types.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.
Import SigmaTypeNotations.
Open Scope vector_scope.

From Coq Require Import Vector Fin.

Open Scope PCF.


Implicit Types (ϕ : nat).


Require Import dlPCF_Safety.

Import Skel.





(** ** Joining Lemma *)


(** *** Case distinction lemma for typing *)


Definition ctx_if {ϕ} (f : Vector.t nat ϕ -> bool) (Γ1 Γ2 : ctx ϕ) : ctx ϕ := fun x => mty_if f (Γ1 x) (Γ2 x).

Lemma ctx_if_scons {ϕ} (f : Vector.t nat ϕ -> bool) (τ1 τ2 : mty ϕ) (Γ1 Γ2 : ctx ϕ) :
  ctx_if f (τ1 .: Γ1) (τ2 .: Γ2) = (mty_if f τ1 τ2 .: ctx_if f Γ1 Γ2).
Proof. fext; intros [ | ]; cbn; auto. Qed.



Lemma subCtx_if_cases {ϕ} (t : tm) (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ1 σ2 ρ1 ρ2 : ctx ϕ) :
  ctx! t; Φ1 ⊢ σ1 ⊑ ρ1 ->
  ctx! t; Φ2 ⊢ σ2 ⊑ ρ2 ->
  (forall x, free x t -> mty_strip (σ1 x) = mty_strip (σ2 x)) ->
  (forall x, free x t -> mty_strip (ρ1 x) = mty_strip (ρ2 x)) ->
  ctx! t; fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢
   ctx_if f σ1 σ2 ⊑ ctx_if f ρ1 ρ2.
Proof. intros. hnf; intros x Hx. apply submty_if_cases; eauto. Qed.
Lemma eqCtx_if_cases {ϕ} (t : tm) (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (σ1 σ2 ρ1 ρ2 : ctx ϕ) :
  ctx! t; Φ1 ⊢ σ1 ≡ ρ1 ->
  ctx! t; Φ2 ⊢ σ2 ≡ ρ2 ->
  (forall x, free x t -> mty_strip (σ1 x) = mty_strip (σ2 x)) ->
  (forall x, free x t -> mty_strip (ρ1 x) = mty_strip (ρ2 x)) ->
  ctx! t; fun xs : Vector.t nat ϕ => if f xs then Φ1 xs else Φ2 xs ⊢
   ctx_if f σ1 σ2 ≡ ctx_if f ρ1 ρ2.
Proof. intros. hnf; intros x Hx. apply eqmty_if_cases; eauto. Qed.


Lemma mty_if_eq_Nat {ϕ} (f : Vector.t nat ϕ -> bool) (I1 I2 : idx ϕ) :
  mty_if f (Nat I1) (Nat I2) = Nat (fun xs => if f xs then I1 xs else I2 xs).
Proof. reflexivity. Qed.

Lemma mty_if_eq_Nat' {ϕ} (f : Vector.t nat ϕ -> bool) (I : idx ϕ) :
  mty_if f (Nat I) (Nat I) = Nat I.
Proof. now cbn; f_equal; fext; intros xs; destruct (f xs). Qed.




Lemma bsum_if {ϕ} (f : Vector.t nat ϕ -> bool) (I1 I2 : idx ϕ) (σ1 σ2 : mty (S ϕ)) (τ1 τ2 : mty ϕ) :
  bsum I1 σ1 τ1 ->
  bsum I2 σ2 τ2 ->
  mty_strip σ1 = mty_strip σ2 ->
  bsum (fun xs => if f xs then I1 xs else I2 xs) (mty_if (fun xs => f (tl xs)) σ1 σ2) (mty_if f τ1 τ2).
Proof.
  intros [H1|H1] [H2|H2]; hnf in H1,H2; cbn in *.
  - destruct H1 as (j1&->&->); destruct H2 as (j2&->&->); cbn.
    intros _. left; hnf. eexists _; repeat_split. 2: reflexivity. reflexivity.
  - destruct H1 as (J1&->&->). destruct H2 as (J2&A&->&->); cbn. destruct A; cbn; discriminate.
  - destruct H2 as (J1&->&->). destruct H1 as (J2&A&->&->); cbn. destruct A; cbn; discriminate.
  - destruct H1 as (J1&A1&->&->); destruct H2 as (J2&A2&->&->); cbn.
    intros H. setoid_rewrite subst_lty_strip in H.
    right; hnf. eexists _, _; split; cbn.
    2:{
      f_equal.
      instantiate (1 := fun xs => if f (tl xs) then J1 xs else J2 xs).
      fext; intros xs. cbn. destruct (f xs); auto.
    }
    1:{
      cbn. f_equal.
      replace (fun xs : Vector.t nat (S (S ϕ)) =>
                 hd xs + (Σ_{d < hd (tl xs)} (if f (tl (tl xs)) then J1 (d ::: tl (tl xs)) else J2 (d ::: tl (tl xs)))))
        with (fun xs => if f (tl (tl xs)) then hd xs + (Σ_{d < hd (tl xs)} J1 (d ::: tl (tl xs)))
                                       else hd xs + (Σ_{d < hd (tl xs)} J2 (d ::: tl (tl xs)))).
      2:{ fext; intros xs. destruct f; cbn; auto. }
      now setoid_rewrite lty_if_subst_beta_two2.
    }
Qed.

Lemma msum_if {ϕ} (f : Vector.t nat ϕ -> bool) (σ1 σ2 σ1' σ2' : mty ϕ) (τ1 τ2 : mty ϕ) :
  msum σ1 σ2 τ1 ->
  msum σ1' σ2' τ2 ->
  mty_strip σ1 = mty_strip σ1' ->
  mty_strip σ2' = mty_strip σ2' ->
  msum (mty_if f σ1 σ1') (mty_if f σ2 σ2') (mty_if f τ1 τ2).
Proof.
  intros H1 H2. destruct σ1, σ2, τ1, σ1', σ2', τ2; cbn in *; try tauto.
  - intros _ _. destruct H1, H2; subst. split; reflexivity.
  - destruct H2 as (-> & -> & H2). destruct A1; cbn; discriminate.
  - destruct H1 as (-> & -> & H1). destruct A1; cbn; discriminate.
  - destruct H1 as (-> & -> & H1), H2 as (-> & -> & H2).
    setoid_rewrite subst_lty_strip. intros H3 H4.
    repeat_split.
    + reflexivity.
    + now rewrite lty_if_shift_add2.
    + intros. now destruct f.
Qed.


Lemma ctxBSum_if {ϕ} (t : tm) (f : Vector.t nat ϕ -> bool) (I1 I2 : idx ϕ) (σ1 σ2 : ctx (S ϕ)) (τ1 τ2 : ctx ϕ) :
  ctxBSum t I1 σ1 τ1 ->
  ctxBSum t I2 σ2 τ2 ->
  (forall x, free x t -> mty_strip (σ1 x) = mty_strip (σ2 x)) ->
  ctxBSum t (fun xs => if f xs then I1 xs else I2 xs) (ctx_if (fun xs => f (tl xs)) σ1 σ2) (ctx_if f τ1 τ2).
Proof.
  intros H1 H2 H3.
  hnf in H1,H2|-*. intros x Hx; specialize (H1 x Hx); specialize (H2 x Hx).
  unfold ctx_if. apply bsum_if; auto.
Qed.


Lemma ctxMSum_if {ϕ} (t : tm) (f : Vector.t nat ϕ -> bool) (σ1 σ2 σ1' σ2' : ctx ϕ) (τ1 τ2 : ctx ϕ) :
  ctxMSum t σ1 σ2 τ1 ->
  ctxMSum t σ1' σ2' τ2 ->
  (forall x, free x t -> mty_strip (σ1 x) = mty_strip (σ1' x)) ->
  (forall x, free x t -> mty_strip (σ2' x) = mty_strip (σ2' x)) ->
  ctxMSum t (ctx_if f σ1 σ1') (ctx_if f σ2 σ2') (ctx_if f τ1 τ2).
Proof.
  intros H1 H2 H3 H4.
  hnf in H1,H2|-*. intros x Hx; specialize (H1 x Hx); specialize (H2 x Hx).
  unfold ctx_if. apply msum_if; auto.
Qed.


Lemma fext_lt_iConst {ϕ} (f : Vector.t nat ϕ -> bool) (x : nat) :
  (fun xs => if f xs then iConst x xs else iConst x xs) = (iConst x).
Proof. fext; intros xs. now destruct f. Qed.

Lemma mty_if_beta_ground_iConst0 {ϕ} (f : Vector.t nat ϕ -> bool) (σ1 σ2 : mty (S ϕ)) :
  mty_strip σ1 = mty_strip σ2 ->
  mty_if f (subst_mty_beta_ground σ1 (iConst 0)) (subst_mty_beta_ground σ2 (iConst 0)) =
  subst_mty_beta_ground (mty_if (fun xs => f (tl xs)) σ1 σ2) (iConst 0).
Proof.
  intros H. erewrite <- mty_if_beta_ground2 by assumption.
  f_equal. now apply fext_lt_iConst.
Qed.


(* TODO: Move *)


Section NonStandardSubstitutionsInTheFixRule.

  Context {ϕ} (card1 : idx (S (S ϕ))).

  (** A substitution function that is needed in [tyT_Fix] *)
  Definition subst_beta_Fix1_fun : idx (S (S ϕ)) -> idx (S (S ϕ)) :=
    (subst_beta_one_two_fun
       (fun xs => let a := hd xs in
               let b := hd (tl xs) in
               let xs' := tl (tl xs) in
               S (card1 xs + b)))
      >> subst_beta_ground_fun (iConst 0).

  (** This holds by conversion; we could also use [change] *)
  Lemma subst_beta_Fix1_fun_correct :
    subst_beta_Fix1_fun =
    (fun f xs =>
       let a := hd xs in
       let b := hd (tl xs) in
       let xs' := tl (tl xs) in
       f (0 ::: S (card1 xs + b) ::: xs')).
  Proof. reflexivity. Qed.

  Lemma subst_lty_beta_Fix1_correct (B : lty (S (S ϕ))) :
    subst_lty
      B
      (fun f xs =>
         let a := hd xs in
         let b := hd (tl xs) in
         let xs' := tl (tl xs) in
         f (0 ::: S (card1 xs + b) ::: xs')) =
    subst_lty_beta_ground
      (subst_lty_beta_one_two
         (fun xs => let a := hd xs in
                 let b := hd (tl xs) in
                 let xs' := tl (tl xs) in
                 S (card1 xs + b)) B)
      (iConst 0).
  Proof. setoid_rewrite subst_lty_twice. reflexivity. Qed.


  Variable (card2 : idx (S ϕ)).

  Definition subst_beta_Fix2_fun : idx (S (S ϕ)) -> idx (S ϕ) :=
    (subst_beta_one_fun 1 card2)
      >> subst_beta_ground_fun (iConst 0).

  Lemma subst_beta_Fix2_fun_correct :
    subst_beta_Fix2_fun =
    (fun f xs =>
       let a := hd xs in
       let xs' := tl xs in
       f (0 ::: (card2 xs) ::: xs')).
  Proof. reflexivity. Qed.

  Lemma subst_lty_beta_Fix2_correct (B : lty (S (S ϕ))) :
    (subst_lty
       B
       (fun f xs =>
          let a := hd xs in
          let xs' := tl xs in
          f (0 ::: (card2 xs) ::: xs'))) =
    subst_lty_beta_ground (subst_lty_beta_one 1 card2 B) (iConst 0).
  Proof. setoid_rewrite subst_lty_twice. reflexivity. Qed.


End NonStandardSubstitutionsInTheFixRule.



Lemma lty_if_subst_beta_one_two {ϕ} (f : Vector.t nat (S (S ϕ)) -> bool) (A1 A2 : lty (S (S ϕ))) (i : idx (S (S ϕ))) :
  lty_strip A1 = lty_strip A2 ->
  subst_lty_beta_one_two i (lty_if f A1 A2) =
  lty_if (fun xs => f (hd xs ::: i (tl xs) ::: tl (tl (tl xs))))
         (subst_lty_beta_one_two i A1) (subst_lty_beta_one_two i A2).
Proof.
  intros H.
  rewrite !subst_lty_beta_one_two_correct.
  unshelve erewrite !subst_lty_beta_one_correct. 1-3: reflexivity.
  rewrite lty_if_subst_var_beta by auto.
  rewrite lty_cast_id.
  unfold subst_lty_var_beta.
  erewrite !subst_var_beta_fun_FS.
  rewrite !subst_var_beta_fun_Fin0.
  f_equal.
  - fext; intros xs. cbn. unfold subst_beta_fun. f_equal. f_equal. f_equal.
    + now rewrite vect_cast_id.
    + f_equal.
      * f_equal. now simp_vect_to_list.
      * simp_vect_to_list. now rewrite !skipn_tl, skipn_O.
  - rewrite lty_cast_id. reflexivity.
  - rewrite lty_cast_id. reflexivity.
  Unshelve.
  all: abstract lia.
Qed.

Lemma lty_if_subst_beta_one_one {ϕ} (f : Vector.t nat (S (S ϕ)) -> bool) (A1 A2 : lty (S (S ϕ))) (i : idx (S ϕ)) :
  lty_strip A1 = lty_strip A2 ->
  subst_lty_beta_one 1 i (lty_if f A1 A2) =
  lty_if (fun xs => f (hd xs ::: i (tl xs) ::: tl (tl xs)))
         (subst_lty_beta_one 1 i A1) (subst_lty_beta_one 1 i A2).
Proof.
  intros H.
  unshelve erewrite !subst_lty_beta_one_correct. 1-3: reflexivity.
  rewrite lty_if_subst_var_beta by auto.
  rewrite lty_cast_id.
  unfold subst_lty_var_beta.
  erewrite !subst_var_beta_fun_FS.
  rewrite !subst_var_beta_fun_Fin0.
  f_equal.
  - fext; intros xs. cbn. unfold subst_beta_fun. f_equal. f_equal. f_equal.
    + now rewrite vect_cast_id.
    + f_equal.
      * f_equal. now simp_vect_to_list.
      * simp_vect_to_list. now rewrite !skipn_tl, skipn_O.
  - rewrite lty_cast_id. reflexivity.
  - rewrite lty_cast_id. reflexivity.
  Unshelve.
  all: abstract lia.
Qed.


Lemma lty_if_subst_beta_ground {ϕ} (f : Vector.t nat (S ϕ) -> bool) (A1 A2 : lty (S ϕ)) (i : idx ϕ) :
  lty_strip A1 = lty_strip A2 ->
  subst_lty_beta_ground (lty_if f A1 A2) i =
  lty_if (fun xs => f (i xs ::: xs)) (subst_lty_beta_ground A1 i) (subst_lty_beta_ground A2 i).
Proof.
  intros H.
  rewrite !subst_lty_beta_ground_correct.
  rewrite lty_if_subst_var_beta by auto.
  f_equal. rewrite subst_var_beta_fun_Fin0. reflexivity.
Qed.



Lemma tyT_cases {ϕ} (f : Vector.t nat ϕ -> bool) (Φ1 Φ2 : constr ϕ) (Γ1 Γ2 : ctx ϕ) (M1 M2 : idx ϕ)
      (t : tm) (ρ1 ρ2 : mty ϕ) (s : skel) :
  (Ty! Φ1; Γ1 ⊢(M1) t : ρ1 @ s) ->
  (Ty! Φ2; Γ2 ⊢(M2) t : ρ2 @ s) ->
  (forall x, free x t -> mty_strip (Γ1 x) = mty_strip (Γ2 x)) ->
  mty_strip ρ1 = mty_strip ρ2 ->
  Ty! (fun xs => if f xs then Φ1 xs else Φ2 xs); (ctx_if f Γ1 Γ2) ⊢(fun xs => if f xs then M1 xs else M2 xs) t : mty_if f ρ1 ρ2 @ s.
Proof.
  intros ty1 ty2 HstripΓ Hstripρ. induction ty1 in f,Φ2,Γ2,M2,ρ2,ty2,HstripΓ,Hstripρ|-*.
  - (* Var *) inv ty2. econstructor.
    unfold ctx_if. eapply eqmty_if_cases; eauto. now apply HstripΓ.
    { hnf; intros xs Hxs. destruct f; auto. }

  - (* Lam *) inv ty2.
    assert (forall x, free x (Lam t) -> mty_strip (Γ' x) = mty_strip (Γ'0 x)) as HΓ'_strip.
    { intros. eapply eqCtx_strip in HΓ; eauto. eapply eqCtx_strip in HΓ0; eauto.
      eapply ctxBSum_strip in Hbsum; eauto. eapply ctxBSum_strip in Hbsum0; eauto. unfold stripCtx in *.
      rewrite <- HΓ, <- HΓ0. now rewrite HstripΓ. }

    assert (forall x, free x (Lam t) -> mty_strip (Δ x) = mty_strip (Δ0 x)) as HΔ_strip.
    { intros. eapply eqCtx_strip in HΓ; eauto. eapply eqCtx_strip in HΓ0; eauto.
      eapply ctxBSum_strip in Hbsum; eauto. eapply ctxBSum_strip in Hbsum0; eauto. unfold stripCtx in *.
      rewrite Hbsum, Hbsum0. rewrite <- HΓ, <- HΓ0. now rewrite HstripΓ. }

    specialize IHty1 with (1 := Hty) (f := fun xs => f (tl xs)); spec_assert IHty1.
    { intros [ | x] Hx; cbn; eauto.
      - apply eqmty_strip in Hρ; cbn in Hρ. apply eqmty_strip in Hρ0; cbn in Hρ0. congruence.
    }
    spec_assert IHty1.
    { apply eqmty_strip in Hρ; cbn in Hρ. apply eqmty_strip in Hρ0; cbn in Hρ0. congruence. }
    rewrite ctx_if_scons in IHty1.

    eapply tyT_Lam with (I1 := fun xs => if f xs then I xs else I0 xs).
    + eapply tyT_weaken_Φ. apply IHty1.
      hnf; intros xs (Hxs1&Hxs2). destruct (f (tl xs)); auto.
    + eapply ctxBSum_if; eauto.
    + apply eqCtx_if_cases; cbn; eauto.
    + cbn; hnf. intros xs Hxs. destruct (f xs); auto.
    + etransitivity.
      2:{ apply eqmty_if_cases; eauto. cbn. apply eqmty_strip in Hρ; cbn in Hρ. apply eqmty_strip in Hρ0; cbn in Hρ0. congruence. }
      reflexivity.

  - (* Fix *) inv ty2.
    assert (forall x, free x (Fix t) -> mty_strip (Γ' x) = mty_strip (Γ'0 x)) as HΓ'_strip.
    { intros. eapply eqCtx_strip in HΓ; eauto. eapply eqCtx_strip in HΓ0; eauto.
      eapply ctxBSum_strip in Hbsum; eauto. eapply ctxBSum_strip in Hbsum0; eauto. unfold stripCtx in *.
      rewrite <- HΓ, <- HΓ0. now rewrite HstripΓ. }

    assert (forall x, free x (Fix t) -> mty_strip (Δ x) = mty_strip (Δ0 x)) as HΔ_strip.
    { intros. eapply eqCtx_strip in HΓ; eauto. eapply eqCtx_strip in HΓ0; eauto.
      eapply ctxBSum_strip in Hbsum; eauto. eapply ctxBSum_strip in Hbsum0; eauto. unfold stripCtx in *.
      rewrite Hbsum, Hbsum0. rewrite <- HΓ, <- HΓ0. now rewrite HstripΓ. }

    assert (lty_strip A = lty_strip A0 /\ lty_strip B = lty_strip B0) as (HA_strip&HB_strip).
    { apply eqmty_strip in Hρ; cbn in Hρ. apply eqmty_strip in Hρ0; cbn in Hρ0.
      eapply eqlty_strip in Hsub. eapply eqlty_strip in Hsub0.
      setoid_rewrite subst_lty_strip in Hsub. setoid_rewrite subst_lty_strip in Hsub0.
      setoid_rewrite subst_lty_strip in Hρ. setoid_rewrite subst_lty_strip in Hρ0. split;congruence. }

    specialize IHty1 with (1 := Hty) (f := fun xs => f (tl xs)); spec_assert IHty1.
    { cbn. intros [ | x] Hx; cbn; eauto. }
    spec_assert IHty1.
    { cbn. apply eqmty_strip in Hρ; cbn in Hρ. apply eqmty_strip in Hρ0; cbn in Hρ0. congruence. }
    rewrite ctx_if_scons in IHty1. cbn [mty_if] in IHty1. rewrite fext_lt_iConst in IHty1.

    eapply tyT_Fix with
        (cardH1 := fun xs => if f xs then cardH xs else cardH0 xs)
        (K1 := fun xs => if f xs then K xs else K0 xs)
        (card4 := fun xs => if f (tl (tl xs)) then card1 xs else card0 xs)
        (card5 := fun xs => if f (tl xs) then card2 xs else card3 xs).
    + eapply tyT_weaken_Φ. apply IHty1.
      hnf; intros xs (Hxs1&Hxs2). destruct (f (tl xs)); auto.
    + cbn. etransitivity.
      2:{ eapply eqlty_mono_Φ. eapply eqlty_if_cases; eauto.
          - now rewrite !subst_lty_strip.
          - cbn. hnf; intros xs (Hxs1&Hxs2&Hxs3). destruct f; cbn; auto. }
      cbn.
      rewrite !subst_lty_beta_Fix1_correct.
      rewrite !lty_if_subst_beta_one_two. rewrite !lty_if_subst_beta_ground.
      cbn -[Nat.ltb]. repeat setoid_rewrite subst_lty_twice; unfold funcomp; cbn -[Nat.ltb].
      apply eqlty_if_cases_f.
      { hnf; reflexivity. }
      { apply eqlty_subst_idx_sem.
        hnf; intros xs (Hxs1&Hxs2&Hxs3).
        intros j; unfold subst_beta_ground_fun, subst_beta_one_two_fun; f_equal; clear j.
        cbn. now rewrite Hxs1. }
      { apply eqlty_subst_idx_sem.
        hnf; intros xs (Hxs1&Hxs2&Hxs3).
        intros j; unfold subst_beta_ground_fun, subst_beta_one_two_fun; f_equal; clear j.
        cbn. now rewrite Hxs1. }
      all: now repeat setoid_rewrite subst_lty_strip.
    + hnf; intros xs Hxs; cbn in Hxs|-*. destruct f; auto.
    + hnf; intros xs Hxs; cbn in Hxs|-*. destruct f; auto.
    + hnf; intros xs Hxs; cbn in Hxs|-*. destruct f; auto.
    + apply ctxBSum_if; eauto.
    + now apply eqCtx_if_cases.
    + hnf; intros xs Hxs; cbn in Hxs|-*. destruct f; auto.
    + rewrite !subst_lty_beta_Fix2_correct.
      rewrite !lty_if_subst_beta_one_one. rewrite !lty_if_subst_beta_ground.
      cbn -[Nat.ltb]. repeat setoid_rewrite subst_lty_twice; unfold funcomp; cbn -[Nat.ltb].
      rewrite <- mty_if_eq_Quant.
      apply eqmty_if_cases_f.
      { hnf; reflexivity. }
      {
        etransitivity.
        2:{ eapply eqmty_mono_Φ. apply Hρ. hnf; intros xs (Hxs1&Hxs2). now rewrite Hxs1 in Hxs2. }
        apply eqmty_Quant. 2: hnf; reflexivity.
        apply eqlty_subst_idx_sem.
        hnf; intros xs (Hxs1&Hxs2&Hxs3).
        cbn. unfold subst_beta_ground_fun, subst_beta_one_fun. cbn. unfold iConst.
        intros j; f_equal; clear j.
        f_equal. f_equal. now rewrite Hxs2.
      }
      {
        etransitivity.
        2:{ eapply eqmty_mono_Φ. apply Hρ0. hnf; intros xs (Hxs1&Hxs2). now rewrite Hxs1 in Hxs2. }
        apply eqmty_Quant. 2: hnf; reflexivity.
        apply eqlty_subst_idx_sem.
        cbn. unfold subst_beta_ground_fun, subst_beta_one_fun. cbn. unfold iConst.
        hnf; intros xs (Hxs1&Hxs2&Hxs3).
        intros j; f_equal; clear j.
        f_equal. f_equal. now rewrite Hxs2.
      }
      all: now repeat setoid_rewrite subst_lty_strip.

  - (* App *) inv ty2.
    assert (forall x, free x (App t1 t2) -> mty_strip (Γ' x) = mty_strip (Γ'0 x)) as HΓ'_strip.
    { intros. eapply eqCtx_strip in HΓ; eauto. eapply eqCtx_strip in HΓ0; eauto.
      eapply ctxMSum_strip in Hmsum; eauto. eapply ctxMSum_strip in Hmsum0; eauto. unfold stripCtx in *.
      rewrite <- HΓ, <- HΓ0. now rewrite HstripΓ. }
    assert (forall x : nat, free x (App t1 t2) -> mty_strip (Δ1 x) = mty_strip (Δ0 x)) as HΔ_strip.
    { intros x Hx.
      eapply ctxMSum_strip in Hmsum as (Hmsum&Hmsum'); cbn; eauto; eapply ctxMSum_strip in Hmsum0 as (Hmsum0&Hmsum0'); cbn; eauto.
      eapply eqCtx_strip with (x0 := x) in HΓ; cbn; eauto. eapply eqCtx_strip with (x0 := x) in HΓ0; cbn; eauto.
      specialize (HstripΓ x ltac:(cbn;eauto)). unfold stripCtx in *. congruence. }

    assert (mty_strip τ = mty_strip τ0) as Hτ_strip.
    { apply eqmty_strip in Hρ; apply eqmty_strip in Hρ0.
      setoid_rewrite subst_mty_strip in Hρ. setoid_rewrite subst_mty_strip in Hρ0. congruence. }

    specialize IHty1_1 with (1 := Hty1) (f := f); spec_assert IHty1_1.
    { intros x Hx. rewrite HΔ_strip; cbn; auto. }
    spec_assert IHty1_1.
    { cbn. f_equal; eauto. }

    specialize IHty1_2 with (1 := Hty2) (f := f); spec_assert IHty1_2.
    { intros x Hx.
      eapply ctxMSum_strip in Hmsum as (Hmsum&Hmsum'); cbn; eauto; eapply ctxMSum_strip in Hmsum0 as (Hmsum0&Hmsum0'); cbn; eauto.
      eapply eqCtx_strip with (x0 := x) in HΓ; cbn; eauto. eapply eqCtx_strip with (x0 := x) in HΓ0; cbn; eauto.
      specialize (HstripΓ x ltac:(cbn;eauto)). unfold stripCtx in *. congruence. }
    spec_assert IHty1_2.
    { now setoid_rewrite subst_mty_strip. }

    epose proof eq_refl ?[σ0'] as Hσ0'; replace (mty_strip σ0) with ?σ0'; clear Hσ0'.
    eapply tyT_App with (σ1 := mty_if (fun xs => f (tl xs)) σ σ0).
    + eapply tyT_weaken_Φ. eapply tyT_sub'. apply IHty1_1.
      * cbn. apply eqmty_Quant.
        -- reflexivity.
        -- hnf; intros xs Hxs. destruct (f xs); reflexivity.
      * instantiate (1 := fun xs => _); cbn. hnf; reflexivity.
      * exact (fun xs Hxs => Hxs).
    + eapply tyT_weaken_Φ. eapply tyT_sub'. apply IHty1_2.
      * rewrite mty_if_beta_ground_iConst0 by auto. reflexivity.
      * instantiate (1 := fun xs => _); cbn; hnf; reflexivity.
      * exact (fun xs Hxs => Hxs).
    + eapply ctxMSum_if; eauto.
    + eapply eqCtx_if_cases; eauto.
    + hnf; intros xs Hxs. destruct (f xs); auto.
    + rewrite <- mty_if_beta_ground_iConst0 by auto.
      eapply eqmty_if_cases; eauto. now setoid_rewrite subst_mty_strip.
    + now rewrite mty_if_strip.

  - (* Ifz *) inv ty2.
    assert (forall x, free x (Ifz t1 t2 t3) -> mty_strip (Γ' x) = mty_strip (Γ'0 x)) as HΓ'_strip.
    { intros. eapply eqCtx_strip in HΓ; eauto. eapply eqCtx_strip in HΓ0; eauto.
      eapply ctxMSum_strip in Hmsum; eauto. eapply ctxMSum_strip in Hmsum0; eauto. unfold stripCtx in *.
      rewrite <- HΓ, <- HΓ0. now rewrite HstripΓ. }
    assert (forall x : nat, free x (Ifz t1 t2 t3) -> mty_strip (Δ1 x) = mty_strip (Δ0 x)) as HΔ_strip.
    { intros x Hx.
      eapply ctxMSum_strip in Hmsum as (Hmsum&Hmsum'); cbn; eauto; eapply ctxMSum_strip in Hmsum0 as (Hmsum0&Hmsum0'); cbn; eauto.
      eapply eqCtx_strip with (x0 := x) in HΓ; cbn; eauto. eapply eqCtx_strip with (x0 := x) in HΓ0; cbn; eauto.
      specialize (HstripΓ x ltac:(cbn;eauto)). unfold stripCtx in *. congruence. }

    specialize IHty1_1 with (1 := Hty1) (f := f); spec_assert IHty1_1.
    { intros x Hx. rewrite HΔ_strip; cbn; auto. }
    spec_assert IHty1_1.
    { cbn. f_equal; eauto. }

    specialize IHty1_2 with (1 := Hty2) (f := f); spec_assert IHty1_2.
    { intros x Hx.
      eapply ctxMSum_strip in Hmsum as (Hmsum&Hmsum'); cbn; eauto; eapply ctxMSum_strip in Hmsum0 as (Hmsum0&Hmsum0'); cbn; eauto.
      eapply eqCtx_strip with (x0 := x) in HΓ; cbn; eauto. eapply eqCtx_strip with (x0 := x) in HΓ0; cbn; eauto.
      specialize (HstripΓ x ltac:(cbn;eauto)). unfold stripCtx in *. congruence. }
    spec_assert IHty1_2 by eauto.

    specialize IHty1_3 with (1 := Hty3) (f := f); spec_assert IHty1_3.
    { intros x Hx.
      eapply ctxMSum_strip in Hmsum as (Hmsum&Hmsum'); cbn; eauto; eapply ctxMSum_strip in Hmsum0 as (Hmsum0&Hmsum0'); cbn; eauto.
      eapply eqCtx_strip with (x0 := x) in HΓ; cbn; eauto. eapply eqCtx_strip with (x0 := x) in HΓ0; cbn; eauto.
      specialize (HstripΓ x ltac:(cbn;eauto)). unfold stripCtx in *. congruence. }
    spec_assert IHty1_3 by eauto.

    eapply tyT_Ifz.
    + apply IHty1_1.
    + eapply tyT_weaken_Φ. apply IHty1_2. hnf; intros xs Hxs. now destruct f.
    + eapply tyT_weaken_Φ. apply IHty1_3. hnf; intros xs Hxs. now destruct f.
    + eapply ctxMSum_if; eauto.
    + eapply eqCtx_if_cases; eauto.
    + hnf; intros xs Hxs. destruct (f xs); auto.

  - (* Const *) inv ty2. apply tyT_Const.
    erewrite <- mty_if_eq_Nat'.
    apply eqmty_if_cases. apply Hty. apply Hty0. all: auto.
    { hnf; intros xs Hxs. destruct f; auto. }

  - (* Succ *) inv ty2. specialize IHty1 with (f := f) (1 := Hty); (do 2 spec_assert IHty1 by eauto). cbn in IHty1.
    eapply tyT_Succ.
    + apply IHty1.
    + transitivity (Nat (fun xs : Vector.t nat ϕ => if f xs then S (K xs) else S (K0 xs))).
      { apply eqmty_Nat. hnf; unfold funcomp; cbn; intros xs Hxs. now destruct (f xs). }
      erewrite <- mty_if_eq_Nat.
      apply eqmty_if_cases; eauto.
  - (* Pred *) inv ty2. specialize IHty1 with (f := f) (1 := Hty); (do 2 spec_assert IHty1 by eauto). cbn in IHty1.
    eapply tyT_Pred.
    + apply IHty1.
    + transitivity (Nat (fun xs : Vector.t nat ϕ => if f xs then pred (K xs) else pred (K0 xs))).
      { apply eqmty_Nat. hnf; unfold funcomp; cbn; intros xs Hxs. now destruct (f xs). }
      erewrite <- mty_if_eq_Nat.
      apply eqmty_if_cases; eauto.
Qed.



(** *** Joining lemma *)

Import SigmaTypeNotations.


(** Two typings for [Const k] are always equivalent. *)
Lemma tyT_Const_inv2 {ϕ} (Φ : constr ϕ) (Γ1 Γ2 : ctx ϕ) (K1 K2 : idx ϕ) (k : nat) (τ1 τ2 : mty ϕ) (s1 s2 : skel) :
  (Ty! Φ; Γ1 ⊢(K1) Const k : τ1 @ s1) ->
  (Ty! Φ; Γ2 ⊢(K2) Const k : τ2 @ s2) ->
  { I : idx ϕ | τ1 = Nat I /\ (mty! Φ ⊢ τ1 ≡ τ2) }.
Proof.
  intros Hty1 Hty2. inv Hty1; inv Hty2.
  apply eqmty_Nat_inv'_sig in Hty as (I1&->&Hty1). apply eqmty_Nat_inv'_sig in Hty0 as (I2&->&Hty2). unfold iConst in Hty1, Hty2.
  eexists. split. reflexivity. eapply eqmty_Nat. hnf in Hty1,Hty2|-*. firstorder.
Qed.


Lemma joining_Const {ϕ} (Φ : constr ϕ) (Γ1 Γ2 : ctx ϕ) (N1 N2 : idx ϕ) (k : nat) (ρ1 ρ2 : mty ϕ) (s : skel) :
  (Ty! Φ; Γ1 ⊢(N1) Const k : ρ1 @ s) ->
  (Ty! Φ; Γ2 ⊢(N2) Const k : ρ2 @ s) ->
  existsS (ρ : mty ϕ) (ρ1' ρ2' : mty ϕ) (Γ1' Γ2' Γ12 : ctx ϕ),
    (mty! Φ ⊢ ρ1' ≡ ρ1) **
    (mty! Φ ⊢ ρ2' ≡ ρ2) **
    (msum ρ1' ρ2' ρ) **
    (ctx! Const k; Φ ⊢ Γ1' ≡ Γ1) **
    (ctx! Const k; Φ ⊢ Γ2' ≡ Γ2) **
    (ctxMSum (Const k) Γ1' Γ2' Γ12) **
    (Ty! Φ; Γ12 ⊢(fun xs => N1 xs + N2 xs) Const k : ρ @ s).
Proof.
  intros ty1 ty2.
  inv ty1; inv ty2.
  apply eqmty_Nat_inv'_sig in Hty as (k1&->&Hty1); apply eqmty_Nat_inv'_sig in Hty0 as (k2&->&Hty2).
  exists (Nat (iConst k)), (Nat (iConst k)), (Nat (iConst k)), Γ1, def_ctx, def_ctx. repeat_split.
  - apply eqmty_Nat. firstorder.
  - apply eqmty_Nat. firstorder.
  - apply msum_Nat'.
  - hnf; intros x Hx; exfalso. contradict Hx.
  - hnf; intros x Hx; exfalso. contradict Hx.
  - hnf; intros x Hx; exfalso. contradict Hx.
  - apply tyT_Const.
    + apply eqmty_Nat. firstorder.
    + hnf; intros xs Hxs. now rewrite <- HM, <- HM0.
Qed.


(* TODO: Move *)
Lemma fext (X Y : Type) (f1 f2 : X -> Y) :
  (forall x, f1 x = f2 x) -> f1 = f2.
Proof. intros. now fext. Qed.

(* TODO: Move *)
Lemma fext_inv (X Y : Type) (f1 f2 : X -> Y) :
  f1 = f2 -> (forall x, f1 x = f2 x).
Proof. intros ->. reflexivity. Qed.


Definition shift_var_sub_fun' {X : Type} {ϕ} (x : Fin.t (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) :
  (Vector.t nat (S ϕ) -> X) -> (Vector.t nat (S ϕ) -> X).
Proof.
  refine (subst_var_beta_fun x 1 _).
  cbn. refine (fun xs => hd xs - i (tl xs)).
Defined.

Lemma shift_sub_fun_correct {X : Type} {ϕ} (i : idx ϕ) :
  @shift_sub_fun X ϕ i = shift_var_sub_fun' Fin0 i.
Proof.
  unfold shift_sub_fun, shift_var_sub_fun'.
  erewrite subst_var_beta_fun_Fin0; cbn. unfold subst_beta_fun, idx_cast; cbn.
  fext; intros j xs. reflexivity.
Qed.

Lemma lty_shift_sub_correct' {ϕ} (A : lty (S ϕ)) (I : idx ϕ) :
  lty_shift_sub A I = subst_lty_var_beta Fin0 1 (fun xs =>  hd xs - I (tl xs)) A.
Proof. unfold lty_shift_sub, subst_lty_var_beta. f_equal. erewrite shift_sub_fun_correct; eauto. Qed.

Lemma lty_if_shift_sub {ϕ} (f : Vector.t nat ϕ -> bool) (A1 A2 : lty (S ϕ)) (i1 i2 : idx ϕ) :
  lty_strip A1 = lty_strip A2 ->
  lty_shift_sub (lty_if (fun xs => f (tl xs)) A1 A2) (fun xs => if f xs then i1 xs else i2 xs) =
  lty_if (fun xs => f (tl xs)) (lty_shift_sub A1 i1) (lty_shift_sub A2 i2).
Proof.
  intros H.
  setoid_rewrite lty_shift_sub_correct'. cbn.
  replace (fun xs : Vector.t nat (S ϕ) => f (tl xs)) with (fun xs => f (tam Fin0 xs)).
  2:{ fext; intros. f_equal. unfold tam; cbn. now simp_vect_to_list. }
  replace (fun xs : Vector.t nat (S ϕ) => hd xs - (if f (tl xs) then i1 (tl xs) else i2 (tl xs)))
    with (fun xs : Vector.t nat (S ϕ) => if f (tl xs) then hd xs - i1 (tl xs) else hd xs - i2 (tl xs)).
  2:{ fext; intros xs. now destruct f. }
  erewrite lty_if_subst_var_beta2 with (x := Fin0) (y := 1) (i3 := fun xs => hd xs - i1 (tl xs)) (i4 := fun xs => hd xs - i2 (tl xs)); auto.
  f_equal. fext; intros xs. f_equal. now unfold tam, tamtam; simp_vect_to_list.
Qed.


Definition ctx_shift_sub {ϕ} (Γ : ctx (S ϕ)) (i : idx ϕ) : ctx (S ϕ) := fun var => mty_shift_sub (Γ var) i.

Lemma ctxBSum_join {ϕ} (t : tm) (I1 I2 : idx ϕ) (Δ1 Δ2 : ctx (S ϕ)) (Γ1 Γ2 : ctx ϕ) :
  ctxBSum t I1 Δ1 Γ1 ->
  ctxBSum t I2 Δ2 Γ2 ->
  (forall x, free x t -> mty_comp ( fun xs => hd xs < I1 (tl xs) + I2 (tl xs)) (Δ1 x) (Δ2 x)) ->
  existsS (Δ : ctx (S ϕ)) (Γ : ctx ϕ),
    (ctx! t; fun xs => hd xs < I1 (tl xs) + I2 (tl xs) ⊢ (ctx_if (fun xs => hd xs <? I1 (tl xs)) Δ1 (ctx_shift_sub Δ2 I1)) ≡ Δ) **
    ctxBSum t (fun xs => I1 xs + I2 xs) Δ Γ.
Proof.
  intros H1 H2 H3.
  exists (fun x =>
       match free_dec x t with
       | left Hx => projT1 (bsum_join (H1 x Hx) (H2 x Hx) (H3 x Hx))
       | right _ => Nat (iConst 42)
       end).
  exists (fun x =>
       match free_dec x t with
       | left Hx => projT1 (projT2 (bsum_join (H1 x Hx) (H2 x Hx) (H3 x Hx)))
       | right _ => Nat (iConst 42)
       end).
  split.
  - hnf; intros x Hx. destruct (free_dec x t) as [Hx'|Hx']; try tauto.
    destruct bsum_join as (?&?&?&?); cbn -[Nat.ltb]; auto.
  - hnf; intros x Hx. destruct (free_dec x t) as [Hx'|Hx']; try tauto.
    unfold ctx_if, ctx_shift_sub.
    destruct bsum_join as (?&?&?&?); cbn -[Nat.ltb]; auto.
Qed.


Fact subty_msum_doesn't_commute {ϕ} :
  ~ forall (Φ : constr ϕ) σ1 σ2 τ1 τ2,
      mty_comp Φ σ1 σ2 ->
      mty_comp Φ τ1 τ2 ->
      (mty! Φ ⊢ σ1 ⊑ τ1) ->
      (mty! Φ ⊢ σ2 ⊑ τ2) ->
      (mty! Φ ⊢ snd (snd (make_msum σ1 σ2)) ⊑ snd (snd (make_msum τ1 τ2))).
Proof.
  intros H.
  specialize H with
      (Φ := fun _ => True)
      (σ1 := [ < iConst 2] ⋅ Nat hd ⊸ Nat hd)
      (σ2 := [ < iConst 1] ⋅ Nat hd ⊸ Nat hd)
      (τ1 := [ < iConst 1] ⋅ Nat hd ⊸ Nat hd)
      (τ2 := [ < iConst 1] ⋅ Nat hd ⊸ Nat hd).
  cbn in H. change (iConst ?i _) with i in *.
  do 2 spec_assert H by reflexivity.
  spec_assert H.
  { apply submty_Quant.
    - apply eqlty_Arr; reflexivity.
    - unfold iConst. hnf; lia. }
  spec_assert H.
  { reflexivity. }
  apply submty_Quant_inv in H as (H&_).
  apply sublty_arr_inv in H as (H1&H2).
  apply submty_Nat_inv in H1; apply submty_Nat_inv in H2. hnf in H1,H2.
  specialize (H1 (1 ::: Vector.const 0 ϕ)); cbn in H1.
  spec_assert H1 by (split;auto;lia). discriminate.
Qed.

Lemma sublty_cases_shift {ϕ} (Φ : constr ϕ) (A B : lty (S ϕ)) (i1 i2 : idx ϕ) :
  (lty! fun xs => hd xs < i1 (tl xs) /\ Φ (tl xs) ⊢ A ⊑ B) ->
  (lty! fun xs => hd xs < i2 (tl xs) /\ Φ (tl xs) ⊢ lty_shift_add A i1 ⊑ lty_shift_add B i1) ->
  (lty! fun xs => hd xs < i1 (tl xs) + i2 (tl xs) /\ Φ (tl xs) ⊢ A ⊑ B).
Proof.
  intros H1 H2.
  eapply sublty_shift_sub with (I := i1) in H2; unfold shift_sub_fun in H2; cbn in H2.
  eapply sublty_mono_Φ.
  eapply sublty_cases with
      (f := fun xs => hd xs <? i1 (tl xs))
      (Φ1 := fun xs => hd xs < i1 (tl xs) /\ Φ (tl xs))
      (Φ2 := fun xs => hd xs >= i1 (tl xs) /\ hd xs < i1 (tl xs) + i2 (tl xs) /\ Φ (tl xs)).
  - eapply sublty_mono_Φ; eauto. hnf; intros xs (Hxs1&Hxs2); split; auto.
  - transitivity (lty_shift_sub (lty_shift_add A i1) i1).
    { rewrite <- subst_lty_id at 1.
      setoid_rewrite subst_lty_twice; unfold funcomp, lty_shift_sub, shift_add_fun, shift_sub_fun, id; cbn -[Nat.ltb].
      eapply sublty_subst_idx_sem. hnf; intros xs (Hxs1&Hxs2). intros j; f_equal; clear j.
      replace (i1 (tl xs) + (hd xs - i1 (tl xs))) with (hd xs). now rewrite <- eta. lia. }
    etransitivity.
    { eapply sublty_mono_Φ. apply H2. hnf; intros xs (Hxs1&Hxs2&Hxs3). split; auto. lia. }
    {
      setoid_rewrite <- subst_lty_id at 4.
      setoid_rewrite subst_lty_twice; unfold funcomp, lty_shift_sub, shift_add_fun, shift_sub_fun, id; cbn -[Nat.ltb].
      eapply sublty_subst_idx_sem. hnf; intros xs (Hxs1&Hxs2). intros j; f_equal; clear j.
      replace (i1 (tl xs) + (hd xs - i1 (tl xs))) with (hd xs). now rewrite <- eta. lia. }
  - hnf; intros xs (Hxs1&Hxs2). destruct (hd xs <? i1 (tl xs)) eqn:E; split; auto.
    + now apply Nat.ltb_lt in E.
    + now apply Nat.ltb_ge in E.
Qed.

(* Analogously *)
Lemma submty_cases_shift {ϕ} (Φ : constr ϕ) (A B : mty (S ϕ)) (i1 i2 : idx ϕ) :
  (mty! fun xs => hd xs < i1 (tl xs) /\ Φ (tl xs) ⊢ A ⊑ B) ->
  (mty! fun xs => hd xs < i2 (tl xs) /\ Φ (tl xs) ⊢ mty_shift_add A i1 ⊑ mty_shift_add B i1) ->
  (mty! fun xs => hd xs < i1 (tl xs) + i2 (tl xs) /\ Φ (tl xs) ⊢ A ⊑ B).
Proof.
  intros H1 H2.
  eapply submty_shift_sub with (I := i1) in H2; unfold shift_sub_fun in H2; cbn in H2.
  eapply submty_mono_Φ.
  eapply submty_cases with
      (f := fun xs => hd xs <? i1 (tl xs))
      (Φ1 := fun xs => hd xs < i1 (tl xs) /\ Φ (tl xs))
      (Φ2 := fun xs => hd xs >= i1 (tl xs) /\ hd xs < i1 (tl xs) + i2 (tl xs) /\ Φ (tl xs)).
  - eapply submty_mono_Φ; eauto. hnf; intros xs (Hxs1&Hxs2); split; auto.
  - transitivity (mty_shift_sub (mty_shift_add A i1) i1).
    { rewrite <- subst_mty_id at 1.
      setoid_rewrite subst_mty_twice; unfold funcomp, mty_shift_sub, shift_add_fun, shift_sub_fun, id; cbn -[Nat.ltb].
      eapply submty_subst_idx_sem. hnf; intros xs (Hxs1&Hxs2). intros j; f_equal; clear j.
      replace (i1 (tl xs) + (hd xs - i1 (tl xs))) with (hd xs). now rewrite <- eta. lia. }
    etransitivity.
    { eapply submty_mono_Φ. apply H2. hnf; intros xs (Hxs1&Hxs2&Hxs3). split; auto. lia. }
    {
      setoid_rewrite <- subst_mty_id at 4.
      setoid_rewrite subst_mty_twice; unfold funcomp, mty_shift_sub, shift_add_fun, shift_sub_fun, id; cbn -[Nat.ltb].
      eapply submty_subst_idx_sem. hnf; intros xs (Hxs1&Hxs2). intros j; f_equal; clear j.
      replace (i1 (tl xs) + (hd xs - i1 (tl xs))) with (hd xs). now rewrite <- eta. lia. }
  - hnf; intros xs (Hxs1&Hxs2). destruct (hd xs <? i1 (tl xs)) eqn:E; split; auto.
    + now apply Nat.ltb_lt in E.
    + now apply Nat.ltb_ge in E.
Qed.

Lemma eqlty_cases_shift {ϕ} (Φ : constr ϕ) (A B : lty (S ϕ)) (i1 i2 : idx ϕ) :
  (lty! fun xs => hd xs < i1 (tl xs) /\ Φ (tl xs) ⊢ A ≡ B) ->
  (lty! fun xs => hd xs < i2 (tl xs) /\ Φ (tl xs) ⊢ lty_shift_add A i1 ≡ lty_shift_add B i1) ->
  (lty! fun xs => hd xs < i1 (tl xs) + i2 (tl xs) /\ Φ (tl xs) ⊢ A ≡ B).
Proof. intros [H1 H2] [H3 H4]; split; eapply sublty_cases_shift; eauto. Qed.


Lemma eqmty_msum_commutes {ϕ} :
  forall (Φ : constr ϕ) σ1 σ2 τ1 τ2 ρ1 ρ2,
    msum σ1 σ2 ρ1 ->
    msum τ1 τ2 ρ2 ->
    (mty! Φ ⊢ σ1 ≡ τ1) ->
    (mty! Φ ⊢ σ2 ≡ τ2) ->
    (mty! Φ ⊢ ρ1 ≡ ρ2).
Proof.
  intros * Hsum1 Hsum2 Heq1 Heq2.
  destruct σ1 eqn:E1, σ2 eqn:E2, ρ1 eqn:E3; cbn in *; try tauto.
  - subst.
    apply eqmty_Nat_inv' in Heq1 as (k' & -> & Heq1); apply eqmty_Nat_inv' in Heq2 as (j' & -> & Heq2).
    cbn in Hsum2. destruct ρ2; try tauto.
    hnf in Hsum1,Hsum2,Heq1,Heq2. apply eqmty_Nat. firstorder congruence.
  - destruct Hsum1 as (-> & -> & Hsum1).
    replace i1 with (fun xs => i xs + i0 xs) in * by (fext; auto); clear i1 Hsum1.
    apply eqmty_Quant_inv' in Heq1 as (j & B & -> & Heq1 & Heq1').
    apply eqmty_Quant_inv' in Heq2 as (k & C & -> & Heq2 & Heq2').
    destruct ρ2 as [? | m D]; cbn in Hsum2; try tauto. destruct Hsum2 as (-> & -> & Hsum2).
    replace m with (fun xs => j xs + k xs) in * by (fext; auto); clear m Hsum2.
    apply eqmty_Quant.
    + apply eqlty_cases_shift; eauto.
      etransitivity; eauto.
      eapply eqlty_subst_idx_sem.
      hnf; intros xs (Hxs&Hxs2). cbn in Hxs2. unfold shift_add_fun. intros jjjj; f_equal; clear jjjj.
      f_equal. f_equal. firstorder.
    + hnf; intros xs Hxs. hnf in Heq1', Heq2'. rewrite Heq1', Heq2' by auto. reflexivity.
Qed.


Lemma make_msum_eqmty_commutes {ϕ} :
  forall (Φ : constr ϕ) σ1 σ2 τ1 τ2,
    mty_comp Φ σ1 σ2 ->
    mty_comp Φ τ1 τ2 ->
    (mty! Φ ⊢ σ1 ≡ τ1) ->
    (mty! Φ ⊢ σ2 ≡ τ2) ->
    (mty! Φ ⊢ snd (snd (make_msum σ1 σ2)) ≡ snd (snd (make_msum τ1 τ2))).
Proof.
  intros * Hcomp1 Hcomp2 H1 H2.
  pose proof make_msum_correct _ _ _ Hcomp1 as (L1&L2&L3).
  pose proof make_msum_correct _ _ _ Hcomp2 as (L4&L5&L6).
  eapply eqmty_msum_commutes; eauto.
  - transitivity σ1.
    { eapply eqmty_mono_Φ; eauto. firstorder. }
    transitivity τ1; auto.
    symmetry. eapply eqmty_mono_Φ; eauto. firstorder.
  - transitivity σ2.
    { eapply eqmty_mono_Φ; eauto. firstorder. }
    transitivity τ2; auto.
    symmetry. eapply eqmty_mono_Φ; eauto. firstorder.
Qed.



(* TODO: Move *)
Lemma mty_shift_add_sub {ϕ} (A : mty (S ϕ)) (i : idx ϕ) :
  mty_shift_add (mty_shift_sub A i) i = A.
Proof.
  setoid_rewrite subst_mty_twice. unfold shift_sub_fun, shift_add_fun, funcomp.
  rewrite <- subst_mty_id. f_equal; fext; intros j' xs. unfold id; cbn.
  f_equal. rewrite eta. f_equal. nia.
Qed.


(* Similar to [eqmty_cases_shift] *)

Lemma tyT_cases_shift {ϕ} (i1 i2 : idx ϕ) (Φ : constr ϕ) (Γ : ctx (S ϕ)) (M : idx (S ϕ)) (t : tm) (ρ : mty (S ϕ)) (s : skel) :
  (Ty! fun xs => hd xs < i1 (tl xs) /\ Φ (tl xs); Γ ⊢(M) t : ρ @ s) ->
  (Ty! fun xs => hd xs < i2 (tl xs) /\ Φ (tl xs); ctx_shift_add Γ i1 ⊢(fun xs => M (hd xs + i1 (tl xs) ::: tl xs))
       t : mty_shift_add ρ i1 @ s) ->
  (Ty! fun xs => hd xs < i1 (tl xs) + i2 (tl xs) /\ Φ (tl xs); Γ ⊢(M) t : ρ @ s).
Proof.
  intros Hty1 Hty2.
  eapply tyT_shift_sub with (I := i1) in Hty2; unfold shift_add_fun in Hty2; cbn -[Nat.ltb] in Hty2.
  eapply tyT_weaken_Φ. eapply tyT_sub.
  eapply tyT_cases with
      (f := fun xs => hd xs <? i1 (tl xs))
      (Φ1 := fun xs => hd xs < i1 (tl xs) /\ Φ (tl xs))
      (Φ2 := fun xs => hd xs >= i1 (tl xs) /\ hd xs < i1 (tl xs) + i2 (tl xs) /\ Φ (tl xs)).
  - apply Hty1.
  - eapply tyT_weaken_Φ. apply Hty2. hnf; intros xs (Hxs1&Hxs2&Hxs3). split; cbn; auto. lia.
  - intros x Hx. now repeat setoid_rewrite subst_mty_strip.
  - now repeat setoid_rewrite subst_mty_strip.
  - hnf; intros y Hy. unfold ctx_if, ctx_shift_sub, ctx_shift_add.
    eapply eqmty_if_cases'.
    + reflexivity.
    + unfold mty_shift_sub, shift_sub_fun; cbn.
      setoid_rewrite <- subst_mty_id at 1.
      setoid_rewrite subst_mty_twice. unfold shift_add_fun, shift_sub_fun, funcomp. cbn.
      eapply eqmty_subst_idx_sem. hnf; intros xs (Hxs1&Hxs2) j; unfold id; f_equal; clear j.
      setoid_rewrite eta at 1. f_equal. lia.
    + now repeat setoid_rewrite subst_mty_strip.
  - eapply eqmty_if_cases''.
    + reflexivity.
    + unfold mty_shift_sub, shift_sub_fun; cbn.
      setoid_rewrite <- subst_mty_id at 1. repeat setoid_rewrite subst_mty_twice. unfold shift_add_fun, funcomp. cbn.
      eapply eqmty_subst_idx_sem. hnf; intros xs (Hxs1&Hxs2) j; unfold id; f_equal; clear j.
      setoid_rewrite eta at 1. f_equal. lia.
    + now repeat setoid_rewrite subst_mty_strip.
  - cbn -[Nat.ltb].
    hnf; intros xs Hxs. destruct (hd xs <? i1 (tl xs)) eqn:E.
    + reflexivity.
    + destruct Hxs as (Hxs1&Hxs2&Hxs3). apply Nat.ltb_ge in E.
      unfold shift_sub_fun; cbn.
      f_equal. rewrite eta. f_equal. lia.
  - cbn -[Nat.ltb]. hnf; intros xs (Hxs1&Hxs2).
    destruct (hd xs <? i1 (tl xs)) eqn:E; split; auto.
    + apply Nat.ltb_lt in E. lia.
    + apply Nat.ltb_ge in E. lia.
Qed.


(** Refinment for the weights *)
Lemma tyT_cases_shift' {ϕ} (i1 i2 : idx ϕ) (Φ : constr ϕ) (Γ : ctx (S ϕ)) (M1 M2 : idx (S ϕ)) (t : tm) (ρ : mty (S ϕ)) (s : skel) :
  (Ty! fun xs => hd xs < i1 (tl xs) /\ Φ (tl xs); Γ ⊢(M1) t : ρ @ s) ->
  (Ty! fun xs => hd xs < i2 (tl xs) /\ Φ (tl xs); ctx_shift_add Γ i1 ⊢(M2) t : mty_shift_add ρ i1 @ s) ->
  (Ty! fun xs => hd xs < i1 (tl xs) + i2 (tl xs) /\ Φ (tl xs);
       Γ ⊢(fun xs => if hd xs <? i1 (tl xs) then M1 xs else M2 (hd xs - i1 (tl xs) ::: tl xs)) t : ρ @ s).
Proof.
  intros Hty1 Hty2.
  apply tyT_cases_shift.
  - eapply tyT_sub'. apply Hty1. reflexivity.
    hnf; intros xs (Hxs1&Hxs2).
    rewrite (proj2 (Nat.ltb_lt _ _)) by assumption. reflexivity.
  - eapply tyT_sub'. apply Hty2. reflexivity.
    cbn -[Nat.ltb]; hnf; intros xs (Hxs1&Hxs2).
    rewrite (proj2 (Nat.ltb_ge _ _)) by lia.
    f_equal. rewrite eta at 1. f_equal. lia.
Qed.

(** Another refinment for two context *)
Lemma tyT_cases_shift'' {ϕ} (i1 i2 : idx ϕ) (Φ : constr ϕ) (Γ1 Γ2 : ctx (S ϕ)) (M1 M2 : idx (S ϕ)) (t : tm) (ρ : mty (S ϕ)) (s : skel) :
  (Ty! fun xs => hd xs < i1 (tl xs) /\ Φ (tl xs); Γ1 ⊢(M1) t : ρ @ s) ->
  (Ty! fun xs => hd xs < i2 (tl xs) /\ Φ (tl xs); Γ2 ⊢(M2) t : mty_shift_add ρ i1 @ s) ->
  (forall x, free x t -> mty_strip (Γ1 x) = mty_strip (Γ2 x)) ->
  (Ty! fun xs => hd xs < i1 (tl xs) + i2 (tl xs) /\ Φ (tl xs);
       ctx_if (fun xs => hd xs <? i1 (tl xs)) Γ1 (ctx_shift_sub Γ2 i1)
       ⊢(fun xs => if hd xs <? i1 (tl xs) then M1 xs else M2 (hd xs - i1 (tl xs) ::: tl xs)) t : ρ @ s).
Proof.
  intros Hty1 Hty2 Hstrip.
  apply tyT_cases_shift'.
  - eapply tyT_weaken; eauto.
    hnf; intros x Hx. unfold ctx_if, ctx_shift_add. apply eqmty_if_left.
    + setoid_rewrite subst_mty_strip; auto.
    + hnf; intros xs (Hxs1&Hxs2). now rewrite (proj2 (Nat.ltb_lt _ _)).
  - eapply tyT_weaken; eauto.
    hnf; intros x Hx. unfold ctx_if, ctx_shift_add.
    rewrite mty_if_shift_add by (now setoid_rewrite subst_mty_strip; eauto).
    rewrite eqmty_if_right.
    + unfold ctx_shift_sub. rewrite mty_shift_add_sub. reflexivity.
    + unfold ctx_shift_sub. repeat setoid_rewrite subst_mty_strip; eauto.
    + hnf; intros xs (Hxs1&Hxs2). rewrite (proj2 (Nat.ltb_ge _ _)); cbn; auto. lia.
Qed.


(** Lifting of [bsum_join_msum] to contexts *)

(* Auxilliary lemmas for lifting *)
Section Lifting0.
  Variable (A : Type). (* Nat *)
  Variable (B : A -> Type).
  Variable (C : forall a, B a -> Type).
  Variable H : A -> Prop. (* free a t *)
  Variable dec_H : forall a, { H a } + { ~ H a }. (* [fun a => free_dec a t] *)
  Variable def_B : forall a, B a. (* [def_ctx] *)

  Variable X : forall a : A, H a -> { x : B a & C x }.

  Lemma class_inv0 :
    { f : (forall a : A, B a) &
          (forall a : A, H a -> C (f a)) }.
  Proof.
    exists (fun a => match dec_H a with
             | left Ha => projT1 (X Ha)
             | right _ => def_B a
             end).
    intros a ha.
    destruct (dec_H a) as [Ha'|?]; try tauto.
    destruct (X Ha') as (x & y); cbn; eauto.
  Qed.
End Lifting0.

Section Lifting1.
  Variable (A : Type). (* Nat *)
  Variable (B : A -> Type).
  Variable (C : A -> Type).
  Variable (D : forall a, B a -> C a -> Type).
  Variable H : A -> Prop. (* free a t *)
  Variable dec_H : forall a, { H a } + { ~ H a }. (* [fun a => free_dec a t] *)
  Variable def_B : forall a, B a.

  (* Note that there is no dependency between [y] and [x] *)
  Variable X : forall a : A, H a -> { x : B a & { y : C a & D x y } }.

  Lemma class_inv1 :
    { f : (forall a : A, B a) &
          (forall (a : A), H a -> { y : C a & D (f a) y }) }.
  Proof.
    eexists (fun a =>
               match dec_H a with
               | left Ha => projT1 (X Ha)
               | right _ => def_B a
               end).
    intros a ha.
    destruct (dec_H a) as [Ha'|?]; try tauto.
    destruct (X Ha') as (x & y & z); cbn; eauto.
  Qed.
End Lifting1.


Lemma ctxBSum_join_msum {ϕ} (Φ : constr ϕ) (t : tm) (I1 I2 : idx ϕ) (Δ1 Δ2 : ctx (S ϕ)) (Γ1 Γ2 Γ1' Γ2' : ctx ϕ) :
  ctxBSum t I1 Δ1 Γ1 ->
  ctxBSum t I2 Δ2 Γ2 ->
  (ctx! t; Φ ⊢ Γ1 ≡ Γ1') ->
  (ctx! t; Φ ⊢ Γ2 ≡ Γ2') ->
  (forall x, free x t -> mty_comp Φ (Γ1' x) (Γ2' x)) ->
  existsS (Δ_bsum : ctx (S ϕ)) (Γ_bsum : ctx ϕ) (Γ1'_msum Γ2'_msum Γ_msum : ctx ϕ),
    (ctxBSum t (fun xs => I1 xs + I2 xs) Δ_bsum Γ_bsum) **
    (ctx! t; fun xs => hd xs < I1 (tl xs) + I2 (tl xs) /\ Φ (tl xs) ⊢
             (ctx_if (fun xs => hd xs <? I1 (tl xs)) Δ1 (ctx_shift_sub Δ2 I1)) ≡ Δ_bsum) **
    (ctxMSum t Γ1'_msum Γ2'_msum Γ_msum) **
    (ctx! t; Φ ⊢ Γ1'_msum ≡ Γ1') **
    (ctx! t; Φ ⊢ Γ2'_msum ≡ Γ2') **
    (ctx! t; Φ ⊢ Γ_bsum ≡ Γ_msum).
Proof.
  intros H1 H2 H3 H4 H5.
  pose proof (fun (x : nat) (Hx : free x t) => bsum_join_msum (H1 x Hx) (H2 x Hx) (H3 x Hx) (H4 x Hx) (H5 x Hx)).
  eapply @class_inv1 with (1 := fun a => free_dec a t) (2 := def_ctx) in X as (Δ_bsum   & X).
  eapply @class_inv1 with (1 := fun a => free_dec a t) (2 := def_ctx) in X as (Γ_bsum   & X).
  eapply @class_inv1 with (1 := fun a => free_dec a t) (2 := def_ctx) in X as (Γ1'_msum & X).
  eapply @class_inv1 with (1 := fun a => free_dec a t) (2 := def_ctx) in X as (Γ2'_msum & X).
  eapply @class_inv0 with (1 := fun a => free_dec a t) (2 := def_ctx) in X as (Γ_msum   & X).
  exists Δ_bsum, Γ_bsum, Γ1'_msum, Γ2'_msum, Γ_msum.
  repeat_split; hnf; intros; now apply X.
Qed.



(** Inversion for [ty_Lam] that removes as much redundant parameters as possible (due to 'precise typing') *)
(* TODO: Move *)
Section TyInv_Lam.
  Context {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (ρ : mty ϕ).

  Lemma tyT_Lam_inv t (s : skel) :
    @hastyT ϕ Φ Γ M (Lam t) ρ s ->
    existsS (I : idx ϕ) (Γ' : ctx ϕ) (Δa : ctx (S ϕ)) (Ka : idx (S ϕ)) σa τa
      (s' : skel),
      (ρ = [< I] ⋅ σa ⊸ τa) **
      (ctx! Lam t; Φ ⊢ Γ ≡ Γ') **
      (ctxBSum (Lam t) I Δa Γ') **
      (Ty! (fun xs => hd xs < I (tl xs) /\ Φ (tl xs)); (σa .: Δa) ⊢(Ka) t : τa @ s') **
      (sem! Φ ⊨ fun xs => (I xs + Σ_{a < I xs} Ka (a ::: xs)) = M xs) **
      (s = skel_Lam s').
  Proof.
    intros ty; inv ty.
    apply eqmty_Quant_inv'_sig in Hρ as (J & (ρ1&ρ2) & -> & Hρ & Hρ'').
    apply eqlty_Arr_inv in Hρ as (Hρ & Hρ').

    assert ({ Γ'' & (ctxBSum (Lam t) J Δ Γ'') ** ctx! Lam t; Φ ⊢ Γ ≡ Γ'' }) as (Γ'' & Hbsum' & HΓ').
    { epose proof ctxBSum_monotone' Hbsum (I2 := J) as (Γ'' & X1 & X2 & X3).
      exists Γ''. split. assumption. rewrite HΓ. apply eqCtx_iff; split.
      - eapply subCtx_mono_Φ; eauto. hnf; intros xs Hxs. now rewrite Hρ''.
      - eapply subCtx_mono_Φ; eauto. hnf; intros xs Hxs. now rewrite Hρ''. }

    eexists J, Γ'', Δ, K, _, _, s0. repeat_split.
    1,6: reflexivity.
    - apply HΓ'.
    - eapply Hbsum'.
    - eapply tyT_weaken_Φ. eapply tyT_sub.
      + eassumption.
      + apply eqCtx_scons.
        * symmetry. eapply eqmty_mono_Φ; eauto.
          hnf; intros xs (Hxs1&Hxs2); split; auto.
          eapply Nat.lt_le_trans; eauto.
          apply Nat.eq_le_incl. now apply Hρ''.
        * reflexivity.
      + eapply eqmty_mono_Φ; eauto.
        hnf; intros xs (Hxs1&Hxs2); split; auto.
        eapply Nat.lt_le_trans; eauto.
        apply Nat.eq_le_incl. now apply Hρ''.
      + hnf; reflexivity.
      + hnf; intros xs (Hxs1&Hxs2); split; auto.
        eapply Nat.lt_le_trans; eauto.
        apply Nat.eq_le_incl. now rewrite Hρ''.
    - hnf; intros xs Hxs. rewrite <- Hρ'' by auto. now apply HM.
  Qed.

End TyInv_Lam.


Lemma joining_Lam {ϕ} (Φ : constr ϕ) (Γ1 Γ2 : ctx ϕ) (N1 N2 : idx ϕ) (t : tm) (ρ1 ρ2 : mty ϕ) (s : skel) :
  (Ty! Φ; Γ1 ⊢(N1) Lam t : ρ1 @ s) ->
  (Ty! Φ; Γ2 ⊢(N2) Lam t : ρ2 @ s) ->
  mty_strip ρ1 = mty_strip ρ2 ->
  (forall x, free x (Lam t) -> mty_comp Φ (Γ1 x) (Γ2 x)) ->
  existsS (ρ : mty ϕ) (ρ1' ρ2' : mty ϕ) (Γ1' Γ2' Γ12 : ctx ϕ),
    (mty! Φ ⊢ ρ1' ≡ ρ1) **
    (mty! Φ ⊢ ρ2' ≡ ρ2) **
    (msum ρ1' ρ2' ρ) **
    (ctx! Lam t; Φ ⊢ Γ1' ≡ Γ1) **
    (ctx! Lam t; Φ ⊢ Γ2' ≡ Γ2) **
    (ctxMSum (Lam t) Γ1' Γ2' Γ12) **
    (Ty! Φ; Γ12 ⊢(fun xs => N1 xs + N2 xs) Lam t : ρ @ s).
Proof.
  intros ty1 ty2 Hstripτ HcompΓ.

  eapply tyT_Lam_inv in ty1 as (I1 & Γ'1 & Δ1 & K1 & σ1 & τ1 & s' & -> & HΓ1 & Hbsum1 & Hty1 & HM1 & ->).
  eapply tyT_Lam_inv in ty2 as (I2 & Γ'2 & Δ2 & K2 & σ2 & τ2 & ?  & -> & HΓ2 & Hbsum2 & Hty2 & HM2 & [= <-]).

  (* We need to construct a sum over [ρ1] and [ρ2]. *)
  unshelve epose proof (@make_msum_correct _ Φ ([ < I1]⋅ σ1 ⊸ τ1) ([ < I2]⋅ σ2 ⊸ τ2) _) as Hnewsum; auto.
  set (X := make_msum ([ < I1]⋅ σ1 ⊸ τ1) ([ < I2]⋅ σ2 ⊸ τ2)) in Hnewsum.
  unfold make_msum in X. cbn -[Nat.ltb mty_if lty_shift_add lty_shift_sub] in X.
  repeat change (lty_shift_sub (?τ ⊸ ?ρ) ?I) with (mty_shift_sub τ I ⊸ mty_shift_sub ρ I) in X.
  cbn -[Nat.ltb mty_if lty_shift_add lty_shift_sub] in X.
  set (ρ1' := fst X) in Hnewsum; set (ρ2' := fst (snd X)) in Hnewsum; set (ρ' := snd (snd X)) in Hnewsum.
  cbn zeta in Hnewsum. cbn -[Nat.ltb mty_if lty_shift_add lty_shift_sub] in ρ1', ρ2', ρ'. clear X.
  set (C := mty_if (fun xs : Vector.t nat (S ϕ) => hd xs <? I1 (tl xs)) σ1 (mty_shift_sub σ2 I1)
           ⊸ mty_if (fun xs : Vector.t nat (S ϕ) => hd xs <? I1 (tl xs)) τ1 (mty_shift_sub τ2 I1)) in ρ1',ρ2',ρ'.


  pose proof ctxBSum_join_msum (Φ := Φ) (t := Lam t) (I1 := I1) (I2 := I2)
        (Δ1 := Δ1) (Δ2 := Δ2) Hbsum1 Hbsum2 (eqCtx_sym HΓ1) (eqCtx_sym HΓ2) HcompΓ
    as (Δ_bsum & Γ_bsum & Γ1_msum & Γ2_msum & Γ12_msum & HctxSums).

  (* Before we start, some final simple assertions... *)
  assert (mty_strip σ1 = mty_strip σ2 /\ mty_strip τ1 = mty_strip τ2) as (Hσ_strip & Hτ_strip).
  { cbn in Hstripτ. split; congruence. }
  clear Hstripτ.


  assert (forall x, free x (Lam t) -> mty_strip (Δ1 x) = mty_strip (Δ2 x)) as HΔ_strip.
  { intros x Hx.
    hnf in Hbsum1, Hbsum2; specialize (Hbsum1 _ Hx); specialize (Hbsum2 _ Hx).
    apply bsum_strip in Hbsum1. apply bsum_strip in Hbsum2.
    rewrite Hbsum1, Hbsum2.
    eapply eqmty_strip in HΓ1; eauto. eapply eqmty_strip in HΓ2; eauto.
    rewrite <- HΓ1, <- HΓ2.
    eapply mty_comp_same_shape; eauto.
  }

  assert ((lty! fun xs => hd xs < I1 (tl xs) /\ Φ (tl xs) ⊢ C ≡ σ1 ⊸ τ1) /\
          (lty! fun xs => hd xs < I2 (tl xs) /\ Φ (tl xs) ⊢ lty_shift_add C I1 ≡ σ2 ⊸ τ2)) as (Hnewsum2' & Hnewsum3').
  { destruct Hnewsum as (Hnewsum1 & Hnewsum2 & Hnewsum3). split.
    - unfold ρ1' in Hnewsum2. apply eqmty_Quant_inv in Hnewsum2 as (Hnewsum2 & _).
      eapply eqlty_mono_Φ; eauto. firstorder.
    - unfold ρ1' in Hnewsum3. apply eqmty_Quant_inv in Hnewsum3 as (Hnewsum3 & _).
      eapply eqlty_mono_Φ; eauto. firstorder.
  }
  unfold C in Hnewsum3'. change (lty_shift_add (?τ ⊸ ?ρ) ?I) with (mty_shift_add τ I ⊸ mty_shift_add ρ I) in Hnewsum3'.
  apply eqlty_Arr_inv in Hnewsum2' as (Hnewsum2' & Hnewsum2''); apply eqlty_Arr_inv in Hnewsum3' as (Hnewsum3' & Hnewsum3'').


  (* Putting things together... *)
  exists ρ', ρ1', ρ2'. eexists Γ1_msum, Γ2_msum, Γ12_msum. repeat_split.
  { eapply eqmty_mono_Φ. apply Hnewsum. firstorder. }
  { eapply eqmty_mono_Φ. apply Hnewsum. firstorder. }
  { apply Hnewsum. }
  { eapply eqCtx_mono_Φ. apply HctxSums. firstorder. }
  { eapply eqCtx_mono_Φ. apply HctxSums. firstorder. }
  { apply HctxSums. }
  { (* The typing!!! *)
    eapply tyT_Lam with
        (I := fun xs => I1 xs + I2 xs)
        (σ := Arr_proj1 C) (τ := Arr_proj2 C)
        (K := fun xs => if hd xs <? I1 (tl xs) then K1 xs else K2 (hd xs - I1 (tl xs) ::: tl xs))
        (Δ := Δ_bsum).
    (* First the subtypings *)
    3:{ symmetry. now apply HctxSums. }
    4:{ reflexivity. }
    3:{ (* Cost *)
      hnf; intros xs Hxs. cbn -[Nat.ltb].
      rewrite Sum_idx_plus.
      hnf in HM1,HM2; specialize (HM1 xs Hxs); specialize (HM2 xs Hxs).
      erewrite Sum_ext.
      2:{ intros a Ha. rewrite (proj2 (Nat.ltb_lt _ _)) by assumption. reflexivity. }
      setoid_rewrite Sum_ext at 2.
      2:{ intros a Ha. rewrite (proj2 (Nat.ltb_ge _ _)) by lia.
          progress replace (I1 xs + a - I1 xs ::: xs) with (a ::: xs) by (f_equal; lia). reflexivity. }
      transitivity (I1 xs + (Σ_{a < I1 xs} K1 (a ::: xs)) + I2 xs + (Σ_{a < I2 xs} K2 (a ::: xs))). 2: lia.
      lia.
    }
    2:{ (* Context sum *) eapply HctxSums. }
    1:{ (* The typing!!! *)
      cbn [C Arr_proj1 Arr_proj2].
      eapply tyT_weaken_Φ. eapply tyT_sub.
      eapply tyT_cases_shift'' with
          (Γ3 := σ1 .: Δ1) (Γ4 := σ2 .: Δ2) (i1 := I1) (i2 := I2) (ρ := Arr_proj2 C) (Φ0 := Φ) (M1 := K1) (M2 := K2).
      - cbn [C Arr_proj2].
        eapply tyT_sub. eapply Hty1.
        + reflexivity.
        + symmetry. apply eqmty_if_left. now setoid_rewrite subst_mty_strip.
          hnf; intros xs (Hxs1&Hxs2). now apply Nat.ltb_lt.
        + hnf; reflexivity.
      - cbn [C Arr_proj2].
        eapply tyT_sub.
        + apply Hty2.
        + reflexivity.
        + symmetry in Hnewsum3''. eapply eqmty_mono_Φ. apply Hnewsum3''. firstorder.
        + hnf; reflexivity.

      - intros [ | x] Hx; cbn; auto.

      - rewrite ctx_shift_sub_scons, ctx_if_scons. apply eqCtx_scons.
        + reflexivity.
        + intros x Hx. symmetry. now apply HctxSums.
      - cbn -[Nat.ltb]. reflexivity.
      - cbn -[Nat.ltb]. hnf; reflexivity.
      - refine (fun xs Hxs => Hxs).
    }
  }
Qed.




(** Inversion for [ty_Lam] that removes as much redundant parameters as possible (due to 'precise typing') *)
(* TODO: Move *)
Section TyInv_Fix.
  Context {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (ρ : mty ϕ).

  Lemma tyT_Fix_inv t (s : skel) :
    @hastyT ϕ Φ Γ M (Fix t) ρ s ->
    existsS (Ib : idx (S ϕ)) (Δ : ctx (S ϕ)) (Jb : idx (S ϕ)) (A : lty (S (S ϕ))) (B : lty (S (S ϕ))) (B' : lty (S ϕ))
            (K : idx ϕ) (card1 : idx (S (S ϕ))) (card2 : idx (S ϕ)) (cardH : idx ϕ) (Γ' : ctx ϕ) (s' : skel),
      (@hastyT (S ϕ)
               (fun xs =>
                  let b := hd xs in
                  let xs' := tl xs in
                  b < cardH (xs') /\ Φ xs') ([< Ib] ⋅ A .: Δ)
               (Jb) (Lam t) ([<iConst 1] ⋅ B) s') **
      (lty! fun xs => let a := hd xs in
                   let b := hd (tl xs) in
                   let xs' : Vector.t nat ϕ := tl (tl xs) in
                   a < Ib (b ::: xs') /\ b < cardH xs' /\ Φ xs' ⊢
            subst_lty B
            (fun f xs =>
               let a := hd xs in
               let b := hd (tl xs) in
               let xs' := tl (tl xs) in
               f (0 ::: S (card1 xs + b) ::: xs'))
            ≡ A) **
      (sem! Φ ⊨ fun xs => isForestCard (fun b => Ib (b ::: xs)) (K xs) (cardH xs)) **
      (sem! fun xs => let a := hd xs in
                     let b := hd (tl xs) in
                     let xs' : Vector.t nat ϕ := tl (tl xs) in
                     a < Ib (b ::: xs') /\ b < cardH xs' /\ Φ xs'
            ⊨ fun xs => let a := hd xs in
                     let b := hd (tl xs) in
                     let xs' := tl (tl xs) in
                     isForestCard (fun c => Ib (S (b+c) ::: xs')) a (card1 xs)) **
      (sem! fun xs => let a := hd xs in
                   let xs' := tl xs in
                   a < K xs' /\ Φ xs'
          ⊨ fun xs => let a := hd xs in
                   let xs' := tl xs in
                   isForestCard (fun b => Ib (b ::: xs')) a (card2 xs)) **
      (ctxBSum (Fix t) cardH Δ Γ') **
      (ctx! (Fix t); Φ ⊢ Γ ≡ Γ') **
      (sem! Φ ⊨ fun xs => (Σ_{b < cardH xs} Jb (b ::: xs)) = M xs) **
      (lty! fun xs : Vector.t nat (S ϕ) => hd xs < K (tl xs) /\ Φ (tl xs) ⊢
            subst_lty B (fun (f : idx (S (S ϕ))) (xs : Vector.t nat (S ϕ)) =>
                           let a := hd xs in
                           let xs' := tl xs in
                           f (0 ::: card2 xs ::: xs')) ≡
            B') **
      (ρ = [< K] ⋅ B') **
      s = skel_Fix s'.
  Proof.
    intros ty; inv ty.
    apply eqmty_Quant_inv'_sig in Hρ as (K' & (ρ1&ρ2) (* ρ' *) & -> & Hρ & Hρ'').
    destruct B as (B1 & B2).
    change (subst_lty (?B1 ⊸ ?B2) ?i) with (subst_mty B1 i ⊸ subst_mty B2 i) in Hρ.
    apply eqlty_Arr_inv in Hρ as (Hρ & Hρ').
    cbn zeta in *.

    eexists Ib, Δ, Jb, A, (?[B1] ⊸ ?[B2]) , ?[B'], K', card1, card2, cardH, Γ', s0. repeat_split.
    11: reflexivity. (* skel *)
    10: reflexivity.
    9:{ change (subst_lty (?τ ⊸ ?ρ) ?I) with (subst_mty τ I ⊸ subst_mty ρ I). eapply eqlty_Arr; eauto. }
    8: eassumption.
    7: eassumption.
    6: eassumption.
    5:{ hnf; intros xs (Hxs1&Hxs2). eapply Hcard2; cbn; split; auto. eapply Nat.lt_le_trans; eauto. now rewrite Hρ''. }
    4: assumption.
    3:{ hnf; intros xs Hxs. hnf in Hρ''. rewrite <- Hρ'' by auto. auto. }
    2: assumption.
    1: assumption.
  Qed.

End TyInv_Fix.

Lemma lty_if_shift_one_add {ϕ} (f : Vector.t nat (S (S ϕ)) -> bool) (A1 A2 : lty (S (S ϕ))) (i : idx ϕ) :
  lty_strip A1 = lty_strip A2 ->
  lty_shift_one_add (lty_if f A1 A2) i =
  lty_if (fun xs => f (hd xs ::: i (tl (tl xs)) + hd (tl xs) ::: tl (tl xs))) (lty_shift_one_add A1 i) (lty_shift_one_add A2 i).
Proof.
  intros H.
  rewrite lty_shift_one_add_correct'.
  rewrite lty_if_subst_var_beta by auto.
  rewrite <- !lty_shift_one_add_correct'.
  f_equal. erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0; cbn.
  unfold subst_beta_fun; cbn.
  fext; intros xs. f_equal. simp_vect_to_list. f_equal.
  - now rewrite vect_cast_hd.
  - f_equal. f_equal.
    + f_equal. now simp_vect_to_list.
    + f_equal. now simp_vect_to_list.
  Unshelve. reflexivity.
Qed.

Lemma lty_shift_one_add_sub {ϕ} (A : lty (S (S ϕ))) (i : idx ϕ) :
  lty_shift_one_add (lty_shift_one_sub A i) i = A.
Proof.
  setoid_rewrite subst_lty_twice. unfold shift_one_sub_fun, shift_one_add_fun, funcomp.
  rewrite <- subst_lty_id. f_equal; fext; intros j' xs. unfold id; cbn.
  f_equal. rewrite eta. f_equal. rewrite eta. f_equal. lia.
Qed.



Definition idx_app_forests {ϕ} (I1 I2 : idx (S ϕ)) (H1 : idx ϕ) : idx (S ϕ) :=
  fun xs : Vector.t nat (S ϕ) => app_forests (fun a => I1 (a ::: tl xs)) (fun a => I2 (a ::: tl xs)) (H1 (tl xs)) (hd xs).


Lemma joining_Fix {ϕ} (Φ : constr ϕ) (Γ1 Γ2 : ctx ϕ) (N1 N2 : idx ϕ) (t : tm) (ρ1 ρ2 : mty ϕ) (s : skel) :
  (Ty! Φ; Γ1 ⊢(N1) Fix t : ρ1 @ s) ->
  (Ty! Φ; Γ2 ⊢(N2) Fix t : ρ2 @ s) ->
  mty_strip ρ1 = mty_strip ρ2 ->
  (forall x, free x (Fix t) -> mty_comp Φ (Γ1 x) (Γ2 x)) ->
  existsS (ρ : mty ϕ) (ρ1' ρ2' : mty ϕ) (Γ1' Γ2' Γ12 : ctx ϕ),
    (mty! Φ ⊢ ρ1' ≡ ρ1) **
    (mty! Φ ⊢ ρ2' ≡ ρ2) **
    (msum ρ1' ρ2' ρ) **
    (ctx! Fix t; Φ ⊢ Γ1' ≡ Γ1) **
    (ctx! Fix t; Φ ⊢ Γ2' ≡ Γ2) **
    (ctxMSum (Fix t) Γ1' Γ2' Γ12) **
    (Ty! Φ; Γ12 ⊢(fun xs => N1 xs + N2 xs) Fix t : ρ @ s).
Proof.
  intros ty1 ty2 Hstripτ HcompΓ.

  (* Inversion *)
  apply tyT_Fix_inv in ty1
    as (Ib1 & Δ1 & Jb1 & A1 & B1 & B1' & K1 & card11 & card21 & cardH1 & Γ'1 & s' & Hty1 & Hsub1 & HcardH1 & Hcard11 & Hcard21
        & Hbsum1 & HΓ1 & HM1 & HB1 & -> & ->).
  apply tyT_Fix_inv in ty2
    as (Ib2 & Δ2 & Jb2 & A2 & B2 & B2' & K2 & card12 & card22 & cardH2 & Γ'2 & ? & Hty2 & Hsub2 & HcardH2 & Hcard12 & Hcard22
        & Hbsum2 & HΓ2 & HM2 & HB2 & -> & [= <-]).
  cbn zeta in *. cbn in Hstripτ.

  (* We need to construct a sum over [ρ1] and [ρ2]. *)
  unshelve epose proof (@make_msum_correct _ Φ ([ < K1]⋅ B1') ([ < K2]⋅ B2') _) as Hnewsum; auto.
  set (X := make_msum ([ < K1]⋅ B1') ([ < K2]⋅ B2')) in Hnewsum.
  unfold make_msum in X. cbn -[Nat.ltb mty_if lty_shift_add lty_shift_sub] in X.
  set (ρ1' := fst X) in Hnewsum; set (ρ2' := fst (snd X)) in Hnewsum; set (ρ' := snd (snd X)) in Hnewsum.
  cbn zeta in Hnewsum. cbn -[Nat.ltb mty_if lty_shift_add lty_shift_sub] in ρ1', ρ2', ρ'. clear X.
  set (C := lty_if (fun xs : Vector.t nat (S ϕ) => hd xs <? K1 (tl xs)) B1' (lty_shift_sub B2' K1)) in ρ1',ρ2',ρ'.


  (* Joining the forest cardinalities *)
  (* First, we 'append' [Ib2] to [Ib1]. It holds that the [fc^(K1+K2) Ib12 = fc^K1 Ib1 + fc^K2 Ib2]. *)
  pose (Ib12 := idx_app_forests Ib1 Ib2 cardH1).

  (* First aux card *)
  pose (card1' :=
          fun xs => let a := hd xs in
                 let b := hd (tl xs) in
                 let xs' := tl (tl xs) in
                 if b <? cardH1 xs' then card11 xs
                 else card12 (a ::: b - cardH1 xs' ::: xs')).
  assert
    (sem! fun xs => let a := hd xs in
                 let b := hd (tl xs) in
                 let xs' := tl (tl xs) in
                 a < Ib12 (b ::: xs') /\ b < cardH1 xs' + cardH2 xs' /\ Φ xs'
        ⊨ fun xs => let a := hd xs in
                 let b := hd (tl xs) in
                 let xs' := tl (tl xs) in
                 isForestCard (fun c => Ib12 (S (b+c) ::: xs')) a (card1' xs)) as Hcard1'.
  {
    subst card1'. hnf; intros xs (Hxs1&Hxs2&Hxs3). cbn zeta.
    (* set (a := hd xs) in *; set (b := hd (tl xs)) in *; set (xs' := tl (tl xs)) in *. *)
    destruct (hd (tl xs) <? cardH1 (tl (tl xs))) eqn:E.
    - apply Nat.ltb_lt in E.
      subst Ib12. unfold idx_app_forests in Hxs1|-*; cbn -[Nat.ltb] in *.
      unfold app_forests in Hxs1. rewrite (proj2 (Nat.ltb_lt _ _)) in Hxs1 by auto.
      unfold app_forests.
      eapply isForestCard_ext_strong; eauto. cbn beta.
      intros x Hx.
      enough (S (hd (tl xs) + x) < cardH1 (tl (tl xs))) as -> % Nat.ltb_lt by reflexivity.
      enough (hd (tl xs) + card11 xs < cardH1 (tl (tl xs))) by lia; clear x Hx.
      enough (S (hd (tl xs) + card11 xs) < cardH1 (tl (tl xs))) by lia. (* A bit weakened.. *)
      eapply isForestCard_child_bounded' with (I := fun b => Ib1 (b ::: tl (tl xs))); eauto.
    - apply Nat.ltb_ge in E.
      subst Ib12. unfold idx_app_forests in Hxs1|-*; cbn -[Nat.ltb] in *.
      unfold app_forests in Hxs1. rewrite (proj2 (Nat.ltb_ge _ _)) in Hxs1 by auto.
      eapply isForestCard_ext_strong.
      { apply (Hcard12 (hd xs ::: hd (tl xs) - cardH1 (tl (tl xs)) ::: tl (tl xs))).
        cbn. repeat_split; auto. lia. }
      { cbn -[Nat.ltb].
        intros x Hx. unfold idx_app_forests, app_forests.
        rewrite (proj2 (Nat.ltb_ge _ _)).
        - f_equal. f_equal. lia.
        - lia.
      }
  }

  (* Second aux card *)
  pose (card2' := fun xs : Vector.t nat (S ϕ) =>
                    if hd xs <? K1 (tl xs) then card21 xs
                    else cardH1 (tl xs) + card22 (hd xs - K1 (tl xs) ::: tl xs)).
  assert
  (sem! fun xs => let a := hd xs in
               let xs' := tl xs in
               a < K1 xs' + K2 xs' /\ Φ xs'
      ⊨ fun xs => let a := hd xs in
               let xs' := tl xs in
               isForestCard (fun b => Ib12 (b ::: xs')) a (card2' xs)) as Hcard2'.
  {
    subst card2'. cbn zeta; hnf; intros xs (Hxs1&Hxs2).
    unfold Ib12, idx_app_forests(* , app_forests *); cbn -[Nat.ltb]. eta.
    destruct (hd xs <? K1 (tl xs)) eqn:E.
    - apply Nat.ltb_lt in E.
      eapply isForestCard_append_correct1.
      3: now apply Hcard21.
      2: now apply HcardH1.
      1: lia.
    - apply Nat.ltb_ge in E.
      eapply isForestCard_append_correct2.
      3:{ apply (Hcard22 (hd xs - K1 (tl xs) ::: tl xs)). cbn. split; auto. lia. }
      1-2: eauto.
  }


  (* Joining of the bounded sums *)
  pose proof ctxBSum_join_msum (Φ := Φ) (t := Fix t) (I1 := cardH1) (I2 := cardH2)
        (Δ1 := Δ1) (Δ2 := Δ2) Hbsum1 Hbsum2 (eqCtx_sym HΓ1) (eqCtx_sym HΓ2) HcompΓ
    as (Δ_bsum & Γ_bsum & Γ1_msum & Γ2_msum & Γ12_msum & HctxSums).


  (* Some easy lemmas *)

  assert (lty_strip A1 = lty_strip A2 /\ lty_strip B1 = lty_strip B2) as (A_strip & B_strip).
  { cbn in Hstripτ.
    apply eqlty_strip in HB1; apply eqlty_strip in HB2.
    setoid_rewrite subst_lty_strip in HB1; setoid_rewrite subst_lty_strip in HB2.
    split.
    - eapply eqlty_strip_commutes'; eauto. setoid_rewrite subst_lty_strip. congruence.
    - congruence.
  }


  (* Putting things together... *)
  exists ρ', ρ1', ρ2'. eexists Γ1_msum, Γ2_msum, Γ12_msum. repeat_split.
  { eapply eqmty_mono_Φ. apply Hnewsum. firstorder. }
  { eapply eqmty_mono_Φ. apply Hnewsum. firstorder. }
  { apply Hnewsum. }
  { eapply eqCtx_mono_Φ. apply HctxSums. firstorder. }
  { eapply eqCtx_mono_Φ. apply HctxSums. firstorder. }
  { apply HctxSums. }
  (* The typing! (Of [Fix t]) *)
  {
    eapply tyT_Fix with
        (Jb := fun xs => if hd xs <? cardH1 (tl xs) then Jb1 xs else Jb2 (hd xs - cardH1 (tl xs) ::: tl xs))
        (Δ := Δ_bsum)
        (Ib := Ib12)
        (* 'b' is variable #1 in 'A' and 'B' *)
        (A := lty_if (fun xs => let a := hd xs in
                             let b := hd (tl xs) in
                             let xs' := tl (tl xs) in
                             b <? cardH1 xs')
                     A1 (lty_shift_one_sub A2 cardH1))
        (B := lty_if (fun xs => let a := hd xs in
                             let b := hd (tl xs) in
                             let xs' := tl (tl xs) in
                             b <? cardH1 xs')
                     B1 (lty_shift_one_sub B2 cardH1))
        (K := fun xs => K1 xs + K2 xs)
        (cardH := fun xs => cardH1 xs + cardH2 xs)
        (card1 := card1')
        (card2 := card2')
        (Γ' := Γ_bsum).

    2:{ (* The A-B subtyping *)
      cbn zeta. etransitivity.
      2:{
        symmetry. eapply eqlty_mono_Φ. eapply eqlty_if_cases.
        - symmetry. apply Hsub1.
        - apply eqlty_shift_one_sub. symmetry. apply Hsub2.
        - now setoid_rewrite subst_lty_strip.
        - now repeat setoid_rewrite subst_lty_strip.
        - cbn -[Nat.ltb]; hnf; intros xs (Hxs1&Hxs2&Hxs3).
          {
            destruct (hd (tl xs) <? cardH1 (tl (tl xs))) eqn:E.
            - unfold Ib12, idx_app_forests, app_forests in Hxs1. cbn -[Nat.ltb] in Hxs1. rewrite E in Hxs1.
              apply Nat.ltb_lt in E. repeat_split; auto.
            - unfold Ib12, idx_app_forests, app_forests in Hxs1. cbn -[Nat.ltb] in Hxs1. rewrite E in Hxs1.
              apply Nat.ltb_ge in E. repeat_split; auto. lia.
          }
      }
      {
        cbn -[Nat.ltb].
        rewrite !subst_lty_beta_Fix1_correct.
        rewrite !lty_if_subst_beta_one_two. rewrite !lty_if_subst_beta_ground.
        cbn -[Nat.ltb]. repeat setoid_rewrite subst_lty_twice; unfold funcomp; cbn -[Nat.ltb].
        eapply eqlty_if_cases_f.
        {
          unfold card1'. hnf; intros xs Hxs.
          destruct (hd (tl xs) <? cardH1 (tl (tl xs))) eqn:E.
            - destruct Hxs as (Hxs1&Hxs2&Hxs3).
              unfold Ib12, idx_app_forests, app_forests in Hxs1. cbn -[Nat.ltb] in Hxs1. rewrite E in Hxs1.
              apply Nat.ltb_lt in E. apply Nat.ltb_lt.
              enough (S (hd (tl xs) + card11 xs) < cardH1 (tl (tl xs))) by lia.
              eapply isForestCard_child_bounded' with (I := fun b => Ib1 (b ::: tl (tl xs))); eauto.
          - apply Nat.ltb_ge in E. apply Nat.ltb_ge. lia.
        }
        {
          eapply eqlty_subst_idx_sem.
          hnf; intros xs (Hxs1&Hxs2).
          unfold subst_beta_ground_fun, subst_beta_one_two_fun, iConst. intros j; f_equal; clear j.
          cbn. f_equal. f_equal. f_equal. f_equal.
          unfold card1'.
          enough (hd (tl xs) < cardH1 (tl (tl xs))) as -> % Nat.ltb_lt by reflexivity.
          apply Nat.ltb_lt in Hxs1. lia.
        }
        {
          eapply eqlty_subst_idx_sem.
          hnf; intros xs (Hxs1&Hxs2).
          unfold shift_one_sub_fun, subst_beta_ground_fun, subst_beta_one_two_fun, iConst. intros j; f_equal; clear j.
          cbn -[Nat.ltb "-"]. f_equal. f_equal.
          unfold card1'.
          enough (cardH1 (tl (tl xs)) <= hd (tl xs)) as E by (rewrite (proj2 (Nat.ltb_ge _ _) E); lia).
          apply Nat.ltb_ge in Hxs1.
          destruct (hd (tl xs) <? cardH1 (tl (tl xs))) eqn:E; swap 1 2.
          { now apply Nat.ltb_ge in E. }
          { exfalso.
            destruct Hxs2 as (Hxs2&Hxs3&Hxs4).
            unfold Ib12, idx_app_forests, app_forests in Hxs2. cbn -[Nat.ltb] in Hxs1. cbn -[Nat.ltb] in Hxs2. rewrite E in Hxs2.
            enough (S (hd (tl xs) + card1' xs) < cardH1 (tl (tl xs))) by lia; clear Hxs1.
            eapply isForestCard_child_bounded' with (I := fun b => Ib1 (b ::: tl (tl xs))).
            3:{ eauto. }
            3:{ unfold card1'. rewrite E. eapply Hcard11. apply Nat.ltb_lt in E; auto. }
            1:{ auto. }
            1:{ now apply Nat.ltb_lt in E. }
          }
        }
        all: now repeat setoid_rewrite subst_lty_strip.
      }
    }

    2:{ (* The main forest card *)
      hnf; intros xs Hxs.
      unfold Ib12, idx_app_forests; cbn.
      apply isForestCard_append_correct0; eauto.
    }

    2: exact Hcard1'. (* First aux card *)
    2: exact Hcard2'. (* Second aux card *)

    (* The joined context sum *)
    2: now apply HctxSums.
    2:{ symmetry. apply HctxSums. }


    2:{ (* The cost *)
      cbn -[Nat.ltb]; hnf in HM1,HM2|-*; intros xs Hxs.
      rewrite <- HM1, <- HM2 by auto.
      rewrite Sum_idx_plus.
      setoid_rewrite Sum_ext at 1.
      2:{ intros a Ha. rewrite (proj2 (Nat.ltb_lt _ _)) by assumption. reflexivity. }
      setoid_rewrite Sum_ext at 2.
      2:{ intros a Ha. rewrite (proj2 (Nat.ltb_ge _ _)) by lia.
          rewrite minus_plus. reflexivity. }
      cbn. lia.
    }

    2:{ (* The final subtyping (contains [card2']) *)
      subst ρ' C. cbn zeta.
      eapply eqmty_Quant. 2: hnf; reflexivity.
      etransitivity.
      2:{ symmetry.
          eapply eqlty_mono_Φ. eapply eqlty_if_cases.
          - symmetry. apply HB1.
          - apply eqlty_shift_sub. symmetry. apply HB2.
        - now setoid_rewrite subst_lty_strip.
        - now repeat setoid_rewrite subst_lty_strip.
        - cbn -[Nat.ltb].
          hnf; intros xs (Hxs1&Hxs2).
          destruct Nat.ltb eqn:E.
          + apply Nat.ltb_lt in E. split; auto.
          + apply Nat.ltb_ge in E. split; auto. lia.
      }
      setoid_rewrite subst_lty_twice; unfold funcomp; cbn -[Nat.ltb]. unfold shift_sub_fun; cbn -[Nat.ltb].
      rewrite !subst_lty_beta_Fix2_correct, !lty_if_subst_beta_one_one, !lty_if_subst_beta_ground; cbn -[Nat.ltb].
      eapply eqlty_if_cases_f.
      { hnf; intros xs (Hxs1&Hxs2).
        enough (card2' xs < cardH1 (tl xs) <-> hd xs < K1 (tl xs)) as X.
        { revert X; clear_all; intros X.
          destruct (hd xs <? K1 (tl xs)) eqn:E.
          - apply Nat.ltb_lt in E. apply Nat.ltb_lt. firstorder.
          - apply Nat.ltb_ge in E. apply Nat.ltb_ge. firstorder. }
        eapply isForestCard_append_correct3.
        - apply Hxs1.
        - now apply HcardH1.
        - hnf in Hcard2'; cbn in Hcard2'; unfold Ib12, idx_app_forests in Hcard2'.
          specialize (Hcard2' _ (conj Hxs1 Hxs2)); cbn in Hcard2'. eauto.
      }
      {
        setoid_rewrite subst_lty_twice; unfold funcomp.
        eapply eqlty_subst_idx_sem; eauto.
        hnf; intros xs (Hxs1&Hxs2&Hxs3). apply Nat.ltb_lt in Hxs1.
        unfold subst_beta_ground_fun, subst_beta_one_fun, iConst; cbn -[Nat.ltb].
        intros j; f_equal; clear j.
        f_equal. f_equal.
        unfold card2' in Hxs1|-*. rewrite (proj2 (Nat.ltb_lt _ _)). reflexivity.
        apply Nat.ltb_lt. destruct (hd xs <? K1 (tl xs)) eqn:E; auto. exfalso. lia.
      }
      {
        repeat setoid_rewrite subst_lty_twice; unfold funcomp.
        eapply eqlty_subst_idx_sem; eauto.
        hnf; intros xs (Hxs1&Hxs2&Hxs3). apply Nat.ltb_ge in Hxs1.
        unfold shift_one_sub_fun, subst_beta_ground_fun, subst_beta_one_fun, iConst; cbn -[Nat.ltb].
        intros j; f_equal; clear j.
        f_equal. f_equal.
        unfold card2' in Hxs1|-*. rewrite (proj2 (Nat.ltb_ge _ _)). lia.
        apply Nat.ltb_ge. destruct (hd xs <? K1 (tl xs)) eqn:E; auto. exfalso.
        enough (card21 xs < cardH1 (tl xs)) by lia.
        eapply isForestCard_append_correct3.
        - apply Hxs2.
        - now apply HcardH1.
        - apply Nat.ltb_lt in E.
          eapply isForestCard_append_correct1.
          + apply Nat.lt_le_incl. apply E.
          + now apply HcardH1.
          + now eapply Hcard21.
        - now apply Nat.ltb_lt.
      }
      all: now repeat setoid_rewrite subst_lty_strip.
    }

    1:{ (* The final typing! (Of [Lam t]) *)

      eapply tyT_weaken_Φ. eapply tyT_weaken.
      eapply tyT_cases_shift'' with
          (Γ3 := Quant Ib1 A1 .: Δ1) (Γ4 := Quant Ib2 A2 .: Δ2)
          (i1 := cardH1) (i2 := cardH2) (M1 := Jb1) (M2 := Jb2).
      - eapply tyT_sub'. apply Hty1.
        eapply eqmty_Quant. 2: hnf; reflexivity.
        symmetry; apply eqlty_if_left.
        + now setoid_rewrite subst_lty_strip.
        + hnf; intros xs (Hxs1&Hxs2&Hxs3). cbn -[Nat.ltb]. now apply Nat.ltb_lt.
        + hnf; reflexivity.
      - eapply tyT_sub'. apply Hty2.
        rewrite mty_shift_add_eq_Quant.
        eapply eqmty_Quant. 2: hnf; reflexivity.
        rewrite <- !lty_shift_one_add_correct. (* Why is this even here? *)
        rewrite !lty_if_shift_one_add.
        rewrite lty_shift_one_add_sub.
        symmetry; apply eqlty_if_right.
        + now setoid_rewrite subst_lty_strip.
        + hnf; intros xs (Hxs1&Hxs2&Hxs3). cbn -[Nat.ltb]. apply Nat.ltb_ge. lia.
        + now setoid_rewrite subst_lty_strip.
        + hnf; reflexivity.
      - intros [ | x] Hx; cbn; auto.
        hnf in Hbsum1, Hbsum2; specialize (Hbsum1 _ Hx); specialize (Hbsum2 _ Hx).
        apply bsum_strip in Hbsum1. apply bsum_strip in Hbsum2. rewrite Hbsum1, Hbsum2.
        hnf in HΓ1,HΓ2; specialize (HΓ1 _ Hx); specialize (HΓ2 _ Hx).
        eapply eqmty_strip in HΓ1; eapply eqmty_strip in HΓ2. rewrite <- HΓ1, <- HΓ2.
        eapply mty_comp_same_shape; eauto.
      - rewrite ctx_shift_sub_scons, ctx_if_scons. apply eqCtx_scons.
        + cbn -[Nat.ltb]. apply eqmty_Quant.
          * reflexivity.
          * hnf; intros xs (Hxs1&Hxs2). unfold Ib12, idx_app_forests, app_forests. rewrite <- eta. reflexivity.
        + intros. symmetry. now apply HctxSums.
      - firstorder.
    }
  }
  Unshelve. exact (fun _ => def_nat). (* from where? *)
Qed.

Theorem joining {ϕ} (Φ : constr ϕ) (Γ1 Γ2 : ctx ϕ) (N1 N2 : idx ϕ) (v : tm) (ρ1 ρ2 : mty ϕ) (s : skel) :
  (Ty! Φ; Γ1 ⊢(N1) v : ρ1 @ s) ->
  (Ty! Φ; Γ2 ⊢(N2) v : ρ2 @ s) ->
  val v ->
  mty_strip ρ1 = mty_strip ρ2 ->
  (forall x, free x (v) -> mty_comp Φ (Γ1 x) (Γ2 x)) ->
  existsS (ρ : mty ϕ) (ρ1' ρ2' : mty ϕ) (Γ1' Γ2' Γ12 : ctx ϕ),
    (mty! Φ ⊢ ρ1' ≡ ρ1) **
    (mty! Φ ⊢ ρ2' ≡ ρ2) **
    (msum ρ1' ρ2' ρ) **
    (ctx! v; Φ ⊢ Γ1' ≡ Γ1) **
    (ctx! v; Φ ⊢ Γ2' ≡ Γ2) **
    (ctxMSum (v) Γ1' Γ2' Γ12) **
    (Ty! Φ; Γ12 ⊢(fun xs => N1 xs + N2 xs) v : ρ @ s).
Proof.
  intros ty1 ty2 Hval Hstripτ HstripΓ.
  (* (* destruct Hval *) -- doesn't work because [val] is defined in [Prop]. *)
  apply valb_iff in Hval; destruct v; cbn in Hval; try discriminate; clear Hval.
  - eapply joining_Lam; eauto.
  - eapply joining_Fix; eauto.
  - eapply joining_Const; eauto.
Qed.
