Global Set Implicit Arguments.

Require Export Star.
Require Export LexOrder.

Require Export RelationClasses.
Require Export Setoid Relations Morphisms.
Require Export Omega Lia.


(** *** Natural numbers *)


Lemma complete_induction (p : nat -> Prop) :
  (forall x, (forall y, y < x -> p y) -> p x) -> forall x, p x.
Proof. intros H. intros x. induction (Nat.lt_wf_0 x); eauto. Qed.

Lemma size_induction {X : Type} (p : X -> Prop) (size : X -> nat) :
  (forall x, (forall y, size y < size x -> p y) -> p x) -> forall x, p x.
Proof. intros H. intros x. induction (well_founded_ltof _ size x); eauto. Qed.

Fixpoint le_sig_plus (n m : nat) { struct n } : n <= m -> { p : nat | m = n + p }.
Proof.
  destruct n.
  - intros _. exists m. reflexivity.
  - destruct m.
    + intros H. exfalso. abstract lia.
    + intros H % le_S_n. specialize (le_sig_plus n m H) as (p&IH).
      eexists p. cbn. f_equal. assumption.
Defined.

From Coq Require Import Peano_dec.
Lemma le_sig_plus_unique (m n : nat) (H1 H2 : m <= n) :
  le_sig_plus H1 = le_sig_plus H2.
Proof. f_equal. apply le_unique. Qed.


(** Version of [induction_ltof1] in [Type] *)
Theorem induction_ltof1_Type (A : Type) (f : A -> nat) :
  forall P:A -> Type,
    (forall x:A, (forall y:A, ltof A f y x -> P y) -> P x) -> forall a:A, P a.
Proof.
  intros P F.
  assert (H : forall n (a:A), f a < n -> P a).
  { induction n.
    - intros; absurd (f a < 0); auto with arith.
    - intros a Ha. apply F. unfold ltof. intros b Hb.
      apply IHn. apply Nat.lt_le_trans with (f a); auto with arith. }
  intros a. apply (H (S (f a))). auto with arith.
Defined.






(** *** Tactics *)


(** Inversion *)

Ltac inv H := inversion H; subst; clear H.


(** Changes the goal [P1 /\ ... /\ Pn] into goals [P1], ..., [Pn] *)

Ltac repeat_split :=
  repeat lazymatch goal with
         | [ |- ?P1 /\ ?P2] => split
         | [ |- ?P1 * ?P2] => split
         end.

Ltac repeat_eexists :=
  repeat lazymatch goal with
         | [ |- exists x, _] => eexists
         | [ |- { x & _ }] => eexists
         | [ |- { x | _ }] => eexists
         end.

(** Clear as much as possible! *)

Ltac clear_all :=
  repeat match goal with
         | [ H : _ |- _ ] => clear H
         end.


(** **** Modus ponens tactic *)

(* Prove the non-dependent hypothesis of a hypothesis that is a implication and specialize it *)
Tactic Notation "spec_assert" hyp(H) :=
  let H' := fresh in
  match type of H with
  | ?A -> _ =>
    assert A as H'; [ | specialize (H H'); clear H']
  end.

Tactic Notation "spec_assert" hyp(H) "as" simple_intropattern(p) :=
  let H' := fresh in
  match type of H with
  | ?A -> _ =>
    assert A as H'; [ | specialize (H H') as p; clear H']
  end.

Tactic Notation "spec_assert" hyp(H) "by" tactic(T) :=
  let H' := fresh in
  match type of H with
  | ?A -> _ =>
    assert A as H' by T; specialize (H H'); clear H'
  end.


Tactic Notation "spec_assert" hyp(H) "as" simple_intropattern(p) "by" tactic(T) :=
  let H' := fresh in
  match type of H with
  | ?A -> _ =>
    assert A as H' by T; specialize (H H') as p; clear H'
  end.


(** *** [skipn] *)

From Coq Require Export List.
(* For list notations like [1;2;3], do [Import ListNotations.] *)

Lemma skipn_nil (X : Type) (n : nat) : skipn n nil = @nil X.
Proof. destruct n; cbn; auto. Qed.

Lemma skipn_app (X : Type) (xs ys : list X) (n : nat) :
  skipn (length xs + n) (xs ++ ys) = skipn n ys.
Proof. revert ys. induction xs; intros; cbn in *; auto. Qed.

Lemma skipn_app' (X : Type) (xs ys : list X) (n : nat) :
  n = (length xs) ->
  skipn n (xs ++ ys) = ys.
Proof. intros ->. replace (length xs) with (length xs + 0) by omega. rewrite skipn_app. reflexivity. Qed.

Lemma skipn_length (X : Type) (n : nat) (xs : list X) :
  length (skipn n xs) = length xs - n.
Proof.
  revert xs. induction n; intros; cbn.
  - omega.
  - destruct xs; cbn; auto.
Qed.


Lemma skipn_tl (X : Type) (n : nat) (xs : list X) :
  skipn (S n) xs = tl (skipn n xs).
Proof.
  cbn. induction xs in n|-*; cbn; auto.
  - now rewrite skipn_nil.
  - destruct n; cbn; auto.
Qed.

Lemma skipn_tl' (X : Type) (n : nat) (xs : list X) :
  skipn n (tl xs) = tl (skipn n xs).
Proof.
  cbn. induction xs in n|-*; cbn; auto.
  - now rewrite skipn_nil.
  - destruct n; cbn; auto.
    rewrite <- IHxs.
    destruct xs; auto.
    now rewrite skipn_nil.
Qed.

Lemma skipn_skipn (X : Type) (n m : nat) (xs : list X) :
  skipn n (skipn m xs) = skipn (n+m) xs.
Proof.
  induction m in n,xs|-*.
  - cbn. now replace (n + 0) with n by omega.
  - replace (n + S m) with (S n + m) by omega.
    rewrite <- IHm. now rewrite !skipn_tl, skipn_tl'.
Qed.


Lemma skipn_In (X : Type) n (xs : list X) x :
  In x (skipn n xs) ->
  In x xs.
Proof.
  induction n in xs|-*; cbn in *.
  - auto.
  - destruct xs; cbn; eauto.
Qed.

Lemma skipn_is_nil (X : Type) n (xs : list X) :
  skipn n xs = nil ->
  length xs <= n.
Proof.
  induction n in xs|-*; cbn in *.
  - intros ->. cbn. reflexivity.
  - destruct xs.
    + cbn. omega.
    + cbn. intros. specialize IHn with (1 := H). omega.
Qed.

Lemma skipn_is_not_nil (X : Type) n (xs : list X) :
  skipn n xs <> nil ->
  n < length xs.
Proof.
  induction n in xs|-*; cbn in *.
  - destruct xs; auto. congruence. cbn. omega.
  - destruct xs.
    + cbn. congruence.
    + cbn. intros. specialize IHn with (1 := H). omega.
Qed.


Definition list_id {A}  { f : A -> A} :
  (forall x, f x = x) -> forall xs, List.map f xs = xs.
Proof.
  intros H. induction xs. reflexivity.
  cbn. rewrite H. rewrite IHxs; eauto.
Qed.





Definition bind_option {A B : Type} : (A -> option B) -> option A -> option B :=
  fun f a =>
    match a with
    | None => None
    | Some x => f x
    end.

Lemma bind_option_bind_option {A B C : Type}
      (x : option A) (g : A -> option B) (f : B -> option C) :
  bind_option f (bind_option g x) = bind_option (fun (a : A) => bind_option f (g a)) x.
Proof. destruct x; reflexivity. Qed.



(** *** Functions on sums, options and pairs *)


(** Apply functions in tuples, options, etc. *)
Section Map.
  Variable X Y Z : Type.

  Definition map_opt : (X -> Y) -> option X -> option Y :=
    fun f a =>
      match a with
      | Some x => Some (f x)
      | None => None
      end.

  Definition map_inl : (X -> Y) -> X + Z -> Y + Z :=
    fun f a =>
      match a with
      | inl x => inl (f x)
      | inr y => inr y
      end.

  Definition map_inr : (Y -> Z) -> X + Y -> X + Z :=
    fun f a =>
      match a with
      | inl y => inl y
      | inr x => inr (f x)
      end.

  Definition map_fst : (X -> Z) -> X * Y -> Z * Y := fun f '(x,y) => (f x, y).
  Definition map_snd : (Y -> Z) -> X * Y -> X * Z := fun f '(x,y) => (x, f y).

  Lemma map_opt_eq (f : X -> Y) (o : option X) :
    map_opt f o = bind_option (fun x => Some (f x)) o.
  Proof. reflexivity. Qed.

End Map.


(** Folding for options *)
Definition fold_opt (X Y : Type) : (X -> Y) -> Y -> option X -> Y :=
  fun f def o => match o with
              | Some o' => f o'
              | None => def
              end.


Lemma fold_bind_option {A B C : Type} (f : B -> C) (x : C) (g : A -> option B) (o : option A) :
  fold_opt f x (bind_option g o) =
  fold_opt (fun (a : A) => fold_opt f x (g a)) x o.
Proof. destruct o; reflexivity. Qed.

Lemma map_opt_fold (X Y : Type) (f : X -> Y) (x : option X) :
  map_opt f x = fold_opt (fun x => Some (f x)) None x.
Proof. intros. destruct x; cbn; reflexivity. Qed.



(** *** Bounded sums *)

Fixpoint Sum (i : nat) (K : nat -> nat) : nat :=
  match i with
  | 0 => 0
  | S i' => Sum i' K + K i'
  end.

Notation "'Σ_{' a '<' I '}' K" :=
  (Sum I (fun a => K)) (at level 59, a ident, K at level 99, right associativity, format "'Σ_{' a  '<'  I '}'  K").

(* Compute Σ_{a < 4} a. *)
(*      (* = 6 *) *)
(*      (* : nat *) *)
(* Compute Σ_{a < 4} a*a. *)
(*      (* = 14 *) *)
(*      (* : nat *) *)


Lemma Sum_eq_0 (K : nat -> nat) :
  Sum 0 K = 0.
Proof. reflexivity. Qed.

Lemma Sum_eq_S (i : nat) (K : nat -> nat) :
  Sum (S i) K = Sum i K + K i.
Proof. reflexivity. Qed.

Lemma Sum_idx_plus (K : nat -> nat) (i1 i2 : nat) :
  Sum (i1+i2) K = Sum i1 K + Sum i2 (fun x => K (i1+x)).
Proof.
  induction i2 in i1,K|-*; cbn.
  - replace (i1 + 0) with i1 by omega. omega.
  - replace (i1 + S i2) with (S (i1 + i2)) by omega. cbn. rewrite !IHi2. omega.
Qed.

Lemma Sum_plus I K1 K2 :
  Sum I (fun a => K1 a + K2 a) = Sum I K1 + Sum I K2.
Proof. induction I; cbn; omega. Qed.

Lemma Sum_plus' I K1 K2 :
  Sum I (fun a => K1 a + K2 a) = Sum I K2 + Sum I K1.
Proof. rewrite Sum_plus. omega. Qed.



Lemma Sum_monotone I1 I2 K :
  I1 <= I2 ->
  Sum I1 K <= Sum I2 K.
Proof.
  induction 1; cbn.
  - destruct I1; cbn; omega.
  - omega.
Qed.


Lemma Sum_eq_S' (i : nat) (K : nat -> nat) :
  Sum (S i) K = K 0 + Sum i (fun x => K (S x)).
Proof. replace (S i) with (1 + i) by omega. rewrite Sum_idx_plus. cbn. reflexivity. Qed.


Lemma Sum_ext I K1 K2 :
  (forall a, a < I -> K1 a = K2 a) ->
  (Σ_{a < I} K1 a) = (Σ_{a < I} K2 a).
Proof.
  intros H. induction I; cbn.
  - reflexivity.
  - rewrite IHI; auto.
Qed.

Lemma Sum_idx_plus' (K : nat -> nat) (i1 i2 : nat) :
  Sum (i1+i2) K = Sum i1 K + Sum i2 (fun x => K (x+i1)).
Proof. rewrite Sum_idx_plus. f_equal. apply Sum_ext. intros. f_equal. omega. Qed.


Lemma Sum_const' I c K :
  (forall a, a < I -> K a = c) ->
  (Σ_{a < I} K a) = I * c.
Proof.
  intros H. induction I; cbn.
  - reflexivity.
  - rewrite IHI; auto. rewrite H by omega. omega.
Qed.

Lemma Sum_const I c :
  (Σ_{a < I} c) = I * c.
Proof. induction I; cbn; omega. Qed.

Lemma Sum_constO' I K :
  (forall a, a < I -> K a = 0) ->
  (Σ_{a < I} K a) = 0.
Proof. intros. erewrite Sum_const'; eauto. Qed.

Lemma Sum_constO I :
  (Σ_{a < I} 0) = 0.
Proof. rewrite Sum_const. omega. Qed.

Lemma Sum_const1 I :
  (Σ_{a < I} 1) = I.
Proof. rewrite Sum_const. omega. Qed.


(** Every element in the sum is a lower bound for the sum *)
Lemma Sum_lb (I : nat) (K : nat -> nat) (i : nat) :
  i < I ->
  K i <= Σ_{a < I} K a.
Proof.
  intros H. induction I as [ | I IH] in i,K,H|-*.
  - cbn. omega.
  - destruct i.
    + rewrite Sum_eq_S'. omega.
    + rewrite Sum_eq_S'.
      apply lt_S_n in H. specialize IH with (1 := H) (K := fun x => K (S x)); cbn in *.
      omega.
Qed.


(** *** Infinite sums *)

(* Upper bound for an infinite sum *)
Definition infSum_ub (Ki : nat -> nat) (k : nat) : Prop :=
  forall i, (Σ_{a < i} Ki a) <= k.

Lemma infsum_ub_nil :
  infSum_ub (fun x => 0) 0.
Proof. hnf. intros i. rewrite Sum_constO. reflexivity. Qed.

Lemma infsum_ub_cons (k : nat) (y : nat) (f : nat -> nat) :
  infSum_ub f k ->
  infSum_ub (fun x => match x with 0 => y | S x' => f x' end) (k + y).
Proof.
  unfold infSum_ub. intros H. intros i.
  destruct i.
  - cbn. omega.
  - rewrite Sum_eq_S'. specialize (H i). omega.
Qed.


(** *** Notations for Sigma types *)

Module SigmaTypeNotations.

(** [p] must have sort [Type] *)
Notation "'existsS' x .. y , p" := (sigT (fun x => .. (sigT (fun y => p)) ..))
  (at level 200, x binder, right associativity,
   format "'[' 'existsS' '/ '  x  ..  y ,  '/' p ']'")
  : type_scope.

(* Check (existsS (x : nat) (y : nat) (b : bool), x = 3 /\ y = x + 2 /\ b = true). *)

(** Notation for "chained" [sigT], ending with one last [sig]. The last binder has to be preceeded by "&" *)
Notation "'existsST' x .. y & z , p" := (sigT (fun x => .. (sigT (fun y => (sig (fun z => p)))) ..))
  (at level 200, x binder, z ident, right associativity,
   format "'[' 'existsST' '/ ' x  ..  y  & '/ '  z ,  '/ ' p ']'",
   only parsing)
  : type_scope.


Notation "x ** y" := (prod x y) (at level 80, right associativity).


(* Goal existsST x y & z, x = 4 /\ y = 10 /\ z = x + y. *)
(* Proof. repeat eexists. Qed. *)

End SigmaTypeNotations.
