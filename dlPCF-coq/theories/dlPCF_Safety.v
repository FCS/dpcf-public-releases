Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax PCF_Semantics.
Require Import dlPCF_iSubst.
Require Import ForestCard dlPCF_Types dlPCF_Sum dlPCF_Contexts dlPCF_Typing dlPCF_Splitting.
Require PCF_Types.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.
Open Scope vector_scope.

From Coq Require Import Vector Fin.
From Coq Require Import Lia.

Open Scope PCF.


Implicit Types (ϕ : nat).


(** *** Substitution lemma *)

(** For the beta substitution type preservation lemma, we assume that our substitution is naive. **)

(* [Γ] is like [Γ'] (for all [m] free variables except [x]), but it has [x : σ] *)
Definition ctxExtends {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (m : nat) (x : nat) (σ : mty ϕ) (Γ' : ctx ϕ) : Prop :=
  (forall y, y < m -> x <> y -> mty! Φ ⊢ Γ' y ⊑ Γ y) /\ Γ x = σ.


(* TODO: Move *)
Lemma Sum_monotone' (I : nat) (f1 f2 : nat -> nat) :
  (forall i, i < I -> f1 i <= f2 i) ->
  (Σ_{a < I} f1 a) <= Σ_{a < I} f2 a.
Proof.
  induction I as [ | I IH] in f1,f2|-* ; intros.
  - reflexivity.
  - rewrite !Sum_eq_S'.
    specialize (H 0 ltac:(lia)) as H'.
    enough ((Σ_{x < I} f1 (S x)) <= (Σ_{x < I} f2 (S x))) by lia.
    apply IH. intros i Hi. apply H. lia.
Qed.

Lemma nsubst_free_no_effect (t u: tm) (x : nat) :
  ~ free x t ->
  nsubst t x u = t.
Proof.
  intros Hfree. induction t in x,u,Hfree|-*; cbn in *.
  - subst. destruct Nat.eq_dec; congruence.
  - f_equal. now apply IHt.
  - f_equal. now apply IHt.
  - f_equal; firstorder.
  - f_equal; firstorder.
  - reflexivity.
  - f_equal; firstorder.
  - f_equal; firstorder.
Qed.

Lemma nsubst_maxVar_no_effect (t u: tm) (x : nat) :
  maxVar t <= x ->
  nsubst t x u = t.
Proof.
  intros Hle. induction t in x,u,Hle|-*; cbn in *.
  - destruct Nat.eq_dec; try congruence. subst. lia.
  - f_equal. apply IHt. lia.
  - f_equal. apply IHt. lia.
  - f_equal; [apply IHt1; lia | apply IHt2; lia].
  - f_equal; [apply IHt1; lia | apply IHt2; lia | apply IHt3; lia].
  - reflexivity.
  - f_equal. apply IHt. lia.
  - f_equal. apply IHt. lia.
Qed.


Lemma free_nsubst var t x u :
  free var (nsubst t x u) ->
  free var t \/ exists var', free var' u.
Proof.
  induction t in var,x,u|-*; intros; cbn in *.
  - destruct Nat.eq_dec as [-> | d]; cbn in *.
    + right; eauto.
    + now left.
  - specialize IHt with (1 := H) as [IH|IH]; auto.
  - specialize IHt with (1 := H) as [IH|IH]; auto.
  - destruct H as [H|H].
    + specialize IHt1 with (1 := H) as [IH|IH]; auto.
    + specialize IHt2 with (1 := H) as [IH|IH]; auto.
  - destruct H as [H|[H|H]].
    + specialize IHt1 with (1 := H) as [IH|IH]; auto.
    + specialize IHt2 with (1 := H) as [IH|IH]; auto.
    + specialize IHt3 with (1 := H) as [IH|IH]; auto.
  - tauto.
  - specialize IHt with (1 := H) as [IH|IH]; auto.
  - specialize IHt with (1 := H) as [IH|IH]; auto.
Qed.

Lemma free_nsubst_closed var t x u :
  free var (nsubst t x u) ->
  closed u ->
  free var t.
Proof.
  intros H1 H2.
  pose proof free_nsubst _ _ _ _ H1 as [?|(?&?)]; eauto.
  exfalso. eapply free_closed_iff in H2; eauto.
Qed.

(** After substituting the variable [x], it is obviously not free any more! *)
Lemma nsubst_not_free' x t u :
  free x (nsubst t x u) ->
  exists y, free y u.
Proof.
  intros H. induction t in x,H|-*; cbn in *.
  - destruct Nat.eq_dec as [->|d].
    + eauto.
    + cbn in H. congruence.
  - specialize IHt with (1 := H) as (y&IH); eauto.
  - specialize IHt with (1 := H) as (y&IH); eauto.
  - destruct H as [H|H].
    + specialize IHt1 with (1 := H) as (y&IH); eauto.
    + specialize IHt2 with (1 := H) as (y&IH); eauto.
  - destruct H as [H|[H|H]].
    + specialize IHt1 with (1 := H) as (y&IH); eauto.
    + specialize IHt2 with (1 := H) as (y&IH); eauto.
    + specialize IHt3 with (1 := H) as (y&IH); eauto.
  - tauto.
  - specialize IHt with (1 := H) as (y&IH); eauto.
  - specialize IHt with (1 := H) as (y&IH); eauto.
Qed.

Lemma nsubst_not_free x t u :
  closed u -> ~ free x (nsubst t x u).
Proof.
  intros H1 H2. eapply nsubst_not_free' in H2 as (y&H2).
  eapply free_closed_iff; eauto.
Qed.



Lemma typepres_nsubst {ϕ} (m : nat) (Φ : constr ϕ) (x : nat) (Γ Γ' Σ : ctx ϕ) (σ : mty ϕ) t τ v (M N : idx ϕ) :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  hasty Φ Γ M t τ ->
  bound m t ->
  hasty Φ Σ N v σ ->
  val v -> closed v ->
  ctxExtends Φ Γ m x σ Γ' ->
  exists (K : idx ϕ),
    hasty Φ Γ' K (nsubst t x v) τ /\
    sem! Φ ⊨ fun xs => K xs <= M xs + N xs.
Proof.
  intros dec.
  (* [induction ... in ...] doesn't work here. We have to do it manually *)
  intros Hty1 Hbound Hty2 Hval Hvclosed Hext. revert m x Γ' σ v Hbound Hext Σ Hty2 Hval Hvclosed.
  induction Hty1; intros; bound_inv.
  - (* Var *) cbn.
    destruct Nat.eq_dec as [->|d].
    + eexists. split.
      * eapply retype_closed; eauto.
        eapply ty_sub'; eauto.
        -- rewrite <- (proj2 Hext). eauto.
        -- hnf; reflexivity.
      * hnf; lia.
    + eexists. split; eauto.
      * eapply ty_Var with (M0 := iConst 0). etransitivity; eauto.
        hnf in Hext. apply (proj1 Hext); eauto.
      * unfold iConst. hnf; lia.

  - (* Lam *) cbn in Hbound|-*.
    hnf in Hext; destruct Hext as (Hext1&Hext2).
    hnf in Hbound.

    pose proof free_dec x (Lam t) as [Hx|Hx].
    2:{
      rewrite !nsubst_free_no_effect by assumption.
      eexists; split.
      - econstructor; eauto.
        + (* [Γ'0 ⊑ Γ'] *)
          intros var Hvar. etransitivity; eauto.
          assert (x <> var) by congruence.
          eapply Hext1; auto.
          rewrite free_bound_iff in Hbound.
          apply Hbound in Hvar. lia.
      - hnf; lia. }

    assert (ty! Φ; Σ ⊢(N) v : (Γ' x)) as Hty2'.
    { specialize (HΓ x Hx). rewrite Hext2 in HΓ. eapply ty_sub; eauto. reflexivity. hnf; reflexivity. }

    pose proof psplitting dec Hval (Hbsum x Hx) Hty2' as (Ni&Δi&L1&L2&Δ_sum&Hsum&L3); eauto.

    specialize IHHty1 with (2 := Hbound) (4 := L1) (5 := Hval).
    spec_assert IHHty1.
    { intros. apply conj_dec; eauto using lt_dec. }
    specialize IHHty1 with (x0 := S x) (Γ' := σ .: Δ).
    spec_assert IHHty1 as (K_IH&IH1&IH2); eauto.
    { split; reflexivity. }

    eexists. split.
    + eapply ty_Lam.
      * eapply ty_weaken; eauto. reflexivity.
      * hnf. intros var Hvar.
        instantiate (1 := Γ').
        apply Hbsum. cbn in *. cbn in *. eapply free_nsubst_closed; eauto.
      * hnf. intros var Hvar.
        etransitivity.
        -- cbn in *. assert (x <> var).
           { intros ->. eapply nsubst_not_free; eauto. }
           rewrite free_bound_iff in Hbound.
           eapply free_nsubst_closed in Hvar; eauto.
           specialize Hbound with (1 := Hvar).
           eapply Hext1; auto. lia.
        -- apply HΓ. eapply free_nsubst_closed; eauto.
      * instantiate (1 := fun xs => (I xs + Σ_{a < I xs} K_IH (a ::: xs))); cbn zeta beta. hnf; reflexivity.
      * eauto.
    + hnf; intros xs Hxs.
      transitivity (I xs + (Σ_{a < I xs} K (a ::: xs) + Ni (a ::: xs))).
      {
        apply plus_le_compat_l.
        rewrite Sum_monotone'.
        2:{ intros. hnf in IH2. apply IH2. firstorder. }
        now rewrite !Sum_plus.
      }
      {
        rewrite Sum_plus, Nat.add_assoc.
        enough ((Σ_{a < I xs} Ni (a ::: xs)) <= N xs).
        { hnf in HM. specialize (HM xs Hxs). lia. }
        auto.
      }

  - (* Fix *)
    hnf in Hext; destruct Hext as (Hext1&Hext2).
    hnf in Hbound. cbn zeta in *.

    pose proof free_dec x (Fix t) as [Hx|Hx].
    2:{
      rewrite !nsubst_free_no_effect by assumption.
      eexists; split.
      - econstructor; eauto.
        + (* [Γ'0 ⊑ Γ'] *)
          intros var Hvar. etransitivity; eauto.
          assert (x <> var) by congruence.
          eapply Hext1; auto.
          rewrite free_bound_iff in Hbound.
          apply Hbound in Hvar. lia.
      - hnf; lia. }

    assert (ty! Φ; Σ ⊢(N) v : (Γ' x)) as Hty2'.
    { specialize (HΓ x Hx). rewrite Hext2 in HΓ. eapply ty_sub; eauto. reflexivity. hnf; reflexivity. }

    pose proof psplitting dec Hval (Hbsum x Hx) Hty2' as (Ni&Δi&L1&L2&Δ_sum&Hsum&L3); eauto.

    specialize IHHty1 with (2 := Hbound) (4 := L1) (5 := Hval).
    spec_assert IHHty1.
    { intros. apply conj_dec; eauto using lt_dec. }
    specialize IHHty1 with (x0 := S x) (Γ' := [ < Ib]⋅ A .: Δ).
    spec_assert IHHty1 as (K_IH&IH1&IH2); eauto.
    { split; reflexivity. }

    eexists. split.
    + cbn [nsubst]. eapply ty_Fix; eauto.
      * hnf. intros var Hvar.
        instantiate (1 := Γ').
        apply Hbsum. cbn in *. cbn in *. eapply free_nsubst_closed; eauto.
      * hnf. intros var Hvar.
        etransitivity.
        -- cbn in *. assert (x <> var).
           { intros ->. eapply nsubst_not_free; eauto. }
           rewrite free_bound_iff in Hbound.
           eapply free_nsubst_closed in Hvar; eauto.
           specialize Hbound with (1 := Hvar).
           eapply Hext1; auto. lia.
        -- apply HΓ. eapply free_nsubst_closed; eauto.
      * instantiate (1 := fun xs => _); cbn zeta beta. hnf; reflexivity.
    + hnf; intros xs Hxs.

      transitivity (Σ_{a < cardH xs} Jb (a ::: xs) + Ni (a ::: xs)).
      {
        rewrite Sum_monotone'.
        2:{ intros. hnf in IH2. apply IH2. firstorder. }
        now rewrite !Sum_plus.
      }
      { rewrite Sum_plus. hnf in HM; specialize (HM _ Hxs); hnf in L2; specialize (L2 _ Hxs). lia. }

  - (* App *)
    hnf in Hext; destruct Hext as (Hext1&Hext2).
    hnf in HΓ.

    pose proof free_dec x (App t1 t2) as [Hx|Hx].
    2:{
      rewrite !nsubst_free_no_effect by assumption.
      eexists; split.
      - econstructor; eauto.
        { (* [Γ'0 ⊑ Γ'] *)
          intros var Hvar. etransitivity; eauto.
          assert (x <> var) by congruence.
          eapply Hext1; auto.
          rewrite free_bound_iff in Hbound.
          rewrite free_bound_iff in Hbound0.
          cbn in Hvar. destruct Hvar as [Hvar|Hvar].
          - apply Hbound in Hvar; lia.
          - apply Hbound0 in Hvar; lia.
        }
      - hnf; lia. }

    specialize (Hmsum x Hx) as X'.
    specialize (HΓ x Hx) as H'.
    rewrite Hext2 in H'.

    assert (ty! Φ; Σ ⊢(N) v : (Γ' x)) as Hty2'.
    { eapply ty_sub; eauto. reflexivity. hnf; reflexivity. }

    pose proof splitting _ _ dec Hval X' Hty2' as (N1&N2&Σ1&Σ2&Hty21&Hty22&HN&Σ12&HΣ12_sum&HΣ12_sub).

    specialize IHHty1_1 with (1 := dec) (2 := Hbound) (4 := Hty21) (5 := Hval).
    specialize IHHty1_1 with (Γ' := Δ1) (x0 := x).
    spec_assert IHHty1_1.
    { hnf. split; hnf; reflexivity. }
    spec_assert IHHty1_1 as (K_IH1&IH11&IH12) by assumption.

    specialize IHHty1_2 with (1 := dec) (2 := Hbound0) (4 := Hty22) (5 := Hval).
    specialize IHHty1_2 with (Γ' := Δ2) (x0 := x).
    spec_assert IHHty1_2.
    { split; hnf; reflexivity. }
    spec_assert IHHty1_2 as (K_IH2&IH21&IH22) by assumption.

    exists (fun xs => K_IH1 xs + K_IH2 xs). split.
    {
      cbn [nsubst]. eapply ty_App; eauto.
      { instantiate (1 := Γ').
        intros var Hvar. apply Hmsum. eapply free_nsubst_closed; eauto. }
      { (* [Γ'0 ⊑ Γ'] *)
        hnf; intros var [Hvar|Hvar].
        - etransitivity.
          + cbn in *. assert (x <> var).
            { intros ->. eapply nsubst_not_free; eauto. }
            rewrite free_bound_iff in Hbound.
            eapply free_nsubst_closed in Hvar; eauto.
          + apply HΓ. eapply free_nsubst_closed; cbn; eauto.
        - etransitivity.
          + cbn in *. assert (x <> var).
            { intros ->. eapply nsubst_not_free; eauto. }
            rewrite free_bound_iff in Hbound0.
            eapply free_nsubst_closed in Hvar; eauto.
          + apply HΓ. eapply free_nsubst_closed; cbn; eauto.
      }
      { hnf; reflexivity. }
    }
    {
      revert IH12 IH22 HM HN; clear; intros.
      hnf; intros xs Hxs. hnf in IH12; specialize (IH12 _ Hxs). hnf in IH22; specialize (IH22 _ Hxs).
      hnf in HM; specialize (HM _ Hxs). hnf in HN; specialize (HN xs Hxs). lia.
    }

  - (* Ifz *) (* Similar to App *)
    hnf in Hext; destruct Hext as (Hext1&Hext2).
    hnf in HΓ.

    pose proof free_dec x (Ifz t1 t2 t3) as [Hx|Hx].
    2:{
      rewrite !nsubst_free_no_effect by assumption.
      eexists; split.
      - econstructor; eauto.
        { (* [Γ'0 ⊑ Γ'] *)
          intros var Hvar. etransitivity; eauto.
          assert (x <> var) by congruence.
          eapply Hext1; auto.
          rewrite free_bound_iff in Hbound, Hbound0, Hbound1.
          cbn in Hvar. destruct Hvar as [Hvar|[Hvar|Hvar]].
          - apply Hbound in Hvar; lia.
          - apply Hbound0 in Hvar; lia.
          - apply Hbound1 in Hvar; lia.
        }
      - hnf; lia. }

    specialize (Hmsum x Hx) as X'.
    specialize (HΓ x Hx) as H'.
    rewrite Hext2 in H'.

    assert (ty! Φ; Σ ⊢(N) v : (Γ' x)) as Hty2'.
    { eapply ty_sub; eauto. reflexivity. hnf; reflexivity. }

    pose proof splitting _ _ dec Hval X' Hty2' as (N1&N2&Σ1&Σ2&Hty21&Hty22&HN&Σ12&HΣ12_sum&HΣ12_sub).

    specialize IHHty1_1 with (1 := dec) (2 := Hbound) (4 := Hty21) (5 := Hval).
    specialize IHHty1_1 with (Γ' := Δ1) (x0 := x).
    spec_assert IHHty1_1.
    { hnf. split; hnf; reflexivity. }
    spec_assert IHHty1_1 as (K_IH1&IH11&IH12) by assumption.

    specialize IHHty1_2 with (2 := Hbound0) (5 := Hval).
    specialize IHHty1_2 with (Γ' := Δ2) (x := x) (N := N2) (σ := Δ2 x) (Σ := Σ2).
    spec_assert IHHty1_2.
    { intros. apply conj_dec; eauto using Nat.eq_dec. }
    spec_assert IHHty1_2.
    { split.
      - intros. reflexivity.
      - reflexivity.
    }
    spec_assert IHHty1_2 as (K_IH2&IH21&IH22); eauto.
    { eapply ty_weaken_Φ. eapply Hty22. firstorder. }

    specialize IHHty1_3 with (2 := Hbound1) (5 := Hval).
    specialize IHHty1_3 with (Γ' := Δ2) (x := x) (N := N2) (σ := Δ2 x) (Σ := Σ2).
    spec_assert IHHty1_3.
    { intros. apply conj_dec; eauto using le_dec. }
    spec_assert IHHty1_3.
    { split.
      - intros. reflexivity.
      - reflexivity.
    }
    spec_assert IHHty1_3 as (K_IH3&IH31&IH32); eauto.
    { eapply ty_weaken_Φ. eapply Hty22. firstorder. }

    exists (fun xs => K_IH1 xs + if J xs =? 0 then (K_IH2 xs) else (K_IH3 xs)). split.
    {
      cbn [nsubst]. eapply ty_Ifz with (M4 := fun xs => if J xs =? 0 then (K_IH2 xs) else (K_IH3 xs)); eauto.
      { eapply ty_sub; eauto. 1-2: reflexivity.
        hnf; intros xs (Hxs&Hxs2). rewrite (proj2 (Nat.eqb_eq _ _)); auto. }
      { eapply ty_sub; eauto. 1-2: reflexivity.
        hnf; intros xs (Hxs&Hxs2). rewrite (proj2 (Nat.eqb_neq _ _)) by lia; auto. }
      { instantiate (1 := Γ').
        intros var Hvar. apply Hmsum. eapply free_nsubst_closed; eauto. }
      { (* [Γ'0 ⊑ Γ'] *)
        hnf; intros var [Hvar|[Hvar|Hvar]].
        - etransitivity.
          + cbn in *. assert (x <> var).
            { intros ->. eapply nsubst_not_free; eauto. }
            rewrite free_bound_iff in Hbound.
            eapply free_nsubst_closed in Hvar; eauto.
          + apply HΓ. eapply free_nsubst_closed; cbn; eauto.
        - etransitivity.
          + cbn in *. assert (x <> var).
            { intros ->. eapply nsubst_not_free; eauto. }
            rewrite free_bound_iff in Hbound0.
            eapply free_nsubst_closed in Hvar; eauto.
          + apply HΓ. eapply free_nsubst_closed; cbn; eauto.
        - etransitivity.
          + cbn in *. assert (x <> var).
            { intros ->. eapply nsubst_not_free; eauto. }
            rewrite free_bound_iff in Hbound1.
            eapply free_nsubst_closed in Hvar; eauto.
          + apply HΓ. eapply free_nsubst_closed; cbn; eauto.
      }
      { hnf; reflexivity. }
    }
    {
      revert IH12 IH22 IH32 HM HN; clear; intros.
      hnf; intros xs Hxs. hnf in HM,HN,IH12,IH22,IH32; specialize (IH12 _ Hxs).
      specialize (HM _ Hxs). specialize (HN xs Hxs).

      assert (J xs = 0 \/ 1 <= J xs) as [Hdec|Hdec] by omega.
      - specialize (IH22 xs (conj Hdec Hxs)). rewrite (proj2 (Nat.eqb_eq _ _)) by assumption. lia.
      - specialize (IH32 xs (conj Hdec Hxs)). rewrite (proj2 (Nat.eqb_neq _ _)); lia.
    }

  - (* Const *) eexists. split. apply ty_Const with (M0 := iConst 0); auto. unfold iConst; hnf; lia.
  - (* Succ *)
    pose proof free_dec x (Succ t) as [Hx|Hx].
    2:{
      rewrite !nsubst_free_no_effect by assumption.
      eexists; split.
      - econstructor; eauto.
        eapply ty_weaken; eauto.
        { (* [Γ'0 ⊑ Γ'] *)
          intros var Hvar. cbn in *.
          assert (x <> var) by congruence.
          apply (proj1 Hext); eauto.
          eapply free_bound_iff; eauto.
        }
      - hnf; lia. }
    specialize IHHty1 with (1 := dec) (2 := Hbound) (4 := Hty2) (5 := Hval) (6 := Hvclosed).
    specialize IHHty1 with (1 := Hext) as (K_IH&IH1&IH2).
    eexists; split.
    + cbn. eapply ty_Succ; eauto.
    + auto.
  - (* Pred *)
    pose proof free_dec x (Pred t) as [Hx|Hx].
    2:{
      rewrite !nsubst_free_no_effect by assumption.
      eexists; split.
      - econstructor; eauto.
        eapply ty_weaken; eauto.
        { (* [Γ'0 ⊑ Γ'] *)
          intros var Hvar. cbn in *.
          assert (x <> var) by congruence.
          apply (proj1 Hext); eauto.
          eapply free_bound_iff; eauto.
        }
      - hnf; lia. }
    specialize IHHty1 with (1 := dec) (2 := Hbound) (4 := Hty2) (5 := Hval) (6 := Hvclosed).
    specialize IHHty1 with (1 := Hext) as (K_IH&IH1&IH2).
    eexists; split.
    + cbn. eapply ty_Pred; eauto.
    + auto.
Qed.


Lemma typepres_beta1 {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M N : idx ϕ) (τ1 τ2 : mty ϕ) (t : tm) (v : tm) :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  hasty Φ (τ2 .: Γ) M t τ1 ->
  bound 1 t ->
  val v -> closed v ->
  hasty Φ Γ N v τ2 ->
  exists (K : idx ϕ),
    hasty Φ Γ K (nbeta1 t v) τ1 /\
    sem! Φ ⊨ fun xs => K xs <= M xs + N xs.
Proof.
  intros.
  eapply typepres_nsubst; eauto.
  - hnf. split.
    + intros. omega.
    + cbn. reflexivity.
Qed.


Require Import PCF_Semantics.


Lemma preservation_closed_nbeta1 t1 t2 :
  closed (Lam t1) ->
  closed t2 -> val t2 ->
  closed (nbeta1 t1 t2).
Proof.
  intros. eapply preservation_closed.
  2:{ exists β. now constructor. }
  cbn; eauto.
Qed.


(* TODO: Move to Contexts *)
Lemma subst_ctx_beta_ground_scons {ϕ} (I: idx ϕ) (τ : mty (S ϕ)) (Γ : ctx (S ϕ)) :
  subst_ctx_beta_ground I (τ .: Γ) =
  ((subst_mty_beta_ground τ I) .: (subst_ctx_beta_ground I Γ)).
Proof. fext; intros [ | ]; cbn; auto. Qed.

(* TODO: Move to Typing *)

Lemma ty_App_inv {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (t1 t2 : tm) (ρ : mty ϕ) :
  (ty! Φ; Γ ⊢(M) t1 t2 : ρ) ->
  exists (Δ1 Δ2 : ctx ϕ) (σ τ : mty (S ϕ)) (K1 K2 : idx ϕ) (Γ' : ctx ϕ),
    (ty! Φ; Δ1 ⊢(K1) t1 : [<iConst 1] ⋅ (σ ⊸ τ)) /\
    (ty! Φ; Δ2 ⊢(K2) t2 : (subst_mty_beta_ground σ (iConst 0))) /\
    ctxMSum (App t1 t2) Δ1 Δ2 Γ' /\
    (ctx! (App t1 t2); Φ ⊢ Γ ⊑ Γ') /\
    (sem! Φ ⊨ fun xs => (K1 xs + K2 xs) <= M xs) /\
    (mty! Φ ⊢ subst_mty_beta_ground τ (iConst 0) ⊑ ρ).
Proof. inversion 1; eauto 13. Qed.


Lemma preservation_App_Lam {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) M t1 t2 ρ :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  hasty Φ Γ M (App (Lam t1) t2) ρ ->
  closed (App (Lam t1) t2) -> val t2 ->
  exists M', hasty Φ Γ M' (nbeta1 t1 t2) ρ /\
        sem! Φ ⊨ fun xs => M' xs < M xs.
Proof.
  intros dec Hty Hclos Hval.
  cbn in Hclos; destruct Hclos as [Hclos1 Hclos2].

  (* Inversions (and clear all context hypotheses (because our terms are closed) *)
  apply ty_App_inv in Hty as (Δ1 & Δ2 & σ & τ & K1 & K2 & Γ' & Hty1 & Hty2 & Hmsum & Hctx & HM & Hρ). clear Hctx Hmsum.
  apply ty_Lam_inv in Hty1 as (I&Δa&Δ&Ka&σa&τa&Hbsum&Hστ&Hty1&Hctx&HK1). clear Hbsum Hctx.

  apply submty_Quant_inv in Hστ as (Hστ&HI). apply sublty_arr_inv in Hστ as (Hτ&Hσ). unfold iConst in HI,Hσ,Hτ.

  assert (ty! Φ; subst_mty_beta_ground σa (iConst 0) .: subst_ctx_beta_ground (iConst 0) Δa
              ⊢(subst_beta_ground_fun (iConst 0) Ka) t1 : subst_mty_beta_ground τa (iConst 0)) as L1.
  {
    eapply ty_subst_beta_ground with (I0 := iConst 0) in Hty1; unfold subst_beta_ground_fun in Hty1; cbn in Hty1.
    eapply ty_sub. eapply ty_weaken_Φ.
    - eassumption.
    - unfold iConst. firstorder omega.
    - rewrite subst_ctx_beta_ground_scons. reflexivity.
    - reflexivity.
    - hnf; reflexivity.
  }

  unshelve epose proof typepres_beta1 dec L1 ltac:(eassumption) ltac:(eassumption) ltac:(eassumption) _ as (K'&L2&L3).
  2:{
    instantiate (1 := K2). eapply ty_sub.
    - eassumption.
    - hnf; intros var Hvar; exfalso. contradict Hvar. eapply free_closed_iff; eauto.
    - eapply submty_mono_Φ.
      + eapply submty_subst_beta_ground; eauto.
      + firstorder.
    - hnf; reflexivity.
  }
  eexists. split.
  { eapply ty_sub.
    * apply L2.
    * hnf; intros var Hvar; exfalso.
      contradict Hvar. eapply free_closed_iff; eauto. apply preservation_closed_nbeta1; eauto.
    * etransitivity; eauto.
      eapply submty_mono_Φ. eapply submty_subst_beta_ground; eauto. firstorder.
    * cbn. apply L3.
  }
  {
    hnf; intros xs Hxs.
    unfold subst_beta_ground_fun.
    unfold iConst. eapply Nat.lt_le_trans. 2: apply HM; auto.
    enough (Ka (0 ::: xs) < K1 xs) by lia.
    hnf in HK1. eapply Nat.lt_le_trans. 2: apply HK1; auto.
    enough (Ka (0 ::: xs) <= (Σ_{a < I xs} Ka (a ::: xs))).
    { hnf in HI. specialize (HI xs Hxs). unfold iConst in HI. lia. }
    rewrite <- Sum_monotone with (I1 := 1). reflexivity. firstorder.
  }
Qed.


(** Uniformising subtyping judgments *)

(* TODO: Move *)


Lemma list_eta (X : Type) (xs : list X) (def : X) :
  xs <> List.nil ->
  List.hd def xs :: List.tl xs = xs.
Proof.
  destruct xs.
  - congruence.
  - intros _. cbn. reflexivity.
Qed.

Lemma vect_split (X : Type) (n : nat) (x : Fin.t (S n)) (xs : Vector.t X (S n)) (def : X) :
  { ys : Vector.t X n & { y : X |
    vect_to_list xs = firstn (fin_to_nat x) (vect_to_list ys) ++ y :: skipn (fin_to_nat x) (vect_to_list ys) }}.
Proof.
  eexists ?[tamtam], ?[tam].
  instantiate (tamtam := vect_cast (vect_app (vect_take (fin_to_nat x) _ (vect_cast xs _)) ?[rest]) _); vect_to_list.
  rewrite firstn_app.
  rewrite firstn_firstn, Min.min_idempotent.
  replace (fin_to_nat x - length (firstn (fin_to_nat x) (vect_to_list xs))) with 0.
  2:{ rewrite firstn_length, vect_to_list_length. pose proof fin_to_nat_lt x. lia. }
  cbn. rewrite app_nil_r.
  rewrite !skipn_app'.
  2:{ rewrite firstn_length, vect_to_list_length. pose proof fin_to_nat_lt x. lia. }
  cbn. instantiate (rest := vect_skip (S (fin_to_nat x)) _ (vect_cast xs _)); vect_to_list.
  instantiate (tam := List.hd def (skipn (fin_to_nat x) (vect_to_list xs))).
  setoid_rewrite <- firstn_skipn at 1.
  f_equal. rewrite skipn_tl.
  rewrite list_eta; auto.
  intros H % length_zero_iff_nil. rewrite skipn_length, vect_to_list_length in H. pose proof fin_to_nat_lt x. lia.
  Unshelve.
  3:{ instantiate (1 := ?[x]). instantiate (x := S (n - fin_to_nat x)). abstract (pose proof fin_to_nat_lt x; lia). }
  3:{ instantiate (1 := ?[x]). instantiate (x := n - fin_to_nat x). abstract (pose proof fin_to_nat_lt x; lia). }
  1:{ abstract (pose proof fin_to_nat_lt x; lia). }
Qed.


Lemma p_equal2 (X Y : Type) (f : X -> Y -> Prop) (x1 x2 : X) (y1 y2 : Y) : f x1 y1 -> x1 = x2 -> y1 = y2 -> f x2 y2.
Proof. congruence. Qed.


(** Here we use the same trick as in [sublty_exfalso]. *)

Lemma sublty_submty_var_unif {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (i : nat) :
  (forall (A B : lty (S ϕ)),
      size_lty A + size_lty B <= i ->
      (forall n, lty! subst_var_beta_ground_fun x (iConst n) Φ ⊢ subst_lty_var_beta_ground x (iConst n) A ⊑
            subst_lty_var_beta_ground x (iConst n) B) ->
      (lty! Φ ⊢ A ⊑ B)) /\
  (forall (σ τ : mty (S ϕ)),
      size_mty σ + size_mty τ <= i ->
      (forall n, mty! subst_var_beta_ground_fun x (iConst n) Φ ⊢ subst_mty_var_beta_ground x (iConst n) σ ⊑
            subst_mty_var_beta_ground x (iConst n) τ) ->
      (mty! Φ ⊢ σ ⊑ τ)).
Proof.
  revert dependent ϕ.
  induction i as [i IHi] using lt_wf_ind; intros.
  split; intros * Hi.
  - destruct A, B; intros H; cbn in *.
    constructor; eapply IHi with (x := x); eauto; try lia.
    + intros n. specialize (H n). apply sublty_arr_inv in H as (H1&H2). eauto.
    + intros n. specialize (H n). apply sublty_arr_inv in H as (H1&H2). eauto.
  - destruct σ, τ; intros H.
    + cbn in *. apply submty_Nat. hnf; intros xs Hxs.
      pose proof vect_split x xs 0 as (tamtam&tam&Htam).
      specialize (H tam). apply submty_Nat_inv in H. unfold iConst, subst_var_beta_ground_fun in H; hnf in H.
      specialize (H tamtam); spec_assert H.
      { clear H. eapply p_equal; eauto. simp_vect_to_list. auto. }
      eapply p_equal2; eauto.
      * clear H. f_equal. simp_vect_to_list. auto.
      * clear H. f_equal. simp_vect_to_list. auto.
    + cbn in *. specialize (H 0). apply submty_Nat_inv' in H as (k'&[=]&?).
    + cbn in *. specialize (H 0). apply submty_Quant_inv' in H as (k'& ? & ? & ? & [=]).
    + cbn in Hi. constructor.
      * eapply IHi with (x := FS x); eauto. lia.
        intros n. specialize (H n).
        rewrite !subst_mty_var_beta_ground_eq_Quant in H. eapply submty_Quant_inv in H as (H1&H2).
        unfold subst_lty_var_beta_ground in *. rewrite !subst_var_beta_ground_fun_FS in *. eauto.
      * hnf; intros xs Hxs.
        pose proof vect_split x xs 0 as (tamtam&tam&Htam).
        specialize (H tam). apply submty_Quant_inv in H as (H1&H2). unfold iConst, subst_var_beta_ground_fun in H2; hnf in H2.
        specialize (H2 tamtam); spec_assert H2.
        { clear H2. eapply p_equal; eauto. simp_vect_to_list. auto. }
        eapply p_equal2; eauto.
        -- clear H2. f_equal. simp_vect_to_list. auto.
        -- clear H2. f_equal. simp_vect_to_list. auto.
Qed.

Lemma sublty_var_unif {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (A B : lty (S ϕ)) :
  (forall n, lty! subst_var_beta_ground_fun x (iConst n) Φ ⊢ subst_lty_var_beta_ground x (iConst n) A ⊑
        subst_lty_var_beta_ground x (iConst n) B) ->
  (lty! Φ ⊢ A ⊑ B).
Proof. eapply sublty_submty_var_unif; eauto. Qed.
Lemma submty_var_unif {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (σ τ : mty (S ϕ)) :
  (forall n, mty! subst_var_beta_ground_fun x (iConst n) Φ ⊢ subst_mty_var_beta_ground x (iConst n) σ ⊑
        subst_mty_var_beta_ground x (iConst n) τ) ->
  (mty! Φ ⊢ σ ⊑ τ).
Proof. eapply sublty_submty_var_unif; eauto. Qed.



Lemma sublty_unif {ϕ} (Φ : constr (S ϕ)) (A B : lty (S ϕ)) :
  (forall n, lty! subst_beta_ground_fun (iConst n) Φ ⊢ subst_lty_beta_ground A (iConst n) ⊑ subst_lty_beta_ground B (iConst n)) ->
  (lty! Φ ⊢ A ⊑ B).
Proof.
  intros H. eapply sublty_var_unif with (x := Fin0).
  intros. unfold subst_lty_var_beta_ground. rewrite !subst_var_beta_ground_fun_Fin0. eauto.
Qed.

Lemma submty_unif {ϕ} (Φ : constr (S ϕ)) (σ τ : mty (S ϕ)) :
  (forall n, mty! subst_beta_ground_fun (iConst n) Φ ⊢ subst_mty_beta_ground σ (iConst n) ⊑ subst_mty_beta_ground τ (iConst n)) ->
  (mty! Φ ⊢ σ ⊑ τ).
Proof.
  intros H. eapply submty_var_unif with (x := Fin0).
  intros. unfold subst_mty_var_beta_ground. rewrite !subst_var_beta_ground_fun_Fin0. eauto.
Qed.

Lemma sublty_lt_one {ϕ} (Φ : constr ϕ) (A B : lty (S ϕ)) :
  (lty! Φ ⊢ subst_lty_beta_ground A (iConst 0) ⊑ subst_lty_beta_ground B (iConst 0)) ->
  lty! (fun xs => hd xs < 1 /\ Φ (tl xs)) ⊢ A ⊑ B.
Proof.
  intros.
  eapply sublty_unif.
  intros n.
  etransitivity. 2: etransitivity.
  2:{ eapply sublty_mono_Φ; eauto. unfold subst_beta_ground_fun, iConst; cbn. firstorder. }
  all: unfold subst_lty_beta_ground, subst_beta_ground_fun, iConst;
    eapply sublty_subst_idx_sem; hnf; intros xs (Hxs1&Hxs2); intros j; repeat f_equal; clear j; cbn in *; lia.
Qed.

Lemma submty_lt_one {ϕ} (Φ : constr ϕ) (A B : mty (S ϕ)) :
  (mty! Φ ⊢ subst_mty_beta_ground A (iConst 0) ⊑ subst_mty_beta_ground B (iConst 0)) ->
  mty! (fun xs => hd xs < 1 /\ Φ (tl xs)) ⊢ A ⊑ B.
Proof.
  intros.
  eapply submty_unif.
  intros n.
  etransitivity. 2: etransitivity.
  2:{ eapply submty_mono_Φ; eauto. unfold subst_beta_ground_fun, iConst; cbn. firstorder. }
  all: unfold subst_mty_beta_ground, subst_beta_ground_fun, iConst;
    eapply submty_subst_idx_sem; hnf; intros xs (Hxs1&Hxs2); intros j; repeat f_equal; clear j; cbn in *; lia.
Qed.


Lemma idx_forestCard_split_children {ϕ} (Φ : constr ϕ) (K : idx (S ϕ)) (H : idx ϕ) :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  (sem! Φ ⊨ fun xs => isForestCard (fun a => K (a ::: xs)) 1 (H xs)) ->
  { H' : idx (S ϕ) |
    (sem! Φ ⊨ fun xs => 1 + (Σ_{d < K (0 ::: xs)} H' (d ::: xs)) = H xs) /\
    (sem! Φ ⊨ fun xs => isForestCard (fun a : nat => K (S a ::: xs)) (K (0 ::: xs)) (Σ_{d < K (0 ::: xs)} H' (d ::: xs))) /\
    (sem! fun xs => hd xs < K (0 ::: tl xs) /\ Φ (tl xs) ⊨
          fun xs => isForestCard (fun a => K (S a ::: tl xs)) (hd xs) (Σ_{d < hd xs} H' (d ::: tl xs))) /\
    (sem! fun xs => hd xs < K (0 ::: tl xs) /\ Φ (tl xs) ⊨
          fun xs => isForestCard (fun a => K (S ((Σ_{d < hd xs} H' (d ::: tl xs)) + a) ::: tl xs)) 1 (H' (hd xs ::: tl xs)))
  }.
Proof.
  intros dec Hsem.
  unshelve eexists (create_idx_sem _ _ (iConst 0)).
  3:{ intros xs Hxs.
      pose proof isForestCard_split_children (Hsem (tl xs) Hxs) as (H'&HH).
      apply H'. apply (hd xs). }
  1:{ cbn. intros xs. apply dec. }
  repeat_split.
  - cbn. intros xs Hxs. unfold create_idx_sem. cbn. destruct (dec xs) as [Hdec|?]; try tauto.
    destruct isForestCard_split_children as (H'&HH1&HH2&HH3&HH4); auto.
  - cbn. intros xs Hxs. unfold create_idx_sem. cbn. destruct (dec xs) as [Hdec|?]; try tauto.
    destruct isForestCard_split_children as (H'&HH1&HH2&HH3&HH4); auto.
  - cbn. intros xs (Hxs1&Hxs2). unfold create_idx_sem. cbn. destruct (dec (tl xs)) as [Hdec|?]; try tauto.
    destruct isForestCard_split_children as (H'&HH1&HH2&HH3&HH4); auto.
  - cbn. intros xs (Hxs1&Hxs2). unfold create_idx_sem. cbn. destruct (dec (tl xs)) as [Hdec|?]; try tauto.
    destruct isForestCard_split_children as (H'&HH1&HH2&HH3&HH4); auto.
Qed.


(* TODO: Move *)
Lemma sublty_subst_beta_one_ground {ϕ} (Φ : constr (S (S ϕ))) (A B : lty (S (S ϕ))) (i : idx ϕ) :
  (lty! Φ ⊢ A ⊑ B) ->
  (lty! subst_beta_one_ground_fun i Φ ⊢ subst_lty_beta_one_ground A i ⊑ subst_lty_beta_one_ground B i).
Proof.
  intros H.
  rewrite !subst_lty_beta_one_ground_correct.
  eapply sublty_mono_Φ.
  - apply sublty_subst_var_beta with (x := Fin1) (y := 0); eauto.
  - erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0; cbn.
    hnf; intros xs Hxs. unfold subst_beta_one_ground_fun, subst_beta_fun in *.
    eapply p_equal; eauto.
    f_equal.
    + now rewrite vect_cast_hd.
    + f_equal.
      * now rewrite vect_cast_id.
      * cbn. now rewrite vect_cast_id.
        Unshelve. reflexivity.
Qed.

(* TODO: Move *)
Lemma submty_subst_beta_one_ground {ϕ} (Φ : constr (S (S ϕ))) (σ τ : mty (S (S ϕ))) (i : idx ϕ) :
  (mty! Φ ⊢ σ ⊑ τ) ->
  (mty! subst_beta_one_ground_fun i Φ ⊢ subst_mty_beta_one_ground σ i ⊑ subst_mty_beta_one_ground τ i).
Proof.
  intros H.
  rewrite !subst_mty_beta_one_ground_correct.
  eapply submty_mono_Φ.
  - apply submty_subst_var_beta with (x := Fin1) (y := 0); eauto.
  - erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0; cbn.
    hnf; intros xs Hxs. unfold subst_beta_one_ground_fun, subst_beta_fun in *.
    eapply p_equal; eauto.
    f_equal.
    + now rewrite vect_cast_hd.
    + f_equal.
      * now rewrite vect_cast_id.
      * cbn. now rewrite vect_cast_id.
        Unshelve. reflexivity.
Qed.




Lemma preservation_App_Fix {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) M t1 t2 ρ :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  hasty Φ Γ M (App (Fix t1) t2) ρ ->
  closed (App (Fix t1) t2) -> val t2 ->
  exists M', hasty Φ Γ M' (nbeta2 t1 t2 (Fix t1)) ρ /\
        sem! Φ ⊨ fun xs => M' xs < M xs.
Proof.
  intros dec Hty Hclos Hval.
  cbn in Hclos; destruct Hclos as [Hclos1 Hclos2].

  (* Reduction to the previous case: We only need to consider the substitution of the fixpoint *)
  enough (hasty Φ Γ M (App (Lam (nsubst t1 1 (Fix t1))) t2) ρ) as H.
  {
    apply preservation_App_Lam in H as (M'&H1&H2); cbn; eauto.
    split; auto. apply nsubst_bound'; auto.
  }

  (* Inversion of the application typing *)
  apply ty_App_inv in Hty as (Δ1 & Δ2 & σ & τ & K1 & K2 & Γ' & Hty1 & Hty2 & Hmsum & Hctx & HM & Hρ); clear Hctx Hmsum.

  (* Subtyping *)
  eapply ty_sub'. 2: exact Hρ. 2: exact HM. clear ρ Hρ M HM.

  (* We only need to type the function and can forget about the application (as it was already handled by in the previous case) *)
  enough (hasty Φ Γ K1 (Lam (nsubst t1 1 (Fix t1))) ([ < iConst 1]⋅ σ ⊸ τ)) as H.
  {
    eapply ty_App; eauto. (* only boring goals left... *)
    * hnf; intros var Hvar; exfalso.
      contradict Hvar. eapply free_closed_iff; eauto. cbn. split; auto. eapply nsubst_bound'; eauto.
    * reflexivity.
    * hnf; reflexivity.
    * reflexivity.
  }
  clear dependent t2; clear dependent K2. rename t1 into t, K1 into M, Hty1 into Hty, Hclos1 into Hclos.

  (* Now, we invert the typing of the fixpoint *)
  inv Hty; cbn zeta in *. clear Hbsum HΓ. rename Γ'0 into Γ'', Hty0 into Hty.

  (* Inversion and simplification of subtyping *)
  apply submty_Quant_inv in Hρ as (Hστ&HK). unfold iConst in HK, Hστ.
  (* Instantiate [0 < 1] in [Hστ] *)
  assert
    (lty! Φ ⊢
        subst_lty B (fun (f : idx (S (S ϕ))) (xs : Vector.t nat ϕ) => f (0 ::: card2 (0 ::: xs) ::: xs)) ⊑
        subst_lty_beta_ground (σ ⊸ τ) (iConst 0)) as Hστ'.
  { replace (subst_lty B (fun (f : idx (S (S ϕ))) (xs : Vector.t nat ϕ) => f (0 ::: card2 (0 ::: xs) ::: xs))) with
        (subst_lty_beta_ground (subst_lty B (fun (f : idx (S (S ϕ))) (xs : Vector.t nat (S ϕ)) => f (0 ::: card2 xs ::: tl xs)))
                               (iConst 0)) by (cbn; now setoid_rewrite subst_lty_twice).
    eapply sublty_mono_Φ.
    - apply sublty_subst_beta_ground; eauto.
    - unfold subst_beta_ground_fun; cbn. firstorder. }
  (* We can simplify [card2 (0 ::: xs)] to 0 *)
  assert
    (lty! Φ ⊢
        subst_lty B (fun (f : idx (S (S ϕ))) (xs : Vector.t nat ϕ) => f (0 ::: 0 ::: xs)) ⊑
        subst_lty_beta_ground (σ ⊸ τ) (iConst 0)) as Hστ''.
  { etransitivity; eauto. eapply sublty_subst_idx_sem. hnf; intros xs Hxs. intros j; repeat f_equal; clear j.
    hnf in Hcard2. specialize (Hcard2 (0 ::: xs)). spec_assert Hcard2 by (cbn; split; auto).
    destruct Hcard2 as (fuel&Hcard2). rewrite forestCard_eq_0 in Hcard2. congruence. }
  replace (subst_lty B (fun (f : idx (S (S ϕ))) (xs : Vector.t nat ϕ) => f (0 ::: 0 ::: xs))) with
      (subst_lty_beta_ground (subst_lty_beta_ground B (iConst 0)) (iConst 0)) in Hστ''.
  2:{ setoid_rewrite subst_lty_twice; unfold funcomp; cbn. reflexivity. }
  clear Hστ Hστ'; rename Hστ'' into Hστ.
  (* Do some computation *)
  change (subst_lty_beta_ground (σ ⊸ τ) (iConst 0)) with
      (subst_mty_beta_ground σ (iConst 0) ⊸ subst_mty_beta_ground τ (iConst 0)) in Hστ.
  destruct B as [σ' τ'].
  do 2 change (subst_lty_beta_ground (?σ' ⊸ ?τ') ?f) with (subst_mty_beta_ground σ' f ⊸ subst_mty_beta_ground τ' f) in *.
  apply sublty_arr_inv in Hστ as (Hτ&Hσ); move Hσ after Hτ.


  assert (sem! Φ ⊨ fun xs => 0 < cardH xs) as HcardH_le1.
  { hnf; intros xs Hxs. unfold iConst. enough (1 <= cardH xs) by lia. eapply isForestCard_ge1; eauto. }

  (* Instantiate [Hty] with [0 < H] *)
  assert (ty! Φ; ([ < subst_beta_ground_fun (iConst 0) Ib] ⋅ subst_lty_beta_one_ground A (iConst 0)) .: (subst_ctx_beta_ground (iConst 0) Δ)
          ⊢(subst_beta_ground_fun (iConst 0) Jb) Lam t : [ < iConst 1] ⋅ subst_mty_beta_one_ground σ' (iConst 0) ⊸ subst_mty_beta_one_ground τ' (iConst 0)) as Hty_0.
  { eapply ty_subst_beta_ground with (I := iConst 0) in Hty.
    unfold subst_beta_ground_fun in *; cbn in Hty; unfold subst_beta_ground_fun in *; cbn in *.
    eapply ty_weaken_Φ. eapply ty_sub; eauto.
    - rewrite subst_ctx_beta_ground_scons. reflexivity.
    - unfold iConst. reflexivity.
    - unfold iConst. hnf; reflexivity.
    - firstorder. }


  (* Construction of the new main forest cardinality *)

  (* First we reduce the main forst card to the first tree (we can do so by splitting with [HK : Φ ⊨ 1 <= K]) *)
  assert ({ cardH' |
            (sem! Φ ⊨ fun xs => isForestCard (fun b : nat => Ib (b ::: xs)) 1 (cardH' xs)) /\
            (sem! Φ ⊨ fun xs => cardH' xs <= cardH xs)
         }) as (cardH' & HcardH'1 & HcardH'2).
  { pose proof idx_forestCard_split_le _ _ _ _ dec HcardH HK as (cardH'1 & cardH'2 & HcardH'1 & HcardH'2 & HcardH'12).
    exists cardH'1. split; eauto. hnf; intros xs Hxs. rewrite HcardH'12. lia. }

  (* Now we compute the term that describes the first [a < cardH] children of the root *)
  epose proof idx_forestCard_split_children _ _ dec HcardH'1 as (cardH'' & HcardH''1 & HcardH''2 & HcardH''3 & HcardH''4).
  (* This is equal to [cardH - 1] (because we 'chopped off' the root) *)
  set (cardHstar := fun xs => Σ_{d < Ib (0 ::: xs)} cardH'' (d ::: xs)).
  change (sem! Φ ⊨ (fun xs : Vector.t nat ϕ => 1 + cardHstar xs = cardH' xs)) in HcardH''1.

  (* Subtyping in [Hty_0] *)
  eapply ty_sub' in Hty_0. 3: hnf; reflexivity.
  2:{ apply submty_Quant. 2: hnf; reflexivity.
      instantiate (1 := σ ⊸ τ). apply sublty_lt_one.
      cbn. setoid_rewrite subst_mty_twice. setoid_rewrite subst_mty_twice in Hσ.
      setoid_rewrite subst_mty_twice in Hτ. constructor; eauto. }

  (* The cost of the retyped Fix *)
  set (Mstar := fun xs => Σ_{b < cardHstar xs} Jb (S b ::: xs)).

  (* With substitution, it remains to show that we can re-type the [Fix] *)
  enough (ty! Φ; subst_ctx_beta_ground (iConst 0) Δ ⊢(Mstar)
              Fix t : [ < subst_beta_ground_fun (iConst 0) Ib]⋅ subst_lty_beta_one_ground A (iConst 0)) as L.
  {
    change (Lam (nsubst t 1 (Fix t))) with (nbeta1 (Lam t) (Fix t)).
    unshelve epose proof typepres_beta1 dec Hty_0 Hclos (val_fix _) Hclos L as (M'&L1&L2).
    eapply ty_sub; eauto. 2: reflexivity.
    - hnf; intros var Hvar; exfalso.
      contradict Hvar. eapply free_closed_iff; eauto. cbn. eapply nsubst_bound'; eauto.
    - hnf; intros xs Hxs.
      enough (subst_beta_ground_fun (iConst 0) Jb xs + Mstar xs <= Σ_{b < cardH xs} Jb (b ::: xs)).
      { hnf in HM, L2; specialize (HM _ Hxs); specialize (L2 _ Hxs). lia. }
      unfold subst_beta_ground_fun, iConst; cbn.
      unfold Mstar.
      hnf in HcardH''1; specialize (HcardH''1 _ Hxs). hnf in HcardH'2; specialize (HcardH'2 _ Hxs).
      transitivity (Σ_{b < S (cardHstar xs)} Jb (b ::: xs)).
      2:{ apply Sum_monotone. lia. }
      rewrite Sum_eq_S'. reflexivity.
  }

  (* Now we have everything needed to apply the typing rule *)
  eapply ty_Fix with
      (M0 := Mstar) (* [M] is already fixed *)
      (K0 := fun xs => Ib (0 ::: xs)) (* number of children of the root *)
      (Ib0 := fun xs => Ib (S (hd xs) ::: tl xs)) (* Skip the root *)
      (cardH0 := cardHstar)
      (card3 := fun xs => card1 (hd xs ::: S (hd (tl xs)) ::: tl (tl xs))) (* first aux card *)
      (card4 := fun xs => Σ_{d < hd xs} cardH'' (d ::: tl xs)) (* second aux card *)
      (A0 := lty_shift_one_add A (iConst 1)) (* In [A] and [B], we also just skip the root *)
      (B := lty_shift_one_add (σ' ⊸ τ') (iConst 1))
      (Jb0 := shift_add_fun (iConst 1) Jb);
    cbn zeta.
  - (* Typing *) apply ty_shift_add with (I := iConst 1) in Hty as Hty_1.
    eapply ty_sub. eapply ty_weaken_Φ.
    + apply Hty_1.
    + hnf; intros xs (Hxs1&Hxs2); split; auto. eapply Nat.lt_le_trans; eauto.
      etransitivity. 2: apply HcardH'2; auto. hnf in HcardH''1. rewrite <- HcardH''1; auto. cbn. lia.
    + hnf; intros var Hvar. rewrite ctx_shift_add_scons.
      cbn in Hvar. assert (var = 0) as ->.
      { destruct var; auto. contradict Hvar. intros H'. rewrite free_bound_iff in Hclos. specialize Hclos with (1 := H'). lia. }
      cbn [scons]. rewrite mty_shift_add_eq_Quant'. reflexivity.
    + rewrite mty_shift_add_eq_Quant'. reflexivity.
    + hnf; reflexivity.
  - (* first subtyping *)
    change (lty_shift_one_add (σ' ⊸ τ') (iConst 1)) with (mty_shift_one_add σ' (iConst 1) ⊸ mty_shift_one_add τ' (iConst 1)).
    etransitivity.
    2:{ eapply sublty_mono_Φ. eapply sublty_shift_one_add; eauto. cbn.
        hnf; intros xs (Hxs1&Hxs2&Hxs3); repeat_split; auto.
        eapply Nat.lt_le_trans; eauto.
        etransitivity. 2: apply HcardH'2; auto. hnf in HcardH''1. rewrite <- HcardH''1; auto. cbn. lia. }
    cbn. setoid_rewrite subst_mty_twice; unfold funcomp, shift_one_add_fun; cbn.
    { eapply sublty_congr. f_equal.
      - f_equal. fext; intros f xs. (do 4 f_equal). lia.
      - f_equal. fext; intros f xs. (do 4 f_equal). lia. }
  - (* main card *) cbn. assumption.
  - (* first aux card *) cbn.
    hnf; intros xs (Hxs1&Hxs2&Hxs3).
    hnf in Hcard1; apply (Hcard1 (hd xs ::: S (hd (tl xs)) ::: tl (tl xs))).
    cbn. repeat_split; cbn; auto.
    unfold cardHstar in Hxs2. eapply Nat.lt_le_trans. 2: now apply HcardH'2.
    enough (S (Σ_{d < Ib (0 ::: tl (tl xs))} cardH'' (d ::: tl (tl xs))) <= cardH' (tl (tl xs))) by lia.
    now rewrite <- HcardH''1 by auto.
  - (* second aux card *) cbn; assumption.
  - (* ctx nonsense *) intros var Hvar. contradict Hvar. eapply free_closed_iff; eauto.
  - (* ctx nonsense *) intros var Hvar. contradict Hvar. eapply free_closed_iff; eauto.
  - (* [Mstar] *) hnf; reflexivity.
  - (* Final subtyping *)
    change (lty_shift_one_add (σ' ⊸ τ') (iConst 1)) with (mty_shift_one_add σ' (iConst 1) ⊸ mty_shift_one_add τ' (iConst 1)).
    eapply submty_Quant. 2: hnf; reflexivity.
    etransitivity.
    2:{ eapply sublty_mono_Φ. apply sublty_subst_beta_one_ground; eauto.
        unfold subst_beta_one_ground_fun; cbn. firstorder. }
    {
      cbn. setoid_rewrite subst_mty_twice; unfold funcomp, subst_beta_one_ground_fun, shift_one_add_fun, iConst; cbn.
      eapply sublty_arr.
      - eapply submty_subst_idx_sem.
        hnf; intros xs (Hxs1&Hxs2); cbn; intros j; repeat f_equal; clear j.
        rewrite Nat.add_0_r.
        eapply isForestCard_functional; eauto; cbn. now apply HcardH''3.
      - eapply submty_subst_idx_sem.
        hnf; intros xs (Hxs1&Hxs2); cbn; intros j; repeat f_equal; clear j.
        rewrite Nat.add_0_r.
        eapply isForestCard_functional; eauto; cbn.
        hnf in Hcard1. eapply (Hcard1 (hd xs ::: 0 ::: tl xs)); cbn; eauto.
    }
  (* Some bogus contexts are left *)
  Unshelve. all: exact (def_ctx).
Qed.



(** After a beta substitution, the cost/weight decreases by at least one. *)
Lemma preservation_beta {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) M t τ t' :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  hasty Φ Γ M t τ ->
  closed t ->
  t ≻(β) t' ->
  exists N, hasty Φ Γ N t' τ /\
       sem! Φ ⊨ fun xs => N xs < M xs.
Proof.
  intros dec.
  intros Hty Hclos Hstep.
  induction Hty in dec,Hclos,t',Hstep|-*.
  all: try stuck; closed_inv.
  - (* Applications *) step_inv.
    + clear IHHty2. specialize IHHty1 with (1 := dec) (2 := Hclos) (3 := Hstep) as (N&IH1&IH2).
      exists (fun xs => N xs + K2 xs). split.
      * eapply ty_App; eauto.
        -- hnf; intros xs [Hfree | Hfree]; eapply Hmsum; cbn; eauto.
           contradict Hfree. eapply free_closed_iff; eauto. eapply preservation_closed. 2: hnf; eauto. eauto.
        -- hnf; intros xs [Hfree | Hfree]; eapply HΓ; cbn; eauto.
           contradict Hfree. eapply free_closed_iff; eauto. eapply preservation_closed. 2: hnf; eauto. eauto.
        -- hnf; reflexivity.
      * hnf in HM,IH2|-*; intros xs Hxs. specialize (HM _ Hxs); specialize (IH2 _ Hxs). lia.
    + clear IHHty1. specialize IHHty2 with (1 := dec) (2 := Hclos0) (3 := Hstep) as (N&IH1&IH2).
      exists (fun xs => K1 xs + N xs). split.
      * eapply ty_App; eauto.
        -- hnf; intros xs [Hfree | Hfree]; eapply Hmsum; cbn; eauto.
           contradict Hfree. eapply free_closed_iff; eauto. eapply preservation_closed. 2: hnf; eauto. eauto.
        -- hnf; intros xs [Hfree | Hfree]; eapply HΓ; cbn; eauto.
           contradict Hfree. eapply free_closed_iff; eauto. eapply preservation_closed. 2: hnf; eauto. eauto.
        -- hnf; reflexivity.
      * hnf in HM,IH2|-*; intros xs Hxs. specialize (HM _ Hxs); specialize (IH2 _ Hxs). lia.
    + (* Lam beta *) clear IHHty1 IHHty2.
      unshelve epose proof preservation_App_Lam dec _ (conj Hclos Hclos0) Hvalt' as (M'&L1&L2).
      4: now econstructor; eauto.
      exists M'. split.
      * eapply ty_sub'; eauto. reflexivity. hnf in L2|-*. firstorder omega.
      * assumption.
    + (* Fix beta *) clear IHHty1 IHHty2.
      unshelve epose proof preservation_App_Fix dec _ (conj Hclos Hclos0) Hvalt' as (M'&L1&L2).
      4: now econstructor; eauto.
      exists M'. split.
      * eapply ty_sub'; eauto. reflexivity. hnf in L2|-*. firstorder omega.
      * assumption.
  - (* Ifz *) step_inv.
    + specialize IHHty1 with (1 := dec) (2 := Hclos) (3 := Hstep) as (N&IH1&IH2).
      exists (fun xs => N xs + M2 xs). split.
      * eapply ty_Ifz; eauto.
        -- hnf; intros xs [Hfree | Hfree]; eapply Hmsum; cbn; eauto.
           contradict Hfree. eapply free_closed_iff; eauto. eapply preservation_closed. 2: hnf; eauto. eauto.
        -- hnf; intros xs [Hfree | Hfree]; eapply HΓ; cbn; eauto.
           contradict Hfree. eapply free_closed_iff; eauto. eapply preservation_closed. 2: hnf; eauto. eauto.
        -- hnf; reflexivity.
      * hnf in HM,IH2|-*; intros xs Hxs. specialize (HM _ Hxs); specialize (IH2 _ Hxs). lia.
    + discriminate. (* Not a beta step *)
    + discriminate. (* Not a beta step *)
  - (* Succ *) step_inv.
    + specialize IHHty with (1 := dec) (2 := Hclos) (3 := Hstep) as (N&IH1&IH2).
      exists N. split.
      * eapply ty_Succ; eauto.
      * assumption.
    + discriminate. (* Not a beta step *)
  - (* Pred *) step_inv.
    + specialize IHHty with (1 := dec) (2 := Hclos) (3 := Hstep) as (N&IH1&IH2).
      exists N. split.
      * eapply ty_Pred; eauto.
      * assumption.
    + discriminate. (* Not a beta step *)
Qed.


(** After a nat computation, the cost doesn't decrease (but the term size) *)
Lemma preservation_nat {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) M t τ t' :
  hasty Φ Γ M t τ ->
  closed t ->
  t ≻(ϵ) t' ->
  hasty Φ Γ M t' τ.
Proof.
  intros Hty Hclos Hstep.
  induction Hty in Hclos,t',Hstep|-*.
  all: try stuck; closed_inv.
  - (* Applications *) step_inv; try discriminate.
    + clear IHHty2. specialize IHHty1 with (1 := Hclos) (2 := Hstep). eapply ty_App; eauto.
      * hnf; intros xs [Hfree | Hfree]; eapply Hmsum; cbn; eauto.
        contradict Hfree. eapply free_closed_iff; eauto. eapply preservation_closed. 2: hnf; eauto. eauto.
      * hnf; intros xs [Hfree | Hfree]; eapply HΓ; cbn; eauto.
        contradict Hfree. eapply free_closed_iff; eauto. eapply preservation_closed. 2: hnf; eauto. eauto.
    + clear IHHty1. specialize IHHty2 with (1 := Hclos0) (2 := Hstep). eapply ty_App; eauto.
      * hnf; intros xs [Hfree | Hfree]; eapply Hmsum; cbn; eauto.
        contradict Hfree. eapply free_closed_iff; eauto. eapply preservation_closed. 2: hnf; eauto. eauto.
      * hnf; intros xs [Hfree | Hfree]; eapply HΓ; cbn; eauto.
        contradict Hfree. eapply free_closed_iff; eauto. eapply preservation_closed. 2: hnf; eauto. eauto.
  - (* Ifz *) step_inv; try discriminate.
    + specialize IHHty1 with (1 := Hclos) (2 := Hstep). eapply ty_Ifz; eauto.
      * hnf; intros xs [Hfree | Hfree]; eapply Hmsum; cbn; eauto.
        contradict Hfree. eapply free_closed_iff; eauto. eapply preservation_closed. 2: hnf; eauto. eauto.
      * hnf; intros xs [Hfree | Hfree]; eapply HΓ; cbn; eauto.
        contradict Hfree. eapply free_closed_iff; eauto. eapply preservation_closed. 2: hnf; eauto. eauto.
    + clear IHHty1 IHHty2 IHHty3.
      apply ty_Const_inv, submty_Nat_inv in Hty1; unfold iConst in Hty1.
      eapply ty_sub. eapply ty_weaken_Φ.
      * apply Hty2.
      * hnf; intros xs Hxs. split; auto. hnf in Hty1. erewrite Hty1; eauto.
      * hnf; intros var Hvar. exfalso. eapply free_closed_iff in Hclos0; eauto.
      * reflexivity.
      * hnf; intros xs Hxs. hnf in HM. specialize (HM xs Hxs). lia.
    + clear IHHty1 IHHty2 IHHty3.
      apply ty_Const_inv, submty_Nat_inv in Hty1; unfold iConst in Hty1.
      eapply ty_sub. eapply ty_weaken_Φ.
      * apply Hty3.
      * hnf; intros xs Hxs. split; auto. hnf in Hty1. specialize (Hty1 xs Hxs). lia.
      * hnf; intros var Hvar. exfalso. eapply free_closed_iff in Hclos1; eauto.
      * reflexivity.
      * hnf; intros xs Hxs. hnf in HM. specialize (HM xs Hxs). lia.

  - (* Succ *) step_inv.
    + specialize IHHty with (1 := Hclos) (2 := Hstep). eapply ty_Succ; eauto.
    + eapply submty_Nat_inv' in Hρ as (k'&->&Hρ). hnf in Hρ.
      eapply ty_Const_inv, submty_Nat_inv in Hty. hnf in Hty.
      eapply ty_Const; eauto.
      eapply submty_Nat. hnf; intros xs Hxs. hnf in Hρ. specialize (Hρ xs Hxs). specialize (Hty xs Hxs).
      unfold funcomp, iConst in *. lia.
  - (* Pred *) step_inv.
    + specialize IHHty with (1 := Hclos) (2 := Hstep). eapply ty_Pred; eauto.
    + eapply submty_Nat_inv' in Hρ as (k'&->&Hρ). hnf in Hρ.
      eapply ty_Const_inv, submty_Nat_inv in Hty. hnf in Hty.
      eapply ty_Const; eauto.
      eapply submty_Nat. hnf; intros xs Hxs. hnf in Hρ. specialize (Hρ xs Hxs). specialize (Hty xs Hxs).
      unfold funcomp, iConst in *. lia.
Qed.


Theorem normalisation (Γ : ctx 0) (M : idx 0) (t : tm) (τ : mty 0) :
  (ty! (fun xs => True); Γ ⊢(M) t : τ) ->
  closed t ->
  normalising t.
Proof.
  (* By induction on [i] and [size t] *)
  pose (size := fun '(M, t) => (((M : idx 0) [||]) : nat, size t)); cbn in *.
  enough (forall x, (ty! (fun xs => True); Γ ⊢(fst x) (snd x) : τ) -> closed (snd x) -> normalising (snd x)) as L.
  { specialize (L (M, t)); cbn in L; eauto. }
  clear M t. intros x. induction ((lexof_well_founded size) x) as [ [i t] _ IH ]; cbn in *.
  intros Htyp Hclos.
  constructor. intros t' (κ&Hstep).
  destruct κ; cbn in *.
  - (* β-reduction: The cost is decremented *)
    unshelve epose proof preservation_beta _ Htyp Hclos Hstep as (N&H1&H2).
    { cbn. firstorder. }
    hnf in H2; specialize (H2 [||] I).
    eapply (IH (_,_)); cbn. 2: eassumption. omega.
    eapply preservation_closed; hnf; eauto.
  - (* normal reduction: the term size is decreased *)
    unshelve epose proof preservation_nat Htyp Hclos Hstep as H1.
    pose proof step_nat_decreases_size Hstep.
    eapply (IH (_,_)); cbn. 2: eassumption. omega.
    eapply preservation_closed; hnf; eauto.
Qed.



(** Weakened preservation *)
Corollary preservation {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) M t τ t' κ :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  hasty Φ Γ M t τ ->
  closed t ->
  t ≻(κ) t' ->
  hasty Φ Γ M t' τ.
Proof.
  intros dec. intros Hty Hclos Hstep. destruct κ.
  - epose proof preservation_beta dec Hty Hclos Hstep as (N&Hty'&HN).
    eapply ty_sub'; eauto. reflexivity. firstorder.
  - eapply preservation_nat; eauto.
Qed.

Corollary preservation_steps {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) M (t t' : tm) τ :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  hasty Φ Γ M t τ ->
  closed t ->
  t ≻* t' ->
  hasty Φ Γ M t' τ.
Proof.
  intros dec. intros Htyp Hclos Hstar. induction Hstar in M,τ,Hclos,Htyp|-*.
  - assumption.
  - destruct H as [κ Hstep].
    pose proof preservation dec Htyp Hclos Hstep.
    eapply IHHstar; eauto. eapply preservation_closed; hnf; eauto.
Qed.

Corollary preservation_bigstep {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) M (t t' : tm) τ (k : nat) :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  hasty Φ Γ M t τ ->
  closed t ->
  t ⇓(k) t' ->
  hasty Φ Γ M t' τ.
Proof. eauto using preservation_steps, big_step_to_small_steps'. Qed.



Lemma bigstep_closed (t1 t2 : tm) (k : nat) :
  t1 ⇓(k) t2 ->
  closed t1 ->
  closed t2.
Proof. intros H1 H2. eapply star_closed; eauto. eapply big_step_to_small_steps'; eauto. Qed.


Lemma cost_soundness' (Γ : ctx 0) :
  forall t v (i : nat) (N : idx 0) (τ : mty 0),
    starB i t v ->
    val v -> closed t ->
    (ty! (fun xs => True); Γ ⊢(N) t : τ) ->
    i <= N [||].
Proof.
  intros t v M N τ Heval Hval Hclos Hty.
  induction Heval as [t |n t1 t2 t3 Hstep Hstar IH| n t1 t2 t3 Hstep Hstar IH] in N,τ,Hty,Hval,Hclos|-*; subst; closed_inv.
  - lia.
  - unshelve epose proof preservation_beta _ Hty Hclos Hstep as (N'&L1&L2).
    { cbn. auto. }
    hnf in L2; specialize (L2 [||] I).
    eapply Nat.lt_le_trans. 2: eauto. apply le_n_S.
    eapply IH; eauto.
    eapply preservation_closed; hnf; eauto.
  - epose proof preservation_nat Hty Hclos Hstep as L.
    eapply IH; eauto.
    now eapply preservation_closed; hnf; eauto.
Qed.


Theorem cost_soundness (Γ : ctx 0) :
  forall t v (i : nat) (N : idx 0) (τ : mty 0),
    t ⇓(i) v ->
    closed t ->
    (ty! (fun xs => True); Γ ⊢(N) t : τ) ->
    i <= N [||].
Proof.
  intros t v i N τ Hbig Hclos Hty.
  assert (val v) as Hval by (eapply bigstep_result_is_val; eauto).
  apply big_step_to_small_steps in Hbig.
  eapply cost_soundness'; eauto.
Qed.
