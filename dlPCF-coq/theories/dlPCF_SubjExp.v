Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax PCF_Semantics.
Require Import PCF_StepT PCF_TypesT.
Require Import dlPCF_iSubst.
Require Import ForestCard dlPCF_Types dlPCF_Sum dlPCF_Contexts dlPCF_Typing.
Require Import dlPCF_TypingT.
Require Import dlPCF_MakeSum.
Require PCF_Types.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.
Import SigmaTypeNotations.
Open Scope vector_scope.

From Coq Require Import Vector Fin.

Open Scope PCF.

Implicit Types (ϕ : nat).

Require Import dlPCF_Safety.
Require Import dlPCF_ConvSubst.

Import Skel.
Import EqNotations.



Lemma step_not_val t κ t' :
  step t κ t' -> ~ val t.
Proof. intros H1 H2. eapply val_stuck; eauto. Qed.
Lemma step_valb_false t κ t' :
  step t κ t' -> valb t = false.
Proof. intros H. apply valb_iff'. eapply step_not_val; eauto. Qed.
Lemma stepT_valb_false t κ t' :
  stepT t κ t' -> valb t = false.
Proof. intros H % stepT_to_step. eapply step_valb_false; eauto. Qed.


Lemma mty_strip_eq_Nat_inv {ϕ} (τ : mty ϕ) :
  PCF.Nat = mty_strip τ -> { K & τ = Nat K }.
Proof.
  destruct τ; cbn.
  - eauto.
  - destruct A; discriminate.
Qed.



(** Instance of [converse_subst], where the variable is [0] and is the only free variable in [t]. *)
Lemma converse_subst1 {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (K : idx ϕ) (t : tm) (v : tm)
      (ρ : mty ϕ) (σ_pcf τ_pcf : PCF.ty)
      (pcfTy1 : PCF.hastyT (σ_pcf .: stripCtx Γ) t τ_pcf)
      (pcfTy2 : PCF.hastyT (stripCtx Γ) v σ_pcf) :
  let s1 := PCF.strip pcfTy1 in
  let s2 := PCF.strip pcfTy2 in
  τ_pcf = mty_strip ρ ->
  (Ty! Φ; Γ ⊢(K) nbeta1 t v : ρ @ (PCF.skel_subst 0 t s1 s2)) ->
  bound 1 t -> val v -> closed v ->
  existsS (M N : idx ϕ) σ,
    (Ty! Φ; σ .: Γ ⊢(M) t : ρ @ s1) **
    (Ty! Φ; Γ ⊢(N) v : σ @ s2) **
    (sem! Φ ⊨ fun xs => M xs + N xs = K xs) **
    mty_strip σ = σ_pcf.
Proof.
  intros * Hstrip Hty Hb Hv Hc.
  pose (Γ' := pred >> Γ).

  (* unshelve epose (pcfTy1' := _ : PCF.hastyT (PCF_ctxExtend (stripCtx Γ') 0 σ_pcf) t τ_pcf). *)
  unshelve evar (pcfTy1' : PCF.hastyT (PCF_ctxExtend (stripCtx Γ') 0 σ_pcf) t τ_pcf).
  { eapply PCF.retypeT_free. apply pcfTy1. abstract (intros [ | y] Hy; cbn; reflexivity). }
  unshelve evar (pcfTy2' : PCF.hastyT (stripCtx Γ') v σ_pcf).
  { eapply PCF.retypeT_closed; eauto. }

  assert (Ty! Φ; Γ' ⊢(K) nbeta1 t v : ρ @ (PCF.skel_subst 0 t s1 s2)) as Hty'.
  {
    eapply retypeT_free; eauto.
    unfold Γ', funcomp. intros [ | y] Hy; cbn.
    - reflexivity.
    - exfalso. apply free_nsubst_closed in Hy; eauto. eapply free_bound_iff in Hb; eauto. lia.
  }

  assert (PCF.strip pcfTy1' = s1) as Hs1_strip.
  { unfold pcfTy1'. rewrite PCF.retypeT_free_skel. auto. }
  assert (PCF.strip pcfTy2' = s2) as Hs2_strip.
  { unfold pcfTy2'. now rewrite PCF.retypeT_closed_skel. }

  pose proof converse_subst (Φ := Φ) (K := K) (ρ := ρ) pcfTy1' pcfTy2' Hstrip as L.
  spec_assert L.
  { rewrite Hs1_strip, Hs2_strip. apply Hty'. }
  specialize L with (1 := Hv) (2 := Hc) as (M & N & σ & L1 & L2 & L3 & L4).
  eexists _, _, _; repeat_split; eauto.
  - rewrite Hs1_strip in L1. eapply retypeT_free; eauto.
    unfold Γ', funcomp. intros [ | y] Hy; cbn.
    + reflexivity.
    + exfalso. eapply free_bound_iff in Hb; eauto. lia.
  - rewrite Hs2_strip in L2. eapply retypeT_closed; eauto.
Qed.



(** For the lambda beta case, we need to prove an instance of the 'typing uniformisation lemma':

   If [ϕ; Φ; Γ{0/a} ⊢(M{0/a}) t : ρ{0/a}] then [a,ϕ; a < 1 ∧ Φ; Γ ⊢(M) t : ρ].

(Where [a] is not free in Φ)

Note that the subtyping lemma for this was already required in the subject reduction proof, see [sublty_lt_one].

As usual, we first generalise the lemma over [a] (at position [x : nat] in [ϕ]), then instantiate [x = 0].
*)


(** Remove element number [x] from [xs] *)
Definition remove {ϕ} (x : Fin.t (S ϕ)) (xs : Vector.t nat (S ϕ)) : Vector.t nat ϕ.
Proof.
  apply vect_cast with (n := fin_to_nat x + S (ϕ -' fin_to_nat x)) in xs.
  2: abstract (rewrite sub'_correct; pose proof fin_to_nat_lt x; lia).
  refine (vect_cast (vect_app (vect_take _ _ xs) (tl (vect_skip _ _ xs))) _).
  abstract (rewrite sub'_correct; pose proof fin_to_nat_lt x; lia).
Defined.

(* Compute remove Fin1 (0:::1:::2:::3:::nil _). (* = [|0; 2; 3|] *) *)


Lemma eqmty_unif {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (σ τ : mty (S ϕ)) :
  (forall n, mty! subst_beta_ground_fun (iConst n) Φ ⊢
           subst_mty_beta_ground σ (iConst n) ≡ subst_mty_beta_ground τ (iConst n)) ->
  (mty! Φ ⊢ σ ≡ τ).
Proof. intros H. split; eapply submty_unif; apply H. Qed.

Lemma eqlty_lt_one {ϕ} (Φ : constr ϕ) (A B : lty (S ϕ)) :
  (lty! Φ ⊢ subst_lty_beta_ground A (iConst 0) ≡ subst_lty_beta_ground B (iConst 0)) ->
  lty! (fun xs => hd xs < 1 /\ Φ (tl xs)) ⊢ A ≡ B.
Proof. now intros [H1 H2]; split; apply sublty_lt_one. Qed.

Lemma eqmty_lt_one {ϕ} (Φ : constr ϕ) (σ τ : mty (S ϕ)) :
  (mty! Φ ⊢ subst_mty_beta_ground σ (iConst 0) ≡ subst_mty_beta_ground τ (iConst 0)) ->
  mty! (fun xs => hd xs < 1 /\ Φ (tl xs)) ⊢ σ ≡ τ.
Proof. now intros [H1 H2]; split; apply submty_lt_one. Qed.



Lemma lt_1_eq_0 : forall x, x < 1 -> x = 0. Proof. lia. Qed.


(** Refinement of [eqlty_lt_one], where the variable can be free in [Φ]. *)

Lemma eqlty_lt_one' {ϕ} (Φ : constr (S ϕ)) (A B : lty (S ϕ)) :
  (lty! fun xs => Φ (0 ::: xs) ⊢ subst_lty_beta_ground A (iConst 0) ≡ subst_lty_beta_ground B (iConst 0)) ->
  lty! (fun xs => hd xs < 1 /\ Φ xs) ⊢ A ≡ B.
Proof.
  intros H.
  pose proof eqlty_lt_one (Φ := fun xs : Vector.t nat ϕ => Φ (0 ::: xs)) A B H as X.
  eapply eqlty_mono_Φ; cbn; eauto.
  hnf; intros xs (Hxs1 & Hxs2).
  apply lt_1_eq_0 in Hxs1. rewrite <- Hxs1. rewrite <- eta. auto.
Qed.


Lemma eqmty_lt_one' {ϕ} (Φ : constr (S ϕ)) (σ τ : mty (S ϕ)) :
  (mty! fun xs => Φ (0 ::: xs) ⊢ subst_mty_beta_ground σ (iConst 0) ≡ subst_mty_beta_ground τ (iConst 0)) ->
  mty! (fun xs => hd xs < 1 /\ Φ xs) ⊢ σ ≡ τ.
Proof.
  intros H.
  pose proof eqmty_lt_one (Φ := fun xs : Vector.t nat ϕ => Φ (0 ::: xs)) σ τ H as X.
  eapply eqmty_mono_Φ; cbn; eauto.
  hnf; intros xs (Hxs1 & Hxs2).
  apply lt_1_eq_0 in Hxs1. rewrite <- Hxs1. rewrite <- eta. auto.
Qed.


(* TODO: Move *)
Lemma subst_mty_beta_ground_eq_Quant {ϕ} (I : idx (S ϕ)) (A : lty (S (S ϕ))) (i : idx ϕ) :
  subst_mty_beta_ground (Quant I A) i =
  Quant (subst_beta_ground_fun i I) (subst_lty_beta_one_ground A i).
Proof. reflexivity. Qed.




Require Import dlPCF_SwapVarT.


(**
The type [τ'] witnesses that variable [a] is irrelevant in [τ].
We can construct a new bounded sum over [σ] (where the same fresh variable [a] is inserted).
The new sum is equivalent to the old sum, under the premise [a < 1].
*)

Lemma bsum_unif_lt1 {ϕ} (Φ : constr ϕ) (I : idx ϕ) (σ : mty (S ϕ)) (τ : mty (S ϕ)) (τ' : mty ϕ) :
  bsum I σ τ' ->
  (mty! Φ ⊢ subst_mty_beta_ground τ (iConst 0) ≡ τ') ->
  { τ'' &
    bsum (fun xs : Vector.t nat (S ϕ) => I (tl xs))
         (subst_mty σ (fun (x : idx (S ϕ)) (xs : Vector.t nat (S (S ϕ))) => x (hd xs ::: tl (tl xs))))
         τ'' **
    mty! fun xs : Vector.t nat (S ϕ) => hd xs < 1 /\ Φ (tl xs) ⊢ τ ≡ τ'' }.
Proof.
  intros [H1 | H1] H2; hnf in H1.
  - destruct H1 as (J & -> & ->).
    destruct τ.
    2:{ destruct A. cbn in H2. symmetry in H2. apply eqmty_Nat_inv'_sig in H2. firstorder discriminate. discriminate. }
    cbn in H2. apply eqmty_Nat_inv in H2. unfold subst_beta_ground_fun, iConst in H2.

    eexists; split.
    {
      left; hnf. eexists; split. 2: reflexivity.
      setoid_rewrite subst_mty_twice; unfold funcomp; cbn.
      instantiate (1 := fun xs => J (tl xs)). reflexivity.
    }
    {
      apply eqmty_Nat. hnf; intros xs (Hxs1 & Hxs2). apply lt_1_eq_0 in Hxs1.
      rewrite <- H2 by auto. rewrite <- Hxs1. f_equal. now rewrite <- eta.
    }
  - destruct H1 as (J & A & -> & ->).
    destruct τ.
    1:{ cbn in H2. apply eqmty_Nat_inv'_sig in H2. firstorder discriminate. discriminate. }
    rewrite subst_mty_beta_ground_eq_Quant in H2. apply eqmty_Quant_inv in H2 as (H1 & H2).

    eexists _; split.
    {
      right; hnf. eexists _,_; split. 2: reflexivity.
      cbn. f_equal. 1: reflexivity.
      setoid_rewrite subst_lty_twice; unfold funcomp; cbn.
      instantiate (1 := fun f xs => f (hd xs ::: tl (tl xs))); cbn. reflexivity.
    }
    {
      apply eqmty_Quant.
      - apply @eqlty_mono_Φ with
            (Φ := fun xs => hd (tl xs) < 1 /\ hd xs < (Σ_{a < I (tl (tl xs))} J (hd (a ::: tl xs) ::: tl (tl (a ::: tl xs)))) /\ Φ (tl (tl xs))). 2: firstorder.
        apply eqlty_swap. unfold subst_swap_fun; cbn.
        apply eqlty_lt_one'; cbn.
        repeat setoid_rewrite subst_lty_twice; unfold funcomp; cbn.
        change (subst_lty A0 (fun x : idx (S (S ϕ)) => subst_beta_ground_fun (iConst 0) (subst_swap_fun x)))
          with (subst_lty_beta_one_ground A0 (iConst 0)).
        etransitivity; eauto.
        setoid_rewrite <- subst_lty_id.
        setoid_rewrite subst_lty_twice; unfold funcomp; cbn. apply eqlty_subst_idx_sem.
        unfold id, subst_beta_ground_fun, subst_swap_fun; cbn.
        hnf; intros xs (Hxs1 & Hxs2) j; f_equal; clear j. now rewrite <- eta.
      - cbn. hnf; intros xs (Hxs1 & Hxs2). apply lt_1_eq_0 in Hxs1.
        rewrite <- H2 by auto. unfold subst_beta_ground_fun, iConst. rewrite <- Hxs1. f_equal. now rewrite <- eta.
    }
Qed.


Lemma ctxBSum_unif_lt1 {ϕ} (t : tm) (Φ : constr ϕ) (I : idx ϕ) (Δ : ctx (S ϕ)) (Γ : ctx (S ϕ)) (Γ' : ctx ϕ) :
  ctxBSum t I Δ Γ' ->
  (ctx! t; Φ ⊢ subst_ctx_beta_ground (iConst 0) Γ ≡ Γ') ->
  { Γ'' &
    ctxBSum t (fun xs : Vector.t nat (S ϕ) => I (tl xs))
            (subst_ctx Δ (fun (x : idx (S ϕ)) (xs : Vector.t nat (S (S ϕ))) => x (hd xs ::: tl (tl xs)))) Γ'' **
    ctx! t; fun xs : Vector.t nat (S ϕ) => hd xs < 1 /\ Φ (tl xs) ⊢ Γ ≡ Γ'' }.
Proof.
  intros H1 H2; hnf in H1,H2.
  exists (fun y => match free_dec y t with
           | left Hy => projT1 (bsum_unif_lt1 _ (H1 y Hy) (H2 y Hy))
           | _ => def_mty
           end).
  split.
  - hnf; intros y Hy. destruct (free_dec y t) as [Hy'|Hy']; try tauto.
    destruct (bsum_unif_lt1 (Γ y) (H1 y Hy') (H2 y Hy')) as (X & Y & Z); cbn; eauto.
  - hnf; intros y Hy. destruct (free_dec y t) as [Hy'|Hy']; try tauto.
    destruct (bsum_unif_lt1 (Γ y) (H1 y Hy') (H2 y Hy')) as (X & Y & Z); cbn; eauto.
Qed.



(** Similar as [bsum_unif_lt1], but for [msum] *)

Lemma msum_unif_lt1 {ϕ} (Φ : constr ϕ) (σ1 σ2 : mty ϕ) (τ : mty (S ϕ)) (τ' : mty ϕ) :
  msum σ1 σ2 τ' ->
  mty! Φ ⊢ subst_mty_beta_ground τ (iConst 0) ≡ τ' ->
  { τ'' &
    msum (subst_mty σ1 (fun (f : idx ϕ) (xs : Vector.t nat (S ϕ)) => f (tl xs)))
         (subst_mty σ2 (fun (f : idx ϕ) (xs : Vector.t nat (S ϕ)) => f (tl xs)))
         τ'' **
    mty! fun xs : Vector.t nat (S ϕ) => hd xs < 1 /\ Φ (tl xs) ⊢ τ ≡ τ'' }.
Proof.
  destruct σ1, σ2, τ'; cbn in *; try tauto; intros H1 H2.
  { (* Nat *)
    destruct H1 as [<- <-].
    destruct τ.
    2:{ cbn in H2. symmetry in H2. apply eqmty_Nat_inv'_sig in H2. firstorder discriminate. }
    cbn in H2. apply eqmty_Nat_inv in H2. unfold subst_beta_ground_fun, iConst in H2.
    eexists (Nat _); split.
    { split; reflexivity. }
    {
      apply eqmty_Nat.
      hnf; intros xs (Hxs1 & Hxs2). apply lt_1_eq_0 in Hxs1.
      rewrite <- H2 by auto. rewrite <- Hxs1. now rewrite <- eta.
    }
  }
  { (* Quant *)
    destruct H1 as (<- & -> & H1).
    destruct τ.
    1:{ destruct A. cbn in H2. apply eqmty_Nat_inv'_sig in H2. firstorder discriminate. }
    rewrite subst_mty_beta_ground_eq_Quant in H2.
    apply eqmty_Quant_inv in H2 as (H2 & H3).
    eexists (Quant _ _); split.
    { repeat_split. reflexivity. 2: hnf; reflexivity. setoid_rewrite subst_lty_twice; unfold funcomp; cbn. reflexivity. }
    {
      apply eqmty_Quant.
      - apply @eqlty_mono_Φ with (Φ := fun xs => hd (tl xs) < 1 /\ hd xs < i (tl (tl xs)) + i0 (tl (tl xs)) /\ Φ (tl (tl xs))). 2: firstorder.
        apply eqlty_swap; unfold subst_swap_fun; cbn.
        apply eqlty_lt_one'; unfold subst_swap_fun; cbn.
        replace (subst_lty_beta_ground (subst_lty_swap A0) (iConst 0)) with (subst_lty_beta_one_ground A0 (iConst 0))
          by now setoid_rewrite subst_lty_twice.
        etransitivity.
        { eapply eqlty_mono_Φ. apply H2. hnf; intros xs (Hxs1 & Hxs2); split; auto. now rewrite H1. }
        repeat setoid_rewrite subst_lty_twice; unfold funcomp; cbn.
        rewrite <- subst_lty_id at 1.
        apply eqlty_subst_idx_sem.
        hnf; intros xs (Hxs1 & Hxs2). unfold subst_beta_ground_fun, subst_swap_fun, id; cbn; intros j; f_equal; clear j.
        now rewrite <- eta.
      - hnf; intros xs (Hxs1 & Hxs2). apply lt_1_eq_0 in Hxs1.
        hnf in H3. specialize (H3 (tl xs) Hxs2). unfold subst_beta_ground_fun, iConst in H3.
        rewrite <- Hxs1, <- eta in H3. rewrite <- H1 by auto. now rewrite H3.
    }
  }
Qed.


Lemma ctxMSum_unif_lt1 {ϕ} (t : tm) (Φ : constr ϕ) (Δ1 Δ2 : ctx ϕ) (Γ : ctx (S ϕ)) (Γ' : ctx ϕ) :
  ctxMSum t Δ1 Δ2 Γ' ->
  (ctx! t; Φ ⊢ subst_ctx_beta_ground (iConst 0) Γ ≡ Γ') ->
  { Γ'' &
    ctxMSum t (subst_ctx Δ1 (fun (f : idx ϕ) (xs : Vector.t nat (S ϕ)) => f (tl xs)))
              (subst_ctx Δ2 (fun (f : idx ϕ) (xs : Vector.t nat (S ϕ)) => f (tl xs)))
            Γ'' **
    ctx! t; fun xs : Vector.t nat (S ϕ) => hd xs < 1 /\ Φ (tl xs) ⊢ Γ ≡ Γ'' }.
Proof.
  intros H1 H2; hnf in H1,H2.
  exists (fun y => match free_dec y t with
           | left Hy => projT1 (msum_unif_lt1 _ _ _ (H1 y Hy) (H2 y Hy))
           | _ => def_mty
           end).
  split.
  - hnf; intros y Hy. destruct (free_dec y t) as [Hy'|Hy']; try tauto.
    destruct (msum_unif_lt1 _ _ _ (H1 y Hy') (H2 y Hy')) as (X & Y & Z); cbn; eauto.
  - hnf; intros y Hy. destruct (free_dec y t) as [Hy'|Hy']; try tauto.
    destruct (msum_unif_lt1 _ _ _ (H1 y Hy') (H2 y Hy')) as (X & Y & Z); cbn; eauto.
Qed.



Lemma subst_var_swap_fun_twice {X : Type} {ϕ} (x : Fin.t (S ϕ)) (xs : Vector.t nat _) (f : Vector.t nat (S (S ϕ)) -> X):
  subst_var_swap_fun x (subst_var_swap_fun x f) xs = f xs.
Proof.
  induction ϕ.
  - assert (x = Fin0) as ->.
    { pose proof fin_inv_S x as [ -> | (x' & ->)]; auto. now pose proof fin_inv_0 x'. }
    rewrite <- !subst_swap_fun_correct. intros. now rewrite subst_swap_fun_twice.
  - pose proof fin_inv_S x as [ -> | (x' & ->)].
    + rewrite <- !subst_swap_fun_correct. intros. now rewrite subst_swap_fun_twice.
    + rewrite !subst_var_swap_fun_FS; cbn.
      rewrite IHϕ. now rewrite <- eta.
Qed.

Lemma subst_var_lty_swap_twice {ϕ} (x : Fin.t (S ϕ)) (A : lty (S (S ϕ))) :
  subst_lty_var_swap x (subst_lty_var_swap x A) = A.
Proof.
  setoid_rewrite subst_lty_twice; unfold funcomp, subst_swap_fun; cbn; eta.
  rewrite <- subst_lty_id. f_equal; fext; intros f xs. unfold id.
  now rewrite subst_var_swap_fun_twice.
Qed.

Lemma eqlty_swap_var {ϕ} (x : Fin.t (S ϕ)) (Φ : constr (S (S ϕ))) (A B : lty (S (S ϕ))) :
  lty! subst_var_swap_fun x Φ ⊢ subst_lty_var_swap x A ≡ subst_lty_var_swap x B ->
  lty! Φ ⊢ A ≡ B.
Proof.
  intros H.
  setoid_rewrite <- subst_var_lty_swap_twice with (x0 := x) at 1;
    symmetry; setoid_rewrite <- subst_var_lty_swap_twice with (x0 := x) at 1; symmetry.
  eapply eqlty_mono_Φ. eapply eqlty_subst_var_swap; eauto.
  hnf; intros xs Hxs. now rewrite subst_var_swap_fun_twice.
Qed.

(** Swap #0 with #2 *)
Definition subst_swap2_fun {X:Type} {ϕ} : (Vector.t nat (S (S (S ϕ))) -> X) -> (Vector.t nat (S (S (S ϕ))) -> X) :=
  fun f xs => f (hd (tl (tl xs)) ::: hd (tl xs) ::: hd xs ::: tl (tl (tl xs))).
              (* #2 *)            (* #1 *)      (* #0 *)

(*
#0 <=> #1
#1 <=> #2
#0 <=> #1
=========
Swaps #0 and #2
*)
Lemma subst_swap2_fun_correct {X:Type} {ϕ} :
  subst_swap2_fun (X := X) (ϕ := ϕ) =
  subst_swap_fun >>
  subst_var_swap_fun Fin1 >>
  subst_swap_fun.
Proof. fext. intros f xs. unfold funcomp. rewrite !subst_var_swap_fun_FS, <- !subst_swap_fun_correct. reflexivity. Qed.

Lemma eqlty_swap2 {ϕ} (Φ : constr (S (S (S ϕ)))) (A B : lty (S (S (S ϕ)))) :
  (lty! subst_swap2_fun Φ ⊢ subst_lty A subst_swap2_fun ≡ subst_lty B subst_swap2_fun) ->
  lty! Φ ⊢ A ≡ B.
Proof.
  intros H. rewrite !subst_swap2_fun_correct in H.
  repeat setoid_rewrite <- subst_lty_twice in H.
  now apply eqlty_swap, eqlty_swap_var with (x := Fin1), eqlty_swap.
Qed.




Lemma tyT_unif_lt1 {ϕ} (Φ : constr ϕ) (Γ : ctx (S ϕ)) (M : idx ϕ) (t : tm) (ρ : mty (S ϕ)) (s : skel) :
  (Ty! Φ; subst_ctx_beta_ground (iConst 0) Γ ⊢(M) t : subst_mty_beta_ground ρ (iConst 0) @ s) ->
  (Ty! fun xs => hd xs < 1 /\ Φ (tl xs); Γ ⊢(fun xs => M (tl xs)) t : ρ @ s).
Proof.
  intros ty. revert dependent ϕ; revert s.
  induction t as [t IH] using (@induction_ltof1_Type _ size); unfold ltof in IH; intros.
  inv ty.
  - (* Var y *) apply tyT_Var. apply eqmty_lt_one; eauto.
    firstorder.

  - (* Lam *)
    destruct ρ.
    { exfalso. cbn in Hρ. inv Hρ. inv H. }
    destruct A as [σ' τ'].
    rewrite subst_mty_beta_ground_eq_Quant in Hρ.
    change (subst_lty_beta_one_ground (?σ ⊸ ?τ) ?i) with (subst_mty_beta_one_ground σ i ⊸ subst_mty_beta_one_ground τ i) in Hρ.
    apply eqmty_Quant_inv in Hρ as (Hρ & Hρ3). apply eqlty_Arr_inv in Hρ as (Hρ1 & Hρ2).

    pose proof ctxBSum_unif_lt1 Hbsum HΓ as (Γ'' & Hbsum' & HΓ'').

    eapply tyT_Lam with
        (I0 := fun xs : Vector.t nat (S ϕ) => I (tl xs))
        (Δ0 := subst_ctx Δ (fun (x : idx (S ϕ)) (xs : Vector.t nat (S (S ϕ))) => x (hd xs ::: tl (tl xs))))
        (K0 := fun xs => K (hd xs ::: tl (tl xs)))
        (σ0 := subst_mty σ (fun f xs => f (hd xs ::: tl (tl xs)))) (τ0 := subst_mty τ (fun f xs => f (hd xs ::: tl (tl xs))))
        (Γ'0 := Γ'').
    + apply @tyT_weaken_Φ with (Φ := fun xs => hd (tl xs) < 1 /\ hd xs < I (tl (tl xs)) /\ Φ (tl (tl xs))).
      2: firstorder.
      apply tyT_swap; unfold subst_swap_fun; cbn.
      eapply tyT_sub. eapply tyT_weaken_Φ.
      * apply IH. cbn; lia.
        eapply tyT_sub. apply Hty.
        -- instantiate (1 := subst_ctx (σ .: Δ) (fun f xs => f (tl xs))).
           repeat setoid_rewrite subst_ctx_scons. apply eqCtx_scons.
           ++ setoid_rewrite subst_mty_twice. setoid_rewrite subst_mty_id. reflexivity.
           ++ intros y Hy. setoid_rewrite subst_mty_twice. setoid_rewrite subst_mty_id. reflexivity.
        -- instantiate (1 := subst_mty τ (fun f xs => f (tl xs))).
           setoid_rewrite subst_mty_twice. setoid_rewrite subst_mty_id. reflexivity.
        -- hnf; reflexivity.
      * cbn. firstorder.
      * setoid_rewrite subst_ctx_scons. eapply eqCtx_scons.
        -- setoid_rewrite subst_mty_twice; unfold funcomp. unfold subst_beta_ground_fun; cbn; eta.
           eapply eqmty_subst_idx_sem. hnf; intros xs Hxs j; f_equal; clear j. now rewrite <- eta.
        -- intros y Hy.
           setoid_rewrite subst_mty_twice; unfold funcomp. unfold subst_beta_ground_fun; cbn; eta.
           eapply eqmty_subst_idx_sem. hnf; intros xs Hxs j; f_equal; clear j. now rewrite <- eta.
      * apply eqmty_swap; unfold subst_swap_fun; cbn.
        rewrite subst_mty_swap_twice.
        setoid_rewrite subst_mty_twice; unfold funcomp, subst_swap_fun; cbn.
        reflexivity.
      * cbn. hnf; intros xs Hxs. now rewrite <- eta.
    + apply Hbsum'.
    + apply HΓ''.
    + hnf; intros xs (Hxs1 & Hxs2). rewrite <- HM by auto. reflexivity.
    + apply eqmty_Quant.
      {
        apply eqlty_Arr.
        - apply @eqmty_mono_Φ with (Φ := fun xs => hd (tl xs) < 1 /\ hd xs < i (tl xs) /\ Φ (tl (tl xs))). 2: firstorder.
          apply eqmty_swap; unfold subst_swap_fun; cbn. apply eqmty_lt_one'.
          repeat setoid_rewrite subst_mty_twice; unfold funcomp, iConst, subst_swap_fun, subst_beta_ground_fun; cbn.
          replace (subst_mty σ (fun (x : idx (S ϕ)) (xs : Vector.t nat (S ϕ)) => x (hd xs ::: tl xs))) with σ.
          2:{ symmetry. apply subst_mty_id'. intros f xs; f_equal; clear f. now rewrite <- eta. }
          exact Hρ1.
        - apply @eqmty_mono_Φ with (Φ := fun xs => hd (tl xs) < 1 /\ hd xs < i (tl xs) /\ Φ (tl (tl xs))). 2: firstorder.
          apply eqmty_swap; unfold subst_swap_fun; cbn. apply eqmty_lt_one'.
          repeat setoid_rewrite subst_mty_twice; unfold funcomp, iConst, subst_swap_fun, subst_beta_ground_fun; cbn.
          replace (subst_mty τ (fun (x : idx (S ϕ)) (xs : Vector.t nat (S ϕ)) => x (hd xs ::: tl xs))) with τ.
          2:{ symmetry. apply subst_mty_id'. intros f xs; f_equal; clear f. now rewrite <- eta. }
          exact Hρ2.
      }
      { hnf; intros xs (Hxs1 & Hxs2). apply lt_1_eq_0 in Hxs1. rewrite Hρ3 by auto. unfold subst_beta_ground_fun, iConst.
        f_equal. now rewrite <- Hxs1, <- eta. }


  - (* Fix *) rename t0 into t. specialize IH with (y := Lam t). spec_assert IH by (cbn; lia).
    cbn zeta in *.

    pose proof ctxBSum_unif_lt1 Hbsum HΓ as (Γ'' & Hbsum' & HΓ'').

    eapply tyT_Fix with
        (A0 := subst_lty A (fun f xs => f (hd xs ::: hd (tl xs) ::: tl (tl (tl xs))))) (* The variable is #2 in A and B *)
        (B0 := subst_lty B (fun f xs => f (hd xs ::: hd (tl xs) ::: tl (tl (tl xs))))) (* ([a] and [b] are skipped.) *)
        (Δ0 := subst_ctx Δ (fun f xs => f (hd xs ::: tl (tl xs))))
        (Ib0 := fun xs => Ib (hd xs ::: tl (tl xs)))
        (Jb0 := fun xs => Jb (hd xs ::: tl (tl xs)))
        (K0 := fun xs => K (tl xs))
        (cardH0 := fun xs => cardH (tl xs))
        (card3 := fun xs => card1 (hd xs ::: hd (tl xs) ::: tl (tl (tl xs))))
        (card4 := fun xs => card2 (hd xs ::: tl (tl xs)))
    .
    1:{ (* The typing of [Lam t] *)
      apply @tyT_weaken_Φ with (Φ := fun xs => let b := hd xs in let xs' := tl xs in hd xs' < 1 /\
                                            b < cardH (tl xs') /\ Φ (tl xs')); cbn zeta; [ | firstorder].
      apply tyT_swap; unfold subst_swap_fun; cbn.

      (* eapply tyT_weaken. *)
      (* 2:{ setoid_rewrite subst_ctx_scons. reflexivity. } *)

      eapply tyT_sub'. (* Don't subtype the context *)
      - apply IH with (Φ := fun xs => hd xs < cardH (tl xs) /\ Φ (tl xs)).
        eapply tyT_sub.
        + apply Hty.
        + repeat setoid_rewrite subst_ctx_scons. apply eqCtx_scons.
          * repeat setoid_rewrite subst_mty_twice; unfold funcomp; cbn.
            apply eqmty_Quant.
            -- symmetry. rewrite <- subst_lty_id at 1.
               repeat setoid_rewrite subst_lty_twice; unfold funcomp; cbn.
               eapply eqlty_subst_idx_sem.
               hnf; intros xs (Hxs1 & Hxs2 & Hxs3).
               unfold id, subst_beta_ground_fun, iConst; cbn.
               intros j; f_equal; clear j. now rewrite <- !eta.
            -- hnf; intros y Hy. unfold subst_beta_ground_fun, iConst; cbn. now rewrite <- eta.
          * intros y Hy.
            repeat setoid_rewrite subst_mty_twice; unfold funcomp; cbn.
            unfold subst_beta_ground_fun, iConst; cbn. apply eqmty_eq. apply subst_mty_id'. intros; now rewrite <- eta.
        + instantiate (1 := subst_mty (Quant (iConst 1) B) (fun f xs => f (tl xs))).
          setoid_rewrite subst_mty_twice; unfold funcomp; cbn.
          unfold subst_beta_ground_fun, iConst; cbn. apply eqmty_eq. f_equal. symmetry; apply subst_lty_id'. intros; now rewrite <- eta.
        + hnf; reflexivity.
      - cbn. eapply eqmty_Quant. 2: hnf; reflexivity.
        setoid_rewrite subst_lty_twice; unfold funcomp, subst_swap_fun, iConst; cbn. apply eqlty_subst_idx_sem.
        hnf; intros xs Hxs. intros j; f_equal; clear j. f_equal. now rewrite <- eta.
      - cbn. hnf; intros xs (Hxs1 & Hxs2 & Hxs3). f_equal. now rewrite <- eta.
    }
    5: exact Hbsum'. 5: exact HΓ''. (* The new context sum *)
    2:{ (* The main card *) cbn; firstorder. }
    2:{ (* The first aux card *) cbn; hnf; intros xs (Hxs1 & Hxs2 & Hxs3 & Hxs4).
        apply (Hcard1 (hd xs ::: hd (tl xs) ::: tl (tl (tl xs)))); auto. }
    2:{ (* The second aux card *) cbn; hnf; intros xs (Hxs1 & Hxs2 & Hxs3). apply (Hcard2 (hd xs ::: tl (tl xs))); auto. }
    1:{ (* The first subtyping *)
      cbn. (* Here, the variable is at position #2; it has to be swapped with #0 first. *)
      eapply eqlty_swap2; unfold subst_swap2_fun; cbn.
      apply @eqlty_mono_Φ with
          (Φ := fun xs => hd xs < 1 /\ hd (tl (tl xs)) < Ib (hd (tl xs) ::: tl (tl (tl xs))) /\ hd (tl xs) < cardH (tl (tl (tl xs))) /\ Φ (tl (tl (tl xs)))). 2: firstorder.
      apply eqlty_lt_one'; cbn.
      (* And we have to swap once more... *)
      apply eqlty_swap; unfold subst_swap_fun; cbn.
      (* Now the order is right... But it looks quite chaotic, but it is easy... *)
      replace (subst_lty_swap
     (subst_lty_beta_ground
        (subst_lty
           (subst_lty A (fun (f : idx (S (S ϕ))) (xs : Vector.t nat (S (S (S ϕ)))) => f (hd xs ::: hd (tl xs) ::: tl (tl (tl xs)))))
           (fun (f : Vector.t nat (S (S (S ϕ))) -> nat) (xs : Vector.t nat (S (S (S ϕ)))) =>
            f (hd (tl (tl xs)) ::: hd (tl xs) ::: hd xs ::: tl (tl (tl xs))))) (iConst 0)))
        with A.
      2:{ repeat setoid_rewrite subst_lty_twice; unfold funcomp, subst_beta_ground_fun, subst_swap_fun; cbn.
          symmetry; apply subst_lty_id'. intros g xs; f_equal; clear g. now rewrite <- eta2. }
      etransitivity.
      2:{ apply Hsub. }
      repeat setoid_rewrite subst_lty_twice; unfold funcomp, subst_swap_fun, subst_beta_ground_fun; cbn.
      apply eqlty_eq. f_equal. fext; intros f xs; f_equal; clear f. now rewrite <- !eta.
    }
    2:{ (* The second subtyping *)
      apply eqmty_lt_one.
      etransitivity.
      2:{ apply Hρ. }
      cbn. repeat setoid_rewrite subst_lty_twice; unfold funcomp, subst_beta_ground_fun; cbn.
      apply eqmty_Quant. 2: hnf; reflexivity.
      apply eqlty_eq. apply f_equal; fext; intros f xs; f_equal; clear f. now rewrite <- eta.
    }
    1:{ (* The final cost *) hnf; intros xs (Hxs1 & Hxs2). rewrite <- HM by auto. reflexivity. }

  - (* App *)
    specialize IH with (y := t1) as IHt1; spec_assert IHt1 by (cbn; lia).
    specialize IH with (y := t2) as IHt2; spec_assert IHt2 by (cbn; lia). clear IH.
    pose proof ctxMSum_unif_lt1 Hmsum HΓ as (Γ'' & Hmsum' & HΓ'').

    eapply tyT_skel_congr. eapply tyT_App.
    + eapply IHt1.
      eapply tyT_sub.
      * apply Hty1.
      * instantiate (1 := subst_ctx Δ1 (fun f xs => f (tl xs))).
        hnf; intros y Hy. setoid_rewrite subst_mty_twice. setoid_rewrite subst_mty_id. reflexivity.
      * rewrite subst_mty_beta_ground_eq_Quant. apply eqmty_Quant.
        2: hnf; reflexivity.
        cbn. apply eqlty_Arr.
        -- instantiate (1 := subst_mty σ (fun f xs => f (hd xs ::: tl (tl xs)))).
           setoid_rewrite subst_mty_twice; unfold funcomp, subst_beta_one_ground_fun; cbn; eta.
           apply eqmty_eq. symmetry. apply subst_mty_id'. intros f xs; f_equal; clear f. now rewrite <- eta.
        -- instantiate (1 := subst_mty τ (fun f xs => f (hd xs ::: tl (tl xs)))).
           setoid_rewrite subst_mty_twice; unfold funcomp, subst_beta_one_ground_fun; cbn; eta.
           apply eqmty_eq. symmetry. apply subst_mty_id'. intros f xs; f_equal; clear f. now rewrite <- eta.
      * hnf; reflexivity.
    + eapply IHt2.
      eapply tyT_sub.
      * apply Hty2.
      * instantiate (1 := subst_ctx Δ2 (fun f xs => f (tl xs))).
        hnf; intros y Hy. setoid_rewrite subst_mty_twice. setoid_rewrite subst_mty_id. reflexivity.
      * repeat setoid_rewrite subst_mty_twice. reflexivity.
      * hnf; reflexivity.
    + apply Hmsum'.
    + apply HΓ''.
    + cbn. firstorder.
    + apply eqmty_lt_one. repeat setoid_rewrite subst_mty_twice; unfold funcomp, subst_beta_ground_fun, iConst; cbn. apply Hρ.
    + rewrite subst_mty_strip. reflexivity.

  - (* Ifz *)
    specialize IH with (y := t1) as IHt1; spec_assert IHt1 by (cbn; lia).
    specialize IH with (y := t2) as IHt2; spec_assert IHt2 by (cbn; lia).
    specialize IH with (y := t3) as IHt3; spec_assert IHt3 by (cbn; lia). clear IH.
    pose proof ctxMSum_unif_lt1 Hmsum HΓ as (Γ'' & Hmsum' & HΓ'').

    eapply tyT_Ifz with
        (J0 := fun xs => J (tl xs))
        (Δ3 := subst_ctx Δ1 (fun f xs => f (tl xs)))
        (Δ4 := subst_ctx Δ2 (fun f xs => f (tl xs))).
    + eapply IHt1.
      eapply tyT_sub.
      * apply Hty1.
      * hnf; intros y Hy. setoid_rewrite subst_mty_twice. setoid_rewrite subst_mty_id. reflexivity.
      * reflexivity.
      * hnf; reflexivity.
    + cbn. apply @tyT_weaken_Φ with (Φ := fun xs => hd xs < 1 /\ J (tl xs) = 0 /\ Φ (tl xs)). 2: firstorder.
      eapply IHt2 with (Φ := fun xs => J xs = 0 /\ Φ xs).
      * eapply tyT_weaken.
        -- apply Hty2.
        -- hnf; intros y Hy. setoid_rewrite subst_mty_twice. setoid_rewrite subst_mty_id. reflexivity.
    + cbn. apply @tyT_weaken_Φ with (Φ := fun xs => hd xs < 1 /\ 1 <= J (tl xs) /\ Φ (tl xs)). 2: firstorder.
      eapply IHt3 with (Φ := fun xs => 1 <= J xs /\ Φ xs).
      * eapply tyT_weaken.
        -- apply Hty3.
        -- hnf; intros y Hy. setoid_rewrite subst_mty_twice; setoid_rewrite subst_mty_id. reflexivity.
    + apply Hmsum'.
    + apply HΓ''.
    + cbn. firstorder.

  - (* Const *) clear IH. eapply tyT_Const. now apply eqmty_lt_one.
    firstorder.
  - (* Succ *) rename t0 into t. specialize (IH t); spec_assert IH by (cbn; lia).
    destruct ρ.
    2:{ exfalso. cbn in Hρ. apply eqmty_Nat_inv'_sig in Hρ. firstorder congruence. }
    apply eqmty_Nat_inv in Hρ. unfold subst_beta_ground_fun, iConst in Hρ.
    eapply tyT_Succ with (K0 := fun xs => K (tl xs)).
    + apply IH; eauto.
    + apply eqmty_lt_one. apply eqmty_Nat. firstorder.
  - (* Pred *) rename t0 into t. specialize (IH t); spec_assert IH by (cbn; lia).
    destruct ρ.
    2:{ exfalso. cbn in Hρ. apply eqmty_Nat_inv'_sig in Hρ. firstorder congruence. }
    apply eqmty_Nat_inv in Hρ. unfold subst_beta_ground_fun, iConst in Hρ.
    eapply tyT_Pred with (K0 := fun xs => K (tl xs)).
    + apply IH; eauto.
    + apply eqmty_lt_one. apply eqmty_Nat. firstorder.
Qed.




Lemma subject_expansion_beta_Lam {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (t v : tm) (ρ : mty ϕ)
      (pcfTy : PCF.hastyT (stripCtx Γ) (App (Lam t) v) (mty_strip ρ)) :
  (Ty! Φ; Γ ⊢(M) (nbeta1 t v) : ρ @ PCF.skel_red (Lam t v) (PCF.strip pcfTy)) ->
  closed (Lam t) ->
  val v -> closed v ->
  existsS N : idx ϕ, (Ty! Φ; Γ ⊢(N) App (Lam t) v : ρ @ PCF.strip pcfTy) ** sem! Φ ⊨ (fun xs : Vector.t nat ϕ => N xs = 1 + M xs).
Proof.
  (* TODO: Rename ρ to τ *)
  intros Hty HcLam Hv Hc.

  (* Inversion *)
  pose proof PCF.tyT_App_inv' pcfTy as (σ_pcf & pcfTy1 & pcfTy2 & ->).
  pose proof PCF.tyT_Lam_inv' pcfTy1 as (? & ? & pcfTy1' & Heq & ->).
  injection Heq as -> ->; pose proof PCF.ty_UIP_refl Heq as ->; cbn in *.
  rewrite (valb_true Hv) in Hty.

  (* Apply converse substitution *)
  pose proof converse_subst1 pcfTy1' pcfTy2 eq_refl Hty HcLam Hv Hc as (N1 & N2 & σ & L1 & L2 & L3 & <-).
  move L1 at bottom; move L2 at bottom; move L3 at bottom. (* L1, L2, L3 *)

  eexists _; split.
  {
    eapply tyT_skel_congr.
    eapply tyT_App with
        (σ0 := subst_mty σ (fun f xs => f (tl xs)))
        (τ := subst_mty ρ (fun f xs => f (tl xs))).
    (* Context bogus first *)
    4: reflexivity.
    3:{ hnf; intros y Hy; exfalso. destruct Hy as [Hy|Hy].
        - eapply free_bound_iff in HcLam; eauto. lia.
        - eapply free_bound_iff in Hc; eauto. lia. }

    1:{ (* Lam t *)
      eapply tyT_Lam with (I := iConst 1).
      - eapply tyT_weaken. apply tyT_unif_lt1. 2: rewrite <- subst_ctx_beta_ground_scons; reflexivity.
        eapply tyT_sub.
        + apply L1.
        + rewrite subst_ctx_beta_ground_scons.
          rewrite subst_ctx_beta_ground_scons.
          apply eqCtx_scons.
          * instantiate (2 := subst_mty σ (fun f xs => f (tl (tl xs)))).
            repeat setoid_rewrite subst_mty_twice; unfold funcomp; cbn.
            unfold subst_beta_ground_fun, iConst; cbn. eta. rewrite subst_mty_id. reflexivity.
          * intros y Hy; exfalso. eapply free_bound_iff in HcLam; eauto. lia.
        + instantiate (1 := subst_mty ρ (fun f xs => f (tl xs))).
          setoid_rewrite subst_mty_twice; unfold funcomp; cbn.
          unfold subst_beta_ground_fun, iConst; cbn. eta. rewrite subst_mty_id. reflexivity.
        + hnf; reflexivity.
      - hnf; intros y Hy; exfalso. eapply free_bound_iff in HcLam; eauto. lia.
      - reflexivity.
      - hnf; reflexivity.
      - setoid_rewrite subst_mty_twice. reflexivity.
    }
    1:{ (* v *)
      eapply tyT_sub; eauto.
      - reflexivity.
      - setoid_rewrite subst_mty_twice; unfold funcomp; cbn.
        unfold subst_beta_ground_fun, iConst; cbn. eta. rewrite subst_mty_id. reflexivity.
      - hnf; reflexivity.
    }
    1:{ hnf; reflexivity. }
    1:{
      repeat setoid_rewrite subst_mty_twice; unfold funcomp; cbn.
      unfold subst_beta_ground_fun, iConst; cbn. eta. rewrite subst_mty_id. reflexivity.
    }
    1:{now repeat setoid_rewrite subst_mty_strip. }
  }
  {
    cbn; hnf in L3|-*; intros xs Hxs. specialize (L3 _ Hxs).
    lia.
  }
  Unshelve. all: refine (fun _ => _); (exact def_nat + exact def_mty).
Qed.


(* TODO: Move *)
Lemma preservation_closed_nbeta2 (t v : tm) :
  bound 2 t ->
  val v -> closed v ->
  closed (nbeta2 t v (Fix t)).
Proof.
  intros.
  apply preservation_closed_nbeta1; auto.
  change (Lam (nsubst t 1 (Fix t))) with (nsubst (Lam t) 0 (Fix t)).
  apply preservation_closed_nbeta1; auto.
  isVal.
Qed.



Lemma lty_if_shift_one_sub {ϕ} (f : Vector.t nat (S (S ϕ)) -> bool) (A1 A2 : lty (S (S ϕ))) (i : idx ϕ) :
  lty_strip A1 = lty_strip A2 ->
  lty_shift_one_sub (lty_if f A1 A2) i =
  lty_if (fun xs => f (hd xs ::: hd (tl xs) - i (tl (tl xs)) ::: tl (tl xs))) (lty_shift_one_sub A1 i) (lty_shift_one_sub A2 i).
Proof.
  intros H.
  rewrite lty_shift_one_sub_correct'.
  rewrite lty_if_subst_var_beta by auto.
  rewrite <- !lty_shift_one_sub_correct'.
  f_equal. erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0; cbn.
  unfold subst_beta_fun; cbn.
  fext; intros xs. f_equal. simp_vect_to_list. f_equal.
  - now rewrite vect_cast_hd.
  - f_equal. f_equal.
    + f_equal. now simp_vect_to_list.
    + f_equal. now simp_vect_to_list.
  Unshelve. reflexivity.
Qed.



(** This is the interesting part of the fix case of subject expansion. *)

Lemma subject_expansion_beta_Fix_retyping {ϕ} (Φ : constr ϕ) (M N : idx ϕ) (t : tm) (σ τ : mty (S ϕ)) (ρ_fix : mty ϕ) (s : skel) :
  (Ty! Φ; ρ_fix .: def_ctx ⊢(M) Lam t : [ < iConst 1]⋅ σ ⊸ τ @ skel_Lam s) ->
  (Ty! Φ;          def_ctx ⊢(N) Fix t : ρ_fix @ skel_Fix (skel_Lam s)) ->
  mty_strip ρ_fix = PCF.Arr (mty_strip σ) (mty_strip τ) ->
  closed (Fix t) ->
  Ty! Φ; def_ctx ⊢(fun xs => M xs + N xs)
         Fix t : [ < iConst 1]⋅ σ ⊸ τ @ PCF.Skel.skel_Fix (PCF.Skel.skel_Lam s).
Proof.
  intros Hty_lam Hty_fix Hstrip Hclos.

  (* First we invert the fix typing *)
  apply dlPCF_Joining.tyT_Fix_inv in Hty_fix
    as (Ib & Δ & Jb & A & B & C & K & card1 & card2 & cardH & Γ' & s' & Hty & Hsub & HcardH & Hcard1 & Hcard2
        & Hbsum & HΓ & HM & HB & -> & [= <-]).

  (* Contexts are futile, so let's remove them *)
  clear HΓ Hbsum Γ'.
  apply retypeT_free with (Σ := [ < Ib]⋅ A .: def_ctx) in Hty; auto.
  2:{ intros [ | y] Hy; cbn; auto. exfalso. eapply free_closed_iff in Hclos; eauto. }
  clear Δ.

  (* Note that [Ib] describes a forest consisting of [K] trees. *)
  (* We will build a new forest that consists of exactly one tree, which has the [K] trees as children. *)

  pose (Ib' := fun xs : Vector.t nat (S ϕ) =>
                 if hd xs =? 0 then K (tl xs) (* The new root node has [K] children *)
                 else Ib (hd xs - 1 ::: tl xs)).
  pose (Jb' := fun xs : Vector.t nat (S ϕ) =>
                 if hd xs =? 0 then M (tl xs) (* The cost of the root is simply the cost of the lambda typing *)
                 else Jb (hd xs - 1 ::: tl xs)).
  pose (card1' := fun xs : Vector.t nat (S (S ϕ)) =>
                 if hd (tl xs) =? 0 (* b *) then card2 (hd xs ::: tl (tl xs))
                 else card1 (hd xs ::: hd (tl xs) - 1 ::: tl (tl xs))).
  pose (card2' := iConst 0 : idx (S ϕ)); cbn in card2'.

  pose (A' := lty_if (fun xs : Vector.t nat (S (S ϕ)) => hd (tl xs) (* b *) =? 0)
                     (subst_lty C (fun f xs => f (hd xs ::: tl (tl xs))) (* Make [b] free in [C] *))
                     (lty_shift_one_sub A (iConst 1))).
  pose (B' := lty_if (fun xs : Vector.t nat (S (S ϕ)) => hd (tl xs) (* b *) =? 0)
                     (subst_lty (σ ⊸ τ) (fun f xs => f (hd xs ::: tl (tl xs))) (* Make [b] free in [σ ⊸ τ] *))
                     (lty_shift_one_sub B (iConst 1))).

  (* Some often needed propositions *)
  assert (PCF_Types.Arr (mty_strip σ) (mty_strip τ) = lty_strip B /\
          PCF_Types.Arr (mty_strip σ) (mty_strip τ) = lty_strip A /\
          PCF_Types.Arr (mty_strip σ) (mty_strip τ) = lty_strip C) as (HB_strip & HA_strip & HC_strip).
  {
    apply eqlty_strip in Hsub. setoid_rewrite subst_lty_strip in Hsub.
    apply eqlty_strip in HB. setoid_rewrite subst_lty_strip in HB. cbn in Hstrip.
    repeat_split; congruence.
  }
  assert (forall y, ~ free (S y) (Lam t)) as HcFix'.
  { intros y Hy. eapply free_bound_iff in Hy; eauto. lia. }


  eapply tyT_Fix with
      (Ib0 := Ib') (A0 := A') (B0 := B')
      (Jb0 := Jb')
      (K0 := iConst 1)
      (cardH0 := fun xs => S (cardH xs)) (card3 := card1') (card4 := card2')
      (Δ := def_ctx) (Γ' := def_ctx).
  1:{ (* The typing of [Lam t] *)

    eapply tyT_weaken_Φ with
        (Φ0 := fun xs : Vector.t nat (S ϕ) =>
                if hd xs (* b *) =? 0 then hd xs < 1 /\ Φ (tl xs)
                else 1 <= hd xs /\ hd xs < S (cardH (tl xs)) /\ Φ (tl xs)).
    2:{ hnf; intros xs (Hxs1 & Hxs2). destruct Nat.eqb eqn:E; split; auto.
        - apply Nat.eqb_eq in E. lia.
        - apply Nat.eqb_neq in E. lia. }

    (* Before applying the case distinction lemma, we first have to bring the types to the right shape *)
    eapply tyT_sub with
        (Γ1 := (* This essentially the same as [Quant Ib' A' .: def_ctx] *)
           dlPCF_Joining.ctx_if
             (fun xs => hd xs =? 0)
             ((Quant (fun xs => K (tl xs)) (subst_lty C (fun (f : idx (S ϕ)) (xs : Vector.t nat (S (S ϕ))) => f (hd xs ::: tl (tl xs))))) .: def_ctx)
             ((Quant (fun xs => Ib (hd xs - 1 ::: tl xs)) (lty_shift_one_sub A (iConst 1))) .: def_ctx))
        (I2 := Jb') (* No subtyping here; this is already in the right form *)
        (τ1 :=
           mty_if (fun xs => hd xs =? 0)
                  (Quant (iConst 1) (subst_lty (σ ⊸ τ) (fun (f : idx (S ϕ)) (xs : Vector.t nat (S (S ϕ))) => f (hd xs ::: tl (tl xs)))))
                  (Quant (iConst 1) ((lty_shift_one_sub B (iConst 1))))).
    2:{ (* The context *)
      setoid_rewrite dlPCF_Joining.ctx_if_scons. apply eqCtx_scons.
        - reflexivity.
        - firstorder. }
    2:{ (* The type (B) *)
      cbn [mty_if]. apply eqmty_Quant.
      2:{ hnf; intros xs Hsx. destruct Nat.eqb; reflexivity. }
      change (iConst ?x ?xs) with x. unfold B'.
      eapply eqlty_mono_Φ.
      apply eqlty_if_cases.
      - reflexivity.
      - reflexivity.
      - cbn; now setoid_rewrite subst_lty_strip; setoid_rewrite subst_mty_strip.
      - cbn; now setoid_rewrite subst_lty_strip; setoid_rewrite subst_mty_strip.
      - hnf; intros xs (Hxs1 & Hxs2). destruct Nat.eqb; apply Hxs2.
    }
    2:{ hnf; reflexivity. }

    (* Now we can apply the case distinction lemma *)
    eapply dlPCF_Joining.tyT_cases.
    { (* Case [b < 1] (the root). We drop [b] by using [tyT_unif_lt1]. Then we try to use the original lambda typing. *)
      apply tyT_unif_lt1. eapply tyT_sub.
      - apply Hty_lam.
      - setoid_rewrite subst_ctx_beta_ground_scons. apply eqCtx_scons.
        + cbn. apply eqmty_Quant. 2: hnf; reflexivity.
          apply eqlty_eq. unfold subst_beta_ground_fun, iConst; cbn.
          setoid_rewrite subst_lty_twice; unfold funcomp; cbn.
          apply subst_lty_id'. intros; f_equal; now rewrite <- eta.
        + firstorder.
      - rewrite subst_mty_beta_ground_eq_Quant. apply eqmty_Quant. 2: hnf; reflexivity.
        apply eqlty_eq. unfold subst_beta_one_ground_fun, iConst; cbn.
        unfold subst_beta_one_ground_fun; cbn.
        setoid_rewrite subst_mty_twice; unfold funcomp; cbn.
        f_equal; symmetry; apply subst_mty_id'; intros; now rewrite <- eta.
      - hnf; reflexivity.
    }
    { (* Case [1 <= b < 1 + cardH]. We just shift the Lam typing (from the fix) by one *)
      eapply tyT_sub.
      - eapply tyT_weaken_Φ.
        + apply (tyT_shift_sub (iConst 1) Hty).
        + clear. unfold shift_sub_fun, iConst; cbn. hnf; intros xs (Hxs1 & Hxs2 & Hxs3); split; cbn; auto. lia.
      - rewrite ctx_shift_sub_scons. apply eqCtx_scons.
        + reflexivity.
        + firstorder.
      - reflexivity.
      - hnf; reflexivity.
    }
    { (* Just some easy stripping left *)
      intros [ | y] Hy.
      - cbn. setoid_rewrite subst_lty_strip. congruence.
      - exfalso. eapply free_bound_iff in Hy; eauto. lia. }
    { cbn. setoid_rewrite subst_mty_strip. setoid_rewrite subst_lty_strip. congruence. }
  }
  7:{ (* The final cost *)
    hnf; intros xs Hxs.
    rewrite <- HM by auto.
    rewrite Sum_eq_S'.
    setoid_rewrite Sum_ext at 1.
    2:{ intros a Ha. cbn. rewrite Nat.sub_0_r. reflexivity. }
    cbn. lia.
  }
  2:{ (* The complete card *) revert HcardH; clear_all; intros; cbn.
    hnf; intros xs Hxs. unfold iConst; subst Ib'; cbn.
    replace (S (cardH xs)) with (S (cardH xs + 0)) by lia.
    apply isForestCard_S.
    - cbn. eapply isForestCard_ext; eauto. cbn; intros. f_equal; f_equal. lia.
    - apply isForestCard_0.
  }
  3:{ (* The second aux card *) revert Hcard2; clear_all; intros; cbn.
      subst Ib' card2'; unfold iConst; cbn.
      hnf; intros xs (Hxs1 & Hxs2). apply lt_1_eq_0 in Hxs1. rewrite Hxs1. apply isForestCard_0.
  }
  2:{(* The first aux card *) revert Hcard1 Hcard2; clear_all; intros; cbn.
     subst Ib' card1'; cbn.
     hnf; intros xs (Hxs1 & Hxs2 & Hxs3); set (a := hd xs) in *; set (b := hd (tl xs)) in *; set (xs' := tl (tl xs)) in *.
     destruct b; cbn in *.
     - eapply isForestCard_ext. 2: intros; symmetry; rewrite Nat.sub_0_r; reflexivity.
       apply (Hcard2 (a ::: xs')); cbn; auto.
     - rewrite Nat.sub_0_r in Hxs1. rewrite Nat.sub_0_r.
       apply (Hcard1 (a ::: b ::: xs')). cbn; repeat_split; auto. lia.
  }
  1:{ (* The first subtyping *)
    subst A' B'.
    rewrite <- dlPCF_Joining.subst_beta_Fix1_fun_correct.
    setoid_rewrite <- subst_lty_twice.
    setoid_rewrite dlPCF_Joining.lty_if_subst_beta_one_two.
    2:{ setoid_rewrite subst_lty_strip. cbn; auto. }
    setoid_rewrite dlPCF_Joining.lty_if_subst_beta_ground.
    2:{ cbn. repeat setoid_rewrite subst_lty_strip. repeat setoid_rewrite subst_mty_strip; cbn. auto. }
    cbn -[lty_if].
    etransitivity. apply eqlty_if_right.
    { repeat setoid_rewrite subst_lty_twice; unfold funcomp; cbn.
      repeat setoid_rewrite subst_mty_strip. repeat setoid_rewrite subst_lty_strip. auto. }
    { hnf; reflexivity. }

    (* Here we need a case distinction on [b = 0]. We first have to bring the constraint to the right shape. *)
    apply @eqlty_mono_Φ with
        (Φ := fun xs : Vector.t nat (S (S ϕ)) =>
                if hd (tl xs) =? 0 then
                  hd (tl xs) < 1 /\ hd xs < Ib' (hd (tl xs) ::: tl (tl xs)) /\ hd (tl xs) < S (cardH (tl (tl xs))) /\ Φ (tl (tl xs))
                else 1 <= hd (tl xs) /\ hd xs < Ib' (hd (tl xs) ::: tl (tl xs)) /\ hd (tl xs) < S (cardH (tl (tl xs))) /\ Φ (tl (tl xs))).
    2:{ hnf; intros xs (Hxs1 & Hxs2 & Hxs3). destruct Nat.eqb eqn:E.
        - apply Nat.eqb_eq in E. repeat_split; auto. lia.
        - apply Nat.eqb_neq in E. repeat_split; auto. lia. }
    apply eqlty_if_cases'.
    - (* We have to swap #0 and #1 and then we make use of [b<1] in the goal. *)
      apply eqlty_swap, eqlty_lt_one'; cbn.
      repeat setoid_rewrite subst_lty_twice; unfold funcomp, subst_beta_ground_fun, shift_one_sub_fun, dlPCF_Splitting.subst_beta_one_two_fun, iConst, subst_swap_fun; cbn.
      rewrite subst_lty_id' by (intros; f_equal; now rewrite <- eta).
      etransitivity.
      2:{ eapply eqlty_mono_Φ. apply HB. firstorder. }
      apply eqlty_eq. f_equal; fext; intros f xs. cbn. repeat f_equal. rewrite <- eta. lia.
    - (* Here we shift [Hsub] by one *)
      apply eqlty_shift_one_sub with (I := iConst 1) in Hsub; cbn in Hsub.
      etransitivity.
      2:{
        eapply eqlty_mono_Φ.
        - apply Hsub.
        - hnf; intros xs (Hxs1 & Hxs2 & Hxs3 & Hxs4). unfold iConst. repeat_split; auto.
          + unfold Ib' in Hxs2; cbn in Hxs2. now rewrite (proj2 (Nat.eqb_neq _ _)) in Hxs2 by lia.
          + lia.
      }
      repeat setoid_rewrite subst_lty_twice.
      unfold funcomp, shift_one_sub_fun, iConst, subst_beta_ground_fun, dlPCF_Splitting.subst_beta_one_two_fun, card1'; cbn.
      apply eqlty_subst_idx_sem.
      hnf; intros xs (Hxs1 & Hxs2 & Hxs3 & Hxs4). intros j; f_equal; clear j.
      rewrite (proj2 (Nat.eqb_neq _ _)) by lia.
      f_equal. f_equal. lia.
    - repeat setoid_rewrite subst_lty_strip. congruence.
  }
  3:{ (* The final subtyping *)
    apply eqmty_Quant. 2: hnf; reflexivity.
    apply eqlty_lt_one.
    unfold B'.

    rewrite <- dlPCF_Joining.subst_beta_Fix2_fun_correct.
    unfold dlPCF_Joining.subst_beta_Fix2_fun.
    setoid_rewrite <- subst_lty_twice.

    setoid_rewrite dlPCF_Joining.lty_if_subst_beta_one_one.
    2:{ setoid_rewrite subst_lty_strip. cbn; auto. }
    setoid_rewrite dlPCF_Joining.lty_if_subst_beta_ground.
    2:{ cbn. repeat setoid_rewrite subst_lty_strip. repeat setoid_rewrite subst_mty_strip; cbn. auto. }
    setoid_rewrite dlPCF_Joining.lty_if_subst_beta_ground.
    2:{ cbn. repeat setoid_rewrite subst_lty_strip. repeat setoid_rewrite subst_mty_strip; cbn. auto. }

    etransitivity. apply eqlty_if_left.
    - repeat setoid_rewrite subst_lty_twice; unfold funcomp; cbn.
      repeat setoid_rewrite subst_mty_strip. repeat setoid_rewrite subst_lty_strip. auto.
    - hnf; reflexivity.
    - repeat setoid_rewrite subst_lty_twice; unfold funcomp; cbn. reflexivity.
  }
  1:{ (* Bogus context stuff last *) hnf; intros y Hy. firstorder. }
  1:{ (* Bogus context stuff last *) hnf; intros y Hy. firstorder. }
Qed.


Lemma subject_expansion_beta_Fix {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (t v : tm) (ρ : mty ϕ)
      (pcfTy : PCF.hastyT (stripCtx Γ) (App (Fix t) v) (mty_strip ρ)) :
  (Ty! Φ; Γ ⊢(M) nbeta2 t v (Fix t) : ρ @ PCF.skel_red (App (Fix t) v) (PCF.strip pcfTy)) ->
  closed (Fix t) ->
  val v -> closed v ->
  existsS N : idx ϕ, (Ty! Φ; Γ ⊢(N) App (Fix t) v : ρ @ PCF.strip pcfTy) ** sem! Φ ⊨ (fun xs : Vector.t nat ϕ => N xs = 1 + M xs).
Proof.
  intros Hty HcFix Hv Hc.
  cbn in HcFix.

  (* Inversion of the PCF typing *)
  pose proof PCF.tyT_App_inv' pcfTy as (σ_pcf & pcfTy1 & pcfTy2 & ->).
  pose proof PCF.tyT_Fix_inv' pcfTy1 as (pcfTy1' & ->).
  pose proof PCF.tyT_Lam_inv' pcfTy1' as (? & ? & pcfTy1 & Heq & ->).
  injection Heq as -> ->. pose proof PCF.ty_UIP_refl Heq as ->. cbn in *.
  rewrite valb_true in Hty by auto.
  set (s1 := PCF.strip pcfTy1) in *; set (s2 := PCF.strip pcfTy2) in *.
  set (s := PCF.skel_subst 0 (nsubst t 1 (Fix t)) (PCF.skel_subst 1 t s1 (PCF.Skel.skel_Fix (PCF.Skel.skel_Lam s1))) s2) in *.

  unshelve evar (pcfTy1_Lam : PCF.hastyT (stripCtx Γ) (Lam (nsubst t 1 (Fix t))) (PCF_Types.Arr σ_pcf (mty_strip ρ))).
  {
    eapply PCF.tyT_Lam.
    eapply PCF.typepresT_nsubst with (Γ := σ_pcf .: (PCF_Types.Arr σ_pcf (mty_strip ρ) .: stripCtx Γ)).
    - apply pcfTy1.
    - eauto.
    - eauto.
    - apply PCF.tyT_Fix, PCF.tyT_Lam. eauto.
    - abstract (hnf; split;
                [ intros y **; assert (y = 0) as -> by lia; cbn; reflexivity
                | reflexivity]).
  }
  unshelve evar (pcfTy1_app_lam : PCF.hastyT (stripCtx Γ) (App (Lam (nsubst t 1 (Fix t))) v) (mty_strip ρ)).
  {
    eapply PCF.tyT_App with (τ1 := σ_pcf).
    - apply pcfTy1_Lam.
    - apply pcfTy2.
  }

  assert (PCF.skel_red (App (Lam (nsubst t 1 (Fix t))) v) (PCF.strip pcfTy1_app_lam) = s) as H_s.
  { subst pcfTy1_app_lam s. cbn. rewrite PCF.typepresT_subst_skel; cbn. now rewrite valb_true by auto. }

  (* We can type [(Lam x. t{Fix f x. t / f}) v] using the Lam case. This means we get a typing for [Lam ...] and [v]. *)
  pose proof subject_expansion_beta_Lam (Φ := Φ) (M := M) pcfTy1_app_lam as X.
  rewrite H_s in X.
  spec_assert X by exact Hty.
  spec_assert X.
  { apply nsubst_bound'; auto. }
  do 2 spec_assert X by assumption.
  destruct X as (N & X1 & X2).
  inv X1.

  enough (Ty! Φ; def_ctx ⊢(fun xs : Vector.t nat ϕ => K1 xs) Fix t : [ < iConst 1]⋅ σ ⊸ τ @ PCF.Skel.skel_Fix (PCF.Skel.skel_Lam s1)) as Y.
  {
    eexists; split.
    {
      eapply tyT_App.
      1:{ apply Y. }
      2:{ hnf; intros y Hy. exfalso. apply free_closed_iff in Hy; cbn; eauto. }
      2:{ hnf; intros y Hy. exfalso. apply free_closed_iff in Hy; cbn; eauto. }
      2:{ hnf; reflexivity. }
      all: eassumption.
    }
    {
      hnf; intros xs Hxs.
      replace (S (M xs)) with (N xs) by auto.
      hnf in HM. now apply HM.
    }
  }

  (* Apply converse substitution on [Hty1], to get a typing of the fixpoint *)
  rewrite PCF.typepresT_subst_skel in Hty1.
  change (Lam (nsubst t 1 (Fix t))) with (nsubst (Lam t) 0 (Fix t)) in Hty1.
  change (skel_Lam (PCF.skel_subst 1 t ?x ?y)) with (PCF.skel_subst 0 (Lam t) (skel_Lam x) y) in Hty1.
  change (nsubst ?x 0 ?y) with (nbeta1 x y) in Hty1.

  unshelve epose proof converse_subst1 (t := Lam t)
           (σ_pcf := PCF_Types.Arr (mty_strip σ) (mty_strip ρ)) (τ_pcf := (PCF_Types.Arr (mty_strip σ) (mty_strip ρ)))
           _ _ _
           (tyT_skel_congr Hty1 _) as Y.
  1:{ (* [PCF.hastyT (PCF_Types.Arr (mty_strip σ) (mty_strip ρ) .: stripCtx Δ1) (Lam t) (PCF_Types.Arr (mty_strip σ) (mty_strip ρ))] *)
    eapply PCF.retypeT_free.
    - exact (PCF.tyT_Lam pcfTy1).
    - abstract (intros [ | y] Hy; cbn;
                [ reflexivity
                | exfalso; eapply free_bound_iff in Hy; eauto; lia]).
  }
  1:{
    eapply PCF.retypeT_closed.
    - assumption.
    - exact (PCF.tyT_Fix (PCF.tyT_Lam pcfTy1)).
  }
  1:{
    cbn. apply eqmty_strip in Hρ. setoid_rewrite subst_mty_strip in Hρ. congruence.
  }
  { now rewrite PCF.retypeT_free_skel, PCF.retypeT_closed_skel. }
  specialize Y with (1 := HcFix) (2 := val_fix _) (3 := HcFix) as (M' & N' & σ_fix & Hty_lam & Hty_fix & HK & Hσ_fix_strip).
  rewrite PCF.retypeT_free_skel in Hty_lam. rewrite PCF.retypeT_closed_skel in Hty_fix.

  (* We can now forget everything about [v] *)
  clear dependent v. clear dependent N. clear K2. rename K1 into K. rename s1 into s. clear Hty1.
  clear M. rename M' into M, N' into N. clear pcfTy1_Lam.
  change (PCF.strip (PCF.tyT_Lam pcfTy1)) with (skel_Lam s) in Hty_lam.
  change (PCF.strip (PCF.tyT_Fix (PCF.tyT_Lam pcfTy1))) with (skel_Fix (skel_Lam s)) in Hty_fix.

  (* The context [Δ1] is irrelevant *)
  eapply retypeT_free with (Σ := σ_fix .: def_ctx) in Hty_lam.
  2:{ intros [ | y] Hy.
      - reflexivity.
      - exfalso. eapply free_bound_iff in Hy; eauto. lia.
  }
  apply retypeT_closed with (Σ := def_ctx) (2 := HcFix) in Hty_fix.
  clear Δ1 Δ2.

  (* It is left to show that [Fix t] can be re-typed. We do this in the lemma above. *)
  eapply tyT_sub'.
  - eapply subject_expansion_beta_Fix_retyping; eauto.
    { apply eqmty_strip in Hρ. setoid_rewrite subst_mty_strip in Hρ. congruence. }
  - reflexivity.
  - firstorder.

  Unshelve. exact def_ctx.
Qed.



Lemma subject_expansion_beta {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ)
      (t t' : tm) (s s' : skel) (* Terms & skeletons before and after the step *)
      (ρ : mty ϕ) (ρ_PCF : PCF.ty)
      (pcfTy : PCF.hastyT (stripCtx Γ) t ρ_PCF) : (* PCF typing before the step *)
  (Ty! Φ; Γ ⊢(M) t' : ρ @ s') -> (* dlPCF typing after the step *)
  stepT t β t' -> (* We only consider β substitution steps *)
  ρ_PCF = mty_strip ρ ->
  s = PCF.strip pcfTy ->
  s' = PCF.skel_red t s ->
  closed t ->
  existsS (N : idx ϕ),
    (Ty! Φ; Γ ⊢(N) t : ρ @ s) **
    (sem! Φ ⊨ fun xs => N xs = 1 + M xs).
Proof.
  intros Hty Hstep Heqρ Heqs Heqs' Hclos.
  remember β as κ.
  induction Hstep in s,s',ϕ,Φ,Γ,M,ρ,Hty,ρ_PCF,Heqκ,Heqρ,Heqs,Heqs',pcfTy,Hclos; subst; try discriminate; closed_inv.
  - (* App left *)
    inv Hty.
    specialize IHHstep with (1 := Hty1) (2 := eq_refl) (3 := eq_refl).
    pose proof PCF.tyT_App_inv' pcfTy as (σ_PCF & pcfTy1 & pcfTy2 & ->); cbn in *.
    erewrite stepT_valb_false in H by eauto. injection H as [= <- -> ->].
    specialize IHHstep with (2 := eq_refl) (3 := Hclos).
    unshelve (epose proof eq_refl ?[pcfTy1']; specialize IHHstep with (pcfTy := ?pcfTy1')).
    {
      eapply PCF_tyT_congr with (τ1 := PCF_Types.Arr (mty_strip σ) (mty_strip ρ)).
      2:{ f_equal. eapply eqmty_strip in Hρ. now setoid_rewrite subst_mty_strip in Hρ. }
      apply PCF.retypeT_closed with (Γ := stripCtx Γ); auto.
    }
    clear H. spec_assert IHHstep.
    { clear IHHstep. setoid_rewrite PCF_tyT_congr_skel. now rewrite PCF.retypeT_closed_skel. }
    destruct IHHstep as (N & IH1 & IH2).
    eexists _; split.
    { eapply tyT_App; eauto.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto.
      - hnf; reflexivity. }
    {
      hnf; intros xs Hxs. rewrite <- HM by auto. hnf in IH2; specialize (IH2 _ Hxs). lia.
    }
  - (* App right *)
    inv Hty.
    specialize IHHstep with (1 := Hty2) (2 := eq_refl) (3 := eq_refl).
    pose proof PCF.tyT_App_inv' pcfTy as (σ_PCF & pcfTy1 & pcfTy2 & ->); cbn in *.
    rewrite valb_true in H by auto. erewrite stepT_valb_false in H by eauto. injection H as [= <- -> ->].
    specialize IHHstep with (2 := eq_refl) (3 := Hclos0).
    unshelve (epose proof eq_refl ?[pcfTy2']; specialize IHHstep with (pcfTy := ?pcfTy2')).
    {
      eapply PCF_tyT_congr with (τ1 := mty_strip σ).
      2:{ f_equal. now setoid_rewrite subst_mty_strip. }
      apply PCF.retypeT_closed with (Γ := stripCtx Γ); auto.
    }
    clear H. spec_assert IHHstep.
    { clear IHHstep. setoid_rewrite PCF_tyT_congr_skel. now rewrite PCF.retypeT_closed_skel. }
    destruct IHHstep as (N & IH1 & IH2).
    eexists _; split.
    { eapply tyT_App; eauto.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto.
      - hnf; reflexivity. }
    {
      hnf; intros xs Hxs. rewrite <- HM by auto. hnf in IH2; specialize (IH2 _ Hxs). lia.
    }

    (* The two complicated cases *)
  - (* App β lam *) now apply subject_expansion_beta_Lam.
  - (* App β fix *) now apply subject_expansion_beta_Fix.

  - (* Ifz *)
    inv Hty.
    specialize IHHstep with (1 := Hty1) (2 := eq_refl) (3 := eq_refl).
    pose proof PCF.tyT_Ifz_inv' pcfTy as (pcfTy1 & pcfTy2 & pcfTy3 & ->). cbn in *.

    (* The cases [t1 = Const _] are excluded, because this wouldn't be a [β] step then *)
    assert (skel_Ifz s1 s2 s3 = PCF.Skel.skel_Ifz (PCF.skel_red t1 (PCF.strip pcfTy1)) (PCF.strip pcfTy2) (PCF.strip pcfTy3)) as [= -> -> ->].
    { destruct t1; auto. inv Hstep. }
    clear H.

    specialize IHHstep with (2 := eq_refl) (3 := Hclos).
    unshelve (epose proof eq_refl ?[pcfTy1']; specialize IHHstep with (pcfTy := ?pcfTy1')).
    { apply PCF.retypeT_closed with (Γ := stripCtx Γ); auto. }
    clear H. spec_assert IHHstep.
    { clear IHHstep. now rewrite PCF.retypeT_closed_skel. }
    destruct IHHstep as (N & IH1 & IH2).
    eexists _; split.
    { eapply tyT_Ifz; eauto.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto.
      - hnf; reflexivity. }
    {
      hnf; intros xs Hxs. rewrite <- HM by auto. hnf in IH2; specialize (IH2 _ Hxs). lia.
    }

  - (* Succ *)
    inv Hty.
    specialize IHHstep with (1 := Hty0) (2 := eq_refl) (3 := eq_refl).
    pose proof PCF_tyT_S_inv' pcfTy as (Heq & pcfTy' & ->).
    pose proof mty_strip_eq_Nat_inv _ Heq as (K' & ->); pose proof PCF.ty_UIP_refl Heq as ->; cbn in *.
    assert (skel_S s = PCF.Skel.skel_S (PCF.skel_red t (PCF.strip pcfTy'))) as [= ->].
    { destruct t; auto. inv Hstep. }
    clear H.
    specialize IHHstep with (2 := eq_refl) (3 := Hclos).
    specialize IHHstep with (pcfTy := pcfTy') (1 := eq_refl) as (N & IH1 & IH2).
    eexists _; split.
    { eapply tyT_Succ; eauto. }
    { auto. }
  - (* Pred *)
    inv Hty.
    specialize IHHstep with (1 := Hty0) (2 := eq_refl) (3 := eq_refl).
    pose proof PCF_tyT_P_inv' pcfTy as (Heq & pcfTy' & ->).
    pose proof mty_strip_eq_Nat_inv _ Heq as (K' & ->); pose proof PCF.ty_UIP_refl Heq as ->; cbn in *.
    assert (skel_P s = PCF.Skel.skel_P (PCF.skel_red t (PCF.strip pcfTy'))) as [= ->].
    { destruct t; auto. inv Hstep. }
    clear H.
    specialize IHHstep with (2 := eq_refl) (3 := Hclos).
    specialize IHHstep with (pcfTy := pcfTy') (1 := eq_refl) as (N & IH1 & IH2).
    eexists _; split.
    { eapply tyT_Pred; eauto. }
    { auto. }
  Unshelve. all: exact def_ctx. (* Some bogus contexts *)
Qed.


Lemma subject_expansion_nat {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ)
      (t t' : tm) (s s' : skel) (* Terms & skeletons before and after the step *)
      (ρ : mty ϕ) (ρ_PCF : PCF.ty)
      (pcfTy : PCF.hastyT (stripCtx Γ) t ρ_PCF) : (* PCF typing before the step *)
  (Ty! Φ; Γ ⊢(M) t' : ρ @ s') -> (* dlPCF typing after the step *)
  stepT t ϵ t' -> (* We only consider nat computations here *)
  ρ_PCF = mty_strip ρ ->
  s = PCF.strip pcfTy ->
  s' = PCF.skel_red t s ->
  closed t ->
  (Ty! Φ; Γ ⊢(M) t : ρ @ s).
Proof.
  intros Hty Hstep Heqρ Heqs Heqs' Hclos.
  remember ϵ as κ.
  induction Hstep in s,s',ϕ,Φ,Γ,M,ρ,Hty,ρ_PCF,Heqκ,Heqρ,Heqs,Heqs',pcfTy,Hclos; subst; try discriminate; closed_inv.
  - (* App left *)
    inv Hty.
    specialize IHHstep with (1 := Hty1) (2 := eq_refl) (3 := eq_refl).
    pose proof PCF.tyT_App_inv' pcfTy as (σ_PCF & pcfTy1 & pcfTy2 & ->); cbn in *.
    erewrite stepT_valb_false in H by eauto. injection H as [= <- -> ->].
    specialize IHHstep with (2 := eq_refl) (3 := Hclos).
    unshelve (epose proof eq_refl ?[pcfTy1']; specialize IHHstep with (pcfTy := ?pcfTy1')).
    {
      eapply PCF_tyT_congr with (τ1 := PCF_Types.Arr (mty_strip σ) (mty_strip ρ)).
      2:{ f_equal. eapply eqmty_strip in Hρ. now setoid_rewrite subst_mty_strip in Hρ. }
      apply PCF.retypeT_closed with (Γ := stripCtx Γ); auto.
    }
    clear H. spec_assert IHHstep.
    { clear IHHstep. setoid_rewrite PCF_tyT_congr_skel. now rewrite PCF.retypeT_closed_skel. }
    { eapply tyT_App; eauto.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto.
    }
  - (* App right *)
    inv Hty.
    specialize IHHstep with (1 := Hty2) (2 := eq_refl) (3 := eq_refl).
    pose proof PCF.tyT_App_inv' pcfTy as (σ_PCF & pcfTy1 & pcfTy2 & ->); cbn in *.
    rewrite valb_true in H by auto. erewrite stepT_valb_false in H by eauto. injection H as [= <- -> ->].
    specialize IHHstep with (2 := eq_refl) (3 := Hclos0).
    unshelve (epose proof eq_refl ?[pcfTy2']; specialize IHHstep with (pcfTy := ?pcfTy2')).
    {
      eapply PCF_tyT_congr with (τ1 := mty_strip σ).
      2:{ f_equal. now setoid_rewrite subst_mty_strip. }
      apply PCF.retypeT_closed with (Γ := stripCtx Γ); auto.
    }
    clear H. spec_assert IHHstep.
    { clear IHHstep. setoid_rewrite PCF_tyT_congr_skel. now rewrite PCF.retypeT_closed_skel. }
    { eapply tyT_App; eauto.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto. }
    (* The two complicated cases *)

  - (* Ifz (context) *)
    inv Hty.
    specialize IHHstep with (1 := Hty1) (2 := eq_refl) (3 := eq_refl).
    pose proof PCF.tyT_Ifz_inv' pcfTy as (pcfTy1 & pcfTy2 & pcfTy3 & ->). cbn in *.

    (* The cases [t1 = Const _] are excluded, because this wouldn't be a [β] step then *)
    assert (skel_Ifz s1 s2 s3 = PCF.Skel.skel_Ifz (PCF.skel_red t1 (PCF.strip pcfTy1)) (PCF.strip pcfTy2) (PCF.strip pcfTy3)) as [= -> -> ->].
    { destruct t1; auto. inv Hstep. }
    clear H.

    specialize IHHstep with (2 := eq_refl) (3 := Hclos).
    unshelve (epose proof eq_refl ?[pcfTy1']; specialize IHHstep with (pcfTy := ?pcfTy1')).
    { apply PCF.retypeT_closed with (Γ := stripCtx Γ); auto. }
    clear H. spec_assert IHHstep.
    { clear IHHstep. now rewrite PCF.retypeT_closed_skel. }
    { eapply tyT_Ifz; eauto.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto. }

  - (* Ifz zero *)
    cbn in Hty.
    pose proof PCF.tyT_Ifz_inv' pcfTy as (pcfTy1 & pcfTy2 & pcfTy3 & ->). cbn in *.
    pose proof PCF.tyT_Const_inv' pcfTy1 as ->; cbn in *.
    { eapply tyT_Ifz with (M2 := M).
      - apply tyT_Const. reflexivity. hnf; reflexivity.
      - eapply tyT_weaken_Φ; eauto. firstorder.
      - eapply tyT_sub. eapply tyT_exfalso.
        + unfold iConst. hnf; firstorder lia.
        + reflexivity.
        + reflexivity.
        + unfold iConst. hnf; firstorder lia.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto.
      - reflexivity.
      - hnf; reflexivity.
    }
  - (* Ifz nozero *)
    cbn in Hty.
    pose proof PCF.tyT_Ifz_inv' pcfTy as (pcfTy1 & pcfTy2 & pcfTy3 & ->). cbn in *.
    pose proof PCF.tyT_Const_inv' pcfTy1 as ->; cbn in *.
    { eapply tyT_Ifz with (M2 := M).
      - apply tyT_Const. reflexivity. hnf; reflexivity.
      - eapply tyT_sub. eapply tyT_exfalso.
        + unfold iConst. hnf; firstorder lia.
        + reflexivity.
        + reflexivity.
        + unfold iConst. hnf; firstorder lia.
      - eapply tyT_weaken_Φ; eauto. firstorder.
      - hnf; intros y Hy; contradict Hy; eapply free_closed_iff; cbn; eauto.
      - reflexivity.
      - hnf; reflexivity.
    }

  - (* Succ (context) *)
    inv Hty.
    specialize IHHstep with (1 := Hty0) (2 := eq_refl) (3 := eq_refl).
    pose proof PCF_tyT_S_inv' pcfTy as (Heq & pcfTy' & ->).
    pose proof mty_strip_eq_Nat_inv _ Heq as (K' & ->); pose proof PCF.ty_UIP_refl Heq as ->; cbn in *.
    assert (skel_S s = skel_S (PCF.skel_red t (PCF.strip pcfTy'))) as [= ->].
    { destruct t; auto. inv Hstep. }
    clear H.
    specialize IHHstep with (2 := eq_refl) (3 := Hclos).
    specialize IHHstep with (pcfTy := pcfTy') (1 := eq_refl).
    { eapply tyT_Succ; eauto. }
  - (* Succ (increment) *)
    pose proof PCF_tyT_S_inv' pcfTy as (Heq & pcfTy' & ->).
    pose proof PCF.tyT_Const_inv' pcfTy' as ->.
    pose proof mty_strip_eq_Nat_inv _ Heq as (K & ->). destruct Heq; cbn in *.
    inv Hty.
    { eapply tyT_Succ.
      - apply tyT_Const. reflexivity. firstorder.
      - apply eqmty_Nat. apply eqmty_Nat_inv in Hty0. unfold funcomp, iConst in *. firstorder. }

  - (* Pred (context) *)
    inv Hty.
    specialize IHHstep with (1 := Hty0) (2 := eq_refl) (3 := eq_refl).
    pose proof PCF_tyT_P_inv' pcfTy as (Heq & pcfTy' & ->).
    pose proof mty_strip_eq_Nat_inv _ Heq as (K' & ->); pose proof PCF.ty_UIP_refl Heq as ->; cbn in *.
    assert (skel_P s = skel_P (PCF.skel_red t (PCF.strip pcfTy'))) as [= ->].
    { destruct t; auto. inv Hstep. }
    clear H.
    specialize IHHstep with (2 := eq_refl) (3 := Hclos).
    specialize IHHstep with (pcfTy := pcfTy') (1 := eq_refl).
    { eapply tyT_Pred; eauto. }
  - (* Pred (predecessor) *)
    pose proof PCF_tyT_P_inv' pcfTy as (Heq & pcfTy' & ->).
    pose proof PCF.tyT_Const_inv' pcfTy' as ->.
    pose proof mty_strip_eq_Nat_inv _ Heq as (K & ->). destruct Heq; cbn in *.
    inv Hty.
    { eapply tyT_Pred.
      - apply tyT_Const. reflexivity. firstorder.
      - apply eqmty_Nat. apply eqmty_Nat_inv in Hty0. unfold funcomp, iConst in *. firstorder. }
  Unshelve. all: exact def_ctx. (* Some bogus contexts *)
Qed.
