Require Import Common.
Require Import PCF_Syntax PCF_Semantics PCF_Types.


(** ** Version of [step] in [Type] *)

(** In the version of PCF in [Type], in order to prove subject reduction, we need a version of [step] defined
in [Type] (i.e. 'informative') instead of [Prop]. We show that both are equivalent (because [step] is functional). *)

(** The proofs here are boring. *)



Inductive stepT : tm -> stepKind -> tm -> Type :=
| stepT_app_left t1 t1' t2 κ :
    stepT t1 κ t1' ->
    stepT (App t1 t2) κ (App t1' t2)
| stepT_app_right v1 t2 t2' κ :
    val v1 ->
    stepT t2 κ t2' ->
    stepT (App v1 t2) κ (App v1 t2')
| stepT_app_lam t v :
    val v ->
    stepT (App (Lam t) v) β (nbeta1 t v)
| stepT_app_fix t v :
    val v ->
    stepT (App (Fix t) v) β (nbeta2 t v (Fix t))
| stepT_ifz1 t1 t1' t2 t3 κ :
    stepT t1 κ t1' ->
    stepT (Ifz t1 t2 t3) κ (Ifz t1' t2 t3)
| stepT_ifz2 t2 t3 :
    stepT (Ifz (Const 0) t2 t3) ϵ t2
| stepT_ifz3 t2 t3 k :
    stepT (Ifz (Const (S k)) t2 t3) ϵ t3
| stepT_s_con t t' κ :
    stepT t κ t' ->
    stepT (Succ t) κ (Succ t')
| stepT_s k :
    stepT (Succ (Const k)) ϵ (Const (S k))
| stepT_p_con t t' κ :
    stepT t κ t' ->
    stepT (Pred t) κ (Pred t')
| stepT_p k :
    stepT (Pred (Const k)) ϵ (Const (pred k))
.


Fixpoint stepT_to_step t κ t' (Hstep: stepT t κ t') { struct Hstep } : step t κ t'.
Proof.
  destruct Hstep.
  all: econstructor; eauto.
  abstract lia.
Defined.


Ltac stuck' :=
  repeat lazymatch goal with
         | [ H : stepT ?t ?κ ?t' |- _ ] => apply stepT_to_step in H
         end;
  stuck.


Lemma stepT_functional t κ1 κ2 t1' t2' :
  stepT t κ1 t1' ->
  stepT t κ2 t2' ->
  t1' = t2' /\ κ1 = κ2.
Proof.
  intros Hstep Hstep2. induction Hstep in κ2,t2',Hstep2 |-*.
  all: inv Hstep2;
    try stuck'; auto;
    now match goal with
        | [ IH : forall κ, _, H : stepT _ _ _ |- _ ] => specialize IH with (1 := H) as [-> ->]
        end.
Qed.


(* TODO: Move *)
Lemma valb_iff' t :
  (~ val t) <-> valb t = false.
Proof.
  destruct (valb t) eqn:E; split; intros H; try congruence.
  - now apply valb_iff in E.
  - intros H'. apply valb_iff in H'. congruence.
Qed.

Definition bool_true_false_dec : forall b, {b = true} + {b = false} :=
  fun b => match b as b' return b = b' -> {b = true} + {b = false} with
         | true => left
         | false => right
         end eq_refl.

Definition val_dec : forall t, { val t } + { ~ val t }.
Proof.
  intros t.
  pose proof bool_true_false_dec (valb t) as [Hvalb|Hvalb].
  - left. now apply valb_iff.
  - right. now apply valb_iff'.
Defined.


Lemma step_app_left_inv t1 t2 t' κ :
  step (App t1 t2) κ t' ->
  ~ val t1 ->
  exists t1', step t1 κ t1' /\ t' = App t1' t2.
Proof.
  intros H1 H2.
  inv H1; eauto; try tauto.
  - contradict H2. apply val_lam.
  - contradict H2. apply val_fix.
Qed.

Lemma step_app_right_inv t1 t2 t' κ :
  step (App t1 t2) κ t' ->
  val t1 ->
  ~ val t2 ->
  exists t2', step t2 κ t2' /\ t' = App t1 t2'.
Proof.
  intros Hstep Hval1 Hval2.
  destruct t'.
  all: try now exfalso; inv Hstep; auto.
  inv Hstep; try stuck; try tauto; eauto.
Qed.

Lemma step_app_lam_beta_inv t v t' κ :
  step (App (Lam t) v) κ t' ->
  val v ->
  t' = nbeta1 t v /\
  κ = β.
Proof.
  intros Hstep Hval. inv Hstep.
  - inv H3.
  - exfalso. stuck.
  - eauto.
Qed.

Lemma step_app_fix_beta_inv t v t' κ :
  step (App (Fix t) v) κ t' ->
  val v ->
  t' = nbeta2 t v (Fix t) /\
  κ = β.
Proof.
  intros Hstep Hval. inv Hstep.
  - inv H3.
  - exfalso. stuck.
  - eauto.
Qed.



Lemma step_app_beta_inv v1 v2 t' κ :
  step (App v1 v2) κ t' ->
  val v1 -> val v2 ->
  { t | v1 = Lam t /\ t' = nbeta1 t v2 /\ κ = β } +
  { t | v1 = Fix t /\ t' = nbeta2 t v2 (Fix t) /\ κ = β }.
Proof.
  intros Hstep Hval1 Hval2.
  assert ({ t | v1 = Lam t } + { t | v1 = Fix t }) as H.
  {
    apply valb_iff in Hval1.
    destruct v1; cbn in *; try discriminate; eauto.
    - (* Const n *) exfalso. inv Hstep.
      + inv H3.
      + stuck.
  }
  destruct H as [ [t ->] | [t ->] ]; [left|right]; exists t.
  - apply step_app_lam_beta_inv in Hstep as [-> ->]; auto.
  - apply step_app_fix_beta_inv in Hstep as [-> ->]; auto.
Qed.

Lemma step_app_beta_inv' v1 v2 :
  (exists κ t', step (App v1 v2) κ t') ->
  val v1 -> val v2 ->
  { t | v1 = Lam t } +
  { t | v1 = Fix t }.
Proof.
  intros Hstep Hval1 Hval2.
  apply valb_iff in Hval1.
  destruct v1; cbn in *; try discriminate; eauto.
  - (* Const n *) exfalso. destruct Hstep as (κ & t' & Hstep).
    inv Hstep.
    + inv H3.
    + stuck.
Qed.


Lemma step_Ifz_val_inv t1 t2 t3 κ t' :
  step (Ifz t1 t2 t3) κ t' ->
  val t1 ->
  (t1 = Zero /\ t' = t2 /\ κ = ϵ) +
  { k | t1 = Const (S k) /\ t' = t3 /\ κ = ϵ }.
Proof.
  intros Hstep Hval.
  pose proof step_ifz_inv Hstep as L.

  assert (match t1 with
          | Const _ => True
          | _ => False
          end).
  { destruct L as [ (t1' & Hstep' & ->) | [ (-> & -> & ->) | (k & -> & -> & ->)] ]; auto; stuck. }
  destruct t1; cbn in *; try tauto.
  destruct n.
  - left. split. reflexivity.
    destruct L as [ (t1' & Hstep' & Heq) | [ (? & ? & Heq) | (k & ? & ? & ?) ] ]; subst; auto; try congruence; stuck.
  - right. eexists. split. reflexivity.
    destruct L as [ (t1' & Hstep' & Heq) | [ (? & ? & Heq) | (k & ? & ? & ?) ] ]; subst; auto; try congruence; stuck.
Qed.

Lemma step_Ifz_val_inv' t1 t2 t3 :
  (exists κ t', step (Ifz t1 t2 t3) κ t') ->
  val t1 ->
  { k | t1 = Const k }.
Proof.
  intros Hstep Hval.
  assert (match t1 with
          | Const _ => True
          | _ => False
          end).
  { destruct Hstep as (κ & t' & Hstep).
    pose proof step_ifz_inv Hstep as L.
    destruct L as [ (t1' & Hstep' & ->) | [ (-> & -> & ->) | (k & -> & -> & ->)] ]; auto; stuck. }
  destruct t1; eauto; tauto.
Qed.

Lemma step_Ifz_Const_0 t2 t3 t' κ :
  step (Ifz Zero t2 t3) κ t' ->
  t' = t2 /\ κ = ϵ.
Proof. intros H. inv H; try stuck; eauto. lia. Qed.

Lemma step_Ifz_Const_S k t2 t3 t' κ :
  step (Ifz (Const (S k)) t2 t3) κ t' ->
  t' = t3 /\ κ = ϵ.
Proof. intros H. inv H; try stuck; eauto. lia. Qed.


Lemma step_Pred_val_inv' t :
  (exists κ t', step (Pred t) κ t') ->
  val t ->
  { k | t = Const k }.
Proof.
  intros Hstep Hval.
  assert (match t with
          | Const _ => True
          | _ => False
          end).
  { destruct Hstep as (κ & t' & Hstep).
    pose proof step_p_inv Hstep as L.
    destruct L as [ (t'' & Hstep' & ->) | (k & -> & -> & ->) ]; auto; stuck. }
  destruct t; eauto; tauto.
Qed.

Lemma step_Pred_val_inv t κ t' :
  step (Pred t) κ t' ->
  val t ->
  { k | t = Const k /\ t' = Const (pred k) /\ κ = ϵ }.
Proof.
  intros Hstep Hval.
  assert (exists κ t', step (Pred t) κ t') as Hstep' by eauto.
  pose proof step_Pred_val_inv' Hstep' Hval as (k & ->).
  exists k. split. reflexivity.
  pose proof step_p_inv Hstep as L.
  destruct L as [ (t'' & Hstep'' & ?) | (k' & ? & ? & ?) ]; subst; auto; try stuck; split; congruence.
Qed.

Lemma step_Succ_val_inv' t :
  (exists κ t', step (Succ t) κ t') ->
  val t ->
  { k | t = Const k }.
Proof.
  intros Hstep Hval.
  assert (match t with
          | Const _ => True
          | _ => False
          end).
  { destruct Hstep as (κ & t' & Hstep).
    pose proof step_s_inv Hstep as L.
    destruct L as [ (t'' & Hstep' & ->) | (k & -> & -> & ->) ]; auto; stuck. }
  destruct t; eauto; tauto.
Qed.

Lemma step_Succ_val_inv t κ t' :
  step (Succ t) κ t' ->
  val t ->
  { k | t = Const k /\ t' = Const (S k) /\ κ = ϵ }.
Proof.
  intros Hstep Hval.
  assert (exists κ t', step (Succ t) κ t') as Hstep' by eauto.
  pose proof step_Succ_val_inv' Hstep' Hval as (k & ->).
  exists k. split. reflexivity.
  pose proof step_s_inv Hstep as L.
  destruct L as [ (t'' & Hstep'' & ?) | (k' & ? & ? & ?) ]; subst; auto; try stuck; split; congruence.
Qed.


Lemma step_sig t :
  (exists κ t', step t κ t') ->
  { κ & { t' | step t κ t' } }.
Proof.
  induction t; intros H.
  - (* Var *) exfalso. firstorder stuck.
  - (* Lam *) exfalso. firstorder stuck.
  - (* Fix *) exfalso. firstorder stuck.
  - (* App *) pose proof val_dec t1 as [Hd1|Hd1].
    + pose proof val_dec t2 as [Hd2|Hd2].
      * exists β. pose proof step_app_beta_inv' H Hd1 Hd2 as [ (t1' & ->) | (t2' & ->) ].
        -- eexists. now apply step_app_lam.
        -- eexists. now apply step_app_fix.
      * spec_assert IHt2 as (κ & t' & IHt2).
        { destruct H as (κ & t' & H). apply step_app_right_inv in H as (t1' & Hstep & ->); firstorder. }
        exists κ. eexists. now constructor; eauto.
    + spec_assert IHt1 as (κ & t' & IHt1).
      { destruct H as (κ & t' & H). apply step_app_left_inv in H as (t1' & Hstep & ->); firstorder. }
      exists κ. eexists. now constructor; eauto.
  - (* Ifz *) pose proof val_dec t1 as [Hd1|Hd1].
    + pose proof step_Ifz_val_inv' H Hd1 as (k & ->).
      destruct k.
      * exists ϵ, t2. destruct H as (κ & t' & Hstep). apply step_Ifz_Const_0 in Hstep as (-> & ->). now apply step_ifz2.
      * exists ϵ, t3. destruct H as (κ & t' & Hstep). apply step_Ifz_Const_S in Hstep as (-> & ->). apply step_ifz3. lia.
    + spec_assert IHt1 as (κ & t' & IHt1).
      { destruct H as (κ & t' & Hstep). apply valb_iff' in Hd1.
        pose proof step_ifz_inv Hstep as L.
        destruct L as [ (t1' & Hstep' & ->) | [ (-> & -> & ->) | (k & -> & -> & ->)] ]; eauto; cbn in *; discriminate.
      }
      eexists _, _; eauto using step.
  - (* Const *) exfalso. firstorder stuck.
  - (* Pred *) pose proof val_dec t as [Hd|Hd].
    + pose proof step_Pred_val_inv' H Hd as (k & ->).
      exists ϵ, (Const (pred k)).
      destruct H as (κ & t' & Hstep).
      eauto using step.
    + spec_assert IHt as (κ & t' & IH).
      { destruct H as (κ & t' & Hstep). apply valb_iff' in Hd.
        pose proof step_p_inv Hstep as L.
        destruct L as [ (t'' & Hstep'' & ?) | (k' & ? & ? & ?) ]; subst; cbn in *; eauto; try stuck; discriminate.
      }
      eauto using step.
  - (* Succ *) pose proof val_dec t as [Hd|Hd].
    + pose proof step_Succ_val_inv' H Hd as (k & ->).
      exists ϵ, (Const (S k)).
      destruct H as (κ & t' & Hstep).
      eauto using step.
    + spec_assert IHt as (κ & t' & IH).
      { destruct H as (κ & t' & Hstep). apply valb_iff' in Hd.
        pose proof step_s_inv Hstep as L.
        destruct L as [ (t'' & Hstep'' & ?) | (k' & ? & ? & ?) ]; subst; cbn in *; eauto; try stuck; discriminate.
      }
      eauto using step.
Qed.


Lemma step_sig' t κ :
  (exists t', step t κ t') ->
  { t' | step t κ t' }.
Proof.
  intros H.
  assert (exists κ t', step t κ t') as (κ' & t' & Hstep) % step_sig by eauto.
  assert (κ' = κ) as -> by (destruct H; eapply step_deterministic; hnf; eauto).
  eauto.
Qed.



Lemma step_app_right_inv_sig t1 t2 t' κ :
  step (App t1 t2) κ t' ->
  val t1 ->
  ~ val t2 ->
  { t2' | step t2 κ t2' /\ t' = App t1 t2' }.
Proof.
  intros H1 H2 H3.
  pose proof step_app_right_inv H1 H2 H3 as H.
  assert (exists t2', step t2 κ t2') as (t2' & Hstep) % step_sig' by firstorder.
  assert (t' = App t1 t2') as ->.
  { destruct H; eapply step_deterministic; hnf; eauto using step. }
  eauto.
Qed.

Lemma step_app_not_const_val k κ v t' :
  step ((Const k) v) κ t' -> val v -> False.
Proof. intros H1 H2. inv H1; stuck. Qed.

Lemma step_app_left_inv_sig t1 t2 t' κ :
  step (App t1 t2) κ t' ->
  ~ val t1 ->
  { t1' | step t1 κ t1' /\ t' = App t1' t2 }.
Proof.
  intros H1 H2.
  pose proof step_app_left_inv H1 H2 as H.
  assert (exists t1', step t1 κ t1') as (t1' & Hstep) % step_sig' by firstorder.
  assert (t' = App t1' t2) as ->.
  { destruct H; eapply step_deterministic; hnf; eauto using step. }
  eauto.
Qed.

Lemma step_ifz_left_inv t1 t2 t3 κ t' :
  step (Ifz t1 t2 t3) κ t' ->
  ~ val t1 ->
  exists t1', step t1 κ t1' /\ t' = Ifz t1' t2 t3.
Proof.
  intros Hstep Hval.
  pose proof step_ifz_inv Hstep as L.
  destruct L as [ (t1' & Hstep' & ->) | [ (-> & -> & ->) | (k & -> & -> & ->)] ]; eauto; cbn in *.
  { contradict Hval. isVal. }
  { contradict Hval. isVal. }
Qed.

Lemma step_ifz_left_inv_sig t1 t2 t3 κ t' :
  step (Ifz t1 t2 t3) κ t' ->
  ~ val t1 ->
  { t1' | step t1 κ t1' /\ t' = Ifz t1' t2 t3 }.
Proof.
  intros H1 H2.
  pose proof step_ifz_left_inv H1 H2 as H.
  assert (exists t1', step t1 κ t1') as (t1' & Hstep) % step_sig' by firstorder.
  assert (t' = Ifz t1' t2 t3) as ->.
  { destruct H; eapply step_deterministic; hnf; eauto using step. }
  eauto.
Qed.

Lemma step_Pred_left_inv t κ t' :
  step (Pred t) κ t' ->
  ~ val t ->
  exists t'', step t κ t'' /\ t' = Pred t''.
Proof.
  intros Hstep Hval.
  pose proof step_p_inv Hstep as L.
  destruct L as [ (t'' & Hstep' & ->) | (k & -> & -> & ->) ]; auto; eauto.
  { contradict Hval. isVal. }
Qed.

Lemma step_Pred_left_inv_sig t κ t' :
  step (Pred t) κ t' ->
  ~ val t ->
  { t'' | step t κ t'' /\ t' = Pred t'' }.
Proof.
  intros H1 H2.
  pose proof step_Pred_left_inv H1 H2 as H.
  assert (exists t'', step t κ t'') as (t'' & Hstep) % step_sig' by firstorder.
  assert (t' = Pred t'') as ->.
  { destruct H; eapply step_deterministic; hnf; eauto using step. }
  eauto.
Qed.

Lemma step_Succ_left_inv t κ t' :
  step (Succ t) κ t' ->
  ~ val t ->
  exists t'', step t κ t'' /\ t' = Succ t''.
Proof.
  intros Hstep Hval.
  pose proof step_s_inv Hstep as L.
  destruct L as [ (t'' & Hstep' & ->) | (k & -> & -> & ->) ]; auto; eauto.
  { contradict Hval. isVal. }
Qed.

Lemma step_Succ_left_inv_sig t κ t' :
  step (Succ t) κ t' ->
  ~ val t ->
  { t'' | step t κ t'' /\ t' = Succ t'' }.
Proof.
  intros H1 H2.
  pose proof step_Succ_left_inv H1 H2 as H.
  assert (exists t'', step t κ t'') as (t'' & Hstep) % step_sig' by firstorder.
  assert (t' = Succ t'') as ->.
  { destruct H; eapply step_deterministic; hnf; eauto using step. }
  eauto.
Qed.



Lemma step_to_stepT t κ t' :
  step t κ t' -> stepT t κ t'.
Proof.
  induction t in κ,t'|-*; intros Hstep.
  - (* var *) exfalso. stuck.
  - (* lam *) exfalso. stuck.
  - (* fix *) exfalso. stuck.
  - (* app *) destruct (val_dec t1) as [Hd1|Hd1].
    + destruct (val_dec t2) as [Hd2 | Hd2].
      * pose proof step_app_beta_inv Hstep Hd1 Hd2 as [(t & -> & -> & ->) | (t & -> & -> & ->)].
        -- (* Lam *) now constructor.
        -- (* Fix *) now constructor.
      * pose proof step_app_right_inv_sig Hstep Hd1 Hd2 as (t2' & Hstep' & ->).
        apply stepT_app_right; auto; isVal.
    + pose proof step_app_left_inv_sig Hstep Hd1 as (t1' & Hstep' & ->).
      apply stepT_app_left; eauto.
  - (* Ifz *) destruct (val_dec t1) as [Hd1| Hd1].
    + pose proof step_Ifz_val_inv Hstep Hd1 as [(-> & -> & ->) | (k & -> & -> & ->)].
      * eapply stepT_ifz2.
      * eapply stepT_ifz3.
    + pose proof step_ifz_left_inv_sig Hstep Hd1 as (t1' & Hstep' & ->).
      apply stepT_ifz1; auto.
  - (* Const *) exfalso. stuck.
  - (* Pred *) destruct (val_dec t) as [Hd| Hd].
    + pose proof step_Pred_val_inv Hstep Hd as (k & -> & -> & ->). now constructor.
    + pose proof step_Pred_left_inv_sig Hstep Hd as (t'' & Hstep' & ->).
      apply stepT_p_con; auto.
  - (* Succ *) destruct (val_dec t) as [Hd| Hd].
    + pose proof step_Succ_val_inv Hstep Hd as (k & -> & -> & ->). now constructor.
    + pose proof step_Succ_left_inv_sig Hstep Hd as (t'' & Hstep' & ->).
      apply stepT_s_con; auto.
Qed.



(** *** Step function *)

Fixpoint stepFun (t : tm) { struct t} : option (stepKind * tm) :=
  match t with
  | Var _ | Lam _ | Fix _ | Const _ => None (* stuck *)
  | Succ t =>
    match t with
    | Const k => Some (ϵ, Const (S k))
    | _ => bind_option (fun '(κ, x) => Some (κ, Succ x)) (stepFun t)
    end
  | Pred t =>
    match t with
    | Const k => Some (ϵ, Const (pred k))
    | _ => bind_option (fun '(κ, x) => Some (κ, Pred x)) (stepFun t)
    end
  | Ifz t1 t2 t3 =>
    match t1 with
    | Const 0 => Some (ϵ, t2)
    | Const (S k) => Some (ϵ, t3)
    | _ => bind_option (fun '(κ, x) => Some (κ, Ifz x t2 t3)) (stepFun t1)
    end
  | App t1 t2 =>
    if val_dec t1 then
      if val_dec t2 then
        match t1 with
        | Lam t' => Some (β, nbeta1 t' t2)
        | Fix t' => Some (β, nbeta2 t' t2 (Fix t'))
        | _ => None
        end
      else bind_option (fun '(κ, x) => Some (κ, App t1 x)) (stepFun t2)
    else bind_option (fun '(κ, x) => Some (κ, App x t2)) (stepFun t1)
  end.

Lemma stepFun_correct (t : tm) (κ : stepKind) (t' : tm) :
  stepFun t = Some (κ, t') ->
  step t κ t'.
Proof.
  induction t in κ,t'|-*; cbn.
  all: try discriminate.
  - (* App *) destruct val_dec as [Hd1|Hd1].
    + destruct val_dec as [Hd2|Hd2].
      * now destruct t1; try discriminate; intros [= <- <-]; constructor.
      * destruct (stepFun t2) as [(? & ?)| ] eqn:E; cbn; try discriminate; intros [= <- <-].
        specialize IHt2 with (1 := eq_refl). econstructor; eauto.
    + destruct (stepFun t1) as [(? & ?)| ] eqn:E; cbn; try discriminate; intros [= <- <-].
      specialize IHt1 with (1 := eq_refl). econstructor; eauto.
  - (* Ifz *)
    destruct (stepFun t1) as [(κ' & x)| ] eqn:E; cbn.
    + specialize IHt1 with (1 := eq_refl).
      destruct t1; try discriminate; intros [= <- <-]; eauto using step.
    + destruct t1; try discriminate. destruct n; intros [= <- <-].
      * apply step_ifz2. reflexivity.
      * apply step_ifz3. lia.
  - (* Pred *)
    destruct (stepFun t) as [(κ' & x)| ] eqn:E; cbn.
    + specialize IHt with (1 := eq_refl).
      destruct t; cbn; intros [= <- <-]; try stuck; eauto using step.
    + destruct t; try discriminate; intros [= <- <-]; eauto using step.
  - (* Succ *)
    destruct (stepFun t) as [(κ' & x)| ] eqn:E; cbn.
    + specialize IHt with (1 := eq_refl).
      destruct t; cbn; intros [= <- <-]; try stuck; eauto using step.
    + destruct t; try discriminate; intros [= <- <-]; eauto using step.
Qed.

Lemma stepFun_complete (t : tm) (κ : stepKind) (t' : tm) :
  step t κ t' ->
  stepFun t = Some (κ, t').
Proof.
  induction 1; cbn.
  - (* app_left *) destruct val_dec; try stuck. now rewrite IHstep.
  - (* app_right *) destruct val_dec; try tauto. destruct val_dec; try stuck. now rewrite IHstep.
  - (* app lam *) destruct val_dec; tauto.
  - (* app fix *) destruct val_dec; tauto.
  - (* ifz1 *) rewrite IHstep; cbn. destruct t1; cbn; auto; stuck.
  - (* ifz2 *) subst. reflexivity.
  - (* ifz3 *) destruct k; auto. lia.
  - (* pred *) rewrite IHstep; cbn. destruct t; auto; stuck.
  - (* pred *) reflexivity.
  - (* succ *) rewrite IHstep; cbn. destruct t; auto; stuck.
  - (* succ *) reflexivity.
Qed.


Corollary stuck_dec : forall t, {exists κ t', step t κ t'} + { stuck t }.
Proof.
  intros t.
  destruct (stepFun t) as [(κ&t') | ] eqn:E.
  - apply stepFun_correct in E. left; eauto.
  - right. hnf; intros κ t' Hstep. apply stepFun_complete in Hstep. congruence.
Defined.

Corollary stuck_dec' : forall t, { κ & { t' & stepT t κ t'} } + { stuck t }.
Proof.
  intros t. pose proof stuck_dec t as [ Hd | Hd].
  - apply step_sig in Hd as (κ & t' & Hstep). apply step_to_stepT in Hstep. left; firstorder.
  - now right.
Defined.


Corollary not_stuck_steps (t : tm) :
  ~ stuck t -> exists κ t', step t κ t'.
Proof. intros H1. pose proof stuck_dec t as [ H2 | H2]; firstorder. Qed.
Corollary not_stuck_steps_sig (t : tm) :
  ~ stuck t -> { κ & { t' | step t κ t' }}.
Proof. intros H. now pose proof not_stuck_steps H as H' % step_sig. Qed.
Corollary not_stuck_stepsT (t : tm) :
  ~ stuck t -> { κ & { t' & stepT t κ t' }}.
Proof. intros H. pose proof not_stuck_steps_sig H as (κ & t' & Hstep % step_to_stepT); eauto. Qed.



(** *** Closures *)


From Coq Require Import ConstructiveEpsilon.



Section StarT.

  Variables (X: Type) (R: X -> X -> Type).
  Implicit Types x y z : X.


  Inductive starT : X -> X -> Type :=
  | starReflT x     : starT x x
  | starStepT x y z : R x y -> starT y z -> starT x z.

  Definition starT_trans x y z : starT x y -> starT y z -> starT x z.
  Proof.
    intros H1 H2. induction H1 as [x|x x' y H _ IH].
    + exact H2.
    + econstructor 2; [exact H|]. auto.
  Qed.


  Inductive starIT : nat -> X -> X -> Type :=
  | starIReflT x        : starIT 0 x x
  | starIStepT x x' y n : R x x' -> starIT n x' y -> starIT (S n) x y.

  Fact starIT_inv0 x y :
    starIT 0 x y -> x = y.
  Proof. now inversion 1; subst. Qed.

  Fact starIT_invS x z n :
    starIT (S n) x z -> ({ y & R x y * starIT n y z }) % type.
  Proof. now inversion 1; subst; eauto. Qed.


  Fact starIT_trans x y z n1 n2 :
    starIT n1 x y -> starIT n2 y z -> starIT (n1+n2) x z.
  Proof.
    intros H1 H2.
    induction H1 as [x|x x' y n H _ IH]; cbn.
    + exact H2.
    + econstructor 2; eauto.
  Qed.

  Lemma starIT_starT x y n :
    starIT n x y -> starT x y.
  Proof. induction 1; firstorder eauto using starT. Qed.

  Lemma starT_starIT x y :
    starT x y -> { n & starIT n x y }.
  Proof. induction 1; firstorder eauto using starIT. Qed.

End StarT.




Definition anyStepT t t' : Type := { κ & stepT t κ t' }.

Lemma anyStepT_to_anyStep t t' :
  anyStepT t t' -> anyStep t t'.
Proof. intros (κ & Hstep). exists κ. now apply stepT_to_step. Qed.

Lemma anyStep_to_anyStepT t t' :
  anyStep t t' -> anyStepT t t'.
Proof.
  intros H. hnf in H|-*.
  assert (exists κ t', step t κ t') as (κ & t'' & Hstep) % step_sig by firstorder.
  assert (t'' = t') as ->.
  { destruct H. eapply step_deterministic; eauto. }
  apply step_to_stepT in Hstep; eauto.
Qed.

Lemma anyStep_to_anyStepT' t :
  (exists t', anyStep t t') -> { t' & anyStepT t t' }.
Proof.
  unfold anyStep.
  intros H.
  assert (exists (κ : stepKind) (t' : tm), step t κ t') as (κ & t' & Hstep % step_to_stepT) % step_sig by firstorder.
  firstorder.
Qed.

Lemma starI_to_starIT k t t' :
  starI _ anyStep k t t' -> starIT anyStepT k t t'.
Proof.
  induction k in t,t'|-*; intros Hstar.
  - apply starI_inv0 in Hstar as ->. constructor.
  - apply starI_invS in Hstar.
    assert (exists t'', t ≻ t'') as (t'' & κ & Hstep) % anyStep_to_anyStepT' by firstorder.
    assert (starI _ anyStep k t'' t').
    { destruct Hstar as (t''' & (κ' & Hstep') & H). now pose proof step_deterministic Hstep' (stepT_to_step Hstep) as (<-&<-). }
    clear Hstar. econstructor; hnf; eauto.
Qed.



Lemma star_starI_sig (X : Type) (R : X -> X -> Prop) :
  (forall k t t', { starI _ R k t t' } + { ~ starI _ R k t t'}) ->
  forall t t',
    star _ R t t' ->
    { k | starI _ R k t t' }.
Proof. intros Hdec t t'. intros H % star_starI. apply (constructive_indefinite_ground_description_nat_Acc); auto. Qed.

Lemma stepT_starI_dec : forall (k : nat) (t t' : tm), {starI tm anyStep k t t'} + {~ starI tm anyStep k t t'}.
Proof.
  induction k; intros.
  - destruct (tm_eq_dec t t') as [->|Hd].
    + left. constructor.
    + right. intros -> % starI_inv0. tauto.
  - pose proof stuck_dec' t as [(κ & t'' & Hstep) | Hstuck].
    + specialize (IHk t'' t') as [IH|IH].
      * left. econstructor; eauto. apply anyStepT_to_anyStep. hnf; eauto.
      * right. intros (t''' & Hstep' & Hstar') % starI_invS.
        assert (t''' = t'') as ->.
        { apply stepT_to_step in Hstep. eapply anyStep_deterministic; hnf; eauto.  }
        firstorder.
    + right. intros (t''' & Hstep' & Hstar') % starI_invS. firstorder.
Qed.


Lemma star_step_to_stepT t t' :
  star _ anyStep t t' -> starT (anyStepT) t t'.
Proof.
  intros H.
  eapply star_starI_sig in H as (k & H).
  - eapply starIT_starT; eauto. eapply starI_to_starIT; eauto.
  - clear_all. apply stepT_starI_dec.
Qed.

Lemma starT_step_to_step t t' :
  starT anyStepT t t' -> star _ (anyStep) t t'.
Proof. induction 1; econstructor; eauto using anyStepT_to_anyStep. Qed.

Lemma starIT_step_to_stepI k t t' :
  starIT anyStepT k t t' -> starI _ (anyStep) k t t'.
Proof. induction 1; econstructor; eauto using anyStepT_to_anyStep. Qed.



(** Aux relation counting the number of total steps [k] and the number of β steps [i] *)
Inductive starB' : nat -> nat -> tm -> tm -> Prop :=
| starB'_refl t : starB' 0 0 t t
| starB'_beta k i (t1 t2 t3 : tm) :
    t1 ≻(β) t2 ->
    starB' k i t2 t3 ->
    starB' (S k) (S i) t1 t3
| starB'_nat k i (t1 t2 t3 : tm) :
    t1 ≻(ϵ) t2 ->
    starB' k i t2 t3 ->
    starB' (S k) i t1 t3.

Lemma starB_starB' t i t' :
  starB i t t' ->
  exists k, starB' k i t t'.
Proof.
  induction 1.
  - exists 0. constructor.
  - destruct IHstarB as (k & IH). exists (S k). econstructor; eauto.
  - destruct IHstarB as (k & IH). exists (S k). econstructor; eauto.
Qed.

Lemma starB'_starI k i t t' :
  starB' k i t t' ->
  starI tm anyStep k t t'.
Proof. induction 1; econstructor; hnf; eauto. Qed.



Lemma starB'_dec : forall (t t' : tm) (i k : nat), {starB' k i t t'} + {~ starB' k i t t'}.
Proof.
  intros. induction k in t,t',i|-*.
  - destruct (tm_eq_dec t t') as [->|Hd1].
    + destruct (Nat.eq_dec i 0) as [->|Hd2].
      * left. constructor.
      * right. intros H. inv H; auto.
    + right. intros H. inv H; auto.
  - pose proof stuck_dec' t as [(κ & t'' & Hstep) | Hstuck].
    + destruct κ.
      * (* β *) destruct i.
        { right. intros H. inv H; auto.
          pose proof step_deterministic H1 (stepT_to_step Hstep); firstorder congruence. }
        {
          specialize (IHk t'' t') as [IH|IH].
          - left. econstructor 2; eauto. now apply stepT_to_step.
          - right. intros H. inv H; auto.
            + assert (t2 = t'') as ->.
              { apply stepT_to_step in Hstep. eapply anyStep_deterministic; hnf; eauto.  }
              eauto.
            + pose proof step_deterministic H1 (stepT_to_step Hstep); firstorder congruence.
        }
      * (* ϵ *)
        specialize (IHk t'' t') as [IH|IH].
        -- left. econstructor 3; eauto. now apply stepT_to_step.
        -- right. intros H. inv H; auto.
           ++ pose proof step_deterministic H1 (stepT_to_step Hstep); firstorder congruence.
           ++ assert (t2 = t'') as ->.
              { apply stepT_to_step in Hstep. eapply anyStep_deterministic; hnf; eauto.  }
              eauto.
    + right. intros H. inv H; firstorder.
Qed.

Lemma starB_starB'_sig t i t' :
  starB i t t' ->
  { k | starB' k i t t' }.
Proof.
  intros H % starB_starB'.
  apply (constructive_indefinite_ground_description_nat_Acc); eauto.
  clear H. apply starB'_dec.
Qed.




Inductive starBT' : nat -> nat -> tm -> tm -> Type :=
| starBT'_refl t : starBT' 0 0 t t
| starBT'_beta k i (t1 t2 t3 : tm) :
    stepT t1 β t2 ->
    starBT' k i t2 t3 ->
    starBT' (S k) (S i) t1 t3
| starBT'_nat k i (t1 t2 t3 : tm) :
    stepT t1 ϵ t2 ->
    starBT' k i t2 t3 ->
    starBT' (S k) i t1 t3.


Lemma starB'_to_starBT' (k i : nat) (t t' : tm) :
  starI _ (anyStep) k t t' -> starB' k i t t' -> starBT' k i t t'.
Proof.
  intros Hstar1 Hstar2.
  apply starI_to_starIT in Hstar1.
  induction Hstar1 in i,Hstar2|-*.
  - assert (i = 0) as -> by now inv Hstar2.
    constructor.
  - rename r into Hstep.
    destruct Hstep as (κ & Hstep).
    destruct κ.
    + (* β *) destruct i.
      * exfalso. inv Hstar2. pose proof step_deterministic H0 (stepT_to_step Hstep); firstorder congruence.
      * assert (starB' n i x' y).
        { inv Hstar2.
          - pose proof step_deterministic H1 (stepT_to_step Hstep) as (-> & _). auto.
          - pose proof step_deterministic H0 (stepT_to_step Hstep); firstorder congruence.
        }
        econstructor; eauto.
    + (* κ *)
      assert (starB' n i x' y).
      { inv Hstar2.
        - pose proof step_deterministic H0 (stepT_to_step Hstep); firstorder congruence.
        - pose proof step_deterministic H0 (stepT_to_step Hstep) as (-> & _). auto.
      }
      econstructor; eauto.
Qed.


Inductive starBT : nat -> tm -> tm -> Type :=
| starBT_refl t : starBT 0 t t
| starBT_beta i (t1 t2 t3 : tm) :
    stepT t1 β t2 ->
    starBT i t2 t3 ->
    starBT (S i) t1 t3
| starBT_nat i (t1 t2 t3 : tm) :
    stepT t1 ϵ t2 ->
    starBT i t2 t3 ->
    starBT i t1 t3.

Lemma starB_to_starBT' (i : nat) (t t' : tm) :
  starB i t t' -> { k & starBT' k i t t'}.
Proof.
  intros Hstar.
  pose proof starB_star Hstar as H.
  apply star_step_to_stepT in H.
  pose proof starB_starB'_sig Hstar as (k & Hstar2).
  unshelve epose proof starB'_to_starBT' _ Hstar2 as L2.
  { eapply starB'_starI; eauto. }
  eauto.
Qed.

Lemma starB_to_starBT (i : nat) (t t' : tm) :
  starB i t t' -> starBT i t t'.
Proof.
  intros (k & Hstar) % starB_to_starBT'.
  induction Hstar; econstructor; eauto.
Qed.
