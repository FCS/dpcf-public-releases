Require Import Common.
Require Import PCF_Syntax.


(** ** Naive substitution for PCF *)


(** We only use weak reduction (i.e. no "reduction under λ"), so naive substitution is actually enough.
This is, under the assumption that the starting terms are closed (and hence all terms in the execution
are closed.)

Naive substitution does not avoid capturing. Therefore, no renaming is needed.
*)


(* Naive substitution *)
Fixpoint nsubst (t : tm) (x : nat) (s : tm) : tm :=
  match t with
  | Var y => if Nat.eq_dec x y then s else Var y
  | Lam t => Lam (nsubst t (S x) s)
  | Fix t => Fix (nsubst t (S (S x)) s)
  | App t1 t2 => App (nsubst t1 x s) (nsubst t2 x s)
  | Ifz t1 t2 t3 => Ifz (nsubst t1 x s) (nsubst t2 x s) (nsubst t3 x s)
  | Const k => Const k
  | Pred t => Pred (nsubst t x s)
  | Succ t => Succ (nsubst t x s)
  end.


Lemma val_nsubst t (x : nat) (s : tm) :
  val t ->
  val (nsubst t x s).
Proof. induction 1; cbn; constructor; eauto. Qed.


Definition nbeta1 (t : tm) (s : tm) : tm := nsubst t 0 s.
Definition nbeta2 (t : tm) (s0 s1 : tm) : tm := nsubst (nsubst t 1 s1) 0 s0.

Lemma nsubst_bound' (x : nat) (t u : tm) :
  bound (S x) t -> closed u -> bound x (nsubst t x u).
Proof.
  induction t in x|-*.
  1: { intros.
       cbn [nsubst].
       cbn in H.
       destruct Nat.eq_dec as [->|d].
       - eapply bound_monotone; eauto. lia.
       - cbn. lia. }
  all: firstorder.
Qed.


Lemma maxVar_nsubst (x : nat) (t u : tm) :
  maxVar (nsubst t x u) <= maxVar t + maxVar u.
Proof.
  induction t in x|-*; cbn in *.
  - destruct Nat.eq_dec; cbn; lia.
  - specialize (IHt (S x)). lia.
  - specialize (IHt (S (S x))). lia.
  - specialize (IHt1 x); specialize (IHt2 x). lia.
  - specialize (IHt1 x); specialize (IHt2 x); specialize (IHt3 x). lia.
  - lia.
  - specialize (IHt x). lia.
  - specialize (IHt x). lia.
Qed.

Lemma maxVar_nsubst_closed (x : nat) (t u : tm) :
  closed u ->
  maxVar (nsubst t x u) <= maxVar t.
Proof. intros H. rewrite maxVar_nsubst. rewrite (maxVar_closed' _ H). lia. Qed.

Lemma nsubst_bound (x : nat) (t u : tm) m :
  bound m t ->
  closed u ->
  bound m (nsubst t x u).
Proof.
  intros. eapply maxVar_bound_le. rewrite maxVar_nsubst_closed at 1; eauto.
  now apply maxVar_bound_glb.
Qed.







(** ** PCF Small-step semantics *)

Variant stepKind :=
| stepBeta (** A λ or fix substitution *)
| stepNat (** Anything else (pred, ifz, etc.) *)
. 

Notation "'β'" := stepBeta : PCF.
Notation "'ϵ'" := stepNat : PCF.

Implicit Type κ : stepKind.



Reserved Notation "t '≻(' κ ')' t'" (at level 70, format "t  '≻(' κ ')'  t'").
Inductive step : tm -> stepKind -> tm -> Prop :=
| step_app_left t1 t1' t2 κ :
    t1 ≻(κ) t1' ->
    t1 t2 ≻(κ) t1' t2
| step_app_right v1 t2 t2' κ :
    val v1 ->
    t2 ≻(κ) t2' ->
    v1 t2 ≻(κ) v1 t2'
| step_app_lam t v :
    val v ->
    (Lam t) v ≻(β) (nbeta1 t v)
| step_app_fix t v :
    val v ->
    (Fix t) v ≻(β) (nbeta2 t v (Fix t))
| step_ifz1 t1 t1' t2 t3 κ :
    t1 ≻(κ) t1' ->
    Ifz t1 t2 t3 ≻(κ) Ifz t1' t2 t3
| step_ifz2 t2 t3 k :
    k = 0 ->
    Ifz (Const k) t2 t3 ≻(ϵ) t2
| step_ifz3 t2 t3 k :
    1 <= k ->
    Ifz (Const k) t2 t3 ≻(ϵ) t3
| step_s_con t t' κ :
    t ≻(κ) t' ->
    Succ t ≻(κ) Succ t'
| step_s k :
    Succ (Const k) ≻(ϵ) Const (S k)
| step_p_con t t' κ :
    t ≻(κ) t' ->
    Pred t ≻(κ) Pred t'
| step_p k :
    Pred (Const k) ≻(ϵ) Const (pred k)
where "c '≻(' κ ')' c'" := (step c κ c')
.

Definition anyStep t t' := exists κ, t ≻(κ) t'.

Infix "≻" := (@anyStep) (at level 70).
Infix "≻*" := (@star _ (@anyStep)) (at level 70).



(** *** Inversion *)

Lemma step_app_inv t1 t2 t κ :
  App t1 t2 ≻(κ) t ->
  (exists t1', t1 ≻(κ) t1' /\
          t = App t1' t2) \/
  (exists t2', t2 ≻(κ) t2' /\
          val t1 /\
          t = App t1 t2') \/
  (exists t', t1 = Lam t' /\
         val t2 /\
         κ = β /\
         t = nbeta1 t' t2) \/
  (exists t', t1 = Fix t' /\
         val t2 /\
         κ = β /\
         t = nbeta2 t' t2 (Fix t')).
Proof. intros H. inv H; eauto 8. Qed.

Lemma step_ifz_inv t1 t2 t3 t κ :
  Ifz t1 t2 t3 ≻(κ) t ->
  (exists t1', t1 ≻(κ) t1' /\
          t = Ifz t1' t2 t3) \/
  (t1 = Zero /\ κ = ϵ /\ t = t2) \/
  (exists k, t1 = Const (S k) /\ κ = ϵ /\ t = t3).
Proof. intros H. inv H; eauto 8. destruct k; cbn in *; eauto 8. omega. Qed.

Lemma step_p_inv t1 t κ :
  Pred t1 ≻(κ) t ->
  (exists t1', t1 ≻(κ) t1' /\ t = Pred t1') \/
  (exists k, t1 = Const k /\ κ = ϵ /\ t = Const (pred k)).
Proof. intros H. inv H; eauto 5. Qed.

Lemma step_s_inv t1 t κ :
  Succ t1 ≻(κ) t ->
  (exists t1', t1 ≻(κ) t1' /\ t = Succ t1') \/
  (exists k, t1 = Const k /\ κ = ϵ /\ t = Const (S k)).
Proof. intros H. inv H; eauto 5. Qed.


Ltac step_inv :=
  let Heq1 := fresh "Heq" in
  let Heq2 := fresh "Heq" in
  let Heq3 := fresh "Heq" in
  lazymatch goal with
  | [ H : App ?t1 ?t2 ≻(?κ) ?t |- _ ] =>
    let t1' := fresh t1 "'" in
    let t2' := fresh t2 "'" in
    let t' := fresh "t'" in
    let Hval := fresh "Hval" t in
    apply step_app_inv in H as
        [ (t1'&H&Heq1) | [ (t2'&H&Hval&Heq1) | [ (t'&Heq1&Hval&Heq2&Heq3) | (t'&Heq1&Hval&Heq2&Heq3) ] ] ];
    subst
  | [ H : Ifz ?t1 ?t2 ?t3 ≻(?κ) ?t |- _ ] =>
    let t1' := fresh t1 "'" in
    let k := fresh "k" in
    apply step_ifz_inv in H as
        [ (t1'&H&Heq11) | [ (Heq1&Heq2&Heq3) | (k&Heq1&Heq2&Heq3) ] ];
    subst
  | [ H : Pred ?t1 ≻(?κ) ?t |- _ ] =>
    let t1' := fresh t1 "'" in
    let k := fresh "k" in
    apply step_p_inv in H as
        [ (t1'&H&Heq1) | (k&Heq1&Heq2&Heq3) ];
    subst
  | [ H : Succ ?t1 ≻(?κ) ?t |- _ ] =>
    let t1' := fresh t1 "'" in
    let k := fresh "k" in
    apply step_s_inv in H as
        [ (t1'&H&Heq1) | (k&Heq1&Heq2&Heq3) ];
    subst
  end.



(** ** Stuckness *)

Definition stuck (t : tm) :=
  forall κ t', ~ t ≻(κ) t'.

Lemma Const_stuck (k : nat) :
  stuck (Const k).
Proof. unfold stuck. intros κ t' Hstep. remember (Const k) as t. induction Hstep; intros; congruence. Qed.

Lemma val_stuck (t : tm) :
  val t -> stuck t.
Proof.
  unfold stuck. intros H κ t' Hstep. induction H; intros.
  - inv Hstep.
  - inv Hstep.
  - eapply Const_stuck; eauto.
Qed.

Lemma var_stuck (x : nat) :
  stuck (Var x).
Proof. hnf. intros κ t H. inv H. Qed.

Ltac stuck :=
  match goal with
  | [ H: (Var ?x) ≻(?κ) ?t |- _] =>
    contradict (var_stuck H)
  | [ H: ?s ≻(?κ) ?t |- _] =>
    let L := fresh "L" in
    enough (val s) as L by (contradict (val_stuck L H));
    isVal
  | [ H: ?s ≻ ?t |- _] =>
    let L := fresh "L" in
    let κ := fresh "κ" in
    destruct H as (κ&H);
    enough (val s) as L by (contradict (val_stuck L H));
    isVal
  end.


(** Example execution *)

Ltac starStep := eapply starStep; [ eexists | ].

Example steps_example :
  example_term' ≻* Const 0.
Proof.
  (** This could be automated... *)
  cbv. starStep. apply step_app_fix. isVal.
  starStep. apply step_ifz3; cbn. reflexivity.
  starStep. cbn. apply step_app_right. isVal. apply step_p. cbn.
  starStep. apply step_app_fix. isVal.
  starStep; cbn. apply step_ifz2. reflexivity. reflexivity.
Qed.


(** *** Determinism *)

(** The semantics is deterministic *)

Lemma step_deterministic t1 t2 t3 κ1 κ2 :
  t1 ≻(κ1) t2 ->
  t1 ≻(κ2) t3 ->
  t2 = t3 /\ κ1 = κ2.
Proof.
  intros H1 H2. induction H1 in κ2,t3,H2|-*.
  - inv H2; eauto; try stuck.
    now specialize IHstep with (1 := H5) as [-> ->].
  - inv H2; eauto; try stuck.
    now specialize IHstep with (1 := H7) as [-> ->].
  - inv H2; eauto; try stuck.
  - inv H2; eauto; try stuck.
  - inv H2; eauto; try stuck.
    now specialize IHstep with (1 := H6) as [-> ->].
  - inv H2; eauto; try stuck.
    omega.
  - inv H2; eauto; try stuck.
    omega.
  - inv H2; eauto; try stuck.
    now specialize IHstep with (1 := H0) as [-> ->].
  - inv H2; eauto; try stuck.
  - inv H2; eauto; try stuck.
    now specialize IHstep with (1 := H0) as [-> ->].
  - inv H2; eauto; try stuck.
Qed.

Lemma anyStep_deterministic t1 t2 t3 :
  t1 ≻ t2 ->
  t1 ≻ t3 ->
  t2 = t3.
Proof. intros (κ1&H1) (κ2&H2). eapply step_deterministic; eauto. Qed.



(** *** PCF Big-step semantics *)

(** In the big-step semantics, we count the number of β-substitutions. *)


Reserved Notation "t '⇓(' i ')' v" (at level 70, format "t  '⇓(' i ')'  v").

Inductive bigstep : tm -> nat -> tm -> Prop :=
| bigstep_val v i :
    val v ->
    i = 0 ->
    v ⇓(i) v
| bigstep_app_lam t1 t2 t v1 v2 i1 i2 i3 i :
    t1 ⇓(i1) Lam t ->
    t2 ⇓(i2) v1 ->
    val v1 ->
    nbeta1 t v1 ⇓(i3) v2 ->
    i = 1+i1+i2+i3 ->
    t1 t2 ⇓(i) v2
| bigstep_app_fix t1 t2 t v1 v2 i1 i2 i3 i :
    t1 ⇓(i1) Fix t ->
    t2 ⇓(i2) v1 ->
    val v1 ->
    nbeta2 t v1 (Fix t) ⇓(i3) v2 ->
    i = 1+i1+i2+i3 ->
    t1 t2 ⇓(i) v2
| bigstep_ifzO t1 t2 t3 v i1 i2 i k :
    t1 ⇓(i1) Const k ->
    k = 0 ->
    t2 ⇓(i2) v ->
    i = i1+i2 ->
    Ifz t1 t2 t3 ⇓(i) v
| bigstep_ifzS t1 t2 t3 v2 i1 i3 i k :
    t1 ⇓(i1) Const k ->
    1 <= k ->
    t3 ⇓(i3) v2 ->
    i1+i3 = i ->
    Ifz t1 t2 t3 ⇓(i) v2
| bigstep_s t i k k' :
    t ⇓(i) (Const k) ->
    k' = S k ->
    Succ t ⇓(i) (Const k')
| bigstep_p t i k k' :
    t ⇓(i) Const k ->
    k' = pred k ->
    Pred t ⇓(i) (Const k')
where "t '⇓(' i ')' v" := (bigstep t i v).


       

(** Example evaluation *)

Example bigstep_example0 :
  example_term (Const 0) ⇓(1) Const 0.
Proof.
  eapply bigstep_app_fix.
  - eapply bigstep_val. isVal. reflexivity.
  - eapply bigstep_val. isVal. reflexivity.
  - isVal.
  - cbn. eapply bigstep_ifzO.
    + apply bigstep_val. isVal. reflexivity.
    + reflexivity.
    + apply bigstep_val. isVal. reflexivity.
    + cbn. reflexivity.
  - cbn. reflexivity.
Qed.

Example bigstep_example1 :
  example_term (Const 1) ⇓(2) @Const 0.
Proof.
  eapply bigstep_app_fix.
  - eapply bigstep_val. isVal. reflexivity.
  - eapply bigstep_val. isVal. reflexivity.
  - isVal.
  - fold (example_term). cbn. eapply bigstep_ifzS.
    + apply bigstep_val. isVal. reflexivity.
    + reflexivity.
    + eapply bigstep_app_fix.
      * apply bigstep_val. isVal. reflexivity.
      * eapply bigstep_p. apply bigstep_val. isVal. reflexivity. reflexivity.
      * isVal.
      * cbn. eapply bigstep_ifzO.
        -- apply bigstep_val. isVal. reflexivity.
        -- reflexivity.
        -- apply bigstep_val. isVal. reflexivity.
        -- cbn. reflexivity.
      * cbn. reflexivity.
    + cbn. reflexivity.
  - cbn. reflexivity.
Qed.



(** **** Inversion rules for the big-step semantics *)


Lemma bigstep_Const_inv k (t' : tm) i :
  Const k ⇓(i) t' ->
  t' = Const k /\ i = 0.
Proof.
  intros Hstep. remember (Const k) as t. induction Hstep in Heqt|-*; subst; try congruence; auto.
Qed.

Lemma bigstep_val_inv (v t : tm) i :
  val v ->
  v ⇓(i) t ->
  t = v /\ i = 0.
Proof.
  intros Hval Hstep. induction Hval in i,Hstep|-*.
  - inv Hstep. eauto.
  - inv Hstep. eauto.
  - eapply bigstep_Const_inv; eauto.
Qed.


Lemma bigstep_app_lam_inv t1 t2 v i :
  (Lam t1) t2 ⇓(i) v ->
  exists v' i1 i2, i = 1 + i1 + i2 /\
             t2 ⇓(i1) v' /\
             nbeta1 t1 v' ⇓(i2) v.
Proof.
  intros H. inv H.
  - inv H0.
  - apply bigstep_val_inv in H2 as [ [= ->] ->]; [| isVal].
    eexists _,_,_; repeat split; eauto.
  - apply bigstep_val_inv in H2 as [ [= ->] ->]. isVal.
Qed.

Lemma bigstep_app_fix_inv t1 t2 v i :
  (Fix t1) t2 ⇓(i) v ->
  exists v' i1 i2, i = 1 + i1 + i2 /\
             t2 ⇓(i1) v' /\
             nbeta2 t1 v' (Fix t1) ⇓(i2) v.
Proof.
  intros H. inv H.
  - inv H0.
  - apply bigstep_val_inv in H2 as [ [= ->] ->]. isVal.
  - apply bigstep_val_inv in H2 as [ [= ->] ->]; [| isVal].
    eexists _,_,_; repeat split; eauto.
Qed.


Lemma bigstep_app_inv t1 t2 v i :
  App t1 t2 ⇓(i) v ->
  (exists t1' v' i1 i2 i3,
      i = 1 + i1 + i2 + i3 /\
      t1 ⇓(i1) Lam t1' /\
      t2 ⇓(i2) v' /\
      nbeta1 t1' v' ⇓(i3) v) \/
  (exists t1' v' i1 i2 i3,
      i = 1 + i1 + i2 + i3 /\
      t1 ⇓(i1) Fix t1' /\
      t2 ⇓(i2) v' /\
      nbeta2 t1' v' (Fix t1') ⇓(i3) v).
Proof.
  intros H. inv H.
  - inv H0.
  - left. eexists _,_,_,_,_; repeat split; eauto.
  - right. eexists _,_,_,_,_; repeat split; eauto.
Qed.



Lemma bigstep_ifz_inv t1 t2 t3 v i :
  Ifz t1 t2 t3 ⇓(i) v ->
  (exists i1 i2, i = i1 + i2 /\
            t1 ⇓(i1) Zero /\
            t2 ⇓(i2) v) \/
  (exists k i1 i3, i = i1 + i3 /\
              1 <= k /\
              t1 ⇓(i1) (Const k) /\
              t3 ⇓(i3) v).
Proof.
  intros H. inv H.
  - inv H0.
  - left. eexists _,_. repeat split; eauto.
  - right. eexists _,_,_. split; [ | split; [ | split]]; eauto.
Qed.

Lemma bigstep_s_inv t v i :
  Succ t ⇓(i) v ->
  exists k, t ⇓(i) (Const k) /\ v = Const (S k).
Proof.
  intros H. inv H.
  - inv H0.
  - eexists _. split; eauto.
Qed.

Lemma bigstep_p_inv t i v :
  Pred t ⇓(i) v ->
  exists k, t ⇓(i) (Const k) /\ v = Const (pred k).
Proof.
  intros H. inv H.
  - inv H0.
  - eexists. split; eauto.
Qed.



(** The right side of ⇓ is a value *)
Lemma bigstep_result_is_val (t v : tm) i :
  t ⇓(i) v ->
  val v.
Proof.
  induction 1; subst;
    solve [ auto
          | isVal; auto
          ].
Qed.



(** *** Star *)

(** Star step relation with cost (number of beta reductions) *)

Inductive starB : nat -> tm -> tm -> Prop :=
| starB_refl t : starB 0 t t
| starB_beta i (t1 t2 t3 : tm) :
    t1 ≻(β) t2 ->
    starB i t2 t3 ->
    starB (S i) t1 t3
| starB_nat i (t1 t2 t3 : tm) :
    t1 ≻(ϵ) t2 ->
    starB i t2 t3 ->
    starB i t1 t3.

Notation "t1 '≻^(' i ')' t2" := (starB i t1 t2) (at level 70, format "t1  '≻^(' i ')'  t2").

(** Increase the cost [i] if [κ = β], else return [i] *)
Definition costAfter (i : nat) (κ : stepKind) : nat :=
  match κ with
  | β => S i
  | ϵ => i
  end.

Lemma starB_any (i : nat) (κ : stepKind) (t1 t2 t3 : tm) :
  t1 ≻(κ) t2 ->
  starB i t2 t3 ->
  starB (costAfter i κ) t1 t3.
Proof. destruct κ; cbn; eauto using starB. Qed.

Lemma starB_trans (i1 i2 : nat) (t1 t2 t3 : tm) :
  t1 ≻^(i1) t2 ->
  t2 ≻^(i2) t3 ->
  t1 ≻^(i1 + i2) t3.
Proof.
  induction 1; intros Hstar2.
  - cbn. assumption.
  - econstructor 2; eauto.
  - econstructor 3; eauto.
Qed.

Lemma starB_trans' (i1 i2 i' : nat) (t1 t2 t3 : tm) :
  t1 ≻^(i1) t2 ->
  t2 ≻^(i2) t3 ->
  i' = (i1 + i2) ->
  t1 ≻^(i') t3.
Proof. intros H1 H2 ->. eapply starB_trans; eauto. Qed.


Lemma starB_star (i : nat) (t1 t2 : tm) :
  t1 ≻^(i) t2 -> t1 ≻* t2.
Proof. induction 1; econstructor; hnf; eauto. Qed.

Lemma star_starB (t1 t2 : tm) :
  t1 ≻* t2 ->
  exists (i : nat), t1 ≻^(i) t2.
Proof.
  induction 1.
  - exists 0. constructor.
  - destruct IHstar as (i&IH). destruct H as [[ | ] H].
    + exists (S i). econstructor; eauto.
    + exists i. econstructor; eauto.
Qed.


(** *** Big-step and small-step agreement *)


(** **** Small steps (with costs) to big step *)

Lemma big_step_extend (t1 t2 t3 : tm) i κ :
  t1 ≻(κ) t2 ->
  t2 ⇓(i) t3 ->
  t1 ⇓(costAfter i κ) t3.
Proof.
  intros Hstep Hbigstep. induction Hstep in i,t3,Hbigstep|-*; subst.
  - inv Hbigstep.  
    + contradict (val_not_app H).
    + specialize IHHstep with (1 := H1) as IH.
      eapply bigstep_app_lam; eauto. cbn. destruct κ; reflexivity.
    + specialize IHHstep with (1 := H1) as IH.
      eapply bigstep_app_fix; eauto. cbn. destruct κ; reflexivity.
  - (* beta *) apply bigstep_app_inv in Hbigstep as
        [(t1' & v' & i1 & i2 & i3 & -> & Hbig_v & Hbig_t2' & Hbig_subs)
        |(t1' & v' & i1 & i2 & i3 & -> & Hbig_v & Hbig_t2' & Hbig_subs)
        ].
    + (* β-lambda *) apply bigstep_val_inv in Hbig_v as (<-&->); auto; cbn.
      specialize IHHstep with (1 := Hbig_t2').
      eapply bigstep_app_lam; eauto.
      * eapply bigstep_val; eauto.
      * eapply bigstep_result_is_val; eauto.
      * cbn. destruct κ; reflexivity.
    + (* β-fix *) apply bigstep_val_inv in Hbig_v as (<-&->); auto; cbn.
      specialize IHHstep with (1 := Hbig_t2').
      eapply bigstep_app_fix; eauto.
      * eapply bigstep_val; eauto.
      * eapply bigstep_result_is_val; eauto.
      * cbn. destruct κ; reflexivity.
  - eapply bigstep_app_lam.
    + apply bigstep_val. isVal. reflexivity.
    + apply bigstep_val. assumption. reflexivity.
    + assumption.
    + eassumption.
    + cbn. reflexivity.
  - eapply bigstep_app_fix.
    + apply bigstep_val. isVal. reflexivity.
    + apply bigstep_val. assumption. reflexivity.
    + assumption.
    + eassumption.
    + cbn. reflexivity.
  - apply bigstep_ifz_inv in Hbigstep as [(i1&i2&->&H1&H2) | (k&i1&i2&->&Hge&H1&H2)].
    + specialize IHHstep with (1 := H1) as IH.
      eapply bigstep_ifzO; eauto. destruct κ; reflexivity.
    + specialize IHHstep with (1 := H1) as IH.
     eapply bigstep_ifzS; eauto. destruct κ; reflexivity.
  - eapply bigstep_ifzO.
    + apply bigstep_val. isVal. reflexivity.
    + eauto.
    + eassumption.
    + reflexivity.
  - eapply bigstep_ifzS.
    + apply bigstep_val. isVal. reflexivity.
    + eauto.
    + eauto.
    + reflexivity.
  - apply bigstep_s_inv in Hbigstep as (k&H&->).
    specialize IHHstep with (1 := H) as IH.
    eapply bigstep_s; eauto.
  - apply bigstep_val_inv in Hbigstep as [-> ->]. 2: isVal.
    eapply bigstep_s; eauto. apply bigstep_val. isVal. reflexivity.
  - apply bigstep_p_inv in Hbigstep as (k&H&->).
    specialize IHHstep with (1 := H) as IH.
    eapply bigstep_p; eauto.
  - apply bigstep_val_inv in Hbigstep as [-> ->]. 2: isVal.
    eapply bigstep_p; eauto. apply bigstep_val. isVal. reflexivity.
Qed.


Lemma small_steps_to_big_step (i : nat) (t v : tm) :
  t ≻^(i) v -> val v -> t ⇓(i) v.
Proof.
  induction 1; intros.
  - now constructor.
  - change (S i) with (costAfter i β). eapply big_step_extend; eauto.
  - change (i) with (costAfter i ϵ). eapply big_step_extend; eauto.
Qed.


Lemma small_steps_to_big_step' (t v : tm) :
  t ≻* v -> val v -> exists i, t ⇓(i) v.
Proof. intros (i&H) % star_starB Hval. exists i. eapply small_steps_to_big_step; eauto. Qed.


Lemma starB_app1 (i : nat) (t1 t2 v : tm) :
  t1 ≻^(i) v ->
  t1 t2 ≻^(i) v t2.
Proof.
  induction 1 in t2.
  - constructor.
  - econstructor 2.
    + eapply step_app_left; eauto.
    + eauto.
  - econstructor 3.
    + eapply step_app_left; eauto.
    + eauto.
Qed.

Lemma starB_app2 (i : nat) (v1 t2 v2 : tm) :
  t2 ≻^(i) v2 ->
  val v1 ->
  v1 t2 ≻^(i) v1 v2.
Proof.
  induction 1 in v1; intros.
  - constructor.
  - econstructor 2.
    + eapply step_app_right; eauto.
    + eauto.
  - econstructor 3.
    + eapply step_app_right; eauto.
    + eauto.
Qed.

Lemma starB_ifz (i : nat) (t1 t2 t3 v : tm) :
  t1 ≻^(i) v ->
  Ifz t1 t2 t3 ≻^(i) Ifz v t2 t3.
Proof.
  induction 1 in t2,t3; intros.
  - constructor.
  - econstructor 2.
    + eapply step_ifz1; eauto.
    + eauto.
  - econstructor 3.
    + eapply step_ifz1; eauto.
    + eauto.
Qed.

Lemma starB_succ (i : nat) (t v : tm) :
  t ≻^(i) v ->
  Succ t ≻^(i) Succ v.
Proof.
  induction 1.
  - constructor.
  - econstructor 2.
    + eapply step_s_con; eauto.
    + eauto.
  - econstructor 3.
    + eapply step_s_con; eauto.
    + eauto.
Qed.

Lemma starB_pred (i : nat) (t v : tm) :
  t ≻^(i) v ->
  Pred t ≻^(i) Pred v.
Proof.
  induction 1.
  - constructor.
  - econstructor 2.
    + eapply step_p_con; eauto.
    + eauto.
  - econstructor 3.
    + eapply step_p_con; eauto.
    + eauto.
Qed.


Lemma big_step_to_small_steps (t v : tm) i :
  t ⇓(i) v -> t ≻^(i) v.
Proof.
  intros Hbig. induction Hbig; subst.
  - (* val *) apply starB_refl.
  - (* β-lam *) eapply starB_trans'.
    + eapply starB_app1. eassumption.
    + eapply starB_trans'.
      * eapply starB_app2. eassumption. isVal.
      * eapply starB_beta.
        -- now eapply step_app_lam.
        -- eauto.
      * reflexivity.
    + lia.
  - (* β-fix *) eapply starB_trans'.
    + eapply starB_app1. eassumption.
    + eapply starB_trans'.
      * eapply starB_app2. eassumption. isVal.
      * eapply starB_beta.
        -- now eapply step_app_fix.
        -- eauto.
      * reflexivity.
    + lia.
  - (* ifz zero *) eapply starB_trans.
    + eapply starB_ifz. eassumption.
    + eapply starB_nat.
      * now apply step_ifz2.
      * eauto.
  - (* ifz succ *) eapply starB_trans.
    + eapply starB_ifz. eassumption.
    + eapply starB_nat.
      * now apply step_ifz3.
      * eauto.
  - (* succ *)
    eapply starB_trans'.
    + eapply starB_succ; eauto.
    + eapply starB_nat.
      * apply step_s.
      * apply starB_refl.
    + lia.
  - (* pred *)
    eapply starB_trans'.
    + eapply starB_pred; eauto.
    + eapply starB_nat.
      * apply step_p.
      * apply starB_refl.
    + lia.
Qed.


Lemma big_step_to_small_steps' (t v : tm) i :
  t ⇓(i) v -> t ≻* v.
Proof. now intros H % big_step_to_small_steps % starB_star. Qed.



(** *** Misc *)


(** Determinism *)
Lemma bigstep_deterministic t1 t2 t3 i1 i2 :
  t1 ⇓(i1) t2 ->
  t1 ⇓(i2) t3 ->
  t2 = t3 /\ i1 = i2.
Proof.
  intros H1 H2. induction H1 in i2,t3,H2|-*; subst.
  - apply bigstep_val_inv in H2 as [-> ->]; auto.
  - apply bigstep_app_inv in H2 as [(t1'&v'&j1&j2&j3&->&HInv1&HInv2&HInv3) | (t1'&v'&j1&j2&j3&->&HInv1&HInv2&HInv3)].
    + specialize IHbigstep1 with (1 := HInv1) as [ [= ->] ->].
      specialize IHbigstep2 with (1 := HInv2) as [-> ->].
      specialize IHbigstep3 with (1 := HInv3) as [-> ->].
      auto.
    + specialize IHbigstep1 with (1 := HInv1) as [ [= ->] ->].
  - apply bigstep_app_inv in H2 as [(t1'&v'&j1&j2&j3&->&HInv1&HInv2&HInv3) | (t1'&v'&j1&j2&j3&->&HInv1&HInv2&HInv3)].
    + specialize IHbigstep1 with (1 := HInv1) as [ [= ->] ->].
    + specialize IHbigstep1 with (1 := HInv1) as [ [= ->] ->].
      specialize IHbigstep2 with (1 := HInv2) as [-> ->].
      specialize IHbigstep3 with (1 := HInv3) as [-> ->].
      auto.
  - apply bigstep_ifz_inv in H2 as [ (j1&j2&->&HInv1&HInv2) | (k&j1&j3&->&Hge&HInv1&HInv3)].
    + specialize IHbigstep1 with (1 := HInv1) as [ _ ->].
      specialize IHbigstep2 with (1 := HInv2) as [-> ->].
      auto.
    + specialize IHbigstep1 with (1 := HInv1) as [ [= <-] ->]. omega.
  - apply bigstep_ifz_inv in H2 as [ (j1&j2&->&HInv1&HInv2) | (k'&j1&j3&->&Hge&HInv1&HInv3)].
    + specialize IHbigstep1 with (1 := HInv1) as [ [= ->] ->]. omega.
    + specialize IHbigstep1 with (1 := HInv1) as [ [= <-] ->].
      specialize IHbigstep2 with (1 := HInv3) as [-> ->].
      auto.
  - apply bigstep_s_inv in H2 as (k'&HInv&->).
    specialize IHbigstep with (1 := HInv) as [[=->] ->].
    auto.
  - apply bigstep_p_inv in H2 as (k'&HInv&->).
    specialize IHbigstep with (1 := HInv) as [[=->] ->].
    auto.
Qed.



(** *** Progression and termination *)

(** The size decreases after nat computations *)
Lemma step_nat_decreases_size t t' :
  t ≻(ϵ) t' ->
  size t' < size t.
Proof.
  remember ϵ as κ. induction 1 in Heqκ|-*; intros; subst; cbn in *; try congruence; auto.
  all: try match goal with
           | [ IH : ϵ = ϵ -> _ |- _ ] => specialize IH with (1 := eq_refl)
           end.
  all: omega.
Qed.


(** Progressiveness *)
Variant progressive  : tm -> Prop :=
| progressive_red t t' κ : t ≻(κ) t' -> progressive t
| progressive_val t      : val t  -> progressive t
.



Lemma preservation_closed (t t' : tm) :
  closed t ->
  t ≻ t' ->
  closed t'.
Proof.
  intros Hclosed (κ&Hstep). induction Hstep; closed_inv; cbn in *; try tauto.
  - apply nsubst_bound'; eauto.
  - apply nsubst_bound'; eauto. apply nsubst_bound'; eauto.
Qed.

Corollary star_closed t t' :
  t ≻* t' ->
  closed t ->
  closed t'.
Proof. induction 1; eauto using preservation_closed. Qed.



(** A term is normalising if there is no infinite step sequence *)
Definition normalising (t : tm) : Prop := Acc (fun t2 t1 => anyStep t1 t2) t.

Definition terminates (t : tm) : Prop := exists t', t ≻* t' /\ val t'.

Lemma val_terminates (t : tm) :
  val t -> terminates t.
Proof. intros H. eexists. split; eauto. reflexivity. Qed.

(** This holds because the language is deterministic. The other direction only holds with progressiveness. *)
Lemma terminates_normalising (t : tm) :
  terminates t -> normalising t.
Proof.
  intros (t'&Hstar&Hval).
  induction Hstar.
  - constructor. intros t' Hstep. stuck.
  - constructor. intros t' Hstep.
    pose proof anyStep_deterministic H Hstep as ->.
    now apply IHHstar.
Qed.

Lemma terminates_iff (t : tm) :
  terminates t <-> exists i t', t ⇓(i) t'.
Proof.
  split.
  - intros (t'&Hstar&Hval). apply small_steps_to_big_step' in Hstar as [i Hsteps]; eauto.
  - intros (i&t'&Hbig).
    apply big_step_to_small_steps' in Hbig as Hbig'.
    eexists; split; eauto. eapply bigstep_result_is_val; eauto.
Qed.


Example example_div_not_terminates :
  ~ terminates example_div.
Proof.
  intros (i&t'&H) % terminates_iff.
  revert H. revert i. refine (complete_induction _ _). intros i IH. intros Hterm.
  apply bigstep_app_fix_inv in Hterm as (v'&i1&i2&->&H1&H2).
  apply bigstep_val_inv in H1 as [-> ->]. 2: isVal.
  cbn in *. eapply IH; eauto.
Qed.
