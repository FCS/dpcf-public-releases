From Coq Require Import Omega.
From Coq Require Import List.
Import ListNotations.

From Coq Require Import Vector Fin.
Require Import FinNotations.
Import FinNumNotations VectorNotations2.
Open Scope vector_scope.

Set Implicit Arguments.


Section Vector_Basic.

  Variable (X:Type).

  Lemma vector_inv_0 (v : Vector.t X 0) :
    v = [||].
  Proof.
    refine (match v with
            | [||] => _
            end).
    reflexivity.
  Defined.
  Lemma vector_inv_0' (v : Vector.t X 0) :
    v = [||].
  Proof. apply vector_inv_0. Qed.

  Lemma vector_inv_S (n : nat) (v : Vector.t X (S n)) :
    {x & {v' | v = x ::: v'}}.
  Proof.
    refine (match v with
            | x ::: v' =>  _
            end).
    eauto.
  Qed.
  Lemma vector_inv_S' (n : nat) (v : Vector.t X (S n)) :
    {x & {v' | v = x ::: v'}}.
  Proof. apply vector_inv_S. Qed.

  Lemma fin_inv_0 (i : Fin.t 0) : False.
  Proof. refine (match i with end). Defined.
  Lemma fin_inv_0' (i : Fin.t 0) : False.
  Proof. now apply fin_inv_0. Qed.

  Lemma fin_inv_S (n : nat) (i : Fin.t (S n)) :
    (i = Fin.F1) + { i' | i = Fin.FS i' }.
  Proof.
    refine (match i with
            | Fin.F1 => _
            | Fin.FS _ => _
            end); eauto.
  Defined.
  Lemma fin_inv_S' (n : nat) (i : Fin.t (S n)) :
    (i = Fin.F1) + { i' | i = Fin.FS i' }.
  Proof. now apply fin_inv_S. Qed.


  Lemma vector_hd_eq (n : nat) (x : X) (v : Vector.t X n) :
    hd (x ::: v) = x.
  Proof. reflexivity. Qed.

  Lemma vector_tl_eq (n : nat) (x : X) (v : Vector.t X n) :
    tl (x ::: v) = v.
  Proof. reflexivity. Qed.

  Lemma vector_nth_hd (n : nat) (v : Vector.t X (S n)) :
    v[@Fin0] = hd v.
  Proof.
    (* Long form: *)
    (* refine (match v as v0 in (Vector.t _ n0) *)
    (*               return (match n0 as n1 return (Vector.t X n1 -> Type) with *)
    (*                       | 0 => fun _ => True *)
    (*                       | S n1 => fun v' : Vector.t X (S n1) => v'[@Fin0] = hd v' *)
    (*                       end v0) with *)
    (*         | [||] => I *)
    (*         | x ::: v' => eq_refl *)
    (*         end). *)
    refine (match v with
            | x ::: v' => _
            end); reflexivity.
  Qed.

  Lemma vector_nth_tl (n : nat) (v : Vector.t X (S (S n))) (x : Fin.t _) :
    v[@Fin.FS x] = (tl v)[@x].
  Proof.
    pose proof vector_inv_S v as (x1&v'&->).
    pose proof vector_inv_S v' as (x2&v''&->).
    reflexivity.
  Qed.

  (** Equations for rewriting *)
  Lemma vect_hd_eq (n : nat) (x : X) (xs : Vector.t X n) :
    hd (x ::: xs) = x.
  Proof. reflexivity. Qed.
  Lemma vect_tl_eq (n : nat) (x : X) (xs : Vector.t X n) :
    tl (x ::: xs) = xs.
  Proof. reflexivity. Qed.

End Vector_Basic.


(** This module declares a few usueful abbreviations *)
Module VectFunctionNotations.

  Notation vect_cast := Vector.cast.
  Notation fin_cast := Fin.cast.
  Notation vect_to_list := to_list.
  Notation vect_app := Vector.append.

End VectFunctionNotations.

Import VectFunctionNotations.


Section Fin_To_Nat.

  (** The function [Fin.to_nat] is unsuitable for our purposes. The reason is that [proj1_sig (Fin.to_nat (Fin.FS i))] doesn't
evaluate to [S (proj1_sig (Fin.to_nat i))]. (It could be defined so, but the problem is that the function destructs the sigma
type instead of projecting.

We define our own function [fin_to_nat] here, without sigma types. *)

  Fixpoint fin_to_nat (n : nat) (i : Fin.t n) : nat :=
    match i with
    | Fin.F1 => 0
    | Fin.FS i => S (fin_to_nat i)
    end.

  Lemma fin_to_nat_lt (n : nat) (i : Fin.t n) :
    fin_to_nat i < n.
  Proof. induction i; cbn; omega. Qed.


  Lemma fin_to_nat_stdlib (n : nat) (i : Fin.t n) :
    fin_to_nat i = proj1_sig (Fin.to_nat i).
  Proof.
    induction i.
    - reflexivity.
    - cbn. destruct to_nat; cbn; f_equal; auto.
  Qed.


  Lemma fin_to_nat_inj (m : nat) (i1 i2 : Fin.t m) :
    fin_to_nat i1 = fin_to_nat i2 ->
    i1 = i2.
  Proof.
    induction i1 in i2|-*; intros H.
    - cbn in H. pose proof fin_inv_S i2 as [-> | (i2'&->)]; cbn in *; congruence.
    - pose proof fin_inv_S i2 as [-> | (i2'&->)]; cbn in *.
      + discriminate.
      + f_equal. apply IHi1. congruence.
  Qed.

End Fin_To_Nat.


Section FinCast.

  Lemma fin_cast_to_nat (m n : nat) (i : Fin.t m) (H : m = n) :
    fin_to_nat (fin_cast i H) = fin_to_nat i.
  Proof.
    induction i in n,H|-*.
    - subst. cbn. reflexivity.
    - cbn. destruct n.
      + exfalso. omega.
      + cbn. f_equal. auto.
  Qed.

  Lemma fin_cast_ext (m n : nat) (i : Fin.t m) (H1 H2 : m = n) :
    fin_cast i H1 = fin_cast i H2.
  Proof.
    induction i in n,H1,H2|-*.
    - destruct n.
      + exfalso. omega.
      + cbn. reflexivity.
    - destruct n.
      + exfalso. omega.
      + cbn. f_equal. auto.
  Qed.

  Lemma fin_cast_id (m : nat) (i : Fin.t m) (H : m = m) :
    fin_cast i H = i.
  Proof. induction i; cbn; f_equal; auto. Qed.

  Lemma fin_cast_twice (m1 m2 m3 : nat) (i : Fin.t m1) (H1 : m1 = m2) (H2 : m2 = m3) (H3 : m1 = m3) :
    fin_cast (fin_cast i H1) H2 = fin_cast i H3.
  Proof.
    induction i in m2,m3,H1,H2,H3|-*.
    - destruct m2,m3; cbn; auto. all: omega.
    - cbn. destruct m2, m3; cbn; auto; try omega.
      f_equal. erewrite IHi; eauto.
  Qed.

  Lemma fin_cast_inv (m1 m2 : nat) (xs : Fin.t m1) (H1 : m1 = m2) (H2 : m2 = m1) :
    fin_cast (fin_cast xs H1) H2 = xs.
  Proof. now unshelve erewrite fin_cast_twice, fin_cast_id. Qed.

End FinCast.

Section VectorCast.

  Variable X : Type.

  Lemma vect_cast_id (m : nat) (xs : Vector.t X m) (H : m = m) :
    vect_cast xs H = xs.
  Proof. induction xs; cbn; f_equal; auto. Qed.

  Lemma vect_cast_ext (m n : nat) (xs : Vector.t X m) (H1 H2 : m = n) :
    vect_cast xs H1 = vect_cast xs H2.
  Proof.
    induction xs in n,H1,H2|-*.
    - subst. cbn. reflexivity.
    - destruct n.
      + exfalso. omega.
      + cbn. f_equal. auto.
  Qed.

  Lemma vect_cast_twice (m1 m2 m3 : nat) (xs : Vector.t X m1) (H1 : m1 = m2) (H2 : m2 = m3) (H3 : m1 = m3) :
    vect_cast (vect_cast xs H1) H2 = vect_cast xs H3.
  Proof.
    induction xs in m2,m3,H1,H2,H3|-*.
    - destruct m2,m3; cbn; auto. all: omega.
    - cbn. destruct m2, m3; cbn; auto; try omega.
      f_equal. erewrite IHxs; eauto.
  Qed.

  Lemma vect_cast_inv (m1 m2 : nat) (xs : Vector.t X m1) (H1 : m1 = m2) (H2 : m2 = m1) :
    vect_cast (vect_cast xs H1) H2 = xs.
  Proof. now unshelve erewrite vect_cast_twice, vect_cast_id. Qed.


  Lemma vect_cast_hd (m1 m2 : nat) (xs : Vector.t X (S m1)) (H : S m1 = S m2) :
    hd (vect_cast xs H) = hd xs.
  Proof. pose proof vector_inv_S' xs as (x&xs'&->). cbn. reflexivity. Qed.

  Lemma vect_cast_tl (m1 m2 : nat) (xs : Vector.t X (S m1)) (H : S m1 = S m2) (H' : m1 = m2) :
    tl (vect_cast xs H) = vect_cast (tl xs) H'.
  Proof. pose proof vector_inv_S' xs as (x&xs'&->). cbn. apply vect_cast_ext. Qed.

  Lemma vect_cast_nth (m1 m2 : nat) (xs : Vector.t X m1) (i : Fin.t m2) (H : m1 = m2) (H' : m2 = m1) :
    (vect_cast xs H)[@i] = xs[@fin_cast i H'].
  Proof.
    induction xs in i,m2,H,H'|-*.
    - subst. exfalso. now apply fin_inv_0.
    - subst. pose proof fin_inv_S' i as [-> | (i'&->)].
      + reflexivity.
      + cbn. apply IHxs.
  Qed.

  Lemma vect_cast_eq_nil (m : nat) (x : X) (xs : Vector.t X m) (H1 H2 : 0 = m) :
    vect_cast (@nil X) H1 = vect_cast (@nil X) H2.
  Proof. subst. apply vect_cast_ext. Qed.

  Lemma vect_cast_eq_cons (m n : nat) (x : X) (xs : Vector.t X m)
        (H1 : S m = S n) (H2 : m = n) :
    vect_cast (x ::: xs) H1 = x ::: (vect_cast xs H2).
  Proof. cbn. f_equal. apply vect_cast_ext. Qed.

End VectorCast.


Section SkipTake.

  Variable (X : Type).

  Fixpoint vect_skip (m n : nat) : Vector.t X (m+n) -> Vector.t X n :=
    match m with
    | 0 => fun (xs : Vector.t X n) => xs
    | S m => fun (xs : Vector.t X (S (m+n))) => vect_skip _ _ (tl xs)
    end.

  Fixpoint vect_take (m n : nat) : Vector.t X (m+n) -> Vector.t X m :=
    match m with
    | 0 => fun (xs : Vector.t X n) => nil _
    | S m => fun (xs : Vector.t X (S (m+n))) => hd xs ::: vect_take _ _ (tl xs)
    end.

  Lemma vect_app_take_skip (m n : nat) (v : Vector.t X (m + n)) :
    Vector.append (vect_take m n v) (vect_skip m n v) = v.
  Proof.
    induction m in n,v|-*; cbn in *.
    - reflexivity.
    - rewrite IHm. now rewrite <- eta.
  Qed.

  Lemma vect_take_hd (m n : nat) (xs : Vector.t X (S m+n)) :
    hd (vect_take _ _ xs) = hd xs.
  Proof. reflexivity. Qed.

  Lemma vec_take_cast (m m' n n' : nat) (xs : Vector.t X (m'+n'))
        (Heq1 : m'+n' = m+n) (Heq2: m' = m) :
    vect_take m n (vect_cast xs Heq1) = vect_cast (vect_take m' n' xs) Heq2.
  Proof.
    subst. rewrite !vect_cast_id.
    assert (n' = n) as -> by omega.
    now rewrite vect_cast_id.
  Qed.

End SkipTake.



(** Sometimes, working with vectors is tedious, next to impossible, due to impossibility of rewriting.
In those cases, it is convinient, do reason over lists instead *)

Section VectToList.
  Variable X : Type.


  (** The standard library provides the function [vect_to_list]. However, it has weird [cbn] behaviour,
      so we might want to use rewriting instead. *)
  Lemma vect_to_list_eq_nil (xs : Vector.t X 0) : to_list xs = List.nil.
  Proof. pose proof vector_inv_0 xs as ->. reflexivity. Qed.

  Lemma vect_to_list_eq_cons (n : nat) (x : X) (xs : Vector.t X n) : to_list (x ::: xs) = x :: to_list xs.
  Proof. reflexivity. Qed.

  Lemma vect_to_list_eq_cons' (n : nat) (xs : Vector.t X (S n)) : to_list xs = hd xs :: to_list (tl xs).
  Proof. pose proof vector_inv_S xs as (x&xs'&->). reflexivity. Qed.

  Lemma vect_to_list_cast (n m : nat) (xs : Vector.t X n) (H : n = m) :
    vect_to_list (vect_cast xs H) = vect_to_list xs.
  Proof. subst. now rewrite vect_cast_id. Qed.

  Lemma vect_to_list_tl (m : nat) (xs : Vector.t X (S m)) :
    vect_to_list (tl xs) = List.tl (vect_to_list xs).
  Proof. pose proof vector_inv_S xs as (x&xs'&->). reflexivity. Qed.

  Lemma vect_to_list_hd (m : nat) (xs : Vector.t X (S m)) (def : X) :
    hd xs = List.hd def (vect_to_list xs).
  Proof. pose proof vector_inv_S xs as (x&xs'&->). reflexivity. Qed.

  Lemma vect_to_list_app (m n : nat) (xs : Vector.t X m) (ys : Vector.t X n) :
    vect_to_list (vect_app xs ys) = vect_to_list xs ++ vect_to_list ys.
  Proof.
    induction xs.
    - reflexivity.
    - cbn [vect_app]. rewrite !vect_to_list_eq_cons. cbn [app].
      setoid_rewrite (vect_to_list_eq_cons h (vect_app xs ys)).
      f_equal. auto.
  Qed.

  Lemma vect_to_list_take (m n : nat) (xs : Vector.t X (m + n)) :
    vect_to_list (vect_take _ _ xs) = List.firstn m (vect_to_list xs).
  Proof.
    induction m.
    - reflexivity.
    - cbn [vect_app]. cbn [vect_take].
      setoid_rewrite vect_to_list_eq_cons.
      rewrite (eta xs) at 3. setoid_rewrite vect_to_list_eq_cons.
      cbn [firstn]. f_equal. auto.
  Qed.

  Lemma vect_to_list_skip (m n : nat) (xs : Vector.t X (m + n)) :
    vect_to_list (vect_skip _ _ xs) = List.skipn m (vect_to_list xs).
  Proof.
    induction m in n,xs|-*.
    - reflexivity.
    - cbn [vect_app]. cbn [vect_skip].
      rewrite IHm.
      rewrite (eta xs) at 2. now setoid_rewrite vect_to_list_eq_cons.
  Qed.

  Lemma vect_to_list_inj (n : nat) (xs ys : Vector.t X n) :
    (to_list xs = to_list ys) -> xs = ys.
  Proof.
    intros H. induction xs.
    - pose proof vector_inv_0 ys as ->. reflexivity.
    - pose proof vector_inv_S ys as (y&ys'&->).
      rewrite !vect_to_list_eq_cons in H. injection H as ->.
      f_equal; eauto.
  Qed.

  Lemma vect_to_list_length (n : nat) (xs : Vector.t X n) :
    length (vect_to_list xs) = n.
  Proof.
    induction xs.
    - reflexivity.
    - rewrite vect_to_list_eq_cons. cbn. f_equal. auto.
  Qed.

End VectToList.

Create HintDb vect_to_list.
Hint Rewrite vect_to_list_eq_cons : vect_to_list.
Hint Rewrite vect_to_list_eq_nil : vect_to_list.
Hint Rewrite vect_to_list_tl : vect_to_list.
Hint Rewrite vect_to_list_cast : vect_to_list.
Hint Rewrite vect_to_list_app : vect_to_list.
Hint Rewrite vect_to_list_take : vect_to_list.
Hint Rewrite vect_to_list_skip : vect_to_list.

Ltac vect_to_list := autorewrite with vect_to_list.
Ltac simp_vect_to_list := apply vect_to_list_inj; vect_to_list.
