Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax.
Require Import dlPCF_iSubst.
Require Import ForestCard dlPCF_Types dlPCF_Sum dlPCF_Contexts.
Require PCF_Types.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.
Open Scope vector_scope.

From Coq Require Import Vector Fin.
From Coq Require Import Lia.

Open Scope PCF.


(** ** dlPCF Type system *)

Implicit Types (ϕ : nat).


(** *** Typing rules *)

Reserved Notation "ty! Φ ; Γ '⊢(' i ')' t : τ" (at level 80, t at level 99,
                                                    format "ty!  Φ ;  Γ  '⊢(' i ')' '/' '['  t  ':'  τ ']'").

(** [Γ], [M], are the context/cost index/type after subtyping. Note that these are parameters
instead of variables. [ρ] could be an parameter to, but the order would be wrong. *)
Inductive hasty {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) : tm -> mty ϕ -> Prop :=
| ty_Var x ρ :
    (* Subtyping *)
    forall (Hsub: mty! Φ ⊢ Γ x ⊑ ρ),
      (ty! Φ; Γ ⊢(M) (Var x) : ρ)
| ty_Lam (t : tm) (I : idx ϕ) (Δ : ctx (S ϕ)) (σ τ : mty (S ϕ)) (K : idx (S ϕ))
         (ρ : mty ϕ) (* The type after subtyping *)
         (Γ' : ctx ϕ) : (* The context sum of [Δ], before subtyping *)
    forall (Hty: @hasty (S ϕ) (fun xs => hd xs < I (tl xs) /\ Φ (tl xs)) (σ .: Δ) K t τ)
      (Hbsum: ctxBSum (Lam t) I Δ Γ')
      (* Subtyping *)
      (HΓ: ctx! (Lam t); Φ ⊢ Γ ⊑ Γ') (* The variable "x" is excluded from the subtyping *)
      (HM: sem! Φ ⊨ fun xs => (I xs + Σ_{a<I xs} K (a ::: xs)) <= M xs)
      (Hρ: mty! Φ ⊢ [<I] ⋅ σ ⊸ τ ⊑ ρ),
      (ty! Φ; Γ ⊢(M) Lam t : ρ)
| ty_Fix (t : tm) (* The term! *)
         (Ib : idx (S ϕ)) (* depends on [b<H] *)
         (Δ : ctx (S ϕ)) (* depends on [b<H] *)
         (Jb : idx (S ϕ)) (* depends on [b<H] *)
         (A : lty (S (S ϕ))) (* depends on [a<I] and [b<H] *)
         (B : lty (S (S ϕ))) (* depends on [a<1] and [b<H] *)
         (K : idx ϕ) (* Fixed number of times the fixpoint will be used *)
         (card1 : idx (S (S ϕ))) (* Value of the cardinal in the second line. Depends on [a] and [b]. *)
         (card2 : idx (S ϕ)) (* Value of the cardinal that is substituted for [b] in the last line. Depends on [a]. *)
         (cardH : idx ϕ) (* Cardinal of the entire tree *)
         (Γ' : ctx ϕ) (* The context sum of [Γb], before subtyping *)
         (ρ : mty ϕ) : (* The type after subtyping *)
    forall (Hty: (* Typing obligation *)
         @hasty (S ϕ)
                (fun xs =>
                   let b := hd xs in
                   let xs' := tl xs in
                   b < cardH (xs') /\ Φ xs') ([< Ib] ⋅ A .: Δ)
                (Jb) (Lam t) ([<iConst 1] ⋅ B))
      (* Subtyping obligation *)
      (Hsub:
         lty! fun xs => let a := hd xs in
                     let b := hd (tl xs) in
                     let xs' : Vector.t nat ϕ := tl (tl xs) in
                     a < Ib (b ::: xs') /\ b < cardH xs' /\ Φ xs' ⊢
             subst_lty B
             (fun f xs =>
                let a := hd xs in
                let b := hd (tl xs) in
                let xs' := tl (tl xs) in
                f (0 ::: S (card1 xs + b) ::: xs'))
             ⊑ A)
      (* Three forest cardinalities *)
      (HcardH: sem! Φ ⊨ fun xs => isForestCard (fun b => Ib (b ::: xs)) (K xs) (cardH xs))
      (Hcard1:
         sem! fun xs => let a := hd xs in
                     let b := hd (tl xs) in
                     let xs' : Vector.t nat ϕ := tl (tl xs) in
                     a < Ib (b ::: xs') /\ b < cardH xs' /\ Φ xs'
            ⊨ fun xs => let a := hd xs in
                     let b := hd (tl xs) in
                     let xs' := tl (tl xs) in
                     isForestCard (fun c => Ib (S (b+c) ::: xs')) a (card1 xs))
      (Hcard2:
         sem! fun xs => let a := hd xs in
                     let xs' := tl xs in
                     a < K xs' /\ Φ xs'
            ⊨ fun xs => let a := hd xs in
                     let xs' := tl xs in
                     isForestCard (fun b => Ib (b ::: xs')) a (card2 xs))
      (* The bounded sum *)
      (Hbsum: ctxBSum (Fix t) cardH Δ Γ')
      (* Subtyping *)
      (HΓ: ctx! (Fix t); Φ ⊢ Γ ⊑ Γ')
      (HM: sem! Φ ⊨ fun xs => (Σ_{b < cardH xs} Jb (b ::: xs)) <= M xs)
      (Hρ:
         mty! Φ ⊢ ([<K] ⋅ (subst_lty
                             B
                             (fun f xs =>
                                let a := hd xs in
                                let xs' := tl xs in
                                f (0 ::: (card2 xs) ::: xs'))))
            ⊑ ρ),
      (ty! Φ; Γ ⊢(M) Fix t : ρ)
| ty_App (t1 t2 : tm) (Δ1 Δ2 : ctx ϕ) (σ τ : mty (S ϕ)) (K1 K2 : idx ϕ)
         (Γ' : ctx ϕ)  (* The context sum before subtyping *)
         (ρ : mty ϕ) : (* The type after subtyping *)
    forall (Hty1: ty! Φ; Δ1 ⊢(K1) t1 : [<iConst 1] ⋅ (σ ⊸ τ))
      (Hty2: ty! Φ; Δ2 ⊢(K2) t2 : (subst_mty_beta_ground σ (iConst 0)))
      (Hmsum: ctxMSum (App t1 t2) Δ1 Δ2 Γ')
      (* Subtyping *)
      (HΓ: ctx! (App t1 t2); Φ ⊢ Γ ⊑ Γ')
      (HM: sem! Φ ⊨ fun xs => (K1 xs + K2 xs) <= M xs)
      (Hρ: mty! Φ ⊢ subst_mty_beta_ground τ (iConst 0) ⊑ ρ),
      (ty! Φ; Γ ⊢(M) t1 t2 : ρ)
| ty_Ifz (Δ1 Δ2 : ctx ϕ) (t1 t2 t3 : tm) (M1 M2 : idx ϕ) (J : idx ϕ)
         (Γ' : ctx ϕ)  (* The context sum before subtyping *)
         (ρ : mty ϕ) : (* The type. (Subtyping in the other typings) *)
    forall (Hty1: ty! Φ; Δ1 ⊢(M1) t1 : Nat J)
      (Hty2: ty! (fun xs => J xs = 0 /\ Φ xs); Δ2 ⊢(M2) t2 : ρ)
      (Hty3: ty! (fun xs => 1 <= J xs /\ Φ xs); Δ2 ⊢(M2) t3 : ρ)
      (Hmsum: ctxMSum (Ifz t1 t2 t3) Δ1 Δ2 Γ')
      (* Subtyping *)
      (HΓ: ctx! (Ifz t1 t2 t3); Φ ⊢ Γ ⊑ Γ')
      (HM: sem! Φ ⊨ fun xs => M1 xs + M2 xs <= M xs),
      (ty! Φ; Γ ⊢(M) (Ifz t1 t2 t3) : ρ)
| ty_Const n ρ :
    (* Subtyping *)
    forall (Hty: mty! Φ ⊢ Nat (iConst n) ⊑ ρ),
      (ty! Φ; Γ ⊢(M) Const n : ρ)
| ty_Succ t K ρ :
    forall (Hty: ty! Φ; Γ ⊢(M) t : Nat K)
      (* Subtyping *)
      (Hρ: mty! Φ ⊢ Nat (K >> S) ⊑ ρ),
      ty! Φ; Γ ⊢(M) Succ t : ρ
| ty_Pred t K ρ :
    forall (Hty: ty! Φ; Γ ⊢(M) t : Nat K)
      (* Subtyping *)
      (Hρ: mty! Φ ⊢ Nat (K >> pred) ⊑ ρ),
      ty! Φ; Γ ⊢(M) Pred t : ρ
where "ty! Φ ; Γ '⊢(' i ')' t : τ" := (hasty Φ Γ i t τ).


(** Typing rules without subsumption *)

Section TypingRulesWithoutSubsumption.

  Context {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ).

  Lemma ty_Var' x M :
    ty! Φ; Γ ⊢(M) (Var x) : (Γ x).
  Proof. apply ty_Var. reflexivity. Qed.

  Lemma ty_Var'' x :
    ty! Φ; Γ ⊢(iConst 0) (Var x) : (Γ x).
  Proof. apply ty_Var'. Qed.

  Lemma ty_Lam' (t : tm) (I : idx ϕ) (Δ : ctx (S ϕ)) (σ τ : mty (S ϕ)) (K : idx (S ϕ)) :
    (@hasty (S ϕ) (fun xs => hd xs < I (tl xs) /\ Φ (tl xs)) (σ .: Δ) K t τ) ->
    ctxBSum (Lam t) I Δ Γ ->
    (ty! Φ; Γ ⊢(fun xs => I xs + Σ_{a<I xs} K (a ::: xs)) Lam t : ([<I] ⋅ σ ⊸ τ)).
  Proof. intros. eapply ty_Lam; eauto. all: hnf; reflexivity. Qed.

  Lemma ty_App' (t1 t2 : tm) (Δ1 Δ2 : ctx ϕ) (σ τ : mty (S ϕ)) (K1 K2 : idx ϕ) :
    (ty! Φ; Δ1 ⊢(K1) t1 : [<iConst 1] ⋅ (σ ⊸ τ)) ->
    (ty! Φ; Δ2 ⊢(K2) t2 : (subst_mty_beta_ground σ (iConst 0))) ->
    ctxMSum (App t1 t2) Δ1 Δ2 Γ ->
    (ty! Φ; Γ ⊢(fun xs => K1 xs + K2 xs) t1 t2 : (subst_mty_beta_ground τ (iConst 0))).
  Proof. intros. eapply ty_App; eauto. all: hnf; reflexivity. Qed.

  Lemma ty_Ifz' (Δ1 Δ2 : ctx ϕ) (t1 t2 t3 : tm) (M1 M2 : idx ϕ) (J : idx ϕ) (ρ : mty ϕ) :
    (ty! Φ; Δ1 ⊢(M1) t1 : Nat J) ->
    (ty! (fun xs => J xs = 0 /\ Φ xs); Δ2 ⊢(M2) t2 : ρ) ->
    (ty! (fun xs => 1 <= J xs /\ Φ xs); Δ2 ⊢(M2) t3 : ρ) ->
    ctxMSum (Ifz t1 t2 t3) Δ1 Δ2 Γ ->
    (ty! Φ; Γ ⊢(fun xs => M1 xs + M2 xs) (Ifz t1 t2 t3) : ρ).
  Proof. intros. eapply ty_Ifz; eauto. all: hnf; reflexivity. Qed.

  Lemma ty_Const' n M :
    (ty! Φ; Γ ⊢(M) Const n : (Nat (iConst n))).
  Proof. eapply ty_Const; eauto. reflexivity. Qed.

  Lemma ty_Const'' n :
    (ty! Φ; Γ ⊢(iConst 0) Const n : (Nat (iConst n))).
  Proof. apply ty_Const'. Qed.

  Lemma ty_Succ' t K M :
    (ty! Φ; Γ ⊢(M) t : Nat K) ->
    ty! Φ; Γ ⊢(M) Succ t : (Nat (K >> S)).
  Proof. intros. eapply ty_Succ; eauto. reflexivity. Qed.

  Lemma ty_Pred' t K M :
    (ty! Φ; Γ ⊢(M) t : Nat K) ->
    ty! Φ; Γ ⊢(M) Pred t : (Nat (K >> pred)).
  Proof. intros. eapply ty_Pred; eauto. reflexivity. Qed.

End TypingRulesWithoutSubsumption.





(** *** Subsumption *)

(* Weakening of [Φ] *)
Lemma ty_weaken_Φ {ϕ} (Φ Ψ : constr ϕ) Γ I t τ :
  (ty! Φ; Γ ⊢(I) t : τ) ->
  (sem! Ψ ⊨ Φ) ->
  (ty! Ψ; Γ ⊢(I) t : τ).
Proof.
  intros H1 H2. induction H1 in Ψ,H2|-*.
  - (* Var *) eapply ty_Var; eauto using submty_mono_Φ.
  - (* Lam *) eapply ty_Lam; eauto.
    + eapply IHhasty. firstorder.
    + eapply subCtx_mono_Φ; eauto.
    + firstorder.
    + eapply submty_mono_Φ; eauto.
  - (* Fix *) eapply ty_Fix; eauto.
    + eapply IHhasty; eauto. firstorder.
    + eapply sublty_mono_Φ; eauto. firstorder.
    + firstorder.
    + firstorder.
    + firstorder.
    + eapply subCtx_mono_Φ; eauto.
    + firstorder.
    + eapply submty_mono_Φ; eauto.
  - (* App *) eapply ty_App; eauto.
    + eapply subCtx_mono_Φ; eauto.
    + firstorder.
    + eapply submty_mono_Φ; eauto.
  - (* If *) eapply ty_Ifz; eauto.
    + apply IHhasty2. firstorder.
    + apply IHhasty3. firstorder.
    + eapply subCtx_mono_Φ; eauto.
    + firstorder.
  - (* Const *) eapply ty_Const; eauto using submty_mono_Φ.
  - (* Succ *) eapply ty_Succ; eauto using submty_mono_Φ.
  - (* Pred *) eapply ty_Pred; eauto using submty_mono_Φ.
Qed.


Lemma ty_sub {ϕ} (Φ : constr ϕ) Γ1 Γ2 t τ1 τ2 I1 I2 :
  (ty! Φ; Γ1 ⊢(I1) t : τ1) ->
  (ctx! t; Φ ⊢ Γ2 ⊑ Γ1) ->
  (mty! Φ ⊢ τ1 ⊑ τ2) ->
  (sem! Φ ⊨ fun xs => I1 xs <= I2 xs) ->
  ty! Φ; Γ2 ⊢(I2) t : τ2.
Proof.
  intros Hty Hctx Hmty Hsem.
  revert dependent Γ2. revert dependent I2. revert dependent τ2.
  induction Hty; intros.
  - (* Var *) eapply ty_Var; eauto. etransitivity; eauto. etransitivity; eauto.
    hnf in Hctx. cbn in Hctx. now apply Hctx.
  - (* Lam *) eapply ty_Lam; eauto.
    + etransitivity; eauto.
    + hnf in Hsem,HM|-*; intros xs Hxs; specialize Hsem with (1 := Hxs); specialize HM with (1 := Hxs). etransitivity; eassumption.
    + etransitivity; eauto.
  - (* Fix *) cbn zeta in *. eapply ty_Fix; eauto.
    + etransitivity; eauto.
    + hnf in Hsem,HM|-*; intros xs Hxs; specialize Hsem with (1 := Hxs); specialize HM with (1 := Hxs). etransitivity; eassumption.
    + etransitivity; eauto.
  - (* App *) eapply ty_App; eauto.
    + etransitivity; eauto.
    + hnf in Hsem,HM|-*; intros xs Hxs; specialize Hsem with (1 := Hxs); specialize HM with (1 := Hxs). etransitivity; eassumption.
    + etransitivity; eauto.
  - (* Ifz *) eapply ty_Ifz; eauto.
    + eapply IHHty2; eauto.
      * eapply submty_mono_Φ; eauto. firstorder.
      * hnf; reflexivity.
      * reflexivity.
    + eapply IHHty3; eauto.
      * eapply submty_mono_Φ; eauto. firstorder.
      * hnf; reflexivity.
      * reflexivity.
    + etransitivity; eauto.
    + hnf in Hsem,HM|-*; intros xs Hxs; specialize Hsem with (1 := Hxs); specialize HM with (1 := Hxs). etransitivity; eassumption.
  - (* Const *) eapply ty_Const. etransitivity; eauto.
  - (* Succ *) eapply ty_Succ.
    + eapply IHHty; eauto. reflexivity.
    + etransitivity; eauto.
  - (* Pred *) eapply ty_Pred.
    + eapply IHHty; eauto. reflexivity.
    + etransitivity; eauto.
Qed.


Lemma ty_sub' {ϕ} (Φ : constr ϕ) Γ t τ1 τ2 I1 I2 :
  (ty! Φ; Γ ⊢(I1) t : τ1) ->
  (mty! Φ ⊢ τ1 ⊑ τ2) ->
  (sem! Φ ⊨ fun xs => I1 xs <= I2 xs) ->
  ty! Φ; Γ ⊢(I2) t : τ2.
Proof. intros. eapply ty_sub; eauto. reflexivity. Qed.

(* Weakening/strengthening in/of the context *)
Lemma ty_weaken {ϕ} (Φ : constr ϕ) Γ1 Γ2 i t τ :
  (ty! Φ; Γ1 ⊢(i) t : τ) ->
  (ctx! t; Φ ⊢ Γ2 ⊑ Γ1) ->
  ty! Φ; Γ2 ⊢(i) t : τ.
Proof. intros. eapply ty_sub; eauto. reflexivity. hnf; reflexivity. Qed.


(** *** Typing inversion *)


Section TyInv.

  Context {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (ρ : mty ϕ).

  Lemma ty_Const_inv (k : nat) :
    hasty Φ Γ M (Const k) ρ ->
    mty! Φ ⊢ Nat (iConst k) ⊑ ρ.
  Proof. now inversion 1. Qed.

  Lemma ty_Lam_inv t :
    hasty Φ Γ M (Lam t) ρ ->
    exists (I : idx ϕ) (Δa : ctx (S ϕ)) (Δ : ctx ϕ) (Ka : idx (S ϕ)) σa τa
      (_ : ctxBSum (Lam t) I Δa Δ),
      (mty! Φ ⊢ ([< I] ⋅ σa ⊸ τa) ⊑ ρ) /\
      (ty! (fun xs => hd xs < I (tl xs) /\ Φ (tl xs)); (σa .: Δa) ⊢(Ka) t : τa) /\
      (ctx! (Lam t); Φ ⊢ Γ ⊑ Δ) /\
      sem! Φ ⊨ fun xs => (I xs + Σ_{a < I xs} Ka (a ::: xs)) <= M xs.
  Proof. inversion 1; eauto 11. Qed.

End TyInv.


Lemma ty_cast {ϕ1 ϕ2} {Φ : constr ϕ1} (Γ : ctx ϕ1) (I : idx ϕ1) (t : tm) (τ : mty ϕ1) (Heq : ϕ2 = ϕ1) :
  hasty Φ Γ I t τ ->
  hasty (constr_cast Φ Heq) (ctx_cast Γ Heq) (idx_cast I Heq) t (mty_cast τ Heq).
Proof. subst. now rewrite constr_cast_id, ctx_cast_id, idx_cast_id, !mty_cast_id. Qed.


Lemma subst_var_beta_fun_le {ϕ} (x : t (S ϕ)) (y : nat) (I : idx (y + (ϕ -' fin_to_nat x))) (K1 K2 : idx (S ϕ)) xs :
  subst_var_beta_fun x y I (fun xs => K1 xs <= K2 xs) xs <->
  subst_var_beta_fun x y I K1 xs <= subst_var_beta_fun x y I K2 xs.
Proof. firstorder. Qed.

Lemma subst_var_beta_fun_plus {ϕ} (x : t (S ϕ)) (y : nat) (I : idx (y + (ϕ -' fin_to_nat x))) (K1 K2 : idx (S ϕ)) xs :
  subst_var_beta_fun x y I (fun xs => K1 xs + K2 xs) xs =
  subst_var_beta_fun x y I K1 xs + subst_var_beta_fun x y I K2 xs.
Proof. reflexivity. Qed.

Lemma subst_var_beta_fun_sum {ϕ} (x : t (S ϕ)) (y : nat) (I : idx (y + (ϕ -' fin_to_nat x))) (K1 : idx (S ϕ)) (K2 : idx (S (S ϕ))) xs
      (Heq : S (y + ϕ) = y + S ϕ) :
  subst_var_beta_fun x y I (fun xs => Σ_{a < K1 xs} K2 (a ::: xs)) xs =
  Σ_{a < subst_var_beta_fun x y I K1 xs} idx_cast (subst_var_beta_fun (FS x) y I K2) Heq (a ::: xs).
Proof.
  erewrite subst_var_beta_fun_FS; cbn.
  unfold subst_var_beta_fun, idx_cast. f_equal. fext; intros a.
  f_equal. simp_vect_to_list. f_equal.
  - now rewrite vect_cast_inv.
  - repeat f_equal. now rewrite vect_cast_inv.
    Unshelve. auto.
Qed.

Lemma sem_subst_var_beta {ϕ} (x : t (S ϕ)) (y : nat) (I : idx (y + (ϕ -' fin_to_nat x))) (Φ Ψ : constr (S ϕ)) :
  (sem! Φ ⊨ Ψ) ->
  (sem! subst_var_beta_fun x y I Φ ⊨ subst_var_beta_fun x y I Ψ).
Proof. firstorder. Qed.


Lemma subst_beta_ground_fun_sum {ϕ} (I : idx ϕ) (K1 : idx (S ϕ)) (K2 : idx (S (S ϕ))) xs :
  subst_beta_ground_fun I (fun xs : Vector.t nat (S ϕ) => Σ_{a < K1 xs} K2 (a ::: xs)) xs =
  Σ_{a < subst_beta_ground_fun I K1 xs} (subst_beta_one_ground_fun I K2 (a ::: xs)).
Proof. reflexivity. Qed.



Require Import Program.Equality. (* [Axiom JMeq_eq] *)



(* TODO: Use these lemmas more often instead of [abstract lia] *)
Lemma aux1 ϕ y : y + S ϕ = S (y + ϕ). lia. Qed.
Lemma aux2 ϕ y : S (y + ϕ) = y + S ϕ. lia. Qed.


(* TODO: Move *)
Lemma lty_cast_inv {ϕ1 ϕ2} (A : lty ϕ1) (H1 : ϕ2 = ϕ1) (H2 : ϕ1 = ϕ2) :
  lty_cast (lty_cast A H1) H2 = A.
Proof. subst. now rewrite !lty_cast_id. Qed.
Lemma mty_cast_inv {ϕ1 ϕ2} (τ : mty ϕ1) (H1 : ϕ2 = ϕ1) (H2 : ϕ1 = ϕ2) :
  mty_cast (mty_cast τ H1) H2 = τ.
Proof. subst. now rewrite !mty_cast_id. Qed.

Lemma lty_cast_twice {ϕ1 ϕ2 ϕ3} (A : lty ϕ1) (H1 : ϕ2 = ϕ1) (H2 : ϕ3 = ϕ2) (H3 : ϕ3 = ϕ1) :
  lty_cast (lty_cast A H1) H2 = lty_cast A H3.
Proof. subst. now rewrite !lty_cast_id. Qed.
Lemma mty_cast_twice {ϕ1 ϕ2 ϕ3} (τ : mty ϕ1) (H1 : ϕ2 = ϕ1) (H2 : ϕ3 = ϕ2) (H3 : ϕ3 = ϕ1) :
  mty_cast (mty_cast τ H1) H2 = mty_cast τ H3.
Proof. subst. now rewrite !mty_cast_id. Qed.


Lemma sublty_congr {ϕ} Φ (A B : lty ϕ) :
  A = B -> lty! Φ ⊢ A ⊑ B.
Proof. now intros ->. Qed.
Lemma submty_congr {ϕ} Φ (σ τ : mty ϕ) :
  σ = τ -> mty! Φ ⊢ σ ⊑ τ.
Proof. now intros ->. Qed.

Lemma eqlty_congr {ϕ} Φ (A B : lty ϕ) :
  A = B -> lty! Φ ⊢ A ≡ B.
Proof. now intros ->. Qed.
Lemma eqmty_congr {ϕ} Φ (σ τ : mty ϕ) :
  σ = τ -> mty! Φ ⊢ σ ≡ τ.
Proof. now intros ->. Qed.



Lemma ty_subst_var_beta {ϕ} (x : Fin.t (S ϕ)) (y : nat) (Φ : constr (S ϕ)) (Γ : ctx (S ϕ)) (K : idx (S ϕ)) (t : tm)
      (τ : mty (S ϕ)) (I : idx (y + (ϕ -' fin_to_nat x))) :
  (ty! Φ; Γ ⊢(K) t : τ) ->
  (ty! subst_var_beta_fun x y I Φ; subst_ctx_var_beta x y I Γ ⊢(subst_var_beta_fun x y I K) t : subst_mty_var_beta x y I τ).
Proof.
  intros Hty. dependent induction Hty; intros; bound_inv.
  - (* Var *) apply ty_Var. now eapply submty_subst_var_beta.

  - (* Lam *) eapply ty_Lam; eauto.
    + eapply ty_sub. eapply ty_weaken_Φ.
      * specialize IHHty with (1 := eq_refl).
        do 4 specialize IHHty with (1 := JMeq_refl).
        specialize (IHHty (FS x)). cbn in IHHty. specialize (IHHty I).
        (* Vector.t nat (y + S ϕ) -> Prop *)
        (* ?Φ : constr (S (y + ϕ)) *)
        eapply ty_cast. now apply IHHty.
      * clear_all. hnf; intros xs (Hxs1&Hxs2). unfold constr_cast.
        erewrite subst_var_beta_fun_FS. cbn. erewrite vect_cast_inv.
        split; eauto.
        eapply Nat.lt_le_trans; eauto. apply Nat.eq_le_incl.
        instantiate (1 := subst_var_beta_fun _  _ _ _). reflexivity.
      * rewrite subst_ctx_var_beta_scons. rewrite ctx_cast_scons. eapply subCtx_scons; reflexivity.
      * reflexivity.
      * hnf; reflexivity.
    + cbn zeta. eapply ctxBSum_subst_var_beta. eassumption.
    + now eapply subCtx_subst_var_beta.
    + etransitivity. eapply sem_subst_var_beta; eassumption. clear.
      unfold idx_cast.
      intros xs Hxs.
      apply subst_var_beta_fun_le in Hxs.
      rewrite <- Hxs. clear Hxs. rewrite subst_var_beta_fun_plus.
      eapply Nat.eq_le_incl. f_equal.
      erewrite subst_var_beta_fun_sum. f_equal. fext; intros a. reflexivity.
    + cbn. eapply submty_mono_Φ.
      * etransitivity.
        2: eapply submty_subst_var_beta; eassumption.
        change (mty_cast (subst_mty_var_beta (FS x) y I σ) ?Heq ⊸ mty_cast (subst_mty_var_beta (FS x) y I τ0) ?Heq) with
               (lty_cast (subst_lty_var_beta (FS x) y I (σ ⊸ τ0)) Heq).
        rewrite <- subst_mty_var_beta_eq_Quant.
        eapply submty_subst_var_beta. eapply submty_Quant; hnf; reflexivity.
      * refine (fun x Hxs => Hxs).

  - (* Fix *)
    assert (y + ϕ = fin_to_nat x + (y + (ϕ -' fin_to_nat x))) as aux.
    { rewrite sub'_correct. pose proof fin_to_nat_lt x. lia. }

    eapply ty_Fix; eauto.
    + eapply ty_sub. eapply ty_weaken_Φ.
      specialize IHHty with (1 := eq_refl).
      do 4 specialize IHHty with (1 := JMeq_refl).
      specialize (IHHty (FS x)). cbn in IHHty. specialize (IHHty I).
      eapply ty_cast; eauto.
      * clear_all. hnf; intros xs (Hxs1&Hxs2). unfold constr_cast.
        unshelve erewrite subst_var_beta_fun_FS. apply aux1. cbn zeta. rewrite vect_cast_inv.
        split; eauto.
        eapply Nat.lt_le_trans; eauto. apply Nat.eq_le_incl.
        instantiate (1 := subst_var_beta_fun _  _ _ _). reflexivity.
      * rewrite subst_ctx_var_beta_scons. rewrite ctx_cast_scons. cbn zeta.
        eapply subCtx_scons. 2: reflexivity.
        unshelve erewrite subst_mty_var_beta_eq_Quant. abstract lia.
        cbn [mty_cast]. eapply submty_Quant. 2: hnf; reflexivity.
        instantiate (1 := aux2 _ _). unshelve erewrite lty_cast_twice. abstract lia. reflexivity.
      * cbn [mty_cast]. eapply eqmty_Quant. reflexivity. firstorder.
      * hnf; reflexivity.
    + cbn zeta.
      (* ?B: lty (S (S (y + ϕ))) *)
      etransitivity.
      2:{ eapply sublty_mono_Φ.
          - eapply sublty_cast. eapply sublty_subst_var_beta. eassumption.
          - clear_all. hnf; intros xs (Hxs1&Hxs2&Hxs3). unfold constr_cast.
            erewrite !subst_var_beta_fun_FS; cbn. erewrite vect_cast_twice, vect_cast_hd, vect_cast_tl.
            unfold subst_var_beta_fun. repeat_split; eauto.
            + eapply Nat.lt_le_trans, Nat.eq_le_incl; eauto. cbn.
              unfold idx_cast. erewrite subst_var_beta_fun_FS. cbn -[vect_cast]. erewrite vect_cast_twice, vect_cast_hd, vect_cast_tl.
              cbn. unfold subst_var_beta_fun. f_equal. f_equal.
              all: try now simp_vect_to_list.
              * erewrite !vect_to_list_hd. now vect_to_list.
              * simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
            + eapply Nat.lt_le_trans. erewrite !vect_cast_twice, vect_cast_hd. eassumption. apply Nat.eq_le_incl.
              unfold subst_var_beta_fun. f_equal. simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
            + unfold subst_var_beta_fun in Hxs3. eapply p_equal; eauto. clear_all.
              simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
      }
      {
        clear_all.
        setoid_rewrite subst_lty_cast; setoid_rewrite subst_lty_cast'.
        eapply sublty_congr.
        setoid_rewrite subst_lty_twice; unfold funcomp; cbn. unfold idx_cast.
        f_equal. fext; intros f xs.
        erewrite !subst_var_beta_fun_FS; cbn -[vect_cast]. erewrite vect_cast_twice, vect_cast_hd, vect_cast_tl.
        unfold subst_var_beta_fun. f_equal. f_equal. erewrite vect_cast_twice, vect_cast_hd, vect_cast_tl. cbn. f_equal.
        - f_equal. f_equal.
          + reflexivity.
          + now erewrite vect_cast_tl, vect_cast_twice, vect_cast_hd.
        - simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
      }
    + revert HcardH. clear_all. intros H. hnf in H|-*. intros xs Hxs.
      unfold subst_var_beta_fun in *; cbn in *. unfold idx_cast. cbn -[vect_cast].
      specialize (H ((vect_cast
          (vect_app (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
             (I (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
              ::: vect_skip y (ϕ -' fin_to_nat x)
                    (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))))
          (subst_var_beta_fun_subproof0 x)))).
      spec_assert H by firstorder. clear Hxs.
      eapply p_equal; eauto.
      replace (fun b : nat =>
         Ib
           (b
            ::: vect_cast
                  (vect_app (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
                     (I (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
                      ::: vect_skip y (ϕ -' fin_to_nat x)
                            (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))))
                  (subst_var_beta_fun_subproof0 x)))
        with (fun b : nat =>
     Ib
       (hd (vect_cast (vect_cast (b ::: xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))
        ::: vect_cast
              (vect_app
                 (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                    (tl (vect_cast (vect_cast (b ::: xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))
                 (I
                    (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                       (tl (vect_cast (vect_cast (b ::: xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))
                  ::: vect_skip y (ϕ -' fin_to_nat x)
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                           (tl (vect_cast (vect_cast (b ::: xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))))
              (f_equal Init.Nat.pred (subst_var_beta_fun_subproof0 (FS x))))) in H; eauto.
      { instantiate (1 := fun xs => (K0
           (vect_cast
              (vect_app (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
                 (I (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))
                  ::: vect_skip y (ϕ -' fin_to_nat x)
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast xs (subst_var_beta_fun_subproof x y)))))
              (subst_var_beta_fun_subproof0 x)))); eauto. }
      { clear_all. fext; intros b. f_equal. erewrite vect_cast_twice, vect_cast_hd. cbn. f_equal.
        simp_vect_to_list. do 3 f_equal. now simp_vect_to_list. }

    + cbn zeta. revert Hcard1. clear_all. intros H. hnf in H|-*. intros xs (Hxs1&Hxs2&Hxs3).
      instantiate (1 := aux1 _ _). unshelve instantiate (1 := _). abstract lia.
      unfold subst_var_beta_fun in *; cbn in *. unfold idx_cast. cbn -[vect_cast].
      erewrite !vect_cast_hd, !vect_cast_tl. erewrite !vect_cast_twice, vect_cast_hd.
      unshelve instantiate (1 := _). abstract lia.
      specialize (H (hd xs
        ::: hd (tl xs)
            ::: vect_cast
                  (vect_app
                     (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                        (vect_cast (tl (vect_cast (tl xs) (ty_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                           (subst_var_beta_fun_subproof x y)))
                     (I
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                           (vect_cast (tl (vect_cast (tl xs) (ty_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                              (subst_var_beta_fun_subproof x y)))
                      ::: vect_skip y (ϕ -' fin_to_nat x)
                            (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                               (vect_cast (tl (vect_cast (tl xs) (ty_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                          (subst_var_beta_fun_subproof x y))))) (subst_var_beta_fun_subproof0 x))).
      spec_assert H.
      { clear H. cbn. repeat_split; eauto.
        - eapply Nat.lt_le_trans; eauto. clear Hxs1 Hxs2 Hxs3. eapply Nat.eq_le_incl. unfold idx_cast. f_equal. f_equal.
          + now erewrite vect_cast_twice, vect_cast_hd.
          + simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
        - eapply Nat.lt_le_trans. eauto. clear Hxs1 Hxs2 Hxs3. eapply Nat.eq_le_incl. unfold idx_cast. f_equal.
          simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
        - clear Hxs1 Hxs2. eapply p_equal; eauto. clear. simp_vect_to_list. do 3 f_equal. now simp_vect_to_list. }
      clear Hxs1 Hxs2 Hxs3.
      replace (fun c : nat =>
         Ib
           (S
              (hd
                 (tl
                    (hd xs
                     ::: hd (tl xs)
                         ::: vect_cast
                               (vect_app
                                  (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                                     (vect_cast (tl (vect_cast (tl xs) (ty_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                        (subst_var_beta_fun_subproof x y)))
                                  (I
                                     (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                                        (vect_cast (tl (vect_cast (tl xs) (ty_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                           (subst_var_beta_fun_subproof x y)))
                                   ::: vect_skip y (ϕ -' fin_to_nat x)
                                         (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                                            (vect_cast (tl (vect_cast (tl xs) (ty_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                               (subst_var_beta_fun_subproof x y))))) (subst_var_beta_fun_subproof0 x))) + c)
            ::: tl
                  (tl
                     (hd xs
                      ::: hd (tl xs)
                          ::: vect_cast
                                (vect_app
                                   (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                                      (vect_cast (tl (vect_cast (tl xs) (ty_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                         (subst_var_beta_fun_subproof x y)))
                                   (I
                                      (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                                         (vect_cast (tl (vect_cast (tl xs) (ty_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                            (subst_var_beta_fun_subproof x y)))
                                    ::: vect_skip y (ϕ -' fin_to_nat x)
                                          (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                                             (vect_cast (tl (vect_cast (tl xs) (ty_subst_var_beta_subproof2 y ϕ Ib cardH x I xs)))
                                                        (subst_var_beta_fun_subproof x y))))) (subst_var_beta_fun_subproof0 x)))))
        with ((fun c : nat =>
     Ib
       (hd (vect_cast (vect_cast (S (hd (tl xs) + c) ::: tl (tl xs)) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))
        ::: vect_cast
              (vect_app
                 (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                    (tl (vect_cast (vect_cast (S (hd (tl xs) + c) ::: tl (tl xs)) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))
                 (I
                    (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                       (tl (vect_cast (vect_cast (S (hd (tl xs) + c) ::: tl (tl xs)) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))
                  ::: vect_skip y (ϕ -' fin_to_nat x)
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                           (tl
                              (vect_cast (vect_cast (S (hd (tl xs) + c) ::: tl (tl xs)) (aux2 ϕ y))
                                 (subst_var_beta_fun_subproof (FS x) y))))))
              (f_equal Init.Nat.pred (subst_var_beta_fun_subproof0 (FS x)))))) in H; eauto.
      { clear H. fext; intros z. f_equal. f_equal.
        - now erewrite vect_cast_twice, vect_cast_hd.
        - simp_vect_to_list. cbn -[vect_cast]. do 3 f_equal. now simp_vect_to_list. }
    + cbn zeta. revert Hcard2 aux. clear_all. intros H aux. hnf in H|-*. intros xs (Hxs1&Hxs2).
      unfold subst_var_beta_fun in *; cbn in *. unfold idx_cast.
      (* assert (aux: y + ϕ = y + S ϕ) by lia. *)
      specialize (H (hd xs ::: vect_cast
              (vect_app
                 (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                    ((vect_cast (tl xs) aux)))
                 (I
                    (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                       (vect_cast (tl xs) aux))
                  ::: vect_skip y (ϕ -' fin_to_nat x)
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                           (vect_cast (tl xs) aux))))
              (f_equal Init.Nat.pred (subst_var_beta_fun_subproof0 (FS x))))).
      spec_assert H.
      { clear H. cbn. split.
        - eapply Nat.lt_le_trans; eauto. eapply Nat.eq_le_incl. f_equal. simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
        - eapply p_equal; eauto. simp_vect_to_list. do 3 f_equal. now simp_vect_to_list. }
      clear Hxs1 Hxs2. cbn -[vect_to_list vect_cast] in *.
      assert ((fun b : nat =>
     Ib
       (hd (vect_cast (vect_cast (b ::: tl xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))
        ::: vect_cast
              (vect_app
                 (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                    (tl (vect_cast (vect_cast (b ::: tl xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))
                 (I
                    (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                       (tl (vect_cast (vect_cast (b ::: tl xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))
                  ::: vect_skip y (ϕ -' fin_to_nat x)
                        (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x))
                           (tl (vect_cast (vect_cast (b ::: tl xs) (aux2 ϕ y)) (subst_var_beta_fun_subproof (FS x) y))))))
              (f_equal Init.Nat.pred (subst_var_beta_fun_subproof0 (FS x)))))
        = (fun b : nat =>
    Ib
      (b
       ::: vect_cast
             (vect_app (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast (tl xs) aux))
                (I (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast (tl xs) aux))
                 ::: vect_skip y (ϕ -' fin_to_nat x) (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast (tl xs) aux))))
             (f_equal Init.Nat.pred (subst_var_beta_fun_subproof0 (FS x)))))).
      { clear H. fext; intros b. f_equal. simp_vect_to_list. f_equal.
        - now erewrite vect_cast_twice, vect_cast_hd.
        - do 3 f_equal. now simp_vect_to_list. }
      { setoid_rewrite H0. clear H0.
          now instantiate (1 := fun xs => (card2 (hd xs
            ::: vect_cast
                  (vect_app (vect_take (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast (tl xs) aux))
                     (I (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast (tl xs) aux))
                      ::: vect_skip y (ϕ -' fin_to_nat x) (vect_skip (fin_to_nat x) (y + (ϕ -' fin_to_nat x)) (vect_cast (tl xs) aux))))
                  (f_equal Init.Nat.pred (subst_var_beta_fun_subproof0 (FS x)))))). }
    + eapply ctxBSum_subst_var_beta. eassumption.
    + now apply subCtx_subst_var_beta.
    + revert HM. clear. etransitivity. eapply sem_subst_var_beta; eassumption. clear.
      unfold idx_cast.
      intros xs Hxs.
      apply subst_var_beta_fun_le in Hxs.
      rewrite <- Hxs. clear Hxs.
      eapply Nat.eq_le_incl. f_equal.
      erewrite subst_var_beta_fun_sum. f_equal. fext; intros a. reflexivity.
    + revert Hρ. clear. intros H.
      cbn. eapply submty_mono_Φ.
      * etransitivity.
        2: eapply submty_subst_var_beta; eassumption.
        clear H.
        unshelve erewrite subst_mty_var_beta_eq_Quant. apply aux2.
        eapply submty_Quant.
        -- eapply sublty_congr. setoid_rewrite subst_lty_cast'.
           setoid_rewrite subst_lty_twice; unfold funcomp, idx_cast; cbn -[vect_cast]. f_equal. fext; intros j xs; cbn -[vect_cast].
           erewrite subst_var_beta_fun_FS; cbn -[vect_cast]. unfold subst_var_beta_fun; cbn -[vect_cast].
           f_equal. f_equal.
           erewrite !vect_cast_tl. erewrite vect_cast_twice, vect_cast_hd. cbn.
           f_equal.
           ++ f_equal. f_equal.
              ** now erewrite vect_cast_twice, vect_cast_hd.
              ** simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
           ++ simp_vect_to_list. do 3 f_equal. now simp_vect_to_list.
        -- hnf; reflexivity.
      * refine (fun xs Hxs => Hxs).

  - (* App *)
    eapply ty_App.
    + eapply ty_sub. eapply ty_weaken_Φ.
      * eapply IHHty1; eauto.
      * refine (fun xs Hxs => Hxs).
      * reflexivity.
      * unshelve erewrite subst_mty_var_beta_eq_Quant. abstract lia. cbn. reflexivity.
      * hnf; reflexivity.
    + eapply ty_sub. eapply ty_weaken_Φ.
      * eapply IHHty2; eauto.
      * refine (fun xs Hxs => Hxs).
      * reflexivity.
      * change (subst_mty σ (subst_var_beta_fun (FS x) y I)) with (subst_mty_var_beta (FS x) y I σ).
        replace (subst_mty_beta_ground (mty_cast (subst_mty_var_beta (FS x) y I σ) (ty_subst_var_beta_subproof3 y ϕ)) (iConst 0))
          with (subst_mty_var_beta x y I (subst_mty_beta_ground σ (iConst 0))).
        2:{ clear_all. setoid_rewrite subst_mty_cast'. setoid_rewrite subst_mty_twice. f_equal.
            unfold funcomp, subst_beta_ground_fun, idx_cast. cbn -[vect_cast].
            fext; intros j xs. erewrite subst_var_beta_fun_FS; cbn zeta. rewrite vect_cast_inv. cbn. reflexivity. }
        eapply submty_subst_var_beta. reflexivity.
      * hnf; reflexivity.
    + eapply ctxMSum_subst_var_beta. eassumption.
    + eapply subCtx_subst_var_beta. assumption.
    + firstorder.
    + change (subst_mty τ0 (subst_var_beta_fun (FS x) y I)) with (subst_mty_var_beta (FS x) y I τ0).
      replace (subst_mty_beta_ground (mty_cast (subst_mty_var_beta (FS x) y I τ0) (ty_subst_var_beta_subproof3 y ϕ)) (iConst 0))
        with (subst_mty_var_beta x y I (subst_mty_beta_ground τ0 (iConst 0))).
      2:{ clear_all. setoid_rewrite subst_mty_cast'. setoid_rewrite subst_mty_twice. f_equal.
          unfold funcomp, subst_beta_ground_fun, idx_cast. cbn -[vect_cast].
          fext; intros j xs. erewrite subst_var_beta_fun_FS; cbn zeta. rewrite vect_cast_inv. cbn. reflexivity. }
      now eapply submty_subst_var_beta.
  - (* Ifz *) eapply ty_Ifz; eauto.
    + eapply ty_sub'.
      * eapply IHHty1; eauto.
      * cbn. reflexivity.
      * hnf; reflexivity.
    + eapply ty_sub'. eapply ty_weaken_Φ.
      * eapply IHHty2; eauto. reflexivity.
      * instantiate (1 := I). firstorder.
      * reflexivity.
      * hnf; reflexivity.
    + eapply ty_sub'. eapply ty_weaken_Φ.
      * eapply IHHty3; eauto. reflexivity.
      * firstorder.
      * reflexivity.
      * hnf; reflexivity.
    + apply ctxMSum_subst_var_beta; eauto.
    + apply subCtx_subst_var_beta; auto.
    + firstorder.
  - (* Const *) eapply ty_Const.
    change (Nat (iConst n)) with (subst_mty_var_beta x y I (Nat (iConst n))). now apply submty_subst_var_beta.
  - (* Succ *) eapply ty_Succ.
    + instantiate (1 := (subst_var_beta_fun x y I K0)).
      change (Nat ?K1) with (subst_mty_var_beta x y I (Nat K0)).
      eapply IHHty; eauto.
    + change (Nat _) with (subst_mty_var_beta x y I (Nat (K0>>S))).
      now apply submty_subst_var_beta.
  - (* Pred *) eapply ty_Pred.
    + instantiate (1 := (subst_var_beta_fun x y I K0)).
      change (Nat ?K1) with (subst_mty_var_beta x y I (Nat K0)).
      eapply IHHty; eauto.
    + change (Nat _) with (subst_mty_var_beta x y I (Nat (K0>>pred))).
      now apply submty_subst_var_beta.
      Unshelve. all: try now auto.
      Unshelve. all: cbn; try abstract lia.
      all: abstract (rewrite !sub'_correct; pose proof fin_to_nat_lt x; lia).
Qed.


Lemma ty_shift_var_add {ϕ} (Φ : constr (S ϕ)) (x : Fin.t (S ϕ)) (Γ : ctx (S ϕ)) (K : idx (S ϕ)) (t : tm) (τ : mty (S ϕ))
      (I : idx (ϕ -' fin_to_nat x)) :
  (ty! Φ; Γ ⊢(K) t : τ) ->
  (ty! shift_var_add_fun x I Φ; ctx_shift_var_add x Γ I
          ⊢(shift_var_add_fun x I K) t : mty_shift_var_add x τ I).
Proof.
  unfold mty_shift_var_add, shift_var_add_fun.
  intros H.
  eapply ty_sub; [eapply ty_weaken_Φ|..].
  1: apply ty_subst_var_beta with (y := 1) (x0 := x); eassumption.
  all: try now (reflexivity + hnf;reflexivity).
Qed.

Lemma ty_shift_add {ϕ} (Φ : constr (S ϕ)) (Γ : ctx (S ϕ)) (K : idx (S ϕ)) (t : tm) (τ : mty (S ϕ)) (I : idx ϕ) :
  (ty! Φ; Γ ⊢(K) t : τ) ->
  (ty! shift_add_fun I Φ; ctx_shift_add Γ I
          ⊢(shift_add_fun I K) t : mty_shift_add τ I).
Proof.
  unfold mty_shift_add. intros H.
  rewrite !shift_add_fun_correct.
  replace (ctx_shift_add Γ I) with (ctx_shift_var_add Fin0 Γ I).
  2:{ unfold ctx_shift_var_add, ctx_shift_add. fext; intros var. now erewrite mty_shift_add_correct. }
  replace (ctx_shift_var_add Fin0 Γ I) with (subst_ctx_var_beta Fin0 1 (fun xs : Vector.t nat (S ϕ) => I (tl xs) + hd xs) Γ).
  2:{ unfold ctx_shift_var_add, subst_ctx_var_beta, subst_mty_var_beta; cbn. reflexivity. }
  now apply ty_subst_var_beta with (x := Fin0) (y := 1).
Qed.


Lemma ty_subst_beta_ground {ϕ} (Φ : constr (S ϕ)) (Γ : ctx (S ϕ)) (K : idx (S ϕ)) (t : tm) (τ : mty (S ϕ)) (I : idx ϕ) :
  (ty! Φ; Γ ⊢(K) t : τ) ->
  (ty! subst_beta_ground_fun I Φ; subst_ctx_beta_ground I Γ
       ⊢(subst_beta_ground_fun I K) t : subst_mty_beta_ground τ I).
Proof.
  intros H.
  rewrite !subst_beta_ground_fun_correct.
  rewrite subst_mty_beta_ground_correct, !subst_beta_fun_correct.
  replace (subst_ctx_beta_ground I Γ) with (subst_ctx_var_beta Fin0 0 I Γ).
  2:{ unfold subst_ctx_var_beta, subst_ctx_beta_ground. fext. intros. now rewrite subst_mty_beta_ground_correct. }
  now apply ty_subst_var_beta with (x := Fin0) (y := 0).
Qed.




(** *** Retyping closed terms for arbitrary contexts *)

Lemma retype_free {ϕ} (Φ : constr ϕ) Γ Σ M t τ :
  (ty! Φ; Γ ⊢(M) t : τ) ->
  (forall var, free var t -> Σ var = Γ var) ->
  (ty! Φ; Σ ⊢(M) t : τ).
Proof.
  intros Hty Hext. induction Hty in Σ,Hext|-*; bound_inv.
  - (* Var *) eapply ty_Var. rewrite Hext; eauto. cbn; reflexivity.
  - (* Lam *)
    eapply ty_Lam.
    + eapply ty_weaken. eapply IHHty; eauto.
      hnf; intros [ | var].
      { cbn. reflexivity. }
      cbn [scons]. instantiate (1 := fun var => _). cbn beta zeta. reflexivity.
    + instantiate (1 := fun var => Γ' var). hnf. intros var Hvar. now apply Hbsum.
    + intros i Hi. rewrite Hext; eauto.
    + assumption.
    + assumption.
  - (* Fix *)
    eapply ty_Fix.
    + eapply ty_weaken.
      -- eapply IHHty. eauto.
      -- cbn. apply subCtx_scons; reflexivity.
    + eauto.
    + eauto.
    + eauto.
    + eauto.
    + instantiate (1 := fun var => Γ' var). hnf. intros var Hvar. now apply Hbsum.
    + hnf. intros var **. rewrite Hext; auto.
    + firstorder.
    + firstorder.
  - (* App *) eapply ty_App.
    + now eapply IHHty1; eauto.
    + now eapply IHHty2; eauto.
    + instantiate (1 := fun var => Γ' var). hnf. intros var Hvar. eauto.
    + hnf. intros var **. rewrite Hext; auto.
    + assumption.
    + assumption.
  - (* Ifz *) eapply ty_Ifz.
    + now eapply IHHty1; eauto.
    + now eapply IHHty2; eauto.
    + now eapply IHHty3; eauto.
    + instantiate (1 := fun var => Γ' var). hnf. intros var Hvar. eauto.
    + hnf. intros var **. rewrite Hext; auto.
    + assumption.
  - (* Const *) now apply ty_Const.
  - (* Succ *) eapply ty_Succ; eauto.
  - (* Pred *) eapply ty_Pred; eauto.
Qed.

(** The context is irrelevant for closed terms *)
Lemma retype_closed {ϕ} (Φ : constr ϕ) Γ Σ M t τ :
  (ty! Φ; Γ ⊢(M) t : τ) ->
  closed t ->
  (ty! Φ; Σ ⊢(M) t : τ).
Proof.
  intros Hty Hclos. eapply retype_free; eauto.
  rewrite free_closed_iff in Hclos.
  firstorder.
Qed.


(** *** Ex-falso *)


Fixpoint decorate (A : PCF_Types.ty) (ϕ : nat) : mty ϕ :=
  match A with
  | PCF_Types.Nat => Nat (iConst 0)
  | PCF_Types.Arr A B => Quant (iConst 0) (decorate A (S ϕ) ⊸ decorate B (S ϕ))
  end.

Lemma decorate_strip (A : PCF_Types.ty) (ϕ : nat) :
  mty_strip (decorate A ϕ) = A.
Proof. induction A in ϕ|-*; cbn; f_equal; auto. Qed.

Lemma msum_decorate (A : PCF_Types.ty) (ϕ : nat) :
  msum (decorate A ϕ) (decorate A ϕ) (decorate A ϕ).
Proof.
  destruct A in ϕ|-*; cbn.
  - repeat_split; cbn; f_equal.
    + setoid_rewrite <- subst_mty_id at 1. f_equal. unfold shift_add_fun, iConst. cbn. fext; intros j xs. now rewrite <- eta.
    + setoid_rewrite <- subst_mty_id at 1. f_equal. unfold shift_add_fun, iConst. cbn. fext; intros j xs. now rewrite <- eta.
    + reflexivity.
  - auto.
Qed.


Lemma decorate_subst (ϕ : nat) (A : PCF_Types.ty)
      (x : Fin.t (S ϕ)) (* (i : idx (S (S (ϕ -' fin_to_nat x)))) *) :
  subst_mty_var_beta x 2 hd (decorate A (S ϕ)) = decorate A (S (S ϕ)).
Proof.
  (* clear i. *)
  induction A in ϕ,x|-*.
  - cbn [decorate]. unshelve erewrite !subst_mty_var_beta_eq_Quant. abstract lia. f_equal.
    change (lty_cast (subst_lty_var_beta (FS x) 2 hd (?A ⊸ ?B)) ?H) with
        (mty_cast (subst_mty_var_beta (FS x) 2 hd A) H ⊸ mty_cast (subst_mty_var_beta (FS x) 2 hd B) H).
    f_equal.
    + specialize IHA1 with (x := FS x) (ϕ := S ϕ). setoid_rewrite IHA1. now rewrite mty_cast_id.
    + specialize IHA2 with (x := FS x) (ϕ := S ϕ). setoid_rewrite IHA2. now rewrite mty_cast_id.
  - reflexivity.
Qed.


Lemma bsum_decorate (A : PCF_Types.ty) (ϕ : nat) :
  bsum (iConst 0) (decorate A (S ϕ)) (decorate A ϕ).
Proof.
  destruct A.
  - cbn. right; hnf. eexists _,_; split; cbn. 2: reflexivity.
    f_equal. cbn. f_equal.
    + unfold iConst; cbn.
      replace (subst_mty (decorate A1 (S ϕ)) (fun (f : idx (S ϕ)) (xs : Vector.t nat (S (S ϕ))) => f (hd xs + (Σ_{_ < hd (tl xs)} 0) ::: tl (tl xs))))
        with (subst_mty (decorate A1 (S ϕ)) (fun (f : idx (S ϕ)) (xs : Vector.t nat (S (S ϕ))) => f (hd xs ::: tl (tl xs)))).
      2:{ f_equal. fext; intros f xs. now rewrite Sum_constO, Nat.add_0_r. }
      setoid_rewrite subst_mty_beta_two_correct'. now rewrite decorate_subst.
    + unfold iConst; cbn.
      replace (subst_mty (decorate A2 (S ϕ)) (fun (f : idx (S ϕ)) (xs : Vector.t nat (S (S ϕ))) => f (hd xs + (Σ_{_ < hd (tl xs)} 0) ::: tl (tl xs))))
        with (subst_mty (decorate A2 (S ϕ)) (fun (f : idx (S ϕ)) (xs : Vector.t nat (S (S ϕ))) => f (hd xs ::: tl (tl xs)))).
      2:{ f_equal. fext; intros f xs. now rewrite Sum_constO, Nat.add_0_r. }
      setoid_rewrite subst_mty_beta_two_correct'. now rewrite decorate_subst.
  - cbn. left. hnf. eexists; split. 2: reflexivity. reflexivity.
Qed.



Definition decorateCtx (Γ : PCF_Syntax.ctx) (ϕ : nat) : ctx ϕ := fun y => decorate (Γ y) ϕ.

Lemma decorateCtx_strip (Γ : PCF_Types.ctx) (ϕ : nat) :
  stripCtx (decorateCtx Γ ϕ) = Γ.
Proof. fext; intros y. unfold stripCtx, decorateCtx. apply decorate_strip. Qed.

Lemma ctxMSum_decorate (Γ : PCF_Types.ctx) (ϕ : nat) (t : tm) :
  ctxMSum t (decorateCtx Γ ϕ) (decorateCtx Γ ϕ) (decorateCtx Γ ϕ).
Proof. hnf. intros x _. unfold decorateCtx. apply msum_decorate. Qed.

Lemma ctxBSum_decorate (Γ : PCF_Types.ctx) (ϕ : nat) (t : tm) :
  ctxBSum t (iConst 0) (decorateCtx Γ (S ϕ)) (decorateCtx Γ ϕ).
Proof. hnf. intros x _. unfold decorateCtx. apply bsum_decorate. Qed.


Lemma ty_exfalso {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (K : idx ϕ) (t : tm) (τ : mty ϕ) :
  (sem! Φ ⊨ fun _ => False) ->
  PCF_Types.hasty (stripCtx Γ) t (mty_strip τ) ->
  hasty Φ Γ K t τ.
Proof.
  intros Hsem Hsty.
  remember (stripCtx Γ) as Σ. remember (mty_strip τ) as A. induction Hsty in ϕ,Φ,Hsem,K,Γ,HeqΣ,τ,HeqA|-*.
  - (* Var *) eapply ty_Var. apply submty_exfalso; eauto. subst. unfold stripCtx in H. congruence.
  - (* Lam *) rename τ1 into A, τ2 into B.
    destruct τ as [ ? | k c]; cbn in *; try discriminate. destruct c as [τ1 τ2]. cbn in HeqA. injection HeqA as -> ->.
    eapply ty_Lam with (I := iConst 0).
    + eapply IHHsty; eauto.
      * firstorder.
      * rewrite stripCtx_scons; f_equal. symmetry. apply decorateCtx_strip.
    + apply ctxBSum_decorate.
    + apply subCtx_exfalso. auto. subst. now rewrite decorateCtx_strip.
    + hnf; intros xs Hxs; exfalso; firstorder.
    + apply submty_exfalso; eauto.

  - (* Fix *) subst. destruct τ as [ ? | k c]; cbn in *; try discriminate.
    1: now inv Hsty. (* The Fix can't have type [Nat] *)
    (* The forest cardinalites are all bogus because the obligations for them are semantical. *)
    eapply ty_Fix with (cardH := iConst 0) (card1 := iConst 0) (card2 := iConst 0).
    + eapply IHHsty; eauto.
      * firstorder.
      * rewrite stripCtx_scons; cbn. f_equal.
        2: symmetry; apply decorateCtx_strip.
        (* Bogus substitution *)
        instantiate (1 := subst_lty c (fun j xs => j (tl xs))). now rewrite !subst_lty_strip.
      * cbn. instantiate (1 := subst_lty c (fun j xs => j (tl xs))). now rewrite !subst_lty_strip.
    + cbn. eapply sublty_exfalso; eauto.
      * hnf. unfold iConst. intros. lia.
      * now rewrite !subst_lty_strip.
    + hnf in Hsem|-*. intros. exfalso. eapply Hsem; eauto.
    + cbn. hnf in Hsem|-*. intros xs (Hxs1&Hxs2&Hxs3). unfold iConst in *. exfalso. lia.
    + cbn. hnf in Hsem|-*. intros xs (Hxs1&Hxs2). exfalso. eapply Hsem; eauto.
    + apply ctxBSum_decorate.
    + eapply subCtx_exfalso; eauto. now rewrite decorateCtx_strip.
    + firstorder.
    + eapply submty_exfalso; eauto. cbn. rewrite subst_lty_strip. now rewrite !subst_lty_strip.

  - (* App *) subst. eapply ty_App.
    + apply IHHsty1.
      * firstorder.
      * symmetry. apply decorateCtx_strip.
      * cbn. f_equal; symmetry; apply decorate_strip.
    + apply IHHsty2.
      * firstorder.
      * symmetry. apply decorateCtx_strip.
      * cbn. setoid_rewrite subst_mty_strip. now rewrite decorate_strip.
    + apply ctxMSum_decorate.
    + eapply subCtx_exfalso; eauto. now rewrite decorateCtx_strip.
    + instantiate (2 := iConst 0). instantiate (1 := K). hnf; reflexivity.
    + eapply submty_exfalso; eauto. setoid_rewrite subst_mty_strip. now rewrite decorate_strip.

  - (* Ifz *) subst. eapply ty_Ifz.
    + apply IHHsty1.
      * firstorder.
      * symmetry. apply decorateCtx_strip.
      * cbn. reflexivity.
    + apply IHHsty2.
      * instantiate (1 := iConst 42). firstorder.
      * symmetry. apply decorateCtx_strip.
      * reflexivity.
    + apply IHHsty3.
      * firstorder.
      * symmetry. apply decorateCtx_strip.
      * reflexivity.
    + apply ctxMSum_decorate.
    + eapply subCtx_exfalso; eauto. now rewrite decorateCtx_strip.
    + instantiate (2 := iConst 0). instantiate (1 := K). hnf; reflexivity.

  - (* Const *) apply ty_Const. now apply submty_exfalso.
  - (* Succ *) eapply ty_Succ; eauto. now apply submty_exfalso.
  - (* Pred *) eapply ty_Pred; eauto. now apply submty_exfalso.

    (* A lot of bogus indexes are left *)
    Unshelve. all: exact (iConst 0).
Qed.


(** *** Strip the typing to a PCF typing *)

Lemma msum_strip {ϕ} (σ τ ρ : mty ϕ) :
  msum σ τ ρ ->
  mty_strip σ = mty_strip τ /\
  mty_strip τ = mty_strip ρ.
Proof.
  destruct σ, τ, ρ; cbn; try tauto.
  intros (<-&->&H).
  now rewrite lty_shift_add_strip.
Qed.

Lemma bsum_strip {ϕ} (I : idx ϕ) (σ : mty (S ϕ)) (ρ : mty ϕ) :
  bsum I σ ρ ->
  mty_strip σ = mty_strip ρ.
Proof.
  intros [H|H]; hnf in H.
  - destruct H as (J&->&->). reflexivity.
  - destruct H as (J&A&->&->). cbn. now setoid_rewrite subst_lty_strip.
Qed.

Lemma ctxMSum_strip {ϕ} (Δ1 Δ2 Σ : ctx ϕ) t :
  ctxMSum t Δ1 Δ2 Σ ->
  forall x, free x t ->
       stripCtx Δ1 x = stripCtx Δ2 x /\
       stripCtx Δ2 x = stripCtx Σ x.
Proof. intros. hnf in H. specialize (H x H0). now apply msum_strip in H. Qed.

Lemma ctxBSum_strip {ϕ} (I : idx ϕ) (Δ : ctx (S ϕ)) (Σ : ctx ϕ) t :
  ctxBSum t I Δ Σ ->
  forall x, free x t -> stripCtx Δ x = stripCtx Σ x.
Proof. intros. hnf in X. specialize (X x H). now apply bsum_strip in X. Qed.


Lemma ty_strip {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (t : tm) (τ : mty ϕ) :
  hasty Φ Γ M t τ ->
  PCF_Types.hasty (stripCtx Γ) t (mty_strip τ).
Proof.
  induction 1; cbn.
  - (* Var *) constructor. unfold stripCtx. erewrite submty_strip; eauto.
  - (* Lam *) clear H.
    rewrite stripCtx_scons in IHhasty.
    apply submty_strip in Hρ. destruct ρ; cbn in *; try discriminate. destruct A; cbn in *; injection Hρ as Hρ Hρ'.
    pose proof subCtx_strip HΓ as L1. pose proof ctxBSum_strip Hbsum as L2.
    econstructor; eauto. rewrite <- Hρ, <- Hρ'.
    eapply PCF_Types.retype_free. 2: eassumption.
    intros [ | y] Hy; cbn; [reflexivity| ].
    now rewrite L1, L2 by auto.
  - (* Fix *) cbn zeta in *. clear HcardH Hcard1 Hcard2 HM. (* all semantical judgements *)
    rewrite stripCtx_scons in IHhasty.
    apply sublty_strip in Hsub. apply submty_strip in Hρ.
    destruct A, B; cbn in *. injection Hsub as Hsub Hsub'.
    rewrite subst_mty_strip in Hsub, Hsub'.
    destruct ρ; cbn in *; try discriminate. destruct A; cbn in *. injection Hρ as Hρ Hρ'.
    rewrite subst_mty_strip in Hρ, Hρ'.
    pose proof subCtx_strip HΓ as L1. pose proof ctxBSum_strip Hbsum as L2.
    econstructor; eauto. rewrite <- Hρ, <- Hρ'.
    eapply PCF_Types.retype_free. 2: eassumption.
    rewrite Hsub, Hsub'. intros [ | y] Hy; cbn; [reflexivity| ].
    now rewrite L1, L2 by auto.
  - (* App *) cbn in *. setoid_rewrite subst_mty_strip in IHhasty2.
    apply submty_strip in Hρ. setoid_rewrite subst_mty_strip in Hρ. setoid_rewrite <- Hρ. clear Hρ.
    pose proof ctxMSum_strip Hmsum as L1. pose proof subCtx_strip HΓ as L2.
    econstructor.
    + eapply PCF_Types.retype_free. 2: eapply IHhasty1. firstorder congruence.
    + eapply PCF_Types.retype_free. 2: eapply IHhasty2. firstorder congruence.
  - (* Ifz *) cbn in *.
    pose proof ctxMSum_strip Hmsum as L1. pose proof subCtx_strip HΓ as L2.
    econstructor.
    + eapply PCF_Types.retype_free. 2: eapply IHhasty1. firstorder congruence.
    + eapply PCF_Types.retype_free. 2: eapply IHhasty2. firstorder congruence.
    + eapply PCF_Types.retype_free. 2: eapply IHhasty3. firstorder congruence.
  - (* Const *) apply submty_strip in Hty. cbn in Hty. rewrite <- Hty. constructor.
  - (* Succ *) apply submty_strip in Hρ. cbn in Hρ. destruct ρ; cbn in *; try discriminate. 2: destruct A; discriminate. now constructor.
  - (* Pred *) apply submty_strip in Hρ. cbn in Hρ. destruct ρ; cbn in *; try discriminate. 2: destruct A; discriminate. now constructor.
Qed.


(** *** Trivial type *)

(** There is a more/less trivial type for every value *)

(* TODO: Move *)
Lemma subst_ctx_strip {m n} (Γ : ctx m) (f : idx m -> idx n) :
  stripCtx (subst_ctx Γ f) = stripCtx Γ.
Proof. unfold stripCtx; cbn. fext; intros var. apply subst_mty_strip. Qed.

Lemma ty_triv_Lam {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (t : tm) (σ τ : mty (S ϕ)) :
  PCF_Types.hasty (stripCtx Γ) (Lam t) (lty_strip (σ ⊸ τ)) ->
  closed (Lam t) ->
  ty! Φ; Γ ⊢(M) Lam t : [< iConst 0] ⋅ σ ⊸ τ.
Proof.
  intros Hty Hclos; cbn in *.
  eapply ty_Lam.
  3,5: reflexivity.
  - inv Hty.
    eapply ty_exfalso; eauto.
    + unfold iConst. firstorder.
    + rewrite stripCtx_scons. instantiate (1 := subst_ctx Γ (fun f xs => f (tl xs))). now rewrite subst_ctx_strip.
  - hnf; intros var Hvar. exfalso. contradict Hclos. cbn in *. rewrite free_bound_iff. firstorder.
  - unfold iConst; cbn. hnf; lia.
  Unshelve. exact (iConst 42).
Qed.

Lemma ty_triv_Fix {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (t : tm) (σ τ : mty (S ϕ)) :
  PCF_Types.hasty (stripCtx Γ) (Fix t) (lty_strip (σ ⊸ τ)) ->
  closed (Fix t) ->
  ty! Φ; Γ ⊢(M) Fix t : [< iConst 0] ⋅ σ ⊸ τ.
Proof.
  intros Hty Hclos; cbn in *.
  eapply ty_Fix with (cardH := iConst 0) (K := iConst 0); cbn zeta.
  - inv Hty.
    eapply ty_exfalso; eauto.
    + unfold iConst. firstorder omega.
    + rewrite stripCtx_scons. cbn.
      instantiate (2 := subst_ctx Γ (fun f xs => f (tl xs))); rewrite subst_ctx_strip.
      do 2 instantiate (1 := subst_mty σ (fun f xs => f (tl xs)) ⊸ subst_mty τ (fun f xs => f (tl xs))); cbn; rewrite !subst_mty_strip.
      eauto.
  - eapply sublty_exfalso; eauto.
    + unfold iConst. firstorder omega.
    + cbn. now rewrite !subst_mty_strip.
  - unfold iConst. hnf; intros. hnf. exists 0. cbn. reflexivity.
  - unfold iConst. firstorder omega.
  - unfold iConst. firstorder omega.
  - hnf; intros var Hvar. exfalso. contradict Hclos. cbn in *. rewrite free_bound_iff. firstorder.
  - reflexivity.
  - cbn. hnf; lia.
  - cbn. eapply submty_Quant.
    + setoid_rewrite subst_mty_twice; unfold funcomp; cbn.
      instantiate (1 := hd).
      replace (fun (f : idx (S ϕ)) (xs : Vector.t nat (S ϕ)) => f (hd xs ::: tl xs)) with (fun (f : idx (S ϕ)) xs => f xs).
      2:{ fext; intros. f_equal. now rewrite <- eta. }
      rewrite !subst_mty_id. reflexivity.
    + hnf; reflexivity.
  Unshelve. all: exact (iConst 0).
Qed.


(* Only needed for [ty_triv_sig] *)
Definition ty_triv {ϕ} (v : tm) (A : PCF_Types.ty) : mty ϕ :=
  match v, A with
  | Lam t, PCF_Types.Arr A B => [ < iConst 0] ⋅ decorate A _ ⊸ decorate B _
  | Fix t, PCF_Types.Arr A B => [ < iConst 0] ⋅ decorate A _ ⊸ decorate B _
  | Const k, PCF_Types.Nat => Nat (iConst k)
  | _, _ => Nat (iConst 0) (* invalid *)
  end.

Lemma ty_triv_sig {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) (M : idx ϕ) (v : tm) (A : PCF_Types.ty) :
  PCF_Types.hasty (stripCtx Γ) v A ->
  val v -> closed v ->
  { ρ | ty! Φ; Γ ⊢(M) v : ρ }.
Proof.
  intros Hty Hval Hclos.
  exists (ty_triv v A).
  destruct Hval.
  - (* Lam *) destruct A; inv Hty; cbn. apply ty_triv_Lam; auto. cbn. rewrite !decorate_strip. now constructor.
  - (* Fix *) destruct A; inv Hty; cbn.
    + apply ty_triv_Fix; auto. cbn. rewrite !decorate_strip. now constructor.
    + inv H0.
  - (* Const *) destruct A; inv Hty; cbn. now constructor.
Qed.
