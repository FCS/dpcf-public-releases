Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax.
Require Import dlPCF_Types.
Require Import dlPCF_iSubst.
Require Import dlPCF_Sum.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.
Open Scope vector_scope.

From Coq Require Import Vector Fin.
From Coq Require Import Lia.

Delimit Scope PCF with PCF.
Open Scope PCF.

Implicit Type ϕ : nat.


(** ** dlPCF Typing contexts *)


(** This file is *boring*. All definitions and proofs are
lifted from the modules [dlPCF_Types] and [dlPCF_Sum]. *)



(** *** Definition of contexts *)

(** Contexts assign modal types to variables *)
Notation ctx ϕ := (@ctx (mty ϕ)).

Definition def_ctx {ϕ} : ctx ϕ. Proof. exact (fun _ => def_mty). Qed.


Definition ctx_cast {ϕ1 ϕ2} (Γ : ctx ϕ1) (H : ϕ2 = ϕ1) : ctx ϕ2 :=
  fun y => mty_cast (Γ y) H.

Lemma ctx_cast_scons {ϕ1 ϕ2} (τ : mty ϕ1) (Γ : ctx ϕ1) (H : ϕ2 = ϕ1) :
  ctx_cast (τ .: Γ) H = (mty_cast τ H .: ctx_cast Γ H).
Proof. unfold ctx_cast. fext. intros [ |x]; cbn; reflexivity. Qed.

Lemma ctx_cast_id {ϕ} (Γ : ctx ϕ) (H : ϕ = ϕ) :
  ctx_cast Γ H = Γ.
Proof. unfold ctx_cast; fext; cbn. intros. now rewrite mty_cast_id. Qed.

Lemma ctx_cast_inv {ϕ} (Γ : ctx ϕ) (H : ϕ = ϕ) :
  ctx_cast Γ H = Γ.
Proof. unfold ctx_cast; fext; cbn. intros. now rewrite mty_cast_id. Qed.


(** *** Substitution *)

Definition subst_ctx {ϕ1 ϕ2:nat} (Γ : ctx ϕ1) (f : idx ϕ1 -> idx ϕ2) : ctx ϕ2 :=
  fun var => subst_mty (Γ var) f.

Lemma subst_ctx_cast m m' n (A : ctx m') (f : idx m -> idx n) (H : m = m') :
  subst_ctx (ctx_cast A H) f = subst_ctx A (fun j : idx m' => f (idx_cast j H)).
Proof. subst. rewrite ctx_cast_id. f_equal. fext; intros. now rewrite idx_cast_id. Qed.

Lemma subst_ctx_cast' m n n' (A : ctx m) (f : idx m -> idx n) (H : n' = n) :
  ctx_cast (subst_ctx A f) H = subst_ctx A (fun j : idx m => idx_cast (f j) H).
Proof. subst. rewrite ctx_cast_id. f_equal. fext; intros. now rewrite idx_cast_id. Qed.


(** General beta substitution *)
Definition subst_ctx_var_beta {ϕ:nat} (x : Fin.t (S ϕ)) (y : nat) (i : idx (y + (ϕ -' fin_to_nat x)))
           (Γ : ctx (S ϕ)) : ctx (y + ϕ) := fun var => subst_mty_var_beta x y i (Γ var).

(** Ground beta substitution *)
Definition subst_ctx_beta_ground {ϕ:nat} (i : idx ϕ) (Γ : ctx (S ϕ)) : ctx ϕ := fun var => subst_mty_beta_ground (Γ var) i.

(** Ground beta substitution of the next variable *)
Definition subst_ctx_beta_one_ground {ϕ:nat} (i : idx ϕ) (Γ : ctx (S (S ϕ))) : ctx (S ϕ) := fun var => subst_mty_beta_one_ground (Γ var) i.

(** (Ground) shifting *)
Definition ctx_shift_add {ϕ} (Γ : ctx (S ϕ)) (i : idx ϕ) : ctx (S ϕ) := fun var => mty_shift_add (Γ var) i.
Definition ctx_shift_sub {ϕ} (Γ : ctx (S ϕ)) (i : idx ϕ) : ctx (S ϕ) := fun var => mty_shift_sub (Γ var) i.

Definition ctx_shift_var_add {ϕ} (x : Fin.t (S ϕ)) (Γ : ctx (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) : ctx (S ϕ) :=
  fun var => mty_shift_var_add x (Γ var) i.
Definition ctx_shift_var_sub {ϕ} (x : Fin.t (S ϕ)) (Γ : ctx (S ϕ)) (i : idx (ϕ -' fin_to_nat x)) : ctx (S ϕ) :=
  fun var => mty_shift_var_sub x (Γ var) i.


Lemma subst_ctx_var_beta_scons {ϕ} (x : t (S ϕ)) (y : nat) (I: idx (y + (ϕ -' fin_to_nat x))) (τ : mty (S ϕ)) (Γ : ctx (S ϕ)) :
  subst_ctx_var_beta x y I (τ .: Γ) =
  ((subst_mty_var_beta x y I τ) .: (subst_ctx_var_beta x y I Γ)).
Proof. fext; intros [ | ]; cbn; auto. Qed.


(** *** Subtyping *)

(** All properties about contexts are only about the _free_ variables. That means we assume that a term has at most m
free variables (i.e. [bound m t]). We write [ctx! m; Φ ⊢ Γ ⊑ Δ] if the first [m] variables in [Γ] are subtypes of the
first [m] variables in [Δ] (in the constraint [Φ]). *)


Reserved Notation "'ctx!' t ; Φ '⊢' Γ ⊑ Δ" (at level 80, format "ctx!  t ;  Φ  ⊢ '/' '['  Γ  ⊑ '/'  Δ ']'", no associativity).
Reserved Notation "'ctx!' t ; Φ '⊢' Γ ≡ Δ" (at level 80, format "ctx!  t ;  Φ  ⊢ '/' '['  Γ  ≡ '/'  Δ ']'", no associativity).

(** Subtyping of the first [m] types *)
Definition subCtx {ϕ} (t : tm) (Φ : constr ϕ) (Δ Γ : ctx ϕ) : Prop :=
  forall var, free var t -> mty! Φ ⊢ Δ var ⊑ Γ var.
Notation "'ctx!' t ; Φ '⊢' Γ ⊑ Δ" := (subCtx t Φ Γ Δ).

Lemma subCtx_cast {ϕ1 ϕ2} m (Φ : constr ϕ1) (Γ Σ : ctx ϕ1) (H : ϕ2 = ϕ1)
         (H1: ctx! m; Φ ⊢ Γ ⊑ Σ) :
  (ctx! m; constr_cast Φ H ⊢ ctx_cast Γ H ⊑ ctx_cast Σ H).
Proof. hnf. intros. eapply submty_cast; eauto. Qed.

Lemma subCtx_refl {ϕ} {m} {Φ : constr ϕ} : reflexive _ (subCtx m Φ).
Proof. hnf. intros; hnf; intros. reflexivity. Qed.

Lemma subCtx_trans {ϕ} {m} {Φ : constr ϕ} : transitive _ (subCtx m Φ).
Proof. hnf. intros; hnf; intros. etransitivity; eauto. Qed.

Instance subCtx_Pre {ϕ} {m} {Φ : constr ϕ} : PreOrder (subCtx m Φ).
Proof.
  constructor.
  - hnf. apply subCtx_refl.
  - hnf. apply subCtx_trans.
Qed.

Lemma subCtx_scons {ϕ} (t : tm) (Φ : constr ϕ) (Δ Γ : ctx ϕ) σ τ :
  mty! Φ ⊢ σ ⊑ τ ->
  (forall var, free (S var) t -> mty! Φ ⊢ Δ var ⊑ Γ var) ->
  ctx! t; Φ ⊢ (σ .: Δ) ⊑ (τ .: Γ).
Proof.
  unfold subCtx. intros H1 H2. intros [ | ]; cbn; intros Hm.
  - assumption.
  - now apply H2.
Qed.

(** Context equality *)

Definition eqCtx {ϕ} (t : tm) (Φ : constr ϕ) (Γ Δ : ctx ϕ) : Prop :=
  (forall x, free x t -> mty! Φ ⊢ Γ x ≡ Δ x).
Notation "'ctx!' m ; Φ '⊢' Γ ≡ Δ" := (eqCtx m Φ Γ Δ) : PCF.

Lemma eqCtx_refl {ϕ} {m} (Φ : constr ϕ) : reflexive _ (eqCtx m Φ).
Proof. hnf. split; reflexivity. Qed.

Lemma eqCtx_trans {ϕ} {m} (Φ : constr ϕ) : transitive _ (eqCtx m Φ).
Proof. hnf. firstorder (etransitivity;eauto). Qed.

Lemma eqCtx_sym {ϕ} {m} (Φ : constr ϕ) : symmetric _ (eqCtx m Φ).
Proof. hnf. split; firstorder. Qed.

Instance eqCtx_Equiv {ϕ} {m} (Φ : constr ϕ) : Equivalence (eqCtx m Φ).
Proof.
  unfold eqCtx. constructor.
  - hnf. apply eqCtx_refl.
  - hnf. apply eqCtx_sym.
  - hnf. apply eqCtx_trans.
Qed.

Lemma subCtx_eqCtx {ϕ} {m} (Φ : constr ϕ) (Γ1 Γ2 Γ1' Γ2' : ctx ϕ) :
  ctx! m; Φ ⊢ Γ1  ⊑ Γ2 ->
  ctx! m; Φ ⊢ Γ1  ≡ Γ1' ->
  ctx! m; Φ ⊢ Γ2  ≡ Γ2' ->
  ctx! m; Φ ⊢ Γ1' ⊑ Γ2'.
Proof.
  intros H1 H2 H3. hnf in *.
  intros x Hx; specialize (H1 x Hx); specialize (H2 x Hx); specialize (H3 x Hx).
  now rewrite <- H2, <- H3.
Qed.

(* The setoid rewriting version of the above lemma *)
Instance subCtx_Proper {ϕ} {m} (Φ : constr ϕ) : Proper ((eqCtx m Φ) ==> (eqCtx m Φ) ==> Basics.flip Basics.impl) (subCtx m Φ).
Proof.
  hnf. intros. hnf. intros. hnf. intros. eapply subCtx_eqCtx; eauto.
  - now symmetry.
  - now symmetry.
Qed.

Lemma eqCtx_iff {ϕ} (t : tm) (Φ : constr ϕ) (Γ1 Γ2 : (ctx ϕ)) :
  (ctx! t; Φ ⊢ Γ1 ≡ Γ2) <->
  (ctx! t; Φ ⊢ Γ1 ⊑ Γ2) /\ (ctx! t; Φ ⊢ Γ2 ⊑ Γ1).
Proof. firstorder. Qed.

Lemma eqCtx_scons {ϕ} {t} (Φ : constr ϕ) (Γ1 Γ2 : (ctx ϕ)) τ1 τ2 :
  mty! Φ ⊢ τ1 ≡ τ2 ->
  (forall var, free (S var) t -> mty! Φ ⊢ Γ1 var ≡ Γ2 var) ->
  ctx! t; Φ ⊢ (τ1 .: Γ1) ≡ (τ2 .: Γ2).
Proof. intros H1 H2. hnf; intros [ | x] Hx; eauto. Qed.

Lemma subCtx_mono_Φ {ϕ} {t} {Φ Ψ : constr ϕ} (Γ Δ : ctx ϕ) :
  ctx! t; Φ ⊢ Γ ⊑ Δ ->
  sem! Ψ ⊨ Φ ->
  ctx! t; Ψ ⊢ Γ ⊑ Δ.
Proof.
  intros H1 H2. hnf in H1|-*.
  eauto using submty_mono_Φ.
Qed.

Lemma eqCtx_mono_Φ {ϕ} {t} {Φ : constr ϕ} (Γ Δ : ctx ϕ) :
  ctx! t; Φ ⊢ Γ ≡ Δ ->
  forall (Ψ : constr ϕ),
    sem! Ψ ⊨ Φ ->
    ctx! t; Ψ ⊢ Γ ≡ Δ.
Proof.
  intros H1 Ψ H2. hnf in H1,H2|-*.
  intros x Hx. specialize (H1 x Hx).
  eapply eqmty_mono_Φ; eauto.
Qed.



(** *** Modal sum for contexts *)

(* TODO: Rename [ctxMSum] to [msumCtx]? *)

(** Binary and bounded sums on contexts. *)
(** Again, we only consider the first [t] variables. *)
Definition ctxMSum {ϕ} (t : tm) (Γ1 Γ2 : ctx ϕ) : ctx ϕ -> Prop :=
  fun Δ => forall x, free x t -> msum (Γ1 x) (Γ2 x) (Δ x).

Definition ctxBSum {ϕ} (t : tm) (I : idx ϕ) (Γ : ctx (S ϕ)) : ctx ϕ -> Type :=
  fun Δ => forall x, free x t -> bsum I (Γ x) (Δ x).

Lemma ctxMSum_cast {ϕ1 ϕ2} (t : tm) (I : idx ϕ1) (Γ1 Γ2 : ctx ϕ1) (Δ : ctx ϕ1) (Heq : ϕ2 = ϕ1) :
  ctxMSum t Γ1 Γ2 Δ ->
  ctxMSum t (ctx_cast Γ1 Heq) (ctx_cast Γ2 Heq) (ctx_cast Δ Heq).
Proof. subst. now rewrite !ctx_cast_id. Qed.

Lemma ctxBSum_cast {ϕ1 ϕ2} (t : tm) (I : idx ϕ1) (Δ : ctx (S ϕ1)) (Γ : ctx ϕ1) (Heq1 : ϕ2 = ϕ1) (Heq2 : S ϕ2 = S ϕ1) :
  ctxBSum t I Δ Γ ->
  ctxBSum t (idx_cast I Heq1) (ctx_cast Δ Heq2) (ctx_cast Γ Heq1).
Proof. subst. now rewrite !idx_cast_id, !ctx_cast_id. Qed.


(** **** Substitution and (bounded) sums *)

Lemma ctxMSum_subst_var_beta {ϕ} t (x : Fin.t (S ϕ)) (y : nat) (i : idx (y + (ϕ -' fin_to_nat x))) (Γ1 Γ2 Δ : ctx (S ϕ)) :
  ctxMSum t Γ1 Γ2 Δ ->
  ctxMSum t (subst_ctx_var_beta x y i Γ1) (subst_ctx_var_beta x y i Γ2) (subst_ctx_var_beta x y i Δ).
Proof. intros H. hnf in H|-*. intros var. eauto using msum_subst_var_beta. Qed.

Lemma ctxMSum_subst_beta_ground {ϕ} t (Γ1 Γ2 Δ : ctx (S ϕ)) (i : idx ϕ) :
  ctxMSum t Γ1 Γ2 Δ ->
  ctxMSum t (subst_ctx_beta_ground i Γ1) (subst_ctx_beta_ground i Γ2) (subst_ctx_beta_ground i Δ).
Proof. intros H. hnf in H|-*. intros var. eauto using msum_subst_beta_ground. Qed.

Lemma ctxMSum_shift {ϕ} t (Γ1 Γ2 Δ : ctx (S ϕ)) (i : idx ϕ) :
  ctxMSum t Γ1 Γ2 Δ ->
  ctxMSum t (ctx_shift_add Γ1 i) (ctx_shift_add Γ2 i) (ctx_shift_add Δ i).
Proof. intros H. hnf in H|-*. intros var. eauto using msum_shift. Qed.


Lemma ctxBSum_subst_var_beta {ϕ} t (x : Fin.t (S ϕ)) (y : nat) (i : idx (y + (ϕ -' fin_to_nat x))) (I : idx (S ϕ))
      (Γ : ctx (S (S ϕ))) Δ
      (Heq: S (y + ϕ) = y + S ϕ) :
  ctxBSum t I Γ Δ ->
  ctxBSum t (subst_var_beta_fun x y i I) (ctx_cast (subst_ctx_var_beta (FS x) y i Γ) Heq) (subst_ctx_var_beta x y i Δ).
Proof. intros H. hnf in H|-*. intros var. eauto using bsum_subst_var_beta. Qed.

Lemma ctxBSum_subst_beta_ground {ϕ} t (Γ : ctx (S (S ϕ))) (Δ : ctx (S ϕ)) (i : idx ϕ) (I : idx (S ϕ)) :
  ctxBSum t I Γ Δ ->
  ctxBSum t (subst_beta_ground_fun i I) (subst_ctx_beta_one_ground i Γ) (subst_ctx_beta_ground i Δ).
Proof. intros H. hnf in H|-*. intros var. eauto using bsum_subst_beta_ground. Qed.

Lemma ctxBSum_shift {ϕ} t (Γ1 Γ2 Δ : ctx (S ϕ)) (i : idx ϕ) :
  ctxMSum t Γ1 Γ2 Δ ->
  ctxMSum t (ctx_shift_add Γ1 i) (ctx_shift_add Γ2 i) (ctx_shift_add Δ i).
Proof. intros H. hnf in H|-*. intros var. eauto using msum_shift. Qed.



(** **** Assocativity lemmas on contexts *)

Lemma ctxMSum_assoc {ϕ} t {Γ0 Γ1 Γ2 Δ Σ : ctx ϕ} :
  ctxMSum t Γ0 Γ1 Δ ->
  ctxMSum t Δ Γ2 Σ ->
  { Σ' & ctxMSum t Γ0 Σ' Σ /\ ctxMSum t Γ1 Γ2 Σ' }.
Proof.
  intros H1 H2. hnf in H1,H2.
  exists (fun x =>
       match free_dec x t with
       | left Hlt => proj1_sig (msum_assoc (H1 x Hlt) (H2 x Hlt))
       | right _ => Nat (iConst 42)
       end).
  split.
  - hnf. intros x Hx. destruct (free_dec x t) as [Hx'|?]; try tauto.
    destruct (msum_assoc (H1 x Hx') (H2 x Hx')) as (σ'&L1&L2); cbn; tauto.
  - hnf. intros x Hx. destruct (free_dec x t) as [Hx'|?]; try tauto.
    destruct (msum_assoc (H1 x Hx') (H2 x Hx')) as (σ'&L1&L2); cbn; tauto.
Qed.


(** Assocativity in the other direction *)
Lemma ctxMSum_assoc' {ϕ} t {Γ1 Γ2 Γ3 Δ Σ : ctx ϕ} :
  ctxMSum t Γ1 Σ Δ ->
  ctxMSum t Γ2 Γ3 Σ ->
  { Σ' & ctxMSum t Γ1 Γ2 Σ' /\ ctxMSum t Σ' Γ3 Δ }.
Proof.
  intros H1 H2. hnf in H1,H2.
  exists (fun x =>
       match free_dec x t with
       | left Hlt => proj1_sig (msum_assoc' (H1 x Hlt) (H2 x Hlt))
       | right _ => Nat (iConst 42)
       end).
  split.
  - hnf. intros x Hx. destruct (free_dec x t) as [Hx'|?]; try tauto.
    destruct (msum_assoc' (H1 x Hx') (H2 x Hx')) as (σ'&L1&L2); cbn; tauto.
  - hnf. intros x Hx. destruct (free_dec x t) as [Hx'|?]; try tauto.
    destruct (msum_assoc' (H1 x Hx') (H2 x Hx')) as (σ'&L1&L2); cbn; tauto.
Qed.


(** **** Merging and splitting *)


Lemma ctx_shift_add_scons {ϕ} (τ : mty (S ϕ)) (Γ : ctx (S ϕ)) (I : idx ϕ) :
  ctx_shift_add (τ .: Γ) I = ((mty_shift_add τ I) .: (ctx_shift_add Γ I)).
Proof. fext; intros [ | ]; reflexivity. Qed.
Lemma ctx_shift_sub_scons {ϕ} (τ : mty (S ϕ)) (Γ : ctx (S ϕ)) (I : idx ϕ) :
  ctx_shift_sub (τ .: Γ) I = ((mty_shift_sub τ I) .: (ctx_shift_sub Γ I)).
Proof. fext; intros [ | ]; reflexivity. Qed.

Lemma ctx_shift_var_add_scons {ϕ} (x : Fin.t (S ϕ)) (τ : mty (S ϕ)) (Γ : ctx (S ϕ)) (I : idx (ϕ -' fin_to_nat x)) :
  ctx_shift_var_add x (τ .: Γ) I = ((mty_shift_var_add x τ I) .: (ctx_shift_var_add x Γ I)).
Proof. fext; intros [ | ]; reflexivity. Qed.



Lemma ctxBSum_monotone {ϕ} t {I1 I2 : idx ϕ} {Γ : ctx (S ϕ)} {Δ : ctx ϕ} :
  ctxBSum t I2 Γ Δ ->
  { Σ & ctxBSum t I1 Γ Σ }.
Proof.
  intros H. hnf in H.
  exists (fun x =>
       match free_dec x t with
       | left Hlt => projT1 (bsum_monotone (I1 := I1) (H x Hlt))
       | right _ => Nat (iConst 42)
       end).
  hnf. intros x Hx. destruct (free_dec x t) as [Hx'|?]; try tauto.
  destruct bsum_monotone; cbn; auto.
Qed.

Lemma ctxBSum_monotone' {ϕ} (t : tm) {I1 I2 : idx ϕ} {Γ : ctx (S ϕ)} {Δ : ctx ϕ} :
  ctxBSum t I1 Γ Δ ->
  { Σ & { _ : (ctxBSum t I2 Γ Σ) |
    (ctx! t; fun xs => I1 xs <= I2 xs ⊢ Σ ⊑ Δ) /\
    ctx! t; fun xs => I2 xs <= I1 xs ⊢ Δ ⊑ Σ } }.
Proof.
  intros H. hnf in H.
  eexists (fun x =>
       match free_dec x t with
       | left Hlt => projT1 (bsum_monotone' (I1 := I1) (H x Hlt))
       | right _ => Nat (iConst 42)
       end).
  eexists. 2: split.
  - hnf. intros x Hx. destruct (free_dec x t) as [Hx'|?]; try tauto.
    exact (proj1_sig (projT2 (bsum_monotone' (H x Hx')))).
  - hnf. intros x Hx. destruct (free_dec x t) as [Hx'|?]; try tauto.
    exact (proj1 (proj2_sig (projT2 (bsum_monotone' (H x Hx'))))).
  - hnf. intros x Hx. destruct (free_dec x t) as [Hx'|?]; try tauto.
    exact (proj2 (proj2_sig (projT2 (bsum_monotone' (H x Hx'))))).
Qed.


Theorem ctxBSum_split {ϕ} t {I1 I2 : idx ϕ} {Γ : ctx (S ϕ)} {Δ : ctx ϕ} :
  ctxBSum t (fun xs => I1 xs + I2 xs) Γ Δ ->
  { Σ1 & { Σ2 &
    ctxBSum t I1 Γ Σ1 *
    ctxBSum t I2 (ctx_shift_add Γ I1) Σ2 *
    ctxMSum t Σ1 Σ2 Δ } } % type.
Proof.
  intros H. hnf in H.
  eexists (fun x =>
       match free_dec x t with
       | left Hlt => projT1 (bsum_split (H x Hlt))
       | right _ => Nat (iConst 42)
       end).
  eexists (fun x =>
       match free_dec x t with
       | left Hlt => projT1 (projT2 (bsum_split (H x Hlt)))
       | right _ => Nat (iConst 42)
       end).
  repeat_split.
  - hnf. intros x Hx. destruct (free_dec x t) as [Hx'|?]; try tauto.
    destruct (bsum_split (H x Hx')) as (ρ1&ρ2&(L1&L2)&L3); cbn; auto.
  - hnf. intros x Hx. destruct (free_dec x t) as [Hx'|?]; try tauto.
    destruct (bsum_split (H x Hx')) as (ρ1&ρ2&(L1&L2)&L3); cbn; auto.
  - hnf. intros x Hx. destruct (free_dec x t) as [Hx'|?]; try tauto.
    destruct (bsum_split (H x Hx')) as (ρ1&ρ2&(L1&L2)&L3); cbn; auto.
Qed.


(** **** Index substitution *)

(** General theorem *)
Lemma subCtx_subst_var_beta {ϕ} t (x : Fin.t (S ϕ)) (y : nat) (Φ : constr (S ϕ)) (Γ1 Γ2 : ctx (S ϕ))
      (I : idx (y + (ϕ -' fin_to_nat x))) :
  (ctx! t; Φ ⊢ Γ1 ⊑ Γ2) ->
  (ctx! t; subst_var_beta_fun x y I Φ ⊢
      subst_ctx_var_beta x y I Γ1 ⊑ subst_ctx_var_beta x y I Γ2).
Proof. intros. hnf in H|-*. intros var; specialize (H var). eauto using submty_subst_var_beta. Qed.

Lemma eqCtx_subst_var_beta {ϕ} t (x : Fin.t (S ϕ)) (y : nat) (Φ : constr (S ϕ)) (Γ1 Γ2 : ctx (S ϕ))
      (I : idx (y + (ϕ -' fin_to_nat x))) :
  (ctx! t; Φ ⊢ Γ1 ≡ Γ2) ->
  (ctx! t; subst_var_beta_fun x y I Φ ⊢
      subst_ctx_var_beta x y I Γ1 ≡ subst_ctx_var_beta x y I Γ2).
Proof. intros. intros var; specialize (H var). eauto using eqmty_subst_var_beta. Qed.

(** Shifting *)
Lemma subCtx_shift_var_add {ϕ} t (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (Γ1 Γ2 : ctx (S ϕ)) (I : idx (ϕ -' fin_to_nat x)) :
  (ctx! t; Φ ⊢ Γ1 ⊑ Γ2) ->
  (ctx! t; shift_var_add_fun x I Φ ⊢
      ctx_shift_var_add x Γ1 I ⊑ ctx_shift_var_add x Γ2 I).
Proof. intros H. hnf in H|-*. intros y; specialize (H y). eauto using submty_shift_var_add. Qed.

Lemma subCtx_shift_add {ϕ} t (Φ : constr (S ϕ)) (Γ1 Γ2 : ctx (S ϕ)) (I : idx ϕ) :
  (ctx! t; Φ ⊢ Γ1 ⊑ Γ2) ->
  (ctx! t; fun xs : Vector.t nat (S ϕ) => Φ (I (tl xs) + hd xs ::: tl xs) ⊢
      ctx_shift_add Γ1 I ⊑ ctx_shift_add Γ2 I).
Proof. intros H. hnf in H|-*. intros y; specialize (H y). eauto using submty_shift_add. Qed.

Lemma eqCtx_shift_var_add {ϕ} t (x : Fin.t (S ϕ)) (Φ : constr (S ϕ)) (Γ1 Γ2 : ctx (S ϕ)) (I : idx (ϕ -' fin_to_nat x)) :
  (ctx! t; Φ ⊢ Γ1 ≡ Γ2) ->
  (ctx! t; shift_var_add_fun x I Φ ⊢
      ctx_shift_var_add x Γ1 I ≡ ctx_shift_var_add x Γ2 I).
Proof. intros H. hnf in H|-*. intros y; specialize (H y). eauto using eqmty_shift_var_add. Qed.


(** Ground beta *)

Lemma subCtx_subst_beta_ground {ϕ} t (Φ : constr (S ϕ)) (Γ1 Γ2 : ctx (S ϕ)) (I : idx ϕ) :
  (ctx! t; Φ ⊢ Γ1 ⊑ Γ2) ->
  (ctx! t; subst_beta_ground_fun I Φ ⊢ subst_ctx_beta_ground I Γ1 ⊑ subst_ctx_beta_ground I Γ2).
Proof. intros. hnf in H|-*. intros var; specialize (H var). eauto using submty_subst_beta_ground. Qed.

Lemma eqCtx_subst_beta_ground {ϕ} t (Φ : constr (S ϕ)) (Γ1 Γ2 : ctx (S ϕ)) (I : idx ϕ) :
  (ctx! t; Φ ⊢ Γ1 ≡ Γ2) ->
  (ctx! t; subst_beta_ground_fun I Φ ⊢ subst_ctx_beta_ground I Γ1 ≡ subst_ctx_beta_ground I Γ2).
Proof. intros H. intros var; specialize (H var). eauto using eqmty_subst_beta_ground. Qed.



(** *** Stripping and exfalso *)

Definition stripCtx {ϕ} (Γ : ctx ϕ) : @PCF_Syntax.ctx (PCF_Types.ty) := fun x => mty_strip (Γ x).

Lemma stripCtx_scons {ϕ} (τ : mty ϕ) (Γ : ctx ϕ) :
  stripCtx (τ .: Γ) = (mty_strip τ .: stripCtx Γ).
Proof. unfold stripCtx. fext. intros [ |x]; cbn; reflexivity. Qed.

Lemma subCtx_strip {ϕ} (Φ : constr ϕ) (Γ Δ : ctx ϕ) (t : tm) :
  (ctx! t; Φ ⊢ Γ ⊑ Δ) ->
  forall x, free x t -> stripCtx Γ x = stripCtx Δ x.
Proof.
  intros. unfold stripCtx. hnf in H. specialize (H x H0).
  now apply submty_strip in H.
Qed.

Lemma eqCtx_strip {ϕ} (Φ : constr ϕ) (Γ Δ : ctx ϕ) (t : tm) :
  (ctx! t; Φ ⊢ Γ ≡ Δ) ->
  forall x, free x t -> stripCtx Γ x = stripCtx Δ x.
Proof. intros [H1 H2] % eqCtx_iff. eapply subCtx_strip; eauto. Qed.

Lemma subCtx_exfalso {ϕ} {t} (Φ : constr ϕ) Γ1 Γ2 :
  (sem! Φ ⊨ fun _ => False) ->
  stripCtx Γ1 = stripCtx Γ2 ->
  ctx! t; Φ ⊢ Γ1 ⊑ Γ2.
Proof.
  intros H1 H2. hnf. intros. apply submty_exfalso; eauto.
  unfold stripCtx in H2.
  change ((fun x => mty_strip (Γ1 x)) var = (fun x => mty_strip (Γ2 x)) var).
  now rewrite H2.
Qed.

Lemma eqCtx_exfalso {ϕ} {t} (Φ : constr ϕ) Γ1 Γ2 :
  (sem! Φ ⊨ fun _ => False) ->
  stripCtx Γ1 = stripCtx Γ2 ->
  ctx! t; Φ ⊢ Γ1 ≡ Γ2.
Proof. intros. split; eapply subCtx_exfalso; eauto. Qed.
