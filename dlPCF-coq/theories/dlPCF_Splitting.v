Require Import Common.
Require Import VectorBasic.
Require Import FinNotations.
Require Import PCF_Syntax.
Require Import dlPCF_iSubst.
Require Import ForestCard dlPCF_Types dlPCF_Sum dlPCF_Contexts dlPCF_Typing.
Require PCF_Types.

From Coq Require Import Vector Fin.
Import FinNumNotations VectorNotations2.
Import VectFunctionNotations.
Open Scope vector_scope.

From Coq Require Import Vector Fin.
From Coq Require Import Lia.

Open Scope PCF.


Implicit Types (ϕ : nat).


(** ** dlPCF Splitting Lemmas **)

(** The splitting lemmas are important in the proof of subject reduction. *)



(** *** Splitting lemma for binary sums *)


Lemma splitting_Lam {ϕ} (Φ : constr ϕ) Γ M t τ1 τ2 τ :
  msum τ1 τ2 τ ->
  (ty! Φ; Γ ⊢(M) Lam t : τ) ->
  exists (N1 N2 : idx ϕ) (Γ1 Γ2 : ctx ϕ),
    (ty! Φ; Γ1 ⊢(N1) Lam t : τ1) /\ (ty! Φ; Γ2 ⊢(N2) Lam t : τ2) /\
    (sem! Φ ⊨ fun xs => N1 xs + N2 xs <= M xs) /\
    (exists Γ12 : ctx ϕ, ctxMSum (Lam t) Γ1 Γ2 Γ12 /\ (ctx! (Lam t); Φ ⊢ Γ ⊑ Γ12)).
Proof.
  intros Hmsum Hty.
  apply ty_Lam_inv in Hty as (I & Δa & Δ & Ka & τa & σa & HΔ & Hτ & Hty & HΓ & HM).
  apply submty_Quant_inv' in Hτ as (j&Aj&Hτ&Hi&->).
  apply msum_Quant_inv in Hmsum as (i1&i2&->&->&Hmsum).

  (* Decomposition of the the bounded context sum [Γ] *)
  assert (j = fun xs => i1 xs + i2 xs) as -> by (fext; auto); clear Hmsum.
  pose proof ctxBSum_monotone' (I2 := fun xs => i1 xs + i2 xs) HΔ as (Δ'&HΔ'&_&L).
  assert (ctx! (Lam t); Φ ⊢ Δ ⊑ Δ') as HΔΔ'.
  { eapply subCtx_mono_Φ; eassumption. }
  clear L.
  pose proof ctxBSum_split HΔ' as (Σ1&Σ2&(HΣ1&HΣ2)&HΣ12).

  (* The weights of the two typings *)
  set (N1 := fun xs => i1 xs + Σ_{a<i1 xs} Ka (a         ::: xs)).
  set (N2 := fun xs => i2 xs + Σ_{a<i2 xs} Ka (i1 xs + a ::: xs)).

  exists N1. exists N2. eexists Σ1. eexists Σ2. repeat_split.
  { eapply ty_sub'. 3: hnf; reflexivity.
    - eapply ty_Lam'; eauto.
      eapply ty_weaken_Φ; eauto.
      hnf; intros xs (Hxs1&Hxs2); split; auto.
      hnf in Hτ. specialize (Hi (tl xs) Hxs2); cbn in *. nia.
    - apply submty_Quant.
      + eapply sublty_mono_Φ; eauto.
        hnf; intros xs (Hxs1&Hxs2); split; auto. nia.
      + hnf; reflexivity.
  }
  {
    eapply ty_sub'. 3: hnf; reflexivity.
    - eapply ty_Lam. 3,5:reflexivity.
      2: apply HΣ2.
      1:{ eapply ty_sub.
          - eapply ty_weaken_Φ.
            + eapply (ty_shift_add (ϕ := _) (t := t) (Γ := _ .: _)). apply Hty.
            + cbn. hnf. intros xs (Hxs1&Hxs2); split; eauto.
              instantiate (1 := i1). hnf in Hi. specialize (Hi (tl xs) Hxs2). cbn. nia.
          - rewrite ctx_shift_add_scons. reflexivity.
          - reflexivity.
          - cbn. hnf. intros xs (Hxs1&Hxs2).
            instantiate (1 := fun xs => Ka (i1 (tl xs) + hd xs ::: tl xs)). reflexivity.
      }
      1:{ hnf; reflexivity. }
    - apply submty_Quant.
      + eapply sublty_mono_Φ; eauto.
        * change (mty_shift_add τa i1 ⊸ mty_shift_add σa i1) with (lty_shift_add (τa ⊸ σa) i1).
          apply sublty_shift_add; eauto.
        * hnf; intros xs (Hxs1&Hx2); split; auto. cbn. nia.
      + hnf; reflexivity.
  }
  { unfold N1, N2. intros xs Hxs. hnf in HM. specialize (HM xs Hxs). erewrite <- HM. hnf in Hi. specialize (Hi xs Hxs).
    enough ((Σ_{a < i1 xs} Ka (a ::: xs)) + (Σ_{a < i2 xs} Ka (i1 xs + a ::: xs)) <= (Σ_{a < I xs} Ka (a ::: xs))) by nia.
    rewrite <- (@Sum_monotone (i1 xs + i2 xs) (I xs)) by nia.
    rewrite Sum_idx_plus. nia.
  }
  {
    eexists; split.
    - eauto.
    - etransitivity; eauto.
  }
Qed.


(* TODO: Move *)
Lemma split_idx {ϕ} (I J : idx ϕ) :
  (forall xs, I xs <= J xs) ->
  { K : idx ϕ | forall xs, J xs = I xs + K xs }.
Proof.
  intros H1. exists (fun xs => proj1_sig (le_sig_plus (H1 xs))). intros xs.
  eapply proj2_sig; eauto.
Defined.


(** Create an index under a semantical assumption *)
Definition create_idx_sem {ϕ} {Φ : constr ϕ} (dec: forall xs, { Φ xs } + { ~ Φ xs }) (f : forall xs, Φ xs -> nat) (def : idx ϕ) : idx ϕ :=
  fun xs => match dec xs with
         | left p => f xs p
         | _ => def xs
         end.

Lemma idx_forestCard_split {ϕ} (Φ : constr ϕ) (I1 I2 : idx ϕ) (K : idx (S ϕ)) (H : idx ϕ) :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  (sem! Φ ⊨ fun xs => isForestCard (fun a => K (a ::: xs)) (I1 xs + I2 xs) (H xs)) ->
  { H1 & {H2 |
           (sem! Φ ⊨ fun xs => isForestCard (fun a => K (a ::: xs)) (I1 xs) (H1 xs)) /\
           (sem! Φ ⊨ fun xs => isForestCard (fun a => K (H1 xs + a ::: xs)) (I2 xs) (H2 xs)) /\
           (forall xs, H xs = H1 xs + H2 xs) } }.
Proof.
  intros dec Hsem.
  unshelve eexists (create_idx_sem dec _ (iConst 0)).
  { intros xs Hxs. pose proof isForestCard_sig (Hsem xs Hxs) as (fuel&Hforest).
    exact (projT1 (forestCard_split _ _ _ _ Hforest)). }
  unshelve eexists (create_idx_sem dec _ H).
  { intros xs Hxs. pose proof isForestCard_sig (Hsem xs Hxs) as (fuel&Hforest).
    exact (proj1_sig (projT2 (forestCard_split _ _ _ _ Hforest))). }
  cbn. repeat_split. (* The same script three times *)
  - hnf; intros xs Hxs. unfold create_idx_sem. destruct (dec xs); try tauto.
    destruct (isForestCard_sig (Hsem xs φ)) as (fuel&Hforest).
    destruct (forestCard_split (I1 xs) (I2 xs) (fun a : nat => K (a ::: xs)) fuel Hforest) as (H1&H2&HH1&HH2&->); cbn.
    hnf. eauto.
  - hnf; intros xs Hxs. unfold create_idx_sem. destruct (dec xs); try tauto.
    destruct (isForestCard_sig (Hsem xs φ)) as (fuel&Hforest).
    destruct (forestCard_split (I1 xs) (I2 xs) (fun a : nat => K (a ::: xs)) fuel Hforest) as (H1&H2&HH1&HH2&->); cbn.
    hnf. eauto.
  - intros xs. unfold create_idx_sem. destruct (dec xs); cbn; auto.
    destruct (isForestCard_sig (Hsem xs φ)) as (fuel&Hforest).
    destruct (forestCard_split (I1 xs) (I2 xs) (fun a : nat => K (a ::: xs)) fuel Hforest) as (H1&H2&HH1&HH2&->); cbn.
    auto.
Qed.


Lemma idx_split_le {ϕ} (Φ : constr ϕ) (I J : idx ϕ) :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  (sem! Φ ⊨ fun xs => I xs <= J xs) ->
  { K : idx ϕ | sem! Φ ⊨ fun xs => J xs = I xs + K xs }.
Proof.
  intros dec Hle.
  exists (create_idx_sem dec (fun xs H => proj1_sig (le_sig_plus (Hle xs H))) (iConst 0)); unfold create_idx_sem.
  hnf; intros xs Hxs. destruct (dec xs); try tauto.
  eapply proj2_sig; eauto.
Qed.

Lemma idx_forestCard_split_le {ϕ} (Φ : constr ϕ) (I1 I2 : idx ϕ) (K : idx (S ϕ)) (H : idx ϕ) :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  (sem! Φ ⊨ fun xs => isForestCard (fun a => K (a ::: xs)) (I2 xs) (H xs)) ->
  (sem! Φ ⊨ fun xs => I1 xs <= I2 xs) ->
  { H1 & {H2 |
           (sem! Φ ⊨ fun xs => isForestCard (fun a => K (a ::: xs)) (I1 xs) (H1 xs)) /\
           (sem! Φ ⊨ fun xs => isForestCard (fun a => K (H1 xs + a ::: xs)) (I2 xs - I1 xs) (H2 xs)) /\
           (forall xs, H xs = H1 xs + H2 xs) } }.
Proof.
  intros dec Hfor Hle.
  pose proof idx_split_le _ _ dec Hle as (I3&HI3).
  assert (sem! Φ ⊨ (fun xs : Vector.t nat ϕ => isForestCard (fun a : nat => K (a ::: xs)) (I1 xs + I3 xs) (H xs))) as Hfor'.
  { intros xs Hxs. rewrite <- HI3 by assumption. auto. }
  pose proof idx_forestCard_split _ _ _ _ dec Hfor' as (H1&H2&L1&L2&L3).
  exists H1, H2. repeat_split; auto.
  intros xs Hxs. rewrite HI3 by auto. replace (I1 xs + I3 xs - I1 xs) with (I3 xs) by lia. auto.
Qed.


(** Similar forest cardinality splitting lemmas, where we already know the first cardinality *)

Lemma isForestCard_split2 (K : nat -> nat) (I1 I2 : nat) (H H1 : nat) :
  isForestCard K (I1 + I2) H ->
  isForestCard K I1 H1 ->
  {H2 : nat | isForestCard (fun a : nat => K (H1 + a)) I2 H2 /\ H = H1 + H2}.
Proof.
  intros HH1 HH2. pose proof isForestCard_split _ _ HH1 as (H1'&H2&HL1&L2&->).
  pose proof isForestCard_functional HH2 HL1 as ->. eauto.
Qed.


(* It is not possible to drop the "sem!" here in the last line, like in [idx_forestCard_split]. *)
Lemma idx_forestCard_split2 {ϕ} (Φ : constr ϕ) (I1 I2 : idx ϕ) (K : idx (S ϕ)) (H H1 : idx ϕ) :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  (sem! Φ ⊨ fun xs => isForestCard (fun a => K (a ::: xs)) (I1 xs + I2 xs) (H xs)) ->
  (sem! Φ ⊨ fun xs => isForestCard (fun a => K (a ::: xs)) (I1 xs) (H1 xs)) ->
  {H2 | (sem! Φ ⊨ fun xs => isForestCard (fun a => K (H1 xs + a ::: xs)) (I2 xs) (H2 xs)) /\
        (sem! Φ ⊨ fun xs => H xs = H1 xs + H2 xs) }.
Proof.
  intros dec Hsem1 Hsem2.
  unshelve eexists (create_idx_sem dec _ (iConst 0)).
  { intros xs Hxs. pose proof isForestCard_split2 _ (Hsem1 xs Hxs) (Hsem2 xs Hxs) as (H2 & (fuel&L1) % isForestCard_sig & L2). apply H2. }
  cbn. repeat_split.
  - hnf; intros xs Hxs. unfold create_idx_sem. destruct (dec xs); try tauto.
    destruct (isForestCard_split2 (I2 xs) (Hsem1 xs φ) (Hsem2 xs φ)) as (H2 & L2 & ->).
    destruct (isForestCard_sig L2) as (fuel&Hforest). hnf. eauto.
  - intros xs. unfold create_idx_sem. destruct (dec xs); cbn; auto.
    destruct (isForestCard_split2 (I2 xs) (Hsem1 xs φ) (Hsem2 xs φ)) as (H2 & L2 & ->); cbn; auto.
    + destruct (isForestCard_sig L2) as (fuel&Hforest). reflexivity.
    + tauto.
Qed.



(* TODO: Move *)
Lemma conj_dec (P Q : Prop) :
  { P } + { ~ P } ->
  { Q } + { ~ Q } ->
  { P /\ Q } + { ~ (P /\ Q) }.
Proof. tauto. Qed.


Lemma splitting_Fix {ϕ} (Φ : constr ϕ) Γ M t τ1 τ2 ρ :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  msum τ1 τ2 ρ ->
  (ty! Φ; Γ ⊢(M) Fix t : ρ) ->
  exists (N1 N2 : idx ϕ) (Γ1 Γ2 : ctx ϕ),
    (ty! Φ; Γ1 ⊢(N1) Fix t : τ1) /\ (ty! Φ; Γ2 ⊢(N2) Fix t : τ2) /\
    (sem! Φ ⊨ fun xs => N1 xs + N2 xs <= M xs) /\
    (exists Γ12 : ctx ϕ, ctxMSum (Fix t) Γ1 Γ2 Γ12 /\ (ctx! (Fix t); Φ ⊢ Γ ⊑ Γ12)).
Proof.
  intros dec Hsum Hty.
  inv Hty; cbn in *.

  apply submty_Quant_inv' in Hρ as (K'&C&HC&HK&->).
  apply msum_Quant_inv in Hsum as (K1&K2&->&->&Hmsum).
  assert (K' = fun xs => K1 xs + K2 xs) as -> by (fext; auto); clear Hmsum.

  (* Decomposition of the forest cardinality [cardH] into [cardH1], [cardH2], and [cardH3] *)
  pose proof idx_forestCard_split_le _ _ _ _ dec HcardH HK as (cardH1'&cardH3&HcardH1'&HcardH3&HcardH13).
  epose proof idx_forestCard_split _ _ _ _ dec HcardH1' as (cardH1&cardH2&HcardH1&HcardH2&HcardH12').

  (* The weights of the two typings *)
  pose (N1 := fun xs => Σ_{b < cardH1 xs} Jb (b ::: xs)).
  pose (N2 := fun xs => Σ_{b < cardH2 xs} Jb (cardH1 xs + b ::: xs)).

  (* Decomposition of the the bounded context sum [Γ] *)
  pose proof ctxBSum_monotone' (I2 := fun xs => cardH1 xs + cardH2 xs) Hbsum as (Γ1'&Hbsum1'&_&L).
  assert (ctx! Fix t; Φ ⊢ Γ' ⊑ Γ1') as HΓ1'.
  { eapply subCtx_mono_Φ; eauto.
    hnf in HcardH13,HcardH12'|-*. intros xs Hxs. rewrite HcardH13, HcardH12' by auto. lia. }
  clear L.
  pose proof ctxBSum_split Hbsum1' as (Γ1&Γ2&(HΓ1&HΓ2)&HΓ12).

  (* So far, so good... And now for the fun part! *)
  exists N1, N2, Γ1, Γ2. repeat_split.
  {
    eapply ty_Fix with (K0 := K1).
    all: cbn zeta.
    6: apply HΓ1.
    6: reflexivity.
    - eapply ty_weaken_Φ; eauto.
      hnf; intros xs (Hxs1&Hxs2); split; auto.
      rewrite HcardH13 by auto. rewrite HcardH12' by auto. lia.
    - cbn. eapply sublty_mono_Φ; eauto.
      hnf; intros xs (Hxs1&Hxs2&Hxs3); repeat_split; auto.
      eapply Nat.lt_le_trans; eauto.
      rewrite HcardH13 by auto. rewrite HcardH12' by auto. lia.
    - eassumption.
    - hnf; intros xs (Hxs1&Hxs2&Hxs3). apply Hcard1; repeat_split; auto.
      eapply Nat.lt_le_trans; eauto.
      rewrite HcardH13 by auto. rewrite HcardH12' by auto. lia.
    - eauto. intros xs (Hxs1&Hxs2). apply Hcard2; split; auto.
      eapply Nat.lt_le_trans; eauto.
      specialize (HK (tl xs) Hxs2). rewrite <- HK. lia.
    - hnf; reflexivity.
    - cbn. apply submty_Quant. 2: hnf; reflexivity.
      eapply sublty_mono_Φ; eauto. hnf; intros xs (Hxs1&Hxs2); split; auto. lia.
  }
  {
    (* Shifting (b := H1 + b) of the first subtyping [a < I, b < H ⊢ B{0/a}{\fc_b^{b+1,a}I + b + 1 / b} ⊑ A] *)
    assert
      (lty! fun xs : Vector.t nat (S (S ϕ)) => hd xs < Ib (cardH1 (tl (tl xs)) + hd (tl xs) ::: tl (tl xs)) /\ hd (tl xs) < cardH2 (tl (tl xs)) /\
            Φ (tl (tl xs)) ⊢
      subst_lty (lty_shift_one_add B (fun xs => cardH1 xs)) (* Variable b is [Fin1] in [B] *)
                (fun f xs => f (0 ::: S (card1 (hd xs ::: cardH1 (tl (tl xs)) + hd (tl xs) ::: tl (tl xs)) + hd (tl xs)) ::: tl (tl xs))) ⊑
      lty_shift_one_add A cardH1) as Hsub_shift.
    {
      pose proof sublty_shift_one_add cardH1 Hsub as L1. cbn in L1.
      setoid_rewrite subst_lty_twice in L1; unfold funcomp in L1; unfold shift_add_fun in L1; cbn in L1.
      eapply sublty_mono_Φ.
      { rewrite <- L1. apply sublty_congr. setoid_rewrite subst_lty_twice; unfold shift_one_add_fun, shift_add_fun, funcomp; cbn.
        f_equal. fext; intros x xs. repeat f_equal. lia. }
      { hnf; intros xs (Hxs1&Hxs2&Hxs3). repeat_split; auto.
        enough (cardH1 (tl (tl xs)) + cardH2 (tl (tl xs)) <= cardH (tl (tl xs))) by lia.
        rewrite HcardH13 by auto. rewrite HcardH12' by auto. lia.
      }
    }

    (* We need to apply the forest spltitting lemma in order to simplify [\fc_b^{0,a+K1}I] to [H1 + \fc_b^{0,a} I(b := H1 + b)] *)
    assert ({card2' : idx (S ϕ) |
              (sem! fun xs => let a := hd xs in
                           let xs' := tl xs in
                           a < K2 xs' /\ Φ xs'
                  ⊨ fun xs => let a := hd xs in
                           let xs' := tl xs in
                           isForestCard (fun b => Ib (cardH1 (tl xs) + b ::: xs')) a (card2' xs)) /\
              (* (forall xs, cardH1 (tl xs) + card2' xs = card2 (K1 (tl xs) + hd xs ::: tl xs)) *)
              (sem! fun xs => let a := hd xs in
                           let xs' := tl xs in
                           a < K2 xs' /\ Φ xs'
                  ⊨ fun xs => cardH1 (tl xs) + card2' xs = card2 (K1 (tl xs) + hd xs ::: tl xs))
            }) as (card2' & Hcard2' & Hcard2'_eq).
    {
      (* Shift the second cardinality by K1 *)
      assert(sem! fun xs : Vector.t nat (S ϕ) =>
                    let a := hd xs in
                    let xs' := tl xs in
                    a < K2 xs' /\ Φ xs' ⊨
                  (fun xs : Vector.t nat (S ϕ) =>
                    let a := hd xs in
                    let xs' := tl xs in
                    isForestCard (fun b : nat => Ib (b ::: xs')) (K1 xs' + a) (card2 (K1 (tl xs) + hd xs ::: tl xs)))) as L1.
      { hnf. intros xs (Hxs1&Hxs2). cbn zeta.
        apply (Hcard2 (K1 (tl xs) + hd xs ::: tl xs)); cbn. repeat_split; auto.
        enough (K1 (tl xs) + K2 (tl xs) <= K (tl xs)) by lia. auto. }
      cbn zeta in L1|-*.

      (* And now split it! *)
      unshelve epose proof idx_forestCard_split2 (fun xs => K1 (tl xs)) (fun xs => hd xs) (fun xs => Ib (hd xs ::: tl (tl xs)))
               (fun xs => card2 (K1 (tl xs) + hd xs ::: tl xs)) (fun xs => cardH1 (tl xs)) _ L1 as (card2'&L2&L3).
      { cbn. intros. apply conj_dec; auto. apply lt_dec. }
      { firstorder. }
      cbn in L2, L3.

      exists card2'. repeat_split.
      - clear L1. hnf in L3|-*. intros xs (Hxs1&Hxs2).
        specialize (L3 xs (conj Hxs1 Hxs2)). eauto.
      - clear L2. intros xs (Hxs1&Hxs2).
        specialize (L3 xs).
        erewrite isForestCard_functional; eauto. rewrite <- L3; eauto.
    }
    cbn zeta in Hcard2'.

    (* Shifting (a := K1 + a) of [a < K ⊢ B{0/a}{\fc_b^{0,a} / b} ⊑ C] *)
    assert
    (lty! fun xs : Vector.t nat (S ϕ) => hd xs < K2 (tl xs) /\ Φ (tl xs) ⊢
       subst_lty (lty_shift_one_add B cardH1)
                 (fun f xs => f (0 ::: card2' xs ::: tl xs)) ⊑
       lty_shift_add C K1) as HC_shift.
    {
      pose proof sublty_shift_add K1 HC as L1. cbn in L1.
      setoid_rewrite subst_lty_twice in L1; unfold funcomp in L1; unfold shift_add_fun in L1; cbn in L1.
      (* Replace the cardinality [cardH1 ...] with [cardH1 (tl xs) + card2' xs] inside the substitution *)
      assert (lty! fun xs : Vector.t nat (S ϕ) => K1 (tl xs) + hd xs < K1 (tl xs) + K2 (tl xs) /\ Φ (tl xs) ⊢
        subst_lty B (fun (x : idx (S (S ϕ))) (xs : Vector.t nat (S ϕ)) => x (0 ::: cardH1 (tl xs) + card2' xs ::: tl xs)) ⊑
        lty_shift_add C K1) as L2.
      {
        rewrite <- L1. apply sublty_subst_idx_sem. hnf in Hcard2'_eq|-*. intros xs (Hxs1&Hxs2).
        intros j. repeat f_equal. apply Hcard2'_eq; cbn; split; auto. lia.
      }

      eapply sublty_mono_Φ.
      { rewrite <- L2. apply sublty_congr.
        setoid_rewrite subst_lty_twice; unfold shift_one_add_fun, shift_add_fun, funcomp; cbn.
        reflexivity. }
      { hnf; intros xs (Hxs1&Hxs2). repeat_split; auto. lia. }
    }

    (* The two cardinality indexes shift b by [cardH1] *)
    eapply ty_Fix with (Ib0 := shift_add_fun cardH1 Ib) (K0 := K2)
                       (Jb0 := shift_add_fun cardH1 Jb)
                       (cardH0 := cardH2) (* H := H2 *)
                       (card3 := _)
                       (card4 := card2')
                       (A0 := lty_shift_one_add A cardH1)
                       (B0 := lty_shift_one_add B cardH1).
    all: cbn zeta.
    6: (* context sum *) apply HΓ2.
    6: (* [Γ2 ⊑ Γ2] *) reflexivity.
    4:{ (* first cardinality *) intros xs (Hxs1&Hxs2&Hxs3).
        unfold shift_add_fun; cbn.
        replace (fun c : nat => Ib (cardH1 (tl (tl xs)) + S (hd (tl xs) + c) ::: tl (tl xs))) with
            (fun c : nat => Ib (S ((cardH1 (tl (tl xs)) + hd (tl xs)) + c) ::: tl (tl xs))).
        2:{ fext. intros. repeat f_equal. lia. }
        hnf in Hcard1. apply (Hcard1 (hd xs ::: cardH1 (tl (tl xs)) + hd (tl xs) ::: tl (tl xs))). cbn. repeat_split; auto.
        rewrite HcardH13 by auto. rewrite HcardH12' by auto. lia. }
    4: (* second cardinality *) apply Hcard2'.
    2: (* first subyting shifting *) apply Hsub_shift.
    4: { (* second subtyping shift *) eapply submty_Quant. 2: hnf; reflexivity. apply HC_shift. }

    - (* We need to shift the typing by [cardH1] *)
      eapply ty_sub. eapply ty_weaken_Φ.
      { eapply ty_shift_add with (I := cardH1). apply Hty0. }
      { unfold shift_add_fun; cbn. hnf; intros xs (Hxs1&Hxs2); split; auto.
        enough (cardH1 (tl xs) + cardH2 (tl xs) <= cardH (tl xs)) by lia.
        rewrite HcardH13 by auto. rewrite HcardH12' by auto. lia. }
      { rewrite ctx_shift_add_scons. eapply subCtx_scons; reflexivity. }
      { reflexivity. }
      { hnf; reflexivity. }

    - hnf. intros xs Hxs. unfold shift_add_fun; cbn. now apply HcardH2.
    - hnf; reflexivity.
  }

  {
    hnf in HM,HcardH12',HcardH13|-*. subst N1 N2. intros xs Hxs. cbn.
    rewrite <- HM, HcardH13, HcardH12' by auto.
    rewrite !Sum_idx_plus. lia.
  }
  {
    eexists; split.
    - eauto.
    - etransitivity; eauto.
  }
Qed.


Definition def_ctx {ϕ} : ctx ϕ.
Proof. now repeat constructor. Qed.

Lemma subCtx_def {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) t :
  closed t ->
  ctx! t; Φ ⊢ Γ ⊑ def_ctx.
Proof. intros Hclos. hnf. intros. now apply free_closed_iff in H; eauto. Qed.

Lemma ctxMSum_def {ϕ} Γ1 Γ2 Δ t :
  closed t ->
  ctxMSum (ϕ := ϕ) t Γ1 Γ2 Δ.
Proof. hnf. intros. hnf. intros. now apply free_closed_iff in H0. Qed.

Lemma ctxBSum_def {ϕ} (I : idx ϕ) Γ Δ t :
  closed t ->
  ctxBSum (ϕ := ϕ) t I Γ Δ.
Proof. hnf. intros. hnf. intros. now apply free_closed_iff in H0. Qed.

(* In order to choose an arbitrary context here, we need to assume that [m <= 0].
It is also possible to construct a suiting context ([neutralCtx]), but this hack doesn't work for [ctxBSum]. *)
Lemma splitting_Const {ϕ} (Φ : constr ϕ) (Γ : ctx ϕ) M k τ1 τ2 τ :
  msum τ1 τ2 τ ->
  (ty! Φ; Γ ⊢(M) Const k : τ) ->
  exists (N1 N2 : idx ϕ) (Γ1 Γ2 : ctx ϕ),
    (ty! Φ; Γ1 ⊢(N1) Const k : τ1) /\ (ty! Φ; Γ2 ⊢(N2) Const k : τ2) /\
    (sem! Φ ⊨ fun xs => N1 xs + N2 xs <= M xs) /\
    (exists Γ12 : ctx ϕ, ctxMSum (Const k) Γ1 Γ2 Γ12 /\ (ctx! (Const k); Φ ⊢ Γ ⊑ Γ12)).
Proof.
  intros Hmsum Hty.
  apply ty_Const_inv in Hty. apply submty_Nat_inv' in Hty as (k'&->&Hty).
  destruct τ1, τ2; cbn in *; try tauto. destruct Hmsum as (<-&<-).
  exists (iConst 0), (iConst 0), Γ, def_ctx. repeat_split.
  - eapply ty_sub'.
    + apply ty_Const'.
    + eapply submty_Nat. firstorder.
    + hnf; reflexivity.
  - eapply ty_sub'.
    + apply ty_Const'.
    + eapply submty_Nat. firstorder.
    + hnf; reflexivity.
  - hnf; intros; unfold iConst; cbn; omega.
  - eexists. split.
    + now apply ctxMSum_def.
    + now apply subCtx_def.
Qed.


Lemma splitting {ϕ} (Φ : constr ϕ) Γ M v τ τ1 τ2 :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  val v ->
  msum τ1 τ2 τ ->
  (ty! Φ; Γ ⊢(M) v : τ) ->
  exists (N1 N2 : idx ϕ) (Γ1 Γ2 : ctx ϕ),
    (ty! Φ; Γ1 ⊢(N1) v : τ1) /\ (ty! Φ; Γ2 ⊢(N2) v : τ2) /\
    (sem! Φ ⊨ fun xs => N1 xs + N2 xs <= M xs) /\
    (exists Γ12 : ctx ϕ, ctxMSum v Γ1 Γ2 Γ12 /\ (ctx! v; Φ ⊢ Γ ⊑ Γ12)).
Proof.
  intros dec Hval Hmsum Hty. destruct Hval; cbn [maxVar] in *.
  - eapply splitting_Lam; eauto.
  - eapply splitting_Fix; eauto.
  - eapply splitting_Const; eauto.
Qed.


(** *** "Parametric" splitting lemma *)


Lemma psplitting_Const {ϕ} (Φ : constr ϕ) Γ M (k : nat) τ (I : idx ϕ) (σi : mty (S ϕ)) :
  bsum I σi τ ->
  (ty! Φ; Γ ⊢(M) Const k : τ) ->
  exists (Ni : idx (S ϕ)) (Γi : ctx (S ϕ)),
    (ty! fun xs => hd xs < I (tl xs) /\ Φ (tl xs); Γi ⊢(Ni) Const k : σi) /\
    (sem! Φ ⊨ fun xs => (Σ_{i < I xs} Ni (i ::: xs)) <= M xs) /\
    (exists (Γ_sum : ctx ϕ) (_ : ctxBSum (Const k) I Γi Γ_sum), (ctx! (Const k); Φ ⊢ Γ ⊑ Γ_sum)).
Proof.
  intros Hbsum Hty. apply ty_Const_inv in Hty. apply submty_Nat_inv' in Hty as (k'&->&Hty). unfold iConst in Hty.
  destruct Hbsum as [Hbsum|Hbsum].
  2:{ exfalso. now apply bsum_Quant_not_Nat in Hbsum. }
  hnf in Hbsum. destruct Hbsum as (J&->&[= ->]).
  change (subst_mty (Nat J) (fun (f : idx ϕ) (xs : Vector.t nat (S ϕ)) => f (tl xs))) with (Nat (fun xs => J (tl xs))) in *.

  eexists _, _. repeat_split.
  - apply ty_Const. apply submty_Nat; cbn. hnf; intros xs (Hxs1&Hxs2). unfold iConst. now rewrite Hty.
  - instantiate (1 := fun xs => 0); cbn. hnf; intros xs Hxs. rewrite Sum_constO. lia.
  - eexists. unshelve eexists.
    + now apply ctxBSum_def.
    + now apply subCtx_def.
  Unshelve. exact def_ctx.
Qed.


(* TODO TODO: Move away all this boilerplate code *)

(* TODO: Move *)
Definition subst_beta_two_fun {X : Type} {ϕ} (i : idx (S (S ϕ))) : (Vector.t nat (S ϕ) -> X) -> (Vector.t nat (S (S ϕ)) -> X)  :=
  fun (f : Vector.t nat (S ϕ) -> X) (xs : Vector.t nat (S (S ϕ))) => f (i xs ::: tl (tl xs)).

(* TODO: Move *)
Goal forall ϕ τ i, @subst_mty_beta_two ϕ τ i = subst_mty τ (subst_beta_two_fun i).
Proof. reflexivity. Qed.

(* TODO: Move *)
Lemma subst_beta_two_fun_correct {X : Type} {ϕ} (i : idx (S (S ϕ))) :
  subst_beta_two_fun (X := X) i = subst_beta_fun 2 i.
Proof. reflexivity. Qed.

(* TODO: Move *)
Lemma subst_beta_two_fun_correct' {X : Type} {ϕ} (i : idx (S (S ϕ))) :
  @subst_beta_two_fun X ϕ i = subst_var_beta_fun Fin0 2 i.
Proof. rewrite subst_beta_two_fun_correct. now rewrite subst_var_beta_fun_Fin0. Qed.

(* TODO: Move *)
Definition subst_ctx_beta_two {ϕ} (i : idx (S (S ϕ))) (Γ : ctx (S ϕ)) :=
  fun var => subst_mty_beta_two (Γ var) i.

(* TODO: Move *)
Lemma ty_subst_beta_two {ϕ} (Φ : constr (S ϕ)) (Γ : ctx (S ϕ)) (K : idx (S ϕ)) (t : tm) (τ : mty (S ϕ)) (i : idx (S (S ϕ))) :
  (ty! Φ; Γ ⊢(K) t : τ) ->
  ty! subst_beta_two_fun i Φ; subst_ctx_beta_two i Γ ⊢(subst_beta_two_fun i K)
      t : subst_mty_beta_two τ i.
Proof.
  intros.
  change (subst_ctx_beta_two i Γ) with (subst_ctx Γ (subst_beta_two_fun i)).
  rewrite !subst_beta_two_fun_correct', subst_mty_beta_two_correct'.
  now apply ty_subst_var_beta with (x := Fin0) (y := 2).
Qed.

(* TODO: Move Move *)
Lemma subst_ctx_scons {ϕ1 ϕ2} (τ : mty ϕ1) (Γ : ctx ϕ1) (f : idx ϕ1 -> idx ϕ2) :
  (subst_ctx (τ .: Γ) f) = ((subst_mty τ f) .: (subst_ctx Γ f)).
Proof. fext; intros [ | ]; reflexivity. Qed.

(* TODO: Move Move Move *)
Lemma subst_ctx_beta_two_scons {ϕ} (i : idx (S (S ϕ))) (τ : mty (S ϕ)) (Γ : ctx (S ϕ)) :
  (subst_ctx_beta_two i (τ .: Γ)) = ((subst_mty_beta_two τ i) .: (subst_ctx_beta_two i Γ)).
Proof. fext; intros [ | ]; reflexivity. Qed.


(* TODO: Move Move Move *)
Lemma ctxBSum_subst_beta_two {ϕ} (t : tm) (i : idx (S (S ϕ))) (I : idx (S ϕ))
      (Γ : ctx (S (S ϕ))) (Δ : ctx (S ϕ)) :
  ctxBSum t I Γ Δ ->
  ctxBSum t (subst_beta_two_fun i I) (subst_ctx Γ (fun f xs => f (hd xs ::: i (tl xs) ::: tl (tl (tl xs))))) (subst_ctx_beta_two i Δ).
Proof.
  intros.
  change (subst_ctx_beta_two i Δ) with (subst_ctx Δ (subst_beta_two_fun i)).
  rewrite !subst_beta_two_fun_correct'.
  replace (subst_ctx Γ (fun (f : idx (S (S ϕ))) (xs : Vector.t nat (S (S (S ϕ)))) => f (hd xs ::: i (tl xs) ::: tl (tl (tl xs)))))
    with (subst_ctx Γ (subst_var_beta_fun Fin1 2 i)).
  2:{ unfold subst_ctx; f_equal. fext; intros var.
      f_equal. fext; intros f xs. erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0. cbn.
      unfold subst_beta_fun. now rewrite !vect_cast_id. }
  setoid_rewrite <- ctx_cast_id at 1.
  now apply ctxBSum_subst_var_beta with (x := Fin0) (y := 2) (I0 := I) (i0 := i).
  Unshelve. all: reflexivity.
Qed.

(* TODO: Move Move Move *)
Lemma sublty_subst_beta_two {ϕ} (Φ : constr (S ϕ)) (A B : lty (S ϕ)) (i : idx (S (S ϕ))) :
  (lty! Φ ⊢ A ⊑ B) ->
  (lty! subst_beta_two_fun i Φ ⊢ subst_lty_beta_two A i ⊑ subst_lty_beta_two B i).
Proof.
  intros.
  rewrite !subst_beta_two_fun_correct', !subst_lty_beta_two_correct'.
  now apply sublty_subst_var_beta with (x := Fin0) (y := 2).
Qed.

(* TODO: Move Move Move *)
Lemma submty_subst_beta_two {ϕ} (Φ : constr (S ϕ)) (σ τ : mty (S ϕ)) (i : idx (S (S ϕ))) :
  (mty! Φ ⊢ σ ⊑ τ) ->
  (mty! subst_beta_two_fun i Φ ⊢ subst_mty_beta_two σ i ⊑ subst_mty_beta_two τ i).
Proof.
  intros.
  rewrite !subst_beta_two_fun_correct', !subst_mty_beta_two_correct'.
  now apply submty_subst_var_beta with (x := Fin0) (y := 2).
Qed.


(** A sum where the count is also a sum. The inner function only depends on the outer counter. *)
Lemma Sum_idx_Sum (J : nat) (L : nat -> nat) (K : nat -> nat) :
  (Σ_{a < Σ_{d<J} L d} K a) =
  (Σ_{c<J} Σ_{a < L c} K ((Σ_{d<c} L d) + a)).
Proof.
  induction J.
  - cbn. reflexivity.
  - cbn. now rewrite <- !IHJ, !Sum_idx_plus.
Qed.


(* FIXME: Needed anywhere? *)
(** Generalisation of [bsum_monotone'] *)
Lemma bsum_monotone'' {ϕ} (I : idx ϕ) (σi : mty (S ϕ)) (τ : mty ϕ) :
  bsum I σi τ ->
  { τi : mty (S ϕ) &
         (forall i, bsum (iConst i) σi (subst_mty_beta_ground τi (iConst i))) *
         (forall i, (mty! fun xs => i <= I xs ⊢ τ ⊑ (subst_mty_beta_ground τi (iConst i))))
  } % type.
Proof.
  (* This proof is awful, but it works ;-) *)
  intros [H|H]; hnf in H.
  - destruct H as (J&->&->); cbn.
    eexists. split.
    + intros i. left; hnf; cbn. eexists. split.
      2:{ instantiate (2 := Nat _); cbn. reflexivity. }
      f_equal. unfold subst_beta_ground_fun; cbn. unfold iConst.
      instantiate (1 := fun xs => J (tl xs)). cbn. reflexivity.
    + cbn. intros. apply submty_Nat. firstorder.
  - destruct H as (J&a&->&->); cbn. eexists. split.
    + intros i. right; hnf; cbn. eexists _,_. split.
      2:{ cbn. instantiate (3 := Quant _ _); cbn.
          f_equal. unfold subst_beta_ground_fun, iConst.
          fext; intros xs.
          instantiate (2 := fun xs => Σ_{b < hd xs} (fun xs => ?[y]) (b ::: tl xs)).
          (* Manually unify the two existential variables... *)
          instantiate (1 := fun xs => ?y@{xs0:=xs; b := hd xs}).
          cbn. reflexivity.
      }
      f_equal. setoid_rewrite subst_lty_twice. unfold subst_lty_beta_two, subst_beta_ground_fun, funcomp, iConst; cbn.
      instantiate (1 := subst_lty_beta_two a (fun xs => hd xs)).
      setoid_rewrite subst_lty_twice. unfold funcomp, iConst; cbn. reflexivity.
    + intros. cbn. unfold subst_beta_ground_fun, iConst. apply submty_Quant.
      * setoid_rewrite subst_lty_twice; unfold funcomp; cbn.
        apply sublty_congr.
        setoid_rewrite <- subst_lty_id at 1.
        f_equal. fext; intros f xs. now rewrite <- eta.
      * cbn; hnf; intros xs Hxs. rewrite Sum_monotone; eauto.
Qed.


(** Things that happen to a bounded sum if the bound is a sum itself... *)
(** This is somehow analogous to [Sum_idx_Sum]. *)
Lemma bsum_Sum {ϕ} (J : idx ϕ) (L : idx (S ϕ)) (Δi : mty (S ϕ)) (Δ : mty ϕ) :
  bsum (fun xs => Σ_{a < J xs} L (a ::: xs)) Δi Δ ->
  { Σ : mty (S ϕ) &
        prod (bsum L (subst_mty_beta_two Δi (fun xs : Vector.t nat (S (S ϕ)) =>
                                         let a := hd xs in
                                         let c := hd (tl xs) in
                                         let xs' := tl (tl xs) in
                                         (Σ_{d<c} L (d ::: xs')) + a))
                   Σ)
        (bsum J Σ Δ) }.
Proof.
  intros [H|H]; hnf in H.
  - destruct H as (J'&->&->); cbn.
    eexists. eexists.
    + left; hnf. eexists; split. 2: reflexivity. cbn. instantiate (1 := fun xs => J' (tl xs)); cbn. reflexivity.
    + left; hnf. eexists; split. 2: reflexivity. cbn. reflexivity.
  - destruct H as (J'&A&->&->); cbn.
    eexists. split.
    2:{
      right; hnf; cbn. eexists _,_. split. reflexivity.
      f_equal. fext; intros xs.
      change (Σ_{a < Σ_{b < J xs} L (b ::: xs)} J' (a ::: xs)) with
          (Σ_{a < Σ_{b < J xs} (fun b => L (b ::: xs)) b} (fun a => J' (a ::: xs)) a);
        rewrite Sum_idx_Sum.
      instantiate (1 := ?[x]).
      change (Σ_{a < J xs} _ (a ::: xs)) with (Σ_{c < J xs} ?x (c ::: xs)). (* just a renaming *)
      instantiate (1 := fun xs => let c := hd xs in let xs := tl xs in Σ_{a < L (c ::: xs)} J' ((Σ_{d < c} L (d ::: xs)) + a ::: xs));
        cbn; reflexivity.
    }
    {
      right; hnf; cbn. eexists _,_. split.
      - f_equal. reflexivity.
        cbn. setoid_rewrite subst_lty_twice; unfold funcomp; cbn. f_equal.
        fext; intros f xs. instantiate (1 := ?[x]).
        rewrite Sum_idx_plus, Nat.add_assoc.
        change (Σ_{a < Σ_{b < hd (tl (tl xs))} L (b ::: tl (tl (tl xs)))} J' (a ::: tl (tl (tl xs)))) with
            (Σ_{a < Σ_{b < hd (tl (tl xs))} (fun b => L (b ::: tl (tl (tl xs)))) b} (fun a => J' (a ::: tl (tl (tl xs)))) a);
          rewrite Sum_idx_Sum.
        shelve.
      - cbn. f_equal.
        + fext; intros xs. now rewrite <- eta.
        + unfold subst_lty_beta_two; cbn. reflexivity.
      Unshelve.
      {
        cbn. f_equal. f_equal. rewrite <- !Nat.add_assoc. f_equal. rewrite Nat.add_comm. reflexivity.
      }
    }
Qed.

(** The above lemma lifted to contexts *)
Lemma ctxBSum_Sum {ϕ} t (J : idx ϕ) (L : idx (S ϕ)) (Δi : ctx (S ϕ)) (Δ : ctx ϕ) :
  ctxBSum t (fun xs => Σ_{a < J xs} L (a ::: xs)) Δi Δ ->
  { Σ : ctx (S ϕ) &
        prod (ctxBSum t L
                      (subst_ctx_beta_two (fun xs : Vector.t nat (S (S ϕ)) =>
                                             let a := hd xs in
                                             let c := hd (tl xs) in
                                             let xs' := tl (tl xs) in
                                             (Σ_{d<c} L (d ::: xs')) + a) Δi)
                   Σ)
        (ctxBSum t J Σ Δ) }.
Proof.
  intros H. hnf in H.
  exists (fun var =>
       match free_dec var t with
       | left Hlt => projT1 (bsum_Sum _ _ (H var Hlt))
       | right _ => Nat (iConst 42)
       end).
  split; cbn.
  - hnf; intros var Hvar. destruct (free_dec var t) as [Hlt|?]; cbn; try tauto.
    destruct (bsum_Sum J L (H var Hlt)) as (Σ&L1&L2); cbn; auto.
  - hnf; intros var Hvar. destruct (free_dec var t) as [Hlt|?]; cbn; try tauto.
    destruct (bsum_Sum J L (H var Hlt)) as (Σ&L1&L2); cbn; auto.
Qed.



Lemma psplitting_Lam {ϕ} (Φ : constr ϕ) Γ M (t : tm) ρ (J : idx ϕ) (σc : mty (S ϕ)) :
  bsum J σc ρ ->
  (ty! Φ; Γ ⊢(M) (Lam t) : ρ) ->
  exists (Nc : idx (S ϕ)) (Γc : ctx (S ϕ)),
    (ty! fun xs => hd xs < J (tl xs) /\ Φ (tl xs); Γc ⊢(Nc) (Lam t) : σc) /\
    (sem! Φ ⊨ fun xs => (Σ_{c < J xs} Nc (c ::: xs)) <= M xs) /\
    (exists (Γ_sum : ctx ϕ) (_ : ctxBSum (Lam t) J Γc Γ_sum), (ctx! (Lam t); Φ ⊢ Γ ⊑ Γ_sum)).
Proof.
  intros Hbsum Hty.
  apply ty_Lam_inv in Hty as (I & Δa & Δ & Ka & σa & τa & Hbsum2 & Hsub & Hty & Hctx & HM).
  apply submty_Quant_inv' in Hsub as (L&?&Hsub1&Hsub3&->). apply sublty_arr_inv' in Hsub1 as (σa'&τa'&->&Hsub1&Hsub2).

  destruct Hbsum as [Hbsum|Hbsum].
  1:{ exfalso. now apply bsum_Nat_not_Quant in Hbsum. }
  hnf in Hbsum. destruct Hbsum as (L'&A&->&[= -> <-]); rename L' into L; cbn zeta.

  (* Just a few renamings *)
  change (mty! fun xs : Vector.t nat (S ϕ) => hd xs < (Σ_{d < J (tl xs)} L (d ::: tl xs)) /\ Φ (tl xs) ⊢ τa ⊑ τa') in Hsub1;
  change (mty! fun xs : Vector.t nat (S ϕ) => hd xs < (Σ_{d < J (tl xs)} L (d ::: tl xs)) /\ Φ (tl xs) ⊢ σa' ⊑ σa) in Hsub2;
  change (sem! Φ ⊨ (fun xs : Vector.t nat ϕ => (Σ_{d < J xs} L (d ::: xs)) <= I xs)) in Hsub3.

  eapply ty_subst_beta_two with
      (i := fun xs =>
              let a := hd xs in
              let c := hd (tl xs) in
              let xs' := tl (tl xs) in
              (Σ_{d<c} L (d ::: xs')) + a)
    in Hty; unfold subst_beta_two_fun in Hty; cbn in Hty.
  rewrite subst_ctx_beta_two_scons in Hty.

  assert ({ Σ &
          { _ : ctxBSum (Lam t) (fun xs : Vector.t nat ϕ => Σ_{d < J xs} L (d ::: xs)) Δa Σ |
            ctx! Lam t; Φ ⊢ Δ ⊑ Σ }}) as (Σ & HΣ & HΣΔ).
  { pose proof ctxBSum_monotone' Hbsum2 (I2 := fun xs : Vector.t nat ϕ => Σ_{d < J xs} L (d ::: xs)) as (Σ & HΣ & _ & LL1).
    exists Σ. exists; eauto. eapply subCtx_mono_Φ; eauto. }

  pose proof ctxBSum_Sum _ _ HΣ as (Σ' & HΣ'& HΣ''). cbn in HΣ'.

  eexists _,_; repeat_split.
  {
    eapply ty_Lam with (I0 := L).
    - eapply ty_weaken_Φ. apply Hty.
      hnf; intros xs (Hxs1&Hxs2&Hxs3); split; auto.
      set (a := hd xs) in *; set (c := hd (tl xs)) in *; set (xs' := tl (tl xs)) in *; move c before a; move xs' before c.
      eapply Nat.lt_le_trans. 2: now apply Hsub3.
      eapply Nat.lt_le_trans.
      2:{ rewrite <- Sum_monotone. 2: apply Hxs2. reflexivity. }
      rewrite Sum_eq_S.
      enough (a < L (c ::: xs')) by lia.
      now subst c xs'; rewrite <- eta.
    - apply HΣ'.
    - reflexivity.
    - cbn. instantiate (1 := fun xs => _); cbn; hnf; reflexivity.
    - apply submty_Quant.
      + cbn. apply sublty_arr.
        * replace
            (subst_mty
               σa'
               (fun (f : idx (S ϕ)) (xs : Vector.t nat (S (S ϕ))) => f (hd xs + (Σ_{d < hd (tl xs)} L (d ::: tl (tl xs))) ::: tl (tl xs))))
            with (subst_mty_beta_two σa' (fun xs : Vector.t nat (S (S ϕ)) => (Σ_{d < hd (tl xs)} L (d ::: tl (tl xs))) + hd xs)).
          2:{ unfold subst_mty_beta_two. f_equal. fext; intros f xs. now rewrite Nat.add_comm. }
          eapply submty_mono_Φ. now eapply submty_subst_beta_two; eauto.
          unfold subst_beta_two_fun; cbn. hnf; intros xs (Hxs1&Hxs2&Hxs3); split; auto.
          set (a := hd xs) in *; set (c := hd (tl xs)) in *; set (xs' := tl (tl xs)) in *; move c before a; move xs' before c.
          enough ((Σ_{d < c} L (d ::: xs')) + L (tl xs) <= Σ_{d < J xs'} L (d ::: xs')) by lia.
          etransitivity.
          2:{ rewrite <- Sum_monotone. 2: eassumption. reflexivity. }
          rewrite Sum_eq_S. apply Nat.eq_le_incl. f_equal. subst c xs'; now rewrite <- eta.
        * replace (* As above, but for τ *)
            (subst_mty
               τa'
               (fun (f : idx (S ϕ)) (xs : Vector.t nat (S (S ϕ))) => f (hd xs + (Σ_{d < hd (tl xs)} L (d ::: tl (tl xs))) ::: tl (tl xs))))
            with (subst_mty_beta_two τa' (fun xs : Vector.t nat (S (S ϕ)) => (Σ_{d < hd (tl xs)} L (d ::: tl (tl xs))) + hd xs)).
          2:{ unfold subst_mty_beta_two. f_equal. fext; intros f xs. now rewrite Nat.add_comm. }
          eapply submty_mono_Φ. now eapply submty_subst_beta_two; eauto.
          unfold subst_beta_two_fun; cbn. hnf; intros xs (Hxs1&Hxs2&Hxs3); split; auto.
          set (a := hd xs) in *; set (c := hd (tl xs)) in *; set (xs' := tl (tl xs)) in *; move c before a; move xs' before c.
          enough ((Σ_{d < c} L (d ::: xs')) + L (tl xs) <= Σ_{d < J xs'} L (d ::: xs')) by lia.
          etransitivity.
          2:{ rewrite <- Sum_monotone. 2: eassumption. reflexivity. }
          rewrite Sum_eq_S. apply Nat.eq_le_incl. f_equal. subst c xs'; now rewrite <- eta.
      + hnf; reflexivity.
  }
  {
    hnf; intros xs Hxs; cbn. etransitivity. 2: now apply HM.
    rewrite Sum_plus.
    apply plus_le_compat; auto.
    etransitivity.
    2:{ rewrite <- Sum_monotone. 2: now apply Hsub3. reflexivity. }
    change (Σ_{a < Σ_{d < J xs} L (d ::: xs)} Ka (a ::: xs)) with
        (Σ_{a < Σ_{d < J xs} L (d ::: xs)} (fun a => Ka (a ::: xs)) a).
    now rewrite Sum_idx_Sum.
  }
  {
    unshelve eexists _, _; eauto.
    etransitivity; eauto.
  }
Qed.


Lemma idx_forestCard_split_Sum {ϕ} (Φ : constr ϕ) (J : idx ϕ) (L : idx (S ϕ)) (K : idx (S ϕ)) (H : idx ϕ) :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  (sem! Φ ⊨ fun xs => isForestCard (fun a => K (a ::: xs)) (Σ_{c < J xs} L (c ::: xs)) (H xs)) ->
  { H' : idx (S ϕ) |
    (sem! Φ ⊨ fun xs => (Σ_{c < J xs} H' (c ::: xs)) = H xs) /\
    (sem! fun xs => hd xs < J (tl xs) /\ Φ (tl xs) ⊨
          fun xs => isForestCard (fun a => K (a ::: tl xs)) (Σ_{d < hd xs} L (d ::: tl xs)) (Σ_{d < hd xs} H' (d ::: tl xs))) /\
    (sem! fun xs => hd xs < J (tl xs) /\ Φ (tl xs) ⊨
          fun xs => isForestCard (fun b => K ((Σ_{d < hd xs} H' (d ::: tl xs)) + b ::: tl xs)) (L xs) (H' xs))
  }.
Proof.
  intros dec Hsem.
  unshelve eexists (create_idx_sem _ _ (iConst 0)).
  3:{ intros xs Hxs.
      pose proof isForestCard_split_Sum' _ _ (Hsem (tl xs) Hxs) as (H'&HH).
      apply H'. apply (hd xs). }
  1:{ cbn. intros xs. apply dec. }
  repeat_split.
  - cbn. intros xs Hxs. unfold create_idx_sem. cbn. destruct (dec xs) as [Hdec|?]; try tauto.
    destruct isForestCard_split_Sum' as (H'&HH1&HH2&HH3); auto.
  - cbn. intros xs (Hxs1&Hxs2). unfold create_idx_sem. cbn. destruct (dec (tl xs)) as [Hdec|?]; try tauto.
    destruct isForestCard_split_Sum' as (H'&HH1&HH2&HH3); auto.
  - cbn. intros xs (Hxs1&Hxs2). unfold create_idx_sem. cbn. destruct (dec (tl xs)) as [Hdec|?]; try tauto.
    destruct isForestCard_split_Sum' as (H'&HH1&HH2&HH3); auto.
    specialize (HH3 (hd xs) Hxs1). now rewrite <- eta in HH3.
Qed.


(** We need yet another substitution. This time, we substitute [Fin1] and introduce two variables *)


(* TODO: Move Move Move *)

Definition subst_lty_beta_one {ϕ} (y : nat) (I : idx (y + ϕ)) (A : lty (S (S ϕ))) : lty (S (y + ϕ)).
Proof. eapply (subst_lty A). exact (@subst_beta_one_fun nat ϕ y I). Defined.
Definition subst_mty_beta_one {ϕ} (y : nat) (I : idx (y + ϕ)) (τ : mty (S (S ϕ))) : mty (S (y + ϕ)).
Proof. eapply (subst_mty τ). exact (@subst_beta_one_fun nat ϕ y I). Defined.

Lemma subst_lty_beta_one_correct {ϕ} (y : nat) (I : idx (y + ϕ)) (A : lty (S (S ϕ)))
      (Heq : S (y + ϕ) = y + S ϕ) :
  subst_lty_beta_one y I A = lty_cast (subst_lty_var_beta Fin1 y I A) Heq.
Proof.
  unfold subst_lty_beta_one. setoid_rewrite subst_lty_cast'.
  f_equal. unfold subst_beta_one_fun. fext; intros f xs; cbn.
  unfold idx_cast.
  erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0; cbn.
  unfold subst_beta_fun. rewrite vect_cast_inv. reflexivity.
  Unshelve. now symmetry.
Qed.

Lemma subst_mty_beta_one_correct {ϕ} (y : nat) (I : idx (y + ϕ)) (A : mty (S (S ϕ)))
      (Heq : S (y + ϕ) = y + S ϕ) :
  subst_mty_beta_one y I A = mty_cast (subst_mty_var_beta Fin1 y I A) Heq.
Proof.
  unfold subst_mty_beta_one. setoid_rewrite subst_mty_cast'.
  f_equal. unfold subst_beta_one_fun. fext; intros f xs; cbn.
  unfold idx_cast.
  erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0; cbn.
  unfold subst_beta_fun. rewrite vect_cast_inv. reflexivity.
  Unshelve. now symmetry.
Qed.

Lemma sublty_subst_beta_one {ϕ} (y : nat) (Φ : constr (S (S ϕ))) (A B : lty (S (S ϕ))) (i : idx (y + ϕ)) :
  (lty! Φ ⊢ A ⊑ B) ->
  (lty! subst_beta_one_fun y i Φ ⊢ subst_lty_beta_one y i A ⊑ subst_lty_beta_one y i B).
Proof.
  intros H. eapply sublty_mono_Φ.
  - erewrite !subst_lty_beta_one_correct.
    eapply sublty_cast.
    apply sublty_subst_var_beta with (x := Fin1) (y0 := y). eassumption.
  - clear_all. hnf; intros xs Hxs; unfold constr_cast.
    erewrite subst_var_beta_fun_FS, subst_var_beta_fun_Fin0; cbn.
    rewrite vect_cast_inv. apply Hxs.
    Unshelve. all: auto.
Qed.


Definition subst_beta_one_two_fun {X : Type} {ϕ} (i : idx (S (S ϕ))) : (Vector.t nat (S (S ϕ)) -> X) -> (Vector.t nat (S (S (S ϕ))) -> X) :=
  fun f xs => f (hd xs ::: i (tl xs) ::: tl (tl (tl xs))).

Lemma subst_beta_one_two_fun_correct {X : Type} {ϕ} :
  subst_beta_one_two_fun (X := X) (ϕ := ϕ) = subst_beta_one_fun 2.
Proof. reflexivity. Qed.

Definition subst_lty_beta_one_two {ϕ} (i : idx (S (S ϕ))) (A : lty (S (S ϕ))) : lty (S (S (S ϕ))) :=
  subst_lty A (subst_beta_one_two_fun i).

Lemma subst_lty_beta_one_two_correct {ϕ} (i : idx (S (S ϕ))) (A : lty (S (S ϕ))) :
  subst_lty_beta_one_two i A = subst_lty_beta_one 2 i A.
Proof. reflexivity. Qed.

Lemma sublty_subst_beta_one_two {ϕ} (Φ : constr (S (S ϕ))) (A B : lty (S (S ϕ))) (i : idx (S (S ϕ))) :
  (lty! Φ ⊢ A ⊑ B) ->
  (lty! subst_beta_one_two_fun i Φ ⊢ subst_lty_beta_one_two i A ⊑ subst_lty_beta_one_two i B).
Proof.
  intros H.
  rewrite subst_beta_one_two_fun_correct, !subst_lty_beta_one_two_correct.
  now apply sublty_subst_beta_one with (y := 2).
Qed.

Lemma subst_mty_beta_two_eq_Quant {ϕ} (I : idx (S ϕ)) (A : lty (S (S ϕ))) (i : idx (S (S ϕ))) :
  subst_mty_beta_two (Quant I A) i = Quant (subst_beta_two_fun i I) (subst_lty_beta_one_two i A).
Proof. reflexivity. Qed.



(* FIXME: inconsistent names *)
(* Check subst_beta_one_fun. (* x = Fin1, y = ? *) *)
(* Check subst_beta_two_fun. (* x = Fin0, y = 2; proposed name: subst_beta_zero_two *) *)
(* (This is the one used in [bsum_Quant]) *)
(* In general: subst_beta_x_y_fun *)



Lemma psplitting_Fix {ϕ} (Φ : constr ϕ) Γ M (t : tm) ρ (J : idx ϕ) (σi : mty (S ϕ)) :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  bsum J σi ρ ->
  (ty! Φ; Γ ⊢(M) (Fix t) : ρ) ->
  exists (Ni : idx (S ϕ)) (Γi : ctx (S ϕ)),
    (ty! fun xs => hd xs < J (tl xs) /\ Φ (tl xs); Γi ⊢(Ni) (Fix t) : σi) /\
    (sem! Φ ⊨ fun xs => (Σ_{i < J xs} Ni (i ::: xs)) <= M xs) /\
    (exists (Γ_sum : ctx ϕ) (_ : ctxBSum (Fix t) J Γi Γ_sum), (ctx! (Fix t); Φ ⊢ Γ ⊑ Γ_sum)).
Proof.
  intros dec.
  intros Hbsum Hty.
  inv Hty; cbn zeta in *. rename Hty0 into Hty, Hbsum0 into Hbsum2.
  apply submty_Quant_inv' in Hρ as (L&C&Hρ&Hρ'&->).

  destruct Hbsum as [Hbsum|Hbsum]; hnf in Hbsum.
  { now destruct Hbsum as (?&->&[=]). }
  hnf in Hbsum. destruct Hbsum as (L'&A'&->&[= -> <-]); rename L' into L; cbn zeta.


  assert ({ cardH' |
            (sem! Φ ⊨ fun xs => isForestCard (fun b : nat => Ib (b ::: xs)) (Σ_{a < J xs} L (a ::: xs)) (cardH' xs)) /\
            (sem! Φ ⊨ fun xs => cardH' xs <= cardH xs) })
    as (cardH' & HcardH'1 & HcardH'2).
  {
    pose proof @idx_forestCard_split_le _ _ _ _ _ _ dec HcardH Hρ' as (cardH' & cardH'' & HcardH1' & Hcard''1 & HcardH''2).
    exists cardH'. split.
    - auto.
    - hnf; intros xs Hxs. rewrite HcardH''2. lia.
  }
  apply idx_forestCard_split_Sum in HcardH'1 as (cardH'' & HcardH'' & HcardH''2 & HcardH''3); eauto.


  (* We use this shifting function (for b) in A, B, and I *)
  pose (shift_fun_b :=
          fun xs =>
            let b := hd xs in
            let c := hd (tl xs) in
            let xs' := tl (tl xs) in
            (Σ_{d<c} cardH'' (d ::: xs')) + b).

  (* Shifting in the typing judgement *)
  eapply ty_subst_beta_two with (i := shift_fun_b)
    in Hty; unfold subst_beta_two_fun in Hty; cbn in Hty.
  rewrite subst_ctx_beta_two_scons, subst_mty_beta_two_eq_Quant in Hty.

  (* Shifting in the first subtyping *)
  eapply sublty_subst_beta_one_two with (i := shift_fun_b)
    in Hsub; unfold subst_beta_one_two_fun in Hsub; cbn in Hsub.

  (* The shifting function (for a) in C *)
  pose (shift_fun_a :=
          fun xs =>
            let a := hd xs in
            let c := hd (tl xs) in
            let xs' := tl (tl xs) in
            (Σ_{d<c} L (d ::: xs')) + a).

  (* Shifting in the final subtyping *)
  eapply sublty_subst_beta_two with (i := shift_fun_a) in Hρ; unfold subst_beta_two_fun in Hρ; cbn in Hρ.

  (* The shifted first auxilliary forest card *)
  pose (card1' :=
          fun xs : Vector.t nat (S (S (S ϕ))) =>
            let a := hd xs in
            let b := hd (tl xs) in
            let c := hd (tl (tl xs)) in
            let xs' := tl (tl (tl xs)) in
            card1 (a ::: ((Σ_{d<c} cardH'' (d ::: xs')) + b) ::: xs')).

  (* Derivation of the second aux card (from the split card) *)
  assert
    ({card2' : idx (S (S ϕ)) |
       (sem! fun xs : Vector.t nat (S (S ϕ)) => hd xs < L (tl xs) /\ hd (tl xs) < J (tl (tl xs)) /\ Φ (tl (tl xs)) ⊨
             fun xs : Vector.t nat (S (S ϕ)) =>
               isForestCard (fun b : nat => Ib ((Σ_{d < hd (tl xs)} cardH'' (d ::: tl (tl xs))) + b ::: tl (tl xs))) (hd xs) (card2' xs))})
    as (card2' & card2'').
  {
    revert dec HcardH''3; clear_all; intros.
    (* Just shift the [HcardH''3] and introduce "a < L" *)
    assert
      (sem! fun xs : Vector.t nat (S (S ϕ)) => hd xs < L (tl xs) /\ hd (tl xs) < J (tl (tl xs)) /\ Φ (tl (tl xs)) ⊨
            fun xs : Vector.t nat (S (S ϕ)) =>
              isForestCard (fun b : nat => Ib ((Σ_{d < hd (tl xs)} cardH'' (d ::: tl (tl xs))) + b ::: tl (tl xs)))
                           (L (tl xs)) (cardH'' (tl xs))) as Hcard''3' by firstorder.

    (* Now split it into two parts *)
    unshelve epose proof idx_forestCard_split_le hd (fun xs => L (tl xs))
          (fun xs => Ib ((Σ_{d < hd (tl (tl xs))} cardH'' (d ::: tl (tl (tl xs)))) + hd xs ::: tl (tl (tl xs))))
          _ _ Hcard''3' as (card2' & card2'' & Hcard2' & Hcard2'' & HHH); cbn.
    - intros. auto using lt_dec, conj_dec.
    - hnf; intros xs (Hxs&_). lia.
    - eauto.
  }


  pose proof ctxBSum_monotone' Hbsum2 (I2 := fun xs => Σ_{c < J xs} cardH'' (c ::: xs)) as (Σ & HΣ & _ & LL1).
  apply ctxBSum_Sum in HΣ as (Σ'&HΣ1&HΣ2); cbn zeta in *;
    change (fun xs : Vector.t nat (S (S ϕ)) => (Σ_{d < hd (tl xs)} cardH'' (d ::: tl (tl xs))) + hd xs) with (shift_fun_b) in HΣ1.


  eexists _,_. repeat_split.
  {
    eapply ty_Fix with
        (cardH0 := cardH'') (* The new [cardH] only depends on [c < J] *)
        (Ib0 := subst_beta_two_fun shift_fun_b Ib)
        (A0 := subst_lty_beta_one_two shift_fun_b A) (* The bound variable [a < I{...}] in [A] is not substituted *)
        (B0 := subst_lty_beta_one_two shift_fun_b B) (* here [a<1] isn't substituted *)
        (K0 := L)
        (card3 := card1') (card4 := card2')
    ; cbn zeta.
    2-9: clear Hty. (* only needed in goal 1 *)
    - (* Typing *) eapply ty_weaken_Φ. apply Hty. unfold shift_fun_b; cbn.
      hnf; intros xs (Hxs1&Hxs2&Hxs3). split; auto.
      enough ((Σ_{d < hd (tl xs)} cardH'' (d ::: tl (tl xs))) + cardH'' (tl xs) <= cardH (tl (tl xs))) by lia.
      etransitivity. 2: apply HcardH'2; auto.
      hnf in HcardH''. rewrite <- HcardH'' by auto.
      etransitivity.
      2:{ erewrite <- Sum_monotone. 2: eassumption. reflexivity. }
      now cbn; rewrite <- eta.
    - (* First subtyping *) eapply sublty_mono_Φ. etransitivity. 2: apply Hsub. all: clear Hsub.
      + setoid_rewrite subst_lty_twice; unfold funcomp; cbn. unfold subst_beta_one_two_fun; cbn.
        subst shift_fun_b card1'; cbn.
        apply sublty_congr. f_equal. fext; intros f xs. (do 3 f_equal).
        rewrite Nat.add_succ_r. f_equal. lia.
      + hnf; intros xs (Hxs1&Hxs2&Hxs3&Hxs4). repeat_split; auto.
        unfold shift_fun_b.
        set (a := hd xs) in *; set (b := hd (tl xs)) in *; set (c := hd (tl (tl xs))) in *;
          set (xs' := tl (tl (tl xs))) in *; move c before b; move xs' before c.
        enough ((Σ_{d < c} cardH'' (d ::: xs')) + cardH'' (tl (tl xs)) <= cardH xs') by lia.
        hnf in HcardH'2; rewrite <- HcardH'2 by auto.
        hnf in HcardH''; rewrite <- HcardH'' by auto.
        etransitivity.
        2:{ rewrite <- Sum_monotone. 2: eassumption. reflexivity. }
        rewrite Sum_eq_S.
        enough (cardH'' (tl (tl xs)) <= cardH'' (c ::: xs')) by lia.
        subst c xs'. rewrite <- eta. reflexivity.
    - (* (Main) forest [cardH] *)
      hnf; intros xs Hxs. unfold subst_beta_two_fun, shift_fun_b; cbn.
      hnf in HcardH''2. specialize (HcardH''2 xs Hxs). eauto.
    - (* First aux forest card *) hnf; intros xs (Hxs1&Hxs2&Hxs3&Hxs4). unfold subst_beta_two_fun; subst card1'; cbn.
      unfold shift_fun_b; cbn.
      set (a := hd xs) in *; set (b := hd (tl xs)) in *; set (c := hd (tl (tl xs))) in *;
        set (xs' := tl (tl (tl xs))) in *; move c before b; move xs' before c.
      hnf in Hcard1. specialize (Hcard1 (a ::: (Σ_{d < c} cardH'' (d ::: xs')) + b ::: xs')); spec_assert Hcard1.
      { clear Hcard1. cbn. repeat_split; auto. (* Same proof as above! *)
        enough ((Σ_{d < c} cardH'' (d ::: xs')) + cardH'' (tl (tl xs)) <= cardH xs') by lia.
        hnf in HcardH'2; rewrite <- HcardH'2 by auto.
        hnf in HcardH''; rewrite <- HcardH'' by auto.
        etransitivity.
        2:{ rewrite <- Sum_monotone. 2: eassumption. reflexivity. }
        rewrite Sum_eq_S.
        enough (cardH'' (tl (tl xs)) <= cardH'' (c ::: xs')) by lia.
        subst c xs'. rewrite <- eta. reflexivity. }
      cbn in Hcard1. auto. (* only a minor diffence left... *)
      replace (fun c0 : nat => Ib ((Σ_{d < c} cardH'' (d ::: xs')) + S (b + c0) ::: xs')) with
          (fun c0 : nat => Ib (S ((Σ_{d < c} cardH'' (d ::: xs')) + b + c0) ::: xs')); auto.
      { fext; intros. f_equal. now rewrite Nat.add_succ_r, Nat.add_assoc. }
    - (* Second aux forest card *) assumption. (* proof above *)
    - eauto.
    - reflexivity.
    - cbn. instantiate (1 := fun xs => _); cbn. hnf; reflexivity.
    - (* Final subtyping *) eapply submty_Quant. 2: hnf; reflexivity.
      replace (fun xs : Vector.t nat (S (S ϕ)) => hd xs + (Σ_{d < hd (tl xs)} L (d ::: tl (tl xs)))) with
          (fun xs : Vector.t nat (S (S ϕ)) =>(Σ_{d < hd (tl xs)} L (d ::: tl (tl xs))) + hd xs) by (fext; intros; now rewrite Nat.add_comm).

      etransitivity.
      2:{
        eapply sublty_mono_Φ; eauto.
        unfold shift_fun_a; cbn.
        hnf; intros xs (Hxs1&Hxs2&Hxs3); split; auto.
        eapply Nat.lt_le_trans.
        2:{ erewrite <- Sum_monotone. 2: eassumption. reflexivity. }
        cbn. rewrite <- eta. lia.
      }
      {
        setoid_rewrite subst_lty_twice; unfold funcomp; cbn.
        unfold subst_lty_beta_two, subst_beta_one_two_fun, shift_fun_b; cbn.
        eapply sublty_subst_idx_sem; cbn. hnf; intros xs (Ha&Hc&Hxs3). intros j; (do 3 f_equal).
        unfold shift_fun_a; cbn.
        assert ((Σ_{d < hd (tl xs)} L (d ::: tl (tl xs))) + hd xs < Σ_{a < J (tl (tl xs))} L (a ::: tl (tl xs))) as Hxs'.
        {
          eapply Nat.lt_le_trans.
          2:{ erewrite <- Sum_monotone. 2: eassumption. reflexivity. }
          cbn. rewrite <- eta. lia. }

        hnf in Hcard2; specialize (Hcard2 ((Σ_{d < hd (tl xs)} L (d ::: tl (tl xs))) + hd xs ::: tl (tl xs))); spec_assert Hcard2.
        { cbn. repeat_split; auto. hnf in Hρ'. eapply Nat.lt_le_trans. 2: eapply Hρ'; eauto. auto. }
        cbn in Hcard2.
        epose proof isForestCard_split _ _ Hcard2 as (card2_1 & card2_2 & Hcard2_1 & Hcard2_2 & Hcard2_12).
        rewrite Hcard2_12.
        replace (card2_1) with (Σ_{d < hd (tl xs)} cardH'' (d ::: tl (tl xs))) in *.
        2:{ eapply isForestCard_functional; eauto. }
        replace (card2_2) with (card2' xs).
        2:{ eapply isForestCard_functional; eauto. }
        reflexivity.
      }
  }
  {
    cbn. hnf; intros xs Hxs.
    hnf in HM. rewrite <- HM by auto.
    etransitivity.
    2:{ rewrite <- Sum_monotone. 2: now apply HcardH'2. reflexivity. }
    hnf in HcardH''. rewrite <- HcardH'' by auto.
    rewrite Sum_idx_Sum. reflexivity.
  }
  {
    eexists _. exists; eauto.
    etransitivity; eauto.
    eapply subCtx_mono_Φ; eauto.
    { hnf; intros xs Hxs. etransitivity. 2: apply HcardH'2; eauto. hnf in HcardH''. rewrite HcardH''; auto. }
  }
Qed.


Lemma psplitting {ϕ} (Φ : constr ϕ) Γ M v τ (I : idx ϕ) (σi : mty (S ϕ)) :
  (forall xs, { Φ xs } + { ~ Φ xs }) ->
  val v ->
  bsum I σi τ ->
  (ty! Φ; Γ ⊢(M) v : τ) ->
  exists (Ni : idx (S ϕ)) (Δi : ctx (S ϕ)),
    (ty! fun xs => hd xs < I (tl xs) /\ Φ (tl xs); Δi ⊢(Ni) v : σi) /\
    (sem! Φ ⊨ fun xs => (Σ_{i < I xs} Ni (i ::: xs)) <= M xs) /\
    (exists (Δ_sum : ctx ϕ) (_ : ctxBSum v I Δi Δ_sum), (ctx! v; Φ ⊢ Γ ⊑ Δ_sum)).
Proof.
  intros dec Hval Hmsum Hty. destruct Hval; cbn [maxVar] in *.
  - eapply psplitting_Lam; eauto.
  - eapply psplitting_Fix; eauto.
  - eapply psplitting_Const; eauto.
Qed.
