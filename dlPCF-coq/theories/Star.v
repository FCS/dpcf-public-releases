Require Import Omega.
Require Import RelationClasses.


Section Sec1.
  Variables (X: Type) (R: X -> X -> Prop).
  Implicit Types x y z : X.


  Inductive star : X -> X -> Prop :=
  | starRefl x     : star x x
  | starStep x y z : R x y -> star y z -> star x z.



  Definition star_trans x y z : star x y -> star y z -> star x z.
  Proof.
    intros H1 H2. induction H1 as [x|x x' y H _ IH].
    + exact H2.
    + econstructor 2; [exact H|]. auto.
  Qed.

  
  Global Instance star_PreOrder : PreOrder star.
  Proof. constructor. hnf. exact starRefl. exact star_trans. Qed.


  Inductive starI : nat -> X -> X -> Prop :=
  | starIRefl x        : starI 0 x x
  | starIStep x x' y n : R x x' -> starI n x' y -> starI (S n) x y.

  Fact starI_inv0 x y :
    starI 0 x y -> x = y.
  Proof. now inversion 1; subst. Qed.

  Fact starI_invS x z n :
    starI (S n) x z -> exists y, R x y /\ starI n y z.
  Proof. now inversion 1; subst; eauto. Qed.
    

  Fact starI_trans x y z n1 n2 :
    starI n1 x y -> starI n2 y z -> starI (n1+n2) x z.
  Proof.
    intros H1 H2.
    induction H1 as [x|x x' y n H _ IH]; cbn.
    + exact H2.
    + econstructor 2; eauto.
  Qed.



  (*
  Fact starI_split x z n :
    starI n x z ->
    exists n1 n2 y, n = n1 + n2 /\ starI n1 x y /\ starI n2 y z.
  Proof. induction 1; firstorder eauto 7 using starI. Qed.
*)

  Fact starI_split x z m n :
    starI m x z -> n <= m ->
    exists y, starI n x y /\ starI (m-n) y z.
  Proof.
    intros Hstar. revert n; induction Hstar as [ x | x y z n Hstep Hstar IH]; intros m Hle; cbn.
    - assert (m = 0) as -> by omega. cbn. eauto using starI.
    - destruct m; cbn.
      + eauto using starI.
      + assert (m <= n) as Hle' by omega.
        specialize IH with (1 := Hle') as (m'&IH1&IH2).
        eauto using starI.
  Qed.

  Lemma starI_app x y z n :
    starI n x y -> R y z -> starI (S n) x z.
  Proof. intros. replace (S n) with (n + 1) by omega. eapply starI_trans; eauto using starI. Qed.

  Lemma star_starI x y :
    star x y -> exists n, starI n x y.
  Proof. induction 1; firstorder eauto using starI. Qed.

  Lemma starI_star x y n :
    starI n x y -> star x y.
  Proof. induction 1; firstorder eauto using star. Qed.

End Sec1.
