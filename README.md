# Coq formalisation of dlPCFv

Public releases for the dPCF project

This should compile with the following version of Coq:

```
$ coqc --version
The Coq Proof Assistant, version 8.11.1 (April 2020)
```
